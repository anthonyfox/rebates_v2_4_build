﻿using System;
using System.Windows.Forms;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	public partial class frmDisplayAccrual : Form
	{
		private AccrualTypeEnum _accrualTypeEnum;
		private CalculationPeriod _accrualPeriod;
		private Boolean _isDraft;
		private Int32? _rebateId;
		private frmDisplayAccrualDetail _frmDisplayAccrualDetail = null;
		private String _numberFormat = "#,0.00";
		private CalculationPeriod _lastPeriodLoaded = null;

		public frmDisplayAccrual(AccrualTypeEnum accrualTypeEnum, CalculationPeriod accrualPeriod, Boolean isDraft, Int32? rebateId)
		{
			InitializeComponent();

			_accrualTypeEnum = accrualTypeEnum;
			_accrualPeriod = accrualPeriod;
			_isDraft = isDraft;
			_rebateId = rebateId;

			this.Text = _accrualTypeEnum.ToString() + " " + this.Text;
			this.Load += new System.EventHandler(this.frmDisplayAccrual_Load);
			this.Resize += new EventHandler(frmDisplayAccrual_Resize);
		}

		void frmDisplayAccrual_Resize(object sender, EventArgs e)
		{
			_dgvAccrualTotals.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
			_dgvPeriodSummary.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
		}

		private void frmDisplayAccrual_Load(object sender, EventArgs e)
		{
			if (this.MdiParent != null)
			{
				this.Icon = this.MdiParent.Icon;
			}

			LoadAccrualSummary();

			_dgvAccrualTotals.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			_dgvAccrualTotals.RowEnter += new DataGridViewCellEventHandler(_dgvAccrualTotals_RowEnter);
			_dgvAccrualTotals.DataError += new DataGridViewDataErrorEventHandler(DataGridViewUtilities.DataError);
			_dgvAccrualTotals.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(_dgvAccrualTotals_DataBindingComplete);

			_dgvPeriodSummary.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			_dgvPeriodSummary.CellDoubleClick += new DataGridViewCellEventHandler(_dgvPeriodSummary_CellDoubleClick);
			_dgvPeriodSummary.DataError += new DataGridViewDataErrorEventHandler(DataGridViewUtilities.DataError);
			_dgvPeriodSummary.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(_dgvPeriodSummary_DataBindingComplete);

			this.FormClosing += new FormClosingEventHandler(frmDisplayAccrual_FormClosing);
		}

		void _dgvPeriodSummary_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			DataGridViewUtilities.SetFormat(_dgvAccrualTotals, typeof(Decimal), _numberFormat, DataGridViewContentAlignment.MiddleRight);
			_dgvPeriodSummary.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
		}

		void _dgvAccrualTotals_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			DataGridViewUtilities.SetFormat(_dgvPeriodSummary, typeof(Decimal), _numberFormat, DataGridViewContentAlignment.MiddleRight);
			DataGridViewUtilities.SetFormat(_dgvPeriodSummary, typeof(Decimal?), _numberFormat, DataGridViewContentAlignment.MiddleRight);
			_dgvAccrualTotals.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
		}

		void frmDisplayAccrual_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (_frmDisplayAccrualDetail != null)
			{
				_frmDisplayAccrualDetail.Close();
				_frmDisplayAccrualDetail.Dispose();
			}
		}

		void _dgvAccrualTotals_RowEnter(object sender, DataGridViewCellEventArgs e)
		{
			if (e.RowIndex >= 0)
			{
				CalculationPeriod period = null;

				AccrualSummary accrualSummary = (AccrualSummary) (_dgvAccrualTotals.Rows[e.RowIndex].DataBoundItem);
				period = accrualSummary.ActualPeriod;

				LoadAccruals(period);
			}
		}

		void _dgvPeriodSummary_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
		{
			if (e.RowIndex >= 0 && _dgvPeriodSummary.Rows[e.RowIndex].DataBoundItem != null)
			{
				this.Cursor = Cursors.WaitCursor;
				Accrual accrual = (Accrual) _dgvPeriodSummary.CurrentRow.DataBoundItem;

				if (e.ColumnIndex >= 0 && _dgvPeriodSummary.Columns[e.ColumnIndex].DataPropertyName == AccrualProperty.RebateNumber)
				{
					if (accrual.RebateId.HasValue)
					{
						Rebate rebate = new Rebate();
						rebate.Id = accrual.RebateId.Value;

						if (rebate.ReadCurrent())
						{
							frmMDIParent mdiParent = (frmMDIParent) this.MdiParent;
							Boolean isNew = false;
							Form form = mdiParent.OpenSingleInstance(typeof(frmRebate), new Object[] { rebate }, out isNew, rebate.WindowTitle);
						}
					}
				}
				else
				{
					_frmDisplayAccrualDetail = new frmDisplayAccrualDetail(accrual.Id, null, null);
					_frmDisplayAccrualDetail.MdiParent = this.MdiParent;

					if (_frmDisplayAccrualDetail.DialogResult != DialogResult.Cancel)
					{
						_frmDisplayAccrualDetail.Show();
					}
					else
					{
						_frmDisplayAccrualDetail.Close();
						_frmDisplayAccrualDetail.Dispose();
						_frmDisplayAccrualDetail = null;
					}
				}

				this.Cursor = Cursors.Default;
			}
		}

		private void LoadAccrualSummary()
		{
			if (_rebateId != null)
			{
				Rebate rebate = new Rebate();
				rebate.Id = _rebateId.Value;
				rebate.ReadCurrent();

				if (rebate != null)
				{
					this.Text += "(Rebate: " + rebate.Number.ToString() + " - " + rebate.Description + " Version: " + rebate.Version.ToString() + ")";
				}
			}

			AccrualSummary accrualSummary = new AccrualSummary();
			accrualSummary.DivisionId = Program.Division.Id;

			_dgvAccrualTotals.DataSource = accrualSummary.CreateSummary(_accrualTypeEnum, _accrualPeriod, _rebateId, _isDraft);
			_dgvAccrualTotals.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);

			if (_dgvAccrualTotals.Rows.Count > 0)
			{
				_dgvAccrualTotals.Rows[_dgvAccrualTotals.Rows.Count - 1].Selected = true;
			}
		}

		private void LoadAccruals(CalculationPeriod period)
		{
			if (period != null)
			{
				if (_lastPeriodLoaded == null || !_lastPeriodLoaded.Equals(period))
				{
					// Setup read
					Accrual accrual = new Accrual();

					accrual.DivisionId = Program.Division.Id;
					accrual.AccrualTypeEnum = _accrualTypeEnum;
					accrual.ActualYear = period.Year;
					accrual.ActualPeriod = period.Period;

					accrual.AddReadFilter(AccrualProperty.DivisionId, "=");
					accrual.AddReadFilter(AccrualProperty.AccrualTypeEnum, "=");
					accrual.AddReadFilter(AccrualProperty.ActualYear, "=");
					accrual.AddReadFilter(AccrualProperty.ActualPeriod, "=");

					if (_rebateId == null)
					{
						// Only read summary details where rebates matching exist
						accrual.AddReadFilter(AccrualProperty.RebateId, "is not");
					}
					else
					{
						accrual.RebateId = _rebateId.Value;
						accrual.AddReadFilter(AccrualProperty.RebateId, "=");
					}

					// Ignore erroneous data
					accrual.ReadFilter += " and DivisionAccrual.Year = " + period.Year;

					accrual.OrderBy =
						"Rebate.RebateNumber, " +
						AccrualColumn.ActualYear + ", " +
						AccrualColumn.ActualPeriod + ", " +
						AccrualColumn.CreateYear + ", " +
						AccrualColumn.CreatePeriod + ", " +
						AccrualColumn.InsertSequence;

					// Now read and bind
					ExtendedBindingList<Accrual> accruals = (ExtendedBindingList<Accrual>) accrual.Read();
					_dgvPeriodSummary.DataSource = accruals;
					_lastPeriodLoaded = period;
				}
			}
			else
			{
				_dgvPeriodSummary.DataSource = null;
			}

			_dgvPeriodSummary.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
			_dgvPeriodSummary.Columns[AccrualProperty.RebateDescription].Frozen = true;
		}
	}
}

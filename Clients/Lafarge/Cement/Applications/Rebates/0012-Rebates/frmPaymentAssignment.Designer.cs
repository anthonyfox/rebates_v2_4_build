﻿namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	partial class frmPaymentAssignment
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this._txtValue = new System.Windows.Forms.TextBox();
			this._lblValue = new System.Windows.Forms.Label();
			this._btnAdd = new System.Windows.Forms.Button();
			this._errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
			this._lblPercentage = new System.Windows.Forms.Label();
			this._txtPercentage = new System.Windows.Forms.TextBox();
			this._lblPercent = new System.Windows.Forms.Label();
			this._lblInstructions = new System.Windows.Forms.Label();
			this._ucPayee = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Payee();
			this._ucTaxCode = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Lookups.TaxCode();
			this._lblTaxRate = new System.Windows.Forms.Label();
			this._pnlBottom = new System.Windows.Forms.Panel();
			this._pnlTop = new System.Windows.Forms.Panel();
			this._txtComment = new System.Windows.Forms.TextBox();
			this._lblComment = new System.Windows.Forms.Label();
			this._lblTaxDate = new System.Windows.Forms.Label();
			this._dtpTaxDate = new System.Windows.Forms.DateTimePicker();
			((System.ComponentModel.ISupportInitialize) (this._errorProvider)).BeginInit();
			this._pnlBottom.SuspendLayout();
			this._pnlTop.SuspendLayout();
			this.SuspendLayout();
			// 
			// _txtValue
			// 
			this._txtValue.Location = new System.Drawing.Point(93, 42);
			this._txtValue.Name = "_txtValue";
			this._txtValue.Size = new System.Drawing.Size(83, 20);
			this._txtValue.TabIndex = 2;
			// 
			// _lblValue
			// 
			this._lblValue.AutoSize = true;
			this._lblValue.Location = new System.Drawing.Point(52, 45);
			this._lblValue.Name = "_lblValue";
			this._lblValue.Size = new System.Drawing.Size(34, 13);
			this._lblValue.TabIndex = 1;
			this._lblValue.Text = "Value";
			// 
			// _btnAdd
			// 
			this._btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
			this._btnAdd.Location = new System.Drawing.Point(618, 29);
			this._btnAdd.Name = "_btnAdd";
			this._btnAdd.Size = new System.Drawing.Size(38, 34);
			this._btnAdd.TabIndex = 1;
			this._btnAdd.Text = "+";
			this._btnAdd.UseVisualStyleBackColor = true;
			// 
			// _errorProvider
			// 
			this._errorProvider.ContainerControl = this;
			// 
			// _lblPercentage
			// 
			this._lblPercentage.AutoSize = true;
			this._lblPercentage.Location = new System.Drawing.Point(24, 67);
			this._lblPercentage.Name = "_lblPercentage";
			this._lblPercentage.Size = new System.Drawing.Size(62, 13);
			this._lblPercentage.TabIndex = 3;
			this._lblPercentage.Text = "Percentage";
			// 
			// _txtPercentage
			// 
			this._txtPercentage.Location = new System.Drawing.Point(93, 64);
			this._txtPercentage.Name = "_txtPercentage";
			this._txtPercentage.Size = new System.Drawing.Size(36, 20);
			this._txtPercentage.TabIndex = 4;
			// 
			// _lblPercent
			// 
			this._lblPercent.AutoSize = true;
			this._lblPercent.Location = new System.Drawing.Point(130, 67);
			this._lblPercent.Name = "_lblPercent";
			this._lblPercent.Size = new System.Drawing.Size(15, 13);
			this._lblPercent.TabIndex = 5;
			this._lblPercent.Text = "%";
			// 
			// _lblInstructions
			// 
			this._lblInstructions.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this._lblInstructions.Location = new System.Drawing.Point(24, 27);
			this._lblInstructions.Name = "_lblInstructions";
			this._lblInstructions.Size = new System.Drawing.Size(438, 36);
			this._lblInstructions.TabIndex = 0;
			this._lblInstructions.Text = "You can leave both the value and percentage values blank in order to allocate the" +
				" remainding unsassigned payment amount";
			// 
			// _ucPayee
			// 
			this._ucPayee.AccountNumber = null;
			this._ucPayee.DivisionId = 0;
			this._ucPayee.LabelText = "Payee";
			this._ucPayee.Location = new System.Drawing.Point(50, 20);
			this._ucPayee.MultiSelect = false;
			this._ucPayee.Name = "_ucPayee";
			this._ucPayee.Size = new System.Drawing.Size(417, 20);
			this._ucPayee.TabIndex = 0;
			// 
			// _ucTaxCode
			// 
			this._ucTaxCode.IsZeroRateOnly = false;
			this._ucTaxCode.Location = new System.Drawing.Point(93, 86);
			this._ucTaxCode.Name = "_ucTaxCode";
			this._ucTaxCode.Size = new System.Drawing.Size(152, 21);
			this._ucTaxCode.TabIndex = 7;
			// 
			// _lblTaxRate
			// 
			this._lblTaxRate.AutoSize = true;
			this._lblTaxRate.Location = new System.Drawing.Point(35, 90);
			this._lblTaxRate.Name = "_lblTaxRate";
			this._lblTaxRate.Size = new System.Drawing.Size(51, 13);
			this._lblTaxRate.TabIndex = 6;
			this._lblTaxRate.Text = "Tax Rate";
			// 
			// _pnlBottom
			// 
			this._pnlBottom.Controls.Add(this._lblInstructions);
			this._pnlBottom.Controls.Add(this._btnAdd);
			this._pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
			this._pnlBottom.Location = new System.Drawing.Point(0, 145);
			this._pnlBottom.Name = "_pnlBottom";
			this._pnlBottom.Size = new System.Drawing.Size(668, 85);
			this._pnlBottom.TabIndex = 1;
			// 
			// _pnlTop
			// 
			this._pnlTop.Controls.Add(this._dtpTaxDate);
			this._pnlTop.Controls.Add(this._lblTaxDate);
			this._pnlTop.Controls.Add(this._txtComment);
			this._pnlTop.Controls.Add(this._lblComment);
			this._pnlTop.Controls.Add(this._ucPayee);
			this._pnlTop.Controls.Add(this._txtValue);
			this._pnlTop.Controls.Add(this._lblTaxRate);
			this._pnlTop.Controls.Add(this._lblValue);
			this._pnlTop.Controls.Add(this._ucTaxCode);
			this._pnlTop.Controls.Add(this._txtPercentage);
			this._pnlTop.Controls.Add(this._lblPercentage);
			this._pnlTop.Controls.Add(this._lblPercent);
			this._pnlTop.Dock = System.Windows.Forms.DockStyle.Fill;
			this._pnlTop.Location = new System.Drawing.Point(0, 0);
			this._pnlTop.Name = "_pnlTop";
			this._pnlTop.Size = new System.Drawing.Size(668, 145);
			this._pnlTop.TabIndex = 0;
			// 
			// _txtComment
			// 
			this._txtComment.Location = new System.Drawing.Point(93, 109);
			this._txtComment.MaxLength = 80;
			this._txtComment.Name = "_txtComment";
			this._txtComment.Size = new System.Drawing.Size(546, 20);
			this._txtComment.TabIndex = 11;
			// 
			// _lblComment
			// 
			this._lblComment.AutoSize = true;
			this._lblComment.Location = new System.Drawing.Point(35, 112);
			this._lblComment.Name = "_lblComment";
			this._lblComment.Size = new System.Drawing.Size(51, 13);
			this._lblComment.TabIndex = 10;
			this._lblComment.Text = "Comment";
			// 
			// _lblTaxDate
			// 
			this._lblTaxDate.AutoSize = true;
			this._lblTaxDate.Location = new System.Drawing.Point(263, 90);
			this._lblTaxDate.Name = "_lblTaxDate";
			this._lblTaxDate.Size = new System.Drawing.Size(51, 13);
			this._lblTaxDate.TabIndex = 8;
			this._lblTaxDate.Text = "Tax Date";
			// 
			// _dtpTaxDate
			// 
			this._dtpTaxDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this._dtpTaxDate.Location = new System.Drawing.Point(320, 86);
			this._dtpTaxDate.Name = "_dtpTaxDate";
			this._dtpTaxDate.Size = new System.Drawing.Size(95, 20);
			this._dtpTaxDate.TabIndex = 9;
			// 
			// frmPaymentAssignment
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(668, 230);
			this.Controls.Add(this._pnlTop);
			this.Controls.Add(this._pnlBottom);
			this.Name = "frmPaymentAssignment";
			this.Text = "Add Payment Assignment";
			((System.ComponentModel.ISupportInitialize) (this._errorProvider)).EndInit();
			this._pnlBottom.ResumeLayout(false);
			this._pnlTop.ResumeLayout(false);
			this._pnlTop.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TextBox _txtValue;
		private System.Windows.Forms.Label _lblValue;
		private System.Windows.Forms.Button _btnAdd;
		private System.Windows.Forms.ErrorProvider _errorProvider;
		private System.Windows.Forms.Label _lblPercent;
		private System.Windows.Forms.Label _lblPercentage;
		private System.Windows.Forms.TextBox _txtPercentage;
		private System.Windows.Forms.Label _lblInstructions;
		private UserControls.Rebates.Payee _ucPayee;
		private System.Windows.Forms.Panel _pnlTop;
		private System.Windows.Forms.Label _lblTaxRate;
		private UserControls.Rebates.Lookups.TaxCode _ucTaxCode;
		private System.Windows.Forms.Panel _pnlBottom;
		private System.Windows.Forms.TextBox _txtComment;
		private System.Windows.Forms.Label _lblComment;
		private System.Windows.Forms.DateTimePicker _dtpTaxDate;
		private System.Windows.Forms.Label _lblTaxDate;

	}
}
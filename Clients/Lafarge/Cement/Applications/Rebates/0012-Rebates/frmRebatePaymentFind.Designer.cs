﻿namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	partial class frmRebatePaymentFind
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this._lblPayThruYear = new System.Windows.Forms.Label();
			this._txtPayThruYear = new System.Windows.Forms.TextBox();
			this._txtPayThruPeriod = new System.Windows.Forms.TextBox();
			this._lblPayThruPeriod = new System.Windows.Forms.Label();
			this._pnlIsContract.SuspendLayout();
			this._pnlIsVAP.SuspendLayout();
			this._pnlIsGlobalProduct.SuspendLayout();
			((System.ComponentModel.ISupportInitialize) (this._errorProvider)).BeginInit();
			this.SuspendLayout();
			// 
			// _btnSearch
			// 
			this._btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
			// 
			// _lblPayThruYear
			// 
			this._lblPayThruYear.AutoSize = true;
			this._lblPayThruYear.Location = new System.Drawing.Point(57, 448);
			this._lblPayThruYear.Name = "_lblPayThruYear";
			this._lblPayThruYear.Size = new System.Drawing.Size(75, 13);
			this._lblPayThruYear.TabIndex = 74;
			this._lblPayThruYear.Text = "Pay Thru Year";
			// 
			// _txtPayThruYear
			// 
			this._txtPayThruYear.Location = new System.Drawing.Point(138, 445);
			this._txtPayThruYear.Name = "_txtPayThruYear";
			this._txtPayThruYear.Size = new System.Drawing.Size(50, 20);
			this._txtPayThruYear.TabIndex = 75;
			// 
			// _txtPayThruPeriod
			// 
			this._txtPayThruPeriod.Location = new System.Drawing.Point(249, 445);
			this._txtPayThruPeriod.Name = "_txtPayThruPeriod";
			this._txtPayThruPeriod.Size = new System.Drawing.Size(20, 20);
			this._txtPayThruPeriod.TabIndex = 76;
			// 
			// _lblPayThruPeriod
			// 
			this._lblPayThruPeriod.AutoSize = true;
			this._lblPayThruPeriod.Location = new System.Drawing.Point(211, 448);
			this._lblPayThruPeriod.Name = "_lblPayThruPeriod";
			this._lblPayThruPeriod.Size = new System.Drawing.Size(37, 13);
			this._lblPayThruPeriod.TabIndex = 77;
			this._lblPayThruPeriod.Text = "Period";
			// 
			// frmRebatePaymentFind
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.ClientSize = new System.Drawing.Size(583, 479);
			this.Controls.Add(this._txtPayThruYear);
			this.Controls.Add(this._txtPayThruPeriod);
			this.Controls.Add(this._lblPayThruPeriod);
			this.Controls.Add(this._lblPayThruYear);
			this.Name = "frmRebatePaymentFind";
			this.Controls.SetChildIndex(this._lblRebateDetailId, 0);
			this.Controls.SetChildIndex(this._txtRebateDetailId, 0);
			this.Controls.SetChildIndex(this._lblPayThruYear, 0);
			this.Controls.SetChildIndex(this._lblPayThruPeriod, 0);
			this.Controls.SetChildIndex(this._txtPayThruPeriod, 0);
			this.Controls.SetChildIndex(this._txtPayThruYear, 0);
			this.Controls.SetChildIndex(this._btnSearch, 0);
			this.Controls.SetChildIndex(this._ucPaymentFrequency, 0);
			this.Controls.SetChildIndex(this._lblIsGlobalProduct, 0);
			this.Controls.SetChildIndex(this._pnlIsGlobalProduct, 0);
			this.Controls.SetChildIndex(this._ucRebateType, 0);
			this.Controls.SetChildIndex(this._lblRebateNumber, 0);
			this.Controls.SetChildIndex(this._lblDescription, 0);
			this.Controls.SetChildIndex(this._txtRebateNumber, 0);
			this.Controls.SetChildIndex(this._txtDescription, 0);
			this.Controls.SetChildIndex(this._ucCustomer, 0);
			this.Controls.SetChildIndex(this._ucPayee, 0);
			this.Controls.SetChildIndex(this._ucDeliveryType, 0);
			this.Controls.SetChildIndex(this._ucHierarchy, 0);
			this.Controls.SetChildIndex(this._ucSoldFrom, 0);
			this.Controls.SetChildIndex(this._lblRebateDate, 0);
			this.Controls.SetChildIndex(this._lblPeriod, 0);
			this.Controls.SetChildIndex(this._ucCustomerGroup, 0);
			this.Controls.SetChildIndex(this._lblVersion, 0);
			this.Controls.SetChildIndex(this._txtVersion, 0);
			this.Controls.SetChildIndex(this._pnlIsContract, 0);
			this.Controls.SetChildIndex(this._lblIsContract, 0);
			this.Controls.SetChildIndex(this._pnlIsVAP, 0);
			this.Controls.SetChildIndex(this._lblIsVAP, 0);
			this.Controls.SetChildIndex(this._txtYear, 0);
			this.Controls.SetChildIndex(this._txtPeriod, 0);
			this._pnlIsContract.ResumeLayout(false);
			this._pnlIsContract.PerformLayout();
			this._pnlIsVAP.ResumeLayout(false);
			this._pnlIsVAP.PerformLayout();
			this._pnlIsGlobalProduct.ResumeLayout(false);
			this._pnlIsGlobalProduct.PerformLayout();
			((System.ComponentModel.ISupportInitialize) (this._errorProvider)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}

		#endregion

		private System.Windows.Forms.Label _lblPayThruYear;
		private System.Windows.Forms.TextBox _txtPayThruYear;
		private System.Windows.Forms.TextBox _txtPayThruPeriod;
		private System.Windows.Forms.Label _lblPayThruPeriod;
	}
}

﻿namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	partial class frmDisplayAccrual
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this._splitContainer = new System.Windows.Forms.SplitContainer();
			this._grpAccrualTotals = new System.Windows.Forms.GroupBox();
			this._dgvAccrualTotals = new System.Windows.Forms.DataGridView();
			this._grpPeriodSummary = new System.Windows.Forms.GroupBox();
			this._dgvPeriodSummary = new System.Windows.Forms.DataGridView();
			this._splitContainer.Panel1.SuspendLayout();
			this._splitContainer.Panel2.SuspendLayout();
			this._splitContainer.SuspendLayout();
			this._grpAccrualTotals.SuspendLayout();
			((System.ComponentModel.ISupportInitialize) (this._dgvAccrualTotals)).BeginInit();
			this._grpPeriodSummary.SuspendLayout();
			((System.ComponentModel.ISupportInitialize) (this._dgvPeriodSummary)).BeginInit();
			this.SuspendLayout();
			// 
			// _splitContainer
			// 
			this._splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this._splitContainer.Location = new System.Drawing.Point(0, 0);
			this._splitContainer.Name = "_splitContainer";
			this._splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// _splitContainer.Panel1
			// 
			this._splitContainer.Panel1.Controls.Add(this._grpAccrualTotals);
			// 
			// _splitContainer.Panel2
			// 
			this._splitContainer.Panel2.Controls.Add(this._grpPeriodSummary);
			this._splitContainer.Size = new System.Drawing.Size(827, 454);
			this._splitContainer.SplitterDistance = 175;
			this._splitContainer.TabIndex = 0;
			// 
			// _grpAccrualTotals
			// 
			this._grpAccrualTotals.Controls.Add(this._dgvAccrualTotals);
			this._grpAccrualTotals.Dock = System.Windows.Forms.DockStyle.Fill;
			this._grpAccrualTotals.Location = new System.Drawing.Point(0, 0);
			this._grpAccrualTotals.Name = "_grpAccrualTotals";
			this._grpAccrualTotals.Size = new System.Drawing.Size(827, 175);
			this._grpAccrualTotals.TabIndex = 0;
			this._grpAccrualTotals.TabStop = false;
			this._grpAccrualTotals.Text = "Accrual Totals By Period";
			// 
			// _dgvAccrualTotals
			// 
			this._dgvAccrualTotals.AllowUserToAddRows = false;
			this._dgvAccrualTotals.AllowUserToDeleteRows = false;
			this._dgvAccrualTotals.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this._dgvAccrualTotals.Dock = System.Windows.Forms.DockStyle.Fill;
			this._dgvAccrualTotals.Location = new System.Drawing.Point(3, 16);
			this._dgvAccrualTotals.Name = "_dgvAccrualTotals";
			this._dgvAccrualTotals.ReadOnly = true;
			this._dgvAccrualTotals.Size = new System.Drawing.Size(821, 156);
			this._dgvAccrualTotals.TabIndex = 0;
			// 
			// _grpPeriodSummary
			// 
			this._grpPeriodSummary.Controls.Add(this._dgvPeriodSummary);
			this._grpPeriodSummary.Dock = System.Windows.Forms.DockStyle.Fill;
			this._grpPeriodSummary.Location = new System.Drawing.Point(0, 0);
			this._grpPeriodSummary.Name = "_grpPeriodSummary";
			this._grpPeriodSummary.Size = new System.Drawing.Size(827, 275);
			this._grpPeriodSummary.TabIndex = 0;
			this._grpPeriodSummary.TabStop = false;
			this._grpPeriodSummary.Text = "Period Summary";
			// 
			// _dgvPeriodSummary
			// 
			this._dgvPeriodSummary.AllowUserToAddRows = false;
			this._dgvPeriodSummary.AllowUserToDeleteRows = false;
			this._dgvPeriodSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this._dgvPeriodSummary.Dock = System.Windows.Forms.DockStyle.Fill;
			this._dgvPeriodSummary.Location = new System.Drawing.Point(3, 16);
			this._dgvPeriodSummary.Name = "_dgvPeriodSummary";
			this._dgvPeriodSummary.ReadOnly = true;
			this._dgvPeriodSummary.Size = new System.Drawing.Size(821, 256);
			this._dgvPeriodSummary.TabIndex = 0;
			// 
			// frmDisplayAccrual
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(827, 454);
			this.Controls.Add(this._splitContainer);
			this.Name = "frmDisplayAccrual";
			this.Text = "Accrual Calculation Display";
			this._splitContainer.Panel1.ResumeLayout(false);
			this._splitContainer.Panel2.ResumeLayout(false);
			this._splitContainer.ResumeLayout(false);
			this._grpAccrualTotals.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize) (this._dgvAccrualTotals)).EndInit();
			this._grpPeriodSummary.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize) (this._dgvPeriodSummary)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.SplitContainer _splitContainer;
		private System.Windows.Forms.GroupBox _grpAccrualTotals;
		private System.Windows.Forms.GroupBox _grpPeriodSummary;
		private System.Windows.Forms.DataGridView _dgvAccrualTotals;
		private System.Windows.Forms.DataGridView _dgvPeriodSummary;

	}
}
﻿namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	partial class frmAccrual
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAccrual));
            this._lblLastAccrual = new System.Windows.Forms.Label();
            this._lblAccrualPeriod = new System.Windows.Forms.Label();
            this._txtLastAccrual = new System.Windows.Forms.TextBox();
            this._txtAccrual = new System.Windows.Forms.TextBox();
            this._labelDescription = new System.Windows.Forms.Label();
            this._btnCalculate = new System.Windows.Forms.Button();
            this._lnkLastDraft = new System.Windows.Forms.LinkLabel();
            this._btnFinalise = new System.Windows.Forms.Button();
            this._pgbStage = new System.Windows.Forms.ProgressBar();
            this._chkIncludeInvoiceAudit = new System.Windows.Forms.CheckBox();
            this._lbIncludeInvoiceAudit = new System.Windows.Forms.Label();
            this._pnlCalculationStatus = new System.Windows.Forms.Panel();
            this._lblProgress = new System.Windows.Forms.Label();
            this._pgbProgress = new System.Windows.Forms.ProgressBar();
            this._lblStage = new System.Windows.Forms.Label();
            this.btnMultiAccrual = new System.Windows.Forms.Button();
            this._accrualEndMonth = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this._pnlCalculationStatus.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // _lblLastAccrual
            // 
            this._lblLastAccrual.AutoSize = true;
            this._lblLastAccrual.Location = new System.Drawing.Point(24, 74);
            this._lblLastAccrual.Name = "_lblLastAccrual";
            this._lblLastAccrual.Size = new System.Drawing.Size(99, 13);
            this._lblLastAccrual.TabIndex = 1;
            this._lblLastAccrual.Text = "Last Accrual Period";
            // 
            // _lblAccrualPeriod
            // 
            this._lblAccrualPeriod.AutoSize = true;
            this._lblAccrualPeriod.Location = new System.Drawing.Point(47, 97);
            this._lblAccrualPeriod.Name = "_lblAccrualPeriod";
            this._lblAccrualPeriod.Size = new System.Drawing.Size(76, 13);
            this._lblAccrualPeriod.TabIndex = 3;
            this._lblAccrualPeriod.Text = "Accrual Period";
            // 
            // _txtLastAccrual
            // 
            this._txtLastAccrual.Location = new System.Drawing.Point(129, 71);
            this._txtLastAccrual.Name = "_txtLastAccrual";
            this._txtLastAccrual.ReadOnly = true;
            this._txtLastAccrual.Size = new System.Drawing.Size(59, 20);
            this._txtLastAccrual.TabIndex = 2;
            // 
            // _txtAccrual
            // 
            this._txtAccrual.Location = new System.Drawing.Point(129, 94);
            this._txtAccrual.Name = "_txtAccrual";
            this._txtAccrual.ReadOnly = true;
            this._txtAccrual.Size = new System.Drawing.Size(59, 20);
            this._txtAccrual.TabIndex = 4;
            // 
            // _labelDescription
            // 
            this._labelDescription.BackColor = System.Drawing.Color.PowderBlue;
            this._labelDescription.Location = new System.Drawing.Point(25, 43);
            this._labelDescription.Name = "_labelDescription";
            this._labelDescription.Size = new System.Drawing.Size(356, 43);
            this._labelDescription.TabIndex = 0;
            this._labelDescription.Text = resources.GetString("_labelDescription.Text");
            // 
            // _btnCalculate
            // 
            this._btnCalculate.BackColor = System.Drawing.Color.DeepSkyBlue;
            this._btnCalculate.FlatAppearance.BorderSize = 0;
            this._btnCalculate.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this._btnCalculate.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._btnCalculate.Location = new System.Drawing.Point(57, 168);
            this._btnCalculate.Margin = new System.Windows.Forms.Padding(0);
            this._btnCalculate.Name = "_btnCalculate";
            this._btnCalculate.Size = new System.Drawing.Size(114, 23);
            this._btnCalculate.TabIndex = 8;
            this._btnCalculate.Text = "Calculate Accruals";
            this._btnCalculate.UseVisualStyleBackColor = false;
            this._btnCalculate.Click += new System.EventHandler(this._btnCalculate_Click);
            // 
            // _lnkLastDraft
            // 
            this._lnkLastDraft.AutoSize = true;
            this._lnkLastDraft.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lnkLastDraft.Location = new System.Drawing.Point(23, 137);
            this._lnkLastDraft.Name = "_lnkLastDraft";
            this._lnkLastDraft.Size = new System.Drawing.Size(191, 13);
            this._lnkLastDraft.TabIndex = 5;
            this._lnkLastDraft.TabStop = true;
            this._lnkLastDraft.Text = "Last Draft Run %LastDraftDate%";
            // 
            // _btnFinalise
            // 
            this._btnFinalise.BackColor = System.Drawing.Color.DeepSkyBlue;
            this._btnFinalise.FlatAppearance.BorderSize = 0;
            this._btnFinalise.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this._btnFinalise.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._btnFinalise.Location = new System.Drawing.Point(204, 168);
            this._btnFinalise.Margin = new System.Windows.Forms.Padding(0);
            this._btnFinalise.Name = "_btnFinalise";
            this._btnFinalise.Size = new System.Drawing.Size(114, 23);
            this._btnFinalise.TabIndex = 9;
            this._btnFinalise.Text = "Finalise Accruals";
            this._btnFinalise.UseVisualStyleBackColor = false;
            this._btnFinalise.Click += new System.EventHandler(this._btnFinalise_Click);
            // 
            // _pgbStage
            // 
            this._pgbStage.Location = new System.Drawing.Point(10, 30);
            this._pgbStage.Name = "_pgbStage";
            this._pgbStage.Size = new System.Drawing.Size(809, 23);
            this._pgbStage.TabIndex = 1;
            // 
            // _chkIncludeInvoiceAudit
            // 
            this._chkIncludeInvoiceAudit.AutoSize = true;
            this._chkIncludeInvoiceAudit.Location = new System.Drawing.Point(137, 105);
            this._chkIncludeInvoiceAudit.Name = "_chkIncludeInvoiceAudit";
            this._chkIncludeInvoiceAudit.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._chkIncludeInvoiceAudit.Size = new System.Drawing.Size(15, 14);
            this._chkIncludeInvoiceAudit.TabIndex = 7;
            this._chkIncludeInvoiceAudit.UseVisualStyleBackColor = true;
            // 
            // _lbIncludeInvoiceAudit
            // 
            this._lbIncludeInvoiceAudit.AutoSize = true;
            this._lbIncludeInvoiceAudit.Location = new System.Drawing.Point(23, 106);
            this._lbIncludeInvoiceAudit.Name = "_lbIncludeInvoiceAudit";
            this._lbIncludeInvoiceAudit.Size = new System.Drawing.Size(107, 13);
            this._lbIncludeInvoiceAudit.TabIndex = 6;
            this._lbIncludeInvoiceAudit.Text = "Include Invoice Audit";
            // 
            // _pnlCalculationStatus
            // 
            this._pnlCalculationStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._pnlCalculationStatus.Controls.Add(this._lblProgress);
            this._pnlCalculationStatus.Controls.Add(this._pgbProgress);
            this._pnlCalculationStatus.Controls.Add(this._lblStage);
            this._pnlCalculationStatus.Controls.Add(this._pgbStage);
            this._pnlCalculationStatus.Location = new System.Drawing.Point(11, 292);
            this._pnlCalculationStatus.Name = "_pnlCalculationStatus";
            this._pnlCalculationStatus.Size = new System.Drawing.Size(841, 114);
            this._pnlCalculationStatus.TabIndex = 10;
            // 
            // _lblProgress
            // 
            this._lblProgress.AutoSize = true;
            this._lblProgress.Location = new System.Drawing.Point(11, 64);
            this._lblProgress.Name = "_lblProgress";
            this._lblProgress.Size = new System.Drawing.Size(142, 13);
            this._lblProgress.TabIndex = 2;
            this._lblProgress.Text = "Accrual Calculation Progress";
            // 
            // _pgbProgress
            // 
            this._pgbProgress.Location = new System.Drawing.Point(10, 80);
            this._pgbProgress.Name = "_pgbProgress";
            this._pgbProgress.Size = new System.Drawing.Size(809, 23);
            this._pgbProgress.TabIndex = 3;
            // 
            // _lblStage
            // 
            this._lblStage.AutoSize = true;
            this._lblStage.Location = new System.Drawing.Point(11, 14);
            this._lblStage.Name = "_lblStage";
            this._lblStage.Size = new System.Drawing.Size(90, 13);
            this._lblStage.TabIndex = 0;
            this._lblStage.Text = "Stage Information";
            // 
            // btnMultiAccrual
            // 
            this.btnMultiAccrual.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnMultiAccrual.FlatAppearance.BorderSize = 0;
            this.btnMultiAccrual.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnMultiAccrual.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnMultiAccrual.Location = new System.Drawing.Point(144, 168);
            this.btnMultiAccrual.Margin = new System.Windows.Forms.Padding(0);
            this.btnMultiAccrual.Name = "btnMultiAccrual";
            this.btnMultiAccrual.Size = new System.Drawing.Size(155, 23);
            this.btnMultiAccrual.TabIndex = 11;
            this.btnMultiAccrual.Text = "Multi Month Accrual";
            this.btnMultiAccrual.UseVisualStyleBackColor = false;
            this.btnMultiAccrual.Click += new System.EventHandler(this.btnMultiAccrual_Click);
            // 
            // _accrualEndMonth
            // 
            this._accrualEndMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._accrualEndMonth.FormattingEnabled = true;
            this._accrualEndMonth.Items.AddRange(new object[] {
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec"});
            this._accrualEndMonth.Location = new System.Drawing.Point(173, 93);
            this._accrualEndMonth.Name = "_accrualEndMonth";
            this._accrualEndMonth.Size = new System.Drawing.Size(121, 21);
            this._accrualEndMonth.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(26, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(367, 43);
            this.label1.TabIndex = 13;
            this.label1.Text = "This process will remove EVERY Accrual for this year and then re-calculate them u" +
    "p to and including the selected month.";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(69, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 23);
            this.label2.TabIndex = 14;
            this.label2.Text = "Select End Month";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightBlue;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this._labelDescription);
            this.panel1.Controls.Add(this._lbIncludeInvoiceAudit);
            this.panel1.Controls.Add(this._chkIncludeInvoiceAudit);
            this.panel1.Controls.Add(this._lnkLastDraft);
            this.panel1.Controls.Add(this._btnCalculate);
            this.panel1.Controls.Add(this._btnFinalise);
            this.panel1.Location = new System.Drawing.Point(11, 14);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(417, 257);
            this.panel1.TabIndex = 15;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.checkBox1);
            this.panel2.Controls.Add(this.linkLabel1);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Location = new System.Drawing.Point(-2, -2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(417, 257);
            this.panel2.TabIndex = 16;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(133, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(132, 24);
            this.label4.TabIndex = 10;
            this.label4.Text = "Single Accrual";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(25, 43);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(356, 43);
            this.label5.TabIndex = 0;
            this.label5.Text = resources.GetString("label5.Text");
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(23, 106);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Include Invoice Audit";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(137, 105);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 7;
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel1.Location = new System.Drawing.Point(23, 137);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(191, 13);
            this.linkLabel1.TabIndex = 5;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Last Draft Run %LastDraftDate%";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Location = new System.Drawing.Point(28, 168);
            this.button1.Margin = new System.Windows.Forms.Padding(0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(114, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Calculate Accruals";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this._btnCalculate_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Location = new System.Drawing.Point(267, 168);
            this.button2.Margin = new System.Windows.Forms.Padding(0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(114, 23);
            this.button2.TabIndex = 9;
            this.button2.Text = "Finalise Accruals";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this._btnFinalise_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(133, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(132, 24);
            this.label3.TabIndex = 10;
            this.label3.Text = "Single Accrual";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.btnMultiAccrual);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this._accrualEndMonth);
            this.panel3.Location = new System.Drawing.Point(435, 14);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(417, 257);
            this.panel3.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(140, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(185, 24);
            this.label7.TabIndex = 11;
            this.label7.Text = "Multi Month Accruals";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // frmAccrual
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(881, 418);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this._pnlCalculationStatus);
            this.Controls.Add(this._txtAccrual);
            this.Controls.Add(this._txtLastAccrual);
            this.Controls.Add(this._lblAccrualPeriod);
            this.Controls.Add(this._lblLastAccrual);
            this.Name = "frmAccrual";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Accrual Calculation v2.3";
            this.Load += new System.EventHandler(this.Accrual_Load);
            this._pnlCalculationStatus.ResumeLayout(false);
            this._pnlCalculationStatus.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label _lblLastAccrual;
		private System.Windows.Forms.Label _lblAccrualPeriod;
		private System.Windows.Forms.TextBox _txtLastAccrual;
		private System.Windows.Forms.TextBox _txtAccrual;
		private System.Windows.Forms.Label _labelDescription;
		private System.Windows.Forms.Button _btnCalculate;
		private System.Windows.Forms.LinkLabel _lnkLastDraft;
		private System.Windows.Forms.Button _btnFinalise;
		private System.Windows.Forms.ProgressBar _pgbStage;
		private System.Windows.Forms.CheckBox _chkIncludeInvoiceAudit;
		private System.Windows.Forms.Label _lbIncludeInvoiceAudit;
		private System.Windows.Forms.Panel _pnlCalculationStatus;
		private System.Windows.Forms.Label _lblStage;
		private System.Windows.Forms.Label _lblProgress;
		private System.Windows.Forms.ProgressBar _pgbProgress;
        private System.Windows.Forms.Button btnMultiAccrual;
        private System.Windows.Forms.ComboBox _accrualEndMonth;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label7;
    }
}
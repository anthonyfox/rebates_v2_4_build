﻿using System;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Clients.Lafarge.Cement.UserControls.Rebates;
using Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Forms;
using Evolve.Libraries.WinformsApplication;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	public partial class frmMDIParent : Form
	{
		public ApplicationStatus ApplicationStatus
		{
			get { return _applicationStatus; }
		}

		public frmMDIParent()
		{
			InitializeComponent();

			// Hide tools / snapshot menu option
			_tsmiTools.Visible = false;
            _tsmiTools.Visible = true;

            // Hide help option until its been implemented
            _tsbHelp.Visible = false;

			#region Menu

			_tsmiNewRebate.Click += new EventHandler(_tsmiNewRebate_Click);
			_tsmiRebateFind.Click += new EventHandler(_tsmiRebateFind_Click);
			_tsmiRebateCopy.Click += new EventHandler(_tsmiRebateCopy_Click);
			_tsmiAccrualCalculate.Click += new EventHandler(_tsmiAccrualCalculate_Click);
			_tsmiForecast.Click += new EventHandler(_tsmiForecast_Click);
			_tsmiPaymentsSummary.Click += new EventHandler(_tsmiPaymentsSummary_Click);
			_tsmiPendingAuthorisation.Click += new EventHandler(_tsmiPendingAuthorisation_Click);
			_tsmiSnapShot.Click += new EventHandler(_tsmiSnapShot_Click);
            _tsmiAudit.Click += new EventHandler(_tsmiAudit_Click);
			_tsmiClose.Click += new EventHandler(_tsmiClose_Click);
			_tsmiCloseAll.Click += new EventHandler(_tsmiCloseAll_Click);
			_tsmiCascade.Click += new EventHandler(_tsmiCascade_Click);
			_tsmiTileVertical.Click += new EventHandler(_tsmiTileVertical_Click);
			_tsmiTileHorizontal.Click += new EventHandler(_tsmiTileHorizontal_Click);
			_tsmiArrangeAll.Click += new EventHandler(_tsmiArrangeAll_Click);
	
			#endregion

			#region ToolStrip

			_tsbRebateNew.Click += new EventHandler(_tsbRebateNew_Click);
			_tsbRebateCopy.Click += new EventHandler(_tsbRebateCopy_Click);
			_tsbRebateFind.Click += new EventHandler(_tsbRebateFind_Click);
			_tsbAccrualCalculate.Click += new EventHandler(_tsbAccrualCalculate_Click);
			_tsbPaymentSummary.Click += new EventHandler(_tsbPaymentSummary_Click);
			_tsbPendingAuthorisation.Click += new EventHandler(_tsbPendingAuthorisation_Click);
			_tsbSave.Click += new EventHandler(_tsbSave_Click);
			_tsbSaveAll.Click += new EventHandler(_tsbSaveAll_Click);
			_tsbHelp.Click += new EventHandler(_tsbHelp_Click);

			#endregion
		}

		#region ToolStrip

		void _tsbRebateNew_Click(object sender, EventArgs e)
		{
			NewRebate();
		}
		void _tsbRebateCopy_Click(object sender, EventArgs e)
		{
			CopyRebate();
		}
		void _tsbRebateFind_Click(object sender, EventArgs e)
		{
			RebateFind();
		}
		void _tsbAccrualCalculate_Click(object sender, EventArgs e)
		{
			AccrualCalculate();
		}
		void _tsbPaymentSummary_Click(object sender, EventArgs e)
		{
			PaymentSummary();
		}
		void _tsbPendingAuthorisation_Click(object sender, EventArgs e)
		{
			PendingAuthorisation();
		}
		void _tsbSave_Click(object sender, EventArgs e)
		{
			Save();
		}
		void _tsbSaveAll_Click(object sender, EventArgs e)
		{
			SaveAll();
		}

		void _tsbHelp_Click(object sender, EventArgs e)
		{
			throw new NotImplementedException();
		}

		#endregion

		#region Menu

		#region MENU: Rebates
		void _tsmiNewRebate_Click(object sender, EventArgs e)
		{
			NewRebate();
		}
		void _tsmiRebateFind_Click(object sender, EventArgs e)
		{
			RebateFind();
		}
		void _tsmiRebateCopy_Click(object sender, EventArgs e)
		{
			CopyRebate();
		}
		#endregion

		#region MENU: Accrual
		void _tsmiAccrualCalculate_Click(object sender, EventArgs e)
		{
			AccrualCalculate();
		}
		void _tsmiForecast_Click(object sender, EventArgs e)
		{
			ForecastCalculate();
		}
		#endregion

		#region MENU: Payments
		
		void _tsmiPaymentsSummary_Click(object sender, EventArgs e)
		{
			PaymentSummary();
		}

		private void _tsmiPendingAuthorisation_Click(object sender, EventArgs e)
		{
			PendingAuthorisation();
		}

		private void rejectedPaymentsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Boolean isNew;
			OpenSingleInstance(typeof(frmRejectedPayments), out isNew);
		}

		private void paymentSeachToolStripMenuItem_Click(object sender, EventArgs e)
		{
			PaymentFind();
		}

		#endregion

		#region MENU: Tools
		void _tsmiSnapShot_Click(object sender, EventArgs e)
		{
			SnapShot();
		}

	    void _tsmiAudit_Click(object sender, EventArgs e)
	    {
	        FindAudit();
	    }
		#endregion

		#region MENU: Windows
		void _tsmiClose_Click(object sender, EventArgs e)
		{
			CloseForm();
		}
		void _tsmiCloseAll_Click(object sender, EventArgs e)
		{
			CloseAll();
		}
		void _tsmiCascade_Click(object sender, EventArgs e)
		{
			LayoutMdi(MdiLayout.Cascade);
		}
		void _tsmiTileVertical_Click(object sender, EventArgs e)
		{
			LayoutMdi(MdiLayout.TileVertical);
		}
		void _tsmiTileHorizontal_Click(object sender, EventArgs e)
		{
			LayoutMdi(MdiLayout.TileHorizontal);
		}
		void _tsmiArrangeAll_Click(object sender, EventArgs e)
		{
			LayoutMdi(MdiLayout.ArrangeIcons);
		}
		#endregion

		#region MENU: Maintenance

		private void deliveryWindowsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Boolean isNew = false;
			Business.Rebates.DeliveryWindow deliveryWindow = new Business.Rebates.DeliveryWindow();

			OpenSingleInstance(typeof(frmRebateExtension), new Object[] { typeof(Business.Rebates.DeliveryWindow) }, out isNew, deliveryWindow.Title);
		}

		private void leadTimesToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Boolean isNew = false;
			Business.Rebates.LeadTime leadTime = new Business.Rebates.LeadTime();

			OpenSingleInstance(typeof(frmRebateExtension), new Object[] { typeof(Business.Rebates.LeadTime) }, out isNew, leadTime.Title);
		}

		private void pricingSubGroupsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Boolean isNew = false;
			Business.Rebates.PricingSubGroup pricingSubGroup = new Business.Rebates.PricingSubGroup();

			OpenSingleInstance(typeof(frmRebateExtension), new Object[] { typeof(Business.Rebates.PricingSubGroup) }, out isNew, pricingSubGroup.Title);
		}

		private void vatCodeToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Boolean isNew = false;
			String title = "VAT Codes";

			frmList frmList = (frmList) OpenSingleInstance(typeof(frmList), new Object[] { title }, out isNew, title);

			if (isNew)
			{
				TaxCode taxCode = new TaxCode();
				ExtendedBindingList<TaxCode> taxCodes = (ExtendedBindingList<TaxCode>) taxCode.Read();
				frmList.DataList = taxCodes;
			}
		}

		private void vatRateToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Boolean isNew = false;
			String title = "VAT Rates";

			frmList frmList = (frmList) OpenSingleInstance(typeof(frmList), new Object[] { title }, out isNew, title);

			if (isNew)
			{
				TaxRate taxRate = new TaxRate();
				ExtendedBindingList<TaxRate> taxRates = (ExtendedBindingList<TaxRate>) taxRate.Read();
				frmList.DataList = taxRates;
			}
		}

		#endregion

		#endregion

		#region Business Logic

		private void SnapShot()
		{
			if (this.ActiveMdiChild == null)
			{
				MessageBox.Show("No active window to take a snap shot of", "Send Snap Shot To Evolve", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			else
			{
				Type formType = this.ActiveMdiChild.GetType();
				MethodInfo snapShotMethod = formType.GetMethod("SnapShot");

				if (snapShotMethod == null)
				{
					MessageBox.Show("Snap shot not available for this window", "Send Snap Shot To Evolve", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
				else
				{
					snapShotMethod.Invoke(this.ActiveMdiChild, null);
				}
			}
		}

	    private void FindAudit()
	    {
            Boolean isNew = false;
            AuditFind auditFind = (AuditFind)OpenSingleInstance(typeof(AuditFind), new Object[] { Program.Division }, out isNew);

            if (isNew)
            {
                auditFind.DisplayResults += new UserControls.Rebates.Forms.AuditFind.DisplayResultsHandler(auditFind_DisplayResults);
            }

        }

        private void CopyRebate()
		{
			if (this.ActiveMdiChild != null)
			{
				Type formType = this.ActiveMdiChild.GetType();
				MethodInfo copyMethod = formType.GetMethod("Copy");

				if (copyMethod != null)
				{
					copyMethod.Invoke(this.ActiveMdiChild, null);
				}
			}
		}

		private void NewRebate()
		{
			frmRebate rebate = new frmRebate();
			rebate.MdiParent = this;
			rebate.Show();
		}

		private void RebateFind()
		{
			Boolean isNew = false;
			RebateFind rebateFind = (RebateFind) OpenSingleInstance(typeof(RebateFind), new Object[] { Program.Division }, out isNew);

			if (isNew)
			{
				rebateFind.DisplayResults += new UserControls.Rebates.RebateFind.DisplayResultsHandler(rebateFind_DisplayResults);
			}
		}
        void auditFind_DisplayResults(object sender, EventArgs e)
        {
            AuditFind auditFind = (AuditFind) sender;

                frmAuditFindResults results = new frmAuditFindResults(auditFind);
                results.MdiParent = this;
                results.Show();
        }

        void rebateFind_DisplayResults(object sender, EventArgs e)
		{
			RebateFind rebateFind = (RebateFind) sender;

			if (rebateFind.Rebates.Count == 1)
			{
				frmRebate frmRebate = new frmRebate((Rebate) rebateFind.Rebates[0]);

				if (frmRebate.DialogResult != DialogResult.Abort)
				{
					frmRebate.MdiParent = this;
					frmRebate.Show();
				}
			}
			else
			{
				frmRebateFindResults results = new frmRebateFindResults(rebateFind);
				results.MdiParent = this;
				results.Show();
			}
		}

		private void AccrualCalculate()
		{
			Boolean isNew = false;
			OpenSingleInstance(typeof(frmAccrual), out isNew);
		}

		private void ForecastCalculate()
		{
			Boolean isNew = false;
			OpenSingleInstance(typeof(frmForecast), out isNew);
		}

		private void PaymentSummary()
		{
			Boolean isNew = false;
			frmRebatePaymentFind rebatePaymentFind = (frmRebatePaymentFind) OpenSingleInstance(typeof(frmRebatePaymentFind), new Object[] { Program.Division }, out isNew);

			if (isNew)
			{
				rebatePaymentFind.DisplayResults += new UserControls.Rebates.RebateFind.DisplayResultsHandler(rebatePaymentFind_DisplayResults);
			}
		}

		private void PendingAuthorisation()
		{
			Boolean isNew = false;
			frmPaymentAwaitingAuthorisation frmPaymentAwaitingAuthorisation = (frmPaymentAwaitingAuthorisation) OpenSingleInstance(typeof(frmPaymentAwaitingAuthorisation), out isNew);
			frmPaymentAwaitingAuthorisation.PaymentStatusChanged += new Rebates.frmPaymentAwaitingAuthorisation.PaymentStatusChangedEventHandler(frmPaymentAwaitingAuthorisation_PaymentStatusChanged);
		}

		void frmPaymentAwaitingAuthorisation_PaymentStatusChanged(object sender, EventArgs e)
		{
			foreach (Form form in this.MdiChildren)
			{
				if (form is frmPayment)
				{
					((frmPayment) form).ProcessPaymentStatusChanged();
				}
			}
		}

		void rebatePaymentFind_DisplayResults(object sender, EventArgs e)
		{
			frmRebatePaymentFind rebateFind = (frmRebatePaymentFind) sender;

			frmRebateFindResults results = new frmRebateFindResults(rebateFind);
			results.MdiParent = this;
			results.IsPaymentSearch = true;
			results.PayThruPeriod = rebateFind.PayThruPeriod;
			results.Show();
		}

		private void PaymentFind()
		{
			Boolean isNew = false;
			PaymentFind paymentFind = (PaymentFind) OpenSingleInstance(typeof(PaymentFind), new Object[] { Program.Division }, out isNew);

			if (isNew)
			{
				paymentFind.DisplayResults += new UserControls.Rebates.PaymentFind.DisplayResultsHandler(paymentFind_DisplayResults);
			}
		}

		void paymentFind_DisplayResults(object sender, EventArgs e)
		{
			PaymentFind paymentFind = (PaymentFind)sender;

			frmPaymentReview results = new frmPaymentReview(paymentFind);
			results.MdiParent = this;
			results.Show();
		}

		private void Save()
		{
			if (this.ActiveMdiChild != null)
			{
				Save(this.ActiveMdiChild);
			}
		}

		private void Save(Form form)
		{
			Type formType = form.GetType();
			MethodInfo saveMethod = formType.GetMethod("Save");

			if (saveMethod != null)
			{
				try
				{
					saveMethod.Invoke(form, null);
				}
				catch (Exception ex)
				{
					// Ensure the actual error gets thrown not the Invoke Exception
					throw ex.InnerException;
				}
			}
		}

		private void SaveAll()
		{
			foreach (Form childForm in MdiChildren)
			{
				Save(childForm);
			}
		}

		private void CloseForm()
		{
			if (this.ActiveMdiChild != null)
			{
				this.ActiveMdiChild.Close();
			}
		}

		private void CloseAll()
		{
			foreach (Form childForm in MdiChildren)
			{
				childForm.Close();
			}
		}

		public Form OpenSingleInstance(Type type, out Boolean isNew)
		{
			return OpenSingleInstance(type, null, out isNew);
		}

		public Form OpenSingleInstance(Type type, Object[] parameters, out Boolean isNew)
		{
			return OpenSingleInstance(type, parameters, out isNew, null);
		}

		public Form OpenSingleInstance(Type type, Object[] parameters, out Boolean isNew, String compareText)
		{
			Form form = null;
			isNew = true;

			foreach (Form checkForm in this.MdiChildren.Where(f => f.GetType() == type))
			{
				if (compareText == null)
				{
					isNew = false;
				}
				else
				{
					isNew = !(checkForm.Text.StartsWith(compareText));
				}

				if (!isNew)
				{
					form = checkForm;
					break;
				}
			}

			if (isNew)
			{
				try
				{
					if (parameters == null)
					{
						form = (Form) Activator.CreateInstance(type);
					}
					else
					{
						form = (Form) Activator.CreateInstance(type, parameters);
					}
				}
				catch (Exception ex)
				{
					ApplicationError applicationError = new ApplicationError(ex);
				}

				form.MdiParent = this;
				form.Icon = this.Icon;
			}

			form.Show();
			form.Activate();

			return form;
		}

		#endregion
	}
}

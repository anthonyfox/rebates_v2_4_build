﻿namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	partial class frmRebateExtensionDetail
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this._lblCode = new System.Windows.Forms.Label();
			this._txtCode = new System.Windows.Forms.TextBox();
			this._txtDescription = new System.Windows.Forms.TextBox();
			this._lblDescription = new System.Windows.Forms.Label();
			this.okCancel = new Evolve.Libraries.Controls.Generic.OkCancel();
			this._errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
			((System.ComponentModel.ISupportInitialize) (this._errorProvider)).BeginInit();
			this.SuspendLayout();
			// 
			// _lblCode
			// 
			this._lblCode.AutoSize = true;
			this._lblCode.Location = new System.Drawing.Point(46, 22);
			this._lblCode.Name = "_lblCode";
			this._lblCode.Size = new System.Drawing.Size(32, 13);
			this._lblCode.TabIndex = 0;
			this._lblCode.Text = "Code";
			// 
			// _txtCode
			// 
			this._txtCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this._txtCode.Location = new System.Drawing.Point(87, 19);
			this._txtCode.Name = "_txtCode";
			this._txtCode.Size = new System.Drawing.Size(100, 20);
			this._txtCode.TabIndex = 1;
			// 
			// _txtDescription
			// 
			this._txtDescription.Location = new System.Drawing.Point(87, 42);
			this._txtDescription.Name = "_txtDescription";
			this._txtDescription.Size = new System.Drawing.Size(298, 20);
			this._txtDescription.TabIndex = 3;
			// 
			// _lblDescription
			// 
			this._lblDescription.AutoSize = true;
			this._lblDescription.Location = new System.Drawing.Point(18, 45);
			this._lblDescription.Name = "_lblDescription";
			this._lblDescription.Size = new System.Drawing.Size(60, 13);
			this._lblDescription.TabIndex = 2;
			this._lblDescription.Text = "Description";
			// 
			// okCancel
			// 
			this.okCancel.CausesValidation = false;
			this.okCancel.Location = new System.Drawing.Point(228, 95);
			this.okCancel.Name = "okCancel";
			this.okCancel.Size = new System.Drawing.Size(157, 24);
			this.okCancel.TabIndex = 4;
			// 
			// _errorProvider
			// 
			this._errorProvider.ContainerControl = this;
			// 
			// frmRebateExtensionDetail
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(417, 137);
			this.Controls.Add(this.okCancel);
			this.Controls.Add(this._txtDescription);
			this.Controls.Add(this._lblDescription);
			this.Controls.Add(this._txtCode);
			this.Controls.Add(this._lblCode);
			this.Name = "frmRebateExtensionDetail";
			this.Text = "Delivery Window";
			((System.ComponentModel.ISupportInitialize) (this._errorProvider)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label _lblCode;
		private System.Windows.Forms.TextBox _txtCode;
		private System.Windows.Forms.TextBox _txtDescription;
		private System.Windows.Forms.Label _lblDescription;
		private Libraries.Controls.Generic.OkCancel okCancel;
		private System.Windows.Forms.ErrorProvider _errorProvider;
	}
}
﻿namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	partial class frmDisplayAccrualDetail
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this._dgvAccrualDetail = new System.Windows.Forms.DataGridView();
			((System.ComponentModel.ISupportInitialize) (this._dgvAccrualDetail)).BeginInit();
			this.SuspendLayout();
			// 
			// _dgvAccrualDetail
			// 
			this._dgvAccrualDetail.AllowUserToAddRows = false;
			this._dgvAccrualDetail.AllowUserToDeleteRows = false;
			this._dgvAccrualDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this._dgvAccrualDetail.Dock = System.Windows.Forms.DockStyle.Fill;
			this._dgvAccrualDetail.Location = new System.Drawing.Point(0, 0);
			this._dgvAccrualDetail.Name = "_dgvAccrualDetail";
			this._dgvAccrualDetail.ReadOnly = true;
			this._dgvAccrualDetail.Size = new System.Drawing.Size(752, 392);
			this._dgvAccrualDetail.TabIndex = 0;
			// 
			// frmDisplayAccrualDetail
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(752, 392);
			this.Controls.Add(this._dgvAccrualDetail);
			this.Name = "frmDisplayAccrualDetail";
			this.Text = "Accrual Detail";
			((System.ComponentModel.ISupportInitialize) (this._dgvAccrualDetail)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.DataGridView _dgvAccrualDetail;
	}
}
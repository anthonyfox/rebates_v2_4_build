﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using System.Configuration;
using System.Data.SqlClient;
using Evolve.Libraries.Data.Remoting;

namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	public partial class frmAccrual : Form
	{
		private Timer _timer = new Timer();
		private CalculationPeriod _accrualPeriod;
		private AccrualCalculation _accrualCalculation;
		private frmDisplayAccrual _frmDisplayAccrual;
		private DivisionAccrual _lastDraftAccrual;
		private String _lastDraftText;

		public frmAccrual()
		{
			InitializeComponent();

            _accrualEndMonth.SelectedIndex = (DateTime.Now.Month)-1;
         //   string periodMonth = DateTime.Now.Month.ToString();
         //   _accrualEndMonth.SelectedIndex = _accrualEndMonth.FindStringExact("Feb");


            _lastDraftText = _lnkLastDraft.Text;
			
			// Ensure invoice audit is always turned on and the user
			// cannot change the option
			_chkIncludeInvoiceAudit.Checked = true;
			_lbIncludeInvoiceAudit.Visible = false;
			_chkIncludeInvoiceAudit.Visible = false;

			// Clear Division entity collections to ensure upto date data is displayed
			Program.Division.ClearEntityCollection();

			// Events hooks
			_btnCalculate.Click += new EventHandler(_btnCalculate_Click);
			_btnFinalise.Click += new EventHandler(_btnFinalise_Click);
			_lnkLastDraft.Click += new EventHandler(_lnkLastDraft_Click);
	
			this.Load += new EventHandler(frmAccrual_Load);
			this.FormClosing += new FormClosingEventHandler(frmAccrual_FormClosing);

			// Set timer to ensure update of last draft details
			_timer.Interval = 60000;
			_timer.Tick += new EventHandler(_timer_Tick);
			_timer.Start();
		}

		void _timer_Tick(object sender, EventArgs e)
		{
			SetupLastAccrual();
		}

		void _lnkLastDraft_Click(object sender, EventArgs e)
		{
			DisplayAccrual();
		}

		void frmAccrual_Load(object sender, EventArgs e)
		{
			if (this.MdiParent != null)
			{
				this.Icon = this.MdiParent.Icon;
			}
		}

		void frmAccrual_FormClosing(object sender, FormClosingEventArgs e)
		{
			CloseDisplayAccrual();
		}

		private void Accrual_Load(object sender, EventArgs e)
		{
			SetupLastAccrual();

			_pnlCalculationStatus.Visible = false;
			this.ActiveControl = _btnCalculate;
		}

		private void SetupLastAccrual()
		{
			Program.Division.ClearEntityCollection();

			_lastDraftAccrual = Program.Division.LastDraftAccrual();
			CalculationPeriod lastAccrualPeriod = Program.Division.LastAccrualPeriod();
			_accrualPeriod = lastAccrualPeriod.NextPeriod();

			_txtLastAccrual.Text = lastAccrualPeriod.ToString();
			_txtAccrual.Text = _accrualPeriod.ToString();

			if (_lastDraftAccrual != null)
			{
				_lnkLastDraft.Text = _lastDraftText.Replace("%LastDraftDate%", _lastDraftAccrual.RunTime.ToShortDateString() + " " + _lastDraftAccrual.RunTime.ToShortTimeString());
				_btnCalculate.Text = "Re-Calculate";
			}
			else
			{
				_lnkLastDraft.Visible = false;
				_btnFinalise.Visible = false;
			}
		}

        public void RunStoredProc()
        {

            //create the data connection using the data remoting object
            DataConnection dataConnection = new DataConnection();

            try
            {
                //open connection
                dataConnection.OpenConnection();

                //create sql command using data connection sql connection
                SqlCommand cmd = new SqlCommand("TEB_Reset_Accruals_to_start_of_Current_Year", dataConnection.SqlConnection);

                //command type to sproc
                cmd.CommandType = CommandType.StoredProcedure;

                //set transaction type
                cmd.Transaction = dataConnection.SqlTransaction;

                //execute
                cmd.ExecuteNonQuery();

            }
            finally
            {
                //close connection
                if (dataConnection != null)
                {
                    dataConnection.CloseConnection();
                }
            }


        }
        private void btnMultiAccrual_Click(object sender, EventArgs e)
        {

       
            // Call SQL proc to blat database of previous accruals
            RunStoredProc();
            //_accrualPeriod.Period = 12;
            //_accrualPeriod.Year = 2017;

            int flagLoop = 0;
            int _currentMonth = _accrualEndMonth.SelectedIndex + 1;
            do
            {
                _txtLastAccrual.Text = _accrualPeriod.PreviousPeriod().ToString();
                _txtAccrual.Text = _accrualPeriod.ToString();
                // Disable buttons
                _btnCalculate.Enabled = false;
			    _btnFinalise.Enabled = false;
                btnMultiAccrual.Enabled = false;

            


			Application.DoEvents();

			// Set cursor to processing
			this.Cursor = Cursors.WaitCursor;

			if (Program.Division.StartCalculation(Program.User, AccrualTypeEnum.Actual))
			{
              
                    // Close existing accrual calculation display
                    CloseDisplayAccrual();

                    // Process
                    _accrualCalculation = new AccrualCalculation(Program.User, Program.Division, _accrualPeriod, AccrualTypeEnum.Actual, _chkIncludeInvoiceAudit.Checked);
                    _accrualCalculation.RebateProcessed += new AccrualCalculation.RebateProcessedHandler(AccrualCalculation_RebateProcessed);

                    // Configure maximum threads based on Environment specific setup (if none default of 10 will be used)
                    SettingsProperty maximumThreadsProperty = Properties.Settings.Default.Properties[Program.Environment + "_MaximumThreads"];

                    if (maximumThreadsProperty != null)
                    {
                        Int32 maximumThreads;

                        if (Int32.TryParse(maximumThreadsProperty.DefaultValue.ToString(), out maximumThreads))
                        {
                            _accrualCalculation.MaximumThreads = maximumThreads;
                        }
                    }


                    // Setup display
                    _lblStage.Text = _accrualCalculation.StageText;
                    _pgbStage.Value = 0;
                    _pnlCalculationStatus.Visible = true;

                    Application.DoEvents();

                    // Perform calculation
                    if (_accrualCalculation.Calculate())
                    {
                        _lastDraftAccrual = _accrualCalculation.DivisionAccrual;
                        _lnkLastDraft.Text = _lastDraftText.Replace("%LastDraftDate%", _lastDraftAccrual.RunTime.ToShortDateString() + " " + _lastDraftAccrual.RunTime.ToShortTimeString());
                        _lnkLastDraft.Visible = true;

                        DisplayAccrual();
                    }

                    // Reset button text and re-enable
                    _btnCalculate.Text = "Re-Calculate";
                    Program.Division.EndCalculation(Program.User, AccrualTypeEnum.Actual);

                 

            }
            else
			{
				User user = new User();
				user.Id = Program.Division.CalculationUserId.Value;
				user.ReadCurrent();

				DateTime time = Program.Division.CalculationTime.Value;
				MessageBox.Show(user.Name + " started an accrual calculation at " + time.ToShortTimeString() + " on " + time.ToShortDateString() + "\nPlease re-try later", "Accrual Calculation In Progress", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}


			// Set cursor to processing completed
			this.Cursor = Cursors.Default;


                _btnFinalise_Click(null, null);
                _accrualPeriod = _accrualPeriod.NextPeriod();
                flagLoop = flagLoop + 1;
            } while (flagLoop < _currentMonth);

            _btnCalculate.Enabled = true;
            _btnFinalise.Enabled = true;
            _btnFinalise.Visible = true;
            btnMultiAccrual.Enabled = true;
            //DisplayAccrual();
        }



        void _btnCalculate_Click(object sender, EventArgs e)
        {
          
                // Disable buttons
                _btnCalculate.Enabled = false;
                _btnFinalise.Enabled = false;




                Application.DoEvents();

                // Set cursor to processing
                this.Cursor = Cursors.WaitCursor;

                if (Program.Division.StartCalculation(Program.User, AccrualTypeEnum.Actual))
                {

                    // Close existing accrual calculation display
                    CloseDisplayAccrual();

                    // Process
                    _accrualCalculation = new AccrualCalculation(Program.User, Program.Division, _accrualPeriod, AccrualTypeEnum.Actual, _chkIncludeInvoiceAudit.Checked);
                    _accrualCalculation.RebateProcessed += new AccrualCalculation.RebateProcessedHandler(AccrualCalculation_RebateProcessed);

                    // Configure maximum threads based on Environment specific setup (if none default of 10 will be used)
                    SettingsProperty maximumThreadsProperty = Properties.Settings.Default.Properties[Program.Environment + "_MaximumThreads"];

                    if (maximumThreadsProperty != null)
                    {
                        Int32 maximumThreads;

                        if (Int32.TryParse(maximumThreadsProperty.DefaultValue.ToString(), out maximumThreads))
                        {
                            _accrualCalculation.MaximumThreads = maximumThreads;
                        }
                    }


                    // Setup display
                    _lblStage.Text = _accrualCalculation.StageText;
                    _pgbStage.Value = 0;
                    _pnlCalculationStatus.Visible = true;

                    Application.DoEvents();

                    // Perform calculation
                    if (_accrualCalculation.Calculate())
                    {
                        _lastDraftAccrual = _accrualCalculation.DivisionAccrual;
                        _lnkLastDraft.Text = _lastDraftText.Replace("%LastDraftDate%", _lastDraftAccrual.RunTime.ToShortDateString() + " " + _lastDraftAccrual.RunTime.ToShortTimeString());
                        _lnkLastDraft.Visible = true;

                        DisplayAccrual();
                    }

                    // Reset button text and re-enable
                    _btnCalculate.Text = "Re-Calculate";
                    Program.Division.EndCalculation(Program.User, AccrualTypeEnum.Actual);



                }
                else
                {
                    User user = new User();
                    user.Id = Program.Division.CalculationUserId.Value;
                    user.ReadCurrent();

                    DateTime time = Program.Division.CalculationTime.Value;
                    MessageBox.Show(user.Name + " started an accrual calculation at " + time.ToShortTimeString() + " on " + time.ToShortDateString() + "\nPlease re-try later", "Accrual Calculation In Progress", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                _btnCalculate.Enabled = true;
                _btnFinalise.Enabled = true;
                _btnFinalise.Visible = true;

                // Set cursor to processing completed
                this.Cursor = Cursors.Default;


             
        }



        #region AccrualCalculation RebateProcessed Event

        private delegate void DisplayProcessedCallBack();
		private void AccrualCalculation_RebateProcessed(object sender, EventArgs e)
		{
			if (this.InvokeRequired)
			{
				DisplayProcessedCallBack d = new DisplayProcessedCallBack(DisplayProcessed);
				this.Invoke(d, null);
			}
			else
			{
				DisplayProcessed();
			}
		}

		private void DisplayProcessed()
		{
			_lblStage.Text = _accrualCalculation.StageText;
			_pgbStage.Value = _accrualCalculation.StagePercentageComplete;
			_pgbProgress.Value = _accrualCalculation.PercentComplete;

			Application.DoEvents();
		}

		#endregion

		private void DisplayAccrual()
		{
			_frmDisplayAccrual = new frmDisplayAccrual(AccrualTypeEnum.Actual, _accrualPeriod, true, null);
			_frmDisplayAccrual.MdiParent = this.MdiParent;
			_frmDisplayAccrual.Show();
		}

		private void CloseDisplayAccrual()
		{
			if (_frmDisplayAccrual != null)
			{
				_frmDisplayAccrual.Close();
				_frmDisplayAccrual.Dispose();
				_frmDisplayAccrual = null;
			}
		}

		void _btnFinalise_Click(object sender, EventArgs e)
		{
			AccrualCalculationStatus status = AccrualCalculation.CanFinalise(_lastDraftAccrual);
			Boolean canFinalise = true;

			if (status == AccrualCalculationStatus.Warn)
			{
				DialogResult result = MessageBox.Show("Rebates have been amended since the draft calculation\nAre you sure you wish to finalise ?", "Rebates Amended", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);

				if (result == DialogResult.No)
				{
					canFinalise = false;
				}
			}
			else if (status == AccrualCalculationStatus.Fail)
			{
				MessageBox.Show("Rebates have been amended since the last draft calculation\nThis would affect accruals for the year, and therefore accruals\nmust be recalculated", "Rebates Amended", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}

			if (canFinalise)
			{
				Boolean isOk = true;

				if (_lastDraftAccrual.Period == 12)
				{
					DialogResult result = MessageBox.Show("Are you sure you wish to close down the year and rollover the Rebates ?", "Year End Rollover", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
					isOk = (result == DialogResult.Yes);
				}

				if (isOk)
				{
					if (AccrualCalculation.Finalise(_lastDraftAccrual))
					{
					//	this.Close();
					}
				}
			}
		}


    }
}

﻿namespace Evolve.Clients.Lafarge.Cement.Rebates
{
    partial class frmMDIParent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMDIParent));
            this._menuStrip = new System.Windows.Forms.MenuStrip();
            this._tsmiRebate = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiNewRebate = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiRebateFind = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiRebateCopy = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiAccrual = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiAccrualCalculate = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiForecast = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiPayments = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiPaymentsSummary = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiPendingAuthorisation = new System.Windows.Forms.ToolStripMenuItem();
            this.rejectedPaymentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentSeachToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.maintenanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deliveryWindowsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.leadTimesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pricingSubGroupsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vatCodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vatRateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiTools = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiSnapShot = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiAudit = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiWindows = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiClose = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiCloseAll = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiCascade = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiTileVertical = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiTileHorizontal = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiArrangeAll = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiHelp = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiContents = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiIndex = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiSearch = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this._tsmiAbout = new System.Windows.Forms.ToolStripMenuItem();
            this._toolStrip = new System.Windows.Forms.ToolStrip();
            this._tsbRebateNew = new System.Windows.Forms.ToolStripButton();
            this._tsbRebateFind = new System.Windows.Forms.ToolStripButton();
            this._tsbRebateCopy = new System.Windows.Forms.ToolStripButton();
            this._tss1 = new System.Windows.Forms.ToolStripSeparator();
            this._tsbAccrualCalculate = new System.Windows.Forms.ToolStripButton();
            this._tsbPaymentSummary = new System.Windows.Forms.ToolStripButton();
            this._tsbPendingAuthorisation = new System.Windows.Forms.ToolStripButton();
            this._tss2 = new System.Windows.Forms.ToolStripSeparator();
            this._tsbSave = new System.Windows.Forms.ToolStripButton();
            this._tsbSaveAll = new System.Windows.Forms.ToolStripButton();
            this._tss3 = new System.Windows.Forms.ToolStripSeparator();
            this._tsbHelp = new System.Windows.Forms.ToolStripButton();
            this._toolTip = new System.Windows.Forms.ToolTip(this.components);
            this._menuStrip.SuspendLayout();
            this._toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // _menuStrip
            // 
            this._menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._tsmiRebate,
            this._tsmiAccrual,
            this._tsmiPayments,
            this.maintenanceToolStripMenuItem,
            this._tsmiTools,
            this._tsmiWindows,
            this._tsmiHelp});
            this._menuStrip.Location = new System.Drawing.Point(0, 0);
            this._menuStrip.MdiWindowListItem = this._tsmiWindows;
            this._menuStrip.Name = "_menuStrip";
            this._menuStrip.Size = new System.Drawing.Size(632, 24);
            this._menuStrip.TabIndex = 0;
            this._menuStrip.Text = "MenuStrip";
            // 
            // _tsmiRebate
            // 
            this._tsmiRebate.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._tsmiNewRebate,
            this._tsmiRebateFind,
            this._tsmiRebateCopy});
            this._tsmiRebate.Name = "_tsmiRebate";
            this._tsmiRebate.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this._tsmiRebate.Size = new System.Drawing.Size(55, 20);
            this._tsmiRebate.Text = "&Rebate";
            // 
            // _tsmiNewRebate
            // 
            this._tsmiNewRebate.Image = global::Evolve.Clients.Lafarge.Cement.Rebates.Properties.Resources.page_add;
            this._tsmiNewRebate.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this._tsmiNewRebate.Name = "_tsmiNewRebate";
            this._tsmiNewRebate.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this._tsmiNewRebate.Size = new System.Drawing.Size(141, 22);
            this._tsmiNewRebate.Text = "&New";
            // 
            // _tsmiRebateFind
            // 
            this._tsmiRebateFind.Image = global::Evolve.Clients.Lafarge.Cement.Rebates.Properties.Resources.find;
            this._tsmiRebateFind.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this._tsmiRebateFind.Name = "_tsmiRebateFind";
            this._tsmiRebateFind.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this._tsmiRebateFind.Size = new System.Drawing.Size(141, 22);
            this._tsmiRebateFind.Text = "&Find";
            // 
            // _tsmiRebateCopy
            // 
            this._tsmiRebateCopy.Image = global::Evolve.Clients.Lafarge.Cement.Rebates.Properties.Resources.page_copy;
            this._tsmiRebateCopy.Name = "_tsmiRebateCopy";
            this._tsmiRebateCopy.Size = new System.Drawing.Size(141, 22);
            this._tsmiRebateCopy.Text = "&Copy";
            // 
            // _tsmiAccrual
            // 
            this._tsmiAccrual.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._tsmiAccrualCalculate,
            this._tsmiForecast});
            this._tsmiAccrual.Name = "_tsmiAccrual";
            this._tsmiAccrual.Size = new System.Drawing.Size(59, 20);
            this._tsmiAccrual.Text = "&Accrual";
            // 
            // _tsmiAccrualCalculate
            // 
            this._tsmiAccrualCalculate.Image = global::Evolve.Clients.Lafarge.Cement.Rebates.Properties.Resources.calculator;
            this._tsmiAccrualCalculate.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this._tsmiAccrualCalculate.Name = "_tsmiAccrualCalculate";
            this._tsmiAccrualCalculate.Size = new System.Drawing.Size(165, 22);
            this._tsmiAccrualCalculate.Text = "&Calculate Actuals";
            // 
            // _tsmiForecast
            // 
            this._tsmiForecast.Image = global::Evolve.Clients.Lafarge.Cement.Rebates.Properties.Resources.chart_line_add;
            this._tsmiForecast.Name = "_tsmiForecast";
            this._tsmiForecast.Size = new System.Drawing.Size(165, 22);
            this._tsmiForecast.Text = "&Forecast";
            // 
            // _tsmiPayments
            // 
            this._tsmiPayments.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._tsmiPaymentsSummary,
            this._tsmiPendingAuthorisation,
            this.rejectedPaymentsToolStripMenuItem,
            this.paymentSeachToolStripMenuItem});
            this._tsmiPayments.Name = "_tsmiPayments";
            this._tsmiPayments.Size = new System.Drawing.Size(71, 20);
            this._tsmiPayments.Text = "&Payments";
            // 
            // _tsmiPaymentsSummary
            // 
            this._tsmiPaymentsSummary.Image = global::Evolve.Clients.Lafarge.Cement.Rebates.Properties.Resources.application_view_columns;
            this._tsmiPaymentsSummary.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._tsmiPaymentsSummary.Name = "_tsmiPaymentsSummary";
            this._tsmiPaymentsSummary.Size = new System.Drawing.Size(248, 22);
            this._tsmiPaymentsSummary.Text = "&Summary";
            // 
            // _tsmiPendingAuthorisation
            // 
            this._tsmiPendingAuthorisation.Image = global::Evolve.Clients.Lafarge.Cement.Rebates.Properties.Resources.status_away;
            this._tsmiPendingAuthorisation.Name = "_tsmiPendingAuthorisation";
            this._tsmiPendingAuthorisation.Size = new System.Drawing.Size(248, 22);
            this._tsmiPendingAuthorisation.Text = "&Payments Pending Authorisation";
            // 
            // rejectedPaymentsToolStripMenuItem
            // 
            this.rejectedPaymentsToolStripMenuItem.Image = global::Evolve.Clients.Lafarge.Cement.Rebates.Properties.Resources.status_busy;
            this.rejectedPaymentsToolStripMenuItem.Name = "rejectedPaymentsToolStripMenuItem";
            this.rejectedPaymentsToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.rejectedPaymentsToolStripMenuItem.Text = "&Rejected Payments";
            this.rejectedPaymentsToolStripMenuItem.Click += new System.EventHandler(this.rejectedPaymentsToolStripMenuItem_Click);
            // 
            // paymentSeachToolStripMenuItem
            // 
            this.paymentSeachToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("paymentSeachToolStripMenuItem.Image")));
            this.paymentSeachToolStripMenuItem.Name = "paymentSeachToolStripMenuItem";
            this.paymentSeachToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.paymentSeachToolStripMenuItem.Text = "&Find Payments";
            this.paymentSeachToolStripMenuItem.Click += new System.EventHandler(this.paymentSeachToolStripMenuItem_Click);
            // 
            // maintenanceToolStripMenuItem
            // 
            this.maintenanceToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deliveryWindowsToolStripMenuItem,
            this.leadTimesToolStripMenuItem,
            this.pricingSubGroupsToolStripMenuItem,
            this.vatCodeToolStripMenuItem,
            this.vatRateToolStripMenuItem});
            this.maintenanceToolStripMenuItem.Name = "maintenanceToolStripMenuItem";
            this.maintenanceToolStripMenuItem.Size = new System.Drawing.Size(88, 20);
            this.maintenanceToolStripMenuItem.Text = "&Maintenance";
            // 
            // deliveryWindowsToolStripMenuItem
            // 
            this.deliveryWindowsToolStripMenuItem.Name = "deliveryWindowsToolStripMenuItem";
            this.deliveryWindowsToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.deliveryWindowsToolStripMenuItem.Text = "&Delivery Windows";
            this.deliveryWindowsToolStripMenuItem.Click += new System.EventHandler(this.deliveryWindowsToolStripMenuItem_Click);
            // 
            // leadTimesToolStripMenuItem
            // 
            this.leadTimesToolStripMenuItem.Name = "leadTimesToolStripMenuItem";
            this.leadTimesToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.leadTimesToolStripMenuItem.Text = "&Lead Times";
            this.leadTimesToolStripMenuItem.Click += new System.EventHandler(this.leadTimesToolStripMenuItem_Click);
            // 
            // pricingSubGroupsToolStripMenuItem
            // 
            this.pricingSubGroupsToolStripMenuItem.Name = "pricingSubGroupsToolStripMenuItem";
            this.pricingSubGroupsToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.pricingSubGroupsToolStripMenuItem.Text = "&Pricing Sub-Groups";
            this.pricingSubGroupsToolStripMenuItem.Click += new System.EventHandler(this.pricingSubGroupsToolStripMenuItem_Click);
            // 
            // vatCodeToolStripMenuItem
            // 
            this.vatCodeToolStripMenuItem.Name = "vatCodeToolStripMenuItem";
            this.vatCodeToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.vatCodeToolStripMenuItem.Text = "VAT &Code";
            this.vatCodeToolStripMenuItem.Click += new System.EventHandler(this.vatCodeToolStripMenuItem_Click);
            // 
            // vatRateToolStripMenuItem
            // 
            this.vatRateToolStripMenuItem.Name = "vatRateToolStripMenuItem";
            this.vatRateToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.vatRateToolStripMenuItem.Text = "&VAT Rate";
            this.vatRateToolStripMenuItem.Click += new System.EventHandler(this.vatRateToolStripMenuItem_Click);
            // 
            // _tsmiTools
            // 
            this._tsmiTools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._tsmiSnapShot,
            this._tsmiAudit});
            this._tsmiTools.Name = "_tsmiTools";
            this._tsmiTools.Size = new System.Drawing.Size(47, 20);
            this._tsmiTools.Text = "&Tools";
            // 
            // _tsmiSnapShot
            // 
            this._tsmiSnapShot.Image = global::Evolve.Clients.Lafarge.Cement.Rebates.Properties.Resources.camera;
            this._tsmiSnapShot.Name = "_tsmiSnapShot";
            this._tsmiSnapShot.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.End)));
            this._tsmiSnapShot.Size = new System.Drawing.Size(259, 22);
            this._tsmiSnapShot.Text = "Send &Snap Shot To Evolve";
            // 
            // _tsmiAudit
            // 
            this._tsmiAudit.Image = global::Evolve.Clients.Lafarge.Cement.Rebates.Properties.Resources.key;
            this._tsmiAudit.Name = "_tsmiAudit";
            this._tsmiAudit.Size = new System.Drawing.Size(259, 22);
            this._tsmiAudit.Text = "&Audit";
            // 
            // _tsmiWindows
            // 
            this._tsmiWindows.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._tsmiClose,
            this._tsmiCloseAll,
            this._tsmiCascade,
            this._tsmiTileVertical,
            this._tsmiTileHorizontal,
            this._tsmiArrangeAll});
            this._tsmiWindows.Name = "_tsmiWindows";
            this._tsmiWindows.Size = new System.Drawing.Size(68, 20);
            this._tsmiWindows.Text = "&Windows";
            // 
            // _tsmiClose
            // 
            this._tsmiClose.Name = "_tsmiClose";
            this._tsmiClose.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F4)));
            this._tsmiClose.Size = new System.Drawing.Size(198, 22);
            this._tsmiClose.Text = "Close";
            // 
            // _tsmiCloseAll
            // 
            this._tsmiCloseAll.Name = "_tsmiCloseAll";
            this._tsmiCloseAll.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
            | System.Windows.Forms.Keys.F4)));
            this._tsmiCloseAll.Size = new System.Drawing.Size(198, 22);
            this._tsmiCloseAll.Text = "C&lose All";
            // 
            // _tsmiCascade
            // 
            this._tsmiCascade.Name = "_tsmiCascade";
            this._tsmiCascade.Size = new System.Drawing.Size(198, 22);
            this._tsmiCascade.Text = "&Cascade";
            // 
            // _tsmiTileVertical
            // 
            this._tsmiTileVertical.Name = "_tsmiTileVertical";
            this._tsmiTileVertical.Size = new System.Drawing.Size(198, 22);
            this._tsmiTileVertical.Text = "Tile &Vertical";
            // 
            // _tsmiTileHorizontal
            // 
            this._tsmiTileHorizontal.Name = "_tsmiTileHorizontal";
            this._tsmiTileHorizontal.Size = new System.Drawing.Size(198, 22);
            this._tsmiTileHorizontal.Text = "Tile &Horizontal";
            // 
            // _tsmiArrangeAll
            // 
            this._tsmiArrangeAll.Name = "_tsmiArrangeAll";
            this._tsmiArrangeAll.Size = new System.Drawing.Size(198, 22);
            this._tsmiArrangeAll.Text = "&Arrange Icons";
            // 
            // _tsmiHelp
            // 
            this._tsmiHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._tsmiContents,
            this._tsmiIndex,
            this._tsmiSearch,
            this.toolStripSeparator8,
            this._tsmiAbout});
            this._tsmiHelp.Name = "_tsmiHelp";
            this._tsmiHelp.Size = new System.Drawing.Size(44, 20);
            this._tsmiHelp.Text = "&Help";
            this._tsmiHelp.Visible = false;
            // 
            // _tsmiContents
            // 
            this._tsmiContents.Name = "_tsmiContents";
            this._tsmiContents.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F1)));
            this._tsmiContents.Size = new System.Drawing.Size(168, 22);
            this._tsmiContents.Text = "&Contents";
            // 
            // _tsmiIndex
            // 
            this._tsmiIndex.Image = ((System.Drawing.Image)(resources.GetObject("_tsmiIndex.Image")));
            this._tsmiIndex.ImageTransparentColor = System.Drawing.Color.Black;
            this._tsmiIndex.Name = "_tsmiIndex";
            this._tsmiIndex.Size = new System.Drawing.Size(168, 22);
            this._tsmiIndex.Text = "&Index";
            // 
            // _tsmiSearch
            // 
            this._tsmiSearch.Image = ((System.Drawing.Image)(resources.GetObject("_tsmiSearch.Image")));
            this._tsmiSearch.ImageTransparentColor = System.Drawing.Color.Black;
            this._tsmiSearch.Name = "_tsmiSearch";
            this._tsmiSearch.Size = new System.Drawing.Size(168, 22);
            this._tsmiSearch.Text = "&Search";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(165, 6);
            // 
            // _tsmiAbout
            // 
            this._tsmiAbout.Name = "_tsmiAbout";
            this._tsmiAbout.Size = new System.Drawing.Size(168, 22);
            this._tsmiAbout.Text = "&About ... ...";
            // 
            // _toolStrip
            // 
            this._toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._tsbRebateNew,
            this._tsbRebateFind,
            this._tsbRebateCopy,
            this._tss1,
            this._tsbAccrualCalculate,
            this._tsbPaymentSummary,
            this._tsbPendingAuthorisation,
            this._tss2,
            this._tsbSave,
            this._tsbSaveAll,
            this._tss3,
            this._tsbHelp});
            this._toolStrip.Location = new System.Drawing.Point(0, 24);
            this._toolStrip.Name = "_toolStrip";
            this._toolStrip.Size = new System.Drawing.Size(632, 25);
            this._toolStrip.TabIndex = 1;
            this._toolStrip.Text = "ToolStrip";
            // 
            // _tsbRebateNew
            // 
            this._tsbRebateNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsbRebateNew.Image = global::Evolve.Clients.Lafarge.Cement.Rebates.Properties.Resources.page_add;
            this._tsbRebateNew.ImageTransparentColor = System.Drawing.Color.Black;
            this._tsbRebateNew.Name = "_tsbRebateNew";
            this._tsbRebateNew.Size = new System.Drawing.Size(23, 22);
            this._tsbRebateNew.Text = "New Rebate";
            // 
            // _tsbRebateFind
            // 
            this._tsbRebateFind.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsbRebateFind.Image = ((System.Drawing.Image)(resources.GetObject("_tsbRebateFind.Image")));
            this._tsbRebateFind.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._tsbRebateFind.Name = "_tsbRebateFind";
            this._tsbRebateFind.Size = new System.Drawing.Size(23, 22);
            this._tsbRebateFind.Text = "Find Rebate";
            // 
            // _tsbRebateCopy
            // 
            this._tsbRebateCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsbRebateCopy.Image = ((System.Drawing.Image)(resources.GetObject("_tsbRebateCopy.Image")));
            this._tsbRebateCopy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._tsbRebateCopy.Name = "_tsbRebateCopy";
            this._tsbRebateCopy.Size = new System.Drawing.Size(23, 22);
            this._tsbRebateCopy.Text = "Copy Rebate";
            // 
            // _tss1
            // 
            this._tss1.Name = "_tss1";
            this._tss1.Size = new System.Drawing.Size(6, 25);
            // 
            // _tsbAccrualCalculate
            // 
            this._tsbAccrualCalculate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsbAccrualCalculate.Image = global::Evolve.Clients.Lafarge.Cement.Rebates.Properties.Resources.calculator;
            this._tsbAccrualCalculate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._tsbAccrualCalculate.Name = "_tsbAccrualCalculate";
            this._tsbAccrualCalculate.Size = new System.Drawing.Size(23, 22);
            this._tsbAccrualCalculate.Text = "Accrual Calculate";
            // 
            // _tsbPaymentSummary
            // 
            this._tsbPaymentSummary.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsbPaymentSummary.Image = ((System.Drawing.Image)(resources.GetObject("_tsbPaymentSummary.Image")));
            this._tsbPaymentSummary.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._tsbPaymentSummary.Name = "_tsbPaymentSummary";
            this._tsbPaymentSummary.Size = new System.Drawing.Size(23, 22);
            this._tsbPaymentSummary.Text = "Payment Summary";
            // 
            // _tsbPendingAuthorisation
            // 
            this._tsbPendingAuthorisation.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsbPendingAuthorisation.Image = global::Evolve.Clients.Lafarge.Cement.Rebates.Properties.Resources.status_away;
            this._tsbPendingAuthorisation.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._tsbPendingAuthorisation.Name = "_tsbPendingAuthorisation";
            this._tsbPendingAuthorisation.Size = new System.Drawing.Size(23, 22);
            this._tsbPendingAuthorisation.Text = "Payments Pending Authorisation";
            // 
            // _tss2
            // 
            this._tss2.Name = "_tss2";
            this._tss2.Size = new System.Drawing.Size(6, 25);
            // 
            // _tsbSave
            // 
            this._tsbSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsbSave.Image = ((System.Drawing.Image)(resources.GetObject("_tsbSave.Image")));
            this._tsbSave.ImageTransparentColor = System.Drawing.Color.Black;
            this._tsbSave.Name = "_tsbSave";
            this._tsbSave.Size = new System.Drawing.Size(23, 22);
            this._tsbSave.Text = "Save";
            // 
            // _tsbSaveAll
            // 
            this._tsbSaveAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsbSaveAll.Image = ((System.Drawing.Image)(resources.GetObject("_tsbSaveAll.Image")));
            this._tsbSaveAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._tsbSaveAll.Name = "_tsbSaveAll";
            this._tsbSaveAll.Size = new System.Drawing.Size(23, 22);
            this._tsbSaveAll.Text = "Save All";
            // 
            // _tss3
            // 
            this._tss3.Name = "_tss3";
            this._tss3.Size = new System.Drawing.Size(6, 25);
            // 
            // _tsbHelp
            // 
            this._tsbHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsbHelp.Image = global::Evolve.Clients.Lafarge.Cement.Rebates.Properties.Resources.help;
            this._tsbHelp.ImageTransparentColor = System.Drawing.Color.Black;
            this._tsbHelp.Name = "_tsbHelp";
            this._tsbHelp.Size = new System.Drawing.Size(23, 22);
            this._tsbHelp.Text = "Help";

            // 
            // applicationStatus
            // CRN0005863 show missing application status strip
            // 
            this._applicationStatus = new Evolve.Libraries.WinformsApplication.ApplicationStatus();
            this._applicationStatus.ApplicationUser = "";
            this._applicationStatus.Environment = "";
            this._applicationStatus.Location = new System.Drawing.Point(0, 301);
            this._applicationStatus.Name = "applicationStatus";
            this._applicationStatus.Size = new System.Drawing.Size(521, 22);
            this._applicationStatus.SystemName = "";
            this._applicationStatus.TabIndex = 0;
            this._applicationStatus.Text = "applicationStatus1";

            // 
            // frmMDIParent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 453);
            this.Controls.Add(this._toolStrip);
            this.Controls.Add(this._menuStrip);
            this.Controls.Add(this._applicationStatus);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this._menuStrip;
            this.Name = "frmMDIParent";
            this.Text = "Lafarge Cement Rebates System";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this._menuStrip.ResumeLayout(false);
            this._menuStrip.PerformLayout();
            this._toolStrip.ResumeLayout(false);
            this._toolStrip.PerformLayout();
            this._applicationStatus.ResumeLayout(false);
            this._applicationStatus.PerformLayout();

            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion


        private System.Windows.Forms.MenuStrip _menuStrip;
        private System.Windows.Forms.ToolStrip _toolStrip;
        private System.Windows.Forms.ToolStripSeparator _tss1;
        private System.Windows.Forms.ToolStripSeparator _tss2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem _tsmiAbout;
        private System.Windows.Forms.ToolStripMenuItem _tsmiTileHorizontal;
        private System.Windows.Forms.ToolStripMenuItem _tsmiWindows;
        private System.Windows.Forms.ToolStripMenuItem _tsmiCascade;
        private System.Windows.Forms.ToolStripMenuItem _tsmiTileVertical;
        private System.Windows.Forms.ToolStripMenuItem _tsmiCloseAll;
        private System.Windows.Forms.ToolStripMenuItem _tsmiArrangeAll;
        private System.Windows.Forms.ToolStripMenuItem _tsmiHelp;
        private System.Windows.Forms.ToolStripMenuItem _tsmiContents;
        private System.Windows.Forms.ToolStripMenuItem _tsmiIndex;
        private System.Windows.Forms.ToolStripMenuItem _tsmiSearch;
        private System.Windows.Forms.ToolStripButton _tsbRebateNew;
        private System.Windows.Forms.ToolStripButton _tsbSave;
        private System.Windows.Forms.ToolStripButton _tsbHelp;
        private System.Windows.Forms.ToolTip _toolTip;
        private System.Windows.Forms.ToolStripMenuItem _tsmiRebate;
        private System.Windows.Forms.ToolStripMenuItem _tsmiNewRebate;
        private System.Windows.Forms.ToolStripButton _tsbRebateCopy;
        private System.Windows.Forms.ToolStripButton _tsbRebateFind;
        private System.Windows.Forms.ToolStripMenuItem _tsmiRebateFind;
        private System.Windows.Forms.ToolStripMenuItem _tsmiAccrual;
        private System.Windows.Forms.ToolStripMenuItem _tsmiAccrualCalculate;
        private System.Windows.Forms.ToolStripMenuItem _tsmiPayments;
        private System.Windows.Forms.ToolStripMenuItem _tsmiPaymentsSummary;
        private System.Windows.Forms.ToolStripButton _tsbAccrualCalculate;
        private System.Windows.Forms.ToolStripButton _tsbPaymentSummary;
        private System.Windows.Forms.ToolStripButton _tsbSaveAll;
        private System.Windows.Forms.ToolStripSeparator _tss3;
        private System.Windows.Forms.ToolStripMenuItem _tsmiRebateCopy;
        private System.Windows.Forms.ToolStripMenuItem _tsmiClose;
        private Evolve.Libraries.WinformsApplication.ApplicationStatus _applicationStatus;
        private System.Windows.Forms.ToolStripMenuItem _tsmiTools;
        private System.Windows.Forms.ToolStripMenuItem _tsmiSnapShot;
        private System.Windows.Forms.ToolStripMenuItem _tsmiPendingAuthorisation;
        private System.Windows.Forms.ToolStripButton _tsbPendingAuthorisation;
        private System.Windows.Forms.ToolStripMenuItem maintenanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deliveryWindowsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem leadTimesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rejectedPaymentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pricingSubGroupsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vatRateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vatCodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _tsmiForecast;
        private System.Windows.Forms.ToolStripMenuItem paymentSeachToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _tsmiAudit;
    }
}




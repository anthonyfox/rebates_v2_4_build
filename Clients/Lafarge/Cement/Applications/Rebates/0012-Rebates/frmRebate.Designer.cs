﻿namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	partial class frmRebate
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRebate));
			this._errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
			this.panel1 = new System.Windows.Forms.Panel();
			this._lblNotes = new System.Windows.Forms.Label();
			this._lblNotesLabel = new System.Windows.Forms.Label();
			this._btnClose = new System.Windows.Forms.Button();
			this._okCancel = new Evolve.Libraries.Controls.Generic.OkCancel();
			this._tabControl = new System.Windows.Forms.TabControl();
			this.Header = new System.Windows.Forms.TabPage();
			this._ucCustomer = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Customer();
			this._ucPayee = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Payee();
			this._txtRate = new System.Windows.Forms.TextBox();
			this._lblRate = new System.Windows.Forms.Label();
			this._chkIsAdjustment = new System.Windows.Forms.CheckBox();
			this._chkIsJIF = new System.Windows.Forms.CheckBox();
			this._ucPricingSubGroup = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Lookups.PricingSubGroup();
			this._dtpEndDate = new Evolve.Libraries.Controls.Generic.DateTimePickerNull();
			this._txtVersion = new System.Windows.Forms.TextBox();
			this._ucPaymentFrequency = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.PaymentFrequency();
			this._grpVolumeRange = new System.Windows.Forms.GroupBox();
			this._lblErrorRebateRange = new System.Windows.Forms.Label();
			this._dgvRange = new System.Windows.Forms.DataGridView();
			this._txtFixedValue = new System.Windows.Forms.TextBox();
			this._lblFixedValue = new System.Windows.Forms.Label();
			this._txtEstimatedVolume = new System.Windows.Forms.TextBox();
			this._lblEstimatedVolume = new System.Windows.Forms.Label();
			this._ucRebateType = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.RebateType();
			this._txtGlobalRate = new System.Windows.Forms.TextBox();
			this._lblGlobalRate = new System.Windows.Forms.Label();
			this._lblVersion = new System.Windows.Forms.Label();
			this._chkIsGlobalProduct = new System.Windows.Forms.CheckBox();
			this._ucCustomerGroup = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.CustomerGroup();
			this._txtContractText = new System.Windows.Forms.TextBox();
			this._chkIsVAP = new System.Windows.Forms.CheckBox();
			this._chkIsContract = new System.Windows.Forms.CheckBox();
			this._lblTo = new System.Windows.Forms.Label();
			this._dtpStartDate = new Evolve.Libraries.Controls.Generic.DateTimePickerNull();
			this._lblRebateDate = new System.Windows.Forms.Label();
			this._ucSoldFrom = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Site();
			this._ucHierarchy = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Hierarchy();
			this._ucDeliveryType = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.DeliveryType();
			this._txtDescription = new System.Windows.Forms.TextBox();
			this._txtRebateNumber = new System.Windows.Forms.TextBox();
			this._lblDescription = new System.Windows.Forms.Label();
			this._lblRebateId = new System.Windows.Forms.Label();
			this._lblUcCustomer = new System.Windows.Forms.Label();
			this.Detail = new System.Windows.Forms.TabPage();
			this._grpDetailProduct = new System.Windows.Forms.GroupBox();
			this._lblErrorProductDetail = new System.Windows.Forms.Label();
			this._dgvDetail = new System.Windows.Forms.DataGridView();
			this._grpDetailSelection = new System.Windows.Forms.GroupBox();
			this._btnProductDetailClear = new System.Windows.Forms.Button();
			this._ucProductDetail = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Product();
			this._lblAttribute4 = new System.Windows.Forms.Label();
			this._lblAttribute3 = new System.Windows.Forms.Label();
			this._lblAttribute2 = new System.Windows.Forms.Label();
			this._lblAttribute1 = new System.Windows.Forms.Label();
			this._ucAttribute4 = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Attribute();
			this._ucAttribute3 = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Attribute();
			this._ucAttribute2 = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Attribute();
			this._ucAttribute1 = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Attribute();
			this._btnAddProductDetail = new System.Windows.Forms.Button();
			this.VAP = new System.Windows.Forms.TabPage();
			this._grpVAPProductSelect = new System.Windows.Forms.GroupBox();
			this._ucProductVAP = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Product();
			this._btnVAPProductAdd = new System.Windows.Forms.Button();
			this._grpVAPProducts = new System.Windows.Forms.GroupBox();
			this._lblErrorVAP = new System.Windows.Forms.Label();
			this._dgvVAP = new System.Windows.Forms.DataGridView();
			this._grpVAPActivation = new System.Windows.Forms.GroupBox();
			this._ucVAPActivationMethod = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.VAPActivationMethod();
			this._txtVAPActivationQuantity = new System.Windows.Forms.TextBox();
			this._lblVAPActivationQuantity = new System.Windows.Forms.Label();
			this.RebateExtension = new System.Windows.Forms.TabPage();
			this._grpDeliveryWindows = new System.Windows.Forms.GroupBox();
			this._dgvRebateExtension = new System.Windows.Forms.DataGridView();
			this._lblErrorDeliveryWindow = new System.Windows.Forms.Label();
			this._grpDeliveryWindowSelect = new System.Windows.Forms.GroupBox();
			this._ucLeadTime = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.LeadTime();
			this._ucPackingType = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.PackingType();
			this._ucDeliveryWindow = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.DeliveryWindow();
			this._btnAddRebateExtension = new System.Windows.Forms.Button();
			this.SoldFrom = new System.Windows.Forms.TabPage();
			this._btnSoldFromInclude = new System.Windows.Forms.Button();
			this._btnSoldFromExclude = new System.Windows.Forms.Button();
			this._lblSoldFromExclude = new System.Windows.Forms.Label();
			this._lblSoldFromInclude = new System.Windows.Forms.Label();
			this._lstSoldFromExclude = new System.Windows.Forms.ListBox();
			this._lstSoldFromInclude = new System.Windows.Forms.ListBox();
			this.SoldTo = new System.Windows.Forms.TabPage();
			this._grpSoldToExcluded = new System.Windows.Forms.GroupBox();
			this._dgvSoldTo = new System.Windows.Forms.DataGridView();
			this._grpSoldToSelection = new System.Windows.Forms.GroupBox();
			this._btnSoldToAdd = new System.Windows.Forms.Button();
			this._ucSoldToCustomer = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Customer();
			this.ShipTo = new System.Windows.Forms.TabPage();
			this._grpShipToExcluded = new System.Windows.Forms.GroupBox();
			this._dgvShipTo = new System.Windows.Forms.DataGridView();
			this._grpShipToSelection = new System.Windows.Forms.GroupBox();
			this._btnShipToAdd = new System.Windows.Forms.Button();
			this._ucShipToCustomer = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Customer();
			this.Notes = new System.Windows.Forms.TabPage();
			this._grpNotes = new System.Windows.Forms.GroupBox();
			this._dgvNotes = new System.Windows.Forms.DataGridView();
			this._grpNoteAdd = new System.Windows.Forms.GroupBox();
			this._lblNoteError = new System.Windows.Forms.Label();
			this._btnNoteAdd = new System.Windows.Forms.Button();
			this._txtNote = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)(this._errorProvider)).BeginInit();
			this.panel1.SuspendLayout();
			this._tabControl.SuspendLayout();
			this.Header.SuspendLayout();
			this._grpVolumeRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this._dgvRange)).BeginInit();
			this.Detail.SuspendLayout();
			this._grpDetailProduct.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this._dgvDetail)).BeginInit();
			this._grpDetailSelection.SuspendLayout();
			this.VAP.SuspendLayout();
			this._grpVAPProductSelect.SuspendLayout();
			this._grpVAPProducts.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this._dgvVAP)).BeginInit();
			this._grpVAPActivation.SuspendLayout();
			this.RebateExtension.SuspendLayout();
			this._grpDeliveryWindows.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this._dgvRebateExtension)).BeginInit();
			this._grpDeliveryWindowSelect.SuspendLayout();
			this.SoldFrom.SuspendLayout();
			this.SoldTo.SuspendLayout();
			this._grpSoldToExcluded.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this._dgvSoldTo)).BeginInit();
			this._grpSoldToSelection.SuspendLayout();
			this.ShipTo.SuspendLayout();
			this._grpShipToExcluded.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this._dgvShipTo)).BeginInit();
			this._grpShipToSelection.SuspendLayout();
			this.Notes.SuspendLayout();
			this._grpNotes.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this._dgvNotes)).BeginInit();
			this._grpNoteAdd.SuspendLayout();
			this.SuspendLayout();
			// 
			// _errorProvider
			// 
			this._errorProvider.ContainerControl = this;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this._lblNotes);
			this.panel1.Controls.Add(this._lblNotesLabel);
			this.panel1.Controls.Add(this._btnClose);
			this.panel1.Controls.Add(this._okCancel);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 466);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(895, 40);
			this.panel1.TabIndex = 3;
			// 
			// _lblNotes
			// 
			this._lblNotes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._lblNotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this._lblNotes.Location = new System.Drawing.Point(89, 11);
			this._lblNotes.Name = "_lblNotes";
			this._lblNotes.Size = new System.Drawing.Size(622, 13);
			this._lblNotes.TabIndex = 3;
			this._lblNotes.Text = "000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
    "000000000000000000000";
			// 
			// _lblNotesLabel
			// 
			this._lblNotesLabel.AutoSize = true;
			this._lblNotesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this._lblNotesLabel.Location = new System.Drawing.Point(3, 11);
			this._lblNotesLabel.Name = "_lblNotesLabel";
			this._lblNotesLabel.Size = new System.Drawing.Size(89, 13);
			this._lblNotesLabel.TabIndex = 2;
			this._lblNotesLabel.Text = "Recent Notes:";
			// 
			// _btnClose
			// 
			this._btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._btnClose.Location = new System.Drawing.Point(806, 6);
			this._btnClose.Name = "_btnClose";
			this._btnClose.Size = new System.Drawing.Size(75, 23);
			this._btnClose.TabIndex = 1;
			this._btnClose.Text = "Close";
			this._btnClose.UseVisualStyleBackColor = true;
			// 
			// _okCancel
			// 
			this._okCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._okCancel.CausesValidation = false;
			this._okCancel.Location = new System.Drawing.Point(726, 6);
			this._okCancel.Name = "_okCancel";
			this._okCancel.Size = new System.Drawing.Size(157, 24);
			this._okCancel.TabIndex = 0;
			// 
			// _tabControl
			// 
			this._tabControl.Controls.Add(this.Header);
			this._tabControl.Controls.Add(this.Detail);
			this._tabControl.Controls.Add(this.VAP);
			this._tabControl.Controls.Add(this.RebateExtension);
			this._tabControl.Controls.Add(this.SoldFrom);
			this._tabControl.Controls.Add(this.SoldTo);
			this._tabControl.Controls.Add(this.ShipTo);
			this._tabControl.Controls.Add(this.Notes);
			this._tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this._tabControl.Location = new System.Drawing.Point(0, 0);
			this._tabControl.Name = "_tabControl";
			this._tabControl.SelectedIndex = 0;
			this._tabControl.Size = new System.Drawing.Size(895, 466);
			this._tabControl.TabIndex = 4;
			// 
			// Header
			// 
			this.Header.AutoScroll = true;
			this.Header.Controls.Add(this._ucCustomer);
			this.Header.Controls.Add(this._ucPayee);
			this.Header.Controls.Add(this._txtRate);
			this.Header.Controls.Add(this._lblRate);
			this.Header.Controls.Add(this._chkIsAdjustment);
			this.Header.Controls.Add(this._chkIsJIF);
			this.Header.Controls.Add(this._ucPricingSubGroup);
			this.Header.Controls.Add(this._dtpEndDate);
			this.Header.Controls.Add(this._txtVersion);
			this.Header.Controls.Add(this._ucPaymentFrequency);
			this.Header.Controls.Add(this._grpVolumeRange);
			this.Header.Controls.Add(this._txtFixedValue);
			this.Header.Controls.Add(this._lblFixedValue);
			this.Header.Controls.Add(this._txtEstimatedVolume);
			this.Header.Controls.Add(this._lblEstimatedVolume);
			this.Header.Controls.Add(this._ucRebateType);
			this.Header.Controls.Add(this._txtGlobalRate);
			this.Header.Controls.Add(this._lblGlobalRate);
			this.Header.Controls.Add(this._lblVersion);
			this.Header.Controls.Add(this._chkIsGlobalProduct);
			this.Header.Controls.Add(this._ucCustomerGroup);
			this.Header.Controls.Add(this._txtContractText);
			this.Header.Controls.Add(this._chkIsVAP);
			this.Header.Controls.Add(this._chkIsContract);
			this.Header.Controls.Add(this._lblTo);
			this.Header.Controls.Add(this._dtpStartDate);
			this.Header.Controls.Add(this._lblRebateDate);
			this.Header.Controls.Add(this._ucSoldFrom);
			this.Header.Controls.Add(this._ucHierarchy);
			this.Header.Controls.Add(this._ucDeliveryType);
			this.Header.Controls.Add(this._txtDescription);
			this.Header.Controls.Add(this._txtRebateNumber);
			this.Header.Controls.Add(this._lblDescription);
			this.Header.Controls.Add(this._lblRebateId);
			this.Header.Controls.Add(this._lblUcCustomer);
			this.Header.Location = new System.Drawing.Point(4, 22);
			this.Header.Name = "Header";
			this.Header.Size = new System.Drawing.Size(887, 440);
			this.Header.TabIndex = 0;
			this.Header.Text = " Header";
			this.Header.UseVisualStyleBackColor = true;
			// 
			// _ucCustomer
			// 
			this._ucCustomer.AccountNumber = null;
			this._ucCustomer.CustomerGroupId = null;
			this._ucCustomer.CustomerSearchModeEnum = Evolve.Clients.Lafarge.Cement.Business.Rebates.CustomerSearchModeEnum.SoldTo;
			this._ucCustomer.DivisionId = 0;
			this._ucCustomer.LabelText = "";
			this._ucCustomer.Location = new System.Drawing.Point(123, 85);
			this._ucCustomer.MultiSelect = false;
			this._ucCustomer.Name = "_ucCustomer";
			this._ucCustomer.ParentCustomerId = null;
			this._ucCustomer.Size = new System.Drawing.Size(754, 21);
			this._ucCustomer.TabIndex = 9;
			// 
			// _ucPayee
			// 
			this._ucPayee.AccountNumber = null;
			this._ucPayee.DivisionId = 0;
			this._ucPayee.LabelText = "Payee";
			this._ucPayee.Location = new System.Drawing.Point(89, 107);
			this._ucPayee.MultiSelect = false;
			this._ucPayee.Name = "_ucPayee";
			this._ucPayee.Size = new System.Drawing.Size(585, 20);
			this._ucPayee.TabIndex = 11;
			// 
			// _txtRate
			// 
			this._txtRate.Location = new System.Drawing.Point(258, 409);
			this._txtRate.Name = "_txtRate";
			this._txtRate.Size = new System.Drawing.Size(75, 20);
			this._txtRate.TabIndex = 36;
			this._txtRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _lblRate
			// 
			this._lblRate.AutoSize = true;
			this._lblRate.Location = new System.Drawing.Point(222, 412);
			this._lblRate.Name = "_lblRate";
			this._lblRate.Size = new System.Drawing.Size(30, 13);
			this._lblRate.TabIndex = 35;
			this._lblRate.Text = "Rate";
			// 
			// _chkIsAdjustment
			// 
			this._chkIsAdjustment.AutoSize = true;
			this._chkIsAdjustment.Location = new System.Drawing.Point(55, 255);
			this._chkIsAdjustment.Name = "_chkIsAdjustment";
			this._chkIsAdjustment.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this._chkIsAdjustment.Size = new System.Drawing.Size(89, 17);
			this._chkIsAdjustment.TabIndex = 34;
			this._chkIsAdjustment.Text = "Is Adjustment";
			this._chkIsAdjustment.UseVisualStyleBackColor = true;
			// 
			// _chkIsJIF
			// 
			this._chkIsJIF.AutoSize = true;
			this._chkIsJIF.Location = new System.Drawing.Point(195, 233);
			this._chkIsJIF.Name = "_chkIsJIF";
			this._chkIsJIF.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this._chkIsJIF.Size = new System.Drawing.Size(51, 17);
			this._chkIsJIF.TabIndex = 33;
			this._chkIsJIF.Text = "Is JIF";
			this._chkIsJIF.UseVisualStyleBackColor = true;
			// 
			// _ucPricingSubGroup
			// 
			this._ucPricingSubGroup.DivisionId = 0;
			this._ucPricingSubGroup.IncludeAllOption = false;
			this._ucPricingSubGroup.Location = new System.Drawing.Point(518, 85);
			this._ucPricingSubGroup.Name = "_ucPricingSubGroup";
			this._ucPricingSubGroup.Size = new System.Drawing.Size(240, 21);
			this._ucPricingSubGroup.TabIndex = 10;
			// 
			// _dtpEndDate
			// 
			this._dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this._dtpEndDate.Location = new System.Drawing.Point(252, 177);
			this._dtpEndDate.Name = "_dtpEndDate";
			this._dtpEndDate.Size = new System.Drawing.Size(95, 20);
			this._dtpEndDate.TabIndex = 17;
			this._dtpEndDate.Value = new System.DateTime(2011, 2, 10, 16, 6, 44, 912);
			this._dtpEndDate.ValueNotNull = new System.DateTime(2011, 2, 10, 16, 6, 44, 912);
			// 
			// _txtVersion
			// 
			this._txtVersion.Enabled = false;
			this._txtVersion.Location = new System.Drawing.Point(252, 17);
			this._txtVersion.Name = "_txtVersion";
			this._txtVersion.ReadOnly = true;
			this._txtVersion.Size = new System.Drawing.Size(50, 20);
			this._txtVersion.TabIndex = 3;
			// 
			// _ucPaymentFrequency
			// 
			this._ucPaymentFrequency.AllItemText = "All";
			this._ucPaymentFrequency.DivisionId = 0;
			this._ucPaymentFrequency.IncludeAllOption = false;
			this._ucPaymentFrequency.Location = new System.Drawing.Point(19, 353);
			this._ucPaymentFrequency.Name = "_ucPaymentFrequency";
			this._ucPaymentFrequency.Size = new System.Drawing.Size(314, 23);
			this._ucPaymentFrequency.TabIndex = 25;
			// 
			// _grpVolumeRange
			// 
			this._grpVolumeRange.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this._grpVolumeRange.Controls.Add(this._lblErrorRebateRange);
			this._grpVolumeRange.Controls.Add(this._dgvRange);
			this._grpVolumeRange.Location = new System.Drawing.Point(540, 267);
			this._grpVolumeRange.Name = "_grpVolumeRange";
			this._grpVolumeRange.Size = new System.Drawing.Size(337, 158);
			this._grpVolumeRange.TabIndex = 0;
			this._grpVolumeRange.TabStop = false;
			this._grpVolumeRange.Text = "Tranche Rebate Volume Ranges";
			// 
			// _lblErrorRebateRange
			// 
			this._lblErrorRebateRange.AutoSize = true;
			this._lblErrorRebateRange.Location = new System.Drawing.Point(168, 0);
			this._lblErrorRebateRange.Name = "_lblErrorRebateRange";
			this._lblErrorRebateRange.Size = new System.Drawing.Size(0, 13);
			this._lblErrorRebateRange.TabIndex = 1;
			// 
			// _dgvRange
			// 
			this._dgvRange.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._dgvRange.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this._dgvRange.Dock = System.Windows.Forms.DockStyle.Fill;
			this._dgvRange.Location = new System.Drawing.Point(3, 16);
			this._dgvRange.Name = "_dgvRange";
			this._dgvRange.Size = new System.Drawing.Size(331, 139);
			this._dgvRange.TabIndex = 0;
			// 
			// _txtFixedValue
			// 
			this._txtFixedValue.Location = new System.Drawing.Point(132, 409);
			this._txtFixedValue.Name = "_txtFixedValue";
			this._txtFixedValue.Size = new System.Drawing.Size(75, 20);
			this._txtFixedValue.TabIndex = 29;
			this._txtFixedValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _lblFixedValue
			// 
			this._lblFixedValue.AutoSize = true;
			this._lblFixedValue.Location = new System.Drawing.Point(54, 412);
			this._lblFixedValue.Name = "_lblFixedValue";
			this._lblFixedValue.Size = new System.Drawing.Size(72, 13);
			this._lblFixedValue.TabIndex = 28;
			this._lblFixedValue.Text = "Rebate Value";
			// 
			// _txtEstimatedVolume
			// 
			this._txtEstimatedVolume.Location = new System.Drawing.Point(132, 387);
			this._txtEstimatedVolume.Name = "_txtEstimatedVolume";
			this._txtEstimatedVolume.Size = new System.Drawing.Size(75, 20);
			this._txtEstimatedVolume.TabIndex = 27;
			this._txtEstimatedVolume.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _lblEstimatedVolume
			// 
			this._lblEstimatedVolume.AutoSize = true;
			this._lblEstimatedVolume.Location = new System.Drawing.Point(6, 390);
			this._lblEstimatedVolume.Name = "_lblEstimatedVolume";
			this._lblEstimatedVolume.Size = new System.Drawing.Size(120, 13);
			this._lblEstimatedVolume.TabIndex = 26;
			this._lblEstimatedVolume.Text = "Estimated Sales Volume";
			// 
			// _ucRebateType
			// 
			this._ucRebateType.IncludeAllOption = false;
			this._ucRebateType.Location = new System.Drawing.Point(51, 329);
			this._ucRebateType.Name = "_ucRebateType";
			this._ucRebateType.Size = new System.Drawing.Size(282, 23);
			this._ucRebateType.TabIndex = 24;
			// 
			// _txtGlobalRate
			// 
			this._txtGlobalRate.Enabled = false;
			this._txtGlobalRate.Location = new System.Drawing.Point(252, 299);
			this._txtGlobalRate.Name = "_txtGlobalRate";
			this._txtGlobalRate.Size = new System.Drawing.Size(50, 20);
			this._txtGlobalRate.TabIndex = 31;
			this._txtGlobalRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _lblGlobalRate
			// 
			this._lblGlobalRate.AutoSize = true;
			this._lblGlobalRate.Location = new System.Drawing.Point(216, 302);
			this._lblGlobalRate.Name = "_lblGlobalRate";
			this._lblGlobalRate.Size = new System.Drawing.Size(30, 13);
			this._lblGlobalRate.TabIndex = 30;
			this._lblGlobalRate.Text = "Rate";
			// 
			// _lblVersion
			// 
			this._lblVersion.AutoSize = true;
			this._lblVersion.Location = new System.Drawing.Point(204, 20);
			this._lblVersion.Name = "_lblVersion";
			this._lblVersion.Size = new System.Drawing.Size(42, 13);
			this._lblVersion.TabIndex = 2;
			this._lblVersion.Text = "Version";
			// 
			// _chkIsGlobalProduct
			// 
			this._chkIsGlobalProduct.AutoSize = true;
			this._chkIsGlobalProduct.Location = new System.Drawing.Point(10, 301);
			this._chkIsGlobalProduct.Name = "_chkIsGlobalProduct";
			this._chkIsGlobalProduct.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this._chkIsGlobalProduct.Size = new System.Drawing.Size(134, 17);
			this._chkIsGlobalProduct.TabIndex = 23;
			this._chkIsGlobalProduct.Text = "Global Product Rebate";
			this._chkIsGlobalProduct.UseVisualStyleBackColor = true;
			// 
			// _ucCustomerGroup
			// 
			this._ucCustomerGroup.AlignComboBoxToControl = null;
			this._ucCustomerGroup.ComboBoxWidth = 279;
			this._ucCustomerGroup.ControlSpacing = 6;
			this._ucCustomerGroup.DivisionId = 0;
			this._ucCustomerGroup.IncludeBlankOption = false;
			this._ucCustomerGroup.LabelText = "Customer Group";
			this._ucCustomerGroup.Location = new System.Drawing.Point(40, 84);
			this._ucCustomerGroup.Name = "_ucCustomerGroup";
			this._ucCustomerGroup.ReadOnly = false;
			this._ucCustomerGroup.Size = new System.Drawing.Size(436, 22);
			this._ucCustomerGroup.TabIndex = 7;
			this._ucCustomerGroup.TextBoxWidth = 52;
			// 
			// _txtContractText
			// 
			this._txtContractText.Location = new System.Drawing.Point(158, 207);
			this._txtContractText.MaxLength = 100;
			this._txtContractText.Name = "_txtContractText";
			this._txtContractText.Size = new System.Drawing.Size(312, 20);
			this._txtContractText.TabIndex = 19;
			// 
			// _chkIsVAP
			// 
			this._chkIsVAP.AutoSize = true;
			this._chkIsVAP.Location = new System.Drawing.Point(86, 232);
			this._chkIsVAP.Name = "_chkIsVAP";
			this._chkIsVAP.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this._chkIsVAP.Size = new System.Drawing.Size(58, 17);
			this._chkIsVAP.TabIndex = 20;
			this._chkIsVAP.Text = "Is VAP";
			this._chkIsVAP.UseVisualStyleBackColor = true;
			// 
			// _chkIsContract
			// 
			this._chkIsContract.AutoSize = true;
			this._chkIsContract.Location = new System.Drawing.Point(67, 209);
			this._chkIsContract.Name = "_chkIsContract";
			this._chkIsContract.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this._chkIsContract.Size = new System.Drawing.Size(77, 17);
			this._chkIsContract.TabIndex = 18;
			this._chkIsContract.Text = "Is Contract";
			this._chkIsContract.UseVisualStyleBackColor = true;
			// 
			// _lblTo
			// 
			this._lblTo.AutoSize = true;
			this._lblTo.Location = new System.Drawing.Point(233, 181);
			this._lblTo.Name = "_lblTo";
			this._lblTo.Size = new System.Drawing.Size(16, 13);
			this._lblTo.TabIndex = 16;
			this._lblTo.Text = "to";
			// 
			// _dtpStartDate
			// 
			this._dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this._dtpStartDate.Location = new System.Drawing.Point(132, 177);
			this._dtpStartDate.Name = "_dtpStartDate";
			this._dtpStartDate.Size = new System.Drawing.Size(95, 20);
			this._dtpStartDate.TabIndex = 15;
			this._dtpStartDate.Value = new System.DateTime(2011, 10, 25, 15, 37, 38, 950);
			this._dtpStartDate.ValueNotNull = new System.DateTime(2011, 10, 25, 15, 37, 38, 950);
			// 
			// _lblRebateDate
			// 
			this._lblRebateDate.AutoSize = true;
			this._lblRebateDate.Location = new System.Drawing.Point(58, 181);
			this._lblRebateDate.Name = "_lblRebateDate";
			this._lblRebateDate.Size = new System.Drawing.Size(68, 13);
			this._lblRebateDate.TabIndex = 14;
			this._lblRebateDate.Text = "Rebate Date";
			// 
			// _ucSoldFrom
			// 
			this._ucSoldFrom.AlignComboBoxToControl = null;
			this._ucSoldFrom.Code = null;
			this._ucSoldFrom.ComboBoxWidth = 279;
			this._ucSoldFrom.ControlSpacing = 6;
			this._ucSoldFrom.DivisionId = 0;
			this._ucSoldFrom.IncludeAllOption = false;
			this._ucSoldFrom.LabelText = "Supply Location";
			this._ucSoldFrom.Location = new System.Drawing.Point(40, 153);
			this._ucSoldFrom.Name = "_ucSoldFrom";
			this._ucSoldFrom.ReadOnly = false;
			this._ucSoldFrom.Size = new System.Drawing.Size(458, 22);
			this._ucSoldFrom.TabIndex = 13;
			this._ucSoldFrom.TextBoxWidth = 65;
			// 
			// _ucHierarchy
			// 
			this._ucHierarchy.AllItemText = "All";
			this._ucHierarchy.DivisionId = 0;
			this._ucHierarchy.IncludeAllOption = false;
			this._ucHierarchy.Location = new System.Drawing.Point(27, 62);
			this._ucHierarchy.Name = "_ucHierarchy";
			this._ucHierarchy.Size = new System.Drawing.Size(255, 23);
			this._ucHierarchy.TabIndex = 6;
			// 
			// _ucDeliveryType
			// 
			this._ucDeliveryType.Location = new System.Drawing.Point(48, 129);
			this._ucDeliveryType.Name = "_ucDeliveryType";
			this._ucDeliveryType.SelectedValue = Evolve.Clients.Lafarge.Cement.Business.Rebates.DeliveryTypeEnum.Null;
			this._ucDeliveryType.Size = new System.Drawing.Size(198, 23);
			this._ucDeliveryType.TabIndex = 12;
			// 
			// _txtDescription
			// 
			this._txtDescription.Location = new System.Drawing.Point(132, 40);
			this._txtDescription.MaxLength = 100;
			this._txtDescription.Name = "_txtDescription";
			this._txtDescription.Size = new System.Drawing.Size(300, 20);
			this._txtDescription.TabIndex = 5;
			// 
			// _txtRebateNumber
			// 
			this._txtRebateNumber.Enabled = false;
			this._txtRebateNumber.Location = new System.Drawing.Point(132, 17);
			this._txtRebateNumber.Name = "_txtRebateNumber";
			this._txtRebateNumber.ReadOnly = true;
			this._txtRebateNumber.Size = new System.Drawing.Size(50, 20);
			this._txtRebateNumber.TabIndex = 1;
			// 
			// _lblDescription
			// 
			this._lblDescription.AutoSize = true;
			this._lblDescription.Location = new System.Drawing.Point(66, 43);
			this._lblDescription.Name = "_lblDescription";
			this._lblDescription.Size = new System.Drawing.Size(60, 13);
			this._lblDescription.TabIndex = 4;
			this._lblDescription.Text = "Description";
			// 
			// _lblRebateId
			// 
			this._lblRebateId.AutoSize = true;
			this._lblRebateId.Location = new System.Drawing.Point(44, 20);
			this._lblRebateId.Name = "_lblRebateId";
			this._lblRebateId.Size = new System.Drawing.Size(82, 13);
			this._lblRebateId.TabIndex = 0;
			this._lblRebateId.Text = "Rebate Number";
			// 
			// _lblUcCustomer
			// 
			this._lblUcCustomer.Location = new System.Drawing.Point(10, 86);
			this._lblUcCustomer.Name = "_lblUcCustomer";
			this._lblUcCustomer.Size = new System.Drawing.Size(116, 16);
			this._lblUcCustomer.TabIndex = 32;
			this._lblUcCustomer.Text = "Customer";
			this._lblUcCustomer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// Detail
			// 
			this.Detail.Controls.Add(this._grpDetailProduct);
			this.Detail.Controls.Add(this._grpDetailSelection);
			this.Detail.Location = new System.Drawing.Point(4, 22);
			this.Detail.Name = "Detail";
			this.Detail.Size = new System.Drawing.Size(887, 440);
			this.Detail.TabIndex = 5;
			this.Detail.Text = "Product Detail";
			this.Detail.UseVisualStyleBackColor = true;
			// 
			// _grpDetailProduct
			// 
			this._grpDetailProduct.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._grpDetailProduct.Controls.Add(this._lblErrorProductDetail);
			this._grpDetailProduct.Controls.Add(this._dgvDetail);
			this._grpDetailProduct.Location = new System.Drawing.Point(8, 154);
			this._grpDetailProduct.Name = "_grpDetailProduct";
			this._grpDetailProduct.Size = new System.Drawing.Size(869, 283);
			this._grpDetailProduct.TabIndex = 1;
			this._grpDetailProduct.TabStop = false;
			this._grpDetailProduct.Text = "Included Products / Attributes (Select row(s) and press Delete to remove)";
			// 
			// _lblErrorProductDetail
			// 
			this._lblErrorProductDetail.AutoSize = true;
			this._lblErrorProductDetail.Location = new System.Drawing.Point(362, 0);
			this._lblErrorProductDetail.Name = "_lblErrorProductDetail";
			this._lblErrorProductDetail.Size = new System.Drawing.Size(0, 13);
			this._lblErrorProductDetail.TabIndex = 1;
			// 
			// _dgvDetail
			// 
			this._dgvDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this._dgvDetail.Dock = System.Windows.Forms.DockStyle.Fill;
			this._dgvDetail.Location = new System.Drawing.Point(3, 16);
			this._dgvDetail.Name = "_dgvDetail";
			this._dgvDetail.Size = new System.Drawing.Size(863, 264);
			this._dgvDetail.TabIndex = 0;
			// 
			// _grpDetailSelection
			// 
			this._grpDetailSelection.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._grpDetailSelection.Controls.Add(this._btnProductDetailClear);
			this._grpDetailSelection.Controls.Add(this._ucProductDetail);
			this._grpDetailSelection.Controls.Add(this._lblAttribute4);
			this._grpDetailSelection.Controls.Add(this._lblAttribute3);
			this._grpDetailSelection.Controls.Add(this._lblAttribute2);
			this._grpDetailSelection.Controls.Add(this._lblAttribute1);
			this._grpDetailSelection.Controls.Add(this._ucAttribute4);
			this._grpDetailSelection.Controls.Add(this._ucAttribute3);
			this._grpDetailSelection.Controls.Add(this._ucAttribute2);
			this._grpDetailSelection.Controls.Add(this._ucAttribute1);
			this._grpDetailSelection.Controls.Add(this._btnAddProductDetail);
			this._grpDetailSelection.Location = new System.Drawing.Point(8, 3);
			this._grpDetailSelection.Name = "_grpDetailSelection";
			this._grpDetailSelection.Size = new System.Drawing.Size(869, 145);
			this._grpDetailSelection.TabIndex = 0;
			this._grpDetailSelection.TabStop = false;
			this._grpDetailSelection.Text = "Product / Attribute Selection";
			// 
			// _btnProductDetailClear
			// 
			this._btnProductDetailClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._btnProductDetailClear.Location = new System.Drawing.Point(712, 107);
			this._btnProductDetailClear.Name = "_btnProductDetailClear";
			this._btnProductDetailClear.Size = new System.Drawing.Size(75, 23);
			this._btnProductDetailClear.TabIndex = 14;
			this._btnProductDetailClear.Text = "Clear";
			this._btnProductDetailClear.UseVisualStyleBackColor = true;
			// 
			// _ucProductDetail
			// 
			this._ucProductDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._ucProductDetail.DivisionId = 0;
			this._ucProductDetail.LabelText = "Product";
			this._ucProductDetail.Location = new System.Drawing.Point(421, 32);
			this._ucProductDetail.MultiSelect = false;
			this._ucProductDetail.Name = "_ucProductDetail";
			this._ucProductDetail.Size = new System.Drawing.Size(432, 26);
			this._ucProductDetail.TabIndex = 8;
			// 
			// _lblAttribute4
			// 
			this._lblAttribute4.AutoSize = true;
			this._lblAttribute4.Location = new System.Drawing.Point(57, 103);
			this._lblAttribute4.Name = "_lblAttribute4";
			this._lblAttribute4.Size = new System.Drawing.Size(13, 13);
			this._lblAttribute4.TabIndex = 13;
			this._lblAttribute4.Text = "4";
			// 
			// _lblAttribute3
			// 
			this._lblAttribute3.AutoSize = true;
			this._lblAttribute3.Location = new System.Drawing.Point(57, 80);
			this._lblAttribute3.Name = "_lblAttribute3";
			this._lblAttribute3.Size = new System.Drawing.Size(13, 13);
			this._lblAttribute3.TabIndex = 4;
			this._lblAttribute3.Text = "3";
			// 
			// _lblAttribute2
			// 
			this._lblAttribute2.AutoSize = true;
			this._lblAttribute2.Location = new System.Drawing.Point(57, 56);
			this._lblAttribute2.Name = "_lblAttribute2";
			this._lblAttribute2.Size = new System.Drawing.Size(13, 13);
			this._lblAttribute2.TabIndex = 2;
			this._lblAttribute2.Text = "2";
			// 
			// _lblAttribute1
			// 
			this._lblAttribute1.AutoSize = true;
			this._lblAttribute1.Location = new System.Drawing.Point(15, 32);
			this._lblAttribute1.Name = "_lblAttribute1";
			this._lblAttribute1.Size = new System.Drawing.Size(55, 13);
			this._lblAttribute1.TabIndex = 0;
			this._lblAttribute1.Text = "Attribute 1";
			// 
			// _ucAttribute4
			// 
			this._ucAttribute4.DivisionId = 0;
			this._ucAttribute4.IncludeBlank = false;
			this._ucAttribute4.Level = null;
			this._ucAttribute4.Location = new System.Drawing.Point(76, 100);
			this._ucAttribute4.Name = "_ucAttribute4";
			this._ucAttribute4.Size = new System.Drawing.Size(190, 22);
			this._ucAttribute4.TabIndex = 7;
			// 
			// _ucAttribute3
			// 
			this._ucAttribute3.DivisionId = 0;
			this._ucAttribute3.IncludeBlank = false;
			this._ucAttribute3.Level = null;
			this._ucAttribute3.Location = new System.Drawing.Point(76, 76);
			this._ucAttribute3.Name = "_ucAttribute3";
			this._ucAttribute3.Size = new System.Drawing.Size(190, 22);
			this._ucAttribute3.TabIndex = 5;
			// 
			// _ucAttribute2
			// 
			this._ucAttribute2.DivisionId = 0;
			this._ucAttribute2.IncludeBlank = false;
			this._ucAttribute2.Level = null;
			this._ucAttribute2.Location = new System.Drawing.Point(76, 52);
			this._ucAttribute2.Name = "_ucAttribute2";
			this._ucAttribute2.Size = new System.Drawing.Size(190, 22);
			this._ucAttribute2.TabIndex = 3;
			// 
			// _ucAttribute1
			// 
			this._ucAttribute1.DivisionId = 0;
			this._ucAttribute1.IncludeBlank = false;
			this._ucAttribute1.Level = null;
			this._ucAttribute1.Location = new System.Drawing.Point(76, 28);
			this._ucAttribute1.Name = "_ucAttribute1";
			this._ucAttribute1.Size = new System.Drawing.Size(190, 22);
			this._ucAttribute1.TabIndex = 1;
			// 
			// _btnAddProductDetail
			// 
			this._btnAddProductDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._btnAddProductDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this._btnAddProductDetail.Location = new System.Drawing.Point(805, 96);
			this._btnAddProductDetail.Name = "_btnAddProductDetail";
			this._btnAddProductDetail.Size = new System.Drawing.Size(38, 34);
			this._btnAddProductDetail.TabIndex = 9;
			this._btnAddProductDetail.Text = "+";
			this._btnAddProductDetail.UseVisualStyleBackColor = true;
			// 
			// VAP
			// 
			this.VAP.AutoScroll = true;
			this.VAP.Controls.Add(this._grpVAPProductSelect);
			this.VAP.Controls.Add(this._grpVAPProducts);
			this.VAP.Controls.Add(this._grpVAPActivation);
			this.VAP.Location = new System.Drawing.Point(4, 22);
			this.VAP.Name = "VAP";
			this.VAP.Size = new System.Drawing.Size(887, 440);
			this.VAP.TabIndex = 1;
			this.VAP.Text = "VAP";
			this.VAP.UseVisualStyleBackColor = true;
			// 
			// _grpVAPProductSelect
			// 
			this._grpVAPProductSelect.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._grpVAPProductSelect.Controls.Add(this._ucProductVAP);
			this._grpVAPProductSelect.Controls.Add(this._btnVAPProductAdd);
			this._grpVAPProductSelect.Location = new System.Drawing.Point(269, 3);
			this._grpVAPProductSelect.Name = "_grpVAPProductSelect";
			this._grpVAPProductSelect.Size = new System.Drawing.Size(610, 100);
			this._grpVAPProductSelect.TabIndex = 1;
			this._grpVAPProductSelect.TabStop = false;
			this._grpVAPProductSelect.Text = "Product Selection";
			// 
			// _ucProductVAP
			// 
			this._ucProductVAP.DivisionId = 0;
			this._ucProductVAP.LabelText = "Product";
			this._ucProductVAP.Location = new System.Drawing.Point(10, 19);
			this._ucProductVAP.MultiSelect = false;
			this._ucProductVAP.Name = "_ucProductVAP";
			this._ucProductVAP.Size = new System.Drawing.Size(432, 26);
			this._ucProductVAP.TabIndex = 0;
			// 
			// _btnVAPProductAdd
			// 
			this._btnVAPProductAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this._btnVAPProductAdd.Location = new System.Drawing.Point(393, 51);
			this._btnVAPProductAdd.Name = "_btnVAPProductAdd";
			this._btnVAPProductAdd.Size = new System.Drawing.Size(38, 34);
			this._btnVAPProductAdd.TabIndex = 1;
			this._btnVAPProductAdd.Text = "+";
			this._btnVAPProductAdd.UseVisualStyleBackColor = true;
			// 
			// _grpVAPProducts
			// 
			this._grpVAPProducts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._grpVAPProducts.Controls.Add(this._lblErrorVAP);
			this._grpVAPProducts.Controls.Add(this._dgvVAP);
			this._grpVAPProducts.Location = new System.Drawing.Point(8, 109);
			this._grpVAPProducts.Name = "_grpVAPProducts";
			this._grpVAPProducts.Size = new System.Drawing.Size(871, 328);
			this._grpVAPProducts.TabIndex = 2;
			this._grpVAPProducts.TabStop = false;
			this._grpVAPProducts.Text = "Products";
			// 
			// _lblErrorVAP
			// 
			this._lblErrorVAP.AutoSize = true;
			this._lblErrorVAP.Location = new System.Drawing.Point(57, 0);
			this._lblErrorVAP.Name = "_lblErrorVAP";
			this._lblErrorVAP.Size = new System.Drawing.Size(0, 13);
			this._lblErrorVAP.TabIndex = 1;
			// 
			// _dgvVAP
			// 
			this._dgvVAP.AllowUserToAddRows = false;
			this._dgvVAP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this._dgvVAP.Dock = System.Windows.Forms.DockStyle.Fill;
			this._dgvVAP.Location = new System.Drawing.Point(3, 16);
			this._dgvVAP.Name = "_dgvVAP";
			this._dgvVAP.Size = new System.Drawing.Size(865, 309);
			this._dgvVAP.TabIndex = 0;
			// 
			// _grpVAPActivation
			// 
			this._grpVAPActivation.Controls.Add(this._ucVAPActivationMethod);
			this._grpVAPActivation.Controls.Add(this._txtVAPActivationQuantity);
			this._grpVAPActivation.Controls.Add(this._lblVAPActivationQuantity);
			this._grpVAPActivation.Location = new System.Drawing.Point(8, 3);
			this._grpVAPActivation.Name = "_grpVAPActivation";
			this._grpVAPActivation.Size = new System.Drawing.Size(255, 100);
			this._grpVAPActivation.TabIndex = 0;
			this._grpVAPActivation.TabStop = false;
			this._grpVAPActivation.Text = "VAP Activation Details";
			// 
			// _ucVAPActivationMethod
			// 
			this._ucVAPActivationMethod.Location = new System.Drawing.Point(21, 23);
			this._ucVAPActivationMethod.Name = "_ucVAPActivationMethod";
			this._ucVAPActivationMethod.SelectedValue = Evolve.Clients.Lafarge.Cement.Business.Rebates.VAPActivationMethod.Global;
			this._ucVAPActivationMethod.Size = new System.Drawing.Size(228, 21);
			this._ucVAPActivationMethod.TabIndex = 0;
			// 
			// _txtVAPActivationQuantity
			// 
			this._txtVAPActivationQuantity.Location = new System.Drawing.Point(120, 46);
			this._txtVAPActivationQuantity.Name = "_txtVAPActivationQuantity";
			this._txtVAPActivationQuantity.Size = new System.Drawing.Size(75, 20);
			this._txtVAPActivationQuantity.TabIndex = 2;
			this._txtVAPActivationQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _lblVAPActivationQuantity
			// 
			this._lblVAPActivationQuantity.AutoSize = true;
			this._lblVAPActivationQuantity.Location = new System.Drawing.Point(18, 49);
			this._lblVAPActivationQuantity.Name = "_lblVAPActivationQuantity";
			this._lblVAPActivationQuantity.Size = new System.Drawing.Size(96, 13);
			this._lblVAPActivationQuantity.TabIndex = 1;
			this._lblVAPActivationQuantity.Text = "Activation Quantity";
			// 
			// RebateExtension
			// 
			this.RebateExtension.AutoScroll = true;
			this.RebateExtension.Controls.Add(this._grpDeliveryWindows);
			this.RebateExtension.Controls.Add(this._grpDeliveryWindowSelect);
			this.RebateExtension.Location = new System.Drawing.Point(4, 22);
			this.RebateExtension.Name = "RebateExtension";
			this.RebateExtension.Padding = new System.Windows.Forms.Padding(3);
			this.RebateExtension.Size = new System.Drawing.Size(887, 440);
			this.RebateExtension.TabIndex = 7;
			this.RebateExtension.Text = "Rebate Extension";
			this.RebateExtension.UseVisualStyleBackColor = true;
			// 
			// _grpDeliveryWindows
			// 
			this._grpDeliveryWindows.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._grpDeliveryWindows.Controls.Add(this._dgvRebateExtension);
			this._grpDeliveryWindows.Controls.Add(this._lblErrorDeliveryWindow);
			this._grpDeliveryWindows.Location = new System.Drawing.Point(3, 97);
			this._grpDeliveryWindows.Name = "_grpDeliveryWindows";
			this._grpDeliveryWindows.Size = new System.Drawing.Size(881, 340);
			this._grpDeliveryWindows.TabIndex = 1;
			this._grpDeliveryWindows.TabStop = false;
			this._grpDeliveryWindows.Text = "Delivery Windows";
			// 
			// _dgvRebateExtension
			// 
			this._dgvRebateExtension.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this._dgvRebateExtension.Dock = System.Windows.Forms.DockStyle.Fill;
			this._dgvRebateExtension.Location = new System.Drawing.Point(3, 16);
			this._dgvRebateExtension.Name = "_dgvRebateExtension";
			this._dgvRebateExtension.Size = new System.Drawing.Size(875, 321);
			this._dgvRebateExtension.TabIndex = 1;
			// 
			// _lblErrorDeliveryWindow
			// 
			this._lblErrorDeliveryWindow.AutoSize = true;
			this._lblErrorDeliveryWindow.Location = new System.Drawing.Point(99, 0);
			this._lblErrorDeliveryWindow.Name = "_lblErrorDeliveryWindow";
			this._lblErrorDeliveryWindow.Size = new System.Drawing.Size(0, 13);
			this._lblErrorDeliveryWindow.TabIndex = 0;
			// 
			// _grpDeliveryWindowSelect
			// 
			this._grpDeliveryWindowSelect.Controls.Add(this._ucLeadTime);
			this._grpDeliveryWindowSelect.Controls.Add(this._ucPackingType);
			this._grpDeliveryWindowSelect.Controls.Add(this._ucDeliveryWindow);
			this._grpDeliveryWindowSelect.Controls.Add(this._btnAddRebateExtension);
			this._grpDeliveryWindowSelect.Dock = System.Windows.Forms.DockStyle.Top;
			this._grpDeliveryWindowSelect.Location = new System.Drawing.Point(3, 3);
			this._grpDeliveryWindowSelect.Name = "_grpDeliveryWindowSelect";
			this._grpDeliveryWindowSelect.Size = new System.Drawing.Size(881, 94);
			this._grpDeliveryWindowSelect.TabIndex = 0;
			this._grpDeliveryWindowSelect.TabStop = false;
			this._grpDeliveryWindowSelect.Text = "Delivery Window Selection";
			// 
			// _ucLeadTime
			// 
			this._ucLeadTime.DivisionId = 0;
			this._ucLeadTime.ExcludeIds = ((System.Collections.Generic.List<int>)(resources.GetObject("_ucLeadTime.ExcludeIds")));
			this._ucLeadTime.IncludeAllOption = false;
			this._ucLeadTime.Location = new System.Drawing.Point(47, 52);
			this._ucLeadTime.Name = "_ucLeadTime";
			this._ucLeadTime.Size = new System.Drawing.Size(274, 23);
			this._ucLeadTime.TabIndex = 5;
			// 
			// _ucPackingType
			// 
			this._ucPackingType.Location = new System.Drawing.Point(37, 27);
			this._ucPackingType.Name = "_ucPackingType";
			this._ucPackingType.Size = new System.Drawing.Size(161, 23);
			this._ucPackingType.TabIndex = 4;
			// 
			// _ucDeliveryWindow
			// 
			this._ucDeliveryWindow.DivisionId = 0;
			this._ucDeliveryWindow.ExcludeIds = ((System.Collections.Generic.List<int>)(resources.GetObject("_ucDeliveryWindow.ExcludeIds")));
			this._ucDeliveryWindow.IncludeAllOption = false;
			this._ucDeliveryWindow.Location = new System.Drawing.Point(17, 52);
			this._ucDeliveryWindow.Name = "_ucDeliveryWindow";
			this._ucDeliveryWindow.Size = new System.Drawing.Size(325, 23);
			this._ucDeliveryWindow.TabIndex = 3;
			// 
			// _btnAddRebateExtension
			// 
			this._btnAddRebateExtension.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._btnAddRebateExtension.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this._btnAddRebateExtension.Location = new System.Drawing.Point(704, 31);
			this._btnAddRebateExtension.Name = "_btnAddRebateExtension";
			this._btnAddRebateExtension.Size = new System.Drawing.Size(38, 34);
			this._btnAddRebateExtension.TabIndex = 2;
			this._btnAddRebateExtension.Text = "+";
			this._btnAddRebateExtension.UseVisualStyleBackColor = true;
			// 
			// SoldFrom
			// 
			this.SoldFrom.AutoScroll = true;
			this.SoldFrom.Controls.Add(this._btnSoldFromInclude);
			this.SoldFrom.Controls.Add(this._btnSoldFromExclude);
			this.SoldFrom.Controls.Add(this._lblSoldFromExclude);
			this.SoldFrom.Controls.Add(this._lblSoldFromInclude);
			this.SoldFrom.Controls.Add(this._lstSoldFromExclude);
			this.SoldFrom.Controls.Add(this._lstSoldFromInclude);
			this.SoldFrom.Location = new System.Drawing.Point(4, 22);
			this.SoldFrom.Name = "SoldFrom";
			this.SoldFrom.Size = new System.Drawing.Size(887, 440);
			this.SoldFrom.TabIndex = 2;
			this.SoldFrom.Text = "Supply Location Exceptions";
			this.SoldFrom.UseVisualStyleBackColor = true;
			// 
			// _btnSoldFromInclude
			// 
			this._btnSoldFromInclude.Location = new System.Drawing.Point(290, 216);
			this._btnSoldFromInclude.Name = "_btnSoldFromInclude";
			this._btnSoldFromInclude.Size = new System.Drawing.Size(34, 34);
			this._btnSoldFromInclude.TabIndex = 5;
			this._btnSoldFromInclude.Text = "<";
			this._btnSoldFromInclude.UseVisualStyleBackColor = true;
			// 
			// _btnSoldFromExclude
			// 
			this._btnSoldFromExclude.Location = new System.Drawing.Point(290, 176);
			this._btnSoldFromExclude.Name = "_btnSoldFromExclude";
			this._btnSoldFromExclude.Size = new System.Drawing.Size(34, 34);
			this._btnSoldFromExclude.TabIndex = 4;
			this._btnSoldFromExclude.Text = ">";
			this._btnSoldFromExclude.UseVisualStyleBackColor = true;
			// 
			// _lblSoldFromExclude
			// 
			this._lblSoldFromExclude.AutoSize = true;
			this._lblSoldFromExclude.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this._lblSoldFromExclude.Location = new System.Drawing.Point(327, 20);
			this._lblSoldFromExclude.Name = "_lblSoldFromExclude";
			this._lblSoldFromExclude.Size = new System.Drawing.Size(56, 13);
			this._lblSoldFromExclude.TabIndex = 1;
			this._lblSoldFromExclude.Text = "Exclude:";
			// 
			// _lblSoldFromInclude
			// 
			this._lblSoldFromInclude.AutoSize = true;
			this._lblSoldFromInclude.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this._lblSoldFromInclude.Location = new System.Drawing.Point(24, 20);
			this._lblSoldFromInclude.Name = "_lblSoldFromInclude";
			this._lblSoldFromInclude.Size = new System.Drawing.Size(53, 13);
			this._lblSoldFromInclude.TabIndex = 0;
			this._lblSoldFromInclude.Text = "Include:";
			// 
			// _lstSoldFromExclude
			// 
			this._lstSoldFromExclude.AllowDrop = true;
			this._lstSoldFromExclude.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this._lstSoldFromExclude.FormattingEnabled = true;
			this._lstSoldFromExclude.Location = new System.Drawing.Point(330, 36);
			this._lstSoldFromExclude.Name = "_lstSoldFromExclude";
			this._lstSoldFromExclude.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
			this._lstSoldFromExclude.Size = new System.Drawing.Size(257, 381);
			this._lstSoldFromExclude.TabIndex = 3;
			// 
			// _lstSoldFromInclude
			// 
			this._lstSoldFromInclude.AllowDrop = true;
			this._lstSoldFromInclude.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this._lstSoldFromInclude.FormattingEnabled = true;
			this._lstSoldFromInclude.Location = new System.Drawing.Point(27, 36);
			this._lstSoldFromInclude.Name = "_lstSoldFromInclude";
			this._lstSoldFromInclude.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
			this._lstSoldFromInclude.Size = new System.Drawing.Size(257, 381);
			this._lstSoldFromInclude.TabIndex = 2;
			// 
			// SoldTo
			// 
			this.SoldTo.Controls.Add(this._grpSoldToExcluded);
			this.SoldTo.Controls.Add(this._grpSoldToSelection);
			this.SoldTo.Location = new System.Drawing.Point(4, 22);
			this.SoldTo.Name = "SoldTo";
			this.SoldTo.Size = new System.Drawing.Size(887, 440);
			this.SoldTo.TabIndex = 6;
			this.SoldTo.Text = "Sold To Exceptions";
			this.SoldTo.UseVisualStyleBackColor = true;
			// 
			// _grpSoldToExcluded
			// 
			this._grpSoldToExcluded.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._grpSoldToExcluded.Controls.Add(this._dgvSoldTo);
			this._grpSoldToExcluded.Location = new System.Drawing.Point(9, 101);
			this._grpSoldToExcluded.Name = "_grpSoldToExcluded";
			this._grpSoldToExcluded.Size = new System.Drawing.Size(870, 336);
			this._grpSoldToExcluded.TabIndex = 1;
			this._grpSoldToExcluded.TabStop = false;
			this._grpSoldToExcluded.Text = "Excluded Sold To Customer Accounts (Select row(s) and press Delete to remove)";
			// 
			// _dgvSoldTo
			// 
			this._dgvSoldTo.AllowUserToAddRows = false;
			this._dgvSoldTo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this._dgvSoldTo.Dock = System.Windows.Forms.DockStyle.Fill;
			this._dgvSoldTo.Location = new System.Drawing.Point(3, 16);
			this._dgvSoldTo.Name = "_dgvSoldTo";
			this._dgvSoldTo.ReadOnly = true;
			this._dgvSoldTo.Size = new System.Drawing.Size(864, 317);
			this._dgvSoldTo.TabIndex = 0;
			// 
			// _grpSoldToSelection
			// 
			this._grpSoldToSelection.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._grpSoldToSelection.Controls.Add(this._btnSoldToAdd);
			this._grpSoldToSelection.Controls.Add(this._ucSoldToCustomer);
			this._grpSoldToSelection.Location = new System.Drawing.Point(8, 1);
			this._grpSoldToSelection.Name = "_grpSoldToSelection";
			this._grpSoldToSelection.Size = new System.Drawing.Size(871, 93);
			this._grpSoldToSelection.TabIndex = 0;
			this._grpSoldToSelection.TabStop = false;
			this._grpSoldToSelection.Text = "Selection";
			// 
			// _btnSoldToAdd
			// 
			this._btnSoldToAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._btnSoldToAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this._btnSoldToAdd.Location = new System.Drawing.Point(804, 32);
			this._btnSoldToAdd.Name = "_btnSoldToAdd";
			this._btnSoldToAdd.Size = new System.Drawing.Size(38, 34);
			this._btnSoldToAdd.TabIndex = 1;
			this._btnSoldToAdd.Text = "+";
			this._btnSoldToAdd.UseVisualStyleBackColor = true;
			// 
			// _ucSoldToCustomer
			// 
			this._ucSoldToCustomer.AccountNumber = null;
			this._ucSoldToCustomer.CustomerGroupId = null;
			this._ucSoldToCustomer.CustomerSearchModeEnum = Evolve.Clients.Lafarge.Cement.Business.Rebates.CustomerSearchModeEnum.SoldTo;
			this._ucSoldToCustomer.DivisionId = 0;
			this._ucSoldToCustomer.LabelText = "Sold To";
			this._ucSoldToCustomer.Location = new System.Drawing.Point(18, 36);
			this._ucSoldToCustomer.MultiSelect = false;
			this._ucSoldToCustomer.Name = "_ucSoldToCustomer";
			this._ucSoldToCustomer.ParentCustomerId = null;
			this._ucSoldToCustomer.Size = new System.Drawing.Size(432, 26);
			this._ucSoldToCustomer.TabIndex = 0;
			// 
			// ShipTo
			// 
			this.ShipTo.Controls.Add(this._grpShipToExcluded);
			this.ShipTo.Controls.Add(this._grpShipToSelection);
			this.ShipTo.Location = new System.Drawing.Point(4, 22);
			this.ShipTo.Name = "ShipTo";
			this.ShipTo.Size = new System.Drawing.Size(887, 440);
			this.ShipTo.TabIndex = 3;
			this.ShipTo.Text = "Ship To Exceptions";
			this.ShipTo.UseVisualStyleBackColor = true;
			// 
			// _grpShipToExcluded
			// 
			this._grpShipToExcluded.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._grpShipToExcluded.Controls.Add(this._dgvShipTo);
			this._grpShipToExcluded.Location = new System.Drawing.Point(9, 101);
			this._grpShipToExcluded.Name = "_grpShipToExcluded";
			this._grpShipToExcluded.Size = new System.Drawing.Size(870, 336);
			this._grpShipToExcluded.TabIndex = 1;
			this._grpShipToExcluded.TabStop = false;
			this._grpShipToExcluded.Text = "Excluded Sold To Customer Accounts (Select row(s) and press Delete to remove)";
			// 
			// _dgvShipTo
			// 
			this._dgvShipTo.AllowUserToAddRows = false;
			this._dgvShipTo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this._dgvShipTo.Dock = System.Windows.Forms.DockStyle.Fill;
			this._dgvShipTo.Location = new System.Drawing.Point(3, 16);
			this._dgvShipTo.Name = "_dgvShipTo";
			this._dgvShipTo.ReadOnly = true;
			this._dgvShipTo.Size = new System.Drawing.Size(864, 317);
			this._dgvShipTo.TabIndex = 0;
			// 
			// _grpShipToSelection
			// 
			this._grpShipToSelection.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._grpShipToSelection.Controls.Add(this._btnShipToAdd);
			this._grpShipToSelection.Controls.Add(this._ucShipToCustomer);
			this._grpShipToSelection.Location = new System.Drawing.Point(8, 1);
			this._grpShipToSelection.Name = "_grpShipToSelection";
			this._grpShipToSelection.Size = new System.Drawing.Size(871, 93);
			this._grpShipToSelection.TabIndex = 0;
			this._grpShipToSelection.TabStop = false;
			this._grpShipToSelection.Text = "Selection";
			// 
			// _btnShipToAdd
			// 
			this._btnShipToAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._btnShipToAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this._btnShipToAdd.Location = new System.Drawing.Point(804, 32);
			this._btnShipToAdd.Name = "_btnShipToAdd";
			this._btnShipToAdd.Size = new System.Drawing.Size(38, 34);
			this._btnShipToAdd.TabIndex = 1;
			this._btnShipToAdd.Text = "+";
			this._btnShipToAdd.UseVisualStyleBackColor = true;
			// 
			// _ucShipToCustomer
			// 
			this._ucShipToCustomer.AccountNumber = null;
			this._ucShipToCustomer.CustomerGroupId = null;
			this._ucShipToCustomer.CustomerSearchModeEnum = Evolve.Clients.Lafarge.Cement.Business.Rebates.CustomerSearchModeEnum.ShipTo;
			this._ucShipToCustomer.DivisionId = 0;
			this._ucShipToCustomer.LabelText = "Ship To";
			this._ucShipToCustomer.Location = new System.Drawing.Point(18, 36);
			this._ucShipToCustomer.MultiSelect = false;
			this._ucShipToCustomer.Name = "_ucShipToCustomer";
			this._ucShipToCustomer.ParentCustomerId = null;
			this._ucShipToCustomer.Size = new System.Drawing.Size(432, 26);
			this._ucShipToCustomer.TabIndex = 0;
			// 
			// Notes
			// 
			this.Notes.Controls.Add(this._grpNotes);
			this.Notes.Controls.Add(this._grpNoteAdd);
			this.Notes.Location = new System.Drawing.Point(4, 22);
			this.Notes.Name = "Notes";
			this.Notes.Size = new System.Drawing.Size(887, 440);
			this.Notes.TabIndex = 4;
			this.Notes.Text = "Notes";
			this.Notes.UseVisualStyleBackColor = true;
			// 
			// _grpNotes
			// 
			this._grpNotes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._grpNotes.Controls.Add(this._dgvNotes);
			this._grpNotes.Location = new System.Drawing.Point(8, 107);
			this._grpNotes.Name = "_grpNotes";
			this._grpNotes.Size = new System.Drawing.Size(871, 330);
			this._grpNotes.TabIndex = 1;
			this._grpNotes.TabStop = false;
			this._grpNotes.Text = "History";
			// 
			// _dgvNotes
			// 
			this._dgvNotes.AllowUserToAddRows = false;
			this._dgvNotes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this._dgvNotes.Dock = System.Windows.Forms.DockStyle.Fill;
			this._dgvNotes.Location = new System.Drawing.Point(3, 16);
			this._dgvNotes.Name = "_dgvNotes";
			this._dgvNotes.ReadOnly = true;
			this._dgvNotes.Size = new System.Drawing.Size(865, 311);
			this._dgvNotes.TabIndex = 0;
			// 
			// _grpNoteAdd
			// 
			this._grpNoteAdd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._grpNoteAdd.Controls.Add(this._lblNoteError);
			this._grpNoteAdd.Controls.Add(this._btnNoteAdd);
			this._grpNoteAdd.Controls.Add(this._txtNote);
			this._grpNoteAdd.Location = new System.Drawing.Point(8, 1);
			this._grpNoteAdd.Name = "_grpNoteAdd";
			this._grpNoteAdd.Size = new System.Drawing.Size(871, 100);
			this._grpNoteAdd.TabIndex = 0;
			this._grpNoteAdd.TabStop = false;
			this._grpNoteAdd.Text = "Note Entry";
			// 
			// _lblNoteError
			// 
			this._lblNoteError.AutoSize = true;
			this._lblNoteError.Location = new System.Drawing.Point(6, 53);
			this._lblNoteError.Name = "_lblNoteError";
			this._lblNoteError.Size = new System.Drawing.Size(0, 13);
			this._lblNoteError.TabIndex = 2;
			// 
			// _btnNoteAdd
			// 
			this._btnNoteAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._btnNoteAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this._btnNoteAdd.Location = new System.Drawing.Point(827, 56);
			this._btnNoteAdd.Name = "_btnNoteAdd";
			this._btnNoteAdd.Size = new System.Drawing.Size(38, 34);
			this._btnNoteAdd.TabIndex = 1;
			this._btnNoteAdd.Text = "+";
			this._btnNoteAdd.UseVisualStyleBackColor = true;
			// 
			// _txtNote
			// 
			this._txtNote.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtNote.Location = new System.Drawing.Point(6, 30);
			this._txtNote.MaxLength = 500;
			this._txtNote.Name = "_txtNote";
			this._txtNote.Size = new System.Drawing.Size(859, 20);
			this._txtNote.TabIndex = 0;
			// 
			// frmRebate
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(895, 506);
			this.Controls.Add(this._tabControl);
			this.Controls.Add(this.panel1);
			this.Name = "frmRebate";
			this.Text = "Rebate";
			((System.ComponentModel.ISupportInitialize)(this._errorProvider)).EndInit();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this._tabControl.ResumeLayout(false);
			this.Header.ResumeLayout(false);
			this.Header.PerformLayout();
			this._grpVolumeRange.ResumeLayout(false);
			this._grpVolumeRange.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this._dgvRange)).EndInit();
			this.Detail.ResumeLayout(false);
			this._grpDetailProduct.ResumeLayout(false);
			this._grpDetailProduct.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this._dgvDetail)).EndInit();
			this._grpDetailSelection.ResumeLayout(false);
			this._grpDetailSelection.PerformLayout();
			this.VAP.ResumeLayout(false);
			this._grpVAPProductSelect.ResumeLayout(false);
			this._grpVAPProducts.ResumeLayout(false);
			this._grpVAPProducts.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this._dgvVAP)).EndInit();
			this._grpVAPActivation.ResumeLayout(false);
			this._grpVAPActivation.PerformLayout();
			this.RebateExtension.ResumeLayout(false);
			this._grpDeliveryWindows.ResumeLayout(false);
			this._grpDeliveryWindows.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this._dgvRebateExtension)).EndInit();
			this._grpDeliveryWindowSelect.ResumeLayout(false);
			this.SoldFrom.ResumeLayout(false);
			this.SoldFrom.PerformLayout();
			this.SoldTo.ResumeLayout(false);
			this._grpSoldToExcluded.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this._dgvSoldTo)).EndInit();
			this._grpSoldToSelection.ResumeLayout(false);
			this.ShipTo.ResumeLayout(false);
			this._grpShipToExcluded.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this._dgvShipTo)).EndInit();
			this._grpShipToSelection.ResumeLayout(false);
			this.Notes.ResumeLayout(false);
			this._grpNotes.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this._dgvNotes)).EndInit();
			this._grpNoteAdd.ResumeLayout(false);
			this._grpNoteAdd.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ErrorProvider _errorProvider;
		private System.Windows.Forms.Panel panel1;
		private Evolve.Libraries.Controls.Generic.OkCancel _okCancel;
		private System.Windows.Forms.TabControl _tabControl;
		private System.Windows.Forms.TabPage Header;
		private Evolve.Libraries.Controls.Generic.DateTimePickerNull _dtpEndDate;
		private System.Windows.Forms.TextBox _txtVersion;
		private UserControls.Rebates.PaymentFrequency _ucPaymentFrequency;
		private System.Windows.Forms.GroupBox _grpVolumeRange;
		private System.Windows.Forms.DataGridView _dgvRange;
		private System.Windows.Forms.TextBox _txtFixedValue;
		private System.Windows.Forms.Label _lblFixedValue;
		private System.Windows.Forms.TextBox _txtEstimatedVolume;
		private System.Windows.Forms.Label _lblEstimatedVolume;
		private UserControls.Rebates.RebateType _ucRebateType;
		private System.Windows.Forms.TextBox _txtGlobalRate;
		private System.Windows.Forms.Label _lblGlobalRate;
		private System.Windows.Forms.Label _lblVersion;
		private System.Windows.Forms.CheckBox _chkIsGlobalProduct;
		private UserControls.Rebates.CustomerGroup _ucCustomerGroup;
		private System.Windows.Forms.TextBox _txtContractText;
		private System.Windows.Forms.CheckBox _chkIsVAP;
		private System.Windows.Forms.CheckBox _chkIsContract;
		private System.Windows.Forms.Label _lblTo;
		private Evolve.Libraries.Controls.Generic.DateTimePickerNull _dtpStartDate;
		private System.Windows.Forms.Label _lblRebateDate;
		private UserControls.Rebates.Site _ucSoldFrom;
		private UserControls.Rebates.Hierarchy _ucHierarchy;
		private UserControls.Rebates.DeliveryType _ucDeliveryType;
		private UserControls.Rebates.Customer _ucCustomer;
		private System.Windows.Forms.TextBox _txtDescription;
		private System.Windows.Forms.TextBox _txtRebateNumber;
		private System.Windows.Forms.Label _lblDescription;
		private System.Windows.Forms.Label _lblRebateId;
		private System.Windows.Forms.TabPage Detail;
		private System.Windows.Forms.GroupBox _grpDetailProduct;
		private System.Windows.Forms.DataGridView _dgvDetail;
		private System.Windows.Forms.GroupBox _grpDetailSelection;
		private System.Windows.Forms.Button _btnProductDetailClear;
		private UserControls.Rebates.Product _ucProductDetail;
		private System.Windows.Forms.Label _lblAttribute4;
		private System.Windows.Forms.Label _lblAttribute3;
		private System.Windows.Forms.Label _lblAttribute2;
		private System.Windows.Forms.Label _lblAttribute1;
		private UserControls.Rebates.Attribute _ucAttribute4;
		private UserControls.Rebates.Attribute _ucAttribute3;
		private UserControls.Rebates.Attribute _ucAttribute2;
		private UserControls.Rebates.Attribute _ucAttribute1;
		private System.Windows.Forms.Button _btnAddProductDetail;
		private System.Windows.Forms.TabPage VAP;
		private System.Windows.Forms.GroupBox _grpVAPProductSelect;
		private UserControls.Rebates.Product _ucProductVAP;
		private System.Windows.Forms.Button _btnVAPProductAdd;
		private System.Windows.Forms.GroupBox _grpVAPProducts;
		private System.Windows.Forms.DataGridView _dgvVAP;
		private System.Windows.Forms.GroupBox _grpVAPActivation;
		private UserControls.Rebates.VAPActivationMethod _ucVAPActivationMethod;
		private System.Windows.Forms.TextBox _txtVAPActivationQuantity;
		private System.Windows.Forms.Label _lblVAPActivationQuantity;
		private System.Windows.Forms.TabPage SoldFrom;
		private System.Windows.Forms.Button _btnSoldFromInclude;
		private System.Windows.Forms.Button _btnSoldFromExclude;
		private System.Windows.Forms.Label _lblSoldFromExclude;
		private System.Windows.Forms.Label _lblSoldFromInclude;
		private System.Windows.Forms.ListBox _lstSoldFromExclude;
		private System.Windows.Forms.ListBox _lstSoldFromInclude;
		private System.Windows.Forms.TabPage SoldTo;
		private System.Windows.Forms.GroupBox _grpSoldToExcluded;
		private System.Windows.Forms.DataGridView _dgvSoldTo;
		private System.Windows.Forms.GroupBox _grpSoldToSelection;
		private System.Windows.Forms.Button _btnSoldToAdd;
		private UserControls.Rebates.Customer _ucSoldToCustomer;
		private System.Windows.Forms.TabPage ShipTo;
		private System.Windows.Forms.GroupBox _grpShipToExcluded;
		private System.Windows.Forms.DataGridView _dgvShipTo;
		private System.Windows.Forms.GroupBox _grpShipToSelection;
		private System.Windows.Forms.Button _btnShipToAdd;
		private UserControls.Rebates.Customer _ucShipToCustomer;
		private System.Windows.Forms.TabPage Notes;
		private System.Windows.Forms.GroupBox _grpNotes;
		private System.Windows.Forms.DataGridView _dgvNotes;
		private System.Windows.Forms.GroupBox _grpNoteAdd;
		private System.Windows.Forms.Button _btnNoteAdd;
		private System.Windows.Forms.TextBox _txtNote;
		private System.Windows.Forms.Button _btnClose;
		private System.Windows.Forms.Label _lblErrorProductDetail;
		private System.Windows.Forms.Label _lblErrorRebateRange;
		private System.Windows.Forms.Label _lblErrorVAP;
		private System.Windows.Forms.TabPage RebateExtension;
		private System.Windows.Forms.GroupBox _grpDeliveryWindowSelect;
		private UserControls.Rebates.DeliveryWindow _ucDeliveryWindow;
		private System.Windows.Forms.Button _btnAddRebateExtension;
		private System.Windows.Forms.GroupBox _grpDeliveryWindows;
		private System.Windows.Forms.Label _lblErrorDeliveryWindow;
		private System.Windows.Forms.DataGridView _dgvRebateExtension;
		private UserControls.Rebates.PackingType _ucPackingType;
		private UserControls.Rebates.LeadTime _ucLeadTime;
		private UserControls.Rebates.Lookups.PricingSubGroup _ucPricingSubGroup;
		private System.Windows.Forms.Label _lblNotes;
		private System.Windows.Forms.Label _lblNotesLabel;
		private System.Windows.Forms.Label _lblUcCustomer;
		private System.Windows.Forms.CheckBox _chkIsAdjustment;
		private System.Windows.Forms.CheckBox _chkIsJIF;
		private System.Windows.Forms.Label _lblNoteError;
		private System.Windows.Forms.TextBox _txtRate;
		private System.Windows.Forms.Label _lblRate;
		private UserControls.Rebates.Payee _ucPayee;
	}
}
﻿namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	partial class frmPaymentAwaitingAuthorisation
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this._dgvPending = new System.Windows.Forms.DataGridView();
			this._grpInformation = new System.Windows.Forms.GroupBox();
			this._btnReject = new System.Windows.Forms.Button();
			this._btnAuthorise = new System.Windows.Forms.Button();
			this._lblInformation = new System.Windows.Forms.Label();
			this._grpPending = new System.Windows.Forms.GroupBox();
			((System.ComponentModel.ISupportInitialize) (this._dgvPending)).BeginInit();
			this._grpInformation.SuspendLayout();
			this._grpPending.SuspendLayout();
			this.SuspendLayout();
			// 
			// _dgvPending
			// 
			this._dgvPending.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this._dgvPending.Dock = System.Windows.Forms.DockStyle.Fill;
			this._dgvPending.Location = new System.Drawing.Point(3, 16);
			this._dgvPending.Name = "_dgvPending";
			this._dgvPending.Size = new System.Drawing.Size(694, 246);
			this._dgvPending.TabIndex = 0;
			// 
			// _grpInformation
			// 
			this._grpInformation.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this._grpInformation.Controls.Add(this._btnReject);
			this._grpInformation.Controls.Add(this._btnAuthorise);
			this._grpInformation.Controls.Add(this._lblInformation);
			this._grpInformation.Location = new System.Drawing.Point(12, 12);
			this._grpInformation.Name = "_grpInformation";
			this._grpInformation.Size = new System.Drawing.Size(700, 113);
			this._grpInformation.TabIndex = 1;
			this._grpInformation.TabStop = false;
			this._grpInformation.Text = "Authorisation Notes";
			// 
			// _btnReject
			// 
			this._btnReject.Location = new System.Drawing.Point(531, 62);
			this._btnReject.Name = "_btnReject";
			this._btnReject.Size = new System.Drawing.Size(135, 23);
			this._btnReject.TabIndex = 2;
			this._btnReject.Text = "Reject Payments";
			this._btnReject.UseVisualStyleBackColor = true;
			// 
			// _btnAuthorise
			// 
			this._btnAuthorise.Location = new System.Drawing.Point(531, 32);
			this._btnAuthorise.Name = "_btnAuthorise";
			this._btnAuthorise.Size = new System.Drawing.Size(135, 24);
			this._btnAuthorise.TabIndex = 1;
			this._btnAuthorise.Text = "Authorise Payments";
			this._btnAuthorise.UseVisualStyleBackColor = true;
			// 
			// _lblInformation
			// 
			this._lblInformation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
			this._lblInformation.Location = new System.Drawing.Point(26, 36);
			this._lblInformation.Name = "_lblInformation";
			this._lblInformation.Size = new System.Drawing.Size(415, 46);
			this._lblInformation.TabIndex = 0;
			this._lblInformation.Text = "Payments made within a single payment breakdown schedule (ie: they have the same " +
				"Payment Breakdown Schedule Id) must be authorised or rejected as a single block " +
				"of payments";
			// 
			// _grpPending
			// 
			this._grpPending.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this._grpPending.Controls.Add(this._dgvPending);
			this._grpPending.Location = new System.Drawing.Point(12, 131);
			this._grpPending.Name = "_grpPending";
			this._grpPending.Size = new System.Drawing.Size(700, 265);
			this._grpPending.TabIndex = 2;
			this._grpPending.TabStop = false;
			this._grpPending.Text = "Payments Pending Authorisation";
			// 
			// frmPaymentAwaitingAuthorisation
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(724, 408);
			this.Controls.Add(this._grpPending);
			this.Controls.Add(this._grpInformation);
			this.Name = "frmPaymentAwaitingAuthorisation";
			this.Text = "Payments Awaiting Authorisation";
			((System.ComponentModel.ISupportInitialize) (this._dgvPending)).EndInit();
			this._grpInformation.ResumeLayout(false);
			this._grpPending.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.DataGridView _dgvPending;
		private System.Windows.Forms.GroupBox _grpInformation;
		private System.Windows.Forms.Label _lblInformation;
		private System.Windows.Forms.GroupBox _grpPending;
		private System.Windows.Forms.Button _btnAuthorise;
		private System.Windows.Forms.Button _btnReject;
	}
}
﻿namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	partial class frmTaxRate
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this._txtRate = new System.Windows.Forms.TextBox();
			this._lblCode = new System.Windows.Forms.Label();
			this._dtpEffectiveDate = new System.Windows.Forms.DateTimePicker();
			this._lblEffectiveDate = new System.Windows.Forms.Label();
			this._lblRate = new System.Windows.Forms.Label();
			this._pnlData = new System.Windows.Forms.Panel();
			this._pnlControls = new System.Windows.Forms.Panel();
			this._okCancel = new Evolve.Libraries.Controls.Generic.OkCancel();
			this._errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
			this._grpTax = new System.Windows.Forms.GroupBox();
			this._grpJDE = new System.Windows.Forms.GroupBox();
			this._txtJDETaxRate = new System.Windows.Forms.TextBox();
			this._lblJDETaxRate = new System.Windows.Forms.Label();
			this._txtJDEExplanation = new System.Windows.Forms.TextBox();
			this._lblJDEExplanation = new System.Windows.Forms.Label();
			this._ucTaxCode = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Lookups.TaxCode();
			this._pnlData.SuspendLayout();
			this._pnlControls.SuspendLayout();
			((System.ComponentModel.ISupportInitialize) (this._errorProvider)).BeginInit();
			this._grpTax.SuspendLayout();
			this._grpJDE.SuspendLayout();
			this.SuspendLayout();
			// 
			// _txtRate
			// 
			this._txtRate.Location = new System.Drawing.Point(120, 62);
			this._txtRate.Name = "_txtRate";
			this._txtRate.Size = new System.Drawing.Size(50, 20);
			this._txtRate.TabIndex = 5;
			this._txtRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _lblCode
			// 
			this._lblCode.AutoSize = true;
			this._lblCode.Location = new System.Drawing.Point(77, 23);
			this._lblCode.Name = "_lblCode";
			this._lblCode.Size = new System.Drawing.Size(32, 13);
			this._lblCode.TabIndex = 0;
			this._lblCode.Text = "Code";
			// 
			// _dtpEffectiveDate
			// 
			this._dtpEffectiveDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this._dtpEffectiveDate.Location = new System.Drawing.Point(120, 41);
			this._dtpEffectiveDate.Name = "_dtpEffectiveDate";
			this._dtpEffectiveDate.Size = new System.Drawing.Size(95, 20);
			this._dtpEffectiveDate.TabIndex = 3;
			// 
			// _lblEffectiveDate
			// 
			this._lblEffectiveDate.AutoSize = true;
			this._lblEffectiveDate.Location = new System.Drawing.Point(35, 44);
			this._lblEffectiveDate.Name = "_lblEffectiveDate";
			this._lblEffectiveDate.Size = new System.Drawing.Size(75, 13);
			this._lblEffectiveDate.TabIndex = 2;
			this._lblEffectiveDate.Text = "Effective Date";
			// 
			// _lblRate
			// 
			this._lblRate.AutoSize = true;
			this._lblRate.Location = new System.Drawing.Point(79, 65);
			this._lblRate.Name = "_lblRate";
			this._lblRate.Size = new System.Drawing.Size(30, 13);
			this._lblRate.TabIndex = 4;
			this._lblRate.Text = "Rate";
			// 
			// _pnlData
			// 
			this._pnlData.Controls.Add(this._grpJDE);
			this._pnlData.Controls.Add(this._grpTax);
			this._pnlData.Dock = System.Windows.Forms.DockStyle.Top;
			this._pnlData.Location = new System.Drawing.Point(0, 0);
			this._pnlData.Name = "_pnlData";
			this._pnlData.Size = new System.Drawing.Size(354, 209);
			this._pnlData.TabIndex = 9;
			// 
			// _pnlControls
			// 
			this._pnlControls.Controls.Add(this._okCancel);
			this._pnlControls.Dock = System.Windows.Forms.DockStyle.Bottom;
			this._pnlControls.Location = new System.Drawing.Point(0, 187);
			this._pnlControls.Name = "_pnlControls";
			this._pnlControls.Size = new System.Drawing.Size(354, 52);
			this._pnlControls.TabIndex = 0;
			// 
			// _okCancel
			// 
			this._okCancel.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._okCancel.CausesValidation = false;
			this._okCancel.Location = new System.Drawing.Point(185, 16);
			this._okCancel.Name = "_okCancel";
			this._okCancel.Size = new System.Drawing.Size(157, 24);
			this._okCancel.TabIndex = 0;
			// 
			// _errorProvider
			// 
			this._errorProvider.ContainerControl = this;
			// 
			// _grpTax
			// 
			this._grpTax.Controls.Add(this._ucTaxCode);
			this._grpTax.Controls.Add(this._dtpEffectiveDate);
			this._grpTax.Controls.Add(this._lblCode);
			this._grpTax.Controls.Add(this._txtRate);
			this._grpTax.Controls.Add(this._lblRate);
			this._grpTax.Controls.Add(this._lblEffectiveDate);
			this._grpTax.Dock = System.Windows.Forms.DockStyle.Top;
			this._grpTax.Location = new System.Drawing.Point(0, 0);
			this._grpTax.Name = "_grpTax";
			this._grpTax.Size = new System.Drawing.Size(354, 100);
			this._grpTax.TabIndex = 0;
			this._grpTax.TabStop = false;
			this._grpTax.Text = "Rate Details";
			// 
			// _grpJDE
			// 
			this._grpJDE.Controls.Add(this._txtJDEExplanation);
			this._grpJDE.Controls.Add(this._lblJDEExplanation);
			this._grpJDE.Controls.Add(this._txtJDETaxRate);
			this._grpJDE.Controls.Add(this._lblJDETaxRate);
			this._grpJDE.Dock = System.Windows.Forms.DockStyle.Fill;
			this._grpJDE.Location = new System.Drawing.Point(0, 100);
			this._grpJDE.Name = "_grpJDE";
			this._grpJDE.Size = new System.Drawing.Size(354, 109);
			this._grpJDE.TabIndex = 1;
			this._grpJDE.TabStop = false;
			this._grpJDE.Text = "JDE Details";
			// 
			// _txtJDETaxRate
			// 
			this._txtJDETaxRate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this._txtJDETaxRate.Location = new System.Drawing.Point(120, 28);
			this._txtJDETaxRate.MaxLength = 10;
			this._txtJDETaxRate.Name = "_txtJDETaxRate";
			this._txtJDETaxRate.Size = new System.Drawing.Size(95, 20);
			this._txtJDETaxRate.TabIndex = 2;
			// 
			// _lblJDETaxRate
			// 
			this._lblJDETaxRate.AutoSize = true;
			this._lblJDETaxRate.Location = new System.Drawing.Point(12, 31);
			this._lblJDETaxRate.Name = "_lblJDETaxRate";
			this._lblJDETaxRate.Size = new System.Drawing.Size(102, 13);
			this._lblJDETaxRate.TabIndex = 1;
			this._lblJDETaxRate.Text = "JDE Tax Rate Code";
			// 
			// _txtJDEExplanation
			// 
			this._txtJDEExplanation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this._txtJDEExplanation.Location = new System.Drawing.Point(120, 50);
			this._txtJDEExplanation.MaxLength = 2;
			this._txtJDEExplanation.Name = "_txtJDEExplanation";
			this._txtJDEExplanation.Size = new System.Drawing.Size(20, 20);
			this._txtJDEExplanation.TabIndex = 0;
			// 
			// _lblJDEExplanation
			// 
			this._lblJDEExplanation.AutoSize = true;
			this._lblJDEExplanation.Location = new System.Drawing.Point(52, 53);
			this._lblJDEExplanation.Name = "_lblJDEExplanation";
			this._lblJDEExplanation.Size = new System.Drawing.Size(62, 13);
			this._lblJDEExplanation.TabIndex = 3;
			this._lblJDEExplanation.Text = "Explanation";
			// 
			// _ucTaxCode
			// 
			this._ucTaxCode.IsZeroRateOnly = false;
			this._ucTaxCode.Location = new System.Drawing.Point(120, 19);
			this._ucTaxCode.Name = "_ucTaxCode";
			this._ucTaxCode.Size = new System.Drawing.Size(152, 21);
			this._ucTaxCode.TabIndex = 1;
			// 
			// frmTaxRate
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(354, 239);
			this.Controls.Add(this._pnlControls);
			this.Controls.Add(this._pnlData);
			this.Name = "frmTaxRate";
			this.Text = "Tax Rate";
			this._pnlData.ResumeLayout(false);
			this._pnlControls.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize) (this._errorProvider)).EndInit();
			this._grpTax.ResumeLayout(false);
			this._grpTax.PerformLayout();
			this._grpJDE.ResumeLayout(false);
			this._grpJDE.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TextBox _txtRate;
		private System.Windows.Forms.Label _lblCode;
		private System.Windows.Forms.DateTimePicker _dtpEffectiveDate;
		private System.Windows.Forms.Label _lblEffectiveDate;
		private System.Windows.Forms.Label _lblRate;
		private Libraries.Controls.Generic.OkCancel _okCancel;
		private System.Windows.Forms.Panel _pnlData;
		private System.Windows.Forms.Panel _pnlControls;
		private System.Windows.Forms.ErrorProvider _errorProvider;
		private UserControls.Rebates.Lookups.TaxCode _ucTaxCode;
		private System.Windows.Forms.GroupBox _grpJDE;
		private System.Windows.Forms.TextBox _txtJDEExplanation;
		private System.Windows.Forms.Label _lblJDEExplanation;
		private System.Windows.Forms.TextBox _txtJDETaxRate;
		private System.Windows.Forms.Label _lblJDETaxRate;
		private System.Windows.Forms.GroupBox _grpTax;
	}
}
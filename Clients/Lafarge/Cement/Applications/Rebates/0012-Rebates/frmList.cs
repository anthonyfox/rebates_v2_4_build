﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;
using System.Reflection;

namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	public interface IListMaintenance
	{
		Entity DataItem { get; }
	}

	public partial class frmList : Form
	{
		private IBindingList _dataList;
		private Type _formType;

		public IBindingList DataList
		{
			get { return _dataList; }
			set 
			{
				if (value != null)
				{
					_dataType = value.GetType().GetGenericArguments()[0];

					if (_dataType.IsSubclassOf(typeof(Entity)))
					{
						String formTypeName = this.GetType().Namespace + ".frm" + _dataType.Name;
						_formType = Type.GetType(formTypeName);

						if (_formType == null)
						{
							throw new ArgumentException("Missing object type " + formTypeName);
						}

						if (_formType.BaseType != typeof(Form) || _formType.GetInterface(typeof(IListMaintenance).FullName) == null)
						{
							throw new ArgumentException("Object must be of type Form and implement IListMaintenance");
						}

						_dataList = value;
						BindData();
					}
					else
					{
						throw new ArgumentException("Generic member of DataList must be of type Entity");
					}
				}
			}
		}

		private BindingSource _bindingSource = new BindingSource();
		private Type _dataType;

		public frmList(String title)
		{
			InitializeComponent();

			// Initial setup
			this.Text = title;

			// Initialise events
			bindingNavigatorAddNewItem.Click += new EventHandler(bindingNavigatorAddNewItem_Click);
			bindingNavigatorDeleteItem.Click += new EventHandler(bindingNavigatorDeleteItem_Click);
			_dataGridView.DoubleClick += new EventHandler(_dataGridView_DoubleClick);
			toolStripButtonRefresh.Click += new EventHandler(toolStripButtonRefresh_Click);

			_dataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
		}

		private void BindData()
		{
			// Bind data
			_bindingSource.DataSource = _dataList;
			_bindingNavigator.BindingSource = _bindingSource;
			_dataGridView.DataSource = _bindingSource;

			_dataGridView.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
		}

		void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
		{
			if (_formType != null)
			{
				Form form = (Form) Activator.CreateInstance(_formType, new Object[] { null });
				form.Icon = this.Icon;
					
				if (form.ShowDialog() == DialogResult.OK)
				{
					_dataList.Add(((IListMaintenance) form).DataItem);
				}
			}
		}

		void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
		{
			PropertyInfo propertyInfo = _dataType.GetProperty("CanDelete");
			Entity entity = (Entity) _dataGridView.CurrentRow.DataBoundItem;

			if (propertyInfo != null)
			{
				if ((Boolean) propertyInfo.GetValue(entity, null))
				{
					Boolean retry = false;
					Boolean isRemoved = false;

					do
					{
						try
						{
							entity.Delete();
							isRemoved = true;
						}
						catch (DatabaseException databaseException)
						{
							if (databaseException.IsLockError)
							{
								retry = MessageBox.Show("Tax Rate record is locked, retry", "Record Locked", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes;
							}
							else
							{
								throw databaseException;
							}
						}
					} while (retry);

					if (isRemoved)
					{
						_dataList.Remove(entity);
					}
				}
				else
				{
					MessageBox.Show("Tax code has rates associated with it", "Cannot Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		void _dataGridView_DoubleClick(object sender, EventArgs e)
		{
			Entity entity = (Entity) _dataGridView.CurrentRow.DataBoundItem;

			if (_formType != null && entity != null)
			{
				Form form = (Form) Activator.CreateInstance(_formType, new Object[] { entity });
				form.Icon = this.Icon;

				if (form.ShowDialog() == DialogResult.OK)
				{
					_dataList[_dataList.IndexOf(entity)] = ((IListMaintenance) form).DataItem;
				}
			}
		}

		void toolStripButtonRefresh_Click(object sender, EventArgs e)
		{
			Entity entity = (Entity) Activator.CreateInstance(_dataType);
			_dataList = entity.Read();
			BindData();
		}
	}
}

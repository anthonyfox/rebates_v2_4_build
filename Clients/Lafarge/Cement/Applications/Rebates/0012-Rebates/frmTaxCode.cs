﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Libraries.Data.Remoting;
using System.Reflection;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	public partial class frmTaxCode : Form, IListMaintenance
	{
		private TaxCode _original;
		private TaxCode _taxCode;

		public Entity DataItem
		{
			get { return _taxCode; }
		}

		public frmTaxCode(TaxCode taxCode)
		{
			InitializeComponent();

			// Check and setup data
			if (taxCode == null)
			{
				_taxCode = new TaxCode();
			}
			else
			{
				_original = taxCode;
				_taxCode = (TaxCode) taxCode.Clone();
			}

			// Check if we can set zero rate flag
			TaxCode taxCodeZero = new TaxCode();
			taxCodeZero.IsZeroRate = true;
			taxCodeZero.AddReadFilter(TaxCodeProperty.IsZeroRate, "=");
			_chkIsZeroRate.Enabled = (taxCodeZero.ReadCount() == 0 || _taxCode.IsZeroRate);

			// Data bind
			_txtCode.DataBindings.Add("Text", _taxCode, TaxCodeProperty.Code);
			_chkIsZeroRate.DataBindings.Add("Checked", _taxCode, TaxCodeProperty.IsZeroRate);

			// Create events
			_okCancel.ButtonClick += new Libraries.Controls.Generic.OkCancel.ButtonClickHandler(_okCancel_ButtonClick);
			_txtCode.Validating += new CancelEventHandler(_txtCode_Validating);
		}

		void _okCancel_ButtonClick(object sender, EventArgs e)
		{
			this.ValidateChildren();

			if (_okCancel.DialogResult == DialogResult.OK)
			{
				if (!ValidateControl.HasErrors(this, _errorProvider) && Save())
				{
					this.DialogResult = _okCancel.DialogResult;
				}
			}
			else
			{
				this.Close();
			}			
		}

		void _txtCode_Validating(object sender, CancelEventArgs e)
		{
			Control control = (Control) sender;
			_errorProvider.SetError(control, ValidAndDistinct(control.Text, control.Name.Substring(4)));
		}

		private String ValidAndDistinct(String text, String propertyName)
		{
			String error = "";

			if (String.IsNullOrEmpty(text))
			{
				error = "Must be specified";
			}
			else
			{
				TaxCode taxCode = new TaxCode();
				taxCode.Code = text;

				ExtendedBindingList<TaxCode> taxCodes = (ExtendedBindingList<TaxCode>) taxCode.ReadWithDirtyProperty();
				Boolean isUnique = (taxCodes.Count == 0 || _original != null && taxCodes.Count == 1 && taxCodes[0].Id == _original.Id);
				
				if (!isUnique)
				{
					error = "Code already must be unique";
				}
			}

			return error;
		}

		private Boolean Save()
		{
			Boolean doRetry = false;
			Boolean isSaved = false;

			do
			{
				try
				{
					_taxCode.AcceptChanges();
					isSaved = true;
				}
				catch (DatabaseException databaseException)
				{
					if (databaseException.IsLockError)
					{
						doRetry = MessageBox.Show("Record is locked by another user", "Record Locked", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes;
					}
					else
					{
						throw databaseException;
					}
				}
			} while (doRetry);

			return isSaved;
		}
	}
}

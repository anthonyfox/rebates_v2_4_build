﻿using System;
using System.Windows.Forms;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	public partial class frmShowInvoices : Form
	{
		public frmShowInvoices(Int32 accrualDetailId)
		{
			InitializeComponent();
			LoadInvoices(accrualDetailId);

			this.Load += new EventHandler(frmShowInvoices_Load);
			this.Resize += new EventHandler(frmShowInvoices_Resize);
			_dgvInvoices.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			_dgvInvoices.DataError += new DataGridViewDataErrorEventHandler(DataGridViewUtilities.DataError);
		}

		void frmShowInvoices_Load(object sender, EventArgs e)
		{
			if (this.MdiParent != null)
			{
				this.Icon = this.MdiParent.Icon;
			}
		}

		void frmShowInvoices_Resize(object sender, EventArgs e)
		{
			_dgvInvoices.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
		}

		private void LoadInvoices(Int32 accrualDetailId)
		{
			AccrualDetailInvoice accrualDetailInvoice = new AccrualDetailInvoice();

			accrualDetailInvoice.AccrualDetailId = accrualDetailId;
			accrualDetailInvoice.AddReadFilter(AccrualDetailInvoiceProperty.AccrualDetailId, "=");

			_dgvInvoices.DataSource = (ExtendedBindingList<AccrualDetailInvoice>) accrualDetailInvoice.Read();
		}
	}
}

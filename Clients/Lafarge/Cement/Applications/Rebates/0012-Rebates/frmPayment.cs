﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	public partial class frmPayment : Form
	{
		private PaymentSummary _ps;
		private Boolean _isReadOnly = false;
		private CalculationPeriod _payThruPeriod;
		private frmPaymentBreakdown _frmPaymentBreakdown;
		private frmPaymentAwaitingAuthorisation _frmPending;
		private String _numberFormat = "#,0.00";
		private String _percentFormat = "0.0";
		private String _textBoxBeforeText = "";

		public Boolean IsReadOnly
		{
			get
			{
				return _isReadOnly;
			}
			set
			{
				_isReadOnly = value;

				_lnkQuickPay.Enabled = !_isReadOnly;
				_lblQuickPayAmount.Visible = !_isReadOnly;
				_lblQuickPayPercentage.Visible = !_isReadOnly;
				_lblQuickPayPercent.Visible = !_isReadOnly;
				_txtQuickPayAmount.Visible = !_isReadOnly;
				_txtQuickPayPercent.Visible = !_isReadOnly;
				_btnPayeeBreakdown.Visible = !_isReadOnly;
				_ucRebatePaymentSummary.IsReadOnly = _isReadOnly;
			}
		}

		public frmPayment(ExtendedBindingList<Rebate> rebates, CalculationPeriod payThruPeriod, Boolean showAll)
		{
			InitializeComponent();

			_payThruPeriod = payThruPeriod;

			// Setup control counters and positioning parameters
			Int32 hierarchyLevel = -1;
			String hierarchyCode = "";
			TreeNode hierarchyLevelNode = null;
			TreeNode hierarchyCodeNode = null;
			TreeNode firstNode = null;

			// Create payment summary
			_ps = new PaymentSummary(rebates, showAll, Program.User, Program.Division, Program.Division.LastAccrualPeriod(), payThruPeriod);

			// Create control for each rebate
			foreach (RebatePaymentSummary rps in _ps.List)
			{
				// Tree view
				if (rps.HierarchyLevel != hierarchyLevel)
				{
					hierarchyLevelNode = new TreeNode(rps.HierarchyName);
					_treeView.Nodes.Add(hierarchyLevelNode);
					hierarchyLevel = rps.HierarchyLevel;
				}

				if (rps.HierarchyCode != hierarchyCode)
				{
					hierarchyCodeNode = new TreeNode(rps.HierarchyCode + " - " + rps.HierarchyCodeName);
					hierarchyLevelNode.Nodes.Add(hierarchyCodeNode);
					hierarchyCode = rps.HierarchyCode;
				}

				TreeNode rebateNode = new TreeNode(TreeNodeText(rps));
				rebateNode.Tag = rps;

				if (firstNode == null)
				{
					firstNode = rebateNode;
				}

				hierarchyCodeNode.Nodes.Add(rebateNode);
			}

			_ucRebatePaymentSummary.IsReadOnly = _isReadOnly;
			_treeView.SelectedNode = firstNode;
			_treeView.ExpandAll();

			// Bind summary data
			_txtPayThru.DataBindings.Add("Text", _ps, "PayThruPeriod");
			_txtAccrued.DataBindings.Add("Text", _ps, "Accrued", true, DataSourceUpdateMode.OnPropertyChanged, null, _numberFormat);
			_txtPending.DataBindings.Add("Text", _ps, "Pending", true, DataSourceUpdateMode.OnPropertyChanged, null, _numberFormat);
			_txtPaid.DataBindings.Add("Text", _ps, "Paid", true, DataSourceUpdateMode.OnPropertyChanged, null, _numberFormat);
			_txtClosed.DataBindings.Add("Text", _ps, "Closed", true, DataSourceUpdateMode.OnPropertyChanged, null, _numberFormat);
			_txtCarriedForward.DataBindings.Add("Text", _ps, "CarriedForward", true, DataSourceUpdateMode.OnPropertyChanged, null, _numberFormat);

			_txtProposed.DataBindings.Add("Text", _ps, "Proposed", true, DataSourceUpdateMode.OnPropertyChanged, null, _numberFormat);
			_txtClose.DataBindings.Add("Text", _ps, "Closing", true, DataSourceUpdateMode.OnPropertyChanged, null, _numberFormat);
			_txtBalance.DataBindings.Add("Text", _ps, "Balance", true, DataSourceUpdateMode.OnPropertyChanged, null, _numberFormat);

			// Create event handlers
			_lnkQuickPay.Click += new EventHandler(_lnkQuickPay_Click);
			_lnkPending.Click += new EventHandler(_lnkPending_Click);
			_txtQuickPayAmount.GotFocus += new EventHandler(_textBox_GotFocus);
			_txtQuickPayAmount.Leave += new EventHandler(_txtQuickPayAmount_Leave);
			_txtQuickPayPercent.GotFocus += new EventHandler(_textBox_GotFocus);
			_txtQuickPayPercent.Leave += new EventHandler(_txtQuickPayPercent_Leave);
			_btnClear.Click += new EventHandler(_btnClear_Click);
			_btnPayeeBreakdown.Click += new EventHandler(_btnPayeeBreakdown_Click);
			_treeView.AfterSelect += new TreeViewEventHandler(_treeView_AfterSelect);
			_treeView.NodeMouseDoubleClick += new TreeNodeMouseClickEventHandler(_treeView_NodeMouseDoubleClick);
			_ucRebatePaymentSummary.PaymentAmountChanged += new UserControls.Rebates.RebatePaymentSummary.PaymentAmountChangedHandler(_ucRebatePaymentSummary_PaymentAmountChanged);

			this.Load += new EventHandler(frmPayment_Load);
			this.FormClosing += new FormClosingEventHandler(frmPayment_FormClosing);
		}

		void _treeView_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
		{
			RebatePaymentSummary rebatePaymentSummary = (RebatePaymentSummary) e.Node.Tag;

			if (rebatePaymentSummary != null)
			{
				this.Cursor = Cursors.WaitCursor;

				frmDisplayAccrual frmDisplayAccrual = new Rebates.frmDisplayAccrual(AccrualTypeEnum.Actual, _payThruPeriod, false, rebatePaymentSummary.RebateId);
				frmDisplayAccrual.MdiParent = this.MdiParent;
				frmDisplayAccrual.Show();

				this.Cursor = Cursors.Default;
			}
		}

		#region Event Handlers
		
		void _lnkQuickPay_Click(object sender, EventArgs e)
		{
			if (_ps.Balance > 0)
			{
				foreach (RebatePaymentSummary rps in _ps.List)
				{
					Decimal paymentValue = rps.Proposed.Total + rps.Balance.Total;
					rps.MakePaymentOrClosure(paymentValue, PaymentTypeEnum.Payment);
				}
			}

			_txtQuickPayAmount.Text = _ps.Proposed.ToString(_numberFormat);
			UpdateTreeViewBalances();
		}

		void _lnkPending_Click(object sender, EventArgs e)
		{
			if (_frmPending == null)
			{
				frmMDIParent frmMDIParent = (frmMDIParent) this.MdiParent;
				Boolean isNew = false;

				_frmPending = (frmPaymentAwaitingAuthorisation) frmMDIParent.OpenSingleInstance(typeof(frmPaymentAwaitingAuthorisation), out isNew);
				_frmPending.MdiParent = this.MdiParent;

				if (isNew)
				{
					_frmPending.PaymentStatusChanged += new frmPaymentAwaitingAuthorisation.PaymentStatusChangedEventHandler(_frmPending_PaymentStatusChanged);
				}

				_frmPending.Disposed += new EventHandler(_frmPending_Disposed);
			}

			_frmPending.Show();
		}

		void _frmPending_PaymentStatusChanged(object sender, EventArgs e)
		{
			ProcessPaymentStatusChanged();
		}

		public void ProcessPaymentStatusChanged()
		{
			_ps.ProcessRebatePayments();
			UpdateTreeViewBalances();
		}

		void _frmPending_Disposed(object sender, EventArgs e)
		{
			_frmPending = null;
		}

		void _textBox_GotFocus(object sender, EventArgs e)
		{
			TextBox textBox = (TextBox) sender;
			_textBoxBeforeText = textBox.Text;
		}

		void _txtQuickPayAmount_Leave(object sender, EventArgs e)
		{
			if (_txtQuickPayAmount.Text != "")
			{
				Decimal amount;

				if (!Decimal.TryParse(_txtQuickPayAmount.Text, out amount))
				{
					_txtQuickPayAmount.Text = _textBoxBeforeText;
				}
				else
				{
					// Ok a valid payment amount has been entered, process blick payment
					_txtQuickPayAmount.Text = amount.ToString(_numberFormat);

					if (_txtQuickPayAmount.Text != _textBoxBeforeText)
					{
						_txtQuickPayPercent.Text = "";

						// Proportion payment evenly across all rebates (with an amount outstanding)
						Int32 count = _ps.List.Count(rps => rps.Balance.Total > 0);

						if (count > 0)
						{
							Decimal payment = Decimal.Round(amount / count, 3);

							// Calculate any rounding difference
							Decimal rounding = amount - (payment * count);
							Int32 processed = 0;

							foreach (RebatePaymentSummary rps in _ps.List.Where(rps => rps.Balance.Total > 0))
							{
								processed++;

								// Make sure rounding difference gets assigned to last rebate
								if (processed == count)
								{
									payment += rounding;
								}

								rps.Proposed.Clear();
								rps.MakePaymentOrClosure(payment, PaymentTypeEnum.Payment);
							}
						}
						else
						{
							_txtQuickPayAmount.Text = _textBoxBeforeText;
							MessageBox.Show("No Rebates selected currently have an amount outstanding", "Quick Payment", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
					}
				}
			}

			UpdateTreeViewBalances();
		}

		void _txtQuickPayPercent_Leave(object sender, EventArgs e)
		{
			if (_txtQuickPayPercent.Text != "")
			{
				Decimal percent;

				if (!Decimal.TryParse(_txtQuickPayPercent.Text, out percent) || (percent < 0 || percent > 100))
				{
					_txtQuickPayPercent.Text = _textBoxBeforeText;
				}
				else
				{
					// Ok a valid payment amount has been entered, process blick payment
					_txtQuickPayAmount.Text = "";
					_txtQuickPayPercent.Text = percent.ToString(_percentFormat);

					foreach (RebatePaymentSummary rps in _ps.List)
					{
					    rps.Proposed.Clear();
					    Decimal payment = Decimal.Round(rps.Balance.Total * percent / 100, 3);
						rps.MakePaymentOrClosure(payment, PaymentTypeEnum.Payment);
					}
				}
			}
		
			UpdateTreeViewBalances();
		}

		void rpsControl_DisplayAccrualClick(object sender, EventArgs e)
		{
			Evolve.Clients.Lafarge.Cement.UserControls.Rebates.RebatePaymentSummary rpsControl = (Evolve.Clients.Lafarge.Cement.UserControls.Rebates.RebatePaymentSummary) sender;
			RebatePaymentSummary rps = (RebatePaymentSummary) rpsControl.DataSource;

			frmDisplayAccrual frmDisplayAccrual = new frmDisplayAccrual(AccrualTypeEnum.Actual, _ps.PayThruPeriod, false, rps.RebateId);
			frmDisplayAccrual.Show();
		}

		void _treeView_AfterSelect(object sender, TreeViewEventArgs e)
		{
			RebatePaymentSummary rps = null;

			if (e.Node != null)
			{
				rps = (RebatePaymentSummary) e.Node.Tag;
			}

			_ucRebatePaymentSummary.DataSource = (RebatePaymentSummary) e.Node.Tag;
		}

		void _ucRebatePaymentSummary_PaymentAmountChanged(object sender, EventArgs e)
		{
			// User has entered either a proposed payment or closure amount in the Rebate Payment Summary
			// user control, the treeview node needs its balance updating accordingly
			if (_treeView.SelectedNode != null)
			{
				UpdateNode(_treeView.SelectedNode);
			}
		}

		void frmPayment_Load(object sender, EventArgs e)
		{
			if (this.MdiParent != null)
			{
				this.Icon = this.MdiParent.Icon;
			}
		}

		void frmPayment_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (_ps.Proposed != 0 || _ps.Closing != 0)
			{
				DialogResult result = MessageBox.Show("Unsaved payment details exist\nAre you sure you wish to exit", "Unsaved Payments", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);

				if (result != DialogResult.Yes)
				{
					e.Cancel = true;
				}
			}

			if (_frmPaymentBreakdown != null)
			{
				_frmPaymentBreakdown.Close();
				_frmPaymentBreakdown.Dispose();
				_frmPaymentBreakdown = null;
			}
		}

		void _btnClear_Click(object sender, EventArgs e)
		{
			_txtQuickPayAmount.Text = "";
			_txtQuickPayPercent.Text = "";

			foreach (RebatePaymentSummary rps in _ps.List)
			{
				rps.Proposed.Clear();
			}

			_ps.PaymentBreakdowns.Clear();
			UpdateTreeViewBalances();
		}

		void _btnPayeeBreakdown_Click(object sender, EventArgs e)
		{
			PayeeBreakdown();
		}

		private void PayeeBreakdown()
		{
			if (_frmPaymentBreakdown == null)
			{
				_frmPaymentBreakdown = new frmPaymentBreakdown(_ps);
				_frmPaymentBreakdown.ParentPaymentForm = this;
				_frmPaymentBreakdown.MdiParent = this.MdiParent;
				_frmPaymentBreakdown.Show();
				_frmPaymentBreakdown.Disposed += new EventHandler(_frmPaymentBreakdown_Disposed);
			}
			else
			{
				_frmPaymentBreakdown.Show();
				_frmPaymentBreakdown.Activate();
			}
		}

		void _frmPaymentBreakdown_Disposed(object sender, EventArgs e)
		{
			_frmPaymentBreakdown = null;
		}

		#endregion

		#region Business Logic

		private void UpdateTreeViewBalances()
		{
			foreach (TreeNode node in _treeView.Nodes)
			{
				UpdateNodes(node);
			}
		}

		private void UpdateNodes(TreeNode node)
		{
			foreach (TreeNode childNode in node.Nodes)
			{
				UpdateNodes(childNode);
			}

			UpdateNode(node);
		}

		private void UpdateNode(TreeNode node)
		{
			if (node.Tag != null)
			{
				RebatePaymentSummary rps = (RebatePaymentSummary) node.Tag;
				node.Text = TreeNodeText(rps);
			}
		}

		private String TreeNodeText(RebatePaymentSummary rps)
		{
			return rps.RebateNumber.ToString() + " - " + rps.RebateDescription + "  " + rps.Balance.Total.ToString(_numberFormat);
		}

		public Boolean Save()
		{
			String errorMessage = "";

			if (_ps.UnAssignedPaymentAmount == 0)
			{
				// Create payments and closures
				_ps.CreatePayments();

				// Now attempt to save
				Boolean retry = false;

				DataAccessServer.NewPersistentConnection();
				DataAccessServer.PersistentConnection.BeginTransaction();

				do
				{
					try
					{
						Int32 result = 0;

						// Save payments
						result = SavePaymentOrClosures(PaymentTypeEnum.Payment, out errorMessage);

						// Save closures
						if (result >= 0)
						{
							result = SavePaymentOrClosures(PaymentTypeEnum.Close, out errorMessage);
						}

						if (result >= 0)
						{
							DataAccessServer.PersistentConnection.CommitTransaction();

							// Finally complete the payment details in the payment summary
							_ps.SendProposedPaymentsForAuthorisation();
						}
						else
						{
							DataAccessServer.PersistentConnection.RollbackTransaction();
						}

						DataAccessServer.TerminatePersistentConnection();
					}
					catch (DatabaseException databaseException)
					{
						DataAccessServer.PersistentConnection.RollbackTransaction();
						DataAccessServer.TerminatePersistentConnection();

						if (databaseException.IsLockError)
						{
							if (MessageBox.Show("Database is locked, do you wish to retry ?", "Database Locked", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
							{
								retry = true;
							}
						}
						else
						{
							throw databaseException;
						}
					}
				} while (retry);

				if (errorMessage.Length > 0)
				{
					MessageBox.Show("Failed to save changes\n" + errorMessage, "Save", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
			else
			{
				errorMessage = "You have not allocated some or all of the payments to Payee accounts";
				MessageBox.Show(errorMessage, "Payment Details Incomplete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				PayeeBreakdown();
			}

			return (errorMessage.Length == 0);
		}

		private Int32 SavePaymentOrClosures(PaymentTypeEnum paymentTypeEnum, out String errorMessage)
		{
			Int32 result = 0;
			Int32 payment_PaymentBreakdownId = -1;

			errorMessage = "";

			if (_ps.Payments.Count(p => p.PaymentTypeEnum == paymentTypeEnum) > 0)
			{
				// First create a Payment, PaymentBreakdown link record
				Payment_PaymentBreakdown payment_PaymentBreakdown = new Payment_PaymentBreakdown();

				payment_PaymentBreakdown.DivisionId = Program.Division.Id;
				payment_PaymentBreakdown.CreateUserId = Program.User.Id;
				payment_PaymentBreakdown.CreateTime = DateTime.Now;
				payment_PaymentBreakdown.PaymentStatusEnum = (paymentTypeEnum == PaymentTypeEnum.Payment ? PaymentStatusEnum.PendingAuthorisation : PaymentStatusEnum.Authorised);

				result = payment_PaymentBreakdown.AcceptChanges();

				if (result < 0)
				{
					errorMessage = "Failed to create Payment_PaymentBreakdown (" + paymentTypeEnum.ToString() + ")";
				}
				else
				{
					payment_PaymentBreakdownId = result;
				}

				// Now store the payments
				if (result >= 0)
				{
					foreach (Payment payment in _ps.Payments.Where(p => p.PaymentTypeEnum == paymentTypeEnum))
					{
						payment.Payment_PaymentBreakdownId = payment_PaymentBreakdownId;
						result = payment.AcceptChanges();

						if (result < 0)
						{
							errorMessage = "Failed to insert payment (Rebate Id: " + payment.RebateId.ToString() + ", " + paymentTypeEnum.ToString() + ")";
							break;
						}
					}
				}

				// Finally store the payment breakdown information
				if (result >= 0 && paymentTypeEnum == PaymentTypeEnum.Payment)
				{
					foreach (PaymentBreakdown paymentBreakdown in _ps.PaymentBreakdowns)
					{
						paymentBreakdown.Payment_PaymentBreakdownId = payment_PaymentBreakdownId;
						result = paymentBreakdown.AcceptChanges();

						if (result < 0)
						{
							errorMessage = "Failed to insert payment breakdown (Payee Id: " + paymentBreakdown.PayeeId + ")";
							break;
						}
					}
				}
			}

			return result;
		}

		#endregion
	}
}

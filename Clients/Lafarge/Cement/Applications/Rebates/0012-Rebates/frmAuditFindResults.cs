﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using ClosedXML.Excel;
using System.Windows.Forms;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Forms;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Evolve.Clients.Lafarge.Cement.Rebates
{
    public partial class frmAuditFindResults : Form
    {
        private BindingSource _bsAuditHeader;
        private BindingSource _bsAuditDetail;
        private AuditFind _auditFind;
        private EntityListView<AuditHeader> _auditHeaderView;
        private DatabaseActionEnum _databaseActionEnum;
        public frmAuditFindResults(AuditFind auditFind)
        {
            InitializeComponent();

            // Store reference to the search form
            _auditFind = auditFind;

            // Store reference to the results of the search
            _auditHeaderView = new EntityListView<AuditHeader>(_auditFind.AuditHeaders);

            #region Header

            // Create a binding source
            _bsAuditHeader = new BindingSource();
            _bsAuditHeader.DataSource = _auditHeaderView;

            // Bind the binding navigator
            _bnAuditHeader.BindingSource = _bsAuditHeader;

            // Bind the date grid view. Make it single row selection and readonly
            _dgvAuditHeader.DataSource = _bsAuditHeader;
            _dgvAuditHeader.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            _dgvAuditHeader.MultiSelect = false;
            _dgvAuditHeader.ReadOnly = true;

            // Bind the events to the data grid view
            _dgvAuditHeader.DataError += new DataGridViewDataErrorEventHandler(DataGridViewUtilities.DataError);
            _dgvAuditHeader.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(_dgvAuditHeader_DataBindingComplete);
            _dgvAuditHeader.SelectionChanged += new EventHandler(_dgvAuditHeader_SelectionChanged);

            #endregion

            #region Detail

            // Create a binding source (without a datasource at present)
            _bsAuditDetail = new BindingSource();

            // Bind the binding navigator
            _bnAuditDetail.BindingSource = _bsAuditDetail;

            // Bind the date grid view. Make it single row selection and readonly
            _dgvAuditDetail.DataSource = _bsAuditDetail;
            _dgvAuditDetail.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            _dgvAuditDetail.MultiSelect = false;
            _dgvAuditDetail.ReadOnly = true;

            // Bind the events to the data grid view
            _dgvAuditDetail.DataError += new DataGridViewDataErrorEventHandler(DataGridViewUtilities.DataError);
            _dgvAuditDetail.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(_dgvAuditDetail_DataBindingComplete);

            #endregion

            this.Load += new EventHandler(frmAuditFindResults_Load);
        }

        private void _dgvAuditHeader_SelectionChanged(object sender, EventArgs e)
        {
            var dataGridView = (DataGridView)sender;

            if (dataGridView.SelectedRows.Count > 0)
            {
                // Get the selected row
                var row = dataGridView.SelectedRows[0];

                // Read the audit heade for that row
                var auditHeader = (AuditHeader)row.DataBoundItem;

                // Save the dtabase action (Insert/Update/Delete) for that row
                _databaseActionEnum = auditHeader.DatabaseActionEnum;

                // Point the binding source's data source to the collection od audit details for this row
                _bsAuditDetail.DataSource = auditHeader.AuditDetails;

                // Auto size the columns (including the headers)
                dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            }
            else
            {
                _databaseActionEnum = DatabaseActionEnum.Null;
            }
        }

        void frmAuditFindResults_Load(object sender, EventArgs e)
        {
            if (this.MdiParent != null)
            {
                this.Icon = this.MdiParent.Icon;
            }
        }

        void _dgvAuditHeader_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)

        {
            DataGridView dataGridView = (DataGridView)sender;

            // Disable cell wrapping to prevent the headers from wrapping
            dataGridView.ColumnHeadersDefaultCellStyle.WrapMode = DataGridViewTriState.False;

            // Format the numeric fields to align right
            DataGridViewUtilities.SetColumnFormat(dataGridView.Columns[AuditHeaderProperty.Id], "#######", DataGridViewContentAlignment.MiddleRight);
            DataGridViewUtilities.SetColumnFormat(dataGridView.Columns[AuditHeaderProperty.RecordId], "#######", DataGridViewContentAlignment.MiddleRight);
            DataGridViewUtilities.SetColumnFormat(dataGridView.Columns[AuditHeaderProperty.RebateId], "#######", DataGridViewContentAlignment.MiddleRight);
            DataGridViewUtilities.SetColumnFormat(dataGridView.Columns[AuditHeaderProperty.RebateNumber], "#######", DataGridViewContentAlignment.MiddleRight);
            DataGridViewUtilities.SetColumnFormat(dataGridView.Columns[AuditHeaderProperty.RebateVersion], "#######", DataGridViewContentAlignment.MiddleRight);

            // Auto size the columns (including the headers)
            dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
        }

        void _dgvAuditDetail_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)

        {
            DataGridView dataGridView = (DataGridView)sender;

            // Disable cell wrapping to prevent the headers from wrapping
            dataGridView.ColumnHeadersDefaultCellStyle.WrapMode = DataGridViewTriState.False;

            // Decide which columns to show
            // Always show Id and ColumnName
            var keepColumns = new List<string>
            {
                AuditDetailProperty.Id,
                AuditDetailProperty.ColumnName
            };

            // Show Old Value on Delete and Update
            if (_databaseActionEnum == DatabaseActionEnum.Delete || _databaseActionEnum == DatabaseActionEnum.Update)
            {
                keepColumns.Add(AuditDetailProperty.OldValue);
            }

            // Show New Value on Insert and Update
            if (_databaseActionEnum == DatabaseActionEnum.Insert || _databaseActionEnum == DatabaseActionEnum.Update)
            {
                keepColumns.Add(AuditDetailProperty.NewValue);
            }

            DataGridViewUtilities.RemoveColumns(dataGridView, keepColumns);

            // Format the numeric fields to align right
            DataGridViewUtilities.SetColumnFormat(dataGridView.Columns[AuditDetailProperty.Id], "#######", DataGridViewContentAlignment.MiddleRight);

            // Auto size the columns (including the headers)
            dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
        }

        #region "CRN0005863 add 'export to excel' button and accompanying export logic"

        private void btnExportToExcel_Click(object sender, EventArgs e)
        {

            //get filename and path
            string strPathAndFile = _GetExportedFilename(".xlsx");

            //Creating DataTable
            DataTable dt = _dtAuditResults();

            //create a new workbook instance
            using (XLWorkbook wb = new XLWorkbook())
            {
                //add the datatable to a worksheet in the workbook
                wb.Worksheets.Add(dt, "Rebates Audit Results");
                wb.ShowGridLines = false;

                //save the workbook
                wb.SaveAs(strPathAndFile);

                //confirmation
                string strConfirmationMessage = "Audit results exported to the 'Rebates Audit Export' folder on your desktop as '";

                //get filename from file path
                int iLastChr = strPathAndFile.LastIndexOf('\\');
                string strFileName = strPathAndFile.Substring(iLastChr + 1);
                strConfirmationMessage += strFileName;
                strConfirmationMessage += "'";

                //show confirmation in a messagebox
                MessageBox.Show(strConfirmationMessage, "Audit Results Exported To Excel", MessageBoxButtons.OK, MessageBoxIcon.Information);

                //open file
                Process.Start(strPathAndFile);

            }
        }

        private void btnExportToPDF_Click(object sender, EventArgs e)
        {
            //get filename and path
            string strPathAndFile = _GetExportedFilename(".pdf");

            //create new itextsharp document in landscape format
            Document document = new Document(PageSize.A4.Rotate(), 10, 10, 10, 10);

            //create instance of pdfwriter
            PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(strPathAndFile, FileMode.Create));
            document.Open();

            //create a table with width of 100%
            PdfPTable table = new PdfPTable(_dgvAuditHeader.Columns.Count);
            table.WidthPercentage = 100;

            //create an datatable instance of the results
            DataTable dt = _dtAuditResults();

            //Set columns names in the pdf file
            for (int k = 0; k < dt.Columns.Count; k++)
            {
                //create a pdfcell
                PdfPCell cell = new PdfPCell(new Phrase(dt.Columns[k].ColumnName));

                //set alignments and background colour
                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                cell.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                cell.BackgroundColor = new iTextSharp.text.BaseColor(0, 124, 191);

                //add cell to table
                table.AddCell(cell);
            }

            //Add values of DataTable in pdf file
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    PdfPCell cell = new PdfPCell(new Phrase(dt.Rows[i][j].ToString()));

                    //Align the cell to the left
                    cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                    cell.VerticalAlignment = PdfPCell.ALIGN_CENTER;

                    //add cell to table
                    table.AddCell(cell);
                }
            }

            //add table to the document
            document.Add(table);

            //close doc when finished
            document.Close();

            //confirmation
            string strConfirmationMessage = "Audit results exported to the 'Rebates Audit Export' folder on your desktop as '";

            //get filename from file path
            int iLastChr = strPathAndFile.LastIndexOf('\\');
            string strFileName = strPathAndFile.Substring(iLastChr + 1);
            strConfirmationMessage += strFileName;
            strConfirmationMessage += "'";

            //show confirmation in a messagebox
            MessageBox.Show(strConfirmationMessage, "Audit Results Exported To PDF", MessageBoxButtons.OK, MessageBoxIcon.Information);

            //open file
            Process.Start(strPathAndFile);

        }

        private static string _GetExportedFilename(string strFileExtension)
        {
            //get the current user's desktop folder
            string strFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\Rebates Audit Export";

            //if the directory doesn't exist, create it
            if (!Directory.Exists(strFolderPath))
            {
                Directory.CreateDirectory(strFolderPath);
            }

            //create filename
            string strFileName = "rebate_audit_" + DateTime.Now.ToString().Replace("/", "").Replace(" ", "_").Replace(":", "_");
            return strFolderPath + "\\" + strFileName + strFileExtension;
        }

        private DataTable _dtAuditResults()
        {

            //Creating DataTable
            DataTable dt = new DataTable();

            //add master column + headers
            foreach (DataGridViewColumn column in _dgvAuditHeader.Columns)
            {
                //default the value type to string
                dt.Columns.Add(column.HeaderText);
            }

            //add additional column + headers for details
            dt.Columns.Add("Detail Id");
            dt.Columns.Add("Column Name");
            dt.Columns.Add("Old Value");
            dt.Columns.Add("New Value");

            //Adding the Rows
            foreach (DataGridViewRow row in _dgvAuditHeader.Rows)
            {

                //progress label
                toolStripLabelExporting.Visible = true;
                toolStripLabelExporting.ForeColor = Color.Orange;
                toolStripLabelExporting.Text = "Please wait .... exporting " + row.Index + " of " + _dgvAuditHeader.Rows.Count + " records ";
                int iPercentage = (row.Index * 100) / _dgvAuditHeader.Rows.Count;
                toolStripLabelExporting.Text += "(" + iPercentage.ToString() + "%)";

                //progress bar
                toolStripProgressBar.Visible = true;
                toolStripProgressBar.Value = row.Index * toolStripProgressBar.Maximum / _dgvAuditHeader.Rows.Count;

                //keep form active in every loop
                Application.DoEvents();

                //dt.Rows.Add();
                //foreach (DataGridViewCell cell in row.Cells)
                //{
                //    dt.Rows[dt.Rows.Count - 1][cell.ColumnIndex] = cell.Value;
                //}

                //*********************************
                // add detail rows
                //*********************************

                // Read the audit heade for that row
                var auditHeader = (AuditHeader)row.DataBoundItem;

                // Save the dtabase action (Insert/Update/Delete) for that row
                _databaseActionEnum = auditHeader.DatabaseActionEnum;

                // Point the binding source's data source to the collection od audit details for this row
                _bsAuditDetail.DataSource = auditHeader.AuditDetails;

                //Adding the detail rows
                foreach (DataGridViewRow rowDetail in _dgvAuditDetail.Rows)
                {
                    ////only do the header for the first row
                    //if (rowDetail.Index == 0)
                    //{
                    //    dt.Rows.Add();
                    //    //Adding the Columns
                    //    foreach (DataGridViewColumn column in _dgvAuditDetail.Columns)
                    //    {
                    //        dt.Rows[dt.Rows.Count - 1][column.Index + 9] = column.HeaderText;
                    //    }
                    //}

                    //add the row
                    dt.Rows.Add();

                    //add master record values to each row
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        dt.Rows[dt.Rows.Count - 1][cell.ColumnIndex] = cell.Value;
                    }

                    //indent columns and add detail record values to each row
                    foreach (DataGridViewCell cellDetail in rowDetail.Cells)
                    {
                        dt.Rows[dt.Rows.Count - 1][cellDetail.ColumnIndex + 11] = cellDetail.Value;
                    }
                }

                //dt.Rows.Add();

                //finalise the total exported
                toolStripLabelExporting.ForeColor = Color.YellowGreen;
                toolStripLabelExporting.Text = _dgvAuditHeader.Rows.Count + " records exported";

                //hide progressbar
                toolStripProgressBar.Visible = false;

                //*********************************

                //end of new functionality
                //tony fox

            }

            return dt;
        }

        #endregion

    }
}

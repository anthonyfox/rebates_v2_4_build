﻿namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	partial class frmPayment
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this._grpSummary = new System.Windows.Forms.GroupBox();
			this._lnkPending = new System.Windows.Forms.LinkLabel();
			this._txtPending = new System.Windows.Forms.TextBox();
			this._lnkQuickPay = new System.Windows.Forms.LinkLabel();
			this._btnClear = new System.Windows.Forms.Button();
			this._lblQuickPayPercent = new System.Windows.Forms.Label();
			this._lblQuickPayPercentage = new System.Windows.Forms.Label();
			this._txtQuickPayPercent = new System.Windows.Forms.TextBox();
			this._lblQuickPayAmount = new System.Windows.Forms.Label();
			this._txtQuickPayAmount = new System.Windows.Forms.TextBox();
			this._btnPayeeBreakdown = new System.Windows.Forms.Button();
			this._lblCurrent = new System.Windows.Forms.Label();
			this._lblPrevious = new System.Windows.Forms.Label();
			this._lblProposed = new System.Windows.Forms.Label();
			this._txtProposed = new System.Windows.Forms.TextBox();
			this._lblClose = new System.Windows.Forms.Label();
			this._txtClose = new System.Windows.Forms.TextBox();
			this._txtBalance = new System.Windows.Forms.TextBox();
			this._txtCarriedForward = new System.Windows.Forms.TextBox();
			this._txtClosed = new System.Windows.Forms.TextBox();
			this._txtPaid = new System.Windows.Forms.TextBox();
			this._txtAccrued = new System.Windows.Forms.TextBox();
			this._lblClosed = new System.Windows.Forms.Label();
			this._lblPaid = new System.Windows.Forms.Label();
			this._lblAccrued = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this._lblCarriedForward = new System.Windows.Forms.Label();
			this._txtPayThru = new System.Windows.Forms.TextBox();
			this._lblPayThru = new System.Windows.Forms.Label();
			this._errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
			this._treeView = new System.Windows.Forms.TreeView();
			this._grpSelection = new System.Windows.Forms.GroupBox();
			this._ucRebatePaymentSummary = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.RebatePaymentSummary();
			this._grpSummary.SuspendLayout();
			((System.ComponentModel.ISupportInitialize) (this._errorProvider)).BeginInit();
			this._grpSelection.SuspendLayout();
			this.SuspendLayout();
			// 
			// _grpSummary
			// 
			this._grpSummary.Controls.Add(this._lnkPending);
			this._grpSummary.Controls.Add(this._txtPending);
			this._grpSummary.Controls.Add(this._lnkQuickPay);
			this._grpSummary.Controls.Add(this._btnClear);
			this._grpSummary.Controls.Add(this._lblQuickPayPercent);
			this._grpSummary.Controls.Add(this._lblQuickPayPercentage);
			this._grpSummary.Controls.Add(this._txtQuickPayPercent);
			this._grpSummary.Controls.Add(this._lblQuickPayAmount);
			this._grpSummary.Controls.Add(this._txtQuickPayAmount);
			this._grpSummary.Controls.Add(this._btnPayeeBreakdown);
			this._grpSummary.Controls.Add(this._lblCurrent);
			this._grpSummary.Controls.Add(this._lblPrevious);
			this._grpSummary.Controls.Add(this._lblProposed);
			this._grpSummary.Controls.Add(this._txtProposed);
			this._grpSummary.Controls.Add(this._lblClose);
			this._grpSummary.Controls.Add(this._txtClose);
			this._grpSummary.Controls.Add(this._txtBalance);
			this._grpSummary.Controls.Add(this._txtCarriedForward);
			this._grpSummary.Controls.Add(this._txtClosed);
			this._grpSummary.Controls.Add(this._txtPaid);
			this._grpSummary.Controls.Add(this._txtAccrued);
			this._grpSummary.Controls.Add(this._lblClosed);
			this._grpSummary.Controls.Add(this._lblPaid);
			this._grpSummary.Controls.Add(this._lblAccrued);
			this._grpSummary.Controls.Add(this.label1);
			this._grpSummary.Controls.Add(this._lblCarriedForward);
			this._grpSummary.Controls.Add(this._txtPayThru);
			this._grpSummary.Controls.Add(this._lblPayThru);
			this._grpSummary.Location = new System.Drawing.Point(12, 12);
			this._grpSummary.Name = "_grpSummary";
			this._grpSummary.Size = new System.Drawing.Size(1205, 150);
			this._grpSummary.TabIndex = 0;
			this._grpSummary.TabStop = false;
			this._grpSummary.Text = "Summary";
			// 
			// _lnkPending
			// 
			this._lnkPending.AutoSize = true;
			this._lnkPending.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
			this._lnkPending.Location = new System.Drawing.Point(204, 55);
			this._lnkPending.Name = "_lnkPending";
			this._lnkPending.Size = new System.Drawing.Size(133, 13);
			this._lnkPending.TabIndex = 26;
			this._lnkPending.TabStop = true;
			this._lnkPending.Text = "Awaiting Authorisation";
			// 
			// _txtPending
			// 
			this._txtPending.Enabled = false;
			this._txtPending.Location = new System.Drawing.Point(343, 52);
			this._txtPending.Name = "_txtPending";
			this._txtPending.Size = new System.Drawing.Size(119, 20);
			this._txtPending.TabIndex = 27;
			this._txtPending.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _lnkQuickPay
			// 
			this._lnkQuickPay.AutoSize = true;
			this._lnkQuickPay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
			this._lnkQuickPay.Location = new System.Drawing.Point(950, 15);
			this._lnkQuickPay.Name = "_lnkQuickPay";
			this._lnkQuickPay.Size = new System.Drawing.Size(65, 13);
			this._lnkQuickPay.TabIndex = 18;
			this._lnkQuickPay.TabStop = true;
			this._lnkQuickPay.Text = "Quick Pay";
			// 
			// _btnClear
			// 
			this._btnClear.Location = new System.Drawing.Point(922, 118);
			this._btnClear.Name = "_btnClear";
			this._btnClear.Size = new System.Drawing.Size(96, 20);
			this._btnClear.TabIndex = 24;
			this._btnClear.Text = "Clear Payments";
			this._btnClear.UseVisualStyleBackColor = true;
			// 
			// _lblQuickPayPercent
			// 
			this._lblQuickPayPercent.AutoSize = true;
			this._lblQuickPayPercent.Location = new System.Drawing.Point(959, 77);
			this._lblQuickPayPercent.Name = "_lblQuickPayPercent";
			this._lblQuickPayPercent.Size = new System.Drawing.Size(15, 13);
			this._lblQuickPayPercent.TabIndex = 23;
			this._lblQuickPayPercent.Text = "%";
			// 
			// _lblQuickPayPercentage
			// 
			this._lblQuickPayPercentage.AutoSize = true;
			this._lblQuickPayPercentage.Location = new System.Drawing.Point(854, 77);
			this._lblQuickPayPercentage.Name = "_lblQuickPayPercentage";
			this._lblQuickPayPercentage.Size = new System.Drawing.Size(62, 13);
			this._lblQuickPayPercentage.TabIndex = 21;
			this._lblQuickPayPercentage.Text = "Percentage";
			// 
			// _txtQuickPayPercent
			// 
			this._txtQuickPayPercent.Location = new System.Drawing.Point(922, 74);
			this._txtQuickPayPercent.Name = "_txtQuickPayPercent";
			this._txtQuickPayPercent.Size = new System.Drawing.Size(36, 20);
			this._txtQuickPayPercent.TabIndex = 22;
			this._txtQuickPayPercent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _lblQuickPayAmount
			// 
			this._lblQuickPayAmount.AutoSize = true;
			this._lblQuickPayAmount.Location = new System.Drawing.Point(875, 55);
			this._lblQuickPayAmount.Name = "_lblQuickPayAmount";
			this._lblQuickPayAmount.Size = new System.Drawing.Size(43, 13);
			this._lblQuickPayAmount.TabIndex = 19;
			this._lblQuickPayAmount.Text = "Amount";
			// 
			// _txtQuickPayAmount
			// 
			this._txtQuickPayAmount.Location = new System.Drawing.Point(922, 52);
			this._txtQuickPayAmount.Name = "_txtQuickPayAmount";
			this._txtQuickPayAmount.Size = new System.Drawing.Size(119, 20);
			this._txtQuickPayAmount.TabIndex = 20;
			this._txtQuickPayAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _btnPayeeBreakdown
			// 
			this._btnPayeeBreakdown.Location = new System.Drawing.Point(1024, 118);
			this._btnPayeeBreakdown.Name = "_btnPayeeBreakdown";
			this._btnPayeeBreakdown.Size = new System.Drawing.Size(111, 20);
			this._btnPayeeBreakdown.TabIndex = 25;
			this._btnPayeeBreakdown.Text = "Payee Breakdown";
			this._btnPayeeBreakdown.UseVisualStyleBackColor = true;
			// 
			// _lblCurrent
			// 
			this._lblCurrent.AutoSize = true;
			this._lblCurrent.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
			this._lblCurrent.Location = new System.Drawing.Point(701, 15);
			this._lblCurrent.Name = "_lblCurrent";
			this._lblCurrent.Size = new System.Drawing.Size(48, 13);
			this._lblCurrent.TabIndex = 11;
			this._lblCurrent.Text = "Current";
			// 
			// _lblPrevious
			// 
			this._lblPrevious.AutoSize = true;
			this._lblPrevious.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
			this._lblPrevious.Location = new System.Drawing.Point(376, 14);
			this._lblPrevious.Name = "_lblPrevious";
			this._lblPrevious.Size = new System.Drawing.Size(56, 13);
			this._lblPrevious.TabIndex = 2;
			this._lblPrevious.Text = "Previous";
			// 
			// _lblProposed
			// 
			this._lblProposed.AutoSize = true;
			this._lblProposed.Location = new System.Drawing.Point(563, 55);
			this._lblProposed.Name = "_lblProposed";
			this._lblProposed.Size = new System.Drawing.Size(96, 13);
			this._lblProposed.TabIndex = 12;
			this._lblProposed.Text = "Proposed Payment";
			// 
			// _txtProposed
			// 
			this._txtProposed.Enabled = false;
			this._txtProposed.Location = new System.Drawing.Point(667, 52);
			this._txtProposed.Name = "_txtProposed";
			this._txtProposed.Size = new System.Drawing.Size(119, 20);
			this._txtProposed.TabIndex = 13;
			this._txtProposed.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _lblClose
			// 
			this._lblClose.AutoSize = true;
			this._lblClose.Location = new System.Drawing.Point(569, 77);
			this._lblClose.Name = "_lblClose";
			this._lblClose.Size = new System.Drawing.Size(90, 13);
			this._lblClose.TabIndex = 14;
			this._lblClose.Text = "Proposed Closure";
			// 
			// _txtClose
			// 
			this._txtClose.Enabled = false;
			this._txtClose.Location = new System.Drawing.Point(667, 74);
			this._txtClose.Name = "_txtClose";
			this._txtClose.Size = new System.Drawing.Size(119, 20);
			this._txtClose.TabIndex = 15;
			this._txtClose.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtBalance
			// 
			this._txtBalance.Enabled = false;
			this._txtBalance.Location = new System.Drawing.Point(667, 96);
			this._txtBalance.Name = "_txtBalance";
			this._txtBalance.Size = new System.Drawing.Size(119, 20);
			this._txtBalance.TabIndex = 17;
			this._txtBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtCarriedForward
			// 
			this._txtCarriedForward.Enabled = false;
			this._txtCarriedForward.Location = new System.Drawing.Point(343, 118);
			this._txtCarriedForward.Name = "_txtCarriedForward";
			this._txtCarriedForward.Size = new System.Drawing.Size(119, 20);
			this._txtCarriedForward.TabIndex = 10;
			this._txtCarriedForward.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtClosed
			// 
			this._txtClosed.Enabled = false;
			this._txtClosed.Location = new System.Drawing.Point(343, 96);
			this._txtClosed.Name = "_txtClosed";
			this._txtClosed.Size = new System.Drawing.Size(119, 20);
			this._txtClosed.TabIndex = 8;
			this._txtClosed.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtPaid
			// 
			this._txtPaid.Enabled = false;
			this._txtPaid.Location = new System.Drawing.Point(343, 74);
			this._txtPaid.Name = "_txtPaid";
			this._txtPaid.Size = new System.Drawing.Size(119, 20);
			this._txtPaid.TabIndex = 6;
			this._txtPaid.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtAccrued
			// 
			this._txtAccrued.Enabled = false;
			this._txtAccrued.Location = new System.Drawing.Point(343, 30);
			this._txtAccrued.Name = "_txtAccrued";
			this._txtAccrued.Size = new System.Drawing.Size(119, 20);
			this._txtAccrued.TabIndex = 4;
			this._txtAccrued.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _lblClosed
			// 
			this._lblClosed.AutoSize = true;
			this._lblClosed.Location = new System.Drawing.Point(298, 99);
			this._lblClosed.Name = "_lblClosed";
			this._lblClosed.Size = new System.Drawing.Size(39, 13);
			this._lblClosed.TabIndex = 7;
			this._lblClosed.Text = "Closed";
			// 
			// _lblPaid
			// 
			this._lblPaid.AutoSize = true;
			this._lblPaid.Location = new System.Drawing.Point(309, 77);
			this._lblPaid.Name = "_lblPaid";
			this._lblPaid.Size = new System.Drawing.Size(28, 13);
			this._lblPaid.TabIndex = 5;
			this._lblPaid.Text = "Paid";
			// 
			// _lblAccrued
			// 
			this._lblAccrued.AutoSize = true;
			this._lblAccrued.Location = new System.Drawing.Point(290, 33);
			this._lblAccrued.Name = "_lblAccrued";
			this._lblAccrued.Size = new System.Drawing.Size(47, 13);
			this._lblAccrued.TabIndex = 3;
			this._lblAccrued.Text = "Accrued";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(528, 99);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(131, 13);
			this.label1.TabIndex = 16;
			this.label1.Text = "New Balance Outstanding";
			// 
			// _lblCarriedForward
			// 
			this._lblCarriedForward.AutoSize = true;
			this._lblCarriedForward.Location = new System.Drawing.Point(229, 121);
			this._lblCarriedForward.Name = "_lblCarriedForward";
			this._lblCarriedForward.Size = new System.Drawing.Size(108, 13);
			this._lblCarriedForward.TabIndex = 9;
			this._lblCarriedForward.Text = "Total Carried Forward";
			// 
			// _txtPayThru
			// 
			this._txtPayThru.Enabled = false;
			this._txtPayThru.Location = new System.Drawing.Point(106, 30);
			this._txtPayThru.Name = "_txtPayThru";
			this._txtPayThru.Size = new System.Drawing.Size(57, 20);
			this._txtPayThru.TabIndex = 1;
			// 
			// _lblPayThru
			// 
			this._lblPayThru.AutoSize = true;
			this._lblPayThru.Location = new System.Drawing.Point(17, 33);
			this._lblPayThru.Name = "_lblPayThru";
			this._lblPayThru.Size = new System.Drawing.Size(83, 13);
			this._lblPayThru.TabIndex = 0;
			this._lblPayThru.Text = "Pay Thru Period";
			// 
			// _errorProvider
			// 
			this._errorProvider.ContainerControl = this;
			// 
			// _treeView
			// 
			this._treeView.Dock = System.Windows.Forms.DockStyle.Fill;
			this._treeView.Location = new System.Drawing.Point(3, 16);
			this._treeView.Name = "_treeView";
			this._treeView.Size = new System.Drawing.Size(1199, 81);
			this._treeView.TabIndex = 2;
			// 
			// _grpSelection
			// 
			this._grpSelection.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)));
			this._grpSelection.Controls.Add(this._treeView);
			this._grpSelection.Location = new System.Drawing.Point(12, 168);
			this._grpSelection.Name = "_grpSelection";
			this._grpSelection.Size = new System.Drawing.Size(1205, 100);
			this._grpSelection.TabIndex = 3;
			this._grpSelection.TabStop = false;
			this._grpSelection.Text = "Rebate Selection";
			// 
			// _ucRebatePaymentSummary
			// 
			this._ucRebatePaymentSummary.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this._ucRebatePaymentSummary.DataSource = null;
			this._ucRebatePaymentSummary.IsReadOnly = false;
			this._ucRebatePaymentSummary.Location = new System.Drawing.Point(12, 274);
			this._ucRebatePaymentSummary.Name = "_ucRebatePaymentSummary";
			this._ucRebatePaymentSummary.Size = new System.Drawing.Size(1205, 263);
			this._ucRebatePaymentSummary.TabIndex = 0;
			// 
			// frmPayment
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(1229, 541);
			this.Controls.Add(this._grpSelection);
			this.Controls.Add(this._ucRebatePaymentSummary);
			this.Controls.Add(this._grpSummary);
			this.Name = "frmPayment";
			this.Text = "Payment Calculations Summary";
			this._grpSummary.ResumeLayout(false);
			this._grpSummary.PerformLayout();
			((System.ComponentModel.ISupportInitialize) (this._errorProvider)).EndInit();
			this._grpSelection.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox _grpSummary;
		private System.Windows.Forms.Label _lblCurrent;
		private System.Windows.Forms.Label _lblPrevious;
		private System.Windows.Forms.Label _lblProposed;
		private System.Windows.Forms.TextBox _txtProposed;
		private System.Windows.Forms.Label _lblClose;
		private System.Windows.Forms.TextBox _txtClose;
		private System.Windows.Forms.TextBox _txtBalance;
		private System.Windows.Forms.TextBox _txtCarriedForward;
		private System.Windows.Forms.TextBox _txtClosed;
		private System.Windows.Forms.TextBox _txtPaid;
		private System.Windows.Forms.TextBox _txtAccrued;
		private System.Windows.Forms.Label _lblClosed;
		private System.Windows.Forms.Label _lblPaid;
		private System.Windows.Forms.Label _lblAccrued;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label _lblCarriedForward;
		private System.Windows.Forms.TextBox _txtPayThru;
		private System.Windows.Forms.Label _lblPayThru;
		private System.Windows.Forms.Button _btnPayeeBreakdown;
		private System.Windows.Forms.Label _lblQuickPayPercent;
		private System.Windows.Forms.Label _lblQuickPayPercentage;
		private System.Windows.Forms.TextBox _txtQuickPayPercent;
		private System.Windows.Forms.Label _lblQuickPayAmount;
		private System.Windows.Forms.TextBox _txtQuickPayAmount;
		private System.Windows.Forms.ErrorProvider _errorProvider;
		private System.Windows.Forms.LinkLabel _lnkQuickPay;
		private System.Windows.Forms.Button _btnClear;
		private System.Windows.Forms.TextBox _txtPending;
		private System.Windows.Forms.LinkLabel _lnkPending;
		private System.Windows.Forms.TreeView _treeView;
		private UserControls.Rebates.RebatePaymentSummary _ucRebatePaymentSummary;
		private System.Windows.Forms.GroupBox _grpSelection;
	}
}
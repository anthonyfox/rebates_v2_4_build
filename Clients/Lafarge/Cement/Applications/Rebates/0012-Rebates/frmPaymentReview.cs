﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Evolve.Libraries.Utilities;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Libraries.Data.Remoting;
using Evolve.Clients.Lafarge.Cement.UserControls.Rebates;

namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	public partial class frmPaymentReview : Form
	{
		private String _numberFormat = "#,0.00";
		private BindingSource _bindingSource;

		private PaymentFind _paymentFind;
		private EntityListView<PaymentReview> _payments;

		public frmPaymentReview(PaymentFind paymentFind)
		{
			InitializeComponent();

			_paymentFind = paymentFind;
			_payments = new EntityListView<PaymentReview>(_paymentFind.Payments);
			
			_bindingSource = new BindingSource();
			_bindingSource.DataSource = _payments;
			_bindingNavigator.BindingSource = _bindingSource;

			_dgvPayments.DataSource = _bindingSource;
			_dgvPayments.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			_dgvPayments.MultiSelect = true;
			_dgvPayments.ReadOnly = false;
			_dgvPayments.DataBindingComplete += _dgvPayments_DataBindingComplete;
			_dgvPayments.CellDoubleClick += _dgvPayments_CellDoubleClick;

			bindingNavigatorButtonRefresh.Click += bindingNavigatorButtonRefresh_Click;
		}

		void _dgvPayments_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			DataGridView dataGridView = (DataGridView)sender;

			DataGridViewUtilities.SetColumnFormat(dataGridView.Columns[PaymentReviewProperty.IsJif], "", DataGridViewContentAlignment.MiddleCenter);
			DataGridViewUtilities.SetColumnFormat(dataGridView.Columns[PaymentReviewProperty.IsContract], "", DataGridViewContentAlignment.MiddleCenter);
			DataGridViewUtilities.SetColumnFormat(dataGridView.Columns[PaymentReviewProperty.IsVAP], "", DataGridViewContentAlignment.MiddleCenter);
			DataGridViewUtilities.SetColumnFormat(dataGridView.Columns[PaymentReviewProperty.Value], _numberFormat, DataGridViewContentAlignment.MiddleRight);

			dataGridView.Columns[PaymentReviewProperty.RebateVersion].Frozen = true;
			dataGridView.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);

			toolStripTextBoxTotalPayment.Text = _payments.Sum(pr => pr.Value).ToString(_numberFormat);
		}

		void bindingNavigatorButtonRefresh_Click(object sender, EventArgs e)
		{
			//RefreshList();
		}

		void _dgvPayments_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
		{
			DataGridView dataGridView = (DataGridView)sender;

			if (e.RowIndex >= 0)
			{
				this.Cursor = Cursors.WaitCursor;

				PaymentReview paymentReview = (PaymentReview)dataGridView.CurrentRow.DataBoundItem;
				
				Rebate rebate = new Rebate();
				rebate.Id = paymentReview.RebateId;

				if (rebate.ReadCurrent())
				{
					frmMDIParent mdiParent = (frmMDIParent)this.MdiParent;
					Boolean isNew = false;
					Form form = mdiParent.OpenSingleInstance(typeof(frmRebate), new Object[] { rebate }, out isNew, rebate.WindowTitle);

					if (isNew)
					{
						form.FormClosed += new FormClosedEventHandler(frmRebate_FormClosed);
					}
				}

				this.Cursor = Cursors.Default;
			}
		}

		void frmRebate_FormClosed(object sender, FormClosedEventArgs e)
		{
			frmRebate frmRebate = (frmRebate)sender;

			if (frmRebate.DialogResult == DialogResult.OK)
			{
				RefreshList();
			}
		}

		private void RefreshList()
		{
			this.Cursor = Cursors.WaitCursor;

			_paymentFind.RefreshList();
			_payments = new EntityListView<PaymentReview>(_paymentFind.Payments);
			_bindingSource.DataSource = _payments;

			this.Cursor = Cursors.Default;
		}

		private void frmPaymentReview_Load(object sender, EventArgs e)
		{
			if (this.MdiParent != null)
			{
				this.Icon = this.MdiParent.Icon;
			}
		}
	}
}

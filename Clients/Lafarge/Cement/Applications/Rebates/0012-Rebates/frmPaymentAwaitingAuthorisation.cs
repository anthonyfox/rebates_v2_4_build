﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;
using Evolve.Libraries.Controls.Generic;

namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	public partial class frmPaymentAwaitingAuthorisation : Form
	{
		private EntityListView<PaymentBreakdown> _pendingPaymentsView;
		private DataGridViewCheckBoxHeaderCell _isSelected = new DataGridViewCheckBoxHeaderCell(false);
		private String _numberFormat = "#,0.00";

		#region PaymentStatusChanged

		public delegate void PaymentStatusChangedEventHandler(object sender, EventArgs e);
		public event PaymentStatusChangedEventHandler PaymentStatusChanged;

		public void OnPaymentStatusChanged()
		{
			if (PaymentStatusChanged != null)
			{
				PaymentStatusChanged(this, new EventArgs());
			}
		}

		#endregion

		public frmPaymentAwaitingAuthorisation()
		{
			InitializeComponent();

			if (this.MdiParent != null)
			{
				this.Icon = this.MdiParent.Icon;
			}

			this.Load += new EventHandler(frmPaymentAwaitingAuthorisation_Load);
		}

		void frmPaymentAwaitingAuthorisation_Load(object sender, EventArgs e)
		{
			if (this.MdiParent != null)
			{
				this.Icon = this.MdiParent.Icon;
			}

			_dgvPending.AllowUserToAddRows = false;
			_dgvPending.AllowUserToDeleteRows = false;
			_dgvPending.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(_dgvPending_DataBindingComplete);
			_dgvPending.CurrentCellDirtyStateChanged += new EventHandler(_dgvPending_CurrentCellDirtyStateChanged);

			_isSelected.OnCheckBoxClicked += new CheckBoxClickedHandler(_isSelected_OnCheckBoxClicked);
			_btnAuthorise.Visible = Program.Division.AllowPaymentAuthorisation;
			_btnAuthorise.Click += new EventHandler(_btnAuthorise_Click);
			_btnReject.Click += new EventHandler(_btnReject_Click);

			if (!Program.Division.AllowPaymentAuthorisation)
			{
				_grpInformation.Visible = false;
				_grpPending.Location = _grpInformation.Location;
				_grpPending.Dock = DockStyle.Fill;
			}

			LoadPendingPayments();
		}

		void _dgvPending_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			foreach (DataGridViewColumn column in _dgvPending.Columns)
			{
				switch (column.DataPropertyName)
				{
					case PaymentBreakdownProperty.IsSelected:
						if (Program.Division.AllowPaymentAuthorisation)
						{
							column.HeaderCell = _isSelected;
						}
						else
						{
							column.Visible = false;
						}

						break;

					case PaymentBreakdownProperty.RejectionComment:
					case PaymentBreakdownProperty.ActionTime:
					case PaymentBreakdownProperty.ActionUser:
						column.Visible = false;
						break;

					default:
						if (column.ValueType.Equals(typeof(Decimal)))
						{
							DataGridViewUtilities.SetColumnFormat(column, _numberFormat, DataGridViewContentAlignment.MiddleRight);
						}
						else if (column.ValueType.Equals(typeof(Int32)))
						{
							DataGridViewUtilities.SetColumnFormat(column, "", DataGridViewContentAlignment.MiddleRight);
						}

						column.ReadOnly = true;
						break;
				}
		}

			_dgvPending.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
		}

		public void LoadPendingPayments()
		{
			PaymentBreakdown paymentBreakdown = new PaymentBreakdown();
			paymentBreakdown.AddReadFilter(PaymentBreakdownProperty.PaymentStatusEnum, "=", PaymentStatusEnum.PendingAuthorisation);
			_pendingPaymentsView = new EntityListView<PaymentBreakdown>((ExtendedBindingList<PaymentBreakdown>) paymentBreakdown.Read());
			_dgvPending.DataSource = _pendingPaymentsView;
			_pendingPaymentsView.Filter = PaymentBreakdownProperty.PaymentStatusEnum + " = " + PaymentStatusEnum.PendingAuthorisation.ToString();
		}

		void _dgvPending_CurrentCellDirtyStateChanged(object sender, EventArgs e)
		{
			if (_dgvPending.CurrentRow != null && _dgvPending.IsCurrentCellDirty)
			{
				_dgvPending.CommitEdit(DataGridViewDataErrorContexts.CurrentCellChange);

				PaymentBreakdown paymentBreakdown = (PaymentBreakdown) _dgvPending.CurrentRow.DataBoundItem;
				Boolean isSelected = paymentBreakdown.IsSelected;

				foreach (PaymentBreakdown p in _pendingPaymentsView.List.Where(pb => pb.Payment_PaymentBreakdownId == paymentBreakdown.Payment_PaymentBreakdownId))
				{
					p.IsSelected = isSelected;
				}
			}
		}

		void _isSelected_OnCheckBoxClicked(bool state)
		{
			foreach (PaymentBreakdown paymentBreakdown in _pendingPaymentsView.List.Where(pb => pb.PaymentStatusEnum == PaymentStatusEnum.PendingAuthorisation))
			{
				paymentBreakdown.IsSelected = state;
			}
		}

		void _btnAuthorise_Click(object sender, EventArgs e)
		{
			UpdatePaymentStatus(PaymentStatusEnum.Authorised, null);
		}

		void _btnReject_Click(object sender, EventArgs e)
		{
			frmComment frmComment = new frmComment();
			frmComment.Icon = this.Icon;

			if (frmComment.ShowDialog() == DialogResult.OK)
			{
				UpdatePaymentStatus(PaymentStatusEnum.Rejected, frmComment.Comment);
			}
		}

		private void UpdatePaymentStatus(PaymentStatusEnum paymentStatusEnum, String comment)
		{
			// Get unique list of Payment_PaymentBreakdown id's from the selected payments list
			var results = (from pb in _pendingPaymentsView.List where pb.IsSelected select pb.Payment_PaymentBreakdownId).Distinct();
			String ids = "";

			foreach (Int32 id in results)
			{
				ids += id.ToString() + ", ";
			}

			if (ids.Length > 0)
			{
				ids = "( " + ids.Substring(0, ids.Length - 2) + " )";

				// Now read all relevant Payment_PaymentBreakdown records
				Payment_PaymentBreakdown payment_paymentBreakdown = new Payment_PaymentBreakdown();
				payment_paymentBreakdown.ReadFilter = Payment_PaymentBreakdownProperty.Id + " in " + ids;
				ExtendedBindingList<Payment_PaymentBreakdown> ppbs = (ExtendedBindingList<Payment_PaymentBreakdown>) payment_paymentBreakdown.Read();

				// Open transaction
				Boolean retry = false;
				String errorMessage = "";

				DataAccessServer.NewPersistentConnection();
				DataAccessServer.PersistentConnection.BeginTransaction();

				do
				{
					try
					{
						Int32 result = 0;

						// Update records
						foreach (Payment_PaymentBreakdown ppb in ppbs)
						{
							ppb.ActionTime = DateTime.Now;
							ppb.ActionUserId = Program.User.Id;
							ppb.PaymentStatusEnum = paymentStatusEnum;
							ppb.RejectionComment = comment;

							result = ppb.AcceptChanges();

							if (result < 0)
							{
								errorMessage = "Failed to update Payment_PaymentBreakdown (Id = " + ppb.Id.ToString() + ")";
								break;
							}
						}

						// Commit or rollback as required
						if (result >= 0)
						{
							DataAccessServer.PersistentConnection.CommitTransaction();

							foreach (PaymentBreakdown pb in _pendingPaymentsView.List.Where(pb => pb.IsSelected))
							{
								pb.IsSelected = false;
								pb.PaymentStatusEnum = paymentStatusEnum;
							}

							OnPaymentStatusChanged();
						}
						else
						{
							DataAccessServer.PersistentConnection.RollbackTransaction();
						}

						DataAccessServer.TerminatePersistentConnection();
					}
					catch (DatabaseException databaseException)
					{
						// Handle exceptions
						DataAccessServer.PersistentConnection.RollbackTransaction();
						DataAccessServer.TerminatePersistentConnection();

						if (databaseException.IsLockError)
						{
							if (MessageBox.Show("Database is locked, do you wish to retry ?", "Database Locked", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
							{
								retry = true;
							}
						}
						else
						{
							errorMessage = databaseException.Message;
						}
					}
				} while (retry);

				if (errorMessage != "")
				{
					MessageBox.Show("Failed to Update Payments", errorMessage, MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
		}
	}
}

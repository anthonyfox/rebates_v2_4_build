﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	public partial class frmRejectedPayments : Form
	{
		private String _numberFormat = "#,0.00";

		public frmRejectedPayments()
		{
			InitializeComponent();

			_dtpFrom.Value = DateTime.Now.Date.AddMonths(-1);
			_dtpTo.Value = DateTime.Now.Date;

			this.Load += new EventHandler(frmRejectedPayments_Load);
			_btnFind.Click += new EventHandler(_btnFind_Click);
			_dgvRejectedPayments.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(_dgvRejectedPayments_DataBindingComplete);
			_dgvRejectedPayments.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
		}

		void frmRejectedPayments_Load(object sender, EventArgs e)
		{
			if (this.MdiParent != null)
			{
				this.Icon = this.MdiParent.Icon;
			}
		}

		void _btnFind_Click(object sender, EventArgs e)
		{
			PaymentBreakdown paymentBreakdown = new PaymentBreakdown();

			paymentBreakdown.PaymentStatusEnum = PaymentStatusEnum.Rejected;
			paymentBreakdown.AddReadFilter(PaymentBreakdownProperty.PaymentStatusEnum, "=");
			paymentBreakdown.AddReadFilterBetween(PaymentBreakdownProperty.ActionTime, _dtpFrom.Value, _dtpTo.Value.AddDays(1).AddSeconds(-1));

			_dgvRejectedPayments.DataSource = paymentBreakdown.Read();
		}
	
		void _dgvRejectedPayments_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			foreach (DataGridViewColumn column in _dgvRejectedPayments.Columns)
			{
				switch (column.DataPropertyName)
				{
					case PaymentBreakdownProperty.IsSelected:
						column.Visible = false;
						break;

					default:
						if (column.ValueType.Equals(typeof(Decimal)))
						{
							DataGridViewUtilities.SetColumnFormat(column, _numberFormat, DataGridViewContentAlignment.MiddleRight);
						}
						else if (column.ValueType.Equals(typeof(Int32)))
						{
							DataGridViewUtilities.SetColumnFormat(column, "", DataGridViewContentAlignment.MiddleRight);
						}

						column.ReadOnly = true;
						break;
				}
			}

			_dgvRejectedPayments.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
		}
	}
}

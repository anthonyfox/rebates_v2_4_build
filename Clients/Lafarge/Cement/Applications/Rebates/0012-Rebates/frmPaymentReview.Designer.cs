﻿namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	partial class frmPaymentReview
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPaymentReview));
			this._bindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
			this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
			this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
			this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
			this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
			this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
			this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
			this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.bindingNavigatorButtonRefresh = new System.Windows.Forms.ToolStripButton();
			this._dgvPayments = new System.Windows.Forms.DataGridView();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripLabelTotalPayment = new System.Windows.Forms.ToolStripLabel();
			this.toolStripTextBoxTotalPayment = new System.Windows.Forms.ToolStripTextBox();
			((System.ComponentModel.ISupportInitialize)(this._bindingNavigator)).BeginInit();
			this._bindingNavigator.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this._dgvPayments)).BeginInit();
			this.SuspendLayout();
			// 
			// _bindingNavigator
			// 
			this._bindingNavigator.AddNewItem = null;
			this._bindingNavigator.CountItem = this.bindingNavigatorCountItem;
			this._bindingNavigator.DeleteItem = null;
			this._bindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.toolStripSeparator1,
            this.bindingNavigatorButtonRefresh,
            this.toolStripSeparator2,
            this.toolStripLabelTotalPayment,
            this.toolStripTextBoxTotalPayment});
			this._bindingNavigator.Location = new System.Drawing.Point(0, 0);
			this._bindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
			this._bindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
			this._bindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
			this._bindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
			this._bindingNavigator.Name = "_bindingNavigator";
			this._bindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
			this._bindingNavigator.Size = new System.Drawing.Size(853, 25);
			this._bindingNavigator.TabIndex = 0;
			this._bindingNavigator.Text = "bindingNavigator1";
			// 
			// bindingNavigatorCountItem
			// 
			this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
			this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
			this.bindingNavigatorCountItem.Text = "of {0}";
			this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
			// 
			// bindingNavigatorMoveFirstItem
			// 
			this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
			this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
			this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
			this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
			this.bindingNavigatorMoveFirstItem.Text = "Move first";
			// 
			// bindingNavigatorMovePreviousItem
			// 
			this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
			this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
			this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
			this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
			this.bindingNavigatorMovePreviousItem.Text = "Move previous";
			// 
			// bindingNavigatorSeparator
			// 
			this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
			this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
			// 
			// bindingNavigatorPositionItem
			// 
			this.bindingNavigatorPositionItem.AccessibleName = "Position";
			this.bindingNavigatorPositionItem.AutoSize = false;
			this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
			this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
			this.bindingNavigatorPositionItem.Text = "0";
			this.bindingNavigatorPositionItem.ToolTipText = "Current position";
			// 
			// bindingNavigatorSeparator1
			// 
			this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
			this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
			// 
			// bindingNavigatorMoveNextItem
			// 
			this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
			this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
			this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
			this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
			this.bindingNavigatorMoveNextItem.Text = "Move next";
			// 
			// bindingNavigatorMoveLastItem
			// 
			this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
			this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
			this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
			this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
			this.bindingNavigatorMoveLastItem.Text = "Move last";
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
			// 
			// bindingNavigatorButtonRefresh
			// 
			this.bindingNavigatorButtonRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.bindingNavigatorButtonRefresh.Image = global::Evolve.Clients.Lafarge.Cement.Rebates.Properties.Resources.arrow_refresh;
			this.bindingNavigatorButtonRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.bindingNavigatorButtonRefresh.Name = "bindingNavigatorButtonRefresh";
			this.bindingNavigatorButtonRefresh.Size = new System.Drawing.Size(23, 22);
			this.bindingNavigatorButtonRefresh.Text = "Refresh";
			// 
			// _dgvPayments
			// 
			this._dgvPayments.AllowUserToAddRows = false;
			this._dgvPayments.AllowUserToDeleteRows = false;
			this._dgvPayments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this._dgvPayments.Dock = System.Windows.Forms.DockStyle.Fill;
			this._dgvPayments.Location = new System.Drawing.Point(0, 25);
			this._dgvPayments.Name = "_dgvPayments";
			this._dgvPayments.ReadOnly = true;
			this._dgvPayments.Size = new System.Drawing.Size(853, 415);
			this._dgvPayments.TabIndex = 1;
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
			// 
			// toolStripLabelTotalPayment
			// 
			this.toolStripLabelTotalPayment.Name = "toolStripLabelTotalPayment";
			this.toolStripLabelTotalPayment.Size = new System.Drawing.Size(116, 22);
			this.toolStripLabelTotalPayment.Text = "Total Payment Value";
			// 
			// toolStripTextBoxTotalPayment
			// 
			this.toolStripTextBoxTotalPayment.Name = "toolStripTextBoxTotalPayment";
			this.toolStripTextBoxTotalPayment.ReadOnly = true;
			this.toolStripTextBoxTotalPayment.Size = new System.Drawing.Size(150, 25);
			this.toolStripTextBoxTotalPayment.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// frmPaymentReview
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(853, 440);
			this.Controls.Add(this._dgvPayments);
			this.Controls.Add(this._bindingNavigator);
			this.Name = "frmPaymentReview";
			this.Text = "Payments Search Results";
			this.Load += new System.EventHandler(this.frmPaymentReview_Load);
			((System.ComponentModel.ISupportInitialize)(this._bindingNavigator)).EndInit();
			this._bindingNavigator.ResumeLayout(false);
			this._bindingNavigator.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this._dgvPayments)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.BindingNavigator _bindingNavigator;
		private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
		private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
		private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
		private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
		private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
		private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
		private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
		private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
		private System.Windows.Forms.DataGridView _dgvPayments;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripButton bindingNavigatorButtonRefresh;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripLabel toolStripLabelTotalPayment;
		private System.Windows.Forms.ToolStripTextBox toolStripTextBoxTotalPayment;
	}
}
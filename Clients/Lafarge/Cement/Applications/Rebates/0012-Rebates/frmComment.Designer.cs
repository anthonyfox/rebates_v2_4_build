﻿namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	partial class frmComment
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this._textBox = new System.Windows.Forms.TextBox();
			this._okCancel = new Evolve.Libraries.Controls.Generic.OkCancel();
			this._errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
			((System.ComponentModel.ISupportInitialize)(this._errorProvider)).BeginInit();
			this.SuspendLayout();
			// 
			// _textBox
			// 
			this._textBox.Location = new System.Drawing.Point(28, 22);
			this._textBox.MaxLength = 60;
			this._textBox.Name = "_textBox";
			this._textBox.Size = new System.Drawing.Size(431, 20);
			this._textBox.TabIndex = 0;
			// 
			// _okCancel
			// 
			this._okCancel.CausesValidation = false;
			this._okCancel.Location = new System.Drawing.Point(302, 64);
			this._okCancel.Name = "_okCancel";
			this._okCancel.Size = new System.Drawing.Size(157, 24);
			this._okCancel.TabIndex = 1;
			// 
			// _errorProvider
			// 
			this._errorProvider.ContainerControl = this;
			// 
			// frmComment
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(485, 110);
			this.Controls.Add(this._okCancel);
			this.Controls.Add(this._textBox);
			this.Name = "frmComment";
			this.Text = "Comments";
			((System.ComponentModel.ISupportInitialize)(this._errorProvider)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox _textBox;
		private Libraries.Controls.Generic.OkCancel _okCancel;
		private System.Windows.Forms.ErrorProvider _errorProvider;
	}
}
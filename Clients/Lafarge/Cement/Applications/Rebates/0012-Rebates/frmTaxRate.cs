﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Evolve.Libraries.Data.Remoting;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Libraries.Utilities;
using Evolve.Libraries.WinformsApplication;

namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	public partial class frmTaxRate : Form, IListMaintenance
	{
		private TaxRate _originalTaxRate;
		private TaxRate _taxRate;

		public Entity DataItem
		{
			get { return _taxRate; }
		}

		public frmTaxRate(TaxRate taxRate)
		{
			// Initialise
			InitializeComponent();
			_ucTaxCode.Populate();

			// Events
			_okCancel.ButtonClick += new Libraries.Controls.Generic.OkCancel.ButtonClickHandler(_okCancel_ButtonClick);

			// Setup objects
			if (taxRate == null)
			{
				_taxRate = new TaxRate();
				_taxRate.CreateUserId = Program.User.Id;
			}
			else
			{
				_originalTaxRate = taxRate;
				_taxRate = (TaxRate) _originalTaxRate.Clone();
				_taxRate.UpdateUserId = Program.User.Id;
			}

			// Validation
			_ucTaxCode.ComboBox.SelectedValueChanged += new EventHandler(ComboBox_SelectedValueChanged);
			_ucTaxCode.Validating += new CancelEventHandler(_ucTaxCode_Validating);
			_dtpEffectiveDate.Validating += new CancelEventHandler(_dtpEffectiveDate_Validating);
			_txtRate.Validating += new CancelEventHandler(_txtRate_Validating);
			_txtJDETaxRate.Validating += new CancelEventHandler(_txtJDETaxRate_Validating);

			// Databind
			_ucTaxCode.FindAndSelectId(_taxRate.TaxCodeId);
			_dtpEffectiveDate.DataBindings.Add("Value", _taxRate, TaxRateProperty.EffectiveDate);
			_txtRate.DataBindings.Add("Text", _taxRate, TaxRateProperty.FormattedRate);
			_txtRate.Enabled = !_taxRate.IsZeroRate;
			_txtJDETaxRate.DataBindings.Add("Text", _taxRate, TaxRateProperty.JDETaxRate);
			_txtJDEExplanation.DataBindings.Add("Text", _taxRate, TaxRateProperty.JDEExplanation);
		}

		void ComboBox_SelectedValueChanged(object sender, EventArgs e)
		{
			if (_ucTaxCode.SelectedValue != null)
			{
				_taxRate.IsZeroRate = _ucTaxCode.SelectedValue.IsZeroRate;
				_txtRate.Enabled = _taxRate.IsZeroRate;
			}
			else
			{
				_txtRate.Enabled = true;
			}
		}

		void _ucTaxCode_Validating(object sender, CancelEventArgs e)
		{
			String error = "";

			if (_ucTaxCode.SelectedValue == null)
			{
				error = "A tax code must be specified";
			}

			_errorProvider.SetError(_ucTaxCode, error);
		}

		void _dtpEffectiveDate_Validating(object sender, CancelEventArgs e)
		{
			String error = "";

			TaxRate taxRate = new TaxRate();
			taxRate.TaxCodeId = taxRate.TaxCodeId;
			taxRate.EffectiveDate = _dtpEffectiveDate.Value;

			ExtendedBindingList<TaxRate> taxRates = (ExtendedBindingList<TaxRate>) taxRate.ReadWithDirtyProperty();

			if (!(taxRates.Count == 0 || taxRates.Count == 1 && taxRate.Id != taxRates[0].Id))
			{
				error = "Effective date already exists for this tax code";
			}

			_errorProvider.SetError(_txtRate, error);
		}

		void _txtRate_Validating(object sender, CancelEventArgs e)
		{
			_errorProvider.SetError(_txtRate, TaxRate.ValidateRate(_txtRate.Text));
		}

		void _txtJDETaxRate_Validating(object sender, CancelEventArgs e)
		{
			_errorProvider.SetError(_txtJDETaxRate, TaxRate.ValidateJDERate(_txtJDETaxRate.Text, _taxRate.Id));
		}

		void _okCancel_ButtonClick(object sender, EventArgs e)
		{
			if (_okCancel.DialogResult == DialogResult.OK)
			{
				this.ValidateChildren();

				if (!ValidateControl.HasErrors(this, _errorProvider) && Save())
				{
					this.DialogResult = _okCancel.DialogResult;
				}
			}
			else
			{
				this.Close();
			}
		}

		private Boolean Save()
		{
			Boolean doRetry = false;
			Boolean isSaved = false;

			_taxRate.TaxCodeId = _ucTaxCode.SelectedValue.Id;
			_taxRate.Code = _ucTaxCode.SelectedValue.Code;

			do
			{
				try
				{
					_taxRate.AcceptChanges();
					isSaved = true;
				}
				catch (DatabaseException databaseException)
				{
					if (databaseException.IsLockError)
					{
						doRetry = MessageBox.Show("Tax Rate is locked by another user", "Record Locked", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes;
					}
					else
					{
						throw databaseException;
					}
				}
			} while (doRetry);

			return isSaved;
		}
	}
}

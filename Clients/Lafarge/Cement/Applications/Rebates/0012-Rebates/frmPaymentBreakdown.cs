﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	public partial class frmPaymentBreakdown : Form, INotifyPropertyChanged
	{
		private PaymentSummary _paymentSummary;
		private String _numberFormat = "#,0.00";
		private frmPayment _frmPayment = null;
		private frmPaymentAssignment _frmPaymentAssignment;

		#region Properties

		public frmPayment ParentPaymentForm
		{
			get { return _frmPayment; }
			set { _frmPayment = value; }
		}

		public Decimal AssignedPaymentAmount
		{
			get
			{
				return (_paymentSummary == null ? 0 : _paymentSummary.AssignedPaymentAmount);
			}
		}

		public Decimal UnAssignedPaymentAmount
		{
			get
			{
				return (_paymentSummary == null ? 0 : _paymentSummary.UnAssignedPaymentAmount);
			}
		}


		#endregion

		#region PropertyChanged

		public event PropertyChangedEventHandler PropertyChanged;

		public void NotifyPropertyChanged(String propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		#endregion

		public frmPaymentBreakdown(PaymentSummary paymentSummary)
		{
			Initialize(paymentSummary, null);
		}

		public frmPaymentBreakdown(PaymentSummary paymentSummary, PaymentBreakdown paymentBreakdown)
		{
			Initialize(paymentSummary, paymentBreakdown);
		}

		private void Initialize(PaymentSummary paymentSummary, PaymentBreakdown paymentBreakdown)
		{
			InitializeComponent();

			_paymentSummary = paymentSummary;

			_txtPayments.DataBindings.Add("Text", _paymentSummary, "Proposed", true, DataSourceUpdateMode.OnPropertyChanged, null, _numberFormat);
			_txtAssigned.DataBindings.Add("Text", this, "AssignedPaymentAmount", true, DataSourceUpdateMode.OnPropertyChanged, null, _numberFormat);
			_txtOutstanding.DataBindings.Add("Text", this, "UnAssignedPaymentAmount", true, DataSourceUpdateMode.OnPropertyChanged, null, _numberFormat);

			_dgvBreakdowns.ReadOnly = true;
			_dgvBreakdowns.AllowUserToAddRows = false;
			_dgvBreakdowns.AllowUserToDeleteRows = true;
			_dgvBreakdowns.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(_dgvBreakdowns_DataBindingComplete);
			_dgvBreakdowns.DataError += new DataGridViewDataErrorEventHandler(DataGridViewUtilities.DataError);
			_dgvBreakdowns.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			_dgvBreakdowns.DoubleClick += new EventHandler(_dgvBreakdowns_DoubleClick);

			if (_paymentSummary.PaymentBreakdowns == null)
			{
				_paymentSummary.CreateDefaultPaymentBreakdowns();
			}

			this.Load += new EventHandler(frmPaymentBreakdown_Load);
			this.FormClosing += new FormClosingEventHandler(frmPaymentBreakdown_FormClosing);

			_dgvBreakdowns.DataSource = _paymentSummary.PaymentBreakdowns;
			_dgvBreakdowns.Columns["PayeeName"].ReadOnly = true;
			_dgvBreakdowns.EditMode = DataGridViewEditMode.EditOnEnter;
			_dgvBreakdowns.Columns["Value"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			_dgvBreakdowns.Columns["Value"].DefaultCellStyle.Format = _numberFormat;

			_btnAssignDefaults.Click += new EventHandler(_btnAssignDefaults_Click);
			_btnAddAssignment.Click += new EventHandler(_btnAddAssignment_Click);
			
			_paymentSummary.PaymentBreakdowns.ListChanged += new ListChangedEventHandler(PaymentBreakdowns_ListChanged);
		}

		#region Event Handlers

		void frmPaymentBreakdown_Load(object sender, EventArgs e)
		{
			if (this.MdiParent != null)
			{
				this.Icon = this.MdiParent.Icon;
			}

			_dgvBreakdowns.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
		}

		void frmPaymentBreakdown_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (_frmPaymentAssignment != null)
			{
				_frmPaymentAssignment.Close();
				_frmPaymentAssignment.Dispose();
				_frmPaymentAssignment = null;
			}
		}

		void _btnAssignDefaults_Click(object sender, EventArgs e)
		{
			_paymentSummary.CreateDefaultPaymentBreakdowns();
			_dgvBreakdowns.DataSource = _paymentSummary.PaymentBreakdowns;
		}

		void _btnAddAssignment_Click(object sender, EventArgs e)
		{
			_frmPaymentAssignment = new frmPaymentAssignment(_paymentSummary);
			ShowAssignment();
		}

		private void ShowAssignment()
		{
			_frmPaymentAssignment.MdiParent = this.MdiParent;
			_frmPaymentAssignment.FormClosing += new FormClosingEventHandler(_frmPaymentAssignment_FormClosing);
			_frmPaymentAssignment.Show();
		}

		void _frmPaymentAssignment_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (_frmPaymentAssignment != null && _frmPaymentAssignment.DialogResult == DialogResult.OK)
			{
				Boolean isNew = false;
				PaymentBreakdown paymentBreakdown = _paymentSummary.PaymentBreakdowns.FirstOrDefault(pb => pb.PayeeId == _frmPaymentAssignment.Payee.Id && pb.TaxCodeId == _frmPaymentAssignment.TaxCode.Id);

				if (paymentBreakdown == null)
				{
					isNew = true;
					paymentBreakdown = new PaymentBreakdown();

					paymentBreakdown.PayeeId = _frmPaymentAssignment.Payee.Id;
					paymentBreakdown.PayeeName = _frmPaymentAssignment.Payee.Name;
				}

				paymentBreakdown.TaxCodeId = _frmPaymentAssignment.TaxCode.Id;
				paymentBreakdown.TaxCode = _frmPaymentAssignment.TaxCode.Code;
				paymentBreakdown.TaxDate = _frmPaymentAssignment.TaxDate;
				paymentBreakdown.Comment = _frmPaymentAssignment.Comment;

				// Reset any existing payment values to zero (this will ensure the UnAssignedPaymentAmount
				// is also reset accordingly
				paymentBreakdown.Value = 0;

				// Now setup payment based on entry
				if (!_frmPaymentAssignment.Value.HasValue && !_frmPaymentAssignment.Percentage.HasValue)
				{
					// No entry so assume 100% payment to payee
					paymentBreakdown.Value += this.UnAssignedPaymentAmount;
				}
				else if (_frmPaymentAssignment.Percentage.HasValue)
				{
					// Percentage based payment
					paymentBreakdown.Value += (this.AssignedPaymentAmount + this.UnAssignedPaymentAmount) * (_frmPaymentAssignment.Percentage.Value / 100);
				}
				else
				{
					// Value based payment
					paymentBreakdown.Value = _frmPaymentAssignment.Value.Value;
				}

				if (isNew)
				{
					_paymentSummary.PaymentBreakdowns.Add(paymentBreakdown);
				}
			}

			_dgvBreakdowns.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
		}
	
		void PaymentBreakdowns_ListChanged(object sender, ListChangedEventArgs e)
		{
			NotifyPropertyChanged("AssignedPaymentAmount");
			NotifyPropertyChanged("UnAssignedPaymentAmount");
		}

		void _dgvBreakdowns_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			_dgvBreakdowns.Columns[PaymentBreakdownProperty.IsSelected].Visible = false;
		}

		void _dgvBreakdowns_DoubleClick(object sender, EventArgs e)
		{
			if (_dgvBreakdowns.CurrentRow != null && _dgvBreakdowns.CurrentRow.DataBoundItem != null)
			{
				PaymentBreakdown paymentBreakdown = (PaymentBreakdown) _dgvBreakdowns.CurrentRow.DataBoundItem;

				if (paymentBreakdown != null)
				{
					_frmPaymentAssignment = new frmPaymentAssignment(_paymentSummary, paymentBreakdown);
					ShowAssignment();
				}
			}
		}

		#endregion

		#region Business Logic

		public Boolean Save()
		{
			Boolean saved = false;

			if (_frmPayment != null)
			{
				if (saved = _frmPayment.Save())
				{
					this.Close();
				}
			}

			return saved;
		}

		#endregion
	}
}

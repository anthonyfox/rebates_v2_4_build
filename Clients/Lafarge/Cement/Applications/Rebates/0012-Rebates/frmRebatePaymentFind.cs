﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	public partial class frmRebatePaymentFind : Evolve.Clients.Lafarge.Cement.UserControls.Rebates.RebateFind
	{
		public ExtendedBindingList<DivisionAccrual> _divisionAccruals;

		public CalculationPeriod PayThruPeriod
		{
			get
			{
				Int16 year;
				Int16 period;
				CalculationPeriod payThruPeriod = null;

				if (Int16.TryParse(_txtPayThruYear.Text, out year) && Int16.TryParse(_txtPayThruPeriod.Text, out period))
				{
					payThruPeriod = new CalculationPeriod(year, period);
				}

				return payThruPeriod;
			}
		}

		public frmRebatePaymentFind(Division division)
			: base(division)
		{
			InitializeComponent();
			InitializeControls();
		}

		private void InitializeControls()
		{
			// Setup defaults and move controls as required
			this.Text = "Payment Summary - Rebate Selection";

			Point point = new Point(this.Size.Width - 103, this.Size.Height - 73);
			_btnSearch.Location = point;

			// Additional validation events
			_txtPayThruYear.Validating += new System.ComponentModel.CancelEventHandler(_txtPayThruYear_Validating);
			_txtPayThruPeriod.Validating += new System.ComponentModel.CancelEventHandler(_txtPayThruPeriod_Validating);

			// Load division accrual details
			DivisionAccrual divisionAccrual = new DivisionAccrual();

			divisionAccrual.DivisionId = Program.Division.Id;
			divisionAccrual.AccrualTypeEnum = AccrualTypeEnum.Actual;
			divisionAccrual.IsDraft = false;

			_divisionAccruals = (ExtendedBindingList<DivisionAccrual>) divisionAccrual.ReadWithDirtyProperty();
		}

		protected override void AddExtendedSearchCriteria(Rebate rebate)
		{
			rebate.Version = Int16.Parse(_txtPayThruYear.Text);
			rebate.AddReadFilter(RebateProperty.Version, "=");

			rebate.PaymentPeriod = Int16.Parse(_txtPayThruPeriod.Text);
		}

		private void MoveControlUp(Control control)
		{
			Point point = new Point(control.Location.X, control.Location.Y - _pnlIsVAP.Height);
			control.Location = point;
		}

		void _txtPayThruYear_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			TextBox textBox = (TextBox) sender;
			String errorText = Rebate.ValidateYear(textBox.Text, false);

			if (errorText == "")
			{
				Int32 payThruYear = 0;
				Int32.TryParse(_txtPayThruYear.Text, out payThruYear);
				
				if (_divisionAccruals.Count(da => da.Year == payThruYear) == 0)
				{
					errorText = "No accruals exist for the specified year";
				}
			}

			_errorProvider.SetError(textBox, errorText);
		}

		void _txtPayThruPeriod_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			TextBox textBox = (TextBox) sender;
			String errorText = Rebate.ValidatePeriod(textBox.Text, false);
			if (errorText == "")
			{
				Int32 payThruYear;
				Int32 payThruPeriod;

				Int32.TryParse(_txtPayThruYear.Text, out payThruYear);
				Int32.TryParse(_txtPayThruPeriod.Text, out payThruPeriod);

				if (_divisionAccruals.Count(da => da.Year == payThruYear && da.Period == payThruPeriod) == 0)
				{
					errorText = "No accruals exist for the specified year && period";
				}
			}

			_errorProvider.SetError(textBox, errorText);
		}
	}
}

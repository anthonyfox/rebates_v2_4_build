﻿namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	partial class frmPaymentBreakdown
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this._grpTotals = new System.Windows.Forms.GroupBox();
			this._txtOutstanding = new System.Windows.Forms.TextBox();
			this._lblOutstanding = new System.Windows.Forms.Label();
			this._btnAddAssignment = new System.Windows.Forms.Button();
			this._btnAssignDefaults = new System.Windows.Forms.Button();
			this._txtAssigned = new System.Windows.Forms.TextBox();
			this._txtPayments = new System.Windows.Forms.TextBox();
			this._lblAssigned = new System.Windows.Forms.Label();
			this._lblPayments = new System.Windows.Forms.Label();
			this._grpAssignments = new System.Windows.Forms.GroupBox();
			this._dgvBreakdowns = new System.Windows.Forms.DataGridView();
			this._grpTotals.SuspendLayout();
			this._grpAssignments.SuspendLayout();
			((System.ComponentModel.ISupportInitialize) (this._dgvBreakdowns)).BeginInit();
			this.SuspendLayout();
			// 
			// _grpTotals
			// 
			this._grpTotals.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this._grpTotals.Controls.Add(this._txtOutstanding);
			this._grpTotals.Controls.Add(this._lblOutstanding);
			this._grpTotals.Controls.Add(this._btnAddAssignment);
			this._grpTotals.Controls.Add(this._btnAssignDefaults);
			this._grpTotals.Controls.Add(this._txtAssigned);
			this._grpTotals.Controls.Add(this._txtPayments);
			this._grpTotals.Controls.Add(this._lblAssigned);
			this._grpTotals.Controls.Add(this._lblPayments);
			this._grpTotals.Location = new System.Drawing.Point(12, 12);
			this._grpTotals.Name = "_grpTotals";
			this._grpTotals.Size = new System.Drawing.Size(513, 131);
			this._grpTotals.TabIndex = 0;
			this._grpTotals.TabStop = false;
			this._grpTotals.Text = "Payment Totals";
			// 
			// _txtOutstanding
			// 
			this._txtOutstanding.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
			this._txtOutstanding.Location = new System.Drawing.Point(156, 93);
			this._txtOutstanding.Name = "_txtOutstanding";
			this._txtOutstanding.ReadOnly = true;
			this._txtOutstanding.Size = new System.Drawing.Size(76, 20);
			this._txtOutstanding.TabIndex = 7;
			this._txtOutstanding.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _lblOutstanding
			// 
			this._lblOutstanding.AutoSize = true;
			this._lblOutstanding.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
			this._lblOutstanding.Location = new System.Drawing.Point(25, 96);
			this._lblOutstanding.Name = "_lblOutstanding";
			this._lblOutstanding.Size = new System.Drawing.Size(125, 13);
			this._lblOutstanding.TabIndex = 6;
			this._lblOutstanding.Text = "Balance Outstanding";
			// 
			// _btnAddAssignment
			// 
			this._btnAddAssignment.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._btnAddAssignment.Location = new System.Drawing.Point(307, 54);
			this._btnAddAssignment.Name = "_btnAddAssignment";
			this._btnAddAssignment.Size = new System.Drawing.Size(191, 23);
			this._btnAddAssignment.TabIndex = 5;
			this._btnAddAssignment.Text = "Add Assignment";
			this._btnAddAssignment.UseVisualStyleBackColor = true;
			// 
			// _btnAssignDefaults
			// 
			this._btnAssignDefaults.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._btnAssignDefaults.Location = new System.Drawing.Point(307, 29);
			this._btnAssignDefaults.Name = "_btnAssignDefaults";
			this._btnAssignDefaults.Size = new System.Drawing.Size(191, 23);
			this._btnAssignDefaults.TabIndex = 4;
			this._btnAssignDefaults.Text = "Assign To Default Rebate Payee\'s";
			this._btnAssignDefaults.UseVisualStyleBackColor = true;
			// 
			// _txtAssigned
			// 
			this._txtAssigned.Location = new System.Drawing.Point(156, 56);
			this._txtAssigned.Name = "_txtAssigned";
			this._txtAssigned.ReadOnly = true;
			this._txtAssigned.Size = new System.Drawing.Size(76, 20);
			this._txtAssigned.TabIndex = 3;
			this._txtAssigned.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtPayments
			// 
			this._txtPayments.Location = new System.Drawing.Point(156, 30);
			this._txtPayments.Name = "_txtPayments";
			this._txtPayments.ReadOnly = true;
			this._txtPayments.Size = new System.Drawing.Size(76, 20);
			this._txtPayments.TabIndex = 2;
			this._txtPayments.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _lblAssigned
			// 
			this._lblAssigned.AutoSize = true;
			this._lblAssigned.Location = new System.Drawing.Point(51, 58);
			this._lblAssigned.Name = "_lblAssigned";
			this._lblAssigned.Size = new System.Drawing.Size(99, 13);
			this._lblAssigned.TabIndex = 1;
			this._lblAssigned.Text = "Assigned Payments";
			// 
			// _lblPayments
			// 
			this._lblPayments.AutoSize = true;
			this._lblPayments.Location = new System.Drawing.Point(22, 33);
			this._lblPayments.Name = "_lblPayments";
			this._lblPayments.Size = new System.Drawing.Size(128, 13);
			this._lblPayments.TabIndex = 0;
			this._lblPayments.Text = "Total Proposed Payments";
			// 
			// _grpAssignments
			// 
			this._grpAssignments.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this._grpAssignments.Controls.Add(this._dgvBreakdowns);
			this._grpAssignments.Location = new System.Drawing.Point(12, 149);
			this._grpAssignments.Name = "_grpAssignments";
			this._grpAssignments.Size = new System.Drawing.Size(513, 311);
			this._grpAssignments.TabIndex = 1;
			this._grpAssignments.TabStop = false;
			this._grpAssignments.Text = "Payment Assignments";
			// 
			// _dgvBreakdowns
			// 
			this._dgvBreakdowns.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this._dgvBreakdowns.Dock = System.Windows.Forms.DockStyle.Fill;
			this._dgvBreakdowns.Location = new System.Drawing.Point(3, 16);
			this._dgvBreakdowns.Name = "_dgvBreakdowns";
			this._dgvBreakdowns.Size = new System.Drawing.Size(507, 292);
			this._dgvBreakdowns.TabIndex = 0;
			// 
			// frmPaymentBreakdown
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(537, 472);
			this.Controls.Add(this._grpAssignments);
			this.Controls.Add(this._grpTotals);
			this.Name = "frmPaymentBreakdown";
			this.Text = "Payment Breakdown";
			this._grpTotals.ResumeLayout(false);
			this._grpTotals.PerformLayout();
			this._grpAssignments.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize) (this._dgvBreakdowns)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox _grpTotals;
		private System.Windows.Forms.TextBox _txtAssigned;
		private System.Windows.Forms.TextBox _txtPayments;
		private System.Windows.Forms.Label _lblAssigned;
		private System.Windows.Forms.Label _lblPayments;
		private System.Windows.Forms.GroupBox _grpAssignments;
		private System.Windows.Forms.DataGridView _dgvBreakdowns;
		private System.Windows.Forms.Button _btnAssignDefaults;
		private System.Windows.Forms.Button _btnAddAssignment;
		private System.Windows.Forms.TextBox _txtOutstanding;
		private System.Windows.Forms.Label _lblOutstanding;
	}
}
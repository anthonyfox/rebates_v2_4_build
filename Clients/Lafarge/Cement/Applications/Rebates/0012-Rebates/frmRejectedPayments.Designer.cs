﻿namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	partial class frmRejectedPayments
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBoxSelection = new System.Windows.Forms.GroupBox();
			this._btnFind = new System.Windows.Forms.Button();
			this._dtpTo = new System.Windows.Forms.DateTimePicker();
			this._labelTo = new System.Windows.Forms.Label();
			this._dtpFrom = new System.Windows.Forms.DateTimePicker();
			this._labelFrom = new System.Windows.Forms.Label();
			this.grpRejectedPayments = new System.Windows.Forms.GroupBox();
			this._dgvRejectedPayments = new System.Windows.Forms.DataGridView();
			this.groupBoxSelection.SuspendLayout();
			this.grpRejectedPayments.SuspendLayout();
			((System.ComponentModel.ISupportInitialize) (this._dgvRejectedPayments)).BeginInit();
			this.SuspendLayout();
			// 
			// groupBoxSelection
			// 
			this.groupBoxSelection.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBoxSelection.Controls.Add(this._btnFind);
			this.groupBoxSelection.Controls.Add(this._dtpTo);
			this.groupBoxSelection.Controls.Add(this._labelTo);
			this.groupBoxSelection.Controls.Add(this._dtpFrom);
			this.groupBoxSelection.Controls.Add(this._labelFrom);
			this.groupBoxSelection.Location = new System.Drawing.Point(12, 12);
			this.groupBoxSelection.Name = "groupBoxSelection";
			this.groupBoxSelection.Size = new System.Drawing.Size(656, 72);
			this.groupBoxSelection.TabIndex = 0;
			this.groupBoxSelection.TabStop = false;
			this.groupBoxSelection.Text = "Payment Selection";
			// 
			// _btnFind
			// 
			this._btnFind.Location = new System.Drawing.Point(553, 29);
			this._btnFind.Name = "_btnFind";
			this._btnFind.Size = new System.Drawing.Size(75, 23);
			this._btnFind.TabIndex = 4;
			this._btnFind.Text = "Find";
			this._btnFind.UseVisualStyleBackColor = true;
			// 
			// _dtpTo
			// 
			this._dtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this._dtpTo.Location = new System.Drawing.Point(206, 28);
			this._dtpTo.Name = "_dtpTo";
			this._dtpTo.Size = new System.Drawing.Size(95, 20);
			this._dtpTo.TabIndex = 3;
			// 
			// _labelTo
			// 
			this._labelTo.AutoSize = true;
			this._labelTo.Location = new System.Drawing.Point(180, 31);
			this._labelTo.Name = "_labelTo";
			this._labelTo.Size = new System.Drawing.Size(20, 13);
			this._labelTo.TabIndex = 2;
			this._labelTo.Text = "To";
			// 
			// _dtpFrom
			// 
			this._dtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this._dtpFrom.Location = new System.Drawing.Point(69, 28);
			this._dtpFrom.Name = "_dtpFrom";
			this._dtpFrom.Size = new System.Drawing.Size(95, 20);
			this._dtpFrom.TabIndex = 1;
			// 
			// _labelFrom
			// 
			this._labelFrom.AutoSize = true;
			this._labelFrom.Location = new System.Drawing.Point(33, 31);
			this._labelFrom.Name = "_labelFrom";
			this._labelFrom.Size = new System.Drawing.Size(30, 13);
			this._labelFrom.TabIndex = 0;
			this._labelFrom.Text = "From";
			// 
			// grpRejectedPayments
			// 
			this.grpRejectedPayments.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.grpRejectedPayments.Controls.Add(this._dgvRejectedPayments);
			this.grpRejectedPayments.Location = new System.Drawing.Point(12, 90);
			this.grpRejectedPayments.Name = "grpRejectedPayments";
			this.grpRejectedPayments.Size = new System.Drawing.Size(656, 326);
			this.grpRejectedPayments.TabIndex = 1;
			this.grpRejectedPayments.TabStop = false;
			this.grpRejectedPayments.Text = "Rejected Payments";
			// 
			// _dgvRejectedPayments
			// 
			this._dgvRejectedPayments.AllowUserToAddRows = false;
			this._dgvRejectedPayments.AllowUserToDeleteRows = false;
			this._dgvRejectedPayments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this._dgvRejectedPayments.Dock = System.Windows.Forms.DockStyle.Fill;
			this._dgvRejectedPayments.Location = new System.Drawing.Point(3, 16);
			this._dgvRejectedPayments.Name = "_dgvRejectedPayments";
			this._dgvRejectedPayments.ReadOnly = true;
			this._dgvRejectedPayments.Size = new System.Drawing.Size(650, 307);
			this._dgvRejectedPayments.TabIndex = 0;
			// 
			// frmRejectedPayments
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(680, 428);
			this.Controls.Add(this.grpRejectedPayments);
			this.Controls.Add(this.groupBoxSelection);
			this.Name = "frmRejectedPayments";
			this.Text = "Rejected Payments";
			this.groupBoxSelection.ResumeLayout(false);
			this.groupBoxSelection.PerformLayout();
			this.grpRejectedPayments.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize) (this._dgvRejectedPayments)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBoxSelection;
		private System.Windows.Forms.DateTimePicker _dtpTo;
		private System.Windows.Forms.Label _labelTo;
		private System.Windows.Forms.DateTimePicker _dtpFrom;
		private System.Windows.Forms.Label _labelFrom;
		private System.Windows.Forms.GroupBox grpRejectedPayments;
		private System.Windows.Forms.DataGridView _dgvRejectedPayments;
		private System.Windows.Forms.Button _btnFind;
	}
}
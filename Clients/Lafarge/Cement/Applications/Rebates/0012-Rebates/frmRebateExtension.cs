﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Libraries.Utilities;
using Evolve.Libraries.Data.Remoting;

namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	public partial class frmRebateExtension : Form
	{
		private IBindingList _rebateExtensions;
		private BindingSource _bindingSource;
		private Type _rebateExtensionType;

		public frmRebateExtension(Type rebateExtensionType)
		{
			InitializeComponent();

			_rebateExtensionType = rebateExtensionType;
			this.Load += new EventHandler(frmRebateExtension_Load);
		}

		void frmRebateExtension_Load(object sender, EventArgs e)
		{
			// Setup icon
			if (this.MdiParent != null)
			{
				this.Icon = this.MdiParent.Icon;
			}

			// Load existing Delivery Window Details
			IRebateExtension rebateExtension = (IRebateExtension) Activator.CreateInstance(_rebateExtensionType);
			rebateExtension.DivisionId = Program.Division.Id;
			_rebateExtensions = rebateExtension.Read();

			// Bind data
			_bindingSource = new BindingSource();
			_bindingSource.DataSource = _rebateExtensions;
			_bindingNavigator.BindingSource = _bindingSource;
			_dgvRebateExtensions.DataSource = _bindingSource;

			// Configure data grid view
			_dgvRebateExtensions.ReadOnly = true;
			_dgvRebateExtensions.AllowUserToAddRows = false;
			_dgvRebateExtensions.AllowUserToDeleteRows = true;
			_dgvRebateExtensions.MultiSelect = false;
			_dgvRebateExtensions.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			_dgvRebateExtensions.UserDeletingRow += new DataGridViewRowCancelEventHandler(_dgvRebateExtensions_UserDeletingRow);
			_dgvRebateExtensions.DoubleClick += new EventHandler(_dgvRebateExtensions_DoubleClick);
			_dgvRebateExtensions.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(_dgvRebateExtensions_DataBindingComplete);

			// Setup form text
			this.Text = rebateExtension.Title + " Maintenance";
		}

		void _dgvRebateExtensions_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			_dgvRebateExtensions.Columns["JDECodeName"].Visible = false;
		}

		void _dgvRebateExtensions_DoubleClick(object sender, EventArgs e)
		{
			if (_bindingSource.Current != null)
			{
				frmRebateExtensionDetail detail = new frmRebateExtensionDetail(_rebateExtensionType);
				detail.MdiParent = this.MdiParent;
				detail.RebateExtension = (IRebateExtension) _bindingSource.Current;
				detail.Show();
			}
		}

		void _dgvRebateExtensions_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
		{
			e.Cancel = DeleteRow();
		}

		private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
		{
			frmRebateExtensionDetail add = new frmRebateExtensionDetail(_rebateExtensionType);
			add.MdiParent = this.MdiParent;
			add.FormClosed += new FormClosedEventHandler(add_FormClosed);
			add.Show();
		}

		void add_FormClosed(object sender, FormClosedEventArgs e)
		{
			frmRebateExtensionDetail form = (frmRebateExtensionDetail) sender;

			if (form.DialogResult == DialogResult.OK)
			{
				_rebateExtensions.Add(form.RebateExtension);
			}
		}

		private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
		{
			if (DeleteRow())
			{
				_bindingSource.RemoveCurrent();
			}
		}

		private Boolean DeleteRow()
		{
			Boolean isDeleted = false;

			if (_bindingSource.Current != null)
			{
				IRebateExtension rebateExtension = (IRebateExtension) _bindingSource.Current;
				Boolean isDeleteAllowed = true;

				// Check if any related rebates
				isDeleteAllowed = CheckRelatedData(rebateExtension, "Rebate");

				// Check if any related other data (invoice or otherwise)
				if (isDeleteAllowed)
				{
					isDeleteAllowed = CheckRelatedData(rebateExtension, "");
				}

				// Perform delete
				if (isDeleteAllowed)
				{
					try
					{
						rebateExtension.Delete();
						isDeleted = true;
					}
					catch (DatabaseException databaseException)
					{
						if (databaseException.IsLockError)
						{
							MessageBox.Show("Row locked by another user, retry later", "Delete " + rebateExtension.Title, MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						else
						{
							throw databaseException;
						}
					}
				}
			}

			return isDeleted;
		}

		private static Boolean CheckRelatedData(IRebateExtension rebateExtension, String prefix)
		{
			Boolean isDeleteAllowed = true;
			
			// Get type of rebate extension
			Type rebateExtensionType = rebateExtension.GetType();
	
			// Get related entity property details
			PropertyInfo relatedEntityName = rebateExtensionType.GetProperty(prefix + "EntityName");
			PropertyInfo relatedEntityTitle = rebateExtensionType.GetProperty(prefix + "EntityTitle");
			PropertyInfo relatedEntityProperty = rebateExtensionType.GetProperty(prefix + "PropertyName");

			// Get type details and property for related entity, and create an instance of the object
			String typeName = "Evolve.Clients.Lafarge.Cement.Business.Rebates." + relatedEntityName.GetValue(rebateExtension, null) + ", Evolve.Clients.Lafarge.Cement.Business";
			Type entityType = Type.GetType(typeName);
			Entity entity = (Entity) Activator.CreateInstance(entityType);
			PropertyInfo divisionIdProperty = entityType.GetProperty("DivisionId");
			PropertyInfo rebateExtensionIdProperty = entityType.GetProperty(relatedEntityProperty.GetValue(rebateExtension, null).ToString());

			// Check if the related entity has any matching rows
			if (divisionIdProperty != null)
			{
				divisionIdProperty.SetValue(entity, Program.Division.Id, null);
				entity.AddReadFilter("DivisionId", "=");
			}

			rebateExtensionIdProperty.SetValue(entity, rebateExtension.Id, null);
			entity.AddReadFilter(rebateExtension.PropertyName, "=");

			Int32 count = entity.ReadCount();

			// Check row count and respond accordingly
			if (count > 0)
			{
				isDeleteAllowed = false;
				MessageBox.Show(relatedEntityTitle.GetValue(rebateExtension, null).ToString() + (count > 1 ? "s exist" : " exists") + " for this " + rebateExtension.Title, "Deletion Aborted", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}

			return isDeleteAllowed;
		}
	}
}

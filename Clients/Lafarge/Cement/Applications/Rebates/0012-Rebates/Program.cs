﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.WinformsApplication;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	static class Program
	{
		public static Division Division = new Division();
		public static User User = new User();
		public static String Environment = "";

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(String[] arguments)
		{
			try
			{
				Application.EnableVisualStyles();
				Application.SetCompatibleTextRenderingDefault(false);

				ApplicationDomain.DefineUnhandledExceptionHandler();
				ApplicationArguments.ProcessWindowsLaunch(arguments, "", isDirectAccess: true);

				Environment = arguments[0];

				if (UserAuthentification.Credentials != null)
				{
					if (ValidateUser())
					{
						Application.Run(new frmMDIParent());
					}
				}
			}
			catch (Exception ex)
			{
				ApplicationError applicationError = new ApplicationError(ex);
			}
		}

		static private Boolean ValidateUser()
		{
			String errorMessage = "";

			// Read users that match tis user code
			User user = new User();
			user.Code = UserAuthentification.Credentials.WindowsUserId;
			ExtendedBindingList<User> users = (ExtendedBindingList<User>) user.ReadWithDirtyProperty();

			switch(users.Count)
			{
				case 0:
					// Auto-create new user
					DataAccessServer.NewPersistentConnection();
					DataAccessServer.PersistentConnection.BeginTransaction();

					user.Name = user.Code;
					user.Password = "";
					user.IsActive = true;

					Int32 id = user.AcceptChanges();

					if (id > 0)
					{
						UserDivision userDivision = new UserDivision();
						userDivision.UserId = id;
						userDivision.DivisionId = 1;
						userDivision.AcceptChanges();
					}

					DataAccessServer.PersistentConnection.CommitTransaction();
					DataAccessServer.TerminatePersistentConnection();

					Program.User = user;
					SetupDivision(Program.User);
					break;

				case 1:
					// Assign user details
					Program.User = users[0];

					if (Program.User.IsActive)
					{
						errorMessage = SetupDivision(Program.User);
					}
					else
					{
						errorMessage = "User (" + Program.User.Code + ") " + Program.User.Name + " is inactive";
					}

					break;

				default:
					errorMessage = "PANIC: Multiple " + user.Code + " users found";
					break;
			}

			if(errorMessage.Length > 0)
			{
				MessageBox.Show(errorMessage, "Validate User", MessageBoxButtons.OK, MessageBoxIcon.Stop);
			}

			return errorMessage.Length == 0;
		}

		static private String SetupDivision(User user)
		{
			string errorMessage = "";

			// Read divisions for this user
			ExtendedBindingList<UserDivision> userDivisions = user.UserDivisions;

			if (userDivisions == null)
			{
				errorMessage = "Failed to read user division details";
			} 
			else
			{
				switch (userDivisions.Count)
				{
					case 0:
						// When we have multiple divisions, this should be an error, but for now we can
						// auto create an entry

						//errorMessage = "User (" + user.Code.ToString() + ") " + user.Name + " has no divisions set up";

						UserDivision userDivision = new UserDivision();

						userDivision.DivisionId = 1;
						userDivision.UserId = user.Id;
						userDivision.AcceptChanges();

						Program.Division.Id = 1;

						break;
					case 1:
						Program.Division.Id = userDivisions[0].DivisionId;
						break;
					default:
						// We ought to pop up a list and let the user choose, but as we've only one division at present, we'll write that code later
						errorMessage = "User (" + user.Code.ToString() + ") " + user.Name + " has multiple divisions set up\n\nThe system does support that that yet";
						break;
				}

				// No errors, so read the division details
				if (errorMessage.Length == 0)
				{
					if (!Program.Division.ReadCurrent())
					{
						errorMessage = "Failed to read Division (Id = " + Program.Division.Id.ToString() + ")";
					}
				}
			}

			return errorMessage;
		}
	}
}

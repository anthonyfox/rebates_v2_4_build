﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;

namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	public partial class frmForecast : Form
	{
		private Timer _timer = new Timer();
		private AccrualCalculation _accrualCalculation;
		private frmDisplayAccrual _frmDisplayAccrual;

		public frmForecast()
		{
			InitializeComponent();

			_lnkLastCalculation.Tag = _lnkLastCalculation.Text;
			SetupLastRun();

			_timer.Interval = 60000;
			_timer.Tick += new EventHandler(_timer_Tick);
			_timer.Start();

			_btnCalculate.Click += new EventHandler(_btnCalculate_Click);
			_lnkLastCalculation.Click += new EventHandler(_lnkLastCalculation_Click);

			_pnlCalculationStatus.Visible = false;
			this.Size = new Size(this.Width, this.Height - _pnlCalculationStatus.Height);
		}

		void _timer_Tick(object sender, EventArgs e)
		{
			SetupLastRun();
		}

		private void SetupLastRun()
		{
			Program.Division.RefreshForecastTime();
			DateTime? forecastTime = Program.Division.ForecastTime;

			if (forecastTime.HasValue)
			{
				_lnkLastCalculation.Text = _lnkLastCalculation.Tag.ToString().Replace("%LastDate%", forecastTime.Value.ToShortDateString() + " " + forecastTime.Value.ToShortTimeString());
				_lnkLastCalculation.Visible = true;
			}
			else
			{
				_lnkLastCalculation.Visible = false;
			}
		}

		void _lnkLastCalculation_Click(object sender, EventArgs e)
		{
			DisplayAccrual();
		}

		void _btnCalculate_Click(object sender, EventArgs e)
		{
			// Display progress bars
			if (!_pnlCalculationStatus.Visible)
			{
				_pnlCalculationStatus.Visible = true;
				this.Size = new Size(this.Width, this.Height + _pnlCalculationStatus.Height);
			}

			// Disable buttons
			_btnCalculate.Enabled = false;
			Application.DoEvents();

			// Set cursor to processing
			this.Cursor = Cursors.WaitCursor;

			if (Program.Division.StartCalculation(Program.User, AccrualTypeEnum.Forecast))
			{
				// Close existing accrual calculation display
				CloseDisplayAccrual();

				// Process
				CalculationPeriod accrualPeriod = Program.Division.LastAccrualPeriod();

				_accrualCalculation = new AccrualCalculation(Program.User, Program.Division, accrualPeriod, AccrualTypeEnum.Forecast, true);
				_accrualCalculation.RebateProcessed += new AccrualCalculation.RebateProcessedHandler(AccrualCalculation_RebateProcessed);

				if (_accrualCalculation.Calculate())
				{
					SetupLastRun();
					DisplayAccrual();
				}

				// Close the accrual calculation run
				Program.Division.EndCalculation(Program.User, AccrualTypeEnum.Forecast);
			}
			else
			{
				User user = new User();
				user.Id = Program.Division.ForecastUserId.Value;
				user.ReadCurrent();

				DateTime time = Program.Division.ForecastTime.Value;
				MessageBox.Show(user.Name + " started a forecast accrual calculation at " + time.ToShortTimeString() + " on " + time.ToShortDateString() + "\nPlease re-try later", "Accrual Calculation In Progress", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}

			_btnCalculate.Enabled = true;

			// Set cursor to processing completed
			this.Cursor = Cursors.Default;
		}

		private void DisplayAccrual()
		{
			_frmDisplayAccrual = new frmDisplayAccrual(AccrualTypeEnum.Forecast, null, false, null);
			_frmDisplayAccrual.MdiParent = this.MdiParent;
			_frmDisplayAccrual.Show();
		}

		private void CloseDisplayAccrual()
		{
			if (_frmDisplayAccrual != null)
			{
				_frmDisplayAccrual.Close();
				_frmDisplayAccrual.Dispose();
				_frmDisplayAccrual = null;
			}
		}

		#region AccrualCalculation RebateProcessed Event

		private delegate void DisplayProcessedCallBack();
		private void AccrualCalculation_RebateProcessed(object sender, EventArgs e)
		{
			if (this.InvokeRequired)
			{
				DisplayProcessedCallBack d = new DisplayProcessedCallBack(DisplayProcessed);
				this.Invoke(d, null);
			}
			else
			{
				DisplayProcessed();
			}
		}

		private void DisplayProcessed()
		{
			_lblStage.Text = _accrualCalculation.StageText;
			_pgbStage.Value = _accrualCalculation.StagePercentageComplete;
			_pgbProgress.Value = _accrualCalculation.PercentComplete;

			Application.DoEvents();
		}

		#endregion

	}
}

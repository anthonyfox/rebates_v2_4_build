﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	public partial class frmComment : Form
	{
		public String Comment
		{
			get { return _textBox.Text; }
		}

		public frmComment()
		{
			InitializeComponent();

			this.DialogResult = DialogResult.Cancel;

			_textBox.Validating += new CancelEventHandler(_textBox_Validating);
			_okCancel.ButtonClick += new Libraries.Controls.Generic.OkCancel.ButtonClickHandler(_okCancel_ButtonClick);
			this.FormClosing += new FormClosingEventHandler(frmComment_FormClosing);
		}

		void _textBox_Validating(object sender, CancelEventArgs e)
		{
			_errorProvider.SetError(_textBox, (_textBox.Text.Length > 0 ? "" : "Comments can not be left blank"));
		}

		void frmComment_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (this.DialogResult == DialogResult.OK)
			{
				this.ValidateChildren();
				e.Cancel = ValidateControl.HasErrors(this, _errorProvider);
			}
		}

		void _okCancel_ButtonClick(object sender, EventArgs e)
		{
			this.DialogResult = _okCancel.DialogResult;
			this.Close();
		}
	}
}

﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	public partial class frmPaymentAssignment : Form
	{
		private PaymentSummary _paymentSummary;
		private PaymentBreakdown _paymentBreakdown;
		private String _numberFormat = "#,0.00";
		private String _percentFormat = "0.0";

		public Payee Payee
		{
			get { return _ucPayee.SelectedValue; }
		}

		public Decimal? Value
		{
			get
			{
				return (_txtValue.Text.Length == 0 ? null : (Decimal?) Decimal.Parse(_txtValue.Text));
			}
		}

		public Decimal? Percentage
		{
			get
			{
				return (_txtPercentage.Text.Length == 0 ? null : (Decimal?) Decimal.Parse(_txtPercentage.Text));
			}
		}

		public TaxCode TaxCode
		{
			get
			{
				return _ucTaxCode.SelectedValue;
			}
		}

		public DateTime TaxDate
		{
			get
			{
				return _dtpTaxDate.Value;
			}
		}

		public String Comment
		{
			get 
			{ 
				return _txtComment.Text; 
			}
		}

		public frmPaymentAssignment(PaymentSummary paymentSummary)
		{
			Initialize(paymentSummary, null);
		}

		public frmPaymentAssignment(PaymentSummary paymentSummary, PaymentBreakdown paymentBreakdown)
		{
			Initialize(paymentSummary, paymentBreakdown);
		}

		private void Initialize(PaymentSummary paymentSummary, PaymentBreakdown paymentBreakdown)
		{
			InitializeComponent();

			_paymentSummary = paymentSummary;
			_paymentBreakdown = paymentBreakdown;

			_ucPayee.DivisionId = Program.Division.Id;
			_ucPayee.MultiSelect = false;
			_ucPayee.Validating += new CancelEventHandler(_ucPayee_Validating);
			_ucPayee.PayeeSelected += new EventHandler(_ucPayee_PayeeSelected);
			_txtValue.Validating += new CancelEventHandler(_txtValue_Validating);
			_txtPercentage.Validating += new CancelEventHandler(_txtPercentage_Validating);
			_ucTaxCode.Populate();
			_dtpTaxDate.Value = DateTime.Today;

			this.Load += new EventHandler(frmPaymentAssignment_Load);
			_btnAdd.Click += new EventHandler(_btnAdd_Click);
		}

		void _ucPayee_PayeeSelected(object sender, EventArgs e)
		{
			Payee payee = _ucPayee.SelectedValue;
			_ucTaxCode.IsZeroRateOnly = String.IsNullOrEmpty(payee.TaxNumber);
		}

		void _ucPayee_Validating(object sender, CancelEventArgs e)
		{
			if (_ucPayee.SelectedValue == null)
			{
				_errorProvider.SetError(_ucPayee, "Payee must be specified");
			}
			else if (_paymentBreakdown == null && _paymentSummary.PaymentBreakdowns.Count(pb => pb.PayeeId == _ucPayee.SelectedValue.Id && pb.TaxCodeId == _ucTaxCode.SelectedValue.Id) > 0)
			{
				_errorProvider.SetError(_ucPayee, "Payment record for this payee and tax rate already exists");
			}
			else
			{
				_errorProvider.SetError(_ucPayee, "");
			}
		}

		void _txtValue_Validating(object sender, CancelEventArgs e)
		{
			TextBox textBox = (TextBox) sender;
			String errorMessage = "";

			if (textBox.Text != "")
			{
				Decimal value;

				if (Decimal.TryParse(textBox.Text, out value))
				{
					Decimal unAssignedPaymentAmount = _paymentSummary.UnAssignedPaymentAmount + (_paymentBreakdown == null ? 0 : _paymentBreakdown.Value);

					if (value > unAssignedPaymentAmount)
					{
						value = unAssignedPaymentAmount;
					}

					textBox.Text = value.ToString(_numberFormat);
				}
				else
				{
					errorMessage = "Invalid value";
				}
			}

			_errorProvider.SetError(textBox, errorMessage);
		}

		void _txtPercentage_Validating(object sender, CancelEventArgs e)
		{
			TextBox textBox = (TextBox) sender;
			String errorMessage = "";

			if (textBox.Text != "")
			{
				Decimal value;

				if (Decimal.TryParse(textBox.Text, out value))
				{
					if (value < 0 || value > 100)
					{
						errorMessage = "Invalid percentage value";
					}

					textBox.Text = value.ToString(_percentFormat);
				}
				else
				{
					errorMessage = "Invalid percentage value";
				}
			}

			_errorProvider.SetError(_lblPercent, errorMessage);
		}

		void frmPaymentAssignment_Load(object sender, EventArgs e)
		{
			if (this.MdiParent != null)
			{
				this.Icon = this.MdiParent.Icon;
			}

			if (_paymentBreakdown != null)
			{
				_ucPayee.Id = _paymentBreakdown.PayeeId;
				_ucPayee.Enabled = false;
				_txtValue.Text = _paymentBreakdown.Value.ToString(_numberFormat);
				_ucTaxCode.FindAndSelectId(_paymentBreakdown.TaxCodeId);
				_txtComment.Text = _paymentBreakdown.Comment;
			}
		}

		void _btnAdd_Click(object sender, EventArgs e)
		{
			Control control = ValidateControl.ControlWithError(this, _errorProvider);

			if (control != null)
			{
				control.Focus();
			}
			else
			{
				if (_txtValue.Text != "" && _txtPercentage.Text != "")
				{
					_errorProvider.SetError(_txtValue, "You cannot specify both a value and a percentage");
					_txtValue.Focus();
				}
				else if (!(String.IsNullOrEmpty(_txtValue.Text) && String.IsNullOrEmpty(_txtPercentage.Text)))
				{
					this.DialogResult = DialogResult.OK;
					this.Close();
				}
			}
		}
	}
}

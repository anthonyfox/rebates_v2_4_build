﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Evolve.Libraries.Controls.Generic;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Libraries.Utilities;
using Evolve.Libraries.Data.Remoting;
using Evolve.Clients.Lafarge.Cement.UserControls.Rebates;

namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	public partial class frmRebateFindResults : Form
	{
		#region Properties

		private Boolean _isPaymentSearch = false;
		private CalculationPeriod _payThruPeriod;
		private BindingSource _bindingSource;
		private RebateFind _rebateFind;
		private EntityListView<Rebate> _rebateView;
		private String _numberFormat = "#,0.00";
		private DataGridViewCheckBoxHeaderCell _isSelectedHeader = new DataGridViewCheckBoxHeaderCell(true);
		private CheckBox _chkShowAll = new CheckBox();
		private ToolStripControlHost _toolStripControlHost;

		public Boolean IsPaymentSearch
		{
			get { return _isPaymentSearch; }
			set { _isPaymentSearch = value; }
		}

		public CalculationPeriod PayThruPeriod
		{
			get { return _payThruPeriod; }
			set
			{
				_payThruPeriod = value;
				SetPayThruText();
			}
		}

		#endregion

		public frmRebateFindResults(RebateFind rebateFind)
		{
			InitializeComponent();

			_rebateFind = rebateFind;
			_rebateView = new EntityListView<Rebate>(_rebateFind.Rebates);

			_bindingSource = new BindingSource();
			_bindingSource.DataSource = _rebateView;

			_bindingNavigator.BindingSource = _bindingSource;

			_dgvSearchResults.DataSource = _bindingSource;
			_dgvSearchResults.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			_dgvSearchResults.MultiSelect = true;
			_dgvSearchResults.ReadOnly = false;

			bindingNavigatorPay.Click += new EventHandler(bindingNavigatorPay_Click);

			this.Load += new EventHandler(frmRebateFindResults_Load);

			_dgvSearchResults.CellDoubleClick += new DataGridViewCellEventHandler(_dgvSearchResults_CellDoubleClick);
			_dgvSearchResults.DataError += new DataGridViewDataErrorEventHandler(DataGridViewUtilities.DataError);
			_dgvSearchResults.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(_dgvSearchResults_DataBindingComplete);
			_isSelectedHeader.OnCheckBoxClicked += new CheckBoxClickedHandler(_isSelectedHeader_OnCheckBoxClicked);

			bindingNavigatorButtonRefresh.Click += new EventHandler(bindingNavigatorButtonRefresh_Click);

			// Configure show all checkbox on toolbar
			_chkShowAll.Text = "";
			_chkShowAll.Checked = false;
			_chkShowAll.CheckedChanged += new EventHandler(_chkShowAll_CheckedChanged);

			_toolStripControlHost = new ToolStripControlHost(_chkShowAll);
			_bindingNavigator.Items.Insert(_bindingNavigator.Items.Count - 2, _toolStripControlHost);
		}

		void _chkShowAll_CheckedChanged(object sender, EventArgs e)
		{
			SetFilter();
		}

		void bindingNavigatorRebateToggle_Click(object sender, EventArgs e)
		{
			SetFilter();
		}

		void _isSelectedHeader_OnCheckBoxClicked(bool state)
		{
			foreach (Rebate rebate in _rebateFind.Rebates)
			{
				rebate.IsSelected = state;
			}
		}

		void _dgvSearchResults_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			DataGridView dataGridView = (DataGridView) sender;

			DataGridViewUtilities.SetColumnFormat(dataGridView.Columns[RebateProperty.VAPActivationVolume], _numberFormat, DataGridViewContentAlignment.MiddleRight);
			DataGridViewUtilities.SetColumnFormat(dataGridView.Columns[RebateProperty.EstimatedVolumeString], _numberFormat, DataGridViewContentAlignment.MiddleRight);
			DataGridViewUtilities.SetColumnFormat(dataGridView.Columns[RebateProperty.FixedRebateValueString], _numberFormat, DataGridViewContentAlignment.MiddleRight);
			DataGridViewUtilities.SetColumnFormat(dataGridView.Columns[RebateProperty.AccrualValue], _numberFormat, DataGridViewContentAlignment.MiddleRight);
			DataGridViewUtilities.SetColumnFormat(dataGridView.Columns[RebateProperty.PaymentValue], _numberFormat, DataGridViewContentAlignment.MiddleRight);
			DataGridViewUtilities.SetColumnFormat(dataGridView.Columns[RebateProperty.ClosedValue], _numberFormat, DataGridViewContentAlignment.MiddleRight);
			DataGridViewUtilities.SetColumnFormat(dataGridView.Columns[RebateProperty.Balance], _numberFormat, DataGridViewContentAlignment.MiddleRight);

			DataGridViewUtilities.SetColumnFormat(dataGridView.Columns[RebateProperty.IsSelected], "", DataGridViewContentAlignment.MiddleCenter);
			DataGridViewUtilities.SetColumnFormat(dataGridView.Columns[RebateProperty.IsForecast], "", DataGridViewContentAlignment.MiddleCenter);
			DataGridViewUtilities.SetColumnFormat(dataGridView.Columns[RebateProperty.IsAdjustment], "", DataGridViewContentAlignment.MiddleCenter);
			DataGridViewUtilities.SetColumnFormat(dataGridView.Columns[RebateProperty.IsJif], "", DataGridViewContentAlignment.MiddleCenter);
			DataGridViewUtilities.SetColumnFormat(dataGridView.Columns[RebateProperty.IsContract], "", DataGridViewContentAlignment.MiddleCenter);
			DataGridViewUtilities.SetColumnFormat(dataGridView.Columns[RebateProperty.IsVAP], "", DataGridViewContentAlignment.MiddleCenter);
			DataGridViewUtilities.SetColumnFormat(dataGridView.Columns[RebateProperty.IsAccrued], "", DataGridViewContentAlignment.MiddleCenter);

			DataGridViewColumn isSelectedColumn = dataGridView.Columns[RebateProperty.IsSelected];

			if (_isPaymentSearch)
			{
				// Ensure header cell is setup correctly
				isSelectedColumn.Width = 20;
				isSelectedColumn.HeaderCell = _isSelectedHeader;
				isSelectedColumn.DisplayIndex = 0;
			}
			else
			{
				// Remove the IsSelected checkbox column if we are not in payments mode
				if (isSelectedColumn != null)
				{
					dataGridView.Columns.Remove(isSelectedColumn);
				}
			}

			// Setup read only columns
			foreach (DataGridViewColumn column in dataGridView.Columns)
			{
				column.ReadOnly = (column.DataPropertyName == RebateProperty.IsSelected ? !_isPaymentSearch : true);
			}

			dataGridView.Columns[RebateProperty.Number].Frozen = true;
		}

		void bindingNavigatorButtonRefresh_Click(object sender, EventArgs e)
		{
			RefreshList();
		}

		private void SetPayThruText()
		{
			if (_payThruPeriod != null)
			{
				bindingNavigatorPayThruPeriod.Text = _payThruPeriod.ToString();
			}
		}

		void bindingNavigatorPay_Click(object sender, EventArgs e)
		{
			_dgvSearchResults.EndEdit();

			if (_rebateFind.Rebates.Count(r => r.IsSelected && (r.HasPaymentOutstanding || _chkShowAll.Checked)) > 0)
			{
				frmPayment frmPayment = new frmPayment(_rebateFind.Rebates, _payThruPeriod, _chkShowAll.Checked);
				frmPayment.MdiParent = this.MdiParent;

				this.Cursor = Cursors.WaitCursor;
				frmPayment.Show();
				this.Cursor = Cursors.Default;
			}
		}

		void frmRebateFindResults_Load(object sender, EventArgs e)
		{
			if (this.MdiParent != null)
			{
				this.Icon = this.MdiParent.Icon;
			}

			if (this.IsPaymentSearch)
			{
				this.Text = "Rebate Payment Selection";
			}
			else
			{
				bindingNavigatorSeparator3.Visible = false;
				bindingNavigatorSeparator4.Visible = false;
				bindingNavigatorPayThruPeriodLabel.Visible = false;
				bindingNavigatorPayThruPeriod.Visible = false;
				bindingNavigatorPay.Visible = false;
				toolStripShowAllLabel.Visible = false;
				_toolStripControlHost.Visible = false;
			}

			SetPayThruText();
			SetFilter();
		}

		void _dgvSearchResults_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
		{
			DataGridView dataGridView = (DataGridView) sender;

			if (e.RowIndex >= 0)
			{
				this.Cursor = Cursors.WaitCursor;

				Rebate rebate = (Rebate) dataGridView.CurrentRow.DataBoundItem;
				frmMDIParent mdiParent = (frmMDIParent) this.MdiParent;
				Boolean isNew = false;
				Form form = mdiParent.OpenSingleInstance(typeof(frmRebate), new Object[] { rebate }, out isNew, rebate.WindowTitle);

				if (isNew)
				{
					form.FormClosed += new FormClosedEventHandler(frmRebate_FormClosed);
				}

				this.Cursor = Cursors.Default;
			}
		}

		void frmRebate_FormClosed(object sender, FormClosedEventArgs e)
		{
			frmRebate frmRebate = (frmRebate) sender;

			if (frmRebate.DialogResult == DialogResult.OK)
			{
				RefreshList();
			}
		}

		private void RefreshList()
		{
			this.Cursor = Cursors.WaitCursor;

			_rebateFind.RefreshList();
			_rebateView = new EntityListView<Rebate>(_rebateFind.Rebates);
			_bindingSource.DataSource = _rebateView;

			SetFilter();

			this.Cursor = Cursors.Default;
		}

		private void SetFilter()
		{
			if (_chkShowAll.Checked || !_isPaymentSearch)
			{
				_rebateView.Filter = "";
			}
			else
			{
				_rebateView.Filter = "HasPaymentOutstanding = true";
			}
		}

		public void Copy()
		{
			Rebate rebate = (Rebate) _bindingSource.Current;

			if (rebate != null && rebate.Id != 0)
			{
				Rebate rebateCopy = rebate.Copy(Program.User);

				frmRebate frmRebate = new frmRebate(rebateCopy);
				frmRebate.MdiParent = this.MdiParent;
				frmRebate.Show();
				frmRebate.Activate();
			}
		}
	}
}

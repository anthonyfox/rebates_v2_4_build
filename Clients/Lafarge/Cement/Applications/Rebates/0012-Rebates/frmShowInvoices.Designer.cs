﻿namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	partial class frmShowInvoices
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this._dgvInvoices = new System.Windows.Forms.DataGridView();
			((System.ComponentModel.ISupportInitialize) (this._dgvInvoices)).BeginInit();
			this.SuspendLayout();
			// 
			// _dgvInvoices
			// 
			this._dgvInvoices.AllowUserToAddRows = false;
			this._dgvInvoices.AllowUserToDeleteRows = false;
			this._dgvInvoices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this._dgvInvoices.Dock = System.Windows.Forms.DockStyle.Fill;
			this._dgvInvoices.Location = new System.Drawing.Point(0, 0);
			this._dgvInvoices.Name = "_dgvInvoices";
			this._dgvInvoices.ReadOnly = true;
			this._dgvInvoices.Size = new System.Drawing.Size(816, 365);
			this._dgvInvoices.TabIndex = 0;
			// 
			// frmShowInvoices
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(816, 365);
			this.Controls.Add(this._dgvInvoices);
			this.Name = "frmShowInvoices";
			this.Text = "Accrual Invoices";
			((System.ComponentModel.ISupportInitialize) (this._dgvInvoices)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.DataGridView _dgvInvoices;
	}
}
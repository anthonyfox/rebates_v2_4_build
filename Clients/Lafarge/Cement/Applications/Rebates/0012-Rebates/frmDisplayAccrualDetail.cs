﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	public partial class frmDisplayAccrualDetail : Form
	{
		private Int32 _accrualId;
		private frmDisplayAccrualDetail _frmDisplayAccrualDetail = null;
		private String _numberFormat = "#,0.00";

		public frmDisplayAccrualDetail(Int32 accrualId, Int32? parentHierarchyId, Int32? parentHierarchyEntityId)
		{
			InitializeComponent();

			_accrualId = accrualId;
			ExtendedBindingList<AccrualDetail> accrualDetails = LoadAccrualDetails(parentHierarchyId, parentHierarchyEntityId);

			if (accrualDetails == null || accrualDetails.Count == 0)
			{
				this.DialogResult = DialogResult.Cancel;
			}
			else
			{
				_dgvAccrualDetail.DataSource = accrualDetails;
				_dgvAccrualDetail.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
				_dgvAccrualDetail.DoubleClick += new EventHandler(_dgvAccrualDetail_DoubleClick);
				_dgvAccrualDetail.Click += new EventHandler(_dgvAccrualDetail_Click);
				_dgvAccrualDetail.DataError += new DataGridViewDataErrorEventHandler(DataGridViewUtilities.DataError);
				_dgvAccrualDetail.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(_dgvAccrualDetail_DataBindingComplete);

				// ReSharper disable once ConditionIsAlwaysTrueOrFalse
				if (AccrualCalculation.AreAccrualInvoicesSaved)
				{
					DataGridViewButtonColumn invoiceButton = new DataGridViewButtonColumn();
					invoiceButton.UseColumnTextForButtonValue = true;
					invoiceButton.Text = "Show Invoices";
					invoiceButton.Name = "ShowInvoices";
					invoiceButton.HeaderText = "";

					_dgvAccrualDetail.Columns.Add(invoiceButton);
				}

				_dgvAccrualDetail.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
			}

			this.Load += new EventHandler(frmDisplayAccrualDetail_Load);
			this.FormClosing += new FormClosingEventHandler(frmDisplayAccrualDetail_FormClosing);
			this.Resize += new EventHandler(frmDisplayAccrualDetail_Resize);
		}

		void frmDisplayAccrualDetail_Load(object sender, EventArgs e)
		{
			if (this.MdiParent != null)
			{
				this.Icon = this.MdiParent.Icon;
			}
		}

		void frmDisplayAccrualDetail_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (_frmDisplayAccrualDetail != null)
			{
				_frmDisplayAccrualDetail.Close();
				_frmDisplayAccrualDetail.Dispose();
				_frmDisplayAccrualDetail = null;
			}
		}

		void frmDisplayAccrualDetail_Resize(object sender, EventArgs e)
		{
			_dgvAccrualDetail.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
		}

		void _dgvAccrualDetail_DoubleClick(object sender, EventArgs e)
		{
			if (_dgvAccrualDetail.CurrentRow != null && _dgvAccrualDetail.CurrentRow.Index >= 0)
			{
				Int32? parentHierarchyId = ((AccrualDetail) _dgvAccrualDetail.CurrentRow.DataBoundItem).HierarchyId;
				Int32? parentHierarchyEntityId = ((AccrualDetail) _dgvAccrualDetail.CurrentRow.DataBoundItem).HierarchyEntityId;

				if (parentHierarchyId.HasValue)
				{
					_frmDisplayAccrualDetail = new frmDisplayAccrualDetail(_accrualId, parentHierarchyId, parentHierarchyEntityId);
					_frmDisplayAccrualDetail.MdiParent = this.MdiParent;

					if (_frmDisplayAccrualDetail.DialogResult != DialogResult.Cancel)
					{
						_frmDisplayAccrualDetail.Show();
					}
					else
					{
						_frmDisplayAccrualDetail.Close();
						_frmDisplayAccrualDetail.Dispose();
						_frmDisplayAccrualDetail = null;
					}
				}
			}
		}

		void _dgvAccrualDetail_Click(object sender, EventArgs e)
		{
			Int32 row = _dgvAccrualDetail.CurrentCell.RowIndex;
			Int32 column = _dgvAccrualDetail.CurrentCell.ColumnIndex;

			if (row >= 0 && _dgvAccrualDetail.Columns[column].Name == "ShowInvoices")
			{
				AccrualDetail accrualDetail = (AccrualDetail) _dgvAccrualDetail.CurrentRow.DataBoundItem;
				frmShowInvoices frmShowInvoices = new frmShowInvoices(accrualDetail.Id);
				frmShowInvoices.MdiParent = this.MdiParent;
				frmShowInvoices.Show();
			}
		}

		void _dgvAccrualDetail_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			ExtendedBindingList<AccrualDetail> accrualDetails = (ExtendedBindingList<AccrualDetail>) _dgvAccrualDetail.DataSource;

			if (accrualDetails.Count > 0)
			{
				if (accrualDetails[0].DeliveryWindowId.HasValue)
				{
					// Accrual details relate to a delivery window rebate type
					_dgvAccrualDetail.Columns[AccrualDetailProperty.AttributeName1].Visible = false;
					_dgvAccrualDetail.Columns[AccrualDetailProperty.AttributeName2].Visible = false;
					_dgvAccrualDetail.Columns[AccrualDetailProperty.AttributeName3].Visible = false;
					_dgvAccrualDetail.Columns[AccrualDetailProperty.AttributeName4].Visible = false;
					_dgvAccrualDetail.Columns[AccrualDetailProperty.ProductCode].Visible = false;
					_dgvAccrualDetail.Columns[AccrualDetailProperty.ProductName].Visible = false;
				}
				else
				{
					// Accrual details relate to a standard rebate type
					_dgvAccrualDetail.Columns[AccrualDetailProperty.DeliveryWindowCode].Visible = false;
					_dgvAccrualDetail.Columns[AccrualDetailProperty.PackingTypeEnum].Visible = false;
				}
			}

			DataGridViewUtilities.SetColumnFormat(_dgvAccrualDetail.Columns[AccrualDetailProperty.Rate], _numberFormat, DataGridViewContentAlignment.MiddleRight);
			DataGridViewUtilities.SetColumnFormat(_dgvAccrualDetail.Columns[AccrualDetailProperty.SalesValue], _numberFormat, DataGridViewContentAlignment.MiddleRight);
			DataGridViewUtilities.SetColumnFormat(_dgvAccrualDetail.Columns[AccrualDetailProperty.SalesVolume], _numberFormat, DataGridViewContentAlignment.MiddleRight);
			DataGridViewUtilities.SetColumnFormat(_dgvAccrualDetail.Columns[AccrualDetailProperty.Value], _numberFormat, DataGridViewContentAlignment.MiddleRight);
		}

		private ExtendedBindingList<AccrualDetail> LoadAccrualDetails(Int32? parentHierarchyId, Int32? parentHierarchyEntityId)
		{
			Hierarchy hierarchy = new Hierarchy();
			ExtendedBindingList<Hierarchy> hierarchys = (ExtendedBindingList<Hierarchy>) hierarchy.Read();
			ExtendedBindingList<AccrualDetail> accrualDetails = null;

			Hierarchy joinHierarchy = null;

			if (parentHierarchyId.HasValue)
			{
				Hierarchy parentHierarchy = hierarchys.Find(parentHierarchyId.Value);
				joinHierarchy = hierarchys.GetChild(parentHierarchy.Level);
			}
			else
			{
				Accrual accrual = new Accrual();
				
				accrual.Id = _accrualId;
				accrual.ReadCurrent();

				joinHierarchy = hierarchys.Find(accrual.HierarchyId);
			}

			if (joinHierarchy != null)
			{
				this.Text += " (" + joinHierarchy.Name + ")";

				AccrualDetail accrualDetail = new AccrualDetail();
				accrualDetail.AddHierarchyEntityJoin(joinHierarchy);

				accrualDetail.AccrualId = _accrualId;
				accrualDetail.ParentHierarchyId = parentHierarchyId;
				accrualDetail.ParentHierarchyEntityId = parentHierarchyEntityId;

				accrualDetail.AddReadFilter("AccrualId", "=");
				accrualDetail.AddReadFilter("ParentHierarchyId", "=");
				accrualDetail.AddReadFilter("ParentHierarchyEntityId", "=");

				accrualDetails = (ExtendedBindingList<AccrualDetail>) accrualDetail.Read();
			}

			return accrualDetails;
		}
	}
}

﻿namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	partial class frmTaxCode
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this._lblCode = new System.Windows.Forms.Label();
			this._txtCode = new System.Windows.Forms.TextBox();
			this._panelTop = new System.Windows.Forms.Panel();
			this._lblIsZeroRate = new System.Windows.Forms.Label();
			this._chkIsZeroRate = new System.Windows.Forms.CheckBox();
			this._panelBottom = new System.Windows.Forms.Panel();
			this._okCancel = new Evolve.Libraries.Controls.Generic.OkCancel();
			this._errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
			this._panelTop.SuspendLayout();
			this._panelBottom.SuspendLayout();
			((System.ComponentModel.ISupportInitialize) (this._errorProvider)).BeginInit();
			this.SuspendLayout();
			// 
			// _lblCode
			// 
			this._lblCode.AutoSize = true;
			this._lblCode.Location = new System.Drawing.Point(50, 23);
			this._lblCode.Name = "_lblCode";
			this._lblCode.Size = new System.Drawing.Size(32, 13);
			this._lblCode.TabIndex = 0;
			this._lblCode.Text = "Code";
			// 
			// _txtCode
			// 
			this._txtCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this._txtCode.Location = new System.Drawing.Point(88, 20);
			this._txtCode.MaxLength = 10;
			this._txtCode.Name = "_txtCode";
			this._txtCode.Size = new System.Drawing.Size(118, 20);
			this._txtCode.TabIndex = 1;
			// 
			// _panelTop
			// 
			this._panelTop.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this._panelTop.Controls.Add(this._lblIsZeroRate);
			this._panelTop.Controls.Add(this._chkIsZeroRate);
			this._panelTop.Controls.Add(this._txtCode);
			this._panelTop.Controls.Add(this._lblCode);
			this._panelTop.Location = new System.Drawing.Point(0, 0);
			this._panelTop.Name = "_panelTop";
			this._panelTop.Size = new System.Drawing.Size(232, 77);
			this._panelTop.TabIndex = 0;
			// 
			// _lblIsZeroRate
			// 
			this._lblIsZeroRate.AutoSize = true;
			this._lblIsZeroRate.Location = new System.Drawing.Point(16, 46);
			this._lblIsZeroRate.Name = "_lblIsZeroRate";
			this._lblIsZeroRate.Size = new System.Drawing.Size(66, 13);
			this._lblIsZeroRate.TabIndex = 4;
			this._lblIsZeroRate.Text = "Is Zero Rate";
			// 
			// _chkIsZeroRate
			// 
			this._chkIsZeroRate.AutoSize = true;
			this._chkIsZeroRate.Location = new System.Drawing.Point(88, 46);
			this._chkIsZeroRate.Name = "_chkIsZeroRate";
			this._chkIsZeroRate.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._chkIsZeroRate.Size = new System.Drawing.Size(15, 14);
			this._chkIsZeroRate.TabIndex = 5;
			this._chkIsZeroRate.UseVisualStyleBackColor = true;
			// 
			// _panelBottom
			// 
			this._panelBottom.Controls.Add(this._okCancel);
			this._panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
			this._panelBottom.Location = new System.Drawing.Point(0, 83);
			this._panelBottom.Name = "_panelBottom";
			this._panelBottom.Size = new System.Drawing.Size(232, 52);
			this._panelBottom.TabIndex = 1;
			// 
			// _okCancel
			// 
			this._okCancel.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._okCancel.CausesValidation = false;
			this._okCancel.Location = new System.Drawing.Point(63, 16);
			this._okCancel.Name = "_okCancel";
			this._okCancel.Size = new System.Drawing.Size(157, 24);
			this._okCancel.TabIndex = 0;
			// 
			// _errorProvider
			// 
			this._errorProvider.ContainerControl = this;
			// 
			// frmTaxCode
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(232, 135);
			this.Controls.Add(this._panelBottom);
			this.Controls.Add(this._panelTop);
			this.Name = "frmTaxCode";
			this.Text = "Tax Code";
			this._panelTop.ResumeLayout(false);
			this._panelTop.PerformLayout();
			this._panelBottom.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize) (this._errorProvider)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label _lblCode;
		private System.Windows.Forms.TextBox _txtCode;
		private System.Windows.Forms.Panel _panelTop;
		private System.Windows.Forms.Panel _panelBottom;
		private Libraries.Controls.Generic.OkCancel _okCancel;
		private System.Windows.Forms.ErrorProvider _errorProvider;
		private System.Windows.Forms.Label _lblIsZeroRate;
		private System.Windows.Forms.CheckBox _chkIsZeroRate;
	}
}
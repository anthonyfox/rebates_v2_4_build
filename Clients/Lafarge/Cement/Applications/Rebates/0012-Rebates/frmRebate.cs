﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Libraries.Utilities;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.WinformsApplication;

namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	public partial class frmRebate : Form
	{
		#region Properties
		// Display formats for numbers
		private String _numberFormat = "#,0.00";
		private String _integerFormat = "#,0";

		// Preserve details about tabpages which get dynamically added or removed
		private TabPage _tabPageDetail;
		private TabPage _tabPageVAP;
		private TabPage _tabPageRebateExtension;
		private TabPage _tabPageSoldFrom;
		private TabPage _tabPageSoldTo;
		private TabPage _tabPageShipTo;

		// Preserve rebate data
		private ExtendedBindingList<Site> _includeSoldFroms;
		private ExtendedBindingList<Site> _excludeSoldFroms;
		private ExtendedBindingList<RebateExceptionCustomer> _excludeSoldTos;
		private ExtendedBindingList<RebateExceptionCustomer> _excludeShipTos;
		private Rebate _rebate;

		// General
		private RebateEditMode _editMode = RebateEditMode.Full; 
		private Control _ucHierarchyRelatedControl;
		private PropertyInfo _hiearchyRelatedValueProperty;

		private Boolean _isNew = false;
		private Boolean _isDirty = false;
		private String _originalDataGridViewCellValue;
		private String _originalValue;
		private Boolean _isPricingGroupAllowed = false;

		private ApplicationStatus _applicationStatus;
		#endregion

		#region Constructors
		public frmRebate()
		{
			this.DialogResult = DialogResult.Cancel;
			DataAccessServer.NewPersistentConnection();

			try
			{
				this.Cursor = Cursors.WaitCursor;

				InitializeComponent();
				InitialiseForNewRebate();
				InitialiseForm(true);
			}
			catch (DatabaseException databaseException)
			{
				if (databaseException.IsLockError)
				{
					MessageBox.Show("Failed to read details - records locked", "New Rebate Detail", MessageBoxButtons.OK, MessageBoxIcon.Error);
					this.DialogResult = DialogResult.Abort;
				}
				else
				{
					throw databaseException;
				}
			}
			finally
			{
				DataAccessServer.TerminatePersistentConnection();
				this.Cursor = Cursors.Default;
			}

			MarkFormAsClean();
		}

		public frmRebate(Rebate rebate)
		{
			this.DialogResult = DialogResult.Cancel;
			DataAccessServer.NewPersistentConnection();

			try
			{
				this.Cursor = Cursors.WaitCursor;
				InitializeComponent();

				if (rebate.Id == 0)
				{
					// Copied rebates will have an Id of zero and already be a new instance
					_rebate = rebate;
					_rebate.SetColumnDirty();
					MarkFormAsDirty();
				}
				else
				{
					// Clone rebate so aborted changes are lost
					_rebate = (Rebate) rebate.Clone();
					MarkFormAsClean();
				}

				SetupRebateExceptions();
				InitialiseForm(false);
			}
			catch (DatabaseException databaseException)
			{
				if (databaseException.IsLockError)
				{
					MessageBox.Show("Failed to read rebate details - records locked", "Read Rebate Details", MessageBoxButtons.OK, MessageBoxIcon.Error);
					this.DialogResult = DialogResult.Abort;
				}
				else
				{
					throw databaseException;
				}
			}
			finally
			{
				DataAccessServer.TerminatePersistentConnection();
				this.Cursor = Cursors.Default;
			}
		}
		#endregion

		private void SetupRebateExceptions()
		{
			#region Rebate Exception (Sold From)

			_includeSoldFroms = new ExtendedBindingList<Site>();
			_excludeSoldFroms = new ExtendedBindingList<Site>();

			ExtendedBindingList<Site> sites = (ExtendedBindingList<Site>) (new Site()).Read();

			foreach (RebateException exception in _rebate.RebateExceptions.Where(re => re.ExceptionTypeEnum == ExceptionTypeEnum.SoldFrom))
			{
				Site site = sites.First(s => s.Id == exception.ExceptionId);
				_excludeSoldFroms.Add(site);
				sites.Remove(site);
			}

			_includeSoldFroms = sites;

			#endregion

			#region Rebate Exception (Sold To)

			RebateExceptionCustomer soldTo = new RebateExceptionCustomer();
			soldTo.RebateId = _rebate.Id;
			soldTo.ExceptionTypeEnum = ExceptionTypeEnum.SoldTo;
			_excludeSoldTos = (ExtendedBindingList<RebateExceptionCustomer>) soldTo.ReadWithDirtyProperty();

			#endregion

			#region Rebate Exception (Ship To)

			RebateExceptionCustomer shipTo = new RebateExceptionCustomer();
			shipTo.RebateId = _rebate.Id;
			shipTo.ExceptionTypeEnum = ExceptionTypeEnum.ShipTo;
			_excludeShipTos = (ExtendedBindingList<RebateExceptionCustomer>) shipTo.ReadWithDirtyProperty();

			#endregion
		}

		private void InitialiseForm(Boolean isNew)
		{
			_isNew = isNew || _rebate.Id == 0;

			// Store references to TabPages which may need to be removed (as you cannot hide
			// tabpages)
			_tabPageDetail = _tabControl.TabPages[TabPageName.Detail];
			_tabPageVAP = _tabControl.TabPages[TabPageName.VAP];
			_tabPageRebateExtension = _tabControl.TabPages[TabPageName.RebateExtension];
			_tabPageSoldFrom = _tabControl.TabPages[TabPageName.SoldFrom];
			_tabPageSoldTo = _tabControl.TabPages[TabPageName.SoldTo];
			_tabPageShipTo = _tabControl.TabPages[TabPageName.ShipTo];

			#region VAP

			if (_rebate.IsVAP)
			{
				_ucVAPActivationMethod.SelectedValue = (_rebate.VAPActivationVolume == null ? VAPActivationMethod.Individual : VAPActivationMethod.Global);
				_txtVAPActivationQuantity.Text = _rebate.VAPActivationVolume.ToString();
			}

			#endregion

			#region Rebate Header
			if (!isNew)
			{
				_txtRebateNumber.Text = _rebate.Number.ToString();
				_txtVersion.Text = _rebate.Version.ToString();
				_txtDescription.Text = _rebate.Description;
			}

			_ucCustomerGroup.DivisionId = Program.Division.Id;
			_ucCustomerGroup.IncludeBlankOption = true;
			_ucCustomerGroup.Populate();

			_ucPricingSubGroup.DivisionId = Program.Division.Id;
			_ucPricingSubGroup.IncludeAllOption = true;
			_ucPricingSubGroup.Populate();

			_ucHierarchy.DivisionId = Program.Division.Id;
			_ucHierarchy.IncludeAllOption = true;
			_ucHierarchy.AllItemText = "<Please select>";
			_ucHierarchy.Populate();

			_ucCustomer.DivisionId = Program.Division.Id;
	
			if (!isNew)
			{
				_ucHierarchy.FindAndSelectId(_rebate.HierarchyId);
				_ucPricingSubGroup.FindAndSelectId(_rebate.PricingSubGroupId);

				switch(_ucHierarchy.SelectedValue.RelatedEntityName)
				{
					case "CustomerGroup":
						_ucCustomerGroup.FindAndSelectId(_rebate.HierarchyEntityId);
						_isPricingGroupAllowed = true;
						break;
					case "Customer":
						_ucCustomer.Id = _rebate.HierarchyEntityId;
						_isPricingGroupAllowed = false;
						break;
				}
			}

			_ucPricingSubGroup.Visible = _isPricingGroupAllowed;

			_ucPayee.DivisionId = Program.Division.Id;
			if (!isNew && _rebate.PayeeId.HasValue)
			{
				_ucPayee.Id = (Int32) _rebate.PayeeId;
			}

			if(!isNew)
			{
				_ucDeliveryType.SelectedValue = _rebate.DeliveryTypeEnum;
			}

			_ucSoldFrom.DivisionId = Program.Division.Id;
			_ucSoldFrom.IncludeAllOption = true;
			_ucSoldFrom.Populate();

			if (!isNew)
			{
				_ucSoldFrom.FindAndSelectId(_rebate.SiteId);
			}

			SetDefaultStartAndEndDate(isNew);

			if (!isNew)
			{
				_chkIsContract.Checked = _rebate.IsContract;
				_chkIsVAP.Checked = _rebate.IsVAP;
				_chkIsGlobalProduct.Checked = _rebate.IsGlobalProduct;

				if (_rebate.IsContract)
				{
					_txtContractText.Text = _rebate.ContractText;
				}

				if (_rebate.IsGlobalProduct)
				{
					_txtGlobalRate.Text = _rebate.GlobalRate.ToString();
				}

				_chkIsJIF.Checked = _rebate.IsJif;
				_chkIsAdjustment.Checked = _rebate.IsAdjustment;
			}

			_ucRebateType.Populate();
			if (isNew)
			{
				_ucRebateType.SetDefault();
			}
			else
			{
				_ucRebateType.FindAndSelectId(_rebate.RebateTypeId);
			}

			_ucPaymentFrequency.DivisionId = Program.Division.Id;
			_ucPaymentFrequency.IncludeAllOption = true;
			_ucPaymentFrequency.AllItemText = "<Please select>";
			_ucPaymentFrequency.Populate();

			if (!isNew)
			{
				_ucPaymentFrequency.FindAndSelectId(_rebate.PaymentFrequencyId);
			}

			if (!isNew)
			{
				_txtEstimatedVolume.Text = _rebate.EstimatedVolumeString;
				_txtFixedValue.Text = _rebate.FixedRebateValueString;
			}

			#endregion

			#region Rebate Detail
			_ucProductDetail.DivisionId = Program.Division.Id;
			_ucProductDetail.MultiSelect = true;

			_ucAttribute1.DivisionId = Program.Division.Id;
			_ucAttribute1.Level = 1;
			_ucAttribute1.IncludeBlank = true;
			_ucAttribute1.Populate();

			_ucAttribute2.DivisionId = Program.Division.Id;
			_ucAttribute2.Level = 2;
			_ucAttribute2.IncludeBlank = true;
			_ucAttribute2.Populate();

			_ucAttribute3.DivisionId = Program.Division.Id;
			_ucAttribute3.Level = 3;
			_ucAttribute3.IncludeBlank = true;
			_ucAttribute3.Populate();

			_ucAttribute4.DivisionId = Program.Division.Id;
			_ucAttribute4.Level = 4;
			_ucAttribute4.IncludeBlank = true;
			_ucAttribute4.Populate();

			_ucProductVAP.DivisionId = Program.Division.Id;
			_ucProductVAP.MultiSelect = true;

			_dgvDetail.AllowUserToAddRows = false;
			_dgvDetail.AllowUserToDeleteRows = true;
			#endregion

			#region Rebate Delivery Window
			_ucDeliveryWindow.DivisionId = Program.Division.Id;
			if (!isNew)
			{
				foreach (RebateDeliveryWindow rebateDeliveryWindow in _rebate.RebateDeliveryWindows.Where(rdw => rdw.PackingTypeEnum == _ucPackingType.SelectedValue))
				{
					_ucDeliveryWindow.ExcludeIds.Add(rebateDeliveryWindow.DeliveryWindowId);
				}
			}
			_ucDeliveryWindow.Populate();

			_dgvRebateExtension.AllowUserToAddRows = false;
			_dgvRebateExtension.AllowUserToDeleteRows = true;
			#endregion

			#region Rebate Lead Time
			_ucLeadTime.DivisionId = Program.Division.Id;
			if (!isNew)
			{
				foreach (RebateLeadTime rebateLeadTime in _rebate.RebateLeadTimes.Where(rdw => rdw.PackingTypeEnum == _ucPackingType.SelectedValue))
				{
					_ucLeadTime.ExcludeIds.Add(rebateLeadTime.LeadTimeId);
				}
			}
			_ucLeadTime.Populate();

			_dgvRebateExtension.AllowUserToAddRows = false;
			_dgvRebateExtension.AllowUserToDeleteRows = true;
			#endregion

			#region Rebate Exception (Sold To)
			_ucSoldToCustomer.DivisionId = Program.Division.Id;
			_ucSoldToCustomer.MultiSelect = true;

			_dgvSoldTo.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			_dgvSoldTo.ReadOnly = true;
			_dgvSoldTo.AllowUserToAddRows = false;
			_dgvSoldTo.AllowUserToDeleteRows = true;
			_dgvSoldTo.AllowUserToOrderColumns = false;
			#endregion

			#region Rebate Exception (Ship To)
			_ucShipToCustomer.DivisionId = Program.Division.Id;
			_ucShipToCustomer.MultiSelect = true;

			_dgvShipTo.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			_dgvShipTo.ReadOnly = true;
			_dgvShipTo.AllowUserToAddRows = false;
			_dgvShipTo.AllowUserToDeleteRows = true;
			_dgvShipTo.AllowUserToOrderColumns = false;
			#endregion

			#region Setup event handlers : Rebate Header
			_txtRebateNumber.DataBindings.Add("Text", _rebate, RebateProperty.Number);
			_txtVersion.DataBindings.Add("Text", _rebate, RebateProperty.Version);
			_txtDescription.Validating += new CancelEventHandler(_txtDescription_Validating);
			_txtDescription.Enter +=new EventHandler(TextBox_Enter);
			_txtDescription.Leave += new EventHandler(TextBox_Leave);
			_ucHierarchy.ComboBox.SelectedIndexChanged += new EventHandler(_ucHierarchy_SelectedIndexChanged);
			_ucHierarchy.Validating += new CancelEventHandler(_ucHierarchy_Validating);
			_ucCustomerGroup.ComboBox.SelectedIndexChanged += new EventHandler(_ucCustomerGroup_SelectedIndexChanged);
			_ucPricingSubGroup.ComboBox.SelectedIndexChanged += new EventHandler(MarkFormAsDirty);
			_ucCustomer.CustomerSelected += new EventHandler(_ucCustomer_CustomerSelected);
			_ucCustomer.AccountNumberTextBox.TextChanged += new EventHandler(MarkFormAsDirty);
			_ucPayee.AccountNumberTextBox.TextChanged +=new EventHandler(MarkFormAsDirty);
			_ucDeliveryType.ComboBox.SelectedIndexChanged +=new EventHandler(MarkFormAsDirty);
			_ucSoldFrom.ComboBox.SelectedIndexChanged += new EventHandler(_ucSoldFrom_SelectedIndexChanged);
			_dtpStartDate.Validating += new CancelEventHandler(_dtpStartEndDate_Validating);
			_dtpStartDate.ValueChanged +=new EventHandler(MarkFormAsDirty);
			_dtpEndDate.Validating += new CancelEventHandler(_dtpStartEndDate_Validating);
			_dtpEndDate.ValueChanged += new EventHandler(MarkFormAsDirty);
			_chkIsContract.CheckedChanged += new EventHandler(_chkIsContract_CheckedChanged);
			_txtContractText.Validating += new CancelEventHandler(_txtContractText_Validating);
			_txtContractText.Enter += new EventHandler(TextBox_Enter);
			_txtContractText.Leave += new EventHandler(TextBox_Leave);
			_chkIsVAP.CheckedChanged += new EventHandler(_isVAP_CheckedChanged);
			_chkIsGlobalProduct.CheckedChanged += new EventHandler(_chkIsGlobalProduct_CheckedChanged);
			_txtGlobalRate.Validating += new CancelEventHandler(_txtGlobalRate_Validating);
			_txtGlobalRate.Enter += new EventHandler(TextBox_Enter);
			_txtGlobalRate.Leave += new EventHandler(TextBox_Leave);
			_ucRebateType.ComboBox.SelectedIndexChanged += new EventHandler(_ucRebateType_SelectedIndexChanged);
			_ucPaymentFrequency.ComboBox.SelectedIndexChanged +=new EventHandler(MarkFormAsDirty);
			_ucPaymentFrequency.Validating += new CancelEventHandler(_ucPaymentFrequency_Validating);
			_txtEstimatedVolume.Validating += new CancelEventHandler(_txtEstimatedVolume_Validating);
			_txtEstimatedVolume.Enter += new EventHandler(TextBox_Enter);
			_txtEstimatedVolume.Leave += new EventHandler(TextBox_Leave);
			_txtFixedValue.Validating += new CancelEventHandler(_txtFixedValue_Validating);
			_txtFixedValue.Enter += new EventHandler(TextBox_Enter);
			_txtFixedValue.Leave += new EventHandler(TextBox_Leave);
			#endregion

			#region Setup event handlers : Rebate Ranges
			_dgvRange.Validating += new CancelEventHandler(_dgvRange_Validating);
			_dgvRange.DataError += new DataGridViewDataErrorEventHandler(_dgvRange_DataError);
			_dgvRange.CellBeginEdit += new DataGridViewCellCancelEventHandler(DataGridViewCellBeginEdit);
			_dgvRange.CellEndEdit += new DataGridViewCellEventHandler(DataGridViewCellEndEdit);
			_dgvRange.UserDeletedRow += new DataGridViewRowEventHandler(MarkFormAsDirty);
			_dgvRange.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(_dgvRange_DataBindingComplete);
			#endregion

			#region Setup event handlers : Rebate Detail
			_ucAttribute1.ComboBox.SelectedIndexChanged += new EventHandler(_ucAttribute_SelectedIndexChanged);
			_ucAttribute2.ComboBox.SelectedIndexChanged += new EventHandler(_ucAttribute_SelectedIndexChanged);
			_ucAttribute3.ComboBox.SelectedIndexChanged += new EventHandler(_ucAttribute_SelectedIndexChanged);
			_ucAttribute4.ComboBox.SelectedIndexChanged += new EventHandler(_ucAttribute_SelectedIndexChanged);
			_ucProductDetail.ProductSelected += new EventHandler(_ucProductDetail_ProductSelected);
			_ucProductDetail.MultiProductSelected += new EventHandler(_ucProductDetail_MultiProductSelected);
			_btnAddProductDetail.Click += new EventHandler(_btnAddProductDetail_Click);
			_btnProductDetailClear.Click += new EventHandler(_btnProductDetailClear_Click);
			_dgvDetail.CellBeginEdit += new DataGridViewCellCancelEventHandler(DataGridViewCellBeginEdit);
			_dgvDetail.CellEndEdit += new DataGridViewCellEventHandler(DataGridViewCellEndEdit);
			_dgvDetail.UserDeletingRow += new DataGridViewRowCancelEventHandler(_dgvDetail_UserDeletingRow);
			_dgvDetail.UserDeletedRow += new DataGridViewRowEventHandler(MarkFormAsDirty);
			_dgvDetail.UserAddedRow += new DataGridViewRowEventHandler(MarkFormAsDirty);
			_dgvDetail.DataError += new DataGridViewDataErrorEventHandler(_dgvDetail_DataError);
			_dgvDetail.Validating += new CancelEventHandler(_dgvDetail_Validating);
			_dgvDetail.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(_dgvDetail_DataBindingComplete);
			#endregion

			#region Setup event handlers : VAP
			_ucVAPActivationMethod.ComboBox.SelectedIndexChanged += new EventHandler(_ucVAPActivationMethod_SelectedIndexChanged);
			_txtVAPActivationQuantity.Validating += new CancelEventHandler(_txtVAPActivationQuantity_Validating);
			_txtVAPActivationQuantity.TextChanged += new EventHandler(MarkFormAsDirty);
			_ucProductVAP.ProductSelected += new EventHandler(_ucProductVAP_ProductSelected);
			_ucProductVAP.MultiProductSelected += new EventHandler(_ucProductVAP_MultiProductSelected);
			_btnVAPProductAdd.Click += new EventHandler(_btnVAPProductAdd_Click);
			_dgvVAP.CellBeginEdit += new DataGridViewCellCancelEventHandler(DataGridViewCellBeginEdit);
			_dgvVAP.CellEndEdit += new DataGridViewCellEventHandler(DataGridViewCellEndEdit);
			_dgvVAP.UserAddedRow += new DataGridViewRowEventHandler(MarkFormAsDirty);
			_dgvVAP.UserDeletedRow += new DataGridViewRowEventHandler(MarkFormAsDirty);
			_dgvVAP.DataError += new DataGridViewDataErrorEventHandler(DataGridViewUtilities.DataError);
			_dgvVAP.Validating += new CancelEventHandler(_dgvVAP_Validating);
			_dgvVAP.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(_dgvVAP_DataBindingComplete);
			#endregion

			#region Setup event handlers : Rebate Delivery Window
			_ucPackingType.ComboBox.SelectedIndexChanged += new EventHandler(_ucPackingType_SelectedIndexChanged);
			_btnAddRebateExtension.Click += new EventHandler(_btnAddDeliveryWindow_Click);
			_dgvRebateExtension.CellBeginEdit += new DataGridViewCellCancelEventHandler(DataGridViewCellBeginEdit);
			_dgvRebateExtension.CellEndEdit += new DataGridViewCellEventHandler(DataGridViewCellEndEdit);
			_dgvRebateExtension.UserAddedRow += new DataGridViewRowEventHandler(MarkFormAsDirty);
			_dgvRebateExtension.UserDeletedRow += new DataGridViewRowEventHandler(MarkFormAsDirty);
			_dgvRebateExtension.DataError += new DataGridViewDataErrorEventHandler(DataGridViewUtilities.DataError);
			_dgvRebateExtension.Validating += new CancelEventHandler(_dgvRebateExtension_Validating);
			_dgvRebateExtension.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(_dgvRebateExtension_DataBindingComplete);
			_dgvRebateExtension.UserDeletingRow += new DataGridViewRowCancelEventHandler(_dgvRebateExtension_UserDeletingRow);
			_dgvRebateExtension.DataError += new DataGridViewDataErrorEventHandler(_dgvRebateExtension_DataError);
			#endregion

			#region Setup event handler : Rebate Exception (Sold from)
			_btnSoldFromInclude.Click += new EventHandler(_btnSoldFromInclude_Click);
			_btnSoldFromExclude.Click += new EventHandler(_btnSoldFromExclude_Click);
			_lstSoldFromInclude.MouseDown += new MouseEventHandler(_lstSoldFrom_MouseDown);
			_lstSoldFromExclude.MouseDown += new MouseEventHandler(_lstSoldFrom_MouseDown);
			_lstSoldFromInclude.DragOver += new DragEventHandler(_lstSoldFrom_DragOver);
			_lstSoldFromExclude.DragOver += new DragEventHandler(_lstSoldFrom_DragOver);
			_lstSoldFromInclude.DragDrop += new DragEventHandler(_lstSoldFrom_DragDrop);
			_lstSoldFromExclude.DragDrop += new DragEventHandler(_lstSoldFrom_DragDrop);
			#endregion

			#region Setup event handlers : Rebate Exception (Sold To)
			_btnSoldToAdd.Click += new EventHandler(_btnSoldToAdd_Click);
			_ucSoldToCustomer.CustomerSelected += new EventHandler(_ucSoldToCustomer_CustomerSelected);
			_ucSoldToCustomer.MultiCustomerSelected += new EventHandler(_ucSoldToCustomer_MultiCustomerSelected);
			_dgvSoldTo.UserAddedRow += new DataGridViewRowEventHandler(MarkFormAsDirty);
			_dgvSoldTo.UserDeletedRow += new DataGridViewRowEventHandler(MarkFormAsDirty);
			_dgvSoldTo.DataError += new DataGridViewDataErrorEventHandler(DataGridViewUtilities.DataError);
			#endregion

			#region Setup event handlers : Rebate Exception (Ship To)
			_btnShipToAdd.Click += new EventHandler(_btnShipToAdd_Click);
			_ucShipToCustomer.CustomerSelected += new EventHandler(_ucShipToCustomer_CustomerSelected);
			_ucShipToCustomer.MultiCustomerSelected += new EventHandler(_ucShipToCustomer_MultiCustomerSelected);
			_dgvShipTo.UserAddedRow += new DataGridViewRowEventHandler(MarkFormAsDirty);
			_dgvShipTo.UserDeletedRow += new DataGridViewRowEventHandler(MarkFormAsDirty);
			_dgvShipTo.DataError += new DataGridViewDataErrorEventHandler(DataGridViewUtilities.DataError);
			
			#endregion

			#region Setup event handlers : Rebate Note
			_btnNoteAdd.Click += new EventHandler(_btnNoteAdd_Click);
			_dgvNotes.UserAddedRow += new DataGridViewRowEventHandler(MarkFormAsDirty);
			_dgvNotes.UserDeletedRow += new DataGridViewRowEventHandler(MarkFormAsDirty);
			_dgvNotes.DataError += new DataGridViewDataErrorEventHandler(DataGridViewUtilities.DataError);
			_dgvNotes.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(_dgvNotes_DataBindingComplete);
			_rebate.RebateNotes.ListChanged += new ListChangedEventHandler(RebateNotes_ListChanged);
			#endregion

			#region Setup event handlers : Form
			this.FormClosing +=new FormClosingEventHandler(Rebate_FormClosing);
			this.Load += new System.EventHandler(this.frmRebate_Load);
			this._btnClose.Click += new System.EventHandler(this._btnClose_Click);
			this._okCancel.ButtonClick += new Evolve.Libraries.Controls.Generic.OkCancel.ButtonClickHandler(_okCancel_ButtonClick);
			#endregion

			// Bind rebate data to controls
			SetupData();

			// Setup controls
			SetupHierarchy();
			SetupVAP();
			SetupContract();
			SetupGlobalProduct();
			SetupRebateType();

			DisplayTabPages();
			ValidateConfigurationData();
			SetupRecentNotes();
			GetFixedRate();
			SetEditMode();

			this.Text = _rebate.WindowTitle;
		}

		#region Event Handlers

		#region Form

		#region EVENT: frmRebate_Load
		/// <summary>
		/// For new rebates, mark the form as dirty, otherwise mark it as clean
		/// </summary>
		/// <param name="sender">Form</param>
		/// <param name="e">Event arguments</param>
		private void frmRebate_Load(object sender, EventArgs e)
		{
			if (this.MdiParent != null)
			{
				frmMDIParent frmMDIParent = (frmMDIParent) this.MdiParent;
				this.Icon = frmMDIParent.Icon;
				_applicationStatus = frmMDIParent.ApplicationStatus;
			}
		}
		#endregion

		#region EVENT: Rebate_FormClosing
		/// <summary>
		/// Check if form is dirty and if so check if user wants to save the values. If yes, then save to the database.
		/// </summary>
		/// <remarks>
		/// If saving, the form won't close if there are validation errors.
		/// </remarks>
		/// <param name="sender">Form</param>
		/// <param name="e">FormClosingEventArgs</param>
		void Rebate_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (_isDirty)
			{
				DialogResult dialogResult = MessageBox.Show("Details have changed, do you wish to save?", "Close Rebate", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

				switch (dialogResult)
				{
					case DialogResult.Yes:
						e.Cancel = SaveValues();
						break;
					case DialogResult.Cancel:
						e.Cancel = true;
						break;
				}

				if (!e.Cancel && dialogResult == DialogResult.Yes)
				{
					this.DialogResult = DialogResult.OK;
				}
			}
			else
			{
				this.DialogResult = _okCancel.DialogResult;
			}
		}
		#endregion

		#region EVENT: _btnClose_Click
		/// <summary>
		/// Close button clicked so close the form.
		/// </summary>
		/// <remarks>
		/// The form must be clean for the close button to be visible. So closing the form will not trigger a save or validation.
		/// </remarks>
		/// <param name="sender">Close Button</param>
		/// <param name="e">Event arguments</param>
		private void _btnClose_Click(object sender, EventArgs e)
		{
			this.Close();
		}
		#endregion

		#region EVENT: _okCancel_ButtonClick
		/// <summary>
		/// Ok or Cancel butten clicked. OK should validate and save, but the Cancel should just close without saving.
		/// </summary>
		/// <param name="sender">OK/Cancel Buttton</param>
		/// <param name="e">Event arguments</param>
		void _okCancel_ButtonClick(object sender, EventArgs e)
		{
			switch (_okCancel.DialogResult)
			{
				case DialogResult.Cancel:
					// Mark the form as clean so that the close doesn't trigger validation and save
					MarkFormAsClean();
					this.Close();
					break;
				case DialogResult.OK:
					// Validate and save
					Boolean abortFormClose = SaveValues();
					if(!abortFormClose)
					{
						// Inform users of successful save
						MessageBox.Show("Rebate details saved" + (_isNew ? " (Rebate Number: " + _rebate.Number.ToString() + ")" : ""), "Rebate", MessageBoxButtons.OK, MessageBoxIcon.Information);

						// Saved successfully, so just close form. The form should be marked as clean by the save routine so it won't reigger another validation and save.
						this.DialogResult = DialogResult.OK;
						this.Close();
					}
					break;
				default:
					throw new InvalidOperationException("okCancel returned unknown result of" + _okCancel.DialogResult.ToString());
			}
		}
		#endregion

		#endregion

		#region Generic

		#region METHOD: MarkFormAsDirty
		void MarkFormAsDirty(object sender, EventArgs e)
		{
			MarkFormAsDirty();
		}

		void MarkFormAsDirty(object sender, DataGridViewRowEventArgs e)
		{
			MarkFormAsDirty();
		}

		void MarkFormAsDirty(object sender, DataGridViewRowsRemovedEventArgs e)
		{
			MarkFormAsDirty();
		}

		void MarkFormAsDirty()
		{
			_isDirty = true;
			_okCancel.Visible = true;
			_btnClose.Visible = false;
		}

		void MarkFormAsClean()
		{
			_isDirty = false;
			_okCancel.Visible = false;
			_btnClose.Visible = true;
		}
		#endregion

		#region TextBox

		void TextBox_Enter(object sender, EventArgs e)
		{
			_originalValue = ((TextBox) sender).Text;
		}

		void TextBox_Leave(object sender, EventArgs e)
		{
			String newValue = ((TextBox) sender).Text;
			if (newValue != _originalValue)
			{
				MarkFormAsDirty();
			}
		}

		#endregion

		#region DataGridView
		void DataGridViewCellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			DataGridView dataGridView = (DataGridView) sender;
			_originalDataGridViewCellValue = (dataGridView.CurrentCell == null || dataGridView.CurrentCell.Value == null ? "" : dataGridView.CurrentCell.Value.ToString());
		}

		void DataGridViewCellEndEdit(object sender, DataGridViewCellEventArgs e)
		{
			DataGridView dataGridView = (DataGridView) sender;
			if ((dataGridView.CurrentCell == null || dataGridView.CurrentCell.Value == null ? "" : dataGridView.CurrentCell.Value.ToString()) != _originalDataGridViewCellValue)
			{
				MarkFormAsDirty();
			}
		}
		#endregion

		#endregion

		#region TAB: Rebate Header

		#region Description
		void _txtDescription_Validating(object sender, CancelEventArgs e)
		{
			_errorProvider.SetError(_txtDescription, Rebate.ValidateDescription(_txtDescription.Text));
		}
		#endregion

		#region Customer Hierarchy
		void _ucHierarchy_SelectedIndexChanged(object sender, EventArgs e)
		{
			SetupHierarchy();
			DisplayTabPages();
			MarkFormAsDirty();
			ValidateConfigurationData();
		}

		void _ucHierarchy_Validating(object sender, CancelEventArgs e)
		{
			String error = "";

			if (_ucHierarchy.ComboBox.SelectedIndex <= 0)
			{
				error = "Hierarchy must be selected";
			}

			_errorProvider.SetError(_ucHierarchy, error);
		}


		#endregion

		#region CustomerGroup

		void _ucCustomerGroup_SelectedIndexChanged(object sender, EventArgs e)
		{
			MarkFormAsDirty();
			ValidateConfigurationData();

			// Ensure both the sold to and ship to exclusion screens apply the correct
			// filters
			if (_ucCustomerGroup.SelectedValue != null)
			{
				_ucSoldToCustomer.CustomerGroupId = _ucCustomerGroup.SelectedValue.Id;
				_ucSoldToCustomer.ParentCustomerId = null;
				_ucShipToCustomer.CustomerGroupId = _ucCustomerGroup.SelectedValue.Id;
				_ucShipToCustomer.ParentCustomerId = null;
			}
		}

		#endregion

		#region Customer
		void _ucCustomer_CustomerSelected(object sender, EventArgs e)
		{
			MarkFormAsDirty();
			ValidateConfigurationData();

			// Ensure the ship to exclusion screen uses the correct customer filter (we need
			// not worry if what the hierarchy level was, because if its at ship-to level then
			// the control will not be visible anyway
			_ucShipToCustomer.CustomerGroupId = null;
			_ucShipToCustomer.ParentCustomerId = _ucCustomer.SelectedValue.Id;
		}
		#endregion

		#region Sold From
		void _ucSoldFrom_SelectedIndexChanged(object sender, EventArgs e)
		{
			DisplayTabPages();
			MarkFormAsDirty();
		}
		#endregion

		#region Rebate Date

		void _dtpStartEndDate_Validating(object sender, CancelEventArgs e)
		{
			_errorProvider.SetError(_dtpEndDate, Rebate.ValidateDates(_dtpStartDate.Value, _dtpEndDate.Value, Program.Division.LastAccrualPeriod()));
		}
		#endregion

		#region Is Contract
		void _chkIsContract_CheckedChanged(object sender, EventArgs e)
		{
			SetupContract();
			MarkFormAsDirty();
		}
		#endregion

		#region Contract Text
		void _txtContractText_Validating(object sender, CancelEventArgs e)
		{
			_errorProvider.SetError(_txtContractText, Rebate.ValidateContractText(_chkIsContract.Checked, _txtContractText.Text));
		}
		#endregion
		
		#region Is VAP
		void _isVAP_CheckedChanged(object sender, EventArgs e)
		{
			SetupVAP();
			DisplayTabPages();
			MarkFormAsDirty();
		}
		#endregion

		#region Global Product Rebate
		void _chkIsGlobalProduct_CheckedChanged(object sender, EventArgs e)
		{
			SetupGlobalProduct();
			DisplayTabPages();
			MarkFormAsDirty();
		}
		#endregion

		#region Global Rate
		void _txtGlobalRate_Validating(object sender, CancelEventArgs e)
		{
			_errorProvider.SetError(_txtGlobalRate, Rebate.ValidateGlobalRate(_chkIsGlobalProduct.Checked, _txtGlobalRate.Text));
			
			Decimal rate;
			
			if (Decimal.TryParse(_txtGlobalRate.Text, out rate))
			{
				_txtGlobalRate.Text = rate.ToString(_numberFormat);
			}
		}
		#endregion

		#region Rebate Type
		void _ucRebateType_SelectedIndexChanged(object sender, EventArgs e)
		{
			SetupRebateType();
			DisplayTabPages();
			MarkFormAsDirty();
			DataBindRebateExtension();
		}
		#endregion

		#region Payment Frequency
		void _ucPaymentFrequency_Validating(object sender, CancelEventArgs e)
		{
			String error = "";

			if (_ucPaymentFrequency.ComboBox.SelectedIndex <= 0 || _ucPaymentFrequency.SelectedValue.Id == 0)
			{
				error = "Payment frequency must be selected";
			}

			_errorProvider.SetError(_ucPaymentFrequency, error);
		}
		#endregion

		#region Estimated Sales Volume
		void _txtEstimatedVolume_Validating(object sender, CancelEventArgs e)
		{
			_errorProvider.SetError(_txtEstimatedVolume, Rebate.ValidateEstimatedVolume(_ucRebateType.SelectedValue.IsEstimated, _txtEstimatedVolume.Text));
			Int32 volume;

			if (Int32.TryParse(_txtEstimatedVolume.Text.Replace(",", ""), out volume))
			{
				_txtEstimatedVolume.Text = volume.ToString(_integerFormat);
			}

			GetFixedRate();
		}
		#endregion

		#region Rebate Value
		void _txtFixedValue_Validating(object sender, CancelEventArgs e)
		{
			_errorProvider.SetError(_txtFixedValue, Rebate.ValidateFixedRebateValue(_ucRebateType.SelectedValue.IsFixed, _txtFixedValue.Text));
			Int32 value;

			if (Int32.TryParse(_txtFixedValue.Text.Replace(",", ""), out value))
			{
				_txtFixedValue.Text = value.ToString(_integerFormat);
			}

			GetFixedRate();
		}
		#endregion

		#region DataGridView: _dgvRange
		void _dgvRange_Validating(object sender, CancelEventArgs e)
		{
			DataGridView dataGridView = (DataGridView) sender;

			if (_ucRebateType.SelectedValue.IsRangeBased)
			{
				DataGridViewUtilities.Validate(dataGridView, _errorProvider, _lblErrorRebateRange, ValidateRebateRangeCell, null, ValidateRebateRange);
			}
			else
			{
				_errorProvider.SetError(_lblErrorRebateRange, "");
			}
		}

		private String ValidateRebateRangeCell(DataGridViewCell cell)
		{
			String errorText = "";

			switch (cell.OwningColumn.DataPropertyName)
			{
				case RebateRangeProperty.AboveQuantity:
					errorText = RebateRange.ValidateAboveQty(cell.EditedFormattedValue.ToString(), (RebateRange) cell.OwningRow.DataBoundItem, _rebate.RebateRanges);
					break;
				case RebateRangeProperty.Rate:
					errorText = RebateRange.ValidateRate(cell.EditedFormattedValue.ToString());
					break;
			}
			return errorText;
		}

		private String ValidateRebateRange(DataGridView dataGridView)
		{
			String errorText = "";

			if (dataGridView.NewRowIndex == 0)
			{
				errorText = "There must be at least one range defined";
			}

			return errorText;
		}

		void _dgvRange_DataError(object sender, DataGridViewDataErrorEventArgs e)
		{
			if ((e.Context & DataGridViewDataErrorContexts.Parsing) != 0)
			{
				_errorProvider.SetError(_lblErrorRebateRange, "Invalid value");
			}
			else if ((e.Context & DataGridViewDataErrorContexts.Commit) != 0 && e.Exception is ArgumentException)
			{
				_errorProvider.SetError(_lblErrorRebateRange, "Cannot be left blank");
			}
			else
			{
				e.ThrowException = true;
			}
		}
		void _dgvRange_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			DataGridViewUtilities.SetColumnFormat(_dgvRange.Columns[RebateRangeProperty.AboveQuantity], _integerFormat, DataGridViewContentAlignment.MiddleRight);
			DataGridViewUtilities.SetColumnFormat(_dgvRange.Columns[RebateRangeProperty.Rate], _numberFormat, DataGridViewContentAlignment.MiddleRight);
		}
		#endregion

		#endregion

		#region TAB: Rebate Detail

		#region Attribute
		void _ucAttribute_SelectedIndexChanged(object sender, EventArgs e)
		{
			ComboBox attribute = (ComboBox) sender;

			if (attribute.SelectedIndex > 0)
			{
				_ucProductDetail.Clear();
			}
		}
		#endregion

		#region Product
		void _ucProductDetail_ProductSelected(object sender, EventArgs e)
		{
			_ucAttribute1.ComboBox.SelectedIndex = 0;
			_ucAttribute2.ComboBox.SelectedIndex = 0;
			_ucAttribute3.ComboBox.SelectedIndex = 0;
			_ucAttribute4.ComboBox.SelectedIndex = 0;

			AddProductDetail();
		}

		void _ucProductDetail_MultiProductSelected(object sender, EventArgs e)
		{
			AddProductDetail();
		}
		#endregion

		#region Button: Add
		void _btnAddProductDetail_Click(object sender, EventArgs e)
		{
			AddProductDetail();
		}
		#endregion

		#region Button: Clear
		void _btnProductDetailClear_Click(object sender, EventArgs e)
		{
			_ucAttribute1.ComboBox.SelectedIndex = 0;
			_ucAttribute2.ComboBox.SelectedIndex = 0;
			_ucAttribute3.ComboBox.SelectedIndex = 0;
			_ucAttribute4.ComboBox.SelectedIndex = 0;
			_ucProductDetail.Clear();
		}
		#endregion

		#region DataGridView
		void _dgvDetail_Validating(object sender, CancelEventArgs e)
		{
			DataGridView dataGridView = (DataGridView) sender;
			DataGridViewUtilities.Validate(dataGridView, _errorProvider, _lblErrorProductDetail, ValidateRebateDetailCell, null, ValidateRebateDetail);
		}

		private String ValidateRebateDetailCell(DataGridViewCell cell)
		{
			String errorText = "";

			switch (cell.OwningColumn.DataPropertyName)
			{
				case RebateDetailProperty.Rate:
					RebateType rebateType = _ucRebateType.SelectedValue;
					if (rebateType.IsProductRate)
					{
						errorText = RebateDetail.ValidateRate(cell.EditedFormattedValue.ToString(), rebateType.IsValueBasedCalculation);
					}
					break;
			}
			return errorText;
		}

		private String ValidateRebateDetail(DataGridView dataGridView)
		{
			return dataGridView.RowCount == 0 ? "Must have at least one product" : "";
		}

		void _dgvDetail_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
		{
			// User wants rate blanked out on delete
			RebateDetail rebateDetail = (RebateDetail) e.Row.DataBoundItem;
			rebateDetail.Rate = null;
		}

		void _dgvDetail_DataError(object sender, DataGridViewDataErrorEventArgs e)
		{
			DataGridView dataGridView = (DataGridView) sender;

			if ((e.Context & DataGridViewDataErrorContexts.Parsing) != 0)
			{
				_errorProvider.SetError(_lblErrorProductDetail, dataGridView.Columns[e.ColumnIndex].Name + " has an invalid value");
			}
			else if ((e.Context & DataGridViewDataErrorContexts.Commit) != 0 && e.Exception is ArgumentException)
			{
				_errorProvider.SetError(_lblErrorProductDetail, dataGridView.Columns[e.ColumnIndex].Name + " cannot be left blank");
			}
			else
			{
				e.ThrowException = true;
			}
		}
		void _dgvDetail_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			DataGridViewUtilities.SetColumnFormat(_dgvDetail.Columns[RebateDetailProperty.Rate], _numberFormat, DataGridViewContentAlignment.MiddleRight);
		}

		#endregion
		#endregion

		#region TAB: Revate VAP

		#region Activation Method
		void _ucVAPActivationMethod_SelectedIndexChanged(object sender, EventArgs e)
		{
			SetupVAP();
			MarkFormAsDirty();
		}
		#endregion

		#region Activation Quantity
		void _txtVAPActivationQuantity_Validating(object sender, CancelEventArgs e)
		{
			if (_ucVAPActivationMethod.SelectedValue == VAPActivationMethod.Global)
			{
				_errorProvider.SetError(_txtVAPActivationQuantity, RebateVAP.ValidateActivationVolume(_txtVAPActivationQuantity.Text));
				Int32 quantity;

				if (Int32.TryParse(_txtVAPActivationQuantity.Text.Replace(",", ""), out quantity))
				{
					_txtVAPActivationQuantity.Text = quantity.ToString(_integerFormat);
				}
			}
		}
		#endregion

		#region Product
		void _ucProductVAP_ProductSelected(object sender, EventArgs e)
		{
			AddVAPProduct();
		}

		void _ucProductVAP_MultiProductSelected(object sender, EventArgs e)
		{
			AddVAPProduct();
		}
		#endregion

		#region Button: Add
		void _btnVAPProductAdd_Click(object sender, EventArgs e)
		{
			AddVAPProduct();
		}
		#endregion

		#region DataGridView
		void _dgvVAP_Validating(object sender, CancelEventArgs e)
		{
			DataGridView dataGridView = (DataGridView) sender;
			DataGridViewUtilities.Validate(dataGridView, _errorProvider, _lblErrorVAP, ValidateRebateVAPCell, null, ValidateRebateVAP);
		}

		private String ValidateRebateVAPCell(DataGridViewCell cell)
		{
			String errorText = "";

			switch (cell.OwningColumn.DataPropertyName)
			{
				case RebateVAPProperty.ActivationVolume:
					if(_ucVAPActivationMethod.SelectedValue == VAPActivationMethod.Individual)
					{
						errorText = RebateVAP.ValidateActivationVolume(cell.EditedFormattedValue.ToString());
					}
					break;
			}
			return errorText;
		}

		private String ValidateRebateVAP(DataGridView dataGridView)
		{
			return dataGridView.RowCount == 0 ? "Must have at least one product" : "";
		}

		void _dgvVAP_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			DataGridViewUtilities.SetColumnFormat(_dgvVAP.Columns[RebateVAPProperty.ActivationVolume], _integerFormat, DataGridViewContentAlignment.MiddleRight);
		}

		#endregion
		#endregion

		#region TAB: Rebate Delivery Window

		#region Packing Type: SelectedIndexChanged

		void _ucPackingType_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (_ucRebateType.SelectedValue.IsDeliveryWindow)
			{
				_ucDeliveryWindow.ExcludeIds.Clear();

				foreach (RebateDeliveryWindow rebateDeliveryWindow in _rebate.RebateDeliveryWindows.Where(rdw => rdw.PackingTypeEnum == _ucPackingType.SelectedValue))
				{
					_ucDeliveryWindow.ExcludeIds.Add(rebateDeliveryWindow.DeliveryWindowId);
				}

				_ucDeliveryWindow.Populate();
			}
			else
			{
				_ucLeadTime.ExcludeIds.Clear();

				foreach (RebateLeadTime rebateLeadTime in _rebate.RebateLeadTimes.Where(rdw => rdw.PackingTypeEnum == _ucPackingType.SelectedValue))
				{
					_ucLeadTime.ExcludeIds.Add(rebateLeadTime.LeadTimeId);
				}

				_ucLeadTime.Populate();
			}
		}

		#endregion

		#region Button: Add
		void _btnAddDeliveryWindow_Click(object sender, EventArgs e)
		{
			AddDeliveryWindow();
		}
		#endregion

		#region DataGridView

		void _dgvRebateExtension_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			if (_ucRebateType.SelectedValue.IsDeliveryWindow)
			{
				DataGridViewUtilities.SetColumnFormat(_dgvRebateExtension.Columns[RebateDeliveryWindowProperty.Rate], _numberFormat, DataGridViewContentAlignment.MiddleRight);
			}
			else
			{
				DataGridViewUtilities.SetColumnFormat(_dgvRebateExtension.Columns[RebateLeadTimeProperty.Rate], _numberFormat, DataGridViewContentAlignment.MiddleRight);
			}
		}

		void _dgvRebateExtension_Validating(object sender, CancelEventArgs e)
		{
			DataGridView dataGridView = (DataGridView) sender;
			DataGridViewUtilities.Validate
				(
					dataGridView,
					_errorProvider,
					_lblErrorDeliveryWindow,
					ValidateRebateExtensionCell,
					ValidateRebateExtensionRow,
					ValidateRebateExtension
				);
		}

		private String ValidateRebateExtensionCell(DataGridViewCell cell)
		{
			String errorText = "";

			if (cell.OwningColumn.DataPropertyName == RebateDeliveryWindowProperty.Rate ||
				cell.OwningColumn.DataPropertyName == RebateLeadTimeProperty.Rate)
			{
				if (_ucRebateType.SelectedValue.IsDeliveryWindow)
				{
					errorText = RebateDeliveryWindow.ValidateRate(cell.EditedFormattedValue.ToString());
				}
				else
				{
					errorText = RebateLeadTime.ValidateRate(cell.EditedFormattedValue.ToString());
				}
			}

			return errorText;
		}

		private String ValidateRebateExtensionRow(DataGridViewRow dataGridViewRow)
		{
			return "";
		}

		private String ValidateRebateExtension(DataGridView dataGridView)
		{
			String error = "Must have at least one " + (_ucRebateType.SelectedValue.IsDeliveryWindow ? "delivery window" : "lead time");
			return dataGridView.RowCount == 0 ? error : "";
		}

		void _dgvRebateExtension_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
		{
			if (_ucRebateType.SelectedValue.IsDeliveryWindow)
			{
				RebateDeliveryWindow rebateDeliveryWindow = (RebateDeliveryWindow) e.Row.DataBoundItem;

				_ucDeliveryWindow.ExcludeIds.Remove(rebateDeliveryWindow.DeliveryWindowId);
				_ucDeliveryWindow.Populate();
			}
			else
			{
				RebateLeadTime rebateDeliveryWindow = (RebateLeadTime) e.Row.DataBoundItem;

				_ucLeadTime.ExcludeIds.Remove(rebateDeliveryWindow.LeadTimeId);
				_ucLeadTime.Populate();
			}
		}

		void _dgvRebateExtension_DataError(object sender, DataGridViewDataErrorEventArgs e)
		{
			_dgvRebateExtension.CurrentCell.Value = "";

			e.Cancel = true;
			e.ThrowException = false;
		}

		#endregion

		#endregion

		#region TAB: Rebate Exception (Sold From)
		void _btnSoldFromInclude_Click(object sender, EventArgs e)
		{
			MoveBetweenListBox(_lstSoldFromExclude, _lstSoldFromInclude);
		}

		void _btnSoldFromExclude_Click(object sender, EventArgs e)
		{
			MoveBetweenListBox(_lstSoldFromInclude, _lstSoldFromExclude);
		}

		void _lstSoldFrom_MouseDown(object sender, MouseEventArgs e)
		{
			if (_editMode != RebateEditMode.ReadOnly)
			{
				ListBox listBox = (ListBox) sender;
				ExtendedBindingList<Site> dataList = (ExtendedBindingList<Site>) listBox.DataSource;

				if (listBox.Items.Count > 0)
				{
					Int32 index = listBox.IndexFromPoint(e.X, e.Y);

					if (index >= 0)
					{
						List<Site> data = new List<Site>();

						foreach (Int32 selectedIndex in listBox.SelectedIndices)
						{
							data.Add((Business.Rebates.Site) dataList[selectedIndex]);
						}

						DragDropEffects dragDropEffects = DoDragDrop(new DragDropData(dataList, data), DragDropEffects.All);

						if (dragDropEffects == DragDropEffects.All)
						{
							foreach (Business.Rebates.Site site in data)
							{
								dataList.Remove(site);
							}

							DataBindSoldFrom(listBox, dataList);
						}
					}
				}
			}
		}

		void _lstSoldFrom_DragOver(object sender, DragEventArgs e)
		{
			if (_editMode != RebateEditMode.ReadOnly)
			{
				e.Effect = DragDropEffects.All;
			}
		}

		void _lstSoldFrom_DragDrop(object sender, DragEventArgs e)
		{
			if (_editMode != RebateEditMode.ReadOnly)
			{
				ListBox listBox = (ListBox) sender;
				ExtendedBindingList<Site> dataList = (ExtendedBindingList<Site>) listBox.DataSource;
				Type format = typeof(DragDropData);

				if (e.Data.GetDataPresent(format))
				{
					DragDropData dragDropData = (DragDropData) e.Data.GetData(format);
					foreach (Business.Rebates.Site site in dragDropData.Items)
					{
						dataList.Add(site);
					}

					DataBindSoldFrom(listBox, dataList);

					if (!(dragDropData.Source.Equals(dataList)))
					{
						// Mark form as data if we have dropped the items onto a different list
						// from the one we originally dragged them from
						MarkFormAsDirty();
					}
				}
			}
		}
		#endregion

		#region TAB: Rebate Exception (Sold To)
		void _btnSoldToAdd_Click(object sender, EventArgs e)
		{
			AddCustomerException(_ucSoldToCustomer, _dgvSoldTo, ExceptionTypeEnum.SoldTo);
		}

		void _ucSoldToCustomer_CustomerSelected(object sender, EventArgs e)
		{
			UserControls.Rebates.Customer customer = (UserControls.Rebates.Customer) sender;
			AddCustomerException(customer, _dgvSoldTo, ExceptionTypeEnum.SoldTo);
		}

		void _ucSoldToCustomer_MultiCustomerSelected(object sender, EventArgs e)
		{
			UserControls.Rebates.Customer customer = (UserControls.Rebates.Customer) sender;
			AddCustomerException(customer, _dgvSoldTo, ExceptionTypeEnum.SoldTo);
		}
		#endregion

		#region TAB: Rebate Exception (Ship To)
		void _btnShipToAdd_Click(object sender, EventArgs e)
		{
			AddCustomerException(_ucShipToCustomer, _dgvShipTo, ExceptionTypeEnum.ShipTo);
		}

		void _ucShipToCustomer_CustomerSelected(object sender, EventArgs e)
		{
			UserControls.Rebates.Customer customer = (UserControls.Rebates.Customer) sender;
			AddCustomerException(customer, _dgvShipTo, ExceptionTypeEnum.ShipTo);
		}

		void _ucShipToCustomer_MultiCustomerSelected(object sender, EventArgs e)
		{
			UserControls.Rebates.Customer customer = (UserControls.Rebates.Customer) sender;
			AddCustomerException(customer, _dgvShipTo, ExceptionTypeEnum.ShipTo);
		}
		#endregion

		#region Rebate Notes Events

		void _dgvNotes_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			_dgvNotes.Columns[0].DefaultCellStyle.Format = "dd/MM/yyyy HH:mm:ss";
			_dgvNotes.Columns[2].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

			RebindDataGridView(_dgvNotes, _rebate.RebateNotes, RebateNoteProperty.CreateTime + " desc");
		}

		void RebateNotes_ListChanged(object sender, ListChangedEventArgs e)
		{
			SetupRecentNotes();
		}

		private void SetupRecentNotes()
		{
			if (_rebate.RebateNotes.Count == 0)
			{
				_lblNotesLabel.Visible = false;
				_lblNotes.Text = "";
			}
			else
			{
				_lblNotesLabel.Visible = true;
				_lblNotes.Text = _rebate.RebateNotes[0].Note;
			}
		}


		#endregion

		#region TAB: Rebate Note
		void _btnNoteAdd_Click(object sender, EventArgs e)
		{
			AddRebateNote();
		}

		#endregion

		#endregion

		#region Save

		#region METHOD: SaveValues
		/// <summary>
		/// Validate the form and if valid save the values to the database.
		/// </summary>
		/// <returns>abortFormClose<value>TRUE if required to abort the closing of the form</value>
		/// </returns>
		bool SaveValues()
		{
			// Ensure some notes are entered against an update
			if (!_isNew)
			{
				String error = (_rebate.RebateNotes.Where(rn => !rn.ExistsOnDatabase).Count() == 0 ? "Please enter some notes about your changes" : "");
				_errorProvider.SetError(_lblNoteError, error);
			}

			Boolean isSaved = ApplicationDomain.FormSaveValues(_errorProvider, SaveFormValuesToDatabaseObjects, SaveDatabaseObjectsToDatabase, MarkFormAsClean, null);

			if (isSaved)
			{
				this.Text = _rebate.WindowTitle;
			}

			return isSaved;
		}
		#endregion

		#region METHOD: SaveDatabaseObjectsToDatabase

		private void SaveDatabaseObjectsToDatabase()
		{
			Int32 lineNumber = 0;
			Boolean isNew = (_rebate.Id == 0);

			#region Rebate
			if (_rebate.AcceptChanges() < 0)
			{
				throw new DatabaseException("Rebate Header");
			}
			#endregion

			#region RebateRange
			if (!isNew)
			{
				if (RebateRange.DeleteRebateRanges(_rebate.Id) < 0)
				{
					throw new DatabaseException("Rebate Range");
				}
			}

			if (_ucRebateType.SelectedValue.IsRangeBased)
			{
				lineNumber = 0;

				foreach (RebateRange rebateRange in _rebate.RebateRanges)
				{
					rebateRange.ExistsOnDatabase = false;
					rebateRange.SetColumnDirty();

					lineNumber++;

					if (rebateRange.RebateId == 0 || isNew)
					{
						rebateRange.RebateId = _rebate.Id;
					}

					rebateRange.ExistsOnDatabase = false;

					if (rebateRange.AcceptChanges() < 0)
					{
						throw new DatabaseException("Rebate Range: Line " + lineNumber.ToString());
					}
				}
			}
			else
			{
				if (RebateRange.DeleteRebateRanges(_rebate.Id) < 0)
				{
					throw new DatabaseException("Rebate Ranges" + lineNumber.ToString());
				}

				_rebate.RebateRanges.Clear();
			}

			#endregion

			#region RebateDetail

			lineNumber = 0;

			// May have to logically delete RebateDetail lines which have already been accrued against
			_rebate.RebateDetails.CheckDeletions();

			// Ok Now process rebateDetails
			foreach (RebateDetail rebateDetail in _rebate.RebateDetails)
			{
				lineNumber++;

				if (rebateDetail.RebateId == 0 || isNew || !rebateDetail.ExistsOnDatabase)
				{
					rebateDetail.RebateId = _rebate.Id;
				}

				if (rebateDetail.AcceptChanges() < 0)
				{
					throw new DatabaseException("Rebate Detail: Line " + lineNumber.ToString());
				}
			}

			#endregion

			#region RebateVAP
			if (_rebate.IsVAP)
			{
				lineNumber = 0;
				foreach (RebateVAP rebateVAP in _rebate.RebateVAPs)
				{
					lineNumber++;
					if (rebateVAP.RebateId == 0 || isNew || !rebateVAP.ExistsOnDatabase)
					{
						rebateVAP.RebateId = _rebate.Id;
					}
					if (rebateVAP.AcceptChanges() < 0)
					{
						throw new DatabaseException("Rebate VAP: Line " + lineNumber.ToString());
					}
				}
			}
			else
			{
				if (!isNew)
				{
					if (RebateVAP.DeleteRebateVAPs(_rebate.Id) < 0)
					{
						throw new DatabaseException("Rebate VAP");
					}

					_rebate.RebateVAPs.Clear();
				}
			}
			#endregion

			#region RebateDeliveryWindow
			if (_ucRebateType.SelectedValue.IsDeliveryWindow)
			{
				lineNumber = 0;
				foreach (RebateDeliveryWindow rebateDeliveryWindow in _rebate.RebateDeliveryWindows)
				{
					lineNumber++;
					if (rebateDeliveryWindow.RebateId == 0 || isNew || !rebateDeliveryWindow.ExistsOnDatabase)
					{
						rebateDeliveryWindow.RebateId = _rebate.Id;
					}

					if (rebateDeliveryWindow.AcceptChanges() < 0)
					{
						throw new DatabaseException("Rebate Delivery Window: Line " + lineNumber.ToString());
					}
				}
			}
			else
			{
				if (!isNew)
				{
					if (RebateDeliveryWindow.DeleteRebateDeliveryWindows(_rebate.Id) < 0)
					{
						throw new DatabaseException("Rebate Delivery Window");
					}

					_rebate.RebateDeliveryWindows.Clear();
				}
			}
			#endregion

			#region RebateLeadTime
			if (_ucRebateType.SelectedValue.IsLeadTime)
			{
				lineNumber = 0;
				foreach (RebateLeadTime rebateLeadTime in _rebate.RebateLeadTimes)
				{
					lineNumber++;
					if (rebateLeadTime.RebateId == 0 || isNew || !rebateLeadTime.ExistsOnDatabase)
					{
						rebateLeadTime.RebateId = _rebate.Id;
					}

					if (rebateLeadTime.AcceptChanges() < 0)
					{
						throw new DatabaseException("Rebate Lead Time: Line " + lineNumber.ToString());
					}
				}
			}
			else
			{
				if (!isNew)
				{
					if (RebateLeadTime.DeleteRebateLeadTimes(_rebate.Id) < 0)
					{
						throw new DatabaseException("Rebate Lead Time");
					}

					_rebate.RebateLeadTimes.Clear();
				}
			}
			#endregion

			#region RebateExceptions

			if (!isNew)
			{
				if (RebateException.DeleteRebateExceptions(_rebate.Id, ExceptionTypeEnum.SoldFrom) < 0)
				{
					throw new DatabaseException("Rebate Sold From Exception");
				}

				_rebate.RebateExceptions.Clear();
			}

			#region RebateException (SoldFrom)

			lineNumber = 0;

			foreach (Site site in _excludeSoldFroms)
			{
				lineNumber++;

				RebateException rebateException = new RebateException();
				rebateException.RebateId = _rebate.Id;
				rebateException.ExceptionTypeEnum = ExceptionTypeEnum.SoldFrom;
				rebateException.ExceptionId = site.Id;

				if (rebateException.AcceptChanges() < 0)
				{
					throw new DatabaseException("Rebate Sold From Exception : Line " + lineNumber.ToString());
				}

				_rebate.RebateExceptions.Add(rebateException);
			}

			#endregion

			#region RebateException (SoldTo)

			if (_tabControl.TabPages.Contains(_tabPageSoldTo))
			{
				foreach (RebateExceptionCustomer rebateException in _excludeSoldTos)
				{
					rebateException.RebateId = _rebate.Id;

					if (rebateException.AcceptChanges() < 0)
					{
						throw new DatabaseException("Rebate Sold To Exception: Customer Id " + rebateException.ExceptionId);
					}

					if (!rebateException.IsMarkedForDeletion)
					{
						_rebate.RebateExceptions.Add((RebateException) rebateException);
					}
				}
			}
			else
			{
				if (!isNew)
				{
					if (RebateException.DeleteRebateExceptions(_rebate.Id, ExceptionTypeEnum.SoldTo) < 0)
					{
						throw new DatabaseException("Rebate Sold To Exception");
					}
				}
			}
			#endregion

			#region RebateException (ShipTo)

			if (_tabControl.TabPages.Contains(_tabPageShipTo))
			{
				foreach (RebateExceptionCustomer rebateException in _excludeShipTos)
				{
					rebateException.RebateId = _rebate.Id;

					if (rebateException.AcceptChanges() < 0)
					{
						throw new DatabaseException("Rebate Ship To Exception: Customer Id " + rebateException.ExceptionId);
					}

					if (!rebateException.IsMarkedForDeletion)
					{
						_rebate.RebateExceptions.Add((RebateException) rebateException);
					}
				}
			}
			else
			{
				if (!isNew)
				{
					if (RebateException.DeleteRebateExceptions(_rebate.Id, ExceptionTypeEnum.ShipTo) < 0)
					{
						throw new DatabaseException("Rebate Ship To Exception");
					}
				}
			}

			#endregion

			#endregion

			#region RebateNote

			lineNumber = 0;

			if (isNew)
			{
				// Create initial creation rebate note
				RebateNote rebateNote = new RebateNote();

				rebateNote.Note = "Initial Creation";
				rebateNote.UserId = Program.User.Id;
				rebateNote.CreateTime = _rebate.CreateTime;

				_rebate.RebateNotes.Insert(0, rebateNote);
			}

			foreach (RebateNote rebateNote in _rebate.RebateNotes)
			{
				lineNumber++;

				if (rebateNote.RebateId == 0 || isNew || !rebateNote.ExistsOnDatabase)
				{
					rebateNote.RebateId = _rebate.Id;
				}

				if (rebateNote.AcceptChanges() < 0)
				{
					throw new DatabaseException("Rebate Note: Line " + lineNumber.ToString());
				}
			}

			#endregion
		}

		#endregion

		#region Method: SaveFormValuesToDatabaseObjects
		/// <summary>
		/// Save the values from the forms into database objects, unless they are already held in database objects on the form 
		/// </summary>
		private void SaveFormValuesToDatabaseObjects()
		{
			// Now populate objects from form
			SaveFormValuesForRebate();
			SaveFormValuesForRebateVAP();
		}
		#endregion

		#region METHOD: SaveFormValuesForRebate
		/// <summary>
		/// Populates _rebate with the Rebate Header details from the form.
		/// </summary>
		/// <remarks>
		/// For new rebates, _rebate will be null prior to calling this method.
		/// For existing rebates, _rebate will hold the original values.
		/// Upon exit, Id and Number are set to zero for new rebates.
		/// </remarks>
		private void SaveFormValuesForRebate()
		{
			bool isNew = (_rebate.Id == 0);

			if (isNew)
			{
				_rebate.DivisionId = Program.Division.Id;
				_rebate.Number = 0;
				_rebate.CreateUserId = Program.User.Id;
				_rebate.CreateTime = DateTime.Now;
			}

			_rebate.UpdateUserId = isNew ? (int?) null  : Program.User.Id;
			_rebate.UpdateTime = isNew ? (DateTime?) null : DateTime.Now;
			
			_rebate.Description = _txtDescription.Text;
			_rebate.HierarchyId = _ucHierarchy.SelectedValue.Id;

			Object relatedEntity = _hiearchyRelatedValueProperty.GetValue(_ucHierarchyRelatedControl, null);
			Type typeRelatedEntity = relatedEntity.GetType();
			_rebate.HierarchyEntityId = (Int32) typeRelatedEntity.GetProperty("Id").GetValue(relatedEntity, null);

			_rebate.PricingSubGroupId = (_isPricingGroupAllowed && _ucPricingSubGroup.SelectedValue != null && _ucPricingSubGroup.SelectedValue.Id != 0 ? (Int32?) _ucPricingSubGroup.SelectedValue.Id : null);

			_rebate.PayeeId = _ucPayee.SelectedValue == null ? (int?) null : _ucPayee.SelectedValue.Id;
			_rebate.DeliveryTypeEnum = _ucDeliveryType.SelectedValue;
			_rebate.SiteId = _ucSoldFrom.SelectedValue.Id == 0 ? (int?) null : _ucSoldFrom.SelectedValue.Id;
			_rebate.StartDate = (DateTime) _dtpStartDate.Value;
			_rebate.EndDate = _dtpEndDate.Value;
			_rebate.IsContract = _chkIsContract.Checked;
			_rebate.ContractText = _rebate.IsContract ? _txtContractText.Text : "";

			if (_ucRebateType.SelectedValue.IsRebateExtension)
			{
				_rebate.IsVAP = false;
			}
			else
			{
				_rebate.IsVAP = _chkIsVAP.Checked;
			}

			_rebate.VAPActivationVolume = (_rebate.IsVAP ? (Int32?) Int32.Parse(_txtVAPActivationQuantity.Text.Replace(",", "")) : null);
			_rebate.PaymentFrequencyId = _ucPaymentFrequency.SelectedValue.Id;
			_rebate.IsGlobalProduct = _chkIsGlobalProduct.Checked;
			_rebate.GlobalRate = _rebate.IsGlobalProduct ? (decimal?) decimal.Parse(_txtGlobalRate.Text) : (decimal?) null;
			_rebate.IsJif = _chkIsJIF.Checked;
			_rebate.IsAdjustment = _chkIsAdjustment.Checked;
			_rebate.RebateTypeId = _ucRebateType.SelectedValue.Id;
			_rebate.EstimatedVolume = _ucRebateType.SelectedValue.IsEstimated ? (int?) int.Parse(_txtEstimatedVolume.Text.Replace(",", "")) : null;
			_rebate.FixedRebateValue = _ucRebateType.SelectedValue.IsFixed? (int?) int.Parse(_txtFixedValue.Text.Replace(",", "")) : (int?) null;
		}
		#endregion

		#region METHOD: SaveFormValuesForRebateVAP
		/// <summary>
		/// Saves the form values into database objects for Rebate VAPs
		/// </summary>
		/// <remarks>
		/// The values are already in the database object _rebateVAPProducts.
		/// This routine will null the ActivationVolume if we are using a Global activation method.
		/// </remarks>
		private void SaveFormValuesForRebateVAP()
		{
			if (_ucVAPActivationMethod.SelectedValue == VAPActivationMethod.Global)
			{
				foreach (RebateVAP rebateVAP in _rebate.RebateVAPs)
				{
					rebateVAP.ActivationVolume = null;
				}
			}
		}
		#endregion

		#endregion

		#region Toolbar/menu methods

		public void Save()
		{
			if (_isDirty)
			{
				SaveValues();
			}
		}

		public void Copy()
		{
			this.Cursor = this.MdiParent.Cursor = Cursors.WaitCursor;

			// Must save the current rebate if it is dirty BEFORE we try to copy it.
			if (_isDirty)
			{
				SaveValues();
			}
			// If it is now clean then the save worked so we can now proceed with the copy.
			if (!_isDirty)
			{
				CopyRebate();
			}

			this.Cursor = this.MdiParent.Cursor = Cursors.Default;
		}

		#endregion

		#region Business Logic

		private enum RebateEditMode
		{
			Full,
			Partial,
			ReadOnly
		}

		private void SetEditMode()
		{
			_editMode = RebateEditMode.Full;

			// First determine edit mode
			if (!_isNew)
			{
				// Read completed accruals for the year
				DivisionAccrual divisionAccrual = new DivisionAccrual();

				divisionAccrual.DivisionId = Program.Division.Id;
				divisionAccrual.Year = _rebate.Version;
				divisionAccrual.IsDraft = false;
				divisionAccrual.AccrualTypeEnum = AccrualTypeEnum.Actual;
				divisionAccrual.OrderBy = "{Period} desc";

				ExtendedBindingList<DivisionAccrual> closingAccrual = (ExtendedBindingList<DivisionAccrual>) divisionAccrual.ReadWithDirtyProperty();

				if (closingAccrual != null && closingAccrual.Count > 0)
				{
					DivisionAccrual lastAccrual = closingAccrual[0];

					if (_rebate.EndDate.HasValue)
					{
						if (_rebate.EndDate.Value.Year == lastAccrual.Year)
						{
							// Current years rebate
							if (lastAccrual.Period == 12)
							{
								// Current year has been completed
								_editMode = RebateEditMode.ReadOnly;
							}
							else
							{
								// Open year
								_editMode = RebateEditMode.Partial;
							}
						}
						else if (_rebate.EndDate.Value.Year < lastAccrual.Year)
						{
							// Previous years rebate
							_editMode = RebateEditMode.ReadOnly;
						}
					}
					else
					{
						if (_rebate.StartDate.Year == lastAccrual.Year)
						{
							// Check for accruals for this rebate
							Accrual accrual = new Accrual();

							accrual.RebateId = _rebate.Id;
							accrual.CreateYear = (Int16) _rebate.StartDate.Year;

							accrual.AddReadFilter(AccrualProperty.RebateId, "=");
							accrual.AddReadFilter(AccrualProperty.CreateYear, "=");

							if (accrual.Read().Count > 0)
							{
								// Accruals exist
								_editMode = RebateEditMode.Partial;
							}
						}
					}
				}
			}

			// Now setup control states based on edit mode
			//
			// Header
			_txtDescription.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_ucHierarchy.Enabled = (_editMode == RebateEditMode.Full);
			_ucCustomerGroup.Enabled = (_editMode == RebateEditMode.Full);
			_ucCustomer.Enabled = (_editMode == RebateEditMode.Full);
			_ucPricingSubGroup.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_ucPayee.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_ucDeliveryType.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_ucSoldFrom.Enabled = (_editMode == RebateEditMode.Full);
			_dtpStartDate.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_dtpEndDate.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_chkIsContract.Enabled = (_editMode == RebateEditMode.Full);
			_chkIsVAP.Enabled = (_editMode == RebateEditMode.Full);
			_chkIsGlobalProduct.Enabled = (_editMode == RebateEditMode.Full);
			_chkIsJIF.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_chkIsAdjustment.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_txtGlobalRate.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_ucRebateType.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_ucPaymentFrequency.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_txtEstimatedVolume.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_txtFixedValue.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_dgvRange.ReadOnly = (_editMode == RebateEditMode.ReadOnly);
			_dgvRange.AllowUserToAddRows = (_editMode != RebateEditMode.ReadOnly);
			_dgvRange.AllowUserToDeleteRows = (_editMode != RebateEditMode.ReadOnly);

			// Product Detail
			_ucAttribute1.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_ucAttribute2.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_ucAttribute3.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_ucAttribute4.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_ucProductDetail.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_btnProductDetailClear.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_btnAddProductDetail.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_dgvDetail.ReadOnly = (_editMode == RebateEditMode.ReadOnly);

			// VAP
			_ucVAPActivationMethod.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_txtVAPActivationQuantity.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_ucProductVAP.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_dgvVAP.ReadOnly = true;

			// Rebate Extension
			_ucPackingType.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_ucDeliveryWindow.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_ucLeadTime.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_btnAddRebateExtension.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_dgvRebateExtension.ReadOnly = (_editMode == RebateEditMode.ReadOnly);

			// Sold From
			_lstSoldFromInclude.Enabled = true;
			_lstSoldFromExclude.Enabled = true;
			_btnSoldFromInclude.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_btnSoldFromExclude.Enabled = (_editMode != RebateEditMode.ReadOnly);

			// Sold To
			_ucSoldToCustomer.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_btnSoldToAdd.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_dgvSoldTo.ReadOnly = true;

			// Ship To
			_ucShipToCustomer.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_btnShipToAdd.Enabled = (_editMode != RebateEditMode.ReadOnly);
			_dgvShipTo.ReadOnly = true;

			// Notes
			_txtNote.Enabled = true;
			_btnNoteAdd.Enabled = true;
			_dgvNotes.ReadOnly = true;
		}

		private void InitialiseForNewRebate()
		{
			_rebate = new Rebate();

			Site site = new Site();
			site.DivisionId = Program.Division.Id;
			site.AddReadFilter(SiteColumn.DivisionId, "=");

			_includeSoldFroms = (ExtendedBindingList<Site>) site.Read();
			_excludeSoldFroms = new ExtendedBindingList<Site>();
			_excludeSoldTos = new ExtendedBindingList<RebateExceptionCustomer>();
			_excludeShipTos = new ExtendedBindingList<RebateExceptionCustomer>();
		}

		private void SetupData()
		{
			// Rebate Range
			_dgvRange.DataSource = _rebate.RebateRanges;
			_dgvRange.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;

			foreach (DataGridViewColumn column in _dgvRange.Columns)
			{
				column.Width = 75;
			}

			// Rebate Detail
			_dgvDetail.EditMode = DataGridViewEditMode.EditOnEnter;
			_dgvDetail.DataSource = new EntityListView<RebateDetail>(_rebate.RebateDetails);
			foreach (DataGridViewColumn column in _dgvDetail.Columns)
			{
				column.ReadOnly = (column.DataPropertyName != RebateDetailProperty.Rate);
			}
			_dgvDetail.Columns[RebateDetailProperty.Rate].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			_dgvDetail.Columns[RebateDetailProperty.Rate].DefaultCellStyle.Format = _numberFormat;

			// VAP
			_dgvVAP.DataSource = new EntityListView<RebateVAP>(_rebate.RebateVAPs);
			foreach (DataGridViewColumn column in _dgvVAP.Columns)
			{
				column.ReadOnly = (column.DataPropertyName != RebateVAPProperty.ActivationVolume);
			}
			_dgvVAP.Columns[RebateVAPProperty.ActivationVolume].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

			// Delivery Windows & Lead Times
			DataBindRebateExtension();
			
			// Sold From Exceptions
			DataBindSoldFrom(_lstSoldFromInclude, _includeSoldFroms);
			DataBindSoldFrom(_lstSoldFromExclude, _excludeSoldFroms);

			// Sold To Exceptions
			_dgvSoldTo.DataSource = new EntityListView<RebateExceptionCustomer>(_excludeSoldTos);

			// Ship To Exceptions
			_dgvShipTo.DataSource = new EntityListView<RebateExceptionCustomer>(_excludeShipTos);

			// Notes
			_dgvNotes.DataSource = new EntityListView<RebateNote>(_rebate.RebateNotes);
		}

		private void DataBindRebateExtension()
		{
			if (_ucRebateType.SelectedValue.IsRebateExtension)
			{
				if (_ucRebateType.SelectedValue.IsDeliveryWindow)
				{
					_dgvRebateExtension.DataSource = new EntityListView<RebateDeliveryWindow>(_rebate.RebateDeliveryWindows);
				}
				else
				{
					_dgvRebateExtension.DataSource = new EntityListView<RebateLeadTime>(_rebate.RebateLeadTimes);
				}

				_ucDeliveryWindow.Visible = _ucRebateType.SelectedValue.IsDeliveryWindow;
				_ucLeadTime.Visible = _ucRebateType.SelectedValue.IsLeadTime;
				_tabControl.TabPages[TabPageName.RebateExtension].Text = (_ucRebateType.SelectedValue.IsDeliveryWindow ? "Delivery Window" : "Lead Time");

				foreach (DataGridViewColumn column in _dgvRebateExtension.Columns)
				{
					if (column.DataPropertyName == RebateDeliveryWindowProperty.Rate ||
						column.DataPropertyName == RebateLeadTimeProperty.Rate)
					{
						column.ReadOnly = false;
					}
					else
					{
						column.ReadOnly = true;
					}
				}
			}
		}

		private void ValidateConfigurationData()
		{
			String errorText = "";

			// Hierarchy, Custonmer Group, and Customer
			if (_ucHierarchy.ComboBox.SelectedValue == null)
			{
				errorText = "Missing hierarchy details";
			}
			else if (_ucHierarchy.ComboBox.SelectedIndex > 0)
			{
				_hiearchyRelatedValueProperty = null;
				Hierarchy hierarchy = (Hierarchy) _ucHierarchy.SelectedValue;
				Control[] controls = this.Controls.Find("_uc" + hierarchy.RelatedEntityName, true);

				if (controls != null && controls.Length > 0)
				{
					_ucHierarchyRelatedControl = controls[0];
					Type type = _ucHierarchyRelatedControl.GetType();

					if (type == null)
					{
						errorText = "Unable to determine type of related hierarchy control";
					}
					else
					{
						_hiearchyRelatedValueProperty = type.GetProperty("SelectedValue");

						if (_hiearchyRelatedValueProperty == null)
						{
							errorText = "Unable to find related controls selection property";
						}
						else
						{
							Object relatedHierachyValue = _hiearchyRelatedValueProperty.GetValue(_ucHierarchyRelatedControl, null);

							if (relatedHierachyValue == null)
							{
								_errorProvider.SetError(_ucHierarchyRelatedControl, "No details specified");
							}
							else
							{
								_errorProvider.SetError(_ucHierarchyRelatedControl, "");
							}
						}
					}
				}
			}

			_errorProvider.SetError(_ucHierarchy, errorText);

			// Check mandatory data
			_errorProvider.SetError(_ucDeliveryType, (_ucDeliveryType.ComboBox.SelectedValue == null ? "Delivery type details not specified" : ""));
			_errorProvider.SetError(_ucRebateType, (_ucRebateType.SelectedValue == null ? "Rebate type details not specified" : ""));
			_errorProvider.SetError(_ucPaymentFrequency, (_ucPaymentFrequency.ComboBox.SelectedValue == null ? "Payment frequency details not specified" : ""));
		}
		
		private void SetupHierarchy()
		{
			// Based on the related entity, activate the relevant control
			Hierarchy hierarchy = (Hierarchy) _ucHierarchy.ComboBox.SelectedValue;

			_lblUcCustomer.Visible = false;
			_ucCustomer.Visible = false;
			_ucCustomerGroup.Visible = false;
			_isPricingGroupAllowed = false;

			_errorProvider.SetError(_ucCustomer, "");
			_errorProvider.SetError(_ucCustomerGroup, "");
			_errorProvider.SetError(_ucPricingSubGroup, "");

			if (hierarchy != null)
			{
				switch (hierarchy.RelatedEntityName)
				{
					case "Customer":
						_ucCustomer.Visible = true;
						_ucCustomer.CustomerSearchModeEnum = hierarchy.CustomerSearchModeEnum;
						_isPricingGroupAllowed = (hierarchy.CustomerSearchModeEnum == CustomerSearchModeEnum.SoldTo);

						_lblUcCustomer.Text = _ucHierarchy.ComboBox.Text;
						_lblUcCustomer.Visible = true;

						break;

					case "CustomerGroup":
						_ucCustomerGroup.Visible = true;
						_isPricingGroupAllowed = true;

						if (_ucCustomerGroup.SelectedValue == null)
						{
							// Prevent selection of sold to customer details in the exclusion screen
							// if we have no valid customer group selected
							_ucSoldToCustomer.CustomerGroupId = -1;
						}
						else
						{
							_ucSoldToCustomer.CustomerGroupId = _ucCustomerGroup.SelectedValue.Id;
						}

						break;
				}
			}

			_ucPricingSubGroup.Visible = _isPricingGroupAllowed;
		}

		private void SetupContract()
		{
			_txtContractText.Visible = _chkIsContract.Checked;
		}

		private void SetupVAP()
		{
			// Control visibility of relevant Estimated Volume fields
			VAPActivationMethod vapActivationMethod = (VAPActivationMethod) _ucVAPActivationMethod.ComboBox.SelectedValue;

			_lblVAPActivationQuantity.Visible = (vapActivationMethod == VAPActivationMethod.Global);
			_txtVAPActivationQuantity.Visible = (vapActivationMethod == VAPActivationMethod.Global);

			if (_dgvVAP.DataSource != null)
			{
				_dgvVAP.Columns[RebateVAPColumn.ActivationVolume].Visible = (vapActivationMethod == VAPActivationMethod.Individual);
			}

			_dgvVAP.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
		}

		private void SetupGlobalProduct()
		{
			if (!Program.Division.AllowGlobalProduct)
			{
				_chkIsGlobalProduct.Visible = false;
				_lblGlobalRate.Visible = false;
				_txtGlobalRate.Visible = false;
				_chkIsGlobalProduct.Checked = false;

				// Shift up controls below the Global Product checkbox
				Control[] controls = { _ucRebateType, _ucPaymentFrequency, _lblEstimatedVolume, _txtEstimatedVolume, _lblFixedValue, _txtFixedValue, _lblRate, _txtRate, _dgvRange };

				foreach (Control control in controls)
				{
					Point point = control.Location;
					point.Y = point.Y - _chkIsGlobalProduct.Size.Height;
					control.Location = point;
				}
			}
		}

		private void SetupRebateType()
		{
			RebateType rebateType = (RebateType) _ucRebateType.ComboBox.SelectedValue;

			if (rebateType != null)
			{
				_chkIsVAP.Visible = !rebateType.IsRebateExtension;
				_txtGlobalRate.Visible = (_chkIsGlobalProduct.Checked && rebateType.IsProductRate);

				_lblEstimatedVolume.Visible = rebateType.IsEstimated;
				_txtEstimatedVolume.Visible = rebateType.IsEstimated;

				_lblFixedValue.Visible = rebateType.IsFixed;
				_txtFixedValue.Visible = rebateType.IsFixed;

				_lblRate.Visible = rebateType.IsFixed;
				_txtRate.Visible = rebateType.IsFixed;

				_grpVolumeRange.Visible = rebateType.IsRangeBased;

				_dgvDetail.Columns[RebateDetailProperty.Rate].Visible = rebateType.IsProductRate;
			}
			else
			{
				_txtGlobalRate.Visible = false;

				_lblEstimatedVolume.Visible = false;
				_txtEstimatedVolume.Visible = false;

				_lblFixedValue.Visible = false;
				_txtFixedValue.Visible = false;

				_grpVolumeRange.Visible = false;
				_dgvDetail.Columns[RebateDetailProperty.Rate].Visible = false;
			}
		}

		private void MoveBetweenListBox(ListBox source, ListBox target)
		{
			ExtendedBindingList<Site> sourceList = (ExtendedBindingList<Site>) source.DataSource;
			ExtendedBindingList<Site> targetList = (ExtendedBindingList<Site>) target.DataSource;
			ExtendedBindingList<Site> removeList = new ExtendedBindingList<Site>();

			// Move from source to target
			foreach (Site site in source.SelectedItems)
			{
				
				targetList.Add(site);
				removeList.Add(site);
			}

			// Now remove from source
			foreach (Site removeSite in removeList)
			{
				sourceList.Remove(removeSite);
			}

			// Re-sort lists and force rebind
			DataBindSoldFrom(source, sourceList);
			DataBindSoldFrom(target, targetList);

			if (removeList.Count > 0)
			{
				MarkFormAsDirty();
			}
		}

		private void DataBindSoldFrom(ListBox listBox, ExtendedBindingList<Site> dataSource)
		{
			listBox.DataSource = null;

			if (dataSource != null)
			{
				dataSource.Sort(SiteProperty.Name);
				listBox.DataSource = dataSource;
				listBox.DisplayMember = SiteProperty.Name;
			}
		}

		private void AddCustomerException(UserControls.Rebates.Customer customer, DataGridView dataGridView, ExceptionTypeEnum exceptionTypeEnum)
		{
			EntityListView<RebateExceptionCustomer> dataList = (EntityListView<RebateExceptionCustomer>) dataGridView.DataSource;

			if (customer.SelectedValues != null)
			{
				foreach (CustomerAddress addCustomer in customer.SelectedValues)
				{
					Boolean isOkToAdd = true;

					foreach (RebateExceptionCustomer rebateException in dataList.List)
					{
						if (rebateException.ExceptionId == addCustomer.Id)
						{
							if (rebateException.IsMarkedForDeletion)
							{
								rebateException.IsMarkedForDeletion = false;
							}

							isOkToAdd = false;
							break;
						}
					}

					if (isOkToAdd)
					{
						RebateExceptionCustomer rebateException = new RebateExceptionCustomer();

						rebateException.Id = 0;
						rebateException.RebateId = (_rebate == null ? 0 : _rebate.Id);
						rebateException.ExceptionTypeEnum = exceptionTypeEnum;
						rebateException.ExceptionId = addCustomer.Id;

						rebateException.JDECode = addCustomer.JDECode;
						rebateException.Name = addCustomer.Name;
						rebateException.Address1 = addCustomer.Address1;
						rebateException.Address2 = addCustomer.Address2;
						rebateException.Address3 = addCustomer.Address3;
						rebateException.Address4 = addCustomer.Address4;
						rebateException.County = addCustomer.County;
						rebateException.PostCode = addCustomer.PostCode;

						dataList.Add(rebateException);
						MarkFormAsDirty();
					}
				}

				dataGridView.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
			}
		}

		private void AddProductDetail()
		{
			DataGridView dataGridView = _dgvDetail;
			EntityListView<RebateDetail> dataListView = (EntityListView<RebateDetail>) dataGridView.DataSource;
			ExtendedBindingList<RebateDetail> dataList = (ExtendedBindingList<RebateDetail>) dataListView.List;

			if (_ucProductDetail.SelectedValues == null)
			{
				if (_ucAttribute1.SelectedValue.Id > 0 ||
					_ucAttribute2.SelectedValue.Id > 0 ||
					_ucAttribute3.SelectedValue.Id > 0 ||
					_ucAttribute4.SelectedValue.Id > 0)
				{
					// Sort datagridview so that the attributes appear at the top (as they have a null ProductId)
					dataList.Sort(RebateDetailProperty.ProductId);

					// Copy the attributes ids from the user controls into an array as it makes it easier to compare later on
					Int32[] newAttributeId = new Int32[5];
					newAttributeId[1] = _ucAttribute1.SelectedValue.Id;
					newAttributeId[2] = _ucAttribute2.SelectedValue.Id;
					newAttributeId[3] = _ucAttribute3.SelectedValue.Id;
					newAttributeId[4] = _ucAttribute4.SelectedValue.Id;

					// Make sure that at least one of the attributes is not null. We can't add a row if they all null.
					Boolean isOkToAdd = true;

					for (Int32 loop = 1; loop <= 4; loop++)
					{
						if (newAttributeId[loop] != 0)
						{
							isOkToAdd = true;
							break;
						}
					}

					if (isOkToAdd)
					{
						// Loop through rows and check to see if this list of attributes is already in.
						foreach (RebateDetail rebateDetail in dataList)
						{
							if (rebateDetail.ProductId.HasValue)
							{
								// Reached the products, so we've checked against all the attributes
								break;
							}
							else
							{
								// Check for duplicate. Loop and com[are. if any differ then it can't be a duplicate
								Boolean match = true;

								for (Int32 loop = 1; loop <= 4; loop++)
								{
									if (rebateDetail.AttributeId(loop) != newAttributeId[loop])
									{
										match = false;
										break;
									}
								}
								if (match)
								{
									isOkToAdd = false;
									rebateDetail.IsMarkedForDeletion = false;
									break;
								}
							}
						}
					}

					if (isOkToAdd)
					{
						// Add new row
						RebateDetail addDetail = new RebateDetail();
						addDetail.AttributeId1 = newAttributeId[1];
						addDetail.AttributeId2 = newAttributeId[2];
						addDetail.AttributeId3 = newAttributeId[3];
						addDetail.AttributeId4 = newAttributeId[4];
						addDetail.AttributeName1 = _ucAttribute1.SelectedValue.Name;
						addDetail.AttributeName2 = _ucAttribute2.SelectedValue.Name;
						addDetail.AttributeName3 = _ucAttribute3.SelectedValue.Name;
						addDetail.AttributeName4 = _ucAttribute4.SelectedValue.Name;
						dataList.Add(addDetail);
						MarkFormAsDirty();
					}
				}
			}
			else
			{
				dataList.Sort(RebateDetailProperty.ProductId);

				foreach (Product product in _ucProductDetail.SelectedValues)
				{
					Boolean isOkToAdd = true;

					foreach (RebateDetail rebateDetail in dataList)
					{
						if (rebateDetail.ProductId.HasValue)
						{
							if (rebateDetail.ProductId == product.Id)
							{
								isOkToAdd = false;
								rebateDetail.IsMarkedForDeletion = false;
								break;
							}
							else if (rebateDetail.ProductId > product.Id)
							{
								break;
							}
						}
					}

					if (isOkToAdd)
					{
						RebateDetail addDetail = new RebateDetail();
						addDetail.ProductId = product.Id;
						addDetail.ProductCode = product.JDECode;
						addDetail.ProductName = product.Name;

						dataList.Add(addDetail);
						MarkFormAsDirty();
					}
				}

			}
			// Re-sort and bind data
			RebindDataGridView(dataGridView, dataList, RebateDetailProperty.ProductCode);
		}

		private void GetFixedRate()
		{
			RebateType rebateType = _ucRebateType.SelectedValue;

			if (rebateType != null && rebateType.IsFixed)
			{
				String rate = "";
				Int32 fixedRebateValue = 0;
				Int32 estimatedVolume = 0;

				if (Int32.TryParse(_txtFixedValue.Text.Replace(",", ""), out fixedRebateValue) &&
					Int32.TryParse(_txtEstimatedVolume.Text.Replace(",", ""), out estimatedVolume))
				{
					Rebate rebate = new Rebate();

					rebate.FixedRebateValue = fixedRebateValue;
					rebate.EstimatedVolume = estimatedVolume;
					rebate.RebateTypeId = rebateType.Id;

					RebateTypeFixed rebateTypeFixed = (RebateTypeFixed) rebateType.GetCalculator(rebate);
					rate = rebateTypeFixed.GetFixedRate().ToString("#,0.00");
				}

				_txtRate.Text = rate;
			}
		}

		private void AddVAPProduct()
		{
			DataGridView dataGridView = _dgvVAP;
			EntityListView<RebateVAP> dataListView = (EntityListView<RebateVAP>) dataGridView.DataSource;
			ExtendedBindingList<RebateVAP> dataList = (ExtendedBindingList<RebateVAP>) dataListView.List;

			if (_ucProductVAP.SelectedValues != null)
			{
				dataList.Sort(RebateVAPProperty.ProductId);

				foreach (Product product in _ucProductVAP.SelectedValues)
				{
					Boolean isOkToAdd = true;

					foreach (RebateVAP rebateVAP in dataList)
					{
						if (rebateVAP.ProductId == product.Id)
						{
							isOkToAdd = false;
							rebateVAP.IsMarkedForDeletion = false;	
							break;
						}
						else if (rebateVAP.ProductId > product.Id)
						{
							break;
						}
					}

					if (isOkToAdd)
					{
						RebateVAP addRebateVAP = new RebateVAP();
						addRebateVAP.ProductId = product.Id;
						addRebateVAP.ProductCode = product.JDECode;
						addRebateVAP.ProductName = product.Name;
						addRebateVAP.ActivationVolume = null;

						dataList.Add(addRebateVAP);
						MarkFormAsDirty();
					}
				}

			}
			// Re-sort and bind data
			RebindDataGridView(dataGridView, dataList, RebateVAPProperty.ProductCode);
		}

		private void AddDeliveryWindow()
		{
			DataGridView dataGridView = _dgvRebateExtension;

			if (_ucRebateType.SelectedValue.IsDeliveryWindow)
			{
				EntityListView<RebateDeliveryWindow> dataListView = (EntityListView<RebateDeliveryWindow>) dataGridView.DataSource;
				ExtendedBindingList<RebateDeliveryWindow> dataList = (ExtendedBindingList<RebateDeliveryWindow>) dataListView.List;

				if (_ucDeliveryWindow.SelectedValue != null)
				{
					DeliveryWindow deliveryWindow = _ucDeliveryWindow.SelectedValue;

					RebateDeliveryWindow rebateDeliveryWindow = new RebateDeliveryWindow();
					rebateDeliveryWindow.DeliveryWindowId = deliveryWindow.Id;
					rebateDeliveryWindow.PackingTypeEnum = _ucPackingType.SelectedValue;
					rebateDeliveryWindow.DeliveryWindowJDECode = deliveryWindow.JDECode;
					rebateDeliveryWindow.DeliveryWindowName = deliveryWindow.Name;
					dataList.Add(rebateDeliveryWindow);

					_ucDeliveryWindow.ExcludeIds.Add(deliveryWindow.Id);
					_ucDeliveryWindow.Populate();
					MarkFormAsDirty();
				}

				// Re-sort and bind data
				RebindDataGridView(dataGridView, dataList, RebateDeliveryWindowProperty.DeliveryWindowJDECode);
			}
			else
			{
				EntityListView<RebateLeadTime> dataListView = (EntityListView<RebateLeadTime>) dataGridView.DataSource;
				ExtendedBindingList<RebateLeadTime> dataList = (ExtendedBindingList<RebateLeadTime>) dataListView.List;

				if (_ucLeadTime.SelectedValue != null)
				{
					LeadTime leadTime = _ucLeadTime.SelectedValue;

					RebateLeadTime rebateLeadTime = new RebateLeadTime();
					rebateLeadTime.LeadTimeId = leadTime.Id;
					rebateLeadTime.PackingTypeEnum = _ucPackingType.SelectedValue;
					rebateLeadTime.LeadTimeJDECode = leadTime.JDECode;
					rebateLeadTime.LeadTimeName = leadTime.Name;
					dataList.Add(rebateLeadTime);

					_ucLeadTime.ExcludeIds.Add(leadTime.Id);
					_ucLeadTime.Populate();
					MarkFormAsDirty();
				}

				// Re-sort and bind data
				RebindDataGridView(dataGridView, dataList, RebateLeadTimeProperty.LeadTimeJDECode);
			}
		}

		private void AddRebateNote()
		{
			if (_txtNote.Text.Length > 0)
			{
				RebateNote note = new RebateNote();

				note.Id = 0;
				note.RebateId = 0;
				note.UserId = Program.User.Id;
				note.UserName = Program.User.Name;
				note.CreateTime = DateTime.Now;
				note.Note = _txtNote.Text;

				_rebate.RebateNotes.Insert(0, note);

				MarkFormAsDirty();
				_txtNote.Text = "";
			}

		}

		private void RebindDataGridView<T>(DataGridView dataGridView, ExtendedBindingList<T> dataList, String defaultSort)
		{
			dataGridView.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
		}

		private void SetDefaultStartAndEndDate(Boolean isNew)
		{
 			Int16 year = Program.Division.NextAccrualPeriod().Year;

			// Ensure that the DateTimePickerNull defaults to the correct date on DropDown, but actually displays
			// as empty
			_dtpStartDate.Value = isNew ? new DateTime(year, 1, 1) : _rebate.StartDate;

			if (isNew)
			{
				_dtpStartDate.Value = null;
			}

			_dtpEndDate.Value = (_dtpStartDate.Value > DateTime.Now.Date ? _dtpStartDate.Value : DateTime.Now.Date);
			_dtpEndDate.Value = null;

			if (!isNew)
			{
				_dtpEndDate.Value = _rebate.EndDate;
			}
		}

		private void CopyRebate()
		{
			if (_rebate.Id != 0)
			{
				Rebate rebate = _rebate.Copy(Program.User);

				frmRebate frmRebate = new frmRebate(rebate);
				frmRebate.MdiParent = this.MdiParent;
				frmRebate.Show();
				frmRebate.Activate();
			}
		}

		#region TabPages

		private void DisplayTabPages()
		{
			Boolean show;

			#region Product Detail
			show = true;
			if (_chkIsGlobalProduct.Checked)
			{
				show = false;
			}
			if (_ucRebateType.SelectedValue.IsRebateExtension)
			{
				show = false;
			}
			ShowTabPage(_tabPageDetail, show);
			#endregion

			#region VAP
			show = (_chkIsVAP.Checked);
			if (_ucRebateType.SelectedValue.IsRebateExtension)
			{
				show = false;
			}
			ShowTabPage(_tabPageVAP, show);
			#endregion

			#region Delivery Window
			show = _ucRebateType.SelectedValue.IsRebateExtension;
			ShowTabPage(_tabPageRebateExtension, show);
			#endregion

			#region Sold From
			show = _ucSoldFrom.SelectedValue != null && _ucSoldFrom.SelectedValue.Id == 0;
			ShowTabPage(_tabPageSoldFrom, show);
			#endregion

			Hierarchy hierarchy = (Hierarchy) _ucHierarchy.ComboBox.SelectedValue;

			#region Sold To
			show = hierarchy != null && hierarchy.Level == 1;
			ShowTabPage(_tabPageSoldTo, show);
			#endregion

			#region Ship To
			show = hierarchy != null && (hierarchy.Level == 1 || hierarchy.Level == 2);
			ShowTabPage(_tabPageShipTo, show);
			#endregion
		}

		private void HideTabPage(TabPage tabPage)
		{
			if (_tabControl.TabPages.Contains(tabPage))
			{
				_tabControl.TabPages.Remove(tabPage);
			}
		}
		private void ShowTabPage(TabPage tabPage, Boolean show)
		{
			if(show)
			{
				ShowTabPage(tabPage);
			}
			else
			{
				HideTabPage(tabPage);
			}
		}
		private void ShowTabPage(TabPage tabPage)
		{
			if (!_tabControl.Contains(tabPage))
			{
				String name = tabPage.Name;
				Boolean inserted = false;

				do
				{
					String insertAfter = "";

					// Find the name of the tab we want to insert after
					switch (name)
					{
						case TabPageName.Header:
							// First tab
							break;
						case TabPageName.Detail:
							insertAfter = TabPageName.Header;
							break;
						case TabPageName.VAP:
							insertAfter = TabPageName.Detail;
							break;
						case TabPageName.RebateExtension:
							insertAfter = TabPageName.VAP;
							break;
						case TabPageName.SoldFrom:
							insertAfter = TabPageName.RebateExtension;
							break;
						case TabPageName.SoldTo:
							insertAfter = TabPageName.SoldFrom;
							break;
						case TabPageName.ShipTo:
							insertAfter = TabPageName.SoldTo;
							break;
						case TabPageName.Notes:
							insertAfter = TabPageName.ShipTo;
							break;
						default:
							throw new ArgumentOutOfRangeException("Unknown tabpage of " + name);
					}

					Int32 index = -1;

					if (insertAfter == "")
					{
						// Need to insert as the first tab, hence the index must be zero
						index = 0;
					}
					else
					{
						// Check to see if the tab we want to insert after exists
						index = _tabControl.TabPages.IndexOfKey(insertAfter);

						if(index > -1)
						{
							// Found so the index must be the one after this
							index++;
						}
					}

					
					if(index >= 0)
					{
						// We have an index to insert at then insert the tab and flag as inserted so we exit the do...while loop
						_tabControl.TabPages.Insert(index, tabPage);
						inserted = true;
					}
					else
					{
						// Can't find this tab to insert after so see if the tab before that exists.
						// Force it round the loop again and try the preceeding tab.
						name = insertAfter;
					}
				}
				while (!inserted);
			}
		}

		#endregion

		#endregion

		private class TabPageName
		{
			public const string Header = "Header";
			public const string Detail = "Detail";
			public const string VAP = "VAP";
			public const string RebateExtension = "RebateExtension";
			public const string SoldFrom = "SoldFrom";
			public const string SoldTo = "SoldTo";
			public const string ShipTo = "ShipTo";
			public const string Notes = "Notes";
		}

		private class DragDropData
		{
			private ExtendedBindingList<Business.Rebates.Site> _source;
			private List<Business.Rebates.Site> _items;

			public DragDropData(ExtendedBindingList<Business.Rebates.Site> source, List<Business.Rebates.Site> items)
			{
				_source = source;
				_items = items;
			}

			public ExtendedBindingList<Business.Rebates.Site> Source
			{
				get { return _source; }
			}

			public List<Business.Rebates.Site> Items
			{
				get { return _items; }
			}
		}
	}
}

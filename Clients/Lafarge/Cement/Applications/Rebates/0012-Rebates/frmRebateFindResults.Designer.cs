﻿namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	partial class frmRebateFindResults
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRebateFindResults));
			this._bindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
			this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
			this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
			this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
			this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
			this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
			this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
			this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
			this.bindingNavigatorSeperator2 = new System.Windows.Forms.ToolStripSeparator();
			this.bindingNavigatorButtonRefresh = new System.Windows.Forms.ToolStripButton();
			this.bindingNavigatorSeparator3 = new System.Windows.Forms.ToolStripSeparator();
			this.bindingNavigatorPayThruPeriodLabel = new System.Windows.Forms.ToolStripLabel();
			this.bindingNavigatorPayThruPeriod = new System.Windows.Forms.ToolStripTextBox();
			this.toolStripShowAllLabel = new System.Windows.Forms.ToolStripLabel();
			this.bindingNavigatorSeparator4 = new System.Windows.Forms.ToolStripSeparator();
			this.bindingNavigatorPay = new System.Windows.Forms.ToolStripButton();
			this._dgvSearchResults = new System.Windows.Forms.DataGridView();
			((System.ComponentModel.ISupportInitialize) (this._bindingNavigator)).BeginInit();
			this._bindingNavigator.SuspendLayout();
			((System.ComponentModel.ISupportInitialize) (this._dgvSearchResults)).BeginInit();
			this.SuspendLayout();
			// 
			// _bindingNavigator
			// 
			this._bindingNavigator.AddNewItem = null;
			this._bindingNavigator.CountItem = this.bindingNavigatorCountItem;
			this._bindingNavigator.DeleteItem = null;
			this._bindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeperator2,
            this.bindingNavigatorButtonRefresh,
            this.bindingNavigatorSeparator3,
            this.bindingNavigatorPayThruPeriodLabel,
            this.bindingNavigatorPayThruPeriod,
            this.toolStripShowAllLabel,
            this.bindingNavigatorSeparator4,
            this.bindingNavigatorPay});
			this._bindingNavigator.Location = new System.Drawing.Point(0, 0);
			this._bindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
			this._bindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
			this._bindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
			this._bindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
			this._bindingNavigator.Name = "_bindingNavigator";
			this._bindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
			this._bindingNavigator.Size = new System.Drawing.Size(789, 25);
			this._bindingNavigator.TabIndex = 0;
			this._bindingNavigator.Text = "bindingNavigator1";
			// 
			// bindingNavigatorCountItem
			// 
			this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
			this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
			this.bindingNavigatorCountItem.Text = "of {0}";
			this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
			// 
			// bindingNavigatorMoveFirstItem
			// 
			this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image) (resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
			this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
			this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
			this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
			this.bindingNavigatorMoveFirstItem.Text = "Move first";
			// 
			// bindingNavigatorMovePreviousItem
			// 
			this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image) (resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
			this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
			this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
			this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
			this.bindingNavigatorMovePreviousItem.Text = "Move previous";
			// 
			// bindingNavigatorSeparator
			// 
			this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
			this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
			// 
			// bindingNavigatorPositionItem
			// 
			this.bindingNavigatorPositionItem.AccessibleName = "Position";
			this.bindingNavigatorPositionItem.AutoSize = false;
			this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
			this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
			this.bindingNavigatorPositionItem.Text = "0";
			this.bindingNavigatorPositionItem.ToolTipText = "Current position";
			// 
			// bindingNavigatorSeparator1
			// 
			this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
			this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
			// 
			// bindingNavigatorMoveNextItem
			// 
			this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image) (resources.GetObject("bindingNavigatorMoveNextItem.Image")));
			this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
			this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
			this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
			this.bindingNavigatorMoveNextItem.Text = "Move next";
			// 
			// bindingNavigatorMoveLastItem
			// 
			this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image) (resources.GetObject("bindingNavigatorMoveLastItem.Image")));
			this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
			this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
			this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
			this.bindingNavigatorMoveLastItem.Text = "Move last";
			// 
			// bindingNavigatorSeperator2
			// 
			this.bindingNavigatorSeperator2.Name = "bindingNavigatorSeperator2";
			this.bindingNavigatorSeperator2.Size = new System.Drawing.Size(6, 25);
			// 
			// bindingNavigatorButtonRefresh
			// 
			this.bindingNavigatorButtonRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.bindingNavigatorButtonRefresh.Image = global::Evolve.Clients.Lafarge.Cement.Rebates.Properties.Resources.arrow_refresh;
			this.bindingNavigatorButtonRefresh.ImageTransparentColor = System.Drawing.Color.White;
			this.bindingNavigatorButtonRefresh.Name = "bindingNavigatorButtonRefresh";
			this.bindingNavigatorButtonRefresh.Size = new System.Drawing.Size(23, 22);
			this.bindingNavigatorButtonRefresh.Text = "Refresh";
			// 
			// bindingNavigatorSeparator3
			// 
			this.bindingNavigatorSeparator3.Name = "bindingNavigatorSeparator3";
			this.bindingNavigatorSeparator3.Size = new System.Drawing.Size(6, 25);
			// 
			// bindingNavigatorPayThruPeriodLabel
			// 
			this.bindingNavigatorPayThruPeriodLabel.Name = "bindingNavigatorPayThruPeriodLabel";
			this.bindingNavigatorPayThruPeriodLabel.Size = new System.Drawing.Size(91, 22);
			this.bindingNavigatorPayThruPeriodLabel.Text = "Pay Thru Period";
			// 
			// bindingNavigatorPayThruPeriod
			// 
			this.bindingNavigatorPayThruPeriod.Enabled = false;
			this.bindingNavigatorPayThruPeriod.Name = "bindingNavigatorPayThruPeriod";
			this.bindingNavigatorPayThruPeriod.Size = new System.Drawing.Size(50, 25);
			// 
			// toolStripShowAllLabel
			// 
			this.toolStripShowAllLabel.Name = "toolStripShowAllLabel";
			this.toolStripShowAllLabel.Size = new System.Drawing.Size(59, 22);
			this.toolStripShowAllLabel.Text = " Show All ";
			// 
			// bindingNavigatorSeparator4
			// 
			this.bindingNavigatorSeparator4.Name = "bindingNavigatorSeparator4";
			this.bindingNavigatorSeparator4.Size = new System.Drawing.Size(6, 25);
			// 
			// bindingNavigatorPay
			// 
			this.bindingNavigatorPay.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.bindingNavigatorPay.Image = global::Evolve.Clients.Lafarge.Cement.Rebates.Properties.Resources.sterling_pound_currency_sign;
			this.bindingNavigatorPay.ImageTransparentColor = System.Drawing.Color.White;
			this.bindingNavigatorPay.Name = "bindingNavigatorPay";
			this.bindingNavigatorPay.Size = new System.Drawing.Size(23, 22);
			this.bindingNavigatorPay.Text = "Process Payments";
			// 
			// _dgvSearchResults
			// 
			this._dgvSearchResults.AllowUserToAddRows = false;
			this._dgvSearchResults.AllowUserToDeleteRows = false;
			this._dgvSearchResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this._dgvSearchResults.Dock = System.Windows.Forms.DockStyle.Fill;
			this._dgvSearchResults.Location = new System.Drawing.Point(0, 25);
			this._dgvSearchResults.Name = "_dgvSearchResults";
			this._dgvSearchResults.ReadOnly = true;
			this._dgvSearchResults.Size = new System.Drawing.Size(789, 460);
			this._dgvSearchResults.TabIndex = 1;
			// 
			// frmRebateFindResults
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(789, 485);
			this.Controls.Add(this._dgvSearchResults);
			this.Controls.Add(this._bindingNavigator);
			this.Name = "frmRebateFindResults";
			this.Text = "Find Rebate Results";
			((System.ComponentModel.ISupportInitialize) (this._bindingNavigator)).EndInit();
			this._bindingNavigator.ResumeLayout(false);
			this._bindingNavigator.PerformLayout();
			((System.ComponentModel.ISupportInitialize) (this._dgvSearchResults)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.BindingNavigator _bindingNavigator;
		private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
		private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
		private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
		private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
		private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
		private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
		private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
		private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
		private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator3;
		private System.Windows.Forms.DataGridView _dgvSearchResults;
		private System.Windows.Forms.ToolStripLabel bindingNavigatorPayThruPeriodLabel;
		private System.Windows.Forms.ToolStripButton bindingNavigatorPay;
		private System.Windows.Forms.ToolStripTextBox bindingNavigatorPayThruPeriod;
		private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeperator2;
		private System.Windows.Forms.ToolStripButton bindingNavigatorButtonRefresh;
		private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator4;
		private System.Windows.Forms.ToolStripLabel toolStripShowAllLabel;
	}
}
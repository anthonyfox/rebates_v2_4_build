﻿namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	partial class frmForecast
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this._labelDescription = new System.Windows.Forms.Label();
			this._lnkLastCalculation = new System.Windows.Forms.LinkLabel();
			this._btnCalculate = new System.Windows.Forms.Button();
			this._pnlCalculationStatus = new System.Windows.Forms.Panel();
			this._lblProgress = new System.Windows.Forms.Label();
			this._pgbProgress = new System.Windows.Forms.ProgressBar();
			this._lblStage = new System.Windows.Forms.Label();
			this._pgbStage = new System.Windows.Forms.ProgressBar();
			this._pnlTop = new System.Windows.Forms.Panel();
			this._pnlCalculationStatus.SuspendLayout();
			this._pnlTop.SuspendLayout();
			this.SuspendLayout();
			// 
			// _labelDescription
			// 
			this._labelDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._labelDescription.Location = new System.Drawing.Point(11, 20);
			this._labelDescription.Name = "_labelDescription";
			this._labelDescription.Size = new System.Drawing.Size(477, 43);
			this._labelDescription.TabIndex = 5;
			this._labelDescription.Text = "This process will calculate Forecast Accruals based on the current Forecast Invoi" +
    "ces and matching Rebates. All existing Forecast Accruals will be removed from th" +
    "e system and replaced";
			// 
			// _lnkLastCalculation
			// 
			this._lnkLastCalculation.AutoSize = true;
			this._lnkLastCalculation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this._lnkLastCalculation.Location = new System.Drawing.Point(12, 63);
			this._lnkLastCalculation.Name = "_lnkLastCalculation";
			this._lnkLastCalculation.Size = new System.Drawing.Size(224, 13);
			this._lnkLastCalculation.TabIndex = 7;
			this._lnkLastCalculation.TabStop = true;
			this._lnkLastCalculation.Text = "Last Forecast Calculation %LastDate%";
			// 
			// _btnCalculate
			// 
			this._btnCalculate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this._btnCalculate.Location = new System.Drawing.Point(14, 117);
			this._btnCalculate.MinimumSize = new System.Drawing.Size(165, 23);
			this._btnCalculate.Name = "_btnCalculate";
			this._btnCalculate.Size = new System.Drawing.Size(165, 23);
			this._btnCalculate.TabIndex = 8;
			this._btnCalculate.Text = "Calculate Forecast Accruals";
			this._btnCalculate.UseVisualStyleBackColor = true;
			// 
			// _pnlCalculationStatus
			// 
			this._pnlCalculationStatus.Controls.Add(this._lblProgress);
			this._pnlCalculationStatus.Controls.Add(this._pgbProgress);
			this._pnlCalculationStatus.Controls.Add(this._lblStage);
			this._pnlCalculationStatus.Controls.Add(this._pgbStage);
			this._pnlCalculationStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
			this._pnlCalculationStatus.Location = new System.Drawing.Point(0, 164);
			this._pnlCalculationStatus.Name = "_pnlCalculationStatus";
			this._pnlCalculationStatus.Size = new System.Drawing.Size(500, 120);
			this._pnlCalculationStatus.TabIndex = 11;
			// 
			// _lblProgress
			// 
			this._lblProgress.AutoSize = true;
			this._lblProgress.Location = new System.Drawing.Point(11, 64);
			this._lblProgress.Name = "_lblProgress";
			this._lblProgress.Size = new System.Drawing.Size(186, 13);
			this._lblProgress.TabIndex = 2;
			this._lblProgress.Text = "Forecast Accrual Calculation Progress";
			// 
			// _pgbProgress
			// 
			this._pgbProgress.Location = new System.Drawing.Point(10, 80);
			this._pgbProgress.Name = "_pgbProgress";
			this._pgbProgress.Size = new System.Drawing.Size(481, 23);
			this._pgbProgress.TabIndex = 3;
			// 
			// _lblStage
			// 
			this._lblStage.AutoSize = true;
			this._lblStage.Location = new System.Drawing.Point(11, 14);
			this._lblStage.Name = "_lblStage";
			this._lblStage.Size = new System.Drawing.Size(90, 13);
			this._lblStage.TabIndex = 0;
			this._lblStage.Text = "Stage Information";
			// 
			// _pgbStage
			// 
			this._pgbStage.Location = new System.Drawing.Point(10, 30);
			this._pgbStage.Name = "_pgbStage";
			this._pgbStage.Size = new System.Drawing.Size(481, 23);
			this._pgbStage.TabIndex = 1;
			// 
			// _pnlTop
			// 
			this._pnlTop.Controls.Add(this._lnkLastCalculation);
			this._pnlTop.Controls.Add(this._labelDescription);
			this._pnlTop.Controls.Add(this._btnCalculate);
			this._pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
			this._pnlTop.Location = new System.Drawing.Point(0, 0);
			this._pnlTop.Name = "_pnlTop";
			this._pnlTop.Size = new System.Drawing.Size(500, 158);
			this._pnlTop.TabIndex = 12;
			// 
			// frmForecast
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(500, 284);
			this.Controls.Add(this._pnlTop);
			this.Controls.Add(this._pnlCalculationStatus);
			this.Name = "frmForecast";
			this.Text = "Forecast Accruals";
			this._pnlCalculationStatus.ResumeLayout(false);
			this._pnlCalculationStatus.PerformLayout();
			this._pnlTop.ResumeLayout(false);
			this._pnlTop.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label _labelDescription;
		private System.Windows.Forms.LinkLabel _lnkLastCalculation;
		private System.Windows.Forms.Button _btnCalculate;
		private System.Windows.Forms.Panel _pnlCalculationStatus;
		private System.Windows.Forms.Label _lblProgress;
		private System.Windows.Forms.ProgressBar _pgbProgress;
		private System.Windows.Forms.Label _lblStage;
		private System.Windows.Forms.ProgressBar _pgbStage;
		private System.Windows.Forms.Panel _pnlTop;
	}
}
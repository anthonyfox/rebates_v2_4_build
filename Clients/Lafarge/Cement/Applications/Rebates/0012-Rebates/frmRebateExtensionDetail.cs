﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;
using Evolve.Libraries.WinformsApplication;

namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	public partial class frmRebateExtensionDetail : Form
	{
		private IRebateExtension _rebateExtension;
		private Type _rebateExtensionType;

		public IRebateExtension RebateExtension
		{
			get { return _rebateExtension; }
			set { _rebateExtension = value; }
		}

		public frmRebateExtensionDetail(Type rebateExtensionType)
		{
			InitializeComponent();
			_rebateExtensionType = rebateExtensionType;

			this.Load += new EventHandler(frmRebateExtensionDetail_Load);
			this.okCancel.ButtonClick += new Libraries.Controls.Generic.OkCancel.ButtonClickHandler(okCancel_ButtonClick);
			_txtCode.Validating += new CancelEventHandler(_txtCode_Validating);
			_txtDescription.Validating += new CancelEventHandler(_txtDescription_Validating);
		}

		void frmRebateExtensionDetail_Load(object sender, EventArgs e)
		{
			if (this.MdiParent != null)
			{
				this.Icon = MdiParent.Icon;
			}

			// Set form title (need new instance incase we are in add mode)
			IRebateExtension rebateExtension = (IRebateExtension) Activator.CreateInstance(_rebateExtensionType);
			this.Text = rebateExtension.Title;
			_txtCode.Size = new Size(rebateExtension.TextBoxWidth, _txtCode.Height);
			_txtCode.MaxLength = rebateExtension.MaxLength;

			if (_rebateExtension != null)
			{
				_rebateExtension.ReadCurrent();

				_txtCode.Text = _rebateExtension.JDECode;
				_txtCode.ReadOnly = true;
				_txtDescription.Text = _rebateExtension.Name;

				this.ActiveControl = _txtDescription;
			}
		}

		void okCancel_ButtonClick(object sender, EventArgs e)
		{
			if (okCancel.DialogResult == DialogResult.OK)
			{
				this.Validate();

				if (!ValidateControl.HasErrors(this, _errorProvider))
				{
					if (Save())
					{
						this.Close();
					}
				}
			}
			else
			{
				if (_rebateExtension != null)
				{
					_rebateExtension.ReadCurrent();
				}

				this.Close();
			}
		}

		void _txtCode_Validating(object sender, CancelEventArgs e)
		{
			if (!_txtCode.ReadOnly)
			{
				_errorProvider.SetError(_txtCode, ValidateJDECode(_txtCode.Text));
			}
		}

		void _txtDescription_Validating(object sender, CancelEventArgs e)
		{
			_errorProvider.SetError(_txtDescription, ValidateDescription(_txtDescription.Text));
		}

		private Boolean Save()
		{
			Boolean isSaved = false;

			// Save changes
			if (_rebateExtension == null)
			{
				_rebateExtension = (IRebateExtension) Activator.CreateInstance(_rebateExtensionType);
				_rebateExtension.DivisionId = Program.Division.Id;
				_rebateExtension.JDECode = _txtCode.Text;
				_rebateExtension.Name = _txtDescription.Text;
				_rebateExtension.CreateUserId = Program.User.Id;
				_rebateExtension.CreateTime = DateTime.Now;
			}
			else
			{
				if (_rebateExtension.Name != _txtDescription.Text)
				{
					_rebateExtension.Name = _txtDescription.Text;
					_rebateExtension.UpdateUserId = Program.User.Id;
					_rebateExtension.UpdateTime = DateTime.Now;
				}
			}

			// Save to DB
			if (_rebateExtension.IsEntityDirty)
			{
				Boolean retry = false;

				do
				{
					try
					{
						_rebateExtension.AcceptChanges();
						_rebateExtension.SetColumnClean();

						isSaved = true;
						DialogResult = DialogResult.OK;
					}
					catch (DatabaseException dbException)
					{
						if (dbException.IsLockError)
						{
							retry = (ApplicationDomain.LockMessage(dbException.Message) == DialogResult.Yes);
						}
						else
						{
							throw dbException;
						}
					}
				} while (retry);
			}

			return isSaved;
		}

		private String ValidateJDECode(String code)
		{
			String error = "";

			if (String.IsNullOrEmpty(code))
			{
				error = "JDE code must be specified";
			}
			else
			{
				IRebateExtension rebateExtension = (IRebateExtension) Activator.CreateInstance(_rebateExtensionType);
				rebateExtension.JDECode = code;
				rebateExtension.AddReadFilter(DeliveryWindowProperty.JDECode, "=");

				Int32 count = rebateExtension.ReadCount();

				if (count > 0)
				{
					error = rebateExtension.Title + " code already exists";
				}
			}

			return error;
		}

		private String ValidateDescription(String description)
		{
			String error = "";

			if (description.Trim().Length == 0)
			{
				error = "Description must be entered";
			}

			return error;
		}
	}
}

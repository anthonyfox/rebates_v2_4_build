﻿using System;
using System.IO;
using System.Net.Mail;
using System.Reflection;
using System.Security.Principal;
using System.ServiceProcess;
using System.Timers;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;
using Evolve.Libraries.SubVersion;

namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	public partial class AccrualCalculationService : ServiceBase
	{
		private User _user;
		private User User { get { return _user; } set { _user = value; } }

		private Timer _timer;
		private Timer Timer { get { return _timer; } set { _timer = value; } }

		private DateTime _launchTime;
		private DateTime LaunchTime { get { return _launchTime; } set { _launchTime = value; } }

		private String LaunchTimeString { get { return this.LaunchTime.ToShortDateString() + " " + this.LaunchTime.ToShortTimeString(); } }

		private Boolean _isCalculating;
		private Boolean IsCalculating { get { return _isCalculating; } set { _isCalculating = value; } }

		private bool _hasAutoFinalised = false;

		public AccrualCalculationService()
		{
			InitializeComponent();
		}

		protected override void OnStart(string[] args)
		{
			base.OnStart(args);

			// Check to see if we need to connect the debugger
			WindowsServiceUtilities.Debugger(AppDomain.CurrentDomain, Assembly.GetEntryAssembly());

			// Service Initialisation
			try
			{
				// Initiate logging services
				InitiateLoggingService();

				// Setup Data Access Server configuration
				ConfigureDataAccess();

				// Authentificate to database
				AuthentificateUser();
			}
			catch (Exception ex)
			{
				if (Logger.Instance != null)
				{
					Logger.Instance.Error(ex);
				}

				Logger.Instance.Information("Initialisation Error, Stopping Service");
				this.Stop();
			}

			// Send Test Mail
			if (Properties.Settings.Default.SendTestMailOnStartUp)
			{
				Logger.Instance.Information("Sending Test Mail");
				SendNotificationEmail("Test notification message sent on service startup - This is not an error");
			}

			// No run the main process
			try
			{
				Logger.Instance.Information("Started on Environment: " + UserAuthentification.Environment + " (SVN Revision " + Build.Revision + ")");

				// Initialise Timer (if you wish to bypass the timer
				// during debug change the value of performInstantRun to true in the debugger)
				bool performInstantRun = Properties.Settings.Default.PerformInstantRunOnStartup;

				if (performInstantRun)
				{
					// In debug mode force reset the flags
					AccrualCalculationServiceMode runMode = (AccrualCalculationServiceMode)Properties.Settings.Default.RunMode;
	
					foreach (UserDivision userDivision in this.User.UserDivisions)
					{
						if ((runMode & AccrualCalculationServiceMode.Actuals) == AccrualCalculationServiceMode.Actuals)
						{
							userDivision.Division.EndCalculation(User, AccrualTypeEnum.Actual);
						}

						if ((runMode & AccrualCalculationServiceMode.Forecast) == AccrualCalculationServiceMode.Forecast)
						{
							userDivision.Division.EndCalculation(User, AccrualTypeEnum.Forecast);
						}
					}
				}

				// Finally initialise the timer (set to 1 second in debug mode, and then reverts to usual schedule)
				InitialiseTimer(performInstantRun);
			}
			catch (Exception ex)
			{
				Logger.Instance.Error(ex);
			}
		}

		protected override void OnStop()
		{
			Logger.Instance.Information("Service Stop Request");
			while (this.IsCalculating) { };
			Logger.Instance.Information("Stopping");

			base.OnStop();
		}
		
		/// <summary>
		/// Initiates the service error and message logging
		/// </summary>
		private void InitiateLoggingService()
		{
			Logger logger = Logger.Instance;

			if (Properties.Settings.Default.LogFilePath == null || Properties.Settings.Default.LogFilePath.Length == 0)
			{
				logger.LogFilePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			}
			else
			{
				logger.LogFilePath = Properties.Settings.Default.LogFilePath;
			}

			logger.FileName = "AccrualCalculationService";
			logger.Level = Properties.Settings.Default.LogLevel;
		}

		/// <summary>
		/// Configures the Data Access Server
		/// </summary>
		private void ConfigureDataAccess()
		{
			// Attempt to read Machine Specific Data Access Server configuration details
			String localMachine = System.Environment.MachineName.ToUpper();
			Type type = typeof(Properties.Settings);

			Logger.Instance.Information("Local Machine Name: " + localMachine);

			PropertyInfo dasProperty = type.GetProperty(localMachine + "_DataAccessServer");
			PropertyInfo dasPortProperty = type.GetProperty(localMachine + "_DataAccessServerPort");
			PropertyInfo dasProtocolProperty = type.GetProperty(localMachine + "_DataAccessServerProtocol");

			// Initialise Data Access Server with generic configuration which assumes that the
			// Data Access Server, and Rebates Accrual Calculation Service are located on the same
			// machine
			DataAccessServer.Server = Properties.Settings.Default.DataAccessServer;
			DataAccessServer.Port = Properties.Settings.Default.DataAccessServerPort;
			DataAccessServer.Protocol = Properties.Settings.Default.DataAccessServerProtocol;

			// Look for machine specific configurations and override if required
			if (dasProperty != null)
			{
				DataAccessServer.Server = dasProperty.GetValue(Properties.Settings.Default, null).ToString();
			}

			if (dasPortProperty != null)
			{
				DataAccessServer.Port = (Int32) dasProperty.GetValue(Properties.Settings.Default, null);
			}

			if (dasProtocolProperty != null)
			{
				DataAccessServer.Protocol = dasProtocolProperty.GetValue(Properties.Settings.Default, null).ToString();
			}

			DataAccessServer.SetSecurity();
		}

		/// <summary>
		/// Authentificates the database access and environment setup
		/// </summary>
		private void AuthentificateUser()
		{
			String localMachine = System.Environment.MachineName.ToUpper();
			Type type = typeof(Properties.Settings);
			WindowsIdentity windowsIndentity = WindowsIdentity.GetCurrent();
			PropertyInfo environmentProperty = type.GetProperty(localMachine + "_EnvironmentName");

			UserAuthentification.WindowsUser = windowsIndentity;
			UserAuthentification.Environment = Properties.Settings.Default.EnvironmentName;

			if (environmentProperty != null)
			{
				UserAuthentification.Environment = environmentProperty.GetValue(Properties.Settings.Default, null).ToString();
			}

			UserAuthentification.ReadCredentials();

			if (UserAuthentification.Credentials == null)
			{
				ArgumentNullException ex = new ArgumentNullException("Failed to authentificate onto the database for Environment: " + UserAuthentification.Environment);
				throw ex;
			}

			ValidateUser();
			SetupDivision();
		}

		/// <summary>
		/// Validates the Rebates user configuration
		/// </summary>
		private void ValidateUser()
		{
			String errorMessage = "";

			// Read users that match tis user code
			User user = new User();
			user.Code = UserAuthentification.Credentials.ApplicationUserId;
			ExtendedBindingList<User> users = (ExtendedBindingList<User>) user.ReadWithDirtyProperty();

			switch (users.Count)
			{
				case 0:
					// Auto-create new user
					DataAccessServer.NewPersistentConnection();
					DataAccessServer.PersistentConnection.BeginTransaction();

					user.Name = user.Code;
					user.Password = "";
					user.IsActive = true;

					Int32 id = user.AcceptChanges();

					if (id > 0)
					{
						UserDivision userDivision = new UserDivision();

						userDivision.UserId = id;
						userDivision.DivisionId = 1;
						userDivision.AcceptChanges();
					}

					DataAccessServer.PersistentConnection.CommitTransaction();
					DataAccessServer.TerminatePersistentConnection();

					this.User = user;
					break;

				case 1:
					// Assign user details
					this.User = users[0];

					if (this.User.IsActive)
					{
						if (this.User.UserDivisions.Count == 0)
						{
							errorMessage = "User (" + this.User.Code + ") " + this.User.Name + " has no divisions configured";
						}
					}
					else
					{
						errorMessage = "User (" + this.User.Code + ") " + this.User.Name + " is inactive";
					}

					break;

				default:
					errorMessage = "PANIC: Multiple " + user.Code + " users found";
					break;
			}

			if (errorMessage.Length > 0)
			{
				ArgumentException ex = new ArgumentException(errorMessage);
				throw ex;
			}
		}

		/// <summary>
		/// Sets up the service Division details from the user configuration
		/// </summary>
		private void SetupDivision()
		{
			string errorMessage = "";

			// Read divisions for this user
			ExtendedBindingList<UserDivision> userDivisions = this.User.UserDivisions;

			if (userDivisions == null)
			{
				errorMessage = "Failed to read user division details";
			}
			else
			{
				if (userDivisions.Count == 0)
				{
					UserDivision userDivision = new UserDivision();

					userDivision.DivisionId = 1;
					userDivision.UserId = this.User.Id;
					userDivision.AcceptChanges();

					SetupDivision();
				}
			}

			if (!String.IsNullOrEmpty(errorMessage))
			{
				ArgumentException ex = new ArgumentException(errorMessage);
				throw ex;
			}
		}

		private void InitialiseTimer(bool performInstantRun)
		{
			// Calculate time to first launch
			DateTime now = DateTime.Now;
			String dateTimeString = now.Year.ToString("0000") + "-" + now.Month.ToString("00") + "-" + now.Day.ToString("00") + " " + Properties.Settings.Default.InitialCalculationTime;
			DateTime launchTime = DateTime.Parse(dateTimeString);

			if (launchTime < now)
			{
				launchTime = launchTime.AddDays(1);
			}

			this.LaunchTime = launchTime;

			// Setup initial timer interval
			Timer timer = new Timer();
		
			timer.AutoReset = false;
			timer.Enabled = true;
			timer.Interval = this.LaunchTime.Subtract(now).TotalMilliseconds;
			timer.Elapsed += new ElapsedEventHandler(Timer_Elapsed);

			// Start timer
			Logger.Instance.Information("Setting initial launch time of " + this.LaunchTimeString);

			// Check if we want to override with an instant launch
			if (performInstantRun)
			{
				Logger.Instance.Information("DEBUG code exists performing instant launch");
				timer.Interval = 1000;
			}

			this.Timer = timer;
			this.Timer.Start();
		}

		private void Timer_Elapsed(object sender, ElapsedEventArgs e)
		{
			Logger.Instance.Information("Launching Accruals Calculation (SVN Revision " + Build.Revision + ")");

			// Stop the timer
			this.Timer.Stop();

			// Process Accruals
			ProcessAccruals();

			// Reset the timer
			while (this.LaunchTime < DateTime.Now)
			{
				this.LaunchTime = this.LaunchTime.AddMinutes(Properties.Settings.Default.CalculationIntervalMinutes);
			}

			Logger.Instance.Information("Setting next launch time of " + this.LaunchTimeString);
			this.Timer.Interval = this.LaunchTime.Subtract(DateTime.Now).TotalMilliseconds;

			// Check if auto finalise option has been applied
			if (_hasAutoFinalised)
			{
				this.Timer.Interval = 10000;
			}

			this.Timer.Start();
		}

		private void ProcessAccruals()
		{
			try
			{
				this.IsCalculating = true;
				AccrualCalculationServiceMode runMode = (AccrualCalculationServiceMode)Properties.Settings.Default.RunMode;
				Logger.Instance.Information("Run Mode is " + runMode.ToString());

				String localMachine = System.Environment.MachineName.ToUpper();
				Logger.Instance.Information("Local Machine Name: " + localMachine);
				Logger.Instance.Information("DAS: " + DataAccessServer.Protocol + "://" + DataAccessServer.Server + ":" + DataAccessServer.Port.ToString());
				Logger.Instance.Information("Revision: " + Build.Revision);

				foreach (UserDivision userDivision in this.User.UserDivisions)
				{
					// Actuals
					if ((runMode & AccrualCalculationServiceMode.Actuals) == AccrualCalculationServiceMode.Actuals)
					{
						Logger.Instance.Information("Starting Actual Accruals Calculation");
						RunAccruals(userDivision, AccrualTypeEnum.Actual);
						Logger.Instance.Information("Completed Actual Accruals Calculation");
					}

					// Forecast
					if ((runMode & AccrualCalculationServiceMode.Forecast) == AccrualCalculationServiceMode.Forecast)
					{
						Logger.Instance.Information("Starting Forecast Accruals Calculation (Division " + userDivision.Id.ToString() + ")");
						RunAccruals(userDivision, AccrualTypeEnum.Forecast);
						Logger.Instance.Information("Completed Forecast Accruals Calculation");
					}
				}
			}
			catch (Exception ex)
			{
				Logger.Instance.Error(ex);
			}
			finally
			{
				this.IsCalculating = false;
			}
		}

		private void RunAccruals(UserDivision userDivision, AccrualTypeEnum accrualTypeEnum)
		{
			Logger.Instance.Information("Process " + accrualTypeEnum.ToString() + " Accruals for Division " + userDivision.DivisionName + " (" + userDivision.DivisionId.ToString() + ")");
			Division division = userDivision.Division;
			bool hasFailed = false;

			if (division.StartCalculation(this.User, accrualTypeEnum))
			{
				String message = "";

				division.ReadDivisionAccrual();
				CalculationPeriod period = division.LastAccrualPeriod().NextPeriod();
				Logger.Instance.Information("Starting calculation run, target period " + period.ToString());

				AccrualCalculation accruals = new AccrualCalculation(this.User, userDivision.Division, period, accrualTypeEnum, true);
				accruals.IsBackgroundMode = true;

				// Check for environment specific settings
				Type type = typeof(Properties.Settings);
				String propertyName = UserAuthentification.Environment + "_MaximumThreads";
				PropertyInfo threadsProperty = type.GetProperty(propertyName);

				if (threadsProperty != null)
				{
					try
					{
						Int32 maximumThreads = (Int32)threadsProperty.GetValue(Properties.Settings.Default, null);
						accruals.MaximumThreads = maximumThreads;
						Logger.Instance.Information("Set maximum threads count to " + maximumThreads.ToString());
					}
					catch (Exception ex)
					{
						Logger.Instance.Information("Failed to read property parse " + propertyName + " (" + ex.ToString() + ")"); 
					}
				}

				try
				{
					if (accruals.Calculate())
					{
						// Debug option to force repeat calcultions to specified period
						//Logger.Instance.Debug("Auto finalise period to keep calculations going");
						//_hasAutoFinalised = accruals.AutoFinalise(2015, 09);
					}
				}
				catch (Exception ex)
				{
					Logger.Instance.Error(ex);
					message = "Overnight accrual calculations failed\r\n\r\n" + ex.ToString();
					hasFailed = true;
				}
				finally
				{
					if (!hasFailed)
					{
						// Report flag amendment failure only
						try
						{
							division.EndCalculation(this.User, accrualTypeEnum);
						}
						catch
						{
							message = "Overnight accrual calculations where successful, however, the service was unable to amend the calculation flag successfully to close the calculation run";
						}
					}

					// Now send the mail
					if (!string.IsNullOrEmpty(message))
						SendNotificationEmail(message);

					Logger.Instance.Information("Completed");
				}
			}
			else
			{
				ApplicationException ex = new ApplicationException("Cannot calculate " + accrualTypeEnum.ToString() + " accruals for Division " + userDivision.DivisionName + " (" + userDivision.DivisionId + "), accruals still marked as running");
				Logger.Instance.Error(ex);
			}
		}

		#region SendEmail

		private void SendNotificationEmail(string message)
		{
			try
			{
				string smtpServer = String.IsNullOrEmpty(Properties.Settings.Default.SmtpServer) ? "mail-relay.ltds.lafargetarmac.com" : Properties.Settings.Default.SmtpServer;
				int smtpPort = (Properties.Settings.Default.SmtpPort <= 0 ? 25 : Properties.Settings.Default.SmtpPort);
				string mailFrom = (String.IsNullOrEmpty(Properties.Settings.Default.MailFrom) ? "rebates-donotreply@lafargetarmac.com" : Properties.Settings.Default.MailFrom);
				string[] mailTo = (Properties.Settings.Default.MailTo ?? "").Split(';');
				SmtpClient smtpClient = new SmtpClient(smtpServer, smtpPort);

				foreach (string sendMailTo in mailTo)
				{
					Logger.Instance.Information("Sending notification email to " + sendMailTo.Trim());
					MailMessage mailMessage = new MailMessage(mailFrom, sendMailTo.Trim());

					mailMessage.Subject = "REBATES: Overnight Calculation Service (" + UserAuthentification.Environment + ")";
					mailMessage.Body = message;
					mailMessage.IsBodyHtml = true;

					smtpClient.Send(mailMessage);
				}
			}
			catch
			{
			}
		}

		#endregion
	}
}

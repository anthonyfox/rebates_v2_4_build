﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	[Flags]
	public enum AccrualCalculationServiceMode
	{
		None = 0x0,
		Actuals = 0x1,
		Forecast = 0x2
	}
}

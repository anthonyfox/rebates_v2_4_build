﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	[RunInstaller(true)]
	public class AccrualCalculationServiceInstaller : Installer
	{
		private String _serviceName = "Lafarge Cement Rebates Accrual Calculation Service";

		/// <summary>
		/// Public Constructor for WindowsServiceInstaller.
		/// - Put all of your Initialization code here.
		/// </summary>
		public AccrualCalculationServiceInstaller()
		{
			ServiceProcessInstaller serviceProcessInstaller = new ServiceProcessInstaller();
			ServiceInstaller serviceInstaller = new ServiceInstaller();

			// Service Account Information
			serviceProcessInstaller.Account = ServiceAccount.LocalSystem;
			serviceProcessInstaller.Username = null;
			serviceProcessInstaller.Password = null;

			// Service Information
			serviceInstaller.DisplayName = _serviceName;
			serviceInstaller.StartType = ServiceStartMode.Automatic;

			// This must be identical to the WindowsService.ServiceBase name
			// set in the constructor of WindowsService.cs
			serviceInstaller.ServiceName = _serviceName;

			this.Installers.Add(serviceProcessInstaller);
			this.Installers.Add(serviceInstaller);
		}

		public override void Install(IDictionary stateServer)
		{
			Microsoft.Win32.RegistryKey system;
			Microsoft.Win32.RegistryKey currentControlSet;
			Microsoft.Win32.RegistryKey services;
			Microsoft.Win32.RegistryKey service;
			Microsoft.Win32.RegistryKey config;

			try
			{
				// Let the project installer do its job
				base.Install(stateServer);

				// Open the HKEY_LOCAL_MACHINE\SYSTEM key
				system = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("System");
				// Open CurrentControlSet
				currentControlSet = system.OpenSubKey("CurrentControlSet");
				// Go to the services key
				services = currentControlSet.OpenSubKey("Services");
				// Open the key for your service, and allow writing
				service = services.OpenSubKey(_serviceName, true);
				// Add your service's description as a REG_SZ value named "Description"
				service.SetValue("Description", "Generates Rebate accrual calculations, both Forecast and Actual");
				// (Optional) Add some custom information your service will use...
				config = service.CreateSubKey("Parameters");

				// Now start the windows service
				ServiceController serviceController = new ServiceController();

				serviceController.ServiceName = _serviceName;
				serviceController.Start();
			}
			catch (Exception e)
			{
				Console.WriteLine("An exception was thrown during service installation:\n" + e.ToString());
			}
		}

		public override void Uninstall(IDictionary stateServer)
		{
			Microsoft.Win32.RegistryKey system;
			Microsoft.Win32.RegistryKey currentControlSet;
			Microsoft.Win32.RegistryKey services;
			Microsoft.Win32.RegistryKey service;

			try
			{
				// Stop the windows service
				ServiceController serviceController = new ServiceController();

				serviceController.ServiceName = _serviceName;
				serviceController.Stop();
	
				// Drill down to the service key and open it with write permission
				system = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("System");
				currentControlSet = system.OpenSubKey("CurrentControlSet");
				services = currentControlSet.OpenSubKey("Services");
				service = services.OpenSubKey(_serviceName, true);

				// Delete any keys you created during installation (or that your service created)
				service.DeleteSubKeyTree("Parameters");
			}
			catch (Exception ex)
			{
				Console.WriteLine("Exception encountered while uninstalling service:\n" + ex.ToString());
			}
			finally
			{
				//Let the project installer do its job
				base.Uninstall(stateServer);
			}
		}
	}
}

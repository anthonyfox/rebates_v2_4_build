﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace Evolve.Clients.Lafarge.Cement.Rebates
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		static void Main()
		{
			ServiceBase[] ServicesToRun;
			ServicesToRun = new ServiceBase[] 
			{ 
				new AccrualCalculationService()
			};
			ServiceBase.Run(ServicesToRun);
		}
	}
}

﻿alter table site alter column ACTIVE bit not null

alter table customer alter column ACTIVE bit not null

alter table division alter column ALLOWGLOBALPRODUCT bit not null
alter table division alter column ALLOWPAYMENTAUTHORISATION bit not null

alter table divisionaccrual alter column YEAR smallint not null
alter table divisionaccrual alter column PERIOD smallint not null

alter table accrual alter column CREATEYEAR smallint not null
alter table accrual alter column CREATEPERIOD smallint not null
alter table accrual alter column ACTUALYEAR smallint not null
alter table accrual alter column ACTUALPERIOD smallint not null

alter table attribute alter column LEVELNUMBER smallint not null
alter table hierarchy alter column LEVELNUMBER smallint not null

alter table invoiceline alter column PackingTypeEnum smallint not null

alter table payment alter column CREATEYEAR smallint not null
alter table payment alter column CREATEPERIOD smallint not null
alter table payment alter column ACTUALYEAR smallint not null
alter table payment alter column ACTUALPERIOD smallint not null

alter table payment_paymentbreakdown alter column PaymentStatusEnum smallint not null
alter table paymentfrequencyreminder alter column period smallint not null

alter table product alter column ACTIVE bit not null

alter table rebate alter column REBATEVERSION smallint not null
alter table rebate alter column ISADJUSTMENT bit not null
alter table rebate alter column ISJIF bit not null
alter table rebate alter column ISGLOBALPRODUCT bit not null
﻿-- Create Primary Keys
ALTER TABLE ACCRUAL ADD CONSTRAINT PK_ACCRUAL PRIMARY KEY (Id)
ALTER TABLE ACCRUALDETAIL ADD CONSTRAINT PK_ACCRUALDETAIL PRIMARY KEY (Id)
ALTER TABLE ACCRUALINVOICE ADD CONSTRAINT PK_ACCRUALINVOICE PRIMARY KEY (Id)
ALTER TABLE ACCRUALJDE ADD CONSTRAINT PK_ACCRUALJDE PRIMARY KEY (Id)
ALTER TABLE ADDRESS ADD CONSTRAINT PK_ADDRESS PRIMARY KEY (Id)
ALTER TABLE ATTRIBUTE ADD CONSTRAINT PK_ATTRIBUTE PRIMARY KEY (Id)
ALTER TABLE CUSTOMER ADD CONSTRAINT PK_CUSTOMER PRIMARY KEY (Id)
ALTER TABLE CUSTOMER_COMP ADD CONSTRAINT PK_CUSTOMER_COMP PRIMARY KEY (Id)
ALTER TABLE CUSTOMERGROUP ADD CONSTRAINT PK_CUSTOMERGROUP PRIMARY KEY (Id)
ALTER TABLE DEL_TMP ADD CONSTRAINT PK_DEL_TMP PRIMARY KEY (Id)
ALTER TABLE DELIVERYWINDOW ADD CONSTRAINT PK_DELIVERYWINDOW PRIMARY KEY (Id)
ALTER TABLE DIVISION ADD CONSTRAINT PK_DIVISION PRIMARY KEY (Id)
ALTER TABLE DIVISIONACCRUAL ADD CONSTRAINT PK_DIVISIONACCRUAL PRIMARY KEY (Id)
ALTER TABLE DM_REBATEINPUTDATE_2015 ADD CONSTRAINT PK_DM_REBATEINPUTDATE_2015 PRIMARY KEY (Id)
ALTER TABLE EXCEPTIONTYPE ADD CONSTRAINT PK_EXCEPTIONTYPE PRIMARY KEY (Id)
ALTER TABLE FORECAST_JDE_PRICING ADD CONSTRAINT PK_FORECAST_JDE_PRICING PRIMARY KEY (Id)
ALTER TABLE FORECAST_REBATES ADD CONSTRAINT PK_FORECAST_REBATES PRIMARY KEY (Id)
ALTER TABLE FORECAST_SCENARIO_LOCK ADD CONSTRAINT PK_FORECAST_SCENARIO_LOCK PRIMARY KEY (Id)
ALTER TABLE FORECAST_SCENARIO_MAPPING ADD CONSTRAINT PK_FORECAST_SCENARIO_MAPPING PRIMARY KEY (Id)
ALTER TABLE FORECAST_TRANSACTIONS ADD CONSTRAINT PK_FORECAST_TRANSACTIONS PRIMARY KEY (Id)
ALTER TABLE HIERARCHY ADD CONSTRAINT PK_HIERARCHY PRIMARY KEY (Id)
ALTER TABLE INVOICELINE ADD CONSTRAINT PK_INVOICELINE PRIMARY KEY (Id)
ALTER TABLE INVOICELINE2 ADD CONSTRAINT PK_INVOICELINE2 PRIMARY KEY (Id)
ALTER TABLE LEADTIME ADD CONSTRAINT PK_LEADTIME PRIMARY KEY (Id)
ALTER TABLE PAYEE ADD CONSTRAINT PK_PAYEE PRIMARY KEY (Id)
ALTER TABLE PAYMENT ADD CONSTRAINT PK_PAYMENT PRIMARY KEY (Id)
ALTER TABLE PAYMENT_PAYMENTBREAKDOWN ADD CONSTRAINT PK_PAYMENT_PAYMENTBREAKDOWN PRIMARY KEY (Id)
ALTER TABLE PAYMENTBREAKDOWN ADD CONSTRAINT PK_PAYMENTBREAKDOWN PRIMARY KEY (Id)
ALTER TABLE PAYMENTFREQUENCY ADD CONSTRAINT PK_PAYMENTFREQUENCY PRIMARY KEY (Id)
ALTER TABLE PAYMENTFREQUENCYREMINDER ADD CONSTRAINT PK_PAYMENTFREQUENCYREMINDER PRIMARY KEY (Id)
ALTER TABLE PRICINGSUBGROUP ADD CONSTRAINT PK_PRICINGSUBGROUP PRIMARY KEY (Id)
ALTER TABLE PRODUCT ADD CONSTRAINT PK_PRODUCT PRIMARY KEY (Id)
ALTER TABLE PRODUCT_COMP ADD CONSTRAINT PK_PRODUCT_COMP PRIMARY KEY (Id)
ALTER TABLE REBATE ADD CONSTRAINT PK_REBATE PRIMARY KEY (Id)
ALTER TABLE REBATEDELIVERYWINDOW ADD CONSTRAINT PK_REBATEDELIVERYWINDOW PRIMARY KEY (Id)
ALTER TABLE REBATEDETAIL ADD CONSTRAINT PK_REBATEDETAIL PRIMARY KEY (Id)
ALTER TABLE REBATEEXCEPTION ADD CONSTRAINT PK_REBATEEXCEPTION PRIMARY KEY (Id)
ALTER TABLE REBATELEADTIME ADD CONSTRAINT PK_REBATELEADTIME PRIMARY KEY (Id)
ALTER TABLE REBATENOTE ADD CONSTRAINT PK_REBATENOTE PRIMARY KEY (Id)
ALTER TABLE REBATERANGE ADD CONSTRAINT PK_REBATERANGE PRIMARY KEY (Id)
ALTER TABLE REBATETYPE ADD CONSTRAINT PK_REBATETYPE PRIMARY KEY (Id)
ALTER TABLE REBATEVAP ADD CONSTRAINT PK_REBATEVAP PRIMARY KEY (Id)
ALTER TABLE SITE ADD CONSTRAINT PK_SITE PRIMARY KEY (Id)
ALTER TABLE SITE_COMP ADD CONSTRAINT PK_SITE_COMP PRIMARY KEY (Id)
ALTER TABLE STDCST_REBATE_COST ADD CONSTRAINT PK_STDCST_REBATE_COST PRIMARY KEY (Id)
ALTER TABLE TAXCODE ADD CONSTRAINT PK_TAXCODE PRIMARY KEY (Id)
ALTER TABLE TAXRATE ADD CONSTRAINT PK_TAXRATE PRIMARY KEY (Id)
ALTER TABLE UNIQUE_COMBINATIONS ADD CONSTRAINT PK_UNIQUE_COMBINATIONS PRIMARY KEY (Id)
ALTER TABLE USERDIVISION ADD CONSTRAINT PK_USERDIVISION PRIMARY KEY (Id)
ALTER TABLE USERS ADD CONSTRAINT PK_USERS PRIMARY KEY (Id)


-- DROP unwanted tables
DROP TABLE COMBO
DROP TABLE CUSTOMER_COMP
DROP TABLE DEL_TMP
DROP TABLE DM_REBATEINPUTDATE_2015
DROP TABLE FORECAST_JDE_PRICING
DROP TABLE FORECAST_REBATES
DROP TABLE FORECAST_SCENARIO_LOCK
DROP TABLE FORECAST_SCENARIO_MAPPING
DROP TABLE FORECAST_TRANSACTIONS
DROP TABLE HISTORIC_DM_REBATE_INPUT
DROP TABLE INVOICELINE2
DROP TABLE PRODUCT_COMP
DROP TABLE SITE_COMP
DROP TABLE STDCST_REBATE_COST
DROP TABLE TEMP_IJB
DROP TABLE TEMP_RSJ
DROP TABLE UNIQUE_COMBINATIONS


-- Accrual
ALTER TABLE Accrual
ADD CONSTRAINT FK_Accrual_Rebate FOREIGN KEY (RebateId) REFERENCES Rebate (Id)

ALTER TABLE Accrual
ADD CONSTRAINT FK_Accrual_DivisionAccrual FOREIGN KEY (DivisionAccrualId) REFERENCES DivisionAccrual (Id)

-- Accrual Detail
ALTER TABLE AccrualDetail 
ADD CONSTRAINT FK_AccrualDetail_Accrual FOREIGN KEY (AccrualId) REFERENCES Accrual (Id)

ALTER TABLE AccrualDetail 
ADD CONSTRAINT FK_AccrualDetail_RebateDetail FOREIGN KEY (RebateDetailId) REFERENCES RebateDetail (Id)

ALTER TABLE AccrualDetail 
ADD CONSTRAINT FK_AccrualDetail_DeliveryWindow FOREIGN KEY (RebateDeliveryWindowId) REFERENCES DeliveryWindow (Id)

ALTER TABLE AccrualDetail 
ADD CONSTRAINT FK_AccrualDetail_LeadTime FOREIGN KEY (RebateLeadTimeId) REFERENCES LeadTime (Id)

ALTER TABLE AccrualDetail
ADD CONSTRAINT FK_AccrualDetail_Hierarchy FOREIGN KEY (HierarchyId) REFERENCES Hierarchy (Id)

ALTER TABLE AccrualDetail
ADD CONSTRAINT FK_AccrualDetail_ParentHierarchy FOREIGN KEY (ParentHierarchyId) REFERENCES Hierarchy (Id)

ALTER TABLE AccrualDetail
ADD CONSTRAINT FK_AccrualDetail_CustomerGroup FOREIGN KEY (CustomerGroupId) REFERENCES CustomerGroup (Id)

ALTER TABLE AccrualDetail
ADD CONSTRAINT FK_AccrualDetail_Customer FOREIGN KEY (CustomerId) REFERENCES Customer (Id)

ALTER TABLE AccrualDetail
ADD CONSTRAINT FK_AccrualDetail_ParentCustomerGroup FOREIGN KEY (CustomerGroupId) REFERENCES CustomerGroup (Id)

ALTER TABLE AccrualDetail
ADD CONSTRAINT FK_AccrualDetail_ParentCustomer FOREIGN KEY (ParentCustomerId) REFERENCES Customer (Id)

-- Accrual Invoice
ALTER TABLE AccrualInvoice
ADD CONSTRAINT FK_AccrualInvoice_Accrual FOREIGN KEY (AccrualId) REFERENCES Accrual (Id)

ALTER TABLE AccrualInvoice
ADD CONSTRAINT FK_AccrualInvoice_InvoiceLine FOREIGN KEY (InvoiceId) REFERENCES InvoiceLine (Id)

-- AccrualJde
ALTER TABLE AccrualJde
ADD CONSTRAINT FK_AccrualJde_DivisionAccrual FOREIGN KEY (DivisionAccrualId) REFERENCES DivisionAccrual (Id)

ALTER TABLE AccrualJde
ADD CONSTRAINT FK_AccrualJde_Product FOREIGN KEY (ProductId) REFERENCES Product (Id)

ALTER TABLE AccrualJde
ADD CONSTRAINT FK_AccrualJde_Site FOREIGN KEY (SiteId) REFERENCES Site (Id)

ALTER TABLE AccrualJde
ADD CONSTRAINT FK_AccrualJde_SoldTo FOREIGN KEY (SoldToId) REFERENCES Customer (Id)

-- Attribute
ALTER TABLE Attribute
ADD CONSTRAINT FK_Attribute_Division FOREIGN KEY (DivisionId) REFERENCES Division (Id)

-- Customer
ALTER TABLE Customer
ADD CONSTRAINT FK_Customer_Address FOREIGN KEY (AddressId) REFERENCES Address (Id)

ALTER TABLE Customer
ADD CONSTRAINT FK_Customer_CustomerGroup FOREIGN KEY (CustomerGroupId) REFERENCES CustomerGroup (Id)

ALTER TABLE Customer
ADD CONSTRAINT FK_Customer_PricingSubGroup FOREIGN KEY (PricingSubGroupId) REFERENCES PricingSubGroup (Id)

ALTER TABLE Customer
ADD CONSTRAINT FK_Customer_SoldTo FOREIGN KEY (SoldToId) REFERENCES Customer (Id)

-- Customer Group
ALTER TABLE CustomerGroup
ADD CONSTRAINT FK_CustomerGroup_Division FOREIGN KEY (DivisionId) REFERENCES Division (Id)

-- Delivery Window
ALTER TABLE DeliveryWindow
ADD CONSTRAINT FK_DeliveryWindow_Division FOREIGN KEY (DivisionId) REFERENCES Division (Id)

ALTER TABLE DeliveryWindow
ADD CONSTRAINT FK_DeliveryWindow_CreateUser FOREIGN KEY (CreateUserId) REFERENCES Users (Id)

ALTER TABLE DeliveryWindow
ADD CONSTRAINT FK_DeliveryWindow_UpdateUser FOREIGN KEY (UpdateUserId) REFERENCES Users (Id)

-- Division
ALTER TABLE Division
ADD CONSTRAINT FK_Division_CalculationUser FOREIGN KEY (CalculationUserId) REFERENCES Users (Id)

ALTER TABLE Division
ADD CONSTRAINT FK_Division_ForecastUser FOREIGN KEY (ForecastUserId) REFERENCES Users (Id)

-- DivisionAccrual
ALTER TABLE DivisionAccrual
ADD CONSTRAINT FK_DivisionAccrual_Division FOREIGN KEY (DivisionId) REFERENCES Division (Id)

-- Hierarchy
ALTER TABLE Hierarchy
ADD CONSTRAINT FK_Hierarchy_Division FOREIGN KEY (DivisionId) REFERENCES Division (Id)

-- InvoiceLine
ALTER TABLE InvoiceLine
ADD CONSTRAINT FK_InvoiceLine_Division FOREIGN KEY (DivisionId) REFERENCES Division (Id)

ALTER TABLE InvoiceLine
ADD CONSTRAINT FK_InvoiceLine_Product FOREIGN KEY (ProductId) REFERENCES Product (Id)

ALTER TABLE InvoiceLine
ADD CONSTRAINT FK_InvoiceLine_Site FOREIGN KEY (SiteId) REFERENCES Site (Id)

ALTER TABLE InvoiceLine
ADD CONSTRAINT FK_InvoiceLine_SoldTo FOREIGN KEY (SoldToId) REFERENCES Customer (Id)

ALTER TABLE InvoiceLine
ADD CONSTRAINT FK_InvoiceLine_ShipTo FOREIGN KEY (ShipToId) REFERENCES Customer (Id)

-- LeadTime
ALTER TABLE LeadTime
ADD CONSTRAINT FK_LeadTime_Division FOREIGN KEY (DivisionId) REFERENCES Division (Id)

ALTER TABLE LeadTime
ADD CONSTRAINT FK_LeadTime_CreateUser FOREIGN KEY (CreateUserId) REFERENCES Users (Id)

ALTER TABLE LeadTime
ADD CONSTRAINT FK_LeadTime_UpdateUser FOREIGN KEY (UpdateUserId) REFERENCES Users (Id)

-- Payee
ALTER TABLE Payee
ADD CONSTRAINT FK_Payee_Division FOREIGN KEY (DivisionId) REFERENCES Division (Id)

ALTER TABLE Payee
ADD CONSTRAINT FK_Payee_Address FOREIGN KEY (AddressId) REFERENCES Address (Id)

-- Payment
ALTER TABLE Payment
ADD CONSTRAINT FK_Payment_Payment_PaymentBreakdown FOREIGN KEY (Payment_PaymentBreakdownId) REFERENCES Payment_PaymentBreakdown (Id)

ALTER TABLE Payment
ADD CONSTRAINT FK_Payment_Rebate FOREIGN KEY (RebateId) REFERENCES Rebate (Id)

-- Payment_PaymentBreakdown
ALTER TABLE Payment_PaymentBreakdown
ADD CONSTRAINT FK_Payment_PaymentBreakdown_Division FOREIGN KEY (DivisionId) REFERENCES Division (Id)

ALTER TABLE Payment_PaymentBreakdown
ADD CONSTRAINT FK_Payment_PaymentBreakdown_CreateUser FOREIGN KEY (CreateUserId) REFERENCES Users (Id)

ALTER TABLE Payment_PaymentBreakdown
ADD CONSTRAINT FK_Payment_PaymentBreakdown_ActionUser FOREIGN KEY (ActionUserId) REFERENCES Users (Id)

-- PaymentBreakdown
ALTER TABLE PaymentBreakdown
ADD CONSTRAINT FK_PaymentBreakdown_Payment_PaymentBreakdown FOREIGN KEY (Payment_PaymentBreakdownId) REFERENCES Payment_PaymentBreakdown (Id)

ALTER TABLE PaymentBreakdown
ADD CONSTRAINT FK_PaymentBreakdown_Payee FOREIGN KEY (PayeeId) REFERENCES Payee (Id)

ALTER TABLE PaymentBreakdown
ADD CONSTRAINT FK_PaymentBreakdown_TaxCode FOREIGN KEY (TaxCodeId) REFERENCES TaxCode (Id)

-- PaymentFrequency
ALTER TABLE PaymentFrequency
ADD CONSTRAINT FK_PaymentFrequency_Division FOREIGN KEY (DivisionId) REFERENCES Division (Id)

-- PaymentFrequencyReminder
ALTER TABLE PaymentFrequencyReminder
ADD CONSTRAINT FK_PaymentFrequencyReminder_PaymentFrequency FOREIGN KEY (PaymentFrequencyId) REFERENCES PaymentFrequency (Id)

-- PricingSubGroup
ALTER TABLE PricingSubGroup
ADD CONSTRAINT FK_PricingSubGroup_Division FOREIGN KEY (DivisionId) REFERENCES Division (Id)

ALTER TABLE PricingSubGroup
ADD CONSTRAINT FK_PricingSubGroup_CreateUser FOREIGN KEY (CreateUserId) REFERENCES Users (Id)

ALTER TABLE PricingSubGroup
ADD CONSTRAINT FK_PricingSubGroup_UpdateUser FOREIGN KEY (UpdateUserId) REFERENCES Users (Id)

-- Product
ALTER TABLE Product
ADD CONSTRAINT FK_Product_Division FOREIGN KEY (DivisionId) REFERENCES Division (Id)

ALTER TABLE Product
ADD CONSTRAINT FK_Product_Attribute1 FOREIGN KEY (AttributeId1) REFERENCES Attribute (Id)

ALTER TABLE Product
ADD CONSTRAINT FK_Product_Attribute2 FOREIGN KEY (AttributeId2) REFERENCES Attribute (Id)

ALTER TABLE Product
ADD CONSTRAINT FK_Product_Attribute3 FOREIGN KEY (AttributeId3) REFERENCES Attribute (Id)

ALTER TABLE Product
ADD CONSTRAINT FK_Product_Attribute4 FOREIGN KEY (AttributeId4) REFERENCES Attribute (Id)

-- Rebate
ALTER TABLE Rebate
ADD CONSTRAINT FK_Rebate_Division FOREIGN KEY (DivisionId) REFERENCES Division (Id)

ALTER TABLE Rebate
ADD CONSTRAINT FK_Rebate_Hierarchy FOREIGN KEY (HierarchyId) REFERENCES Hierarchy (Id)

ALTER TABLE Rebate
ADD CONSTRAINT FK_Rebate_PricingSubGroup FOREIGN KEY (PricingSubGroupId) REFERENCES PricingSubGroup (Id)

ALTER TABLE Rebate
ADD CONSTRAINT FK_Rebate_PaymentFrequency FOREIGN KEY (PaymentFrequencyId) REFERENCES PaymentFrequency (Id)

ALTER TABLE Rebate
ADD CONSTRAINT FK_Rebate_RebateType FOREIGN KEY (RebateTypeId) REFERENCES RebateType (Id)

ALTER TABLE Rebate
ADD CONSTRAINT FK_Rebate_CreateUser FOREIGN KEY (CreateUserId) REFERENCES Users (Id)

ALTER TABLE Rebate
ADD CONSTRAINT FK_Rebate_UpdateUser FOREIGN KEY (UpdateUserId) REFERENCES Users (Id)

ALTER TABLE Rebate
ADD CONSTRAINT FK_Rebate_CustomerGroup FOREIGN KEY (CustomerGroupId) REFERENCES CustomerGroup (Id)

ALTER TABLE Rebate
ADD CONSTRAINT FK_Rebate_Customer FOREIGN KEY (CustomerId) REFERENCES Customer (Id)

-- RebateDeliveryWindow
ALTER TABLE RebateDeliveryWindow
ADD CONSTRAINT FK_RebateDeliveryWindow_Rebate FOREIGN KEY (RebateId) REFERENCES Rebate (Id)

ALTER TABLE RebateDeliveryWindow
ADD CONSTRAINT FK_RebateDeliveryWindow_DeliveryWindow FOREIGN KEY (DeliveryWindowId) REFERENCES DeliveryWindow (Id)

-- RebateDetail
ALTER TABLE RebateDetail
ADD CONSTRAINT FK_RebateDetail_Rebate FOREIGN KEY (RebateId) REFERENCES Rebate (Id)

ALTER TABLE RebateDetail
ADD CONSTRAINT FK_RebateDetail_Attribute1 FOREIGN KEY (AttributeId1) REFERENCES Attribute (Id)

ALTER TABLE RebateDetail
ADD CONSTRAINT FK_RebateDetail_Attribute2 FOREIGN KEY (AttributeId2) REFERENCES Attribute (Id)

ALTER TABLE RebateDetail
ADD CONSTRAINT FK_RebateDetail_Attribute3 FOREIGN KEY (AttributeId3) REFERENCES Attribute (Id)

ALTER TABLE RebateDetail
ADD CONSTRAINT FK_RebateDetail_Attribute4 FOREIGN KEY (AttributeId4) REFERENCES Attribute (Id)

-- RebateException
ALTER TABLE RebateException
ADD CONSTRAINT FK_RebateException_Rebate FOREIGN KEY (RebateId) REFERENCES Rebate (Id)

ALTER TABLE RebateException
ADD CONSTRAINT FK_RebateException_Site FOREIGN KEY (SiteId) REFERENCES Site (Id)

ALTER TABLE RebateException
ADD CONSTRAINT FK_RebateException_Customer FOREIGN KEY (CustomerId) REFERENCES Customer (Id)

-- RebateLeadTime
ALTER TABLE RebateLeadTime
ADD CONSTRAINT FK_RebateLeadTime_Rebate FOREIGN KEY (RebateId) REFERENCES Rebate (Id)

ALTER TABLE RebateLeadTime
ADD CONSTRAINT FK_RebateLeadTime_LeadTime FOREIGN KEY (LeadTimeId) REFERENCES LeadTime (Id)

-- RebateNote
ALTER TABLE RebateNote
ADD CONSTRAINT FK_RebateNote_Rebate FOREIGN KEY (RebateId) REFERENCES Rebate (Id)

ALTER TABLE RebateNote
ADD CONSTRAINT FK_RebateNote_User FOREIGN KEY (UserId) REFERENCES Users (Id)

-- RebateRange
ALTER TABLE RebateRange
ADD CONSTRAINT FK_RebateRange_Rebate FOREIGN KEY (RebateId) REFERENCES Rebate (Id)

-- RebateVap
ALTER TABLE RebateVap
ADD CONSTRAINT FK_RebateVap_Rebate FOREIGN KEY (RebateId) REFERENCES Rebate (Id)

ALTER TABLE RebateVap
ADD CONSTRAINT FK_RebateVap_Product FOREIGN KEY (ProductId) REFERENCES Product (Id)

-- Site
ALTER TABLE Site
ADD CONSTRAINT FK_Site_Division FOREIGN KEY (DivisionId) REFERENCES Division (Id)

ALTER TABLE Site
ADD CONSTRAINT FK_Site_Address FOREIGN KEY (AddressId) REFERENCES Address (Id)

-- TaxRate
ALTER TABLE TaxRate
ADD CONSTRAINT FK_TaxRate_TaxCode FOREIGN KEY (TaxCodeId) REFERENCES TaxCode (Id)

ALTER TABLE TaxRate
ADD CONSTRAINT FK_TaxRate_CreateUser FOREIGN KEY (CreateUserId) REFERENCES Users (Id)

ALTER TABLE TaxRate
ADD CONSTRAINT FK_TaxRate_UpdateUser FOREIGN KEY (UpdateUserId) REFERENCES Users (Id)

-- UserDivision
ALTER TABLE UserDivision
ADD CONSTRAINT FK_UserDivision_User FOREIGN KEY (UserId) REFERENCES Users (Id)

ALTER TABLE UserDivision
ADD CONSTRAINT FK_UserDivision_Division FOREIGN KEY (DivisionId) REFERENCES Division (Id)

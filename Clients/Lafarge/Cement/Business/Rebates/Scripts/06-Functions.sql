﻿IF OBJECT_ID (N'dbo.AccrualTotal', N'FN') IS NOT NULL
    DROP FUNCTION AccrualTotal;
GO

CREATE FUNCTION dbo.AccrualTotal( @RebateId INT, @Period INT ) 
RETURNS NUMERIC(18,2)
AS
BEGIN
	DECLARE @AccrualValue AS NUMERIC(18,2) = NULL
  
	SET @AccrualValue = (
		SELECT SUM(Value)
		FROM Accrual
		WHERE RebateId = @RebateId
		AND ActualPeriod <= @Period );
  
	RETURN @AccrualValue;
END;
GO

IF OBJECT_ID (N'dbo.PaymentTotal', N'FN') IS NOT NULL
    DROP FUNCTION PaymentTotal;
GO

CREATE FUNCTION dbo.PaymentTotal( @RebateId INT, @Period INT ) 
RETURNS NUMERIC(18,2)
AS
BEGIN
	DECLARE @PaymentValue AS NUMERIC(18,2) = NULL
  
	SET @PaymentValue = (
		SELECT SUM(Value)
		FROM Payment
  
		INNER JOIN Payment_PaymentBreakdown
		ON Payment_PaymentBreakdown.Id = Payment.Payment_PaymentBreakdownId
		AND Payment_PaymentBreakdown.PaymentStatusEnum IN ( 1, 2 )
  
		WHERE Payment.RebateId = @RebateId
		AND Payment.ActualPeriod <= @Period
		AND Payment.PaymentTypeEnum = 1 );  

	RETURN @PaymentValue;
END;
GO

IF OBJECT_ID (N'dbo.ClosedTotal', N'FN') IS NOT NULL
    DROP FUNCTION ClosedTotal;
GO

CREATE FUNCTION dbo.ClosedTotal( @RebateId INT, @Period INT ) 
RETURNS NUMERIC(18,2)
AS
BEGIN
	DECLARE @PaymentValue AS NUMERIC(18,2) = NULL
  
	SET @PaymentValue = (
		SELECT SUM(Value)
		FROM Payment
  
		INNER JOIN Payment_PaymentBreakdown
		ON Payment_PaymentBreakdown.Id = Payment.Payment_PaymentBreakdownId
		AND Payment_PaymentBreakdown.PaymentStatusEnum IN ( 1, 2 )
  
		WHERE Payment.RebateId = @RebateId
		AND Payment.ActualPeriod <= @Period
		AND Payment.PaymentTypeEnum = 2 );  

	RETURN @PaymentValue;
END;
GO

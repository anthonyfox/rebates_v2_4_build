SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--========================================================================
--= Procedure: ConvertBoolean
--========================================================================

IF OBJECT_ID('ConvertBoolean', 'P') IS NOT NULL
    DROP PROCEDURE ConvertBoolean;
	GO
-- =============================================
-- Author:		<Evolve>
-- Create date: 18th May 2016
-- Description:	Convert from bit value to 'Yes' or 'No'
-- =============================================
CREATE PROCEDURE ConvertBoolean 
	-- Add the parameters for the stored procedure here
	@bit int,
	@output varchar(MAX) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF @bit = 0
	BEGIN
		SET @output = 'No';
	END
	ELSE
	BEGIN
		SET @output = 'Yes'
	END
END
GO

--========================================================================
--= Procedure: ConvertDate
--========================================================================

IF OBJECT_ID('ConvertDate', 'P') IS NOT NULL
    DROP PROCEDURE ConvertDate;
	GO
-- =============================================
-- Author:		<Evolve>
-- Create date: 18th May 2016
-- Description:	Convert from datetime value to YYYY/MM/DD
-- =============================================
CREATE PROCEDURE ConvertDate
	-- Add the parameters for the stored procedure here
	@date datetime,
	@output varchar(MAX) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF @date IS NULL
		SET @output = 'NULL'
	ELSE
		SET @output = CONVERT(varchar(MAX), @date, 111)
END
GO

--========================================================================
--= Procedure: ConvertDivisionId
--========================================================================

IF OBJECT_ID('ConvertDivisionId', 'P') IS NOT NULL
    DROP PROCEDURE ConvertDivisionId
	GO
-- =============================================
-- Author:		<Evolve>
-- Create date: 18th May 2016
-- Description:	Convert from DivisionId to DivisionName
-- =============================================
CREATE PROCEDURE ConvertDivisionId
	-- Add the parameters for the stored procedure here
	@divisionId INT,
	@divisionName VARCHAR(MAX) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON

    -- Insert statements for procedure here
	IF(@divisionId IS NULL)
	BEGIN
		SET @divisionName = 'NULL'
	END
	ELSE
	BEGIN
		SET @divisionName = 'Unknown ' + CAST(@divisionId AS VARCHAR(16))

		SELECT @divisionName = DIVISION.NAME
		FROM DIVISION
		WHERE DIVISION.ID = @divisionId
	END
END
GO

--========================================================================
--= Procedure: ConvertHierarchyId
--========================================================================

IF OBJECT_ID('ConvertHierarchyId', 'P') IS NOT NULL
    DROP PROCEDURE ConvertHierarchyId
	GO
-- =============================================
-- Author:		<Evolve>
-- Create date: 18th May 2016
-- Description:	Convert from HierarchyId to HierarchyName
-- =============================================
CREATE PROCEDURE ConvertHierarchyId
	-- Add the parameters for the stored procedure here
	@hierarchyId INT,
	@hierarchyName VARCHAR(MAX) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON

    -- Insert statements for procedure here
	IF(@hierarchyId IS NULL)
	BEGIN
		SET @hierarchyName = 'NULL'
	END
	ELSE
	BEGIN
		SET @hierarchyName = 'Unknown ' + CAST(@hierarchyId AS VARCHAR(16))

		SELECT @hierarchyName = HIERARCHY.NAME
		FROM HIERARCHY
		WHERE HIERARCHY.ID = @hierarchyId
	END
END
GO

--========================================================================
--= Procedure: ConvertCustomerGroupId
--========================================================================

IF OBJECT_ID('ConvertCustomerGroupId', 'P') IS NOT NULL
    DROP PROCEDURE ConvertCustomerGroupId
	GO
-- =============================================
-- Author:		<Evolve>
-- Create date: 18th May 2016
-- Description:	Convert from CustomerGroupId to CustomerGroupName
-- =============================================
CREATE PROCEDURE ConvertCustomerGroupId
	-- Add the parameters for the stored procedure here
	@customerGroupId INT,
	@customerGroupName VARCHAR(MAX) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF(@customerGroupId IS NULL)
	BEGIN
		SET @customerGroupName = 'NULL'
	END
	ELSE
	BEGIN
		SET @customerGroupName = 'Unknown ' + CAST(@customerGroupId AS VARCHAR(16))

		SELECT @customerGroupName = CUSTOMERGROUP.NAME
		FROM CUSTOMERGROUP
		WHERE CUSTOMERGROUP.ID = @customerGroupId
	END
END
GO

--========================================================================
--= Procedure: ConvertCustomerId
--========================================================================

IF OBJECT_ID('ConvertCustomerId', 'P') IS NOT NULL
    DROP PROCEDURE ConvertCustomerId
GO
-- =============================================
-- Author:		<Evolve>
-- Create date: 18th May 2016
-- Description:	Convert from CustomerId to CustomerName
-- =============================================
CREATE PROCEDURE ConvertCustomerId
	-- Add the parameters for the stored procedure here
	@customerId INT,
	@customerName VARCHAR(MAX) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF(@customerId IS NULL)
	BEGIN
		SET @customerName = 'NULL'
	END
	ELSE
	BEGIN
		SET @customerName = 'Unknown ' + CAST(@customerId AS VARCHAR(16))

		SELECT @customerName = CUSTOMER.NAME
		FROM CUSTOMER
		WHERE CUSTOMER.ID = @customerId
	END
END
GO

--========================================================================
--= Procedure: ConvertPayeeId
--========================================================================

IF OBJECT_ID('ConvertPayeeId', 'P') IS NOT NULL
    DROP PROCEDURE ConvertPayeeId
	GO
-- =============================================
-- Author:		<Evolve>
-- Create date: 18th May 2016
-- Description:	Convert from PayeeId to PayeeName
-- =============================================
CREATE PROCEDURE ConvertPayeeId
	-- Add the parameters for the stored procedure here
	@payeeId INT,
	@payeeName VARCHAR(MAX) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF(@payeeId IS NULL)
	BEGIN
		SET @payeeName = 'NULL'
	END
	ELSE
	BEGIN
		SET @payeeName = 'Unknown ' + CAST(@payeeId AS VARCHAR(16))

		SELECT @payeeName = PAYEE.NAME
		FROM PAYEE
		WHERE PAYEE.ID = @payeeId
	END
END
GO

--========================================================================
--= Procedure: ConvertPaymentFrequencyId
--========================================================================

IF OBJECT_ID('ConvertPaymentFrequencyId', 'P') IS NOT NULL
    DROP PROCEDURE ConvertPaymentFrequencyId
	GO
-- =============================================
-- Author:		<Evolve>
-- Create date: 18th May 2016
-- Description:	Convert from PaymentFreqencyId to PaymentFrequencyName
-- =============================================
CREATE PROCEDURE ConvertPaymentFrequencyId
	-- Add the parameters for the stored procedure here
	@paymentFrequencyId INT,
	@paymentFrequencyName VARCHAR(MAX) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF(@paymentFrequencyId IS NULL)
	BEGIN
		SET @paymentFrequencyName = 'NULL'
	END
	ELSE
	BEGIN
		SET @paymentFrequencyName = 'Unknown ' + CAST(@paymentFrequencyId AS VARCHAR(16))

		SELECT @paymentFrequencyName = PAYMENTFREQUENCY.NAME
		FROM PAYMENTFREQUENCY
		WHERE PAYMENTFREQUENCY.ID = @paymentFrequencyId
	END
END
GO

--========================================================================
--= Procedure: ConvertDeliveryWindowId
--========================================================================

IF OBJECT_ID('ConvertDeliveryWindowId', 'P') IS NOT NULL
    DROP PROCEDURE ConvertDeliveryWindowId
	GO
-- =============================================
-- Author:		<Evolve>
-- Create date: 19th May 2016
-- Description:	Convert from DeliveryWindowId to DeliveryWindowName
-- =============================================
CREATE PROCEDURE ConvertDeliveryWindowId
	-- Add the parameters for the stored procedure here
	@deliveryWindowId INT,
	@deliveryWindowName VARCHAR(MAX) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF(@deliveryWindowId IS NULL)
	BEGIN
		SET @deliveryWindowName = 'NULL'
	END
	ELSE
	BEGIN
		SET @deliveryWindowName = 'Unknown ' + CAST(@deliveryWindowId AS VARCHAR(16))

		SELECT @deliveryWindowName = DELIVERYWINDOW.NAME
		FROM DELIVERYWINDOW
		WHERE DELIVERYWINDOW.ID = @deliveryWindowId
	END
END
GO

--========================================================================
--= Procedure: ConvertAttributeId
--========================================================================

IF OBJECT_ID('ConvertAttributeId', 'P') IS NOT NULL
    DROP PROCEDURE ConvertAttributeId
	GO
-- =============================================
-- Author:		<Evolve>
-- Create date: 18th May 2016
-- Description:	Convert from AttributeId to AttributeName
-- =============================================
CREATE PROCEDURE ConvertAttributeId
	-- Add the parameters for the stored procedure here
	@attributeId INT,
	@attributeName VARCHAR(MAX) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF(@attributeId IS NULL)
	BEGIN
		SET @attributeName = 'NULL'
	END
	ELSE
	BEGIN
		SET @attributeName = 'Unknown ' + CAST(@attributeId AS VARCHAR(16))

		SELECT @attributeName = ATTRIBUTE.NAME
		FROM ATTRIBUTE
		WHERE ATTRIBUTE.ID = @attributeId
	END
END
GO

--========================================================================
--= Procedure: ConvertProductId
--========================================================================

IF OBJECT_ID('ConvertProductId', 'P') IS NOT NULL
    DROP PROCEDURE ConvertProductId
	GO
-- =============================================
-- Author:		<Evolve>
-- Create date: 18th May 2016
-- Description:	Convert from ProductId to ProductName
-- =============================================
CREATE PROCEDURE ConvertProductId
	-- Add the parameters for the stored procedure here
	@productId INT,
	@productName VARCHAR(MAX) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF(@productId IS NULL)
	BEGIN
		SET @productName = 'NULL'
	END
	ELSE
	BEGIN
		SET @productName = 'Unknown ' + CAST(@productId AS VARCHAR(16))

		SELECT @productName = PRODUCT.NAME
		FROM PRODUCT
		WHERE PRODUCT.ID = @productId
	END
END
GO

--========================================================================
--= Procedure: ConvertLeadTimeId
--========================================================================

IF OBJECT_ID('ConvertLeadTimeId', 'P') IS NOT NULL
    DROP PROCEDURE ConvertLeadTimeId
	GO
-- =============================================
-- Author:		<Evolve>
-- Create date: 18th May 2016
-- Description:	Convert from LeadTimeId to LeadTimeName
-- =============================================
CREATE PROCEDURE ConvertLeadTimeId
	-- Add the parameters for the stored procedure here
	@leadTimeId INT,
	@leadTimeName VARCHAR(MAX) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF(@leadTimeId IS NULL)
	BEGIN
		SET @leadTimeName = 'NULL'
	END
	ELSE
	BEGIN
		SET @leadTimeName = 'Unknown ' + CAST(@leadTimeId AS VARCHAR(16))

		SELECT @leadTimeName = LEADTIME.NAME
		FROM LEADTIME
		WHERE LEADTIME.ID = @leadTimeId
	END
END
GO

--========================================================================
--= Procedure: ConvertSiteId
--========================================================================

IF OBJECT_ID('ConvertSiteId', 'P') IS NOT NULL
    DROP PROCEDURE ConvertSiteId
	GO
-- =============================================
-- Author:		<Evolve>
-- Create date: 18th May 2016
-- Description:	Convert from SiteId to SiteName
-- =============================================
CREATE PROCEDURE ConvertSiteId
	-- Add the parameters for the stored procedure here
	@siteId INT,
	@siteName VARCHAR(MAX) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF(@siteId IS NULL)
	BEGIN
		SET @siteName = 'NULL'
	END
	ELSE
	BEGIN
		SET @siteName = 'Unknown ' + CAST(@siteId AS VARCHAR(16))

		SELECT @siteName = SITE.NAME
		FROM SITE
		WHERE SITE.ID = @siteId
	END
END
GO

--========================================================================
--= Procedure: ConvertPricingSubGroupId
--========================================================================

IF OBJECT_ID('ConvertPricingSubGroupId', 'P') IS NOT NULL
    DROP PROCEDURE ConvertPricingSubGroupId
	GO
-- =============================================
-- Author:		<Evolve>
-- Create date: 18th May 2016
-- Description:	Convert from PricingSubGroupId to PricingSubGroupName
-- =============================================
CREATE PROCEDURE ConvertPricingSubGroupId
	-- Add the parameters for the stored procedure here
	@pricingSubGroupId INT,
	@pricingSubGroupName VARCHAR(MAX) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON

    -- Insert statements for procedure here
	IF(@pricingSubGroupId IS NULL)
	BEGIN
		SET @pricingSubGroupName = 'NULL'
	END
	ELSE
	BEGIN
		SET @pricingSubGroupName = 'Unknown ' + CAST(@pricingSubGroupId AS VARCHAR(16))

		SELECT @pricingSubGroupName = PRICINGSUBGROUP.NAME
		FROM PRICINGSUBGROUP
		WHERE PRICINGSUBGROUP.ID = @pricingSubGroupId
	END
END
GO

--========================================================================
--= Procedure: ConvertRebateTypeId
--========================================================================

IF OBJECT_ID('ConvertRebateTypeId', 'P') IS NOT NULL
    DROP PROCEDURE ConvertRebateTypeId
	GO
-- =============================================
-- Author:		<Evolve>
-- Create date: 18th May 2016
-- Description:	Convert from RebateTypeId to RebateType
-- =============================================
CREATE PROCEDURE ConvertRebateTypeId
	-- Add the parameters for the stored procedure here
	@rebateTypeId INT,
	@rebateTypeName VARCHAR(MAX) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON

    -- Insert statements for procedure here
	IF(@rebateTypeId IS NULL)
	BEGIN
		SET @rebateTypeName = 'NULL'
	END
	ELSE
	BEGIN
		SET @rebateTypeName = 'Unknown ' + CAST(@rebateTypeId AS VARCHAR(16))

		SELECT @rebateTypeName = REBATETYPE.NAME
		FROM REBATETYPE
		WHERE REBATETYPE.ID = @rebateTypeId
	END
END
GO

--========================================================================
--= Procedure: ConvertExceptionId
--========================================================================

IF OBJECT_ID('ConvertExceptionId', 'P') IS NOT NULL
    DROP PROCEDURE ConvertExceptionId
GO
-- =============================================
-- Author:		<Evolve>
-- Create date: 18th May 2016
-- Description:	Convert from ExceptionId to ExceptionName
-- =============================================
CREATE PROCEDURE ConvertExceptionId
	-- Add the parameters for the stored procedure here
	@exceptionTypeEnum INT,
	@exceptionId INT,
	@exceptionName VARCHAR(MAX) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF @exceptionTypeEnum = 1
		EXEC ConvertSiteId @exceptionId, @siteName = @exceptionName OUT
	ELSE IF @exceptionTypeEnum = 2 OR @exceptionTypeEnum = 3
		EXEC ConvertCustomerId @exceptionId, @customerName = @exceptionName OUT
	ELSE
		SET @exceptionName = CAST(@exceptionId AS INT)
END
GO

--========================================================================
--= Procedure: ConvertHierarchyEntityId
--========================================================================

IF OBJECT_ID('ConvertHierarchyEntityId', 'P') IS NOT NULL
    DROP PROCEDURE ConvertHierarchyEntityId
GO
-- =============================================
-- Author:		<Evolve>
-- Create date: 18th May 2016
-- Description:	Convert from HierarchyEntityId to HierarchyEntityName
-- =============================================
CREATE PROCEDURE ConvertHierarchyEntityId
	-- Add the parameters for the stored procedure here
	@hierarchyId INT,
	@hierarchyEntityId INT,
	@hierarchyEntityName VARCHAR(MAX) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @relatedEntityName VARCHAR(20) = 'Unknown'
	
	SELECT @relatedEntityName = HIERARCHY.RELATEDENTITYNAME
	FROM HIERARCHY
	WHERE HIERARCHY.ID = @hierarchyId
	
	IF @relatedEntityName = 'CustomerGroup'
		EXEC ConvertCustomerGroupId @hierarchyEntityId, @customerGroupName = @hierarchyEntityName OUT
	ELSE IF @relatedEntityName = 'Customer'
		EXEC ConvertCustomerId @hierarchyEntityId, @customerName = @hierarchyEntityName OUT
	ELSE
		SET @hierarchyEntityName = CAST(@hierarchyEntityId AS INT)
END
GO

--========================================================================
--= Procedure: ConvertDeliveryTypeEnum
--========================================================================

IF OBJECT_ID('ConvertDeliveryTypeEnum', 'P') IS NOT NULL
    DROP PROCEDURE ConvertDeliveryTypeEnum
GO
-- =============================================
-- Author:		<Evolve>
-- Create date: 18th May 2016
-- Description:	Convert from DeliveryTypeEnum to DeliveryTypeName
-- =============================================
CREATE PROCEDURE ConvertDeliveryTypeEnum
	-- Add the parameters for the stored procedure here
	@deliveryTypeEnum INT,
	@deliveryTypeName VARCHAR(MAX) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SET @deliveryTypeName =
	CASE 
		WHEN @deliveryTypeEnum = 1 THEN 'Delivery'
		WHEN @deliveryTypeEnum = 2 THEN 'Collect'
		ELSE 'NULL'
	END
END
GO

--========================================================================
--= Procedure: ConvertExceptionTypeEnum
--========================================================================

IF OBJECT_ID('ConvertExceptionTypeEnum', 'P') IS NOT NULL
    DROP PROCEDURE ConvertExceptionTypeEnum
GO
-- =============================================
-- Author:		<Evolve>
-- Create date: 18th May 2016
-- Description:	Convert from ExceptionTypeEnum to ExceptionTypeName
-- =============================================
CREATE PROCEDURE ConvertExceptionTypeEnum
	-- Add the parameters for the stored procedure here
	@exceptionTypeEnum INT,
	@exceptionTypeName VARCHAR(MAX) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SET @exceptionTypeName =
	CASE 
		WHEN @exceptionTypeEnum = 1 THEN 'Sold From'
		WHEN @exceptionTypeEnum = 2 THEN 'Sold To'
		WHEN @exceptionTypeEnum = 3 THEN 'Ship To'
		ELSE 'NULL'
	END
END
GO

--========================================================================
--= Procedure: ConvertPackingTypeEnum
--========================================================================

IF OBJECT_ID('ConvertPackingTypeEnum', 'P') IS NOT NULL
    DROP PROCEDURE ConvertPackingTypeEnum
GO
-- =============================================
-- Author:		<Evolve>
-- Create date: 18th May 2016
-- Description:	Convert from PackingTypeEnum to PackingTypeName
-- =============================================
CREATE PROCEDURE ConvertPackingTypeEnum
	-- Add the parameters for the stored procedure here
	@packingTypeEnum INT,
	@packingTypeName VARCHAR(MAX) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SET @packingTypeName =
	CASE 
		WHEN @packingTypeEnum = 1 THEN 'Pack'
		WHEN @packingTypeEnum = 2 THEN 'Bulk'
		ELSE 'NULL'
	END
END
GO

--========================================================================
--= Procedure: InsertAuditHeader
--========================================================================

IF OBJECT_ID('InsertAuditHeader', 'P') IS NOT NULL
    DROP PROCEDURE InsertAuditHeader
GO
-- =============================================
-- Author:		<Evolve>
-- Create date: 19th May 2016
-- Description:	Insert AuditHeader record
-- =============================================
CREATE PROCEDURE InsertAuditHeader
	-- Add the parameters for the stored procedure here
	@tableName VARCHAR(50),
	@rebateId INT,
	@recordId INT,
	@customerGroupId INT,
	@customerID INT,
	@databaseAction CHAR(1),
	@auditHeaderId INT OUTPUT
AS
BEGIN
	DECLARE @userName VARCHAR(50)
	DECLARE @timeStamp DATETIME
	DECLARE @databaseActionEnum INT
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	SET @userName = suser_name()
	SET @timeStamp = CURRENT_TIMESTAMP

	-- 1=Insert; 2=Update; 3=Delete
	SET @databaseActionEnum = 1
	IF @databaseAction = 'U'
		SET @databaseActionEnum = 2
	ELSE IF @databaseAction = 'D'
		SET @databaseActionEnum = 3

	INSERT INTO AuditHeader
	(
		UserName,
		[TimeStamp],
		TableName,
		RebateId,
		RecordId,
		CustomerGroupId,
		CustomerId,
		DatabaseActionEnum
	)
	VALUES
	(
		@userName,
		@timeStamp,
		@tableName,
		@rebateId,
		@recordId,
		@customerGroupId,
		@customerID,
		@databaseActionEnum
	)

	SET @auditHeaderId = SCOPE_IDENTITY()

END
GO

--========================================================================
--= Procedure: InsertAuditHeader
--========================================================================

IF OBJECT_ID('InsertAuditDetail', 'P') IS NOT NULL
    DROP PROCEDURE InsertAuditDetail
GO
-- =============================================
-- Author:		<Evolve>
-- Create date: 20th May 2016
-- Description:	Insert AuditDetail record
-- =============================================
CREATE PROCEDURE InsertAuditDetail
	-- Add the parameters for the stored procedure here
	@auditHeaderId INT,
	@columnName VARCHAR(50),
	@oldValueRaw VARCHAR(MAX),
	@oldValue VARCHAR(MAX),
	@newValueRaw VARCHAR(MAX),
	@newValue VARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	INSERT INTO AuditDetail
	(
		AuditHeaderId,
		ColumnName,
		OldValueRaw,
		OldValue,
		NewValueRaw,
		NewValue
	)
	VALUES
	(
		@auditHeaderId,
		@columnName,
		@oldValueRaw,
		@oldValue,
		@newValueRaw,
		@newValue
	)
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--========================================================================
--= Trigger: AuditRebateVAP
--========================================================================

IF OBJECT_ID('AuditRebateVAP', 'TR') IS NOT NULL
    DROP TRIGGER AuditRebateVAP
GO
-- =============================================
-- Author:		<Evolve>
-- Create date: 20th May 2016
-- Description:	Audit RebateVAP table
-- =============================================
CREATE TRIGGER AuditRebateVAP
ON RebateVAP
FOR INSERT, UPDATE, DELETE
AS
BEGIN
	DECLARE @auditType CHAR(1)
	IF EXISTS(SELECT * FROM INSERTED) AND EXISTS(SELECT * FROM DELETED)
		SET @auditType = 'U'
	ELSE IF EXISTS(SELECT * FROM INSERTED)
		SET @auditType = 'I'
	ELSE
		SET @auditType = 'D'

	DECLARE @recordId INT
	DECLARE @rebateId INT
	DECLARE @oldProductId INT
	DECLARE @newProductId INT
	DECLARE @oldActivationVolume INT
	DECLARE @newActivationVolume INT
	DECLARE @auditHeaderId INT
	DECLARE @tableName VARCHAR(50) = 'RebateVAP'
	DECLARE @oldValueRaw VARCHAR(MAX)
	DECLARE @oldValue VARCHAR(MAX)
	DECLARE @newValueRaw VARCHAR(MAX)
	DECLARE @newValue VARCHAR(MAX)

	IF @auditType = 'I' OR @auditType = 'U'
	BEGIN
		DECLARE c1 CURSOR LOCAL FOR
		SELECT
			I.ID,
			I.REBATEID,
			I.PRODUCTID,
			I.ACTIVATIONVOLUME
		FROM INSERTED I

		OPEN c1

		FETCH NEXT FROM c1 INTO
			@recordId,
			@rebateId,
			@newProductId,
			@newActivationVolume


		WHILE @@FETCH_STATUS = 0 
		BEGIN
			SET @auditHeaderId = 0

			IF @auditType = 'U'
			BEGIN
				SELECT
				@oldProductId = D.PRODUCTID,
				@oldActivationVolume = D.ACTIVATIONVOLUME
				FROM DELETED D
				WHERE D.ID = @recordId

				IF @oldProductId != @newProductId
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldProductId AS VARCHAR(MAX))
					EXEC ConvertProductId @oldProductId, @productName = @oldValue OUT
					SET @newValueRaw = CAST(@newProductId AS VARCHAR(MAX))
					EXEC ConvertProductId @newProductId, @productName = @newValue OUT
					EXEC InsertAuditDetail @auditHeaderId, 'ProductId', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END

				IF @oldActivationVolume != @newActivationVolume
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldActivationVolume AS VARCHAR(MAX))
					SET @oldValue = @oldValueRaw
					SET @newValueRaw = CAST(@newActivationVolume AS VARCHAR(MAX))
					SET @newValue = @newValueRaw
					EXEC InsertAuditDetail @auditHeaderId, 'ActivationVolume', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END
			END
			ELSE
			BEGIN			
				EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

				SET @newValueRaw = CAST(@newProductId AS VARCHAR(MAX))
				EXEC ConvertProductId @newProductId, @productName = @newValue OUT
				EXEC InsertAuditDetail @auditHeaderId, 'ProductId', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newActivationVolume AS VARCHAR(MAX))
				SET @newValue = @newValueRaw
				EXEC InsertAuditDetail @auditHeaderId, 'ActivationVolume', 'NULL', 'NULL', @newValueRaw, @newValue
			END

			FETCH NEXT FROM c1 INTO
			@recordId,
			@rebateId,
			@newProductId,
			@newActivationVolume

		END

		CLOSE c1
		DEALLOCATE c1
	END
	ELSE
	BEGIN
		DECLARE c2 CURSOR LOCAL FOR
		SELECT
			D.ID,
			D.REBATEID,
			D.PRODUCTID,
			D.ACTIVATIONVOLUME
		FROM DELETED D

		OPEN c2

		FETCH NEXT FROM c2 INTO
			@recordId,
			@rebateId,
			@oldProductId,
			@oldActivationVolume


		WHILE @@FETCH_STATUS = 0 
		BEGIN
			EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

			SET @oldValueRaw = CAST(@oldProductId AS VARCHAR(MAX))
			EXEC ConvertProductId @oldProductId, @productName = @oldValue OUT
			EXEC InsertAuditDetail @auditHeaderId, 'ProductId', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldActivationVolume AS VARCHAR(MAX))
			SET @oldValue = @oldValueRaw
			EXEC InsertAuditDetail @auditHeaderId, 'ActivationVolume', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			FETCH NEXT FROM c2 INTO
				@recordId,
				@rebateId,
				@oldProductId,
				@oldActivationVolume
		END

		CLOSE c2
		DEALLOCATE c2
	END
END
GO

--========================================================================
--= Trigger: AuditRebateRange
--========================================================================

IF OBJECT_ID('AuditRebateRange', 'TR') IS NOT NULL
    DROP TRIGGER AuditRebateRange
GO
-- =============================================
-- Author:		<Evolve>
-- Create date: 20th May 2016
-- Description:	Audit RebateRange table
-- =============================================
CREATE TRIGGER AuditRebateRange
ON RebateRange
FOR INSERT, UPDATE, DELETE
AS
BEGIN
	DECLARE @auditType CHAR(1)
	IF EXISTS(SELECT * FROM INSERTED) AND EXISTS(SELECT * FROM DELETED)
		SET @auditType = 'U'
	ELSE IF EXISTS(SELECT * FROM INSERTED)
		SET @auditType = 'I'
	ELSE
		SET @auditType = 'D'

	DECLARE @recordId INT
	DECLARE @rebateId INT
	DECLARE @oldAboveQuantity INT
	DECLARE @newAboveQuantity INT
	DECLARE @oldRate NUMERIC(10,2)
	DECLARE @newRate NUMERIC(10,2)
	DECLARE @auditHeaderId INT
	DECLARE @tableName VARCHAR(50) = 'RebateRange'
	DECLARE @oldValueRaw VARCHAR(MAX)
	DECLARE @oldValue VARCHAR(MAX)
	DECLARE @newValueRaw VARCHAR(MAX)
	DECLARE @newValue VARCHAR(MAX)

	IF @auditType = 'I' OR @auditType = 'U'
	BEGIN
		DECLARE c1 CURSOR LOCAL FOR
		SELECT
			I.ID,
			I.REBATEID,
			I.ABOVEQUANTITY,
			I.RATE
		FROM INSERTED I

		OPEN c1

		FETCH NEXT FROM c1 INTO
			@recordId,
			@rebateId,
			@newAboveQuantity,
			@newRate


		WHILE @@FETCH_STATUS = 0 
		BEGIN
			SET @auditHeaderId = 0

			IF @auditType = 'U'
			BEGIN
				SELECT
				@oldAboveQuantity = D.ABOVEQUANTITY,
				@oldRate = D.RATE
				FROM DELETED D
				WHERE D.ID = @recordId

				IF @oldAboveQuantity != @newAboveQuantity
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldAboveQuantity AS VARCHAR(MAX))
					SET @oldValue = @oldValueRaw
					SET @newValueRaw = CAST(@newAboveQuantity AS VARCHAR(MAX))
					SET @newValue = @newValueRaw
					EXEC InsertAuditDetail @auditHeaderId, 'AboveQuantity', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END

				IF @oldRate != @newRate
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldRate AS VARCHAR(MAX))
					SET @oldValue = @oldValueRaw
					SET @newValueRaw = CAST(@newRate AS VARCHAR(MAX))
					SET @newValue = @newValueRaw
					EXEC InsertAuditDetail @auditHeaderId, 'Rate', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END
			END
			ELSE
			BEGIN			
				EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

				SET @newValueRaw = CAST(@newAboveQuantity AS VARCHAR(MAX))
				SET @newValue = @newValueRaw
				EXEC InsertAuditDetail @auditHeaderId, 'AboveQuantity', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newRate AS VARCHAR(MAX))
				SET @newValue = @newValueRaw
				EXEC InsertAuditDetail @auditHeaderId, 'Rate', 'NULL', 'NULL', @newValueRaw, @newValue
			END

			FETCH NEXT FROM c1 INTO
			@recordId,
			@rebateId,
			@newAboveQuantity,
			@newRate

		END

		CLOSE c1
		DEALLOCATE c1
	END
	ELSE
	BEGIN
		DECLARE c2 CURSOR LOCAL FOR
		SELECT
			D.ID,
			D.REBATEID,
			D.ABOVEQUANTITY,
			D.RATE
		FROM DELETED D

		OPEN c2

		FETCH NEXT FROM c2 INTO
			@recordId,
			@rebateId,
			@oldAboveQuantity,
			@oldRate


		WHILE @@FETCH_STATUS = 0 
		BEGIN
			EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

			SET @oldValueRaw = CAST(@oldAboveQuantity AS VARCHAR(MAX))
			SET @oldValue = @oldValueRaw
			EXEC InsertAuditDetail @auditHeaderId, 'AboveQuantity', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldRate AS VARCHAR(MAX))
			SET @oldValue = @oldValueRaw
			EXEC InsertAuditDetail @auditHeaderId, 'Rate', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			FETCH NEXT FROM c2 INTO
				@recordId,
				@rebateId,
				@oldAboveQuantity,
				@oldRate
		END

		CLOSE c2
		DEALLOCATE c2
	END
END
GO

--========================================================================
--= Trigger: AuditRebateLeadTime
--========================================================================

IF OBJECT_ID('AuditRebateLeadTime', 'TR') IS NOT NULL
    DROP TRIGGER AuditRebateLeadTime
GO
-- =============================================
-- Author:		<Evolve>
-- Create date: 24th May 2016
-- Description:	Audit RebateLeadTime table
-- =============================================
CREATE TRIGGER AuditRebateLeadTime
ON RebateLeadTime
FOR INSERT, UPDATE, DELETE
AS
BEGIN
	DECLARE @auditType CHAR(1)
	IF EXISTS(SELECT * FROM INSERTED) AND EXISTS(SELECT * FROM DELETED)
		SET @auditType = 'U'
	ELSE IF EXISTS(SELECT * FROM INSERTED)
		SET @auditType = 'I'
	ELSE
		SET @auditType = 'D'

	DECLARE @recordId INT
	DECLARE @rebateId INT
	DECLARE @oldLeadTimeId INT
	DECLARE @newLeadTimeId INT
	DECLARE @oldPackingTypeEnum INT
	DECLARE @newPackingTypeEnum INT
	DECLARE @oldRate NUMERIC(10,2)
	DECLARE @newRate NUMERIC(10,2)
	DECLARE @auditHeaderId INT
	DECLARE @tableName VARCHAR(50) = 'RebateLeadTime'
	DECLARE @oldValueRaw VARCHAR(MAX)
	DECLARE @oldValue VARCHAR(MAX)
	DECLARE @newValueRaw VARCHAR(MAX)
	DECLARE @newValue VARCHAR(MAX)

	IF @auditType = 'I' OR @auditType = 'U'
	BEGIN
		DECLARE c1 CURSOR LOCAL FOR
		SELECT
			I.ID,
			I.REBATEID,
			I.LEADTIMEID,
			I.PACKINGTYPEENUM,
			I.RATE
		FROM INSERTED I

		OPEN c1

		FETCH NEXT FROM c1 INTO
			@recordId,
			@rebateId,
			@newLeadTimeId,
			@newPackingTypeEnum,
			@newRate


		WHILE @@FETCH_STATUS = 0 
		BEGIN
			SET @auditHeaderId = 0

			IF @auditType = 'U'
			BEGIN
				SELECT
				@oldLeadTimeId = D.LEADTIMEID,
				@oldPackingTypeEnum = D.PACKINGTYPEENUM,
				@oldRate = D.RATE
				FROM DELETED D
				WHERE D.ID = @recordId

				IF @oldLeadTimeId != @newLeadTimeId
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldLeadTimeId AS VARCHAR(MAX))
					EXEC ConvertLeadTimeId @oldLeadTimeId, @leadTimeName = @oldValue OUT
					SET @newValueRaw = CAST(@newLeadTimeId AS VARCHAR(MAX))
					EXEC ConvertLeadTimeId @newLeadTimeId, @leadTimeName = @newValue OUT
					EXEC InsertAuditDetail @auditHeaderId, 'LeadTimeId', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END

				IF @oldPackingTypeEnum != @newPackingTypeEnum
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldPackingTypeEnum AS VARCHAR(MAX))
					EXEC ConvertPackingTypeEnum @oldPackingTypeEnum, @packingTypeName = @oldValue OUT
					SET @newValueRaw = CAST(@newPackingTypeEnum AS VARCHAR(MAX))
					EXEC ConvertPackingTypeEnum @newPackingTypeEnum, @packingTypeName = @newValue OUT
					EXEC InsertAuditDetail @auditHeaderId, 'PackingTypeEnum', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END

				IF @oldRate != @newRate
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldRate AS VARCHAR(MAX))
					SET @oldValue = @oldValueRaw
					SET @newValueRaw = CAST(@newRate AS VARCHAR(MAX))
					SET @newValue = @newValueRaw
					EXEC InsertAuditDetail @auditHeaderId, 'Rate', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END
			END
			ELSE
			BEGIN			
				EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

				SET @newValueRaw = CAST(@newLeadTimeId AS VARCHAR(MAX))
				EXEC ConvertLeadTimeId @newLeadTimeId, @leadTimeName = @newValue OUT
				EXEC InsertAuditDetail @auditHeaderId, 'LeadTimeId', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newPackingTypeEnum AS VARCHAR(MAX))
				EXEC ConvertPackingTypeEnum @newPackingTypeEnum, @packingTypeName = @newValue OUT
				EXEC InsertAuditDetail @auditHeaderId, 'PackingTypeEnum', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newRate AS VARCHAR(MAX))
				SET @newValue = @newValueRaw
				EXEC InsertAuditDetail @auditHeaderId, 'Rate', 'NULL', 'NULL', @newValueRaw, @newValue
			END

			FETCH NEXT FROM c1 INTO
				@recordId,
				@rebateId,
				@newLeadTimeId,
				@newPackingTypeEnum,
				@newRate

		END

		CLOSE c1
		DEALLOCATE c1
	END
	ELSE
	BEGIN
		DECLARE c2 CURSOR LOCAL FOR
		SELECT
			D.ID,
			D.REBATEID,
			D.LEADTIMEID,
			D.PACKINGTYPEENUM,
			D.RATE
		FROM DELETED D

		OPEN c2

		FETCH NEXT FROM c2 INTO
			@recordId,
			@rebateId,
			@oldLeadTimeId,
			@oldPackingTypeEnum,
			@oldRate


		WHILE @@FETCH_STATUS = 0 
		BEGIN
			EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

			SET @oldValueRaw = CAST(@oldLeadTimeId AS VARCHAR(MAX))
			EXEC ConvertLeadTimeId @oldLeadTimeId, @leadTimeName = @oldValue OUT
			EXEC InsertAuditDetail @auditHeaderId, 'LeadTimeId', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldLeadTimeId AS VARCHAR(MAX))
			EXEC ConvertPackingTypeEnum @oldPackingTypeEnum, @packingTypeName = @oldValue OUT
			EXEC InsertAuditDetail @auditHeaderId, 'PackingTypeEnum', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldRate AS VARCHAR(MAX))
			SET @oldValue = @oldValueRaw
			EXEC InsertAuditDetail @auditHeaderId, 'Rate', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			FETCH NEXT FROM c2 INTO
				@recordId,
				@rebateId,
				@oldLeadTimeId,
				@oldPackingTypeEnum,
				@oldRate
		END

		CLOSE c2
		DEALLOCATE c2
	END
END
GO

--========================================================================
--= Trigger: AuditRebateDetail
--========================================================================

IF OBJECT_ID('AuditRebateDetail', 'TR') IS NOT NULL
    DROP TRIGGER AuditRebateDetail
GO
-- =============================================
-- Author:		<Evolve>
-- Create date: 24th May 2016
-- Description:	Audit RebateDetail table
-- =============================================
CREATE TRIGGER AuditRebateDetail
ON RebateDetail
FOR INSERT, UPDATE, DELETE
AS
BEGIN
	DECLARE @auditType CHAR(1)
	IF EXISTS(SELECT * FROM INSERTED) AND EXISTS(SELECT * FROM DELETED)
		SET @auditType = 'U'
	ELSE IF EXISTS(SELECT * FROM INSERTED)
		SET @auditType = 'I'
	ELSE
		SET @auditType = 'D'

	DECLARE @recordId INT
	DECLARE @rebateId INT
	DECLARE @oldAttribute1Id INT
	DECLARE @newAttribute1Id INT
	DECLARE @oldAttribute2Id INT
	DECLARE @newAttribute2Id INT
	DECLARE @oldAttribute3Id INT
	DECLARE @newAttribute3Id INT
	DECLARE @oldAttribute4Id INT
	DECLARE @newAttribute4Id INT
	DECLARE @oldProductId INT
	DECLARE @newProductId INT
	DECLARE @oldRate NUMERIC(10,2)
	DECLARE @newRate NUMERIC(10,2)
	DECLARE @oldPriceEnquiryRate NUMERIC(10,2)
	DECLARE @newPriceEnquiryRate NUMERIC(10,2)
	DECLARE @oldIsDeleted BIT
	DECLARE @newIsDeleted BIT
	DECLARE @auditHeaderId INT
	DECLARE @tableName VARCHAR(50) = 'RebateDetail'
	DECLARE @oldValueRaw VARCHAR(MAX)
	DECLARE @oldValue VARCHAR(MAX)
	DECLARE @newValueRaw VARCHAR(MAX)
	DECLARE @newValue VARCHAR(MAX)

	IF @auditType = 'I' OR @auditType = 'U'
	BEGIN
		DECLARE c1 CURSOR LOCAL FOR
		SELECT
			I.ID,
			I.REBATEID,
			I.ATTRIBUTEID1,
			I.ATTRIBUTEID2,
			I.ATTRIBUTEID3,
			I.ATTRIBUTEID4,
			I.PRODUCTID,
			I.RATE,
			I.PRICEENQUIRYRATE,
			I.ISDELETED
		FROM INSERTED I

		OPEN c1

		FETCH NEXT FROM c1 INTO
			@recordId,
			@rebateId,
			@newAttribute1Id,
			@newAttribute2Id,
			@newAttribute3Id,
			@newAttribute4Id,
			@newProductId,
			@newRate,
			@newPriceEnquiryRate,
			@newIsDeleted


		WHILE @@FETCH_STATUS = 0 
		BEGIN
			SET @auditHeaderId = 0

			IF @auditType = 'U'
			BEGIN
				SELECT
					@oldAttribute1Id = D.ATTRIBUTEID1,
					@oldAttribute2Id = D.ATTRIBUTEID2,
					@oldAttribute3Id = D.ATTRIBUTEID3,
					@oldAttribute4Id = D.ATTRIBUTEID4,
					@oldProductId = D.PRODUCTID,
					@oldRate = D.RATE,
					@oldPriceEnquiryRate = D.PRICEENQUIRYRATE,
					@oldIsDeleted = d.ISDELETED
				FROM DELETED D
				WHERE D.ID = @recordId

				IF COALESCE(@oldAttribute1Id, 0) != COALESCE(@newAttribute1Id, 0)
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldAttribute1Id AS VARCHAR(MAX))
					EXEC ConvertAttributeId @oldAttribute1Id, @attributeName = @oldValue OUT
					SET @newValueRaw = CAST(@newAttribute1Id AS VARCHAR(MAX))
					EXEC ConvertAttributeId @newAttribute1Id, @attributeName = @newValue OUT
					EXEC InsertAuditDetail @auditHeaderId, 'AttributeId1', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END

				IF COALESCE(@oldAttribute2Id, 0) != COALESCE(@newAttribute2Id, 0)
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldAttribute2Id AS VARCHAR(MAX))
					EXEC ConvertAttributeId @oldAttribute2Id, @attributeName = @oldValue OUT
					SET @newValueRaw = CAST(@newAttribute2Id AS VARCHAR(MAX))
					EXEC ConvertAttributeId @newAttribute2Id, @attributeName = @newValue OUT
					EXEC InsertAuditDetail @auditHeaderId, 'AttributeId2', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END

				IF COALESCE(@oldAttribute3Id, 0) != COALESCE(@newAttribute3Id, 0)
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldAttribute3Id AS VARCHAR(MAX))
					EXEC ConvertAttributeId @oldAttribute3Id, @attributeName = @oldValue OUT
					SET @newValueRaw = CAST(@newAttribute3Id AS VARCHAR(MAX))
					EXEC ConvertAttributeId @newAttribute3Id, @attributeName = @newValue OUT
					EXEC InsertAuditDetail @auditHeaderId, 'AttributeId3', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END

				IF COALESCE(@oldAttribute4Id, 0) != COALESCE(@newAttribute4Id, 0)
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldAttribute4Id AS VARCHAR(MAX))
					EXEC ConvertAttributeId @oldAttribute4Id, @attributeName = @oldValue OUT
					SET @newValueRaw = CAST(@newAttribute4Id AS VARCHAR(MAX))
					EXEC ConvertAttributeId @newAttribute4Id, @attributeName = @newValue OUT
					EXEC InsertAuditDetail @auditHeaderId, 'AttributeId4', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END

				IF COALESCE(@oldProductId, 0) != COALESCE(@newProductId, 0)
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldProductId AS VARCHAR(MAX))
					EXEC ConvertProductId @oldProductId, @productName = @oldValue OUT
					SET @newValueRaw = CAST(@newProductId AS VARCHAR(MAX))
					EXEC ConvertProductId @newProductId, @productName = @newValue OUT
					EXEC InsertAuditDetail @auditHeaderId, 'ProductId', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END

				IF COALESCE(@oldRate, 0) != COALESCE(@newRate, 0)
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldRate AS VARCHAR(MAX))
					SET @oldValue = @oldValueRaw
					SET @newValueRaw = CAST(@newRate AS VARCHAR(MAX))
					SET @newValue = @newValueRaw
					EXEC InsertAuditDetail @auditHeaderId, 'Rate', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END

				IF COALESCE(@oldPriceEnquiryRate, 0) != COALESCE(@newPriceEnquiryRate, 0)
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldPriceEnquiryRate AS VARCHAR(MAX))
					SET @oldValue = @oldValueRaw
					SET @newValueRaw = CAST(@newPriceEnquiryRate AS VARCHAR(MAX))
					SET @newValue = @newValueRaw
					EXEC InsertAuditDetail @auditHeaderId, 'PriceEnquiryRate', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END

				IF @oldIsDeleted != @newIsDeleted
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldIsDeleted AS VARCHAR(MAX))
					EXEC ConvertBoolean @oldIsDeleted, @output = @newValue OUT
					SET @newValueRaw = CAST(@newIsDeleted AS VARCHAR(MAX))
					EXEC ConvertBoolean @newIsDeleted, @output = @newValue OUT
					EXEC InsertAuditDetail @auditHeaderId, 'IsDeleted', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END
			END
			ELSE
			BEGIN			
				EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

				SET @newValueRaw = CAST(@newAttribute1Id AS VARCHAR(MAX))
				EXEC ConvertAttributeId @newAttribute1Id, @attributeName = @newValue OUT
				EXEC InsertAuditDetail @auditHeaderId, 'Attribute1Id', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newAttribute2Id AS VARCHAR(MAX))
				EXEC ConvertAttributeId @newAttribute2Id, @attributeName = @newValue OUT
				EXEC InsertAuditDetail @auditHeaderId, 'Attribute2Id', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newAttribute3Id AS VARCHAR(MAX))
				EXEC ConvertAttributeId @newAttribute3Id, @attributeName = @newValue OUT
				EXEC InsertAuditDetail @auditHeaderId, 'Attribute3Id', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newAttribute4Id AS VARCHAR(MAX))
				EXEC ConvertAttributeId @newAttribute4Id, @attributeName = @newValue OUT
				EXEC InsertAuditDetail @auditHeaderId, 'Attribute4Id', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newProductId AS VARCHAR(MAX))
				EXEC ConvertProductId @newProductId, @productName = @newValue OUT
				EXEC InsertAuditDetail @auditHeaderId, 'ProductId', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newRate AS VARCHAR(MAX))
				SET @newValue = @newValueRaw
				EXEC InsertAuditDetail @auditHeaderId, 'Rate', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newPriceEnquiryRate AS VARCHAR(MAX))
				SET @newValue = @newValueRaw
				EXEC InsertAuditDetail @auditHeaderId, 'Rate', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newIsDeleted AS VARCHAR(MAX))
				EXEC ConvertBoolean @newIsDeleted, @output = @newValue OUT
				EXEC InsertAuditDetail @auditHeaderId, 'IsDeleted', 'NULL', 'NULL', @newValueRaw, @newValue
			END

			FETCH NEXT FROM c1 INTO
				@recordId,
				@rebateId,
				@newAttribute1Id,
				@newAttribute2Id,
				@newAttribute3Id,
				@newAttribute4Id,
				@newProductId,
				@newRate,
				@newPriceEnquiryRate,
				@newIsDeleted

		END

		CLOSE c1
		DEALLOCATE c1
	END
	ELSE
	BEGIN
		DECLARE c2 CURSOR LOCAL FOR
		SELECT
			D.ID,
			D.REBATEID,
			D.ATTRIBUTEID1,
			D.ATTRIBUTEID2,
			D.ATTRIBUTEID3,
			D.ATTRIBUTEID4,
			D.PRODUCTID,
			D.RATE,
			D.PRICEENQUIRYRATE,
			D.ISDELETED
		FROM DELETED D

		OPEN c2

		FETCH NEXT FROM c2 INTO
			@recordId,
			@rebateId,
			@oldAttribute1Id,
			@oldAttribute2Id,
			@oldAttribute3Id,
			@oldAttribute4Id,
			@oldProductId,
			@oldRate,
			@oldPriceEnquiryRate,
			@oldIsDeleted


		WHILE @@FETCH_STATUS = 0 
		BEGIN
			EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

			SET @oldValueRaw = CAST(@oldAttribute1Id AS VARCHAR(MAX))
			EXEC ConvertAttributeId @oldAttribute1Id, @attributeName = @oldValue OUT
			EXEC InsertAuditDetail @auditHeaderId, 'Attribute1Id', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldAttribute2Id AS VARCHAR(MAX))
			EXEC ConvertAttributeId @oldAttribute2Id, @attributeName = @oldValue OUT
			EXEC InsertAuditDetail @auditHeaderId, 'Attribute2Id', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldAttribute3Id AS VARCHAR(MAX))
			EXEC ConvertAttributeId @oldAttribute3Id, @attributeName = @oldValue OUT
			EXEC InsertAuditDetail @auditHeaderId, 'Attribute3Id', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldAttribute4Id AS VARCHAR(MAX))
			EXEC ConvertAttributeId @oldAttribute4Id, @attributeName = @oldValue OUT
			EXEC InsertAuditDetail @auditHeaderId, 'Attribute4Id', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldProductId AS VARCHAR(MAX))
			EXEC ConvertProductId @oldProductId, @productName = @oldValue OUT
			EXEC InsertAuditDetail @auditHeaderId, 'ProductId', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldRate AS VARCHAR(MAX))
			SET @oldValue = @oldValueRaw
			EXEC InsertAuditDetail @auditHeaderId, 'Rate', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldPriceEnquiryRate AS VARCHAR(MAX))
			SET @oldValue = @oldValueRaw
			EXEC InsertAuditDetail @auditHeaderId, 'PriceEnquiryRate', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldIsDeleted AS VARCHAR(MAX))
			EXEC ConvertBoolean @oldIsDeleted, @output = @oldValue OUT
			EXEC InsertAuditDetail @auditHeaderId, 'IsDeleted', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			FETCH NEXT FROM c2 INTO
				@recordId,
				@rebateId,
				@oldAttribute1Id,
				@oldAttribute2Id,
				@oldAttribute3Id,
				@oldAttribute4Id,
				@oldProductId,
				@oldRate,
				@oldPriceEnquiryRate,
				@oldIsDeleted
		END

		CLOSE c2
		DEALLOCATE c2
	END
END
GO

--========================================================================
--= Trigger: AuditRebateException
--========================================================================

IF OBJECT_ID('AuditRebateException', 'TR') IS NOT NULL
    DROP TRIGGER AuditRebateException
GO
-- =============================================
-- Author:		<Evolve>
-- Create date: 25th May 2016
-- Description:	Audit RebateException table
-- =============================================
CREATE TRIGGER AuditRebateException
ON RebateException
FOR INSERT, UPDATE, DELETE
AS
BEGIN
	DECLARE @auditType CHAR(1)
	IF EXISTS(SELECT * FROM INSERTED) AND EXISTS(SELECT * FROM DELETED)
		SET @auditType = 'U'
	ELSE IF EXISTS(SELECT * FROM INSERTED)
		SET @auditType = 'I'
	ELSE
		SET @auditType = 'D'

	DECLARE @recordId INT
	DECLARE @rebateId INT
	DECLARE @oldExceptionTypeEnum INT
	DECLARE @newExceptionTypeEnum INT
	DECLARE @oldExceptionId INT
	DECLARE @newExceptionId INT
	DECLARE @auditHeaderId INT
	DECLARE @tableName VARCHAR(50) = 'RebateException'
	DECLARE @oldValueRaw VARCHAR(MAX)
	DECLARE @oldValue VARCHAR(MAX)
	DECLARE @newValueRaw VARCHAR(MAX)
	DECLARE @newValue VARCHAR(MAX)

	IF @auditType = 'I' OR @auditType = 'U'
	BEGIN
		DECLARE c1 CURSOR LOCAL FOR
		SELECT
			I.ID,
			I.REBATEID,
			I.EXCEPTIONTYPEENUM,
			I.EXCEPTIONID
		FROM INSERTED I

		OPEN c1

		FETCH NEXT FROM c1 INTO
			@recordId,
			@rebateId,
			@newExceptionTypeEnum,
			@newExceptionId

		WHILE @@FETCH_STATUS = 0 
		BEGIN
			SET @auditHeaderId = 0

			IF @auditType = 'U'
			BEGIN
				SELECT
					@oldExceptionTypeEnum = D.EXCEPTIONTYPEENUM,
					@oldExceptionId = D.EXCEPTIONID
				FROM DELETED D
				WHERE D.ID = @recordId

				IF @oldExceptionTypeEnum != @newExceptionTypeEnum
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldExceptionTypeEnum AS VARCHAR(MAX))
					EXEC ConvertExceptionTypeEnum @oldExceptionTypeEnum, @exceptionTypeName = @oldValue OUT
					SET @newValueRaw = CAST(@newExceptionTypeEnum AS VARCHAR(MAX))
					EXEC ConvertExceptionTypeEnum @newExceptionTypeEnum, @exceptionTypeName = @newValue OUT
					EXEC InsertAuditDetail @auditHeaderId, 'ExceptionTypeEnum', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END

				IF @oldExceptionId != @newExceptionId
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldExceptionId AS VARCHAR(MAX))
					EXEC ConvertExceptionId @oldExceptionTypeEnum, @oldExceptionId, @exceptionName = @oldValue OUT
					SET @newValueRaw = CAST(@newExceptionId AS VARCHAR(MAX))
					EXEC ConvertExceptionId @newExceptionTypeEnum, @newExceptionId, @exceptionName = @newValue OUT
					EXEC InsertAuditDetail @auditHeaderId, 'ExceptionId', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END
			END
			ELSE
			BEGIN			
				EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

				SET @newValueRaw = CAST(@newExceptionTypeEnum AS VARCHAR(MAX))
				EXEC ConvertExceptionTypeEnum @newExceptionTypeEnum, @exceptionTypeName = @newValue OUT
				EXEC InsertAuditDetail @auditHeaderId, 'ExceptionTypeEnum', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newExceptionId AS VARCHAR(MAX))
				EXEC ConvertExceptionId @newExceptionTypeEnum, @newExceptionId, @exceptionName = @newValue OUT
				EXEC InsertAuditDetail @auditHeaderId, 'ExceptionId', 'NULL', 'NULL', @newValueRaw, @newValue
			END

			FETCH NEXT FROM c1 INTO
				@recordId,
				@rebateId,
				@newExceptionTypeEnum,
				@newExceptionId

		END

		CLOSE c1
		DEALLOCATE c1
	END
	ELSE
	BEGIN
		DECLARE c2 CURSOR LOCAL FOR
		SELECT
			D.ID,
			D.REBATEID,
			D.EXCEPTIONTYPEENUM,
			D.EXCEPTIONID
		FROM DELETED D

		OPEN c2

		FETCH NEXT FROM c2 INTO
			@recordId,
			@rebateId,
			@oldExceptionTypeEnum,
			@oldExceptionId

		WHILE @@FETCH_STATUS = 0 
		BEGIN
			EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

			SET @oldValueRaw = CAST(@oldExceptionTypeEnum AS VARCHAR(MAX))
			EXEC ConvertExceptionTypeEnum @oldExceptionTypeEnum, @exceptionTypeName = @oldValue OUT
			EXEC InsertAuditDetail @auditHeaderId, 'ExceptionTypeEnum', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldExceptionId AS VARCHAR(MAX))
			EXEC ConvertExceptionId @oldExceptionTypeEnum, @oldExceptionId, @exceptionName = @oldValue OUT
			EXEC InsertAuditDetail @auditHeaderId, 'ExceptionId', @oldValueRaw, @oldValue, 'NULL', 'NULL'


			FETCH NEXT FROM c2 INTO
				@recordId,
				@rebateId,
				@oldExceptionTypeEnum,
				@oldExceptionId
		END

		CLOSE c2
		DEALLOCATE c2
	END
END
GO

--========================================================================
--= Trigger: AuditRebateDeliveryWindow
--========================================================================

IF OBJECT_ID('AuditRebateDeliveryWindow', 'TR') IS NOT NULL
    DROP TRIGGER AuditRebateDeliveryWindow
GO
-- =============================================
-- Author:		<Evolve>
-- Create date: 25th May 2016
-- Description:	Audit RebateDeliveryWindow table
-- =============================================
CREATE TRIGGER AuditRebateDeliveryWindow
ON RebateDeliveryWindow
FOR INSERT, UPDATE, DELETE
AS
BEGIN
	DECLARE @auditType CHAR(1)
	IF EXISTS(SELECT * FROM INSERTED) AND EXISTS(SELECT * FROM DELETED)
		SET @auditType = 'U'
	ELSE IF EXISTS(SELECT * FROM INSERTED)
		SET @auditType = 'I'
	ELSE
		SET @auditType = 'D'

	DECLARE @recordId INT
	DECLARE @rebateId INT
	DECLARE @oldDeliveryWindowId INT
	DECLARE @newDeliveryWindowId INT
	DECLARE @oldPackingTypeEnum INT
	DECLARE @newPackingTypeEnum INT
	DECLARE @oldRate NUMERIC(10,2)
	DECLARE @newRate NUMERIC(10,2)
	DECLARE @auditHeaderId INT
	DECLARE @tableName VARCHAR(50) = 'RebateDeliveryWindow'
	DECLARE @oldValueRaw VARCHAR(MAX)
	DECLARE @oldValue VARCHAR(MAX)
	DECLARE @newValueRaw VARCHAR(MAX)
	DECLARE @newValue VARCHAR(MAX)

	IF @auditType = 'I' OR @auditType = 'U'
	BEGIN
		DECLARE c1 CURSOR LOCAL FOR
		SELECT
			I.ID,
			I.REBATEID,
			I.DELIVERYWINDOWID,
			I.PACKINGTYPEENUM,
			I.RATE
		FROM INSERTED I

		OPEN c1

		FETCH NEXT FROM c1 INTO
			@recordId,
			@rebateId,
			@newDeliveryWindowId,
			@newPackingTypeEnum,
			@newRate

		WHILE @@FETCH_STATUS = 0 
		BEGIN
			SET @auditHeaderId = 0

			IF @auditType = 'U'
			BEGIN
				SELECT
					@oldDeliveryWindowId = D.DELIVERYWINDOWID,
					@oldPackingTypeEnum = D.PACKINGTYPEENUM,
					@oldRate = D.RATE
				FROM DELETED D
				WHERE D.ID = @recordId

				IF @oldDeliveryWindowId != @newDeliveryWindowId
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldDeliveryWindowId AS VARCHAR(MAX))
					EXEC ConvertDeliveryWindowId @oldDeliveryWindowId, @deliveryWindowName = @oldValue OUT
					SET @newValueRaw = CAST(@newDeliveryWindowId AS VARCHAR(MAX))
					EXEC ConvertDeliveryWindowId @newDeliveryWindowId, @deliveryWindowName = @newValue OUT
					EXEC InsertAuditDetail @auditHeaderId, 'DeliveryWindowId', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END

				IF @oldPackingTypeEnum != @newPackingTypeEnum
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldPackingTypeEnum AS VARCHAR(MAX))
					EXEC ConvertPackingTypeEnum @oldPackingTypeEnum, @packingTypeName = @oldValue OUT
					SET @newValueRaw = CAST(@newPackingTypeEnum AS VARCHAR(MAX))
					EXEC ConvertPackingTypeEnum @newPackingTypeEnum, @packingTypeName = @newValue OUT
					EXEC InsertAuditDetail @auditHeaderId, 'PackingTypeEnum', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END

				IF @oldRate != @newRate
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldRate AS VARCHAR(MAX))
					SET @oldValue = @oldValueRaw
					SET @newValueRaw = CAST(@newRate AS VARCHAR(MAX))
					SET @newValue = @newValueRaw
					EXEC InsertAuditDetail @auditHeaderId, 'Rate', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END
			END
			ELSE
			BEGIN			
				EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

				SET @newValueRaw = CAST(@newDeliveryWindowId AS VARCHAR(MAX))
				EXEC ConvertDeliveryWindowId @newDeliveryWindowId, @deliveryWindowName = @newValue OUT
				EXEC InsertAuditDetail @auditHeaderId, 'ExceptionTypeEnum', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newPackingTypeEnum AS VARCHAR(MAX))
				EXEC ConvertPackingTypeEnum @newPackingTypeEnum, @packingTypeName = @newValue OUT
				EXEC InsertAuditDetail @auditHeaderId, 'PackingTypeEnum', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newRate AS VARCHAR(MAX))
				SET @newValue = @newValueRaw
				EXEC InsertAuditDetail @auditHeaderId, 'Rate', 'NULL', 'NULL', @newValueRaw, @newValue
			END

			FETCH NEXT FROM c1 INTO
				@recordId,
				@rebateId,
				@newDeliveryWindowId,
				@newPackingTypeEnum,
				@newRate

		END

		CLOSE c1
		DEALLOCATE c1
	END
	ELSE
	BEGIN
		DECLARE c2 CURSOR LOCAL FOR
		SELECT
			D.ID,
			D.REBATEID,
			D.DELIVERYWINDOWID,
			D.PACKINGTYPEENUM,
			D.RATE
		FROM DELETED D

		OPEN c2

		FETCH NEXT FROM c2 INTO
			@recordId,
			@rebateId,
			@oldDeliveryWindowId,
			@oldPackingTypeEnum,
			@oldRate

		WHILE @@FETCH_STATUS = 0 
		BEGIN
			EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

			SET @oldValueRaw = CAST(@oldDeliveryWindowId AS VARCHAR(MAX))
			EXEC ConvertDeliveryWindowId @oldDeliveryWindowId, @deliveryWindowName = @oldValue OUT
			EXEC InsertAuditDetail @auditHeaderId, 'DeliveryWindowId', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldPackingTypeEnum AS VARCHAR(MAX))
			EXEC ConvertPackingTypeEnum @oldPackingTypeEnum, @packingTypeName = @oldValue OUT
			EXEC InsertAuditDetail @auditHeaderId, 'PackingTypeEnum', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldRate AS VARCHAR(MAX))
			SET @oldValue = @oldValueRaw
			EXEC InsertAuditDetail @auditHeaderId, 'Rate', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			FETCH NEXT FROM c2 INTO
				@recordId,
				@rebateId,
				@oldDeliveryWindowId,
				@oldPackingTypeEnum,
				@oldRate
		END

		CLOSE c2
		DEALLOCATE c2
	END
END
GO

--========================================================================
--= Trigger: AuditRebate
--========================================================================

IF OBJECT_ID('AuditRebate', 'TR') IS NOT NULL
    DROP TRIGGER AuditRebate
GO
-- =============================================
-- Author:		<Evolve>
-- Create date: 25th May 2016
-- Description:	Audit AuditRebate table
-- =============================================
CREATE TRIGGER AuditRebate
ON Rebate
FOR INSERT, UPDATE, DELETE
AS
BEGIN
	DECLARE @auditType CHAR(1)
	IF EXISTS(SELECT * FROM INSERTED) AND EXISTS(SELECT * FROM DELETED)
		SET @auditType = 'U'
	ELSE IF EXISTS(SELECT * FROM INSERTED)
		SET @auditType = 'I'
	ELSE
		SET @auditType = 'D'

	DECLARE @recordId INT
	DECLARE @rebateId INT
	DECLARE @oldDivisionId INT
	DECLARE @newDivisionId INT
	DECLARE @oldRebateNumber INT
	DECLARE @newRebateNumber INT
	DECLARE @oldRebateVersion SMALLINT
	DECLARE @newRebateVersion SMALLINT
	DECLARE @oldDescription VARCHAR(100)
	DECLARE @newDescription VARCHAR(100)
	DECLARE @oldHierarchyId INT
	DECLARE @newHierarchyId INT
	DECLARE @oldHierarchyEntityId INT
	DECLARE @newHierarchyEntityId INT
	DECLARE @oldPricingSubGroupId INT
	DECLARE @newPricingSubGroupId INT
	DECLARE @oldPayeeId INT
	DECLARE @newPayeeId INT
	DECLARE @oldDeliveryTypeEnum INT
	DECLARE @newDeliveryTypeEnum INT
	DECLARE @oldSiteId INT
	DECLARE @newSiteId INT
	DECLARE @oldStartDate DATETIME
	DECLARE @newStartDate DATETIME
	DECLARE @oldEndDate DATETIME
	DECLARE @newEndDate DATETIME
	DECLARE @oldIsContract BIT
	DECLARE @newIsContract BIT
	DECLARE @oldIsVAP BIT
	DECLARE @newIsVAP BIT
	DECLARE @oldIsGlobalProduct BIT
	DECLARE @newIsGlobalProduct BIT
	DECLARE @oldIsJIF BIT
	DECLARE @newIsJIF BIT
	DECLARE @oldIsAdjustment BIT
	DECLARE @newIsAdjustment BIT
	DECLARE @oldContractText VARCHAR(100)
	DECLARE @newContractText VARCHAR(100)
	DECLARE @oldVAPActivationVolume INT
	DECLARE @newVAPActivationVolume INT
	DECLARE @oldGlobalRate NUMERIC(10,2)
	DECLARE @newGlobalRate NUMERIC(10,2)
	DECLARE @oldPaymentFrequencyId INT
	DECLARE @newPaymentFrequencyId INT
	DECLARE @oldRebateTypeId INT
	DECLARE @newRebateTypeId INT
	DECLARE @oldEstimatedVolume INT
	DECLARE @newEstimatedVolume INT
	DECLARE @oldFixedRebateValue INT
	DECLARE @newFixedRebateValue INT
	DECLARE @oldIsAccrued BIT
	DECLARE @newIsAccrued BIT
	DECLARE @auditHeaderId INT
	DECLARE @tableName VARCHAR(50) = 'Rebate'
	DECLARE @oldValueRaw VARCHAR(MAX)
	DECLARE @oldValue VARCHAR(MAX)
	DECLARE @newValueRaw VARCHAR(MAX)
	DECLARE @newValue VARCHAR(MAX)

	IF @auditType = 'I' OR @auditType = 'U'
	BEGIN
		DECLARE c1 CURSOR LOCAL FOR
		SELECT
			I.ID,
			I.ID,
			I.REBATENUMBER,
			I.REBATEVERSION,
			I.DESCRIPTION,
			I.HIERARCHYID,
			I.HIERARCHYENTITYID,
			I.PRICINGSUBGROUPID,
			I.PAYEEID,
			I.DELIVERYTYPEENUM,
			I.SITEID,
			I.STARTDATE,
			I.ENDDATE,
			I.ISCONTRACT,
			I.ISVAP,
			I.ISGLOBALPRODUCT,
			I.ISJIF,
			I.ISADJUSTMENT,
			I.CONTRACTTEXT,
			I.VAPACTIVATIONVOLUME,
			I.GLOBALRATE,
			I.PAYMENTFREQUENCYID,
			I.REBATETYPEID,
			I.ESTIMATEDVOLUME,
			I.FIXEDREBATEVALUE,
			I.ISACCRUED
		FROM INSERTED I

		OPEN c1

		FETCH NEXT FROM c1 INTO
			@recordId,
			@rebateId,
			@newRebateNumber,
			@newRebateVersion,
			@newDescription,
			@newHierarchyId,
			@newHierarchyEntityId,
			@newPricingSubGroupId,
			@newPayeeId,
			@newDeliveryTypeEnum,
			@newSiteId,
			@newStartDate,
			@newEndDate,
			@newIsContract,
			@newIsVAP,
			@newIsGlobalProduct,
			@newIsJIF,
			@newIsAdjustment,
			@newContractText,
			@newVAPActivationVolume,
			@newGlobalRate,
			@newPaymentFrequencyId,
			@newRebateTypeId,
			@newEstimatedVolume,
			@newFixedRebateValue,
			@newIsAccrued

		WHILE @@FETCH_STATUS = 0 
		BEGIN
			SET @auditHeaderId = 0

			IF @auditType = 'U'
			BEGIN
				SELECT
					@oldRebateNumber = D.REBATENUMBER,
					@oldRebateVersion = D.REBATEVERSION,
					@oldDescription = D.DESCRIPTION,
					@oldHierarchyId = D.HIERARCHYID,
					@oldHierarchyEntityId = D.HIERARCHYENTITYID,
					@oldPricingSubGroupId = D.PRICINGSUBGROUPID,
					@oldPayeeId = D.PAYEEID,
					@oldDeliveryTypeEnum = D.DELIVERYTYPEENUM,
					@oldSiteId = D.SITEID,
					@oldStartDate = D.STARTDATE,
					@oldEndDate = D.ENDDATE,
					@oldIsContract = D.ISCONTRACT,
					@oldIsVAP = D.ISVAP,
					@oldIsGlobalProduct = D.ISGLOBALPRODUCT,
					@oldIsJIF = D.ISJIF,
					@oldIsAdjustment = D.ISADJUSTMENT,
					@oldContractText = D.CONTRACTTEXT,
					@oldVAPActivationVolume = D.VAPACTIVATIONVOLUME,
					@oldGlobalRate = D.GLOBALRATE,
					@oldPaymentFrequencyId = D.PAYMENTFREQUENCYID,
					@oldRebateTypeId = D.REBATETYPEID,
					@oldEstimatedVolume = D.ESTIMATEDVOLUME,
					@oldFixedRebateValue = D.FIXEDREBATEVALUE,
					@oldIsAccrued = D.ISACCRUED
				FROM DELETED D
				WHERE D.ID = @recordId

				IF @oldRebateNumber != @newRebateNumber
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldRebateNumber AS VARCHAR(MAX))
					SET @oldValue = @oldValueRaw
					SET @newValueRaw = CAST(@newRebateNumber AS VARCHAR(MAX))
					SET @newValue = @newValueRaw
					EXEC InsertAuditDetail @auditHeaderId, 'RebateNumber', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END

				IF @oldRebateVersion != @newRebateVersion
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldRebateVersion AS VARCHAR(MAX))
					SET @oldValue = @oldValueRaw
					SET @newValueRaw = CAST(@newRebateVersion AS VARCHAR(MAX))
					SET @newValue = @newValueRaw
					EXEC InsertAuditDetail @auditHeaderId, 'RebateVersion', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END

				IF @oldDescription != @newDescription
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = @oldDescription
					SET @oldValue = @oldValueRaw
					SET @newValueRaw = @newDescription
					SET @newValue = @newValueRaw
					EXEC InsertAuditDetail @auditHeaderId, 'Description', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END

				IF @oldHierarchyId != @newHierarchyId
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldHierarchyId AS VARCHAR(MAX))
					EXEC ConvertHierarchyId @oldHierarchyId, @hierarchyName = @oldValue OUT
					SET @newValueRaw = CAST(@newHierarchyId AS VARCHAR(MAX))
					EXEC ConvertHierarchyId @newHierarchyId, @hierarchyName = @newValue OUT
					EXEC InsertAuditDetail @auditHeaderId, 'HierarchyId', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END

				IF @oldHierarchyEntityId != @newHierarchyEntityId
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldHierarchyEntityId AS VARCHAR(MAX))
					EXEC ConvertHierarchyEntityId @oldHierarchyId, @oldHierarchyEntityId, @hierarchyEntityName = @oldValue OUT
					SET @newValueRaw = CAST(@newHierarchyEntityId AS VARCHAR(MAX))
					EXEC ConvertHierarchyEntityId @newHierarchyId, @newHierarchyEntityId, @hierarchyEntityName = @newValue OUT
					EXEC InsertAuditDetail @auditHeaderId, 'HierarchyEntityId', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END

				IF COALESCE(@oldPricingSubGroupId, 0) != COALESCE(@newPricingSubGroupId, 0)
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldPricingSubGroupId AS VARCHAR(MAX))
					EXEC ConvertPricingSubGroupId @oldPricingSubGroupId, @pricingSubGroupName = @oldValue OUT
					SET @newValueRaw = CAST(@newPricingSubGroupId AS VARCHAR(MAX))
					EXEC ConvertPricingSubGroupId @newPricingSubGroupId, @pricingSubGroupName = @newValue OUT
					EXEC InsertAuditDetail @auditHeaderId, 'PricingSubGroupId', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END
				
				IF COALESCE(@oldPayeeId, 0) != COALESCE(@newPayeeId, 0)
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldPayeeId AS VARCHAR(MAX))
					EXEC ConvertPayeeId @oldPayeeId, @payeeName = @oldValue OUT
					SET @newValueRaw = CAST(@newPayeeId AS VARCHAR(MAX))
					EXEC ConvertPayeeId @newPayeeId, @payeeName = @newValue OUT
					EXEC InsertAuditDetail @auditHeaderId, 'PayeeId', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END
				
				IF COALESCE(@oldDeliveryTypeEnum, 0) != COALESCE(@newDeliveryTypeEnum, 0)
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldDeliveryTypeEnum AS VARCHAR(MAX))
					EXEC ConvertDeliveryTypeEnum @oldDeliveryTypeEnum, @deliveryTypeName = @oldValue OUT
					SET @newValueRaw = CAST(@newDeliveryTypeEnum AS VARCHAR(MAX))
					EXEC ConvertDeliveryTypeEnum @newDeliveryTypeEnum, @deliveryTypeName = @newValue OUT
					EXEC InsertAuditDetail @auditHeaderId, 'DeliveryTypeEnum', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END
				
				IF COALESCE(@oldSiteId, 0) != COALESCE(@newSiteId, 0)
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldSiteId AS VARCHAR(MAX))
					EXEC ConvertSiteId @oldSiteId, @siteName = @oldValue OUT
					SET @newValueRaw = CAST(@newSiteId AS VARCHAR(MAX))
					EXEC ConvertSiteId @newSiteId, @siteName = @newValue OUT
					EXEC InsertAuditDetail @auditHeaderId, 'SiteId', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END
				
				IF @oldStartDate != @newStartDate
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldStartDate AS VARCHAR(MAX))
					EXEC ConvertDate @oldStartDate, @output = @oldValue OUT
					SET @newValueRaw = CAST(@newStartDate AS VARCHAR(MAX))
					EXEC ConvertDate @newStartDate, @output = @newValue OUT
					EXEC InsertAuditDetail @auditHeaderId, 'StartDate', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END
				
				IF COALESCE(@oldEndDate, '1970-01-01') != COALESCE(@newEndDate, '1970-01-01')
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldEndDate AS VARCHAR(MAX))
					EXEC ConvertDate @oldEndDate, @output = @oldValue OUT
					SET @newValueRaw = CAST(@newEndDate AS VARCHAR(MAX))
					EXEC ConvertDate @newEndDate, @output = @newValue OUT
					EXEC InsertAuditDetail @auditHeaderId, 'EndDate', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END
				
				IF @oldIsContract != @newIsContract
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldIsContract AS VARCHAR(MAX))
					EXEC ConvertBoolean @oldIsContract, @output = @oldValue OUT
					SET @newValueRaw = CAST(@newIsContract AS VARCHAR(MAX))
					EXEC ConvertBoolean @newIsContract, @output = @newValue OUT
					EXEC InsertAuditDetail @auditHeaderId, 'IsContract', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END
				
				IF @oldIsVAP != @newIsVAP
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldIsVAP AS VARCHAR(MAX))
					EXEC ConvertBoolean @oldIsVAP, @output = @oldValue OUT
					SET @newValueRaw = CAST(@newIsVAP AS VARCHAR(MAX))
					EXEC ConvertBoolean @newIsVAP, @output = @newValue OUT
					EXEC InsertAuditDetail @auditHeaderId, 'IsVAP', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END
				
				IF @oldIsGlobalProduct != @newIsGlobalProduct
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldIsGlobalProduct AS VARCHAR(MAX))
					EXEC ConvertBoolean @oldIsGlobalProduct, @output = @oldValue OUT
					SET @newValueRaw = CAST(@newIsGlobalProduct AS VARCHAR(MAX))
					EXEC ConvertBoolean @newIsGlobalProduct, @output = @newValue OUT
					EXEC InsertAuditDetail @auditHeaderId, 'IsGlobalProduct', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END
				
				IF @oldIsJIF != @newIsJIF
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldIsJIF AS VARCHAR(MAX))
					EXEC ConvertBoolean @oldIsJIF, @output = @oldValue OUT
					SET @newValueRaw = CAST(@newIsJIF AS VARCHAR(MAX))
					EXEC ConvertBoolean @newIsJIF, @output = @newValue OUT
					EXEC InsertAuditDetail @auditHeaderId, 'IsJIF', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END
				
				IF @oldIsAdjustment != @newIsAdjustment
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldIsAdjustment AS VARCHAR(MAX))
					EXEC ConvertBoolean @oldIsAdjustment, @output = @oldValue OUT
					SET @newValueRaw = CAST(@newIsAdjustment AS VARCHAR(MAX))
					EXEC ConvertBoolean @newIsAdjustment, @output = @newValue OUT
					EXEC InsertAuditDetail @auditHeaderId, 'IsAdjustment', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END
				
				IF COALESCE(@oldContractText, '') != COALESCE(@newContractText, '')
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = @oldContractText
					SET @oldValue = @oldValueRaw
					SET @newValueRaw = @newContractText
					SET @newValue = @newValueRaw
					EXEC InsertAuditDetail @auditHeaderId, 'ContractText', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END

				IF COALESCE(@oldVAPActivationVolume, 0) != COALESCE(@newVAPActivationVolume, 0)
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldVAPActivationVolume AS VARCHAR(MAX))
					SET @oldValue = @oldValueRaw
					SET @newValueRaw = CAST(@newVAPActivationVolume AS VARCHAR(MAX))
					SET @newValue = @newValueRaw
					EXEC InsertAuditDetail @auditHeaderId, 'VAPActivationVolume', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END
				
				IF COALESCE(@oldGlobalRate, 0) != COALESCE(@newGlobalRate, 0)
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldGlobalRate AS VARCHAR(MAX))
					SET @oldValue = @oldValueRaw
					SET @newValueRaw = CAST(@newGlobalRate AS VARCHAR(MAX))
					SET @newValue = @newValueRaw
					EXEC InsertAuditDetail @auditHeaderId, 'GlobalRate', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END
				
				IF @oldPaymentFrequencyId != @newPaymentFrequencyId
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldPaymentFrequencyId AS VARCHAR(MAX))
					EXEC ConvertPaymentFrequencyId @oldPaymentFrequencyId, @paymentFrequencyName = @oldValue OUT
					SET @newValueRaw = CAST(@newPaymentFrequencyId AS VARCHAR(MAX))
					EXEC ConvertPaymentFrequencyId @newPaymentFrequencyId, @paymentFrequencyName = @newValue OUT
					EXEC InsertAuditDetail @auditHeaderId, 'PaymentFrequencyId', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END
				
				IF @oldRebateTypeId != @newRebateTypeId
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldRebateTypeId AS VARCHAR(MAX))
					EXEC ConvertRebateTypeId @oldRebateTypeId, @rebateTypeName = @oldValue OUT
					SET @newValueRaw = CAST(@newRebateTypeId AS VARCHAR(MAX))
					EXEC ConvertRebateTypeId @newRebateTypeId, @rebateTypeName = @newValue OUT
					EXEC InsertAuditDetail @auditHeaderId, 'RebateTypeId', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END
				
				IF COALESCE(@oldEstimatedVolume, 0) != COALESCE(@newEstimatedVolume, 0)
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldEstimatedVolume AS VARCHAR(MAX))
					SET @oldValue = @oldValueRaw
					SET @newValueRaw = CAST(@newEstimatedVolume AS VARCHAR(MAX))
					SET @newValue = @newValueRaw
					EXEC InsertAuditDetail @auditHeaderId, 'EstimatedVolume', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END
				
				IF COALESCE(@oldFixedRebateValue, 0) != COALESCE(@newFixedRebateValue, 0)
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldFixedRebateValue AS VARCHAR(MAX))
					SET @oldValue = @oldValueRaw
					SET @newValueRaw = CAST(@newFixedRebateValue AS VARCHAR(MAX))
					SET @newValue = @newValueRaw
					EXEC InsertAuditDetail @auditHeaderId, 'FixedRebateValue', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END
				
				IF @oldIsAccrued != @newIsAccrued
				BEGIN
					IF @auditHeaderId = 0
						EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

					SET @oldValueRaw = CAST(@oldIsAccrued AS VARCHAR(MAX))
					EXEC ConvertBoolean @oldIsAccrued, @output = @oldValue OUT
					SET @newValueRaw = CAST(@newIsAccrued AS VARCHAR(MAX))
					EXEC ConvertBoolean @newIsAccrued, @output = @newValue OUT
					EXEC InsertAuditDetail @auditHeaderId, 'IsAccrued', @oldValueRaw, @oldValue, @newValueRaw, @newValue
				END
			END
			ELSE
			BEGIN			
				EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

				SET @newValueRaw = CAST(@newRebateNumber AS VARCHAR(MAX))
				SET @newValue = @newValueRaw
				EXEC InsertAuditDetail @auditHeaderId, 'RebateNumber', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newRebateVersion AS VARCHAR(MAX))
				SET @newValue = @newValueRaw
				EXEC InsertAuditDetail @auditHeaderId, 'RebateVersion', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = @newDescription
				SET @newValue = @newValueRaw
				EXEC InsertAuditDetail @auditHeaderId, 'Description', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newHierarchyId AS VARCHAR(MAX))
				EXEC ConvertHierarchyId @newHierarchyId, @hierarchyName = @newValue OUT
				EXEC InsertAuditDetail @auditHeaderId, 'HierarchyId', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newHierarchyEntityId AS VARCHAR(MAX))
				EXEC ConvertHierarchyEntityId @newHierarchyId, @newHierarchyEntityId, @hierarchyEntityName = @newValue OUT
				EXEC InsertAuditDetail @auditHeaderId, 'HierarchyEntityId', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newPricingSubGroupId AS VARCHAR(MAX))
				EXEC ConvertPricingSubGroupId @newPricingSubGroupId, @pricingSubGroupName = @newValue OUT
				EXEC InsertAuditDetail @auditHeaderId, 'PricingSubGroupId', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newPayeeId AS VARCHAR(MAX))
				EXEC ConvertPayeeId @newPayeeId, @payeeName = @newValue OUT
				EXEC InsertAuditDetail @auditHeaderId, 'PayeeId', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newDeliveryTypeEnum AS VARCHAR(MAX))
				EXEC ConvertDeliveryTypeEnum @newDeliveryTypeEnum, @deliveryTypeName = @newValue OUT
				EXEC InsertAuditDetail @auditHeaderId, 'DeliveryTypeEnum', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newSiteId AS VARCHAR(MAX))
				EXEC ConvertSiteId @newSiteId, @siteName = @newValue OUT
				EXEC InsertAuditDetail @auditHeaderId, 'SiteId', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newStartDate AS VARCHAR(MAX))
				EXEC ConvertDate @newStartDate, @output = @newValue OUT
				EXEC InsertAuditDetail @auditHeaderId, 'StartDate', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newEndDate AS VARCHAR(MAX))
				EXEC ConvertDate @newEndDate, @output = @newValue OUT
				EXEC InsertAuditDetail @auditHeaderId, 'EndDate', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newIsContract AS VARCHAR(MAX))
				EXEC ConvertBoolean @newIsContract, @output = @newValue OUT
				EXEC InsertAuditDetail @auditHeaderId, 'IsContract', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newIsVAP AS VARCHAR(MAX))
				EXEC ConvertBoolean @newIsVAP, @output = @newValue OUT
				EXEC InsertAuditDetail @auditHeaderId, 'IsVAP', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newIsGlobalProduct AS VARCHAR(MAX))
				EXEC ConvertBoolean @newIsGlobalProduct, @output = @newValue OUT
				EXEC InsertAuditDetail @auditHeaderId, 'IsGlobalProduct', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newIsJIF AS VARCHAR(MAX))
				EXEC ConvertBoolean @newIsJIF, @output = @newValue OUT
				EXEC InsertAuditDetail @auditHeaderId, 'IsJIF', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newIsAdjustment AS VARCHAR(MAX))
				EXEC ConvertBoolean @newIsAdjustment, @output = @newValue OUT
				EXEC InsertAuditDetail @auditHeaderId, 'IsAdjustment', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newContractText AS VARCHAR(MAX))
				SET @newValue = @newValueRaw
				EXEC InsertAuditDetail @auditHeaderId, 'ContractText', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newVAPActivationVolume AS VARCHAR(MAX))
				SET @newValue = @newValueRaw
				EXEC InsertAuditDetail @auditHeaderId, 'VAPActivationVolume', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newGlobalRate AS VARCHAR(MAX))
				SET @newValue = @newValueRaw
				EXEC InsertAuditDetail @auditHeaderId, 'GlobalRate', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newPaymentFrequencyId AS VARCHAR(MAX))
				EXEC ConvertPaymentFrequencyId @newPaymentFrequencyId, @paymentFrequencyName = @newValue OUT
				EXEC InsertAuditDetail @auditHeaderId, 'PaymentFrequencyId', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newRebateTypeId AS VARCHAR(MAX))
				EXEC ConvertRebateTypeId @newRebateTypeId, @rebateTypeName = @newValue OUT
				EXEC InsertAuditDetail @auditHeaderId, 'RebateTypeId', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newEstimatedVolume AS VARCHAR(MAX))
				SET @newValue = @newValueRaw
				EXEC InsertAuditDetail @auditHeaderId, 'EstimatedVolume', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newFixedRebateValue AS VARCHAR(MAX))
				SET @newValue = @newValueRaw
				EXEC InsertAuditDetail @auditHeaderId, 'FixedRebateValue', 'NULL', 'NULL', @newValueRaw, @newValue

				SET @newValueRaw = CAST(@newIsAccrued AS VARCHAR(MAX))
				EXEC ConvertBoolean @newIsAccrued, @output = @newValue OUT
				EXEC InsertAuditDetail @auditHeaderId, 'IsAccrued', 'NULL', 'NULL', @newValueRaw, @newValue
			END

			FETCH NEXT FROM c1 INTO
				@recordId,
				@rebateId,
				@newRebateNumber,
				@newRebateVersion,
				@newDescription,
				@newHierarchyId,
				@newHierarchyEntityId,
				@newPricingSubGroupId,
				@newPayeeId,
				@newDeliveryTypeEnum,
				@newSiteId,
				@newStartDate,
				@newEndDate,
				@newIsContract,
				@newIsVAP,
				@newIsGlobalProduct,
				@newIsJIF,
				@newIsAdjustment,
				@newContractText,
				@newVAPActivationVolume,
				@newGlobalRate,
				@newPaymentFrequencyId,
				@newRebateTypeId,
				@newEstimatedVolume,
				@newFixedRebateValue,
				@newIsAccrued

		END

		CLOSE c1
		DEALLOCATE c1
	END
	ELSE
	BEGIN
		DECLARE c2 CURSOR LOCAL FOR
		SELECT
			D.ID,
			D.ID,
			D.REBATENUMBER,
			D.REBATEVERSION,
			D.DESCRIPTION,
			D.HIERARCHYID,
			D.HIERARCHYENTITYID,
			D.PRICINGSUBGROUPID,
			D.PAYEEID,
			D.DELIVERYTYPEENUM,
			D.SITEID,
			D.STARTDATE,
			D.ENDDATE,
			D.ISCONTRACT,
			D.ISVAP,
			D.ISGLOBALPRODUCT,
			D.ISJIF,
			D.ISADJUSTMENT,
			D.CONTRACTTEXT,
			D.VAPACTIVATIONVOLUME,
			D.GLOBALRATE,
			D.PAYMENTFREQUENCYID,
			D.REBATETYPEID,
			D.ESTIMATEDVOLUME,
			D.FIXEDREBATEVALUE,
			D.ISACCRUED
		FROM DELETED D
		
		OPEN c2

		FETCH NEXT FROM c2 INTO
			@recordId,
			@rebateId,
			@oldRebateNumber,
			@oldRebateVersion,
			@oldDescription,
			@oldHierarchyId,
			@oldHierarchyEntityId,
			@oldPricingSubGroupId,
			@oldPayeeId,
			@oldDeliveryTypeEnum,
			@oldSiteId,
			@oldStartDate,
			@oldEndDate,
			@oldIsContract,
			@oldIsVAP,
			@oldIsGlobalProduct,
			@oldIsJIF,
			@oldIsAdjustment,
			@oldContractText,
			@oldVAPActivationVolume,
			@oldGlobalRate,
			@oldPaymentFrequencyId,
			@oldRebateTypeId,
			@oldEstimatedVolume,
			@oldFixedRebateValue,
			@oldIsAccrued

		WHILE @@FETCH_STATUS = 0 
		BEGIN
			EXEC InsertAuditHeader @tableName, @rebateId, @recordId, @auditType, @auditHeaderId = @auditHeaderId OUT

			SET @oldValueRaw = CAST(@oldRebateNumber AS VARCHAR(MAX))
			SET @oldValue = @oldValueRaw
			EXEC InsertAuditDetail @auditHeaderId, 'RebateNumber', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldRebateVersion AS VARCHAR(MAX))
			SET @oldValue = @oldValueRaw
			EXEC InsertAuditDetail @auditHeaderId, 'RebateVersion', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = @oldDescription
			SET @oldValue = @oldValueRaw
			EXEC InsertAuditDetail @auditHeaderId, 'Description', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldHierarchyId AS VARCHAR(MAX))
			EXEC ConvertHierarchyId @oldHierarchyId, @hierarchyName = @oldValue OUT
			EXEC InsertAuditDetail @auditHeaderId, 'HierarchyId', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldHierarchyEntityId AS VARCHAR(MAX))
			EXEC ConvertHierarchyEntityId @oldHierarchyId, @oldHierarchyEntityId, @hierarchyEntityName = @oldValue OUT
			EXEC InsertAuditDetail @auditHeaderId, 'HierarchyEntityId', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldPricingSubGroupId AS VARCHAR(MAX))
			EXEC ConvertPricingSubGroupId @oldPricingSubGroupId, @pricingSubGroupName = @oldValue OUT
			EXEC InsertAuditDetail @auditHeaderId, 'PricingSubGroupId', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldPayeeId AS VARCHAR(MAX))
			EXEC ConvertPayeeId @oldPayeeId, @payeeName = @oldValue OUT
			EXEC InsertAuditDetail @auditHeaderId, 'PayeeId', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldDeliveryTypeEnum AS VARCHAR(MAX))
			EXEC ConvertDeliveryTypeEnum @oldDeliveryTypeEnum, @deliveryTypeName = @oldValue OUT
			EXEC InsertAuditDetail @auditHeaderId, 'DeliveryTypeEnum', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldSiteId AS VARCHAR(MAX))
			EXEC ConvertSiteId @oldSiteId, @siteName = @oldValue OUT
			EXEC InsertAuditDetail @auditHeaderId, 'SiteId', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldStartDate AS VARCHAR(MAX))
			EXEC ConvertDate @oldStartDate, @output = @oldValue OUT
			EXEC InsertAuditDetail @auditHeaderId, 'StartDate', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldEndDate AS VARCHAR(MAX))
			EXEC ConvertDate @oldEndDate, @output = @oldValue OUT
			EXEC InsertAuditDetail @auditHeaderId, 'EndDate', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldIsContract AS VARCHAR(MAX))
			EXEC ConvertBoolean @oldIsContract, @output = @oldValue OUT
			EXEC InsertAuditDetail @auditHeaderId, 'IsContract', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldIsVAP AS VARCHAR(MAX))
			EXEC ConvertBoolean @oldIsVAP, @output = @oldValue OUT
			EXEC InsertAuditDetail @auditHeaderId, 'IsVAP', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldIsGlobalProduct AS VARCHAR(MAX))
			EXEC ConvertBoolean @oldIsGlobalProduct, @output = @oldValue OUT
			EXEC InsertAuditDetail @auditHeaderId, 'IsGlobalProduct', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldIsJIF AS VARCHAR(MAX))
			EXEC ConvertBoolean @oldIsJIF, @output = @oldValue OUT
			EXEC InsertAuditDetail @auditHeaderId, 'IsJIF', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldIsAdjustment AS VARCHAR(MAX))
			EXEC ConvertBoolean @oldIsAdjustment, @output = @oldValue OUT
			EXEC InsertAuditDetail @auditHeaderId, 'IsAdjustment', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldContractText AS VARCHAR(MAX))
			SET @oldValue = @oldValueRaw
			EXEC InsertAuditDetail @auditHeaderId, 'ContractText', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldVAPActivationVolume AS VARCHAR(MAX))
			SET @oldValue = @oldValueRaw
			EXEC InsertAuditDetail @auditHeaderId, 'VAPActivationVolume', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldGlobalRate AS VARCHAR(MAX))
			SET @oldValue = @oldValueRaw
			EXEC InsertAuditDetail @auditHeaderId, 'GlobalRate', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldPaymentFrequencyId AS VARCHAR(MAX))
			EXEC ConvertPaymentFrequencyId @oldPaymentFrequencyId, @paymentFrequencyName = @oldValue OUT
			EXEC InsertAuditDetail @auditHeaderId, 'PaymentFrequencyId', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldRebateTypeId AS VARCHAR(MAX))
			EXEC ConvertRebateTypeId @oldRebateTypeId, @rebateTypeName = @oldValue OUT
			EXEC InsertAuditDetail @auditHeaderId, 'RebateTypeId', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldEstimatedVolume AS VARCHAR(MAX))
			SET @oldValue = @oldValueRaw
			EXEC InsertAuditDetail @auditHeaderId, 'EstimatedVolume', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldFixedRebateValue AS VARCHAR(MAX))
			SET @oldValue = @oldValueRaw
			EXEC InsertAuditDetail @auditHeaderId, 'FixedRebateValue', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			SET @oldValueRaw = CAST(@oldIsAccrued AS VARCHAR(MAX))
			EXEC ConvertBoolean @oldIsAccrued, @output = @oldValue OUT
			EXEC InsertAuditDetail @auditHeaderId, 'IsAccrued', @oldValueRaw, @oldValue, 'NULL', 'NULL'

			FETCH NEXT FROM c2 INTO
				@recordId,
				@rebateId,
				@oldRebateNumber,
				@oldRebateVersion,
				@oldDescription,
				@oldHierarchyId,
				@oldHierarchyEntityId,
				@oldPricingSubGroupId,
				@oldPayeeId,
				@oldDeliveryTypeEnum,
				@oldSiteId,
				@oldStartDate,
				@oldEndDate,
				@oldIsContract,
				@oldIsVAP,
				@oldIsGlobalProduct,
				@oldIsJIF,
				@oldIsAdjustment,
				@oldContractText,
				@oldVAPActivationVolume,
				@oldGlobalRate,
				@oldPaymentFrequencyId,
				@oldRebateTypeId,
				@oldEstimatedVolume,
				@oldFixedRebateValue,
				@oldIsAccrued
		END

		CLOSE c2
		DEALLOCATE c2
	END
END
GO
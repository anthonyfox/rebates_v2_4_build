/****** Object:  Table [dbo].[AuditHeader]    Script Date: 18/05/2016 11:25:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
--========================================================================
--= Create table AuditHeader
--========================================================================

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[AuditHeader](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
	[TableName] [varchar](50) NOT NULL,
	[RebateId] [int] NOT NULL,
	[RecordId] [int] NOT NULL,
	[DatabaseActionEnum] [int] NOT NULL,
 CONSTRAINT [PK_AuditHeader] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[AuditHeader]  WITH CHECK ADD  CONSTRAINT [FK_AuditHeader_REBATE] FOREIGN KEY([RebateId])
REFERENCES [dbo].[REBATE] ([ID])
GO

ALTER TABLE [dbo].[AuditHeader] CHECK CONSTRAINT [FK_AuditHeader_REBATE]
GO

--========================================================================
--= Create table AuditDetail
--========================================================================

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[AuditDetail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AuditHeaderId] [int] NOT NULL,
	[ColumnName] [varchar](50) NOT NULL,
	[OldValueRaw] [varchar](max) NULL,
	[OldValue] [varchar](max) NULL,
	[NewValueRaw] [varchar](max) NULL,
	[NewValue] [varchar](max) NULL,
 CONSTRAINT [PK_AuditDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[AuditDetail]  WITH CHECK ADD  CONSTRAINT [FK_AuditDetail_AuditHeader] FOREIGN KEY([AuditHeaderId])
REFERENCES [dbo].[AuditHeader] ([Id])
GO

ALTER TABLE [dbo].[AuditDetail] CHECK CONSTRAINT [FK_AuditDetail_AuditHeader]
GO



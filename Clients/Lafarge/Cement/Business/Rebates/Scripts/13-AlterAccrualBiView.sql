﻿DROP VIEW [AccrualBIView]
GO

CREATE VIEW [dbo].[AccrualBIView] AS

SELECT
	CASE DivisionAccrual.DivisionId
		WHEN 1 THEN 1200
		ELSE 0000
	END AS COMPANY,
	Site.JdeCode AS SHIPPING_SITE,
	Product.JdeCode AS ITEM,
	Customer.JdeCode AS CUSTOMER_SHIP_TO,
	CASE AccrualBi.DeliveryTypeEnum
		WHEN 1 THEN 'A0'
		WHEN 2 THEN 'C9'
		ELSE 'XX'
	END AS FREIGHT_HANDLING_CODE,
	'REBATE' + CAST(AccrualBi.RebateTypeId AS VARCHAR) AS COMPONENT_TYPE,
	'ACTUAL' AS SCENARIO,
	'GBP' AS CURRENCY,
	'TN' AS UNIT_OF_MEASURE,
	AccrualBi.Period AS PERIOD,
	AccrualBi.Year AS YEAR,
	Period.EndDate AS PERIOD_END_DATE,
	CASE SUM(AccrualBi.SaleQuantity)
		WHEN 0 THEN 0
		ELSE ROUND(SUM(AccrualBi.RebateAmount) / SUM(AccrualBi.SaleQuantity), 2)
	END AS UNIT_COST,
	AccrualBi.UpdateDateTime AS UPDATE_DATETIME

FROM AccrualBI

	INNER JOIN DivisionAccrual ON DivisionAccrual.Id = AccrualBI.DivisionAccrualId
	INNER JOIN Site ON Site.Id = AccrualBI.SiteId
	INNER JOIN Product ON Product.Id = AccrualBI.ProductId
	INNER JOIN Customer ON Customer.Id = AccrualBI.ShipToId
	LEFT OUTER JOIN Period ON Period.Year = AccrualBI.Year AND Period.Period = AccrualBI.Period

GROUP BY
	DivisionAccrual.DivisionId,
	Site.JdeCode,
	Product.JdeCode,
	Customer.JdeCode,
	AccrualBI.DeliveryTypeEnum,
	AccrualBI.RebateTypeId,
	AccrualBI.Year,
	AccrualBI.Period,
	Period.EndDate,
	AccrualBi.UpdateDateTime

GO
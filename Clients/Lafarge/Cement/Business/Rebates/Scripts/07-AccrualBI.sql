﻿IF OBJECT_ID (N'ACCRUALBI', N'U') IS NOT NULL
BEGIN
	DROP TABLE ACCRUALBI
END

CREATE TABLE ACCRUALBI (
	ID					INT IDENTITY(1,1),
	DIVISIONACCRUALID	INT NOT NULL,
	SOLDTOID			INT NOT NULL,
	SITEID				INT NOT NULL,
	SHIPTOID			INT NOT NULL,
	PRODUCTID			INT NOT NULL,
	DELIVERYTYPEENUM	INT NOT NULL,
	REBATETYPEID		INT NOT NULL,
	REBATESTARTDATE		DATETIME NOT NULL,
	REBATEENDDATE		DATETIME NOT NULL,
	YEAR				SMALLINT NOT NULL,
	PERIOD				SMALLINT NOT NULL,
	SALEQUANTITY		DECIMAL(18,2) NOT NULL,
	REBATEAMOUNT		DECIMAL(18,2) NOT NULL,
	UPDATEDATETIME		DATETIME NOT NULL
)

ALTER TABLE ACCRUALBI
ADD CONSTRAINT FK_AccrualBi_DivisionAccrual FOREIGN KEY (DivisionAccrualId) REFERENCES DivisionAccrual (Id)

ALTER TABLE ACCRUALBI
ADD CONSTRAINT FK_AccrualBi_SoldTo FOREIGN KEY (SoldToId) REFERENCES Customer (Id)

ALTER TABLE ACCRUALBI
ADD CONSTRAINT FK_AccrualBi_Site FOREIGN KEY (SiteId) REFERENCES Site (Id)

ALTER TABLE ACCRUALBI
ADD CONSTRAINT FK_AccrualBi_ShipTo FOREIGN KEY (ShipToId) REFERENCES Customer (Id)

ALTER TABLE ACCRUALBI
ADD CONSTRAINT FK_AccrualBi_Product FOREIGN KEY (ProductId) REFERENCES Product (Id)
GO

ALTER TABLE ACCRUALBI
ADD CONSTRAINT FK_AccrualBi_RebateType FOREIGN KEY (RebateTypeId) REFERENCES RebateType (Id)
GO

-- Period Table
IF OBJECT_ID (N'PERIOD', N'U') IS NOT NULL
BEGIN
	DROP TABLE PERIOD
END

CREATE TABLE PERIOD
(
	ID		INT IDENTITY(1,1),
	YEAR	SMALLINT NOT NULL,
	PERIOD	SMALLINT NOT NULL,
	ENDDATE	DATE NOT NULL
)

CREATE UNIQUE INDEX UX_Period_1 ON Period ( YEAR, PERIOD )
GO

-- Create View
IF OBJECT_ID (N'AccrualBIView', N'V') IS NOT NULL
BEGIN
	DROP VIEW AccrualBIView
END
GO

CREATE VIEW [dbo].[AccrualBIView] AS

SELECT
	CASE DivisionAccrual.DivisionId
		WHEN 1 THEN 7070
		ELSE 0000
	END AS COMPANY,
	Site.JdeCode AS SHIPPING_SITE,
	Product.JdeCode AS ITEM,
	Customer.JdeCode AS CUSTOMER_SHIP_TO,
	CASE AccrualBi.DeliveryTypeEnum
		WHEN 1 THEN 'A0'
		WHEN 2 THEN 'C9'
		ELSE 'XX'
	END AS FREIGHT_HANDLING_CODE,
	'REBATE' + CAST(AccrualBi.RebateTypeId AS VARCHAR) AS COMPONENT_TYPE,
	'ACTUAL' AS SCENARIO,
	'GBP' AS CURRENCY,
	'TN' AS UNIT_OF_MEASURE,
	AccrualBi.RebateStartDate AS START_DATE,
	AccrualBi.RebateEndDate AS END_DATE,
	Period.EndDate AS PERIOD_END_DATE,
	CASE SUM(AccrualBi.SaleQuantity)
		WHEN 0 THEN 0
		ELSE ROUND(SUM(AccrualBi.RebateAmount) / SUM(AccrualBi.SaleQuantity), 2)
	END AS UNIT_COST,
	AccrualBi.UpdateDateTime AS UPDATE_DATETIME

FROM AccrualBI

	INNER JOIN DivisionAccrual ON DivisionAccrual.Id = AccrualBI.DivisionAccrualId
	INNER JOIN Site ON Site.Id = AccrualBI.SiteId
	INNER JOIN Product ON Product.Id = AccrualBI.ProductId
	INNER JOIN Customer ON Customer.Id = AccrualBI.ShipToId
	LEFT OUTER JOIN Period ON Period.Year = AccrualBI.Year AND Period.Period = AccrualBI.Period

GROUP BY
	DivisionAccrual.DivisionId,
	Site.JdeCode,
	Product.JdeCode,
	Customer.JdeCode,
	AccrualBI.DeliveryTypeEnum,
	AccrualBI.RebateTypeId,
	AccrualBI.RebateStartDate,
	AccrualBI.RebateEndDate,
	Period.EndDate,
	AccrualBi.UpdateDateTime
GO

-- Create View
IF OBJECT_ID (N'[AccrualBIViewShortColumns]', N'V') IS NOT NULL
BEGIN
	DROP VIEW [AccrualBIViewShortColumns]
END
GO
CREATE VIEW [dbo].[AccrualBIViewShortColumns]
AS
SELECT
	CASE DivisionAccrual.DivisionId
		WHEN 1 THEN 7070
		ELSE 0000
	END AS COMPANY,
	dbo.Site.JdeCode AS SHIPPING_SITE,
	dbo.Product.JdeCode AS ITEM, 
    dbo.Customer.JdeCode AS CUSTOMER_SHIP_TO, 
    CASE AccrualBi.DeliveryTypeEnum
		WHEN 1 THEN 'A0'
		WHEN 2 THEN 'C9'
		ELSE 'XX'
	END AS FREIGHT_HANDLING_CODE,
	'REBATE' + CONVERT(VARCHAR, AccrualBi.RebateTypeId) AS COMPONENT_TYPE,
	dbo.RebateType.Name AS REBATE_TYPE_NAME,
	'ACTUAL' AS SCENARIO,
	'GBP' AS CURRENCY, 
    'TN' AS UNIT_OF_MEASURE, 
	dbo.AccrualBi.RebateStartDate AS RSD, 
	dbo.AccrualBi.RebateEndDate AS RED, 
    dbo.Period.EndDate AS PERIOD_END_DATE,
	CASE SUM(AccrualBi.SaleQuantity)
		WHEN 0 THEN 0
		ELSE ROUND(SUM(AccrualBi.RebateAmount) / SUM(AccrualBi.SaleQuantity), 2)
	END AS UNIT_COST,
    dbo.AccrualBi.UpdateDateTime AS UPDATE_DATETIME
FROM
    dbo.ACCRUALBI
	INNER JOIN dbo.DivisionAccrual ON dbo.DivisionAccrual.Id = dbo.AccrualBi.DivisionAccrualId 
	INNER JOIN dbo.Site ON dbo.Site.Id = dbo.AccrualBi.SiteId 
	INNER JOIN dbo.Product ON dbo.Product.Id = dbo.AccrualBi.ProductId 
	INNER JOIN dbo.Customer ON dbo.Customer.Id = dbo.AccrualBi.ShipToId 
	LEFT OUTER JOIN dbo.Period ON dbo.Period.Year = dbo.AccrualBi.Year AND dbo.Period.Period = dbo.AccrualBi.Period 
	INNER JOIN dbo.RebateType ON dbo.RebateType.Id = dbo.AccrualBi.RebateTypeId
GROUP BY 
	dbo.DivisionAccrual.DivisionId, 
	dbo.Site.JdeCode, 
	dbo.Product.JdeCode, 
	dbo.Customer.JdeCode, 
	dbo.AccrualBi.DeliveryTypeEnum, 
	dbo.AccrualBi.RebateTypeId, 
	dbo.RebateType.Name,
	dbo.AccrualBi.RebateStartDate,
	dbo.AccrualBi.RebateEndDate,
	dbo.Period.EndDate, 
	dbo.AccrualBi.UpdateDateTime
GO

-- Create View
IF OBJECT_ID (N'AccrualSAPView', N'V') IS NOT NULL
BEGIN
	DROP VIEW AccrualSAPView
END
GO

create view [dbo].[AccrualSAPView] as

select
	case DivisionAccrual.DivisionId when 1 then 1200 else 0000 end as COMPANY,
	Site.JdeCode as SHIPPING_SITE,
	Product.JdeCode as ITEM,
	ShipTo.JdeCode as CUSTOMER_SHIP_TO,
	SoldTo.JdeCode as CUSTOMER_SOLD_TO,
	ShipToGroup.JdeCode as CUSTOMER_SHIP_TO_MAJORGRP,
	SoldToGroup.JdeCode as CUSTOMER_SOLD_TO_MAJORGRP,
	case AccrualBi.DeliveryTypeEnum when 1 then 'A0' when 2 then 'C9' else 'XX' end as DELIVERY_TYPE,
	'REBATE' + CAST(AccrualBi.RebateTypeId AS VARCHAR) as COMPONENT_TYPE,
	'ACTUAL' as SCENARIO,
	'GBP' as CURRENCY,
	'TN' as UNIT_OF_MEASURE,
	SUM(AccrualBi.SaleQuantity) as ACTUAL_QUANTITY,
	SUM(AccrualBi.RebateAmount) as ACCRUAL_VALUE,
	CAST(DivisionAccrual.Year AS VARCHAR(4)) + RIGHT('00' + CAST(DIVISIONACCRUAL.Period AS VARCHAR(2)), 2) as ACCRUAL_PERIOD

from AccrualBI

	inner join DivisionAccrual on DivisionAccrual.Id = AccrualBI.DivisionAccrualId
	inner join Site on Site.Id = AccrualBI.SiteId
	inner join Product on Product.Id = AccrualBI.ProductId
	inner join Customer ShipTo on ShipTo.Id = AccrualBI.ShipToId
	inner join Customer SoldTo on SoldTo.Id = AccrualBI.SoldToId
	inner join Customer ShipToSoldTo on ShipToSoldTo.Id = ShipTo.SoldToId
	left outer join CustomerGroup ShipToGroup on ShipToGroup.Id = ShipToSoldTo.CUSTOMERGROUPID
	left outer join CustomerGroup SoldToGroup on SoldToGroup.Id = SoldTo.CUSTOMERGROUPID
	left outer join Period on Period.Year = AccrualBI.Year and Period.Period = AccrualBI.Period

group by
	DivisionAccrual.DivisionId,
	Site.JdeCode,
	Product.JdeCode,
	ShipTo.JdeCode,
	SoldTo.JdeCode,
	ShipToGroup.JdeCode,
	SoldToGroup.JdeCode,
	AccrualBI.DeliveryTypeEnum,
	AccrualBI.RebateTypeId,
	AccrualBI.RebateStartDate,
	AccrualBI.RebateEndDate,
	Period.EndDate,
	AccrualBi.UpdateDateTime,
	DivisionAccrual.Year,
	DivisionAccrual.Period
GO


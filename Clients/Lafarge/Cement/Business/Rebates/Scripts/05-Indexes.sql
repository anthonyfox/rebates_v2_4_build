﻿create        index IX_ACCRUAL_1 on ACCRUAL ( CREATEYEAR, CREATEPERIOD )
create        index IX_ACCRUAL_2 on ACCRUAL ( ACTUALYEAR, ACTUALPERIOD )

declare @jdecode int
declare @count int
declare @id int

declare cDuplicate cursor fast_forward for
	select jdecode, count(*) 
	from address 
	group by jdecode 
	having count(*) > 1

open cDuplicate
fetch cDuplicate into @jdecode, @count

while @@FETCH_STATUS = 0
begin
	set @id = ( select top 1 id from address where jdecode = @jdecode )

	update customer
	set addressid = @id
	where addressid in ( select id from address where jdecode = @jdecode )

	update payee
	set addressid = @id
	where addressid in ( select id from address where jdecode = @jdecode )

	delete from address
	where JdeCode = @jdecode
	and id != @id

	fetch cDuplicate into @jdecode, @count
end

close cDuplicate
deallocate cDuplicate

create unique index UX_ADDRESS_1 on ADDRESS ( JDECODE )
create unique index UX_ATTRIBUTE_1 on ATTRIBUTE ( JDECODE, LEVELNUMBER, DIVISIONID )
create unique index UX_CUSTOMER_1 on CUSTOMER ( JDECODE )
create unique index UX_CUSTOMERGROUP_1 on CUSTOMERGROUP ( JDECODE, DIVISIONID )
create unique index UX_DELIVERYWINDOW_1 on DELIVERYWINDOW ( JDECODE, DIVISIONID )
create        index UX_INVOICELINE_1 on INVOICELINE ( DOCUMENTNUMBER, DOCUMENTTYPE, LINENUMBER, DIVISIONID, ORDERNUMBER, ORDERLINENUMBER, ORDERDOCTYPE )
create unique index UX_LEADTIME_1 on LEADTIME ( JDECODE, DIVISIONID )
create unique index UX_PAYEE_1 on PAYEE ( JDECODE, DIVISIONID )
create        index IX_PAYMENT_1 on PAYMENT ( CREATEYEAR, CREATEPERIOD )
create        index IX_PAYMENT_2 on PAYMENT ( ACTUALYEAR, ACTUALPERIOD )
create        index IX_PAYMENT_3 on PAYMENT ( REBATEID )
create        index IX_PAYMENT_4 on PAYMENT ( PAYMENT_PAYMENTBREAKDOWNID )
create        index IX_PAYMENTBREAKDOWN_1 on PAYMENT ( PAYMENT_PAYMENTBREAKDOWNID )
create unique index UX_PRICINGSUBGROUP_1 on PAYEE ( JDECODE, DIVISIONID )
create unique index UX_PRODUCT_1 on PRODUCT ( JDECODE, DIVISIONID )
create	      index IX_REBATE_1 on REBATE ( REBATEVERSION, REBATENUMBER )

declare @sitejdecode varchar(10)
declare @siteid int

declare cSiteDuplicate cursor fast_forward for
	select jdecode, count(*) 
	from site 
	group by jdecode 
	having count(*) > 1

open cSiteDuplicate
fetch cSiteDuplicate into @sitejdecode, @count

while @@FETCH_STATUS = 0
begin
	set @siteid = ( select top 1 id from site where jdecode = @sitejdecode )

	update accrualjde
	set siteid = @siteid
	where siteid in ( select id from site where jdecode = @sitejdecode )

	update invoiceline
	set siteid = @siteid
	where siteid in ( select id from site where jdecode = @sitejdecode )

	delete from site
	where jdecode = @sitejdecode
	and id != @siteid

	fetch cSiteDuplicate into @sitejdecode, @count
end

close cSiteDuplicate
deallocate cSiteDuplicate

create unique index UX_SITE_1 on SITE ( JDECODE, DIVISIONID )
create unique index UX_USERS_1 on USERS ( CODE )
﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class RebateDeliveryWindow : SqlServerEntity, ICloneable
	{
		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(RebateDeliveryWindowColumn.Id, true, true), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;

				if (this.ExistsOnDatabase)
				{
					SetColumnDirty(RebateDeliveryWindowProperty.Id);
				}
			}
		}
		#endregion

		#region PROPERTY: RebateId
		private Int32 _rebateId;
		[EntityColumn(RebateDeliveryWindowColumn.RebateId), Browsable(false)]
		public Int32 RebateId
		{
			get { return _rebateId; }
			set
			{
				_rebateId = value;
				SetColumnDirty(RebateDeliveryWindowProperty.RebateId);
			}
		}
		#endregion

		#region PROPERTY: DeliveryWindowId
		private Int32 _deliveryWindowId;
		[EntityColumn(RebateDeliveryWindowColumn.DeliveryWindowId), Browsable(false)]
		public Int32 DeliveryWindowId
		{
			get { return _deliveryWindowId; }
			set
			{
				_deliveryWindowId = value;
				SetColumnDirty(RebateDeliveryWindowProperty.DeliveryWindowId);
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: DeliveryWindowJDECode
		private String _deliveryWindowJDECode;
		[EntityColumn(DeliveryWindowColumn.JDECode, DBRebates.DeliveryWindow, RebateDeliveryWindowProperty.DeliveryWindowJDECode), DisplayName("JDE Code")]
		public String DeliveryWindowJDECode
		{
			get { return _deliveryWindowJDECode; }
			set { _deliveryWindowJDECode = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: DeliveryWindowName
		private String _deliveryWindowName;
		[EntityColumn(DeliveryWindowColumn.Name, DBRebates.DeliveryWindow, RebateDeliveryWindowProperty.DeliveryWindowName), DisplayName("Name")]
		public String DeliveryWindowName
		{
			get { return _deliveryWindowName; }
			set { _deliveryWindowName = value; }
		}
		#endregion

		#region PROPERTY: PackingTypeEnum
		private PackingTypeEnum _packingTypeEnum;
		[EntityColumn(RebateDeliveryWindowColumn.PackingTypeEnum)]
		[EntityColumnConversion("DBInt16ToPackingTypeEnum", "PackingTypeEnumToDBInt16", "PackingTypeEnumConvertor", "Evolve.Clients.Lafarge.Cement.Business.Rebates", "Evolve.Clients.Lafarge.Cement.Business")]
		[DisplayName("Packing Type")]
		public PackingTypeEnum PackingTypeEnum
		{
			get { return _packingTypeEnum; }
			set
			{
				_packingTypeEnum = value;
				SetColumnDirty(RebateDeliveryWindowProperty.PackingTypeEnum);
			}
		}
		#endregion

		#region PROPERTY: Rate
		private Decimal? _rate;
		[EntityColumn(RebateDeliveryWindowColumn.Rate), DisplayName("Rate")]
		public Decimal? Rate
		{
			get { return _rate; }
			set
			{
				_rate = value;
				SetColumnDirty(RebateDeliveryWindowProperty.Rate);
			}
		}
		#endregion

		#endregion

		#region Constructors

		public RebateDeliveryWindow()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.RebateDeliveryWindow);

			JoinTable(DBRebates.DatabaseName, DBRebates.DeliveryWindow, DBRebates.DeliveryWindow + "." + DeliveryWindowColumn.Id + " = " + DBRebates.RebateDeliveryWindow + "." + RebateDeliveryWindowColumn.DeliveryWindowId);

			OrderBy = "{" + RebateDeliveryWindowProperty.DeliveryWindowId + "}";
		}

		#endregion

		#region ICloneable

		public object Clone()
		{
			return this.MemberwiseClone();
		}

		#endregion

		#region Business Logic

		public static Int32 DeleteRebateDeliveryWindows(Int32 rebateId)
		{
			RebateDeliveryWindow rebateDeliveryWindow = new RebateDeliveryWindow();
			rebateDeliveryWindow.RebateId = rebateId;

			rebateDeliveryWindow.AddReadFilter(RebateDeliveryWindowProperty.RebateId, "=");

			return rebateDeliveryWindow.DeleteForReadFilter();
		}


		#region Validation

		#region Validate: Rate 
		public static String ValidateRate(String rate)
		{
			const Decimal MAX_RATE = 10000000; // 10 Million
			const Decimal MIN_RATE = 0;

			String errorText = "";
			Decimal value;

			if (rate.Trim().Length > 0)
			{
				if (Decimal.TryParse(rate, out value))
				{
					if (value == 0)
					{
						errorText = "Rate cannot be zero";
					}
					else
					{
						if (value < MIN_RATE || value > MAX_RATE)
						{
							errorText = "Rate must be between " + MIN_RATE.ToString() + " and " + MAX_RATE.ToString();
						}
					}
				}
			}
			else
			{
				errorText = "A valid rate must be entered";
			}

			return errorText;
		}
		#endregion

		#endregion

		#endregion
	}

	public class RebateDeliveryWindowColumn
	{
		public const string Id = "Id";
		public const string RebateId = "RebateId";
		public const string DeliveryWindowId = "DeliveryWindowId";
		public const string PackingTypeEnum = "PackingTypeEnum";
		public const string Rate = "Rate";
		public const string AmendTime = "AmendTime";
		public const string AmendUserId = "AmendUserId";
	}

	public class RebateDeliveryWindowProperty
	{
		public const string Id = "Id";
		public const string RebateId = "RebateId";
		public const string DeliveryWindowId = "DeliveryWindowId";
		public const string PackingTypeEnum = "PackingTypeEnum";
		public const string DeliveryWindowJDECode = "DeliveryWindowJDECode";
		public const string DeliveryWindowName = "DeliveryWindowName";
		public const string Rate = "Rate";
		public const string AmendTime = "AmendTime";
		public const string AmendUserId = "AmendUserId";
	}
}

﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class Payee : SqlServerEntity
	{
		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(PayeeColumn.Id, true, true), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;
				SetColumnDirty(PayeeProperty.Id);
			}
		}
		#endregion

		#region RELATED PROPERTY: DivisionId
		private Int32 _divisionId;
		[EntityColumn(PayeeColumn.DivisionId)]
		[Browsable(false)]
		public Int32 DivisionId
		{
			get { return _divisionId; }
			set
			{
				_divisionId = value;
				SetColumnDirty(PayeeProperty.DivisionId);
			}
		}
		#endregion

		#region PROPERTY: JDECode
		private Int32 _jdeCode;
		[EntityColumn(PayeeColumn.JDECode), DisplayName("Account")]
		public Int32 JDECode
		{
			get { return _jdeCode; }
			set
			{
				_jdeCode = value;
				SetColumnDirty(PayeeProperty.JDECode);
			}
		}
		#endregion

		#region PROPERTY: Name
		private String _name;
		[EntityColumn(PayeeColumn.Name)]
		public String Name
		{
			get { return _name; }
			set
			{
				_name = value;
				SetColumnDirty(PayeeProperty.Name);
			}
		}
		#endregion

		#region PROPERTY: AddressId
		private Int32 _addressId;
		[EntityColumn(PayeeColumn.AddressId), Browsable(false)]
		public Int32 AddressId
		{
			get { return _addressId; }
			set
			{
				_addressId = value;
				SetColumnDirty(PayeeProperty.AddressId);
			}
		}
		#endregion

		#region PROPERTY: TaxNumber
		private String _taxNumber;
		[EntityColumn(PayeeColumn.TaxNumber)]
		[DisplayName("Tax Number")]
		public String TaxNumber
		{
			get { return _taxNumber; }
			set
			{
				_taxNumber = value;
				SetColumnDirty(PayeeProperty.TaxNumber);
			}
		}
		#endregion

		#region PROPERTY: InterfaceTime
		private DateTime _interfaceTime;
		[EntityColumn(PayeeColumn.InterfaceTime), Browsable(false)]
		public DateTime InterfaceTime
		{
			get { return _interfaceTime; }
			set
			{
				_interfaceTime = value;
				SetColumnDirty(PayeeProperty.InterfaceTime);
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: Address1
		private String _address1;
		[EntityColumn(AddressColumn.Address1, DBRebates.Address, PayeeProperty.Address1), DisplayName("Address 1")]
		public String Address1
		{
			get { return _address1; }
			set
			{
				_address1 = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: Address2
		private String _address2;
		[EntityColumn(AddressColumn.Address2, DBRebates.Address, PayeeProperty.Address2), DisplayName("Address 2")]
		public String Address2
		{
			get { return _address2; }
			set
			{
				_address2 = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: Address3
		private String _address3;
		[EntityColumn(AddressColumn.Address3, DBRebates.Address, PayeeProperty.Address3), DisplayName("Address 3")]
		public String Address3
		{
			get { return _address3; }
			set
			{
				_address3 = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: Address4
		private String _address4;
		[EntityColumn(AddressColumn.Address4, DBRebates.Address, PayeeProperty.Address4), DisplayName("Address 4")]
		public String Address4
		{
			get { return _address4; }
			set
			{
				_address4 = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: City
		private String _city;
		[EntityColumn(AddressColumn.City, DBRebates.Address, PayeeProperty.City), DisplayName("City")]
		public String City
		{
			get { return _city; }
			set
			{
				_city = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: County
		private String _county;
		[EntityColumn(AddressColumn.County, DBRebates.Address, PayeeProperty.County), DisplayName("County")]
		public String County
		{
			get { return _county; }
			set
			{
				_county = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: PostCode
		private String _postCode;
		[EntityColumn(AddressColumn.PostCode, DBRebates.Address, PayeeProperty.PostCode), DisplayName("Post Code")]
		public String PostCode
		{
			get { return _postCode; }
			set
			{
				_postCode = value;
			}
		}
		#endregion

		#endregion

		#region Constructors

		public Payee()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.Payee);
			JoinTable(DBRebates.DatabaseName, DBRebates.Address, DBRebates.Address + "." + AddressColumn.Id + " = " + DBRebates.Payee + "." + PayeeProperty.AddressId);
		}

		#endregion

		#region Business Logic

		/// <summary>
		/// Search for payee's matching specified criteria
		/// </summary>
		public ExtendedBindingList<Payee> Search(String name, String address1, String address2, String address3, String address4, String city, String county, String postCode, String taxNumber, Int32 rowLimit, out Boolean isRowLimitExceeded)
		{
			String filter = "";

			filter += SqlHelper.FilterBuilder(filter, "{" + PayeeProperty.DivisionId + "} = " + _divisionId.ToString());

			if (name.Length > 0)
			{
				filter = SqlHelper.FilterBuilder(filter, "{" + PayeeProperty.Name + "} " + this.WildcardKeyword() + " '" + name.Replace('*', this.WildcardCharacter()) + this.WildcardCharacter() + "'");
			}

			if (address1.Length > 0)
			{
				filter = SqlHelper.FilterBuilder(filter, "{" + PayeeProperty.Address1 + "} " + this.WildcardKeyword() + " '" + address1.Replace('*', this.WildcardCharacter()) + this.WildcardCharacter() + "'");
			}

			if (address2.Length > 0)
			{
				filter = SqlHelper.FilterBuilder(filter, "{" + PayeeProperty.Address2 + "} " + this.WildcardKeyword() + " '" + address2.Replace('*', this.WildcardCharacter()) + this.WildcardCharacter() + "'");
			}

			if (address3.Length > 0)
			{
				filter = SqlHelper.FilterBuilder(filter, "{" + PayeeProperty.Address3 + "} " + this.WildcardKeyword() + " '" + address3.Replace('*', this.WildcardCharacter()) + this.WildcardCharacter() + "'");
			}

			if (address4.Length > 0)
			{
				filter = SqlHelper.FilterBuilder(filter, "{" + PayeeProperty.Address4 + "} " + this.WildcardKeyword() + " '" + address4.Replace('*', this.WildcardCharacter()) + this.WildcardCharacter() + "'");
			}

			if (city.Length > 0)
			{
				filter = SqlHelper.FilterBuilder(filter, "{" + PayeeProperty.City + "} " + this.WildcardKeyword() + " '" + city.Replace('*', this.WildcardCharacter()) + this.WildcardCharacter() + "'");
			}

			if (county.Length > 0)
			{
				filter = SqlHelper.FilterBuilder(filter, "{" + PayeeProperty.County + "} " + this.WildcardKeyword() + " '" + county.Replace('*', this.WildcardCharacter()) + this.WildcardCharacter() + "'");
			}

			if (postCode.Length > 0)
			{
				filter = SqlHelper.FilterBuilder(filter, "{" + PayeeProperty.PostCode + "} " + this.WildcardKeyword() + " '" + postCode.Replace('*', this.WildcardCharacter()) + this.WildcardCharacter() + "'");
			}

			if (taxNumber.Length > 0)
			{
				filter = SqlHelper.FilterBuilder(filter, "{" + PayeeProperty.TaxNumber + "} " + this.WildcardKeyword() + " '" + taxNumber.Replace('*', this.WildcardCharacter()) + this.WildcardCharacter() + "'");
			}

			Payee payee = new Payee();
			payee.ReadFilter = filter;
			ExtendedBindingList<Payee> payees = (ExtendedBindingList<Payee>) payee.Read(rowLimit, out isRowLimitExceeded);

			return payees;
		}

		#endregion
	}

	public class PayeeColumn
	{
		public const string Id = "Id";
		public const string DivisionId = "DivisionId";
		public const string JDECode = "JDECode";
		public const string Name = "Name";
		public const string AddressId = "AddressId";
		public const string TaxNumber = "TaxNumber";
		public const string InterfaceTime = "InterfaceTime";
	}

	public class PayeeProperty
	{
		public const string Id = "Id";
		public const string DivisionId = "DivisionId";
		public const string JDECode = "JDECode";
		public const string Name = "Name";
		public const string AddressId = "AddressId";
		public const string TaxNumber = "TaxNumber";
		public const string InterfaceTime = "InterfaceTime";
		public const string Address1 = "Address1";
		public const string Address2 = "Address2";
		public const string Address3 = "Address3";
		public const string Address4 = "Address4";
		public const string City = "City";
		public const string County = "County";
		public const string PostCode = "PostCode";
	}
}

﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class Address : SqlServerEntity
	{
		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(AddressColumn.Id, true, true), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;
				SetColumnDirty(AddressProperty.Id);
			}
		}
		#endregion

		#region PROPERTY: JDECode
		private string _jdeCode;
		[EntityColumn(AddressColumn.JDECode, true), Browsable(false)]
		public string JDECode
		{
			get { return _jdeCode; }
			set
			{
				_jdeCode = value;
				SetColumnDirty(AddressProperty.JDECode);
			}
		}
		#endregion

		#region PROPERTY: Address1
		private String _address1;
		[EntityColumn(AddressColumn.Address1), DisplayName("Address 1")]
		public String Address1
		{
			get { return _address1; }
			set
			{
				_address1 = value;
				SetColumnDirty(AddressProperty.Address1);
			}
		}
		#endregion

		#region PROPERTY: Address2
		private String _address2;
		[EntityColumn(AddressColumn.Address2), DisplayName("Address 2")]
		public String Address2
		{
			get { return _address2; }
			set
			{
				_address2 = value;
				SetColumnDirty(AddressProperty.Address2);
			}
		}
		#endregion

		#region PROPERTY: Address3
		private String _address3;
		[EntityColumn(AddressColumn.Address3), DisplayName("Address 3")]
		public String Address3
		{
			get { return _address3; }
			set
			{
				_address3 = value;
				SetColumnDirty(AddressProperty.Address3);
			}
		}
		#endregion

		#region PROPERTY: Address4
		private String _address4;
		[EntityColumn(AddressColumn.Address4), DisplayName("Address 4")]
		public String Address4
		{
			get { return _address4; }
			set
			{
				_address4 = value;
				SetColumnDirty(AddressProperty.Address4);
			}
		}
		#endregion

		#region PROPERTY: City
		private String _city;
		[EntityColumn(AddressColumn.City), DisplayName("City")]
		public String City
		{
			get { return _city; }
			set
			{
				_city = value;
				SetColumnDirty(AddressProperty.City);
			}
		}
		#endregion

		#region PROPERTY: County
		private String _county;
		[EntityColumn(AddressColumn.County), DisplayName("County")]
		public String County
		{
			get { return _county; }
			set
			{
				_county = value;
				SetColumnDirty(AddressProperty.County);
			}
		}
		#endregion

		#region PROPERTY: PostCode
		private String _postCode;
		[EntityColumn(AddressColumn.PostCode), DisplayName("Post Code")]
		public String PostCode
		{
			get { return _postCode; }
			set
			{
				_postCode = value;
				SetColumnDirty(AddressProperty.PostCode);
			}
		}
		#endregion

		#endregion

		#region Constructors

		public Address()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.Address);
		}

		#endregion
	}

	public class AddressColumn
	{
		public const string Id = "Id";
		public const string JDECode = "JDECode";
		public const string Address1 = "Address1";
		public const string Address2 = "Address2";
		public const string Address3 = "Address3";
		public const string Address4 = "Address4";
		public const string City ="City";
		public const string County = "County";
		public const string PostCode = "PostCode";
	}

	public class AddressProperty
	{
		public const string Id = "Id";
		public const string JDECode = "JDECode";
		public const string Address1 = "Address1";
		public const string Address2 = "Address2";
		public const string Address3 = "Address3";
		public const string Address4 = "Address4";
		public const string City = "City";
		public const string County = "County";
		public const string PostCode = "PostCode";
	}
}


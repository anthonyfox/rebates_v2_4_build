﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class TaxCode : SqlServerEntity, ICloneable
	{
		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(TaxCodeColumn.Id, true, true), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;
				SetColumnDirty(TaxCodeProperty.Id);
			}
		}
		#endregion

		#region PROPERTY: Code
		private String _code;
		[EntityColumn(TaxCodeColumn.Code)]
		public String Code
		{
			get { return _code; }
			set
			{
				_code = value;
				SetColumnDirty(TaxCodeProperty.Code);
			}
		}
		#endregion

		#region PROPERTY: IsZeroRate
		private Boolean _isZeroRate;
		[EntityColumn(TaxCodeColumn.IsZeroRate)]
		[DisplayName("Is Zero Rate")]
		public Boolean IsZeroRate
		{
			get { return _isZeroRate; }
			set
			{
				_isZeroRate = value;
				SetColumnDirty(TaxCodeProperty.IsZeroRate);
			}
		}
		#endregion

		[Browsable(false)]
		public Boolean CanDelete
		{
			get
			{
				TaxRate taxRate = new TaxRate();
				taxRate.TaxCodeId = this.Id;
				return taxRate.Read().Count == 0;
			}
		}

		#region Constructors

		public TaxCode()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.TaxCode);
		}

		#endregion

		#region ICloneable

		public Object Clone()
		{
			return this.MemberwiseClone();
		}

		#endregion
	}

	public class TaxCodeColumn
	{
		public const string Id = "Id";
		public const string Code = "Code";
		public const string IsZeroRate = "IsZeroRate";
	}

	public class TaxCodeProperty
	{
		public const string Id = "Id";
		public const string Code = "Code";
		public const string IsZeroRate = "IsZeroRate";
	}
}

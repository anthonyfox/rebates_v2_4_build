﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class UserDivision : SqlServerEntity
	{
		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(UserDivisionColumn.Id, true, true), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;
				SetColumnDirty(UserDivisionProperty.Id);
			}
		}
		#endregion

		#region PROPERTY: UserId
		private Int32 _userId;
		[EntityColumn(UserDivisionColumn.UserId), Browsable(false)]
		public Int32 UserId
		{
			get { return _userId; }
			set
			{
				_userId = value;
				SetColumnDirty(UserDivisionProperty.UserId);
			}
		}
		#endregion

		#region PROPERTY: DivisionId
		private Int32 _divisionId;
		[EntityColumn(UserDivisionColumn.DivisionId), Browsable(false)]
		public Int32 DivisionId
		{
			get { return _divisionId; }
			set
			{
				_divisionId = value;
				SetColumnDirty(UserDivisionProperty.DivisionId);
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: DivisionName
		private String _divisionName;
		[EntityColumn(DivisionColumn.Name, DBRebates.Division, UserDivisionProperty.DivisionName)]
		public String DivisionName
		{
			get { return _divisionName; }
			set
			{
				_divisionName = value;
			}
		}
		#endregion

		#region RELATED ENTITY: Division

		private Division _division = null;
		public Division Division
		{
			get
			{
				if (_division == null && this.DivisionId > 0)
				{
					_division = new Division();
					_division.Id = this.DivisionId;
					_division.ReadCurrent();
				}

				return _division;
			}
		}

		#endregion

		#endregion

		#region Constructors

		public UserDivision()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.UserDivision);
			JoinTable(DBRebates.DatabaseName, DBRebates.Division, DBRebates.Division + "." + DivisionColumn.Id + " = " + DBRebates.UserDivision + "." + UserDivisionColumn.DivisionId);
			OrderBy =
				"{" + UserDivisionProperty.DivisionName + "}";
		}

		#endregion
	}

	public class UserDivisionColumn
	{
		public const String Id = "Id";
		public const String UserId = "UserId";
		public const String DivisionId = "DivisionId";
	}

	public class UserDivisionProperty
	{
		public const String Id = "Id";
		public const String UserId = "UserId";
		public const String DivisionId = "DivisionId";
		public const String DivisionName = "DivisionName";
	}

}

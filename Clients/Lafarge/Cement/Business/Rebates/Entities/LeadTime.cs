﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class LeadTime : SqlServerEntity, IRebateExtension
	{
		[Browsable(false)]
		public String Title { get { return "Lead Time"; } }
		[Browsable(false)]
		public String EntityName { get { return "InvoiceLine"; } }
		[Browsable(false)]
		public String EntityTitle { get { return "Invoice Line"; } }
		[Browsable(false)]
		public String PropertyName { get { return InvoiceLineProperty.LeadTimeId; } }
		[Browsable(false)]
		public String RebateEntityName { get { return DBRebates.RebateLeadTime; } }
		[Browsable(false)]
		public String RebateEntityTitle { get { return "Rebate"; } }
		[Browsable(false)]
		public String RebatePropertyName { get { return RebateLeadTimeProperty.LeadTimeId; } }
		[Browsable(false)]
		public Int32 TextBoxWidth { get { return 100; } }
		[Browsable(false)]
		public Int32 MaxLength { get { return 40; } }

		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(LeadTimeColumn.Id, true, true), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;
				SetColumnDirty(LeadTimeProperty.Id);
			}
		}
		#endregion

		#region PROPERTY: DivisionId
		private Int32 _divisionId;
		[EntityColumn(LeadTimeColumn.DivisionId), Browsable(false)]
		public Int32 DivisionId
		{
			get { return _divisionId; }
			set
			{
				_divisionId = value;
				SetColumnDirty(LeadTimeProperty.DivisionId);
			}
		}
		#endregion

		#region PROPERTY: JDECode
		private String _jdeCode;
		[EntityColumn(LeadTimeColumn.JDECode)]
		[DisplayName("JDE Code")]
		public String JDECode
		{
			get { return _jdeCode; }
			set
			{
				_jdeCode = value;
				SetColumnDirty(LeadTimeProperty.JDECode);
			}
		}
		#endregion

		#region PROPERTY: Name
		private String _name;
		[EntityColumn(LeadTimeColumn.Name)]
		public String Name
		{
			get { return _name; }
			set
			{
				_name = value;
				SetColumnDirty(LeadTimeProperty.Name);
			}
		}
		#endregion

		#region PROPERTY: CreateTime
		private DateTime _createTime;
		[EntityColumn(LeadTimeColumn.CreateTime, DBDateTime.Year, DBDateTime.Minute)]
		[Browsable(false)]
		public DateTime CreateTime
		{
			get { return _createTime; }
			set
			{
				_createTime = value;
				SetColumnDirty(LeadTimeProperty.CreateTime);
			}
		}
		#endregion

		#region PROPERTY: CreateUserId
		private Int32 _createUserId;
		[EntityColumn(LeadTimeColumn.CreateUserId), Browsable(false)]
		public Int32 CreateUserId
		{
			get { return _createUserId; }
			set
			{
				_createUserId = value;
				SetColumnDirty(LeadTimeProperty.CreateUserId);
			}
		}
		#endregion

		#region PROPERTY: UpdateTime
		private DateTime _updateTime;
		[EntityColumn(LeadTimeColumn.UpdateTime, DBDateTime.Year, DBDateTime.Minute)]
		[Browsable(false)]
		public DateTime UpdateTime
		{
			get { return _updateTime; }
			set
			{
				_updateTime = value;
				SetColumnDirty(LeadTimeProperty.UpdateTime);
			}
		}
		#endregion

		#region PROPERTY: UpdateUserId
		private Int32? _updateUserId;
		[EntityColumn(LeadTimeColumn.UpdateUserId), Browsable(false)]
		public Int32? UpdateUserId
		{
			get { return _updateUserId; }
			set
			{
				_updateUserId = value;
				SetColumnDirty(LeadTimeProperty.UpdateUserId);
			}
		}
		#endregion

		#region PROPERTY: JDECodeName
		public String JDECodeName
		{
			get { return _jdeCode + " - " + _name; }
		}
		#endregion

		#endregion

		#region Constructors

		public LeadTime()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.LeadTime);
			OrderBy = LeadTimeColumn.JDECode;
		}

		#endregion
	}

	public class LeadTimeColumn
	{
		public const string Id = "Id";
		public const string DivisionId = "DivisionId";
		public const string Name = "Name";
		public const string JDECode = "JDECode";
		public const string CreateTime = "CreateTime";
		public const string CreateUserId = "CreateUserId";
		public const string UpdateTime = "UpdateTime";
		public const string UpdateUserId = "UpdateUserId";
	}

	public class LeadTimeProperty
	{
		public const string Id = "Id";
		public const string DivisionId = "DivisionId";
		public const string Name = "Name";
		public const string JDECode = "JDECode";
		public const string CreateTime = "CreateTime";
		public const string CreateUserId = "CreateUserId";
		public const string UpdateTime = "UpdateTime";
		public const string UpdateUserId = "UpdateUserId";
		public const string JDECodeName = "JDECodeName";
	}
}

﻿using System;
using System.ComponentModel;
using System.Data;
using System.Linq;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class RebateType : SqlServerEntity
	{
		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(RebateTypeColumn.Id, true, true), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;
				SetColumnDirty(RebateTypeProperty.Id);
			}
		}
		#endregion

		#region PROPERTY: Name
		private String _name;
		[EntityColumn(RebateTypeColumn.Name)]
		public String Name
		{
			get { return _name; }
			set
			{
				_name = value;
				SetColumnDirty(RebateTypeProperty.Name);
			}
		}
		#endregion

		#region PROPERTY: IsDefault
		private Boolean _isDefault;
		[EntityColumn(RebateTypeColumn.IsDefault)]
		public Boolean IsDefault
		{
			get { return _isDefault; }
			set
			{
				_isDefault = value;
				SetColumnDirty(RebateTypeProperty.IsDefault);
			}
		}
		#endregion

		#region PROPERTY: IsEstimated
		private Boolean _isEstimated;
		[EntityColumn(RebateTypeColumn.IsEstimated)]
		public Boolean IsEstimated
		{
			get { return _isEstimated; }
			set 
			{
				_isEstimated = value;
				SetColumnDirty(RebateTypeProperty.IsEstimated);
			}
		}
		#endregion

		#region PROPERTY: IsFixed
		private Boolean _isFixed;
		[EntityColumn(RebateTypeColumn.IsFixed)]
		public Boolean IsFixed
		{
			get { return _isFixed; }
			set 
			{
				_isFixed = value;
				SetColumnDirty(RebateTypeProperty.IsFixed);
			}
		}
		#endregion

		#region PROPERTY: IsRangeBased
		private Boolean _isRangeBased;
		[EntityColumn(RebateTypeColumn.IsRangeBased)]
		public Boolean IsRangeBased
		{
			get { return _isRangeBased; }
			set 
			{
				_isRangeBased = value;
				SetColumnDirty(RebateTypeProperty.IsRangeBased);
			}
		}
		#endregion

		#region PROPERTY: IsProductRate
		private Boolean _isProductRate;
		[EntityColumn(RebateTypeColumn.IsProductRate)]
		public Boolean IsProductRate
		{
			get { return _isProductRate; }
			set 
			{
				_isProductRate = value;
				SetColumnDirty(RebateTypeProperty.IsProductRate);
			}
		}
		#endregion

		#region PROPERTY: IsValueBasedCalculation

		private Boolean _isValueBasedCalculation;
		[EntityColumn(RebateTypeColumn.IsValueBasedCalculation)]
		public Boolean IsValueBasedCalculation
		{
			get { return _isValueBasedCalculation; }
			set
			{
				_isValueBasedCalculation = value;
				SetColumnDirty(RebateTypeProperty.IsValueBasedCalculation);
			}
		}

		#endregion

		#region PROPERTY: IsDeliveryWindow

		private Boolean _isDeliveryWindow;
		[EntityColumn(RebateTypeColumn.IsDeliveryWindow)]
		public Boolean IsDeliveryWindow
		{
			get { return _isDeliveryWindow; }
			set
			{
				_isDeliveryWindow = value;
				SetColumnDirty(RebateTypeProperty.IsDeliveryWindow);
			}
		}
		#endregion

		#region PROPERTY: IsLeadTime

		private Boolean _isLeadTime;
		[EntityColumn(RebateTypeColumn.IsLeadTime)]
		public Boolean IsLeadTime
		{
			get { return _isLeadTime; }
			set
			{
				_isLeadTime = value;
				SetColumnDirty(RebateTypeProperty.IsLeadTime);
			}
		}
		#endregion

		#region PROPERTY: ObjectName
		private String _objectName;
		[EntityColumn(RebateTypeColumn.ObjectName)]
		public String ObjectName
		{
			get { return _objectName; }
			set
			{
				_objectName = value;
				SetColumnDirty(RebateTypeProperty.ObjectName);
			}
		}
		#endregion

		#region PROPERTY: IsForecast

		private Boolean _isForecast;
		[EntityColumn(RebateTypeColumn.IsForecast)]
		public Boolean IsForecastTime
		{
			get { return _isForecast; }
			set
			{
				_isForecast = value;
				SetColumnDirty(RebateTypeProperty.IsForecast);
			}
		}

		#endregion

		#region PROPERTY IsRebateExtension

		public Boolean IsRebateExtension
		{
			get { return (_isDeliveryWindow || _isLeadTime); }
		}

		#endregion

		#endregion

		#region Constructors

		public RebateType()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.RebateType);
			OrderBy = "{" + RebateTypeProperty.Name + "}";
		}

		#endregion

		#region Business Logic

		public RebateTypeCalculator GetCalculator(Rebate rebate)
		{
			RebateTypeCalculator rebateTypeCalculator = null;

			if (!String.IsNullOrEmpty(this.ObjectName))
			{
				Type type = Type.GetType(this.GetType().Namespace + "." + this.ObjectName);
				rebateTypeCalculator = (RebateTypeCalculator) Activator.CreateInstance(type, new Object[] { rebate });
			}

			return rebateTypeCalculator;
		}

		#endregion
	}

	#region RebateTypeExtensionMethods

	public static class RebateTypeExtensionMethods
	{
		public static RebateType Find(this ExtendedBindingList<RebateType> rebateTypes, Int32 id)
		{
			return rebateTypes.FirstOrDefault(rt => rt.Id == id);
		}
	}

	#endregion

	#region Abstract Calculator Class

	public abstract class RebateTypeCalculator
	{
		#region Properties

		#region PROPERTY: IsSuspendedCalculation
		public Boolean IsSuspendedCalculation { get; set; }
		#endregion

		#region PROPERTY: Rebate
		private Rebate _rebate;
		public Rebate Rebate
		{
			get { return _rebate; }
			set { _rebate = value; }
		}
		#endregion

		#region PROPERTY: Period

		private CalculationPeriod _period;

		public CalculationPeriod Period
		{
			get { return _period; }
			set { _period = value; }
		}

		#endregion

		#region PROPERTY: CalculationPeriod

		private CalculationPeriod _calculationPeriod;

		public CalculationPeriod CalculationPeriod
		{
			get { return _calculationPeriod; }
			set { _calculationPeriod = value; } 
		}

		#endregion

		#region PROPERTY: Accrual
		private Accrual _accrual;
		public Accrual Accrual
		{
			get { return _accrual; }
			set { _accrual = value; }
		}
		#endregion

		#region PROPERTY: Hierarchys
		private ExtendedBindingList<Hierarchy> _hierarchys;
		public ExtendedBindingList<Hierarchy> Hierarchys
		{
			get { return _hierarchys; }
			set { _hierarchys = value; }
		}
		#endregion

		#endregion

		#region Contructors

		public RebateTypeCalculator(Rebate rebate)
		{
			this.Rebate = rebate;
		}
		
		#endregion

		#region Business Logic

		public virtual void ValidateData()
		{
			if (this.Hierarchys == null || this.Hierarchys.Count == 0)
			{
				throw new DataException("Rebate Type Calculator: Missing hierarchy details");
			}

			if (this.Accrual == null)
			{
				throw new DataException("Rebate Type Calculator: No Accrual Header record");
			}
		}

		public abstract void CalculateGlobalProduct();
		public abstract void CalculateDetail();
		public abstract void CalculatePriceEnquiryRate();

		#endregion
	}

	#endregion

	public class RebateTypeColumn	
	{
		public const string Id = "Id";
		public const string Name = "Name";
		public const string IsDefault = "IsDefault";
		public const string IsEstimated = "IsEstimated";
		public const string IsFixed = "IsFixed";
		public const string IsRangeBased = "IsRangeBased";
		public const string IsProductRate = "IsProductRate";
		public const string IsValueBasedCalculation = "IsValueBasedCalculation";
		public const string IsDeliveryWindow = "IsDeliveryWindow";
		public const string IsLeadTime = "IsLeadTime";
		public const string IsForecast = "IsForecast";
		public const string ObjectName = "ObjectName";
	}

	public class RebateTypeProperty
	{
		public const string Id = "Id";
		public const string Name = "Name";
		public const string IsDefault = "IsDefault";
		public const string IsEstimated = "IsEstimated";
		public const string IsFixed = "IsFixed";
		public const string IsRangeBased = "IsRangeBased";
		public const string IsProductRate = "IsProductRate";
		public const string IsValueBasedCalculation = "IsValueBasedCalculation";
		public const string IsDeliveryWindow = "IsDeliveryWindow";
		public const string IsLeadTime = "IsLeadTime";
		public const string IsForecast = "IsForecast";
		public const string ObjectName = "ObjectName";
	}
}

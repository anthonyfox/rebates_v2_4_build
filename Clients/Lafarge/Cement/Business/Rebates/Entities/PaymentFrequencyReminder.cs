﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class PaymentFrequencyReminder : SqlServerEntity
	{
		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(PaymentFrequencyReminderColumn.Id, true, true), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;
				SetColumnDirty(PaymentFrequencyReminderProperty.Id);
			}
		}
		#endregion

		#region PROPERTY: PaymentFrequencyId
		private Int32 _paymentFrequencyId;
		[EntityColumn(PaymentFrequencyReminderColumn.PaymentFrequencyId), Browsable(false)]
		public Int32 PaymentFrequencyId
		{
			get { return _paymentFrequencyId; }
			set
			{
				_paymentFrequencyId = value;
				SetColumnDirty(PaymentFrequencyReminderProperty.PaymentFrequencyId);
			}
		}
		#endregion

		#region PROPERTY: Period
		private Int16 _period;
		[EntityColumn(PaymentFrequencyReminderColumn.Period), Browsable(false)]
		public Int16 Period
		{
			get { return _period; }
			set
			{
				_period = value;
				SetColumnDirty(PaymentFrequencyReminderProperty.Period);
			}
		}
		#endregion

		#endregion

		#region Constructors

		public PaymentFrequencyReminder()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.PaymentFrequencyReminder);
		}

		#endregion
	}

	public class PaymentFrequencyReminderColumn
	{
		public const string Id = "Id";
		public const string PaymentFrequencyId = "PaymentFrequencyId";
		public const string Period = "Period";
	}

	public class PaymentFrequencyReminderProperty
	{
		public const string Id = "Id";
		public const string PaymentFrequencyId = "PaymentFrequencyId";
		public const string Period = "Period";
	}

}

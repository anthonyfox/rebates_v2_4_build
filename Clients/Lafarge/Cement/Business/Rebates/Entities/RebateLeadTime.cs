﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class RebateLeadTime : SqlServerEntity, ICloneable
	{
		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(RebateLeadTimeColumn.Id, true, true), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;

				if (this.ExistsOnDatabase)
				{
					SetColumnDirty(RebateLeadTimeProperty.Id);
				}
			}
		}
		#endregion

		#region PROPERTY: RebateId
		private Int32 _rebateId;
		[EntityColumn(RebateLeadTimeColumn.RebateId), Browsable(false)]
		public Int32 RebateId
		{
			get { return _rebateId; }
			set
			{
				_rebateId = value;
				SetColumnDirty(RebateLeadTimeProperty.RebateId);
			}
		}
		#endregion

		#region PROPERTY: LeadTimeId
		private Int32 _leadTimeId;
		[EntityColumn(RebateLeadTimeColumn.LeadTimeId), Browsable(false)]
		public Int32 LeadTimeId
		{
			get { return _leadTimeId; }
			set
			{
				_leadTimeId = value;
				SetColumnDirty(RebateLeadTimeProperty.LeadTimeId);
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: LeadTimeJDECode
		private String _leadTimeJDECode;
		[EntityColumn(LeadTimeColumn.JDECode, DBRebates.LeadTime, RebateLeadTimeProperty.LeadTimeJDECode), DisplayName("JDE Code")]
		public String LeadTimeJDECode
		{
			get { return _leadTimeJDECode; }
			set { _leadTimeJDECode = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: LeadTimeName
		private String _leadTimeName;
		[EntityColumn(LeadTimeColumn.Name, DBRebates.LeadTime, RebateLeadTimeProperty.LeadTimeName), DisplayName("Name")]
		public String LeadTimeName
		{
			get { return _leadTimeName; }
			set { _leadTimeName = value; }
		}
		#endregion

		#region PROPERTY: PackingTypeEnum
		private PackingTypeEnum _packingTypeEnum;
		[EntityColumn(RebateLeadTimeColumn.PackingTypeEnum)]
		[EntityColumnConversion("DBInt16ToPackingTypeEnum", "PackingTypeEnumToDBInt16", "PackingTypeEnumConvertor", "Evolve.Clients.Lafarge.Cement.Business.Rebates", "Evolve.Clients.Lafarge.Cement.Business")]
		[DisplayName("Packing Type")]
		public PackingTypeEnum PackingTypeEnum
		{
			get { return _packingTypeEnum; }
			set
			{
				_packingTypeEnum = value;
				SetColumnDirty(RebateLeadTimeProperty.PackingTypeEnum);
			}
		}
		#endregion

		#region PROPERTY: Rate
		private Decimal? _rate;
		[EntityColumn(RebateLeadTimeColumn.Rate), DisplayName("Rate")]
		public Decimal? Rate
		{
			get { return _rate; }
			set
			{
				_rate = value;
				SetColumnDirty(RebateLeadTimeProperty.Rate);
			}
		}
		#endregion

		#endregion

		#region Constructors

		public RebateLeadTime()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.RebateLeadTime);

			JoinTable(DBRebates.DatabaseName, DBRebates.LeadTime, DBRebates.LeadTime + "." + LeadTimeColumn.Id + " = " + DBRebates.RebateLeadTime + "." + RebateLeadTimeColumn.LeadTimeId);

			OrderBy = "{" + RebateLeadTimeProperty.LeadTimeId + "}";
		}

		#endregion

		#region ICloneable

		public object Clone()
		{
			return this.MemberwiseClone();
		}

		#endregion

		#region Business Logic

		public static Int32 DeleteRebateLeadTimes(Int32 rebateId)
		{
			RebateLeadTime rebateLeadTime = new RebateLeadTime();
			rebateLeadTime.RebateId = rebateId;

			rebateLeadTime.AddReadFilter(RebateLeadTimeProperty.RebateId, "=");

			return rebateLeadTime.DeleteForReadFilter();
		}


		#region Validation

		#region Validate: Rate
		public static String ValidateRate(String rate)
		{
			const Decimal MAX_RATE = 10000000; // 10 Million
			const Decimal MIN_RATE = 0;

			String errorText = "";
			Decimal value;

			if (rate.Trim().Length > 0)
			{
				if (Decimal.TryParse(rate, out value))
				{
					if (value == 0)
					{
						errorText = "Rate cannot be zero";
					}
					else
					{
						if (value < MIN_RATE || value > MAX_RATE)
						{
							errorText = "Rate must be between " + MIN_RATE.ToString() + " and " + MAX_RATE.ToString();
						}
					}
				}
			}
			else
			{
				errorText = "A valid rate must be entered";
			}

			return errorText;
		}
		#endregion

		#endregion

		#endregion
	}

	public class RebateLeadTimeColumn
	{
		public const string Id = "Id";
		public const string RebateId = "RebateId";
		public const string LeadTimeId = "LeadTimeId";
		public const string PackingTypeEnum = "PackingTypeEnum";
		public const string Rate = "Rate";
		public const string AmendTime = "AmendTime";
		public const string AmendUserId = "AmendUserId";
	}

	public class RebateLeadTimeProperty
	{
		public const string Id = "Id";
		public const string RebateId = "RebateId";
		public const string LeadTimeId = "LeadTimeId";
		public const string PackingTypeEnum = "PackingTypeEnum";
		public const string LeadTimeJDECode = "LeadTimeJDECode";
		public const string LeadTimeName = "LeadTimeName";
		public const string Rate = "Rate";
		public const string AmendTime = "AmendTime";
		public const string AmendUserId = "AmendUserId";
	}
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
    public class AuditDetail : SqlServerEntity, ICloneable
    {
        #region Properties

        #region PROPERTY: Id

        private Int32 _id;

        [EntityColumn(AuditDetailColumn.Id, true, true), Browsable(true)]
        [DisplayName(@"Detail Id")]
        public Int32 Id
        {
            get { return _id; }
            set
            {
                _id = value;

                if (ExistsOnDatabase)
                {
                    SetColumnDirty(AuditDetailColumn.Id);
                }
            }
        }

        #endregion

        #region PROPERTY: AuditHeaderId

        private Int32 _auditHeaderId;

        [EntityColumn(AuditDetailColumn.AuditHeaderId)]
        [DisplayName(@"Header Id")]
        public Int32 AuditHeaderId
        {
            get { return _auditHeaderId; }
            set
            {
                _auditHeaderId = value;
                SetColumnDirty(AuditDetailColumn.AuditHeaderId);
            }
        }

        #endregion

        #region PROPERTY: ColumnName

        private String _columnName;

        [EntityColumn(AuditDetailColumn.ColumnName)]
        [DisplayName(@"Column Name")]
        public String ColumnName
        {
            get { return _columnName; }
            set
            {
                _columnName = value;
                SetColumnDirty(AuditDetailColumn.ColumnName);
            }
        }

        #endregion

        #region PROPERTY: OldValueRaw

        private String _oldValueRaw;

        [EntityColumn(AuditDetailColumn.OldValueRaw)]
        [DisplayName(@"Old Value Raw")]
        public String OldValueRaw
        {
            get { return _oldValueRaw; }
            set
            {
                _oldValueRaw = value;
                SetColumnDirty(AuditDetailColumn.OldValueRaw);
            }
        }

        #endregion

        #region PROPERTY: OldValue

        private String _oldValue;

        [EntityColumn(AuditDetailColumn.OldValue)]
        [DisplayName(@"Old Value")]
        public String OldValue
        {
            get { return _oldValue; }
            set
            {
                _oldValue = value;
                SetColumnDirty(AuditDetailColumn.OldValue);

            }
        }

        #endregion

        #region PROPERTY: NewValueRaw

        private String _newValueRaw;

        [EntityColumn(AuditDetailColumn.NewValueRaw)]
        [DisplayName(@"New Value Raw")]
        public String NewValueRaw
        {
            get { return _newValueRaw; }
            set
            {
                _newValueRaw = value;
                SetColumnDirty(AuditDetailColumn.NewValueRaw);
            }
        }

        #endregion

        #region PROPERTY: NewValue

        private String _newValue;

        [EntityColumn(AuditDetailColumn.NewValue)]
        [DisplayName(@"New Value")]
        public String NewValue
        {
            get { return _newValue; }
            set
            {
                _newValue = value;
                SetColumnDirty(AuditDetailColumn.NewValue);
            }
        }

        #endregion


        #endregion

        #region Constructors

        public AuditDetail()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.AuditDetail);

            OrderBy = "{" + AuditDetailProperty.Id + "}";
        }

        #endregion

        #region ICloneable

        public object Clone()
        {
            // Perform shallow clone to start
            AuditDetail auditDetail = (AuditDetail)this.MemberwiseClone();

            // Now deep clone collections

            // Return the clone
            return auditDetail;
        }


        #endregion

    }


    public class AuditDetailColumn
    {
        public const string Id = "Id";
        public const string AuditHeaderId = "AuditHeaderId";
        public const string ColumnName = "ColumnName";
        public const string OldValueRaw = "OldValueRaw";
        public const string OldValue = "OldValue";
        public const string NewValueRaw = "NewValueRaw";
        public const string NewValue = "NewValue";
    }
    public class AuditDetailProperty
    {
        public const string Id = "Id";
        public const string AuditHeaderId = "AuditHeaderId";
        public const string ColumnName = "ColumnName";
        public const string OldValueRaw = "OldValueRaw";
        public const string OldValue = "OldValue";
        public const string NewValueRaw = "NewValueRaw";
        public const string NewValue = "NewValue";
    }
}
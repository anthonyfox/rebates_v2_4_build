﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class RebateNote : SqlServerEntity, ICloneable
	{
		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(RebateNoteColumn.Id, true, true), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;

				if (this.ExistsOnDatabase)
				{
					SetColumnDirty(RebateNoteProperty.Id);
				}
			}
		}
		#endregion

		#region PROPERTY: RebateId
		private Int32 _rebateId;
		[EntityColumn(RebateNoteColumn.RebateId), Browsable(false)]
		public Int32 RebateId
		{
			get { return _rebateId; }
			set
			{
				_rebateId = value;
				SetColumnDirty(RebateNoteProperty.RebateId);
			}
		}
		#endregion

		#region PROPERTY: UserId
		private Int32 _userId;
		[EntityColumn(RebateNoteColumn.UserId), Browsable(false)]
		public Int32 UserId
		{
			get { return _userId; }
			set
			{
				_userId = value;
				SetColumnDirty(RebateNoteProperty.UserId);
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: UserName
		private String _userName;
		[EntityColumn(UserColumn.Name, DBRebates.User, RebateNoteProperty.UserName)]
		public String UserName
		{
			get { return _userName; }
			set 
			{
				_userName = value;
			}
		}
		#endregion

		#region PROPERTY: CreateTime
		private DateTime _createTime;
		[EntityColumn(RebateNoteColumn.CreateTime, DBDateTime.Year, DBDateTime.Minute)]
		public DateTime CreateTime
		{
			get { return _createTime; }
			set 
			{
				_createTime = value;
				SetColumnDirty(RebateNoteProperty.CreateTime);
			}
		}
		#endregion

		#region PROPERTY: Note
		private String _note;
		[EntityColumn(RebateNoteColumn.Note)]
		public String Note
		{
			get { return _note; }
			set 
			{
				_note = value;
				SetColumnDirty(RebateNoteProperty.Note);
			}
		}
		#endregion

		#endregion

		#region Constructors

		public RebateNote()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.RebateNote);

			JoinTable(DBRebates.DatabaseName, DBRebates.User, DBRebates.User + "." + UserColumn.Id + " = " + DBRebates.RebateNote + "." + RebateNoteColumn.UserId);

			OrderBy = "{" + RebateNoteProperty.CreateTime + "}";
		}

		#endregion

		#region ICloneable

		public object Clone()
		{
			return this.MemberwiseClone();
		}

		#endregion
	}

	public class RebateNoteColumn
	{
		public const string Id = "Id";
		public const string RebateId = "RebateId";
		public const string UserId = "UserId";
		public const string CreateTime = "CreateTime";
		public const string Note = "Note";
	}

	public class RebateNoteProperty
	{
		public const string Id = "Id";
		public const string RebateId = "RebateId";
		public const string UserId = "UserId";
		public const string UserName = "UserName";
		public const string CreateTime = "CreateTime";
		public const string Note = "Note";
	}
}

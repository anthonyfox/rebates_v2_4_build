﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class PaymentBreakdown : SqlServerEntity
	{
		#region Properties

		#region DATAGRIDVIEW PROPERTY: IsSelected

		private Boolean _isSelected = false;

		[DisplayName("Is Selected")]
		public Boolean IsSelected
		{
			get { return _isSelected; }
			set { _isSelected = value; NotifyPropertyChanged(PaymentBreakdownProperty.IsSelected); }
		}

		#endregion

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(PaymentBreakdownColumn.Id, true, true), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;
				SetColumnDirty(PaymentBreakdownProperty.Id);
			}
		}
		#endregion

		#region PROPERTY: PayeeId
		private Int32 _payeeId;
		[EntityColumn(PaymentBreakdownColumn.PayeeId), Browsable(false)]
		public Int32 PayeeId
		{
			get { return _payeeId; }
			set
			{
				_payeeId = value;
				SetColumnDirty(PaymentBreakdownProperty.PayeeId);
			}
		}
		#endregion

		#region RELATED PROPERTY: PayeeName

		private String _payeeName;
		[EntityColumn(PayeeColumn.Name, DBRebates.Payee, PaymentBreakdownProperty.PayeeName)]
		[DisplayName("Payee Name")]
		public String PayeeName
		{
			get { return _payeeName; }
			set	{ _payeeName = value; }
		}

		#endregion

		#region PROPERTY: TaxCodeId
		private Int32 _taxCodeId;
		[EntityColumn(PaymentBreakdownColumn.TaxCodeId)]
		[Browsable(false)]
		public Int32 TaxCodeId
		{
			get { return _taxCodeId; }
			set
			{
				_taxCodeId = value;
				SetColumnDirty(PaymentBreakdownProperty.TaxCodeId);
			}
		}
		#endregion

		#region RELATED PROPERTY: TaxCode

		private String _taxCode;
		[EntityColumn(TaxCodeColumn.Code, DBRebates.TaxCode, PaymentBreakdownProperty.TaxCode)]
		[DisplayName("Tax Code")]
		public String TaxCode
		{
			get { return _taxCode; }
			set { _taxCode = value; }
		}

		#endregion

		#region PROPERTY: TaxDate
		private DateTime _taxDate;
		[EntityColumn(PaymentBreakdownColumn.TaxDate, DBDateTimeFrom = DBDateTime.Year, DBDateTimeTo = DBDateTime.Day)]
		public DateTime TaxDate
		{
			get { return _taxDate; }
			set
			{
				_taxDate = value;
				SetColumnDirty(PaymentBreakdownProperty.TaxDate);
			}
		}
		#endregion

		#region PROPERTY: Value
		private Decimal _value;
		[EntityColumn(PaymentBreakdownColumn.Value)]
		public Decimal Value
		{
			get { return _value; }
			set
			{
				_value = value;
				SetColumnDirty(PaymentBreakdownProperty.Value);
			}
		}
		#endregion

		#region PROPERTY: Comment
		private String _comment;
		[EntityColumn(PaymentBreakdownColumn.Comment)]
		public String Comment
		{
			get { return _comment; }
			set
			{
				_comment = value;
				SetColumnDirty(PaymentBreakdownProperty.Comment);
			}
		}
		#endregion

		#region PROPERTY: InterfaceTime
		private DateTime _interfaceTime;
		[EntityColumn(PaymentBreakdownColumn.InterfaceTime)]
		public DateTime InterfaceTime
		{
			get { return _interfaceTime; }
			set
			{
				_interfaceTime = value;
				SetColumnDirty(PaymentBreakdownProperty.InterfaceTime);
			}
		}
		#endregion

		#region PROPERTY: Payment_PaymentBreakdownId
		private Int32 _payment_PaymentBreakdownId;
		[EntityColumn(PaymentBreakdownColumn.Payment_PaymentBreakdownId)]
		[DisplayName("Payment Schedule Id")]
		public Int32 Payment_PaymentBreakdownId
		{
			get { return _payment_PaymentBreakdownId; }
			set
			{
				_payment_PaymentBreakdownId = value;
				SetColumnDirty(PaymentBreakdownProperty.Payment_PaymentBreakdownId);
			}
		}
		#endregion

		#region RELATED PROPERTY: CreateTime

		private DateTime _createTime;
		[EntityColumn(Payment_PaymentBreakdownColumn.CreateTime, DBRebates.Payment_PaymentBreakdown, PaymentBreakdownProperty.CreateTime), DisplayName("Created")]
		public DateTime CreateTime
		{
			get { return _createTime; }
			set { _createTime = value; }
		}

		#endregion

		#region RELATED PROPERTY: CreateUser

		private String _createUser;
		[EntityColumn(UserColumn.Name, DBRebates.User, PaymentBreakdownProperty.CreateUser), DisplayName("Created By")]
		public String CreateUser
		{
			get { return _createUser; }
			set { _createUser = value; }
		}

		#endregion

		#region RELATED PROPERTY: ActionTime

		private DateTime _actionTime;
		[EntityColumn(Payment_PaymentBreakdownColumn.ActionTime, DBRebates.Payment_PaymentBreakdown, PaymentBreakdownProperty.ActionTime, DBDateTime.Year, DBDateTime.Second), DisplayName("Actioned")]
		public DateTime ActionTime
		{
			get { return _actionTime; }
			set { _actionTime = value; }
		}

		#endregion

		#region RELATED PROPERTY: ActionUser

		private String _actionUser;
		[EntityColumn(UserColumn.Name, DBRebates.User, PaymentBreakdownProperty.ActionUser), DisplayName("Actioned By")]
		public String ActionUser
		{
			get { return _actionUser; }
			set { _actionUser = value; }
		}

		#endregion

		#region RELATED PROPERTY: PaymentStatusEnum

		private PaymentStatusEnum _paymentStatusEnum;

		[EntityColumn(Payment_PaymentBreakdownColumn.PaymentStatusEnum, DBRebates.Payment_PaymentBreakdown, PaymentBreakdownProperty.PaymentStatusEnum)]
		[EntityColumnConversion("DBInt16ToPaymentStatusEnum", "PaymentStatusEnumToDBInt16", "PaymentStatusEnumConvertor", "Evolve.Clients.Lafarge.Cement.Business.Rebates", "Evolve.Clients.Lafarge.Cement.Business")]
		[Browsable(false)]
		public PaymentStatusEnum PaymentStatusEnum
		{
			get { return _paymentStatusEnum; }
			set { _paymentStatusEnum = value; NotifyPropertyChanged(PaymentBreakdownProperty.PaymentStatusEnum); }
		}

		#endregion

		#region RELATED PROPERTY: RejectionComment

		private String _rejectionComment;

		[EntityColumn(Payment_PaymentBreakdownColumn.RejectionComment, DBRebates.Payment_PaymentBreakdown, PaymentBreakdownProperty.RejectionComment)]
		[DisplayName("Comment")]
		public String RejectionComment
		{
			get { return _rejectionComment; }
			set { _rejectionComment = value; }
		}

		#endregion

		#endregion

		#region Constructors

		public PaymentBreakdown()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.PaymentBreakdown);

			JoinTable(DBRebates.DatabaseName, DBRebates.Payee, DBRebates.Payee + "." + PayeeColumn.Id + " = " + DBRebates.PaymentBreakdown + "." + PaymentBreakdownColumn.PayeeId, EntityJoinType.Inner);
			JoinTable(DBRebates.DatabaseName, DBRebates.Payment_PaymentBreakdown, DBRebates.Payment_PaymentBreakdown + "." + Payment_PaymentBreakdownColumn.Id + " = " + DBRebates.PaymentBreakdown + "." + PaymentBreakdownColumn.Payment_PaymentBreakdownId, EntityJoinType.Inner);
			JoinTable(DBRebates.DatabaseName, DBRebates.User, DBRebates.User + "." + UserColumn.Id + " = " + DBRebates.Payment_PaymentBreakdown + "." + Payment_PaymentBreakdownColumn.CreateUserId, EntityJoinType.Inner);
			JoinTable(DBRebates.DatabaseName, DBRebates.TaxCode, DBRebates.TaxCode + "." + TaxCodeColumn.Id + " = " + DBRebates.PaymentBreakdown + "." + PaymentBreakdownColumn.TaxCodeId);
		}

		#endregion
	}

	public class PaymentBreakdownColumn
	{
		public const string Id = "Id";
		public const string Payment_PaymentBreakdownId = "Payment_PaymentBreakdownId";
		public const string PayeeId = "PayeeId";
		public const string TaxCodeId = "TaxCodeId";
		public const string TaxDate = "TaxDate";
		public const string Value = "Value";
		public const string Comment = "Comments";
		public const string InterfaceTime = "InterfaceTime";
	}

	public class PaymentBreakdownProperty
	{
		public const string IsSelected = "IsSelected";
		public const string Id = "Id";
		public const string Payment_PaymentBreakdownId = "Payment_PaymentBreakdownId";
		public const string PayeeId = "PayeeId";
		public const string TaxCodeId = "TaxCodeId";
		public const string TaxCode = "TaxCode";
		public const string TaxDate = "TaxDate";
		public const string PayeeName = "PayeeName";
		public const string Value = "Value";
		public const string Comment = "Comment";
		public const string InterfaceTime = "InterfaceTime";
		public const string CreateTime = "CreateTime";
		public const string CreateUser = "CreateUser";
		public const string PaymentStatusEnum = "PaymentStatusEnum";
		public const string RejectionComment = "RejectionComment";
		public const string ActionTime = "ActionTime";
		public const string ActionUser = "ActionUser";
	}
}

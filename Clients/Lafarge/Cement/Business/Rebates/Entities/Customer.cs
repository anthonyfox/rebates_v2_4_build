﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class Customer : SqlServerEntity
	{
		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(CustomerColumn.Id, true, true), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;
				SetColumnDirty(CustomerProperty.Id);
			}
		}
		#endregion

		#region PROPERTY: JDECode
		private string _jdeCode;
		[EntityColumn(CustomerColumn.JDECode), DisplayName("Account")]
		public string JDECode
		{
			get { return _jdeCode; }
			set
			{
				_jdeCode = value;
				SetColumnDirty(CustomerProperty.JDECode);
			}
		}
		#endregion

		#region PROPERTY: Name
		private String _name;
		[EntityColumn(CustomerColumn.Name)]
		public String Name
		{
			get { return _name; }
			set
			{
				_name = value;
				SetColumnDirty(CustomerProperty.Name);
			}
		}
		#endregion

		#region RELATED PROPERTY: DivisionId
		private Int32 _divisionId;
		[EntityColumn(CustomerGroupColumn.DivisionId, DBRebates.CustomerGroup, CustomerProperty.DivisionId)]
		[Browsable(false)]
		public Int32 DivisionId
		{
			get { return _divisionId; }
			set
			{
				_divisionId = value;
				SetColumnDirty(CustomerProperty.DivisionId);
			}
		}
		#endregion

		#region PROPERTY: AddressId
		private Int32 _addressId;
		[EntityColumn(CustomerColumn.AddressId), Browsable(false)]
		public Int32 AddressId
		{
			get { return _addressId; }
			set
			{
				_addressId = value;
				SetColumnDirty(CustomerProperty.AddressId);
			}
		}
		#endregion

		#region PROPERTY: CustomerGroupId
		private Int32 _customerGroupId;
		[EntityColumn(CustomerColumn.CustomerGroupId), Browsable(false)]
		public Int32 CustomerGroupId
		{
			get { return _customerGroupId; }
			set
			{
				_customerGroupId = value;
				SetColumnDirty(CustomerProperty.CustomerGroupId);
			}
		}
		#endregion

		#region PROPERTY: PricingSubGroupId
		private Int32 _pricingSubGroupId;
		[EntityColumn(CustomerColumn.PricingSubGroupId), Browsable(false)]
		public Int32 PricingSubGroupId
		{
			get { return _pricingSubGroupId; }
			set
			{
				_pricingSubGroupId = value;
				SetColumnDirty(CustomerProperty.PricingSubGroupId);
			}
		}
		#endregion

		#region PROPERTY: SoldToId
		private Int32 _soldToId;
		[EntityColumn(CustomerColumn.SoldToId), Browsable(false)]
		public Int32 SoldToId
		{
			get { return _soldToId; }
			set
			{
				_soldToId = value;
				SetColumnDirty(CustomerProperty.SoldToId);
			}
		}
		#endregion

		#region PROPERTY: CustomerSearchTypeEnum
		private CustomerSearchTypeEnum _customerSearchTypeEnum;
		[EntityColumn(CustomerColumn.CustomerSearchTypeEnum), Browsable(false)]
		[EntityColumnConversion("DBStringToCustomerSearchTypeEnum", "CustomerSearchTypeEnumToDBString", "CustomerSearchTypeEnumConvertor", "Evolve.Clients.Lafarge.Cement.Business.Rebates", "Evolve.Clients.Lafarge.Cement.Business")]
		public CustomerSearchTypeEnum CustomerSearchTypeEnum
		{
			get { return _customerSearchTypeEnum; }
			set
			{
				_customerSearchTypeEnum = value;
				SetColumnDirty(CustomerProperty.CustomerSearchTypeEnum);
			}
		}
        #endregion

        #region PROPERTY: InterfaceDateTime
        private DateTime _interfaceDateTime;
        [EntityColumn(CustomerColumn.InterfaceDateTime, DBDateTime.Year, DBDateTime.Second), Browsable(false)]
        public DateTime InterfaceDateTime
        {
            get { return _interfaceDateTime; }
            set
            {
                _interfaceDateTime = value;
                SetColumnDirty(CustomerProperty.InterfaceDateTime);
            }
        }
        #endregion

        #region PROPERTY: IsActive
        private bool _isActive;
        [EntityColumn(CustomerColumn.IsActive), Browsable(false)]
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                SetColumnDirty(CustomerProperty.IsActive);
            }
        }
        #endregion

        #endregion

        #region Constructors

        public Customer()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.Customer);
			JoinTable(DBRebates.DatabaseName, DBRebates.CustomerGroup, DBRebates.CustomerGroup + "." + CustomerGroupColumn.Id + " = " + DBRebates.Customer + "." + CustomerColumn.CustomerGroupId);
		}

		#endregion
	}

	public class CustomerColumn
	{
		public const string Id = "Id";
		public const string JDECode = "JDECode";
		public const string Name = "Name";
		public const string AddressId = "AddressId";
		public const string CustomerGroupId = "CustomerGroupId";
		public const string PricingSubGroupId = "PricingSubGroupId";
		public const string SoldToId = "SoldToId";
		public const string CustomerSearchTypeEnum = "SearchType";
		public const string InterfaceDateTime = "InterfaceTime";
		public const string IsActive = "Active";
	}

	public class CustomerProperty
	{
		public const string Id = "Id";
		public const string JDECode = "JDECode";
		public const string Name = "Name";
		public const string DivisionId = "DivisionId";
		public const string AddressId = "AddressId";
		public const string CustomerGroupId = "CustomerGroupId";
		public const string PricingSubGroupId = "PricingSubGroupId";
		public const string SoldToId = "SoldToId";
		public const string CustomerSearchTypeEnum = "CustomerSearchTypeEnum";
		public const string InterfaceDateTime = "InterfaceTime";
		public const string IsActive = "IsActive";
	}
}

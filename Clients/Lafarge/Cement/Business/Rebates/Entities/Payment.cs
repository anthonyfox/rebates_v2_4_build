﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class Payment : SqlServerEntity
	{
		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(PaymentColumn.Id, true, true), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;
				SetColumnDirty(PaymentProperty.Id);
			}
		}
		#endregion

		#region PROPERTY: Payment_PaymentBreakdownId
		private Int32 _payment_PaymentBreakdownId;
		[EntityColumn(PaymentColumn.Payment_PaymentBreakdownId), Browsable(false)]
		public Int32 Payment_PaymentBreakdownId
		{
			get { return _payment_PaymentBreakdownId; }
			set
			{
				_payment_PaymentBreakdownId = value;
				SetColumnDirty(PaymentProperty.Payment_PaymentBreakdownId);
			}
		}
		#endregion

		#region PROPERTY: RebateId
		private Int32 _rebateId;
		[EntityColumn(PaymentColumn.RebateId), Browsable(false)]
		public Int32 RebateId
		{
			get { return _rebateId; }
			set
			{
				_rebateId = value;
				SetColumnDirty(PaymentProperty.RebateId);
			}
		}
		#endregion

		#region PROPERTY: CreateYear
		private Int16 _createYear;
		[EntityColumn(PaymentColumn.CreateYear), Browsable(false)]
		public Int16 CreateYear
		{
			get { return _createYear; }
			set
			{
				_createYear = value;
				SetColumnDirty(PaymentProperty.CreateYear);
			}
		}
		#endregion

		#region PROPERTY: CreatePeriod
		private Int16 _createPeriod;
		[EntityColumn(PaymentColumn.CreatePeriod), Browsable(false)]
		public Int16 CreatePeriod
		{
			get { return _createPeriod; }
			set
			{
				_createPeriod = value;
				SetColumnDirty(PaymentProperty.CreatePeriod);
			}
		}
		#endregion

		#region PROPERTY: ActualYear
		private Int16 _actualYear;
		[EntityColumn(PaymentColumn.ActualYear), Browsable(false)]
		public Int16 ActualYear
		{
			get { return _actualYear; }
			set
			{
				_actualYear = value;
				SetColumnDirty(PaymentProperty.ActualYear);
			}
		}
		#endregion

		#region PROPERTY: ActualPeriod
		private Int16 _actualPeriod;
		[EntityColumn(PaymentColumn.ActualPeriod), Browsable(false)]
		public Int16 ActualPeriod
		{
			get { return _actualPeriod; }
			set
			{
				_actualPeriod = value;
				SetColumnDirty(PaymentProperty.ActualPeriod);
			}
		}
		#endregion

		#region PROPERTY: Value
		private Decimal _value;
		[EntityColumn(PaymentColumn.Value), Browsable(false)]
		public Decimal Value
		{
			get { return _value; }
			set
			{
				_value = value;
				SetColumnDirty(PaymentProperty.Value);
			}
		}
		#endregion

		#region PROPERTY: PaymentTypeEnum
		private PaymentTypeEnum _paymentTypeEnum;
		[EntityColumn(PaymentColumn.PaymentTypeEnum), EntityColumnConversion("DBInt16ToPaymentTypeEnum", "PaymentTypeEnumToDBInt16", "PaymentTypeEnumConvertor", "Evolve.Clients.Lafarge.Cement.Business.Rebates", "Evolve.Clients.Lafarge.Cement.Business"), Browsable(false)]
		public PaymentTypeEnum PaymentTypeEnum
		{
			get { return _paymentTypeEnum; }
			set
			{
				_paymentTypeEnum = value;
				SetColumnDirty(PaymentProperty.PaymentTypeEnum);
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: DivisionId

		private Int32 _divisionId;
		[EntityColumn(RebateColumn.DivisionId, DBRebates.Rebate, PaymentProperty.DivisionId), Browsable(false)]
		public Int32 DivisionId
		{
			get { return _divisionId; }
			set { _divisionId = value; }
		}

		#endregion

		#region RELATED ENTITY PROPERTY: PaymentStatusEnum
		private PaymentStatusEnum _paymentStatusEnum;
		[EntityColumn(Payment_PaymentBreakdownColumn.PaymentStatusEnum, DBRebates.Payment_PaymentBreakdown, PaymentProperty.PaymentStatusEnum), EntityColumnConversion("DBInt16ToPaymentStatusEnum", "PaymentStatusEnumToDBInt16", "PaymentStatusEnumConvertor", "Evolve.Clients.Lafarge.Cement.Business.Rebates", "Evolve.Clients.Lafarge.Cement.Business"), Browsable(false)]
		public PaymentStatusEnum PaymentStatusEnum
		{
			get { return _paymentStatusEnum; }
			set
			{
				_paymentStatusEnum = value;
				SetColumnDirty(PaymentProperty.PaymentStatusEnum);
			}
		}
		#endregion

		#endregion

		#region Constructors

		public Payment()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.Payment);
			JoinTable(DBRebates.DatabaseName, DBRebates.Rebate, DBRebates.Rebate + "." + RebateColumn.Id + " = " + DBRebates.Payment + "." + PaymentColumn.RebateId);
			JoinTable(DBRebates.DatabaseName, DBRebates.Payment_PaymentBreakdown, DBRebates.Payment_PaymentBreakdown + "." + Payment_PaymentBreakdownColumn.Id + " = " + DBRebates.Payment + "." + PaymentColumn.Payment_PaymentBreakdownId);
		}

		#endregion
	}

	public class PaymentColumn
	{
		public const string Id = "Id";
		public const string Payment_PaymentBreakdownId = "Payment_PaymentBreakdownId";
		public const string RebateId = "RebateId";
		public const string CreateYear = "CreateYear";
		public const string CreatePeriod = "CreatePeriod";
		public const string ActualYear = "ActualYear";
		public const string ActualPeriod = "ActualPeriod";
		public const string Value = "Value";
		public const string PaymentTypeEnum = "PaymentTypeEnum";
	}

	public class PaymentProperty
	{
		public const string Id = "Id";
		public const string Payment_PaymentBreakdownId = "Payment_PaymentBreakdownId";
		public const string RebateId = "RebateId";
		public const string CreateYear = "CreateYear";
		public const string CreatePeriod = "CreatePeriod";
		public const string ActualYear = "ActualYear";
		public const string ActualPeriod = "ActualPeriod";
		public const string Value = "Value";
		public const string PaymentTypeEnum = "PaymentTypeEnum";
		public const string DivisionId = "DivisionId";
		public const string PaymentStatusEnum = "PaymentStatusEnum";
	}
}

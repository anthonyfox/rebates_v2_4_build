﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class InvoiceLine : SqlServerEntity
	{
		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(InvoiceLineColumn.Id, true, true),  DisplayName("Id"), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set 
			{
				_id = value;
				SetColumnDirty(InvoiceLineProperty.Id);
			}
		}
		#endregion

		#region PROPERTY: DivisionId
		private Int32 _divisionId;
		[EntityColumn(InvoiceLineColumn.DivisionId), Browsable(false)]
		public Int32 DivisionId
		{
			get { return _divisionId; }
			set
			{
				_divisionId = value;
				SetColumnDirty(InvoiceLineProperty.DivisionId);
			}
		}
		#endregion

		#region PROPERTY: Company
		private String _company;
		[EntityColumn(InvoiceLineColumn.Company)]
		public String Company
		{
			get { return _company; }
			set
			{
				_company = value;
				SetColumnDirty(InvoiceLineProperty.Company);
			}
		}
		#endregion

		#region PROPERTY: OrderNumber
		private string _orderNumber;
		[EntityColumn(InvoiceLineColumn.OrderNumber)]
		[DisplayName("Order Number")]
		public string OrderNumber
		{
			get { return _orderNumber; }
			set
			{
				_orderNumber = value;
				SetColumnDirty(InvoiceLineProperty.OrderNumber);
			}
		}
		#endregion

		#region PROPERTY: OrderLineNumber
		private String _orderLineNumber;
		[EntityColumn(InvoiceLineColumn.OrderLineNumber)]
		[DisplayName("Order Number")]
		public String OrderLineNumber
		{
			get { return _orderLineNumber; }
			set
			{
				_orderLineNumber = value;
				SetColumnDirty(InvoiceLineProperty.OrderLineNumber);
			}
		}
		#endregion

		#region PROPERTY: DocumentNumber
		private String _documentNumber;
		[EntityColumn(InvoiceLineColumn.DocumentNumber), DisplayName("Document Number")]
		public String DocumentNumber
		{
			get { return _documentNumber; }
			set 
			{
				_documentNumber = value;
				SetColumnDirty(InvoiceLineProperty.DocumentNumber);
			}
		}
		#endregion

		#region PROPERTY: DocumentType
		private String _documentType;
		[EntityColumn(InvoiceLineColumn.DocumentType), DisplayName("Document Type")]
		public String DocumentType
		{
			get { return _documentType; }
			set 
			{
				_documentType = value;
				SetColumnDirty(InvoiceLineProperty.DocumentType);
			}
		}
		#endregion

		#region PROPERTY: LineNumber
		private String _itemNumber;
		[EntityColumn(InvoiceLineColumn.LineNumber), DisplayName("Item Number")]
		public String LineNumber
		{
			get { return _itemNumber; }
			set
			{
				_itemNumber = value;
				SetColumnDirty(InvoiceLineProperty.LineNumber);
			}
		}
		#endregion
		
		#region PROPERTY: Date
		private DateTime _date;
		[EntityColumn(InvoiceLineColumn.Date)]
		public DateTime Date
		{
			get { return _date; }
			set 
			{ 
				_date = value;
				SetColumnDirty(InvoiceLineProperty.Date);
			}
		}
		#endregion

		#region PROPERTY: Quantity
		private Decimal _quantity;
		[EntityColumn(InvoiceLineColumn.Quantity)]
		public Decimal Quantity
		{
			get { return _quantity; }
			set 
			{
				_quantity = value;
				SetColumnDirty(InvoiceLineProperty.Quantity);
			}
		}
		#endregion

		#region PROPERTY: Amount
		private Decimal _amount;
		[EntityColumn(InvoiceLineColumn.Amount)]
		public Decimal Amount
		{
			get { return _amount; }
			set 
			{
				_amount = value;
				SetColumnDirty(InvoiceLineProperty.Amount);
			}
		}
		#endregion

		#region PROPERTY: DiscountAmount
		private Decimal _discountAmount;
		[EntityColumn(InvoiceLineColumn.DiscountAmount), DisplayName("Discount Amount")]
		public Decimal DiscountAmount
		{
			get { return _discountAmount; }
			set 
			{
				_discountAmount = value;
				SetColumnDirty(InvoiceLineProperty.DiscountAmount);
			}
		}
		#endregion

		#region PROPERTY: ProductId
		private Int32 _productId;
		[EntityColumn(InvoiceLineColumn.ProductId), Browsable(false)]
		public Int32 ProductId
		{
			get { return _productId; }
			set 
			{
				_productId = value;
				SetColumnDirty(InvoiceLineProperty.ProductId);
			}
		}
		#endregion

		#region PROPERTY: SiteId
		private Int32 _siteId;
		[EntityColumn(InvoiceLineColumn.SiteId), Browsable(false)]
		public Int32 SiteId
		{
			get { return _siteId; }
			set 
			{
				_siteId = value;
				SetColumnDirty(InvoiceLineProperty.SiteId);
			}
		}
		#endregion

		#region PROPERTY: SoldToId
		private Int32 _soldToId;
		[EntityColumn(InvoiceLineColumn.SoldToId), Browsable(false)]
		public Int32 SoldToId
		{
			get { return _soldToId; }
			set 
			{
				_soldToId = value;
				SetColumnDirty(InvoiceLineProperty.SoldToId);
			}
		}
		#endregion

		#region PROPERTY: ShipToId
		private Int32 _shipToId;
		[EntityColumn(InvoiceLineColumn.ShipToId), Browsable(false)]
		public Int32 ShipToId
		{
			get { return _shipToId; }
			set 
			{
				_shipToId = value;
				SetColumnDirty(InvoiceLineProperty.ShipToId);
			}
		}
		#endregion

		#region PROPERTY: DeliveryTypeEnum
		private DeliveryTypeEnum _deliveryTypeEnum;
		[EntityColumn(InvoiceLineColumn.DeliveryTypeEnum)]
		[EntityColumnConversion("DBInt16ToDeliveryTypeEnum", "DeliveryTypeEnumToDBInt16", "DeliveryTypeEnumConvertor", "Evolve.Clients.Lafarge.Cement.Business.Rebates", "Evolve.Clients.Lafarge.Cement.Business")]
		[Browsable(false)]
		public DeliveryTypeEnum DeliveryTypeEnum
		{
			get { return _deliveryTypeEnum; }
			set 
			{
				_deliveryTypeEnum = value;
				SetColumnDirty(InvoiceLineProperty.DeliveryTypeEnum);
			}
		}
		#endregion

		#region PROPERTY: IsApplicableQuantity
		private Boolean _isApplicableQuantity;
		[EntityColumn(InvoiceLineColumn.IsApplicableQuantity), Browsable(false)]
		public Boolean IsApplicableQuantity
		{
			get { return _isApplicableQuantity; }
			set 
			{
				_isApplicableQuantity = value;
				SetColumnDirty(InvoiceLineProperty.IsApplicableQuantity);
			}
		}
		#endregion

		#region PROPERTY: IsApplicableValue
		private Boolean _isApplicableValue;
		[EntityColumn(InvoiceLineColumn.IsApplicableValue), Browsable(false)]
		public Boolean IsApplicableValue
		{
			get { return _isApplicableValue; }
			set 
			{
				_isApplicableValue = value;
				SetColumnDirty(InvoiceLineProperty.IsApplicableValue);
			}
		}
		#endregion

		#region PROPERTY: DeliveryWindowId
		private Int32? _deliveryWindowId;
		[EntityColumn(InvoiceLineColumn.DeliveryWindowId), Browsable(false)]
		public Int32? DeliveryWindowId
		{
			get { return _deliveryWindowId; }
			set
			{
				_deliveryWindowId = value;
				SetColumnDirty(InvoiceLineProperty.DeliveryWindowId);
			}
		}
		#endregion

		#region PROPERTY: LeadTimeId
		private Int32? _leadTimeId;
		[EntityColumn(InvoiceLineColumn.LeadTimeId), Browsable(false)]
		public Int32? LeadTimeId
		{
			get { return _leadTimeId; }
			set
			{
				_leadTimeId = value;
				SetColumnDirty(InvoiceLineProperty.LeadTimeId);
			}
		}
		#endregion

		#region PROPERTY: PackingTypeEnum
		private PackingTypeEnum _packingTypeEnum;
		[EntityColumn(InvoiceLineColumn.PackingTypeEnum), EntityColumnConversion("DBInt16ToPackingTypeEnum", "PackingTypeEnumToDBInt16", "PackingTypeEnumConvertor", "Evolve.Clients.Lafarge.Cement.Business.Rebates", "Evolve.Clients.Lafarge.Cement.Business"), Browsable(false)]
		public PackingTypeEnum PackingTypeEnum
		{
			get { return _packingTypeEnum; }
			set
			{
				_packingTypeEnum = value;
				SetColumnDirty(InvoiceLineProperty.PackingTypeEnum);
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: CustomerGroupId
		private Int32 _customerGroupId;
		
		[EntityColumn(CustomerColumn.CustomerGroupId, DBRebates.SoldToCustomer, InvoiceLineProperty.CustomerGroupId), Browsable(false)]
		public Int32 CustomerGroupId
		{
			get { return _customerGroupId; }
			set 
			{
				_customerGroupId = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: PricingSubGroupId
		private Int32? _pricingSubGroupId;

		[EntityColumn(CustomerColumn.PricingSubGroupId, DBRebates.ShipToCustomer, InvoiceLineProperty.PricingSubGroupId)]
		[Browsable(false)]
		public Int32? PricingSubGroupId
		{
			get { return _pricingSubGroupId; }
			set
			{
				_pricingSubGroupId = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: AttributeId1
		private Int32? _attributeId1;
		[EntityColumn(AttributeColumn.Id, "Attribute1", InvoiceLineProperty.AttributeId1), Browsable(false)]
		public Int32? AttributeId1
		{
			get { return _attributeId1; }
			set 
			{
				_attributeId1 = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: AttributeId2
		private Int32? _attributeId2;
		[EntityColumn(AttributeColumn.Id, "Attribute2", InvoiceLineProperty.AttributeId2), Browsable(false)]
		public Int32? AttributeId2
		{
			get { return _attributeId2; }
			set 
			{
				_attributeId2 = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: AttributeId3
		private Int32? _attributeId3;
		[EntityColumn(AttributeColumn.Id, "Attribute3", InvoiceLineProperty.AttributeId3), Browsable(false)]
		public Int32? AttributeId3
		{
			get { return _attributeId3; }
			set 
			{
				_attributeId3 = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: AttributeId4
		private Int32? _attributeId4;
		[EntityColumn(AttributeColumn.Id, "Attribute4", InvoiceLineProperty.AttributeId4), Browsable(false)]
		public Int32? AttributeId4
		{
			get { return _attributeId4; }
			set 
			{
				_attributeId4 = value;
			}
		}
		#endregion

		#endregion

		#region Constructors

		public InvoiceLine()
		{
			InitializeComponent(DBRebates.InvoiceLine);
		}

		public InvoiceLine(String tableName)
		{
			InitializeComponent(tableName);
		}

		private void InitializeComponent(String tableName)
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, tableName);

			JoinTable(DBRebates.DatabaseName, DBRebates.Customer, DBRebates.SoldToCustomer + "." + CustomerColumn.Id + " = " + tableName + "." + InvoiceLineColumn.SoldToId, DBRebates.SoldToCustomer);
			JoinTable(DBRebates.DatabaseName, DBRebates.Customer, DBRebates.ShipToCustomer + "." + CustomerColumn.Id + " = " + tableName + "." + InvoiceLineColumn.ShipToId, DBRebates.ShipToCustomer);
			JoinTable(DBRebates.DatabaseName, DBRebates.Product, DBRebates.Product + "." + ProductColumn.Id + " = " + tableName + "." + InvoiceLineColumn.ProductId);
			JoinTable(DBRebates.DatabaseName, DBRebates.Attribute, "Attribute1" + "." + AttributeColumn.Id + " = " + DBRebates.Product + "." + ProductColumn.AttributeId1.ToString(), "Attribute1", EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.Attribute, "Attribute2" + "." + AttributeColumn.Id.ToString() + " = " + DBRebates.Product + "." + ProductColumn.AttributeId2.ToString(), "Attribute2", EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.Attribute, "Attribute3" + "." + AttributeColumn.Id.ToString() + " = " + DBRebates.Product + "." + ProductColumn.AttributeId3.ToString(), "Attribute3", EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.Attribute, "Attribute4" + "." + AttributeColumn.Id.ToString() + " = " + DBRebates.Product + "." + ProductColumn.AttributeId4.ToString(), "Attribute4", EntityJoinType.LeftOuter);
		}

		#endregion

		#region Business Logic

		public List<RebateDetail> MatchToRebateDetail(ExtendedBindingList<RebateDetail> rebateDetails)
		{
			List<RebateDetail> matched = new List<RebateDetail>();

			foreach (RebateDetail rebateDetail in rebateDetails)
			{
				if (rebateDetail.ProductId.HasValue)
				{
					if (rebateDetail.ProductId == _productId)
					{
						matched.Add(rebateDetail);
					}
				}
				else
				{
					if (IsMatchingAttribute(_attributeId1, rebateDetail.AttributeId1) &&
						IsMatchingAttribute(_attributeId2, rebateDetail.AttributeId2) &&
						IsMatchingAttribute(_attributeId3, rebateDetail.AttributeId3) &&
						IsMatchingAttribute(_attributeId4, rebateDetail.AttributeId4))
					{
						matched.Add(rebateDetail);
					}
				}
			}

			return matched;
		}

		private Boolean IsMatchingAttribute(Int32? productAttributeId, Int32? rebateDetailAttributeId)
		{
			Boolean isMatch = false;

			// Although RebateDetail.AttributeId is a nullable field for various matching reasons we are using
			// zero to mean no value
			if (productAttributeId.HasValue && rebateDetailAttributeId.HasValue && rebateDetailAttributeId.Value != 0)
			{
				if (productAttributeId == rebateDetailAttributeId)
				{
					isMatch = true;
				}
			} 
			else if (!rebateDetailAttributeId.HasValue || rebateDetailAttributeId.Value == 0)
			{
				isMatch = true;
			}

			return isMatch;
		}

		#endregion
	}

	public class InvoiceLineColumn
	{
		public const string Id = "Id";
		public const string DivisionId = "DivisionId";
		public const string Company = "Company";
		public const string DocumentNumber = "DocumentNumber";
		public const string DocumentType = "DocumentType";
		public const string LineNumber = "LineNumber";
		public const string Date = "SaleDate";
		public const string Quantity = "Quantity";
		public const string Amount = "Amount";
		public const string DiscountAmount = "DiscountAmount";
		public const string ProductId = "ProductId";
		public const string SiteId = "SiteId";
		public const string SoldToId = "SoldToId";
		public const string ShipToId = "ShipToId";
		public const string DeliveryTypeEnum = "DeliveryTypeEnum";
		public const string IsApplicableQuantity = "IsApplicableQuantity";
		public const string IsApplicableValue = "IsApplicableValue";
		public const string DeliveryWindowId = "DeliveryWindowId";
		public const string LeadTimeId = "LeadTimeId";
		public const string PackingTypeEnum = "PackingTypeEnum";
		public const string OrderNumber = "OrderNumber";
		public const string OrderLineNumber = "OrderLineNumber";
	}

	public class InvoiceLineProperty
	{
		public const string Id = "Id";
		public const string DivisionId = "DivisionId";
		public const string Company = "Company";
		public const string DocumentNumber = "DocumentNumber";
		public const string DocumentType = "DocumentType";
		public const string LineNumber = "LineNumber";
		public const string Date = "Date";
		public const string Quantity = "Quantity";
		public const string Amount = "Amount";
		public const string DiscountAmount = "DiscountAmount";
		public const string ProductId = "ProductId";
		public const string SiteId = "SiteId";
		public const string SoldToId = "SoldToId";
		public const string ShipToId = "ShipToId";
		public const string DeliveryTypeEnum = "DeliveryTypeEnum";
		public const string IsApplicableQuantity = "IsApplicableQuantity";
		public const string IsApplicableValue = "IsApplicableValue";
		public const string DeliveryWindowId = "DeliveryWindowId";
		public const string LeadTimeId = "LeadTimeId";
		public const string PackingTypeEnum = "PackingTypeEnum";
		public const string CustomerGroupId = "CustomerGroupId";
		public const string PricingSubGroupId = "PricingSubGroupId";
		public const string AttributeId1 = "AttributeId1";
		public const string AttributeId2 = "AttributeId2";
		public const string AttributeId3 = "AttributeId3";
		public const string AttributeId4 = "AttributeId4";
		public const string OrderNumber = "OrderNumber";
		public const string OrderLineNumber = "OrderLineNumber";
	}
}

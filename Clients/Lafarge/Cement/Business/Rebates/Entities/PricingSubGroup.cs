﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class PricingSubGroup : SqlServerEntity, IRebateExtension
	{
		[Browsable(false)]
		public String Title { get { return "Pricing Sub-Group"; } }
		[Browsable(false)]
		public String EntityName { get { return "Customer"; } }
		[Browsable(false)]
		public String EntityTitle { get { return "Customer"; } }
		[Browsable(false)]
		public String PropertyName { get { return CustomerProperty.PricingSubGroupId; } }
		[Browsable(false)]
		public String RebateEntityName { get { return DBRebates.Rebate; } }
		[Browsable(false)]
		public String RebateEntityTitle { get { return "Rebate"; } }
		[Browsable(false)]
		public String RebatePropertyName { get { return RebateProperty.PricingSubGroupId; } }
		[Browsable(false)]
		public Int32 TextBoxWidth { get { return 30; } }
		[Browsable(false)]
		public Int32 MaxLength { get { return 3; } }


		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(PricingSubGroupColumn.Id, true, true), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;
				SetColumnDirty(PricingSubGroupProperty.Id);
			}
		}
		#endregion

		#region PROPERTY: DivisionId
		private Int32 _divisionId;
		[EntityColumn(PricingSubGroupColumn.DivisionId), Browsable(false)]
		public Int32 DivisionId
		{
			get { return _divisionId; }
			set
			{
				_divisionId = value;
				SetColumnDirty(PricingSubGroupProperty.DivisionId);
			}
		}
		#endregion

		#region PROPERTY: JDECode
		private String _jdeCode;
		[EntityColumn(PricingSubGroupColumn.JDECode)]
		[DisplayName("JDE Code")]
		public String JDECode
		{
			get { return _jdeCode; }
			set
			{
				_jdeCode = value;
				SetColumnDirty(PricingSubGroupProperty.JDECode);
			}
		}
		#endregion

		#region PROPERTY: Name
		private String _name;
		[EntityColumn(PricingSubGroupColumn.Name)]
		public String Name
		{
			get { return _name; }
			set
			{
				_name = value;
				SetColumnDirty(PricingSubGroupProperty.Name);
			}
		}
		#endregion

		#region PROPERTY: CreateTime
		private DateTime _createTime;
		[EntityColumn(PricingSubGroupColumn.CreateTime, DBDateTime.Year, DBDateTime.Minute)]
		[Browsable(false)]
		public DateTime CreateTime
		{
			get { return _createTime; }
			set
			{
				_createTime = value;
				SetColumnDirty(PricingSubGroupProperty.CreateTime);
			}
		}
		#endregion

		#region PROPERTY: CreateUserId
		private Int32 _createUserId;
		[EntityColumn(PricingSubGroupColumn.CreateUserId), Browsable(false)]
		public Int32 CreateUserId
		{
			get { return _createUserId; }
			set
			{
				_createUserId = value;
				SetColumnDirty(PricingSubGroupProperty.CreateUserId);
			}
		}
		#endregion

		#region PROPERTY: UpdateTime
		private DateTime _updateTime;
		[EntityColumn(PricingSubGroupColumn.UpdateTime, DBDateTime.Year, DBDateTime.Minute)]
		[Browsable(false)]
		public DateTime UpdateTime
		{
			get { return _updateTime; }
			set
			{
				_updateTime = value;
				SetColumnDirty(PricingSubGroupProperty.UpdateTime);
			}
		}
		#endregion

		#region PROPERTY: UpdateUserId
		private Int32? _updateUserId;
		[EntityColumn(PricingSubGroupColumn.UpdateUserId), Browsable(false)]
		public Int32? UpdateUserId
		{
			get { return _updateUserId; }
			set
			{
				_updateUserId = value;
				SetColumnDirty(PricingSubGroupProperty.UpdateUserId);
			}
		}
		#endregion

		#region PROPERTY: JDECodeName
		public String JDECodeName
		{
			get { return (String.IsNullOrEmpty(_jdeCode) ? "" : _jdeCode + " - " + _name); }
		}
		#endregion

		#endregion

		#region Constructors

		public PricingSubGroup()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.PricingSubGroup);
			OrderBy = PricingSubGroupColumn.JDECode;
		}

		#endregion
	}

	public class PricingSubGroupColumn
	{
		public const string Id = "Id";
		public const string DivisionId = "DivisionId";
		public const string JDECode = "JDECode";
		public const string Name = "Name";
		public const string CreateTime = "CreateTime";
		public const string CreateUserId = "CreateUserId";
		public const string UpdateTime = "UpdateTime";
		public const string UpdateUserId = "UpdateUserId";
	}

	public class PricingSubGroupProperty
	{
		public const string Id = "Id";
		public const string DivisionId = "DivisionId";
		public const string JDECode = "JDECode";
		public const string Name = "Name";
		public const string CreateTime = "CreateTime";
		public const string CreateUserId = "CreateUserId";
		public const string UpdateTime = "UpdateTime";
		public const string UpdateUserId = "UpdateUserId";
		public const string JDECodeName = "JDECodeName";
	}
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class AccrualDetail : SqlServerEntity
	{
		#region Properties

		// Rebate Detail

		#region RELATED ENTITY PROPERTY: AttributeId1
		private Int32? _attributeId1;
		[EntityColumn(AttributeColumn.Id, "Attribute1", AccrualDetailProperty.AttributeId1), Browsable(false), EntityColumnConversion("DBNullToZero", "ZeroToDBNull", "AttributeIdConvertor", "Evolve.Clients.Lafarge.Cement.Business.Rebates", "Evolve.Clients.Lafarge.Cement.Business")]
		public Int32? AttributeId1
		{
			get { return _attributeId1; }
			set	{ _attributeId1 = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: AttributeId2
		private Int32? _attributeId2;
		[EntityColumn(AttributeColumn.Id, "Attribute2", AccrualDetailProperty.AttributeId2), Browsable(false), EntityColumnConversion("DBNullToZero", "ZeroToDBNull", "AttributeIdConvertor", "Evolve.Clients.Lafarge.Cement.Business.Rebates", "Evolve.Clients.Lafarge.Cement.Business")]
		public Int32? AttributeId2
		{
			get { return _attributeId2; }
			set	{ _attributeId2 = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: AttributeId3
		private Int32? _attributeId3;
		[EntityColumn(AttributeColumn.Id, "Attribute3", AccrualDetailProperty.AttributeId3), Browsable(false), EntityColumnConversion("DBNullToZero", "ZeroToDBNull", "AttributeIdConvertor", "Evolve.Clients.Lafarge.Cement.Business.Rebates", "Evolve.Clients.Lafarge.Cement.Business")]
		public Int32? AttributeId3
		{
			get { return _attributeId3; }
			set	{ _attributeId3 = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: AttributeId4
		private Int32? _attributeId4;
		[EntityColumn(AttributeColumn.Id, "Attribute4", AccrualDetailProperty.AttributeId4), Browsable(false), EntityColumnConversion("DBNullToZero", "ZeroToDBNull", "AttributeIdConvertor", "Evolve.Clients.Lafarge.Cement.Business.Rebates", "Evolve.Clients.Lafarge.Cement.Business")]
		public Int32? AttributeId4
		{
			get { return _attributeId4; }
			set	{ _attributeId4 = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: AttributeName1
		private String _attributeName1;
		[EntityColumn(AttributeColumn.Name, "Attribute1", AccrualDetailProperty.AttributeName1), DisplayName("Attribute 1")]
		public String AttributeName1
		{
			get { return _attributeName1; }
			set { _attributeName1 = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: AttributeName2
		private String _attributeName2;
		[EntityColumn(AttributeColumn.Name, "Attribute2", AccrualDetailProperty.AttributeName2), DisplayName("Attribute 2")]
		public String AttributeName2
		{
			get { return _attributeName2; }
			set { _attributeName2 = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: AttributeName3
		private String _attributeName3;
		[EntityColumn(AttributeColumn.Name, "Attribute3", AccrualDetailProperty.AttributeName3), DisplayName("Attribute 3")]
		public String AttributeName3
		{
			get { return _attributeName3; }
			set { _attributeName3 = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: AttributeName4
		private String _attributeName4;
		[EntityColumn(AttributeColumn.Name, "Attribute4", AccrualDetailProperty.AttributeName4), DisplayName("Attribute 4")]
		public String AttributeName4
		{
			get { return _attributeName4; }
			set { _attributeName4 = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: ProductId
		private Int32? _productId;
		[EntityColumn(RebateDetailColumn.ProductId, DBRebates.RebateDetail, AccrualDetailProperty.ProductId), Browsable(false)]
		public Int32? ProductId
		{
			get { return _productId; }
			set	{ _productId = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: ProductCode
		private String _productCode;
		[EntityColumn(ProductColumn.JDECode, DBRebates.Product, AccrualDetailProperty.ProductCode), DisplayName("Product Code")]
		public String ProductCode
		{
			get { return _productCode; }
			set { _productCode = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: ProductName
		private String _productName;
		[EntityColumn(ProductColumn.Name, DBRebates.Product, AccrualDetailProperty.ProductName), DisplayName("Product Name")]
		public String ProductName
		{
			get { return _productName; }
			set { _productName = value; }
		}
		#endregion

		// Rebate Delivery Window

		#region RELATED ENTITY PROPERTY: DeliveryWindowId
		private Int32? _deliveryWindowId;
		[EntityColumn(RebateDeliveryWindowColumn.DeliveryWindowId, DBRebates.RebateDeliveryWindow, AccrualDetailProperty.DeliveryWindowId)]
		[Browsable(false)]
		public Int32? DeliveryWindowId
		{
			get { return _deliveryWindowId; }
			set { _deliveryWindowId = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: DeliveryWindowCode
		private String _deliveryWindowCode;
		[EntityColumn(DeliveryWindowColumn.JDECode, DBRebates.DeliveryWindow, AccrualDetailProperty.DeliveryWindowCode)]
		[DisplayName("Delivery Window")]
		public String DeliveryWindowCode
		{
			get { return _deliveryWindowCode; }
			set { _deliveryWindowCode = value; }
		}

		#endregion


		// Rebate LeadTime

		#region RELATED ENTITY PROPERTY: LeadTimeId
		private Int32? _leadTimeId;
		[EntityColumn(RebateLeadTimeColumn.LeadTimeId, DBRebates.RebateLeadTime, AccrualDetailProperty.LeadTimeId)]
		[Browsable(false)]
		public Int32? LeadTimeId
		{
			get { return _leadTimeId; }
			set { _leadTimeId = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: LeadTimeCode
		private String _leadTimeCode;
		[EntityColumn(LeadTimeColumn.JDECode, DBRebates.LeadTime, AccrualDetailProperty.LeadTimeCode)]
		[DisplayName("Lead Time")]
		public String LeadTimeCode
		{
			get { return _leadTimeCode; }
			set { _leadTimeCode = value; }
		}

		#endregion

		// Delivery Window & Lead Time

		#region PROPERTY: PackingTypeEnum
		private PackingTypeEnum _packingTypeEnum;
		[EntityColumn(RebateDeliveryWindowColumn.PackingTypeEnum, DBRebates.RebateDeliveryWindow, AccrualDetailProperty.PackingTypeEnum)]
		[EntityColumnConversion("DBInt16ToPackingTypeEnum", "PackingTypeEnumToDBInt16", "PackingTypeEnumConvertor", "Evolve.Clients.Lafarge.Cement.Business.Rebates", "Evolve.Clients.Lafarge.Cement.Business")]
		[DisplayName("Packing Type")]
		public PackingTypeEnum PackingTypeEnum
		{
			get { return _packingTypeEnum; }
			set
			{
				_packingTypeEnum = value;
				SetColumnDirty(RebateDeliveryWindowProperty.PackingTypeEnum);
			}
		}
		#endregion

		// Accrual Detail
		
		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(AccrualDetailProperty.Id, true, true), DisplayName("Id"), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;

				if (_accrualDetailInvoices != null)
				{
					foreach (AccrualDetailInvoice accrualDetailInvoice in _accrualDetailInvoices.Where(adi => !adi.ExistsOnDatabase))
					{
						accrualDetailInvoice.AccrualDetailId = _id;
					}
				}

				if (this.ExistsOnDatabase)
				{
					SetColumnDirty(AccrualDetailProperty.Id);
				}
			}
		}
		#endregion

		#region PROPERTY: AccrualId
		private Int32 _accrualId;
		[EntityColumn(AccrualDetailProperty.AccrualId), Browsable(false)]
		public Int32 AccrualId
		{
			get { return _accrualId; }
			set
			{
				_accrualId = value;

				if (this.ExistsOnDatabase)
				{
					SetColumnDirty(AccrualDetailProperty.AccrualId);
				}
			}
		}
		#endregion

		#region PROPERTY: RebateDetailId
		private Int32? _rebateDetailId;
		[EntityColumn(AccrualDetailProperty.RebateDetailId), Browsable(false)]
		public Int32? RebateDetailId
		{
			get { return _rebateDetailId; }
			set
			{
				_rebateDetailId = value; 
				SetColumnDirty(AccrualDetailProperty.RebateDetailId); 
			}
		}
		#endregion

		#region PROPERTY: RebateDeliveryWindowId
		private Int32? _rebateDeliveryWindowId;
		[EntityColumn(AccrualDetailProperty.RebateDeliveryWindowId), Browsable(false)]
		public Int32? RebateDeliveryWindowId
		{
			get { return _rebateDeliveryWindowId; }
			set
			{
				_rebateDeliveryWindowId = value;
				SetColumnDirty(AccrualDetailProperty.RebateDeliveryWindowId);
			}
		}
		#endregion

		#region PROPERTY: RebateLeadTimeId
		private Int32? _rebateLeadTimeId;
		[EntityColumn(AccrualDetailProperty.RebateLeadTimeId), Browsable(false)]
		public Int32? RebateLeadTimeId
		{
			get { return _rebateLeadTimeId; }
			set
			{
				_rebateLeadTimeId = value;
				SetColumnDirty(AccrualDetailProperty.RebateLeadTimeId);
			}
		}
		#endregion

		#region PROPERTY: HierarchyId
		private Int32 _hierarchyId;
		[EntityColumn(AccrualDetailProperty.HierarchyId), Browsable(false)]
		public Int32 HierarchyId
		{
			get { return _hierarchyId; }
			set
			{
				_hierarchyId = value;
				SetColumnDirty(AccrualDetailProperty.HierarchyId);
			}
		}
		#endregion

		#region PROPERTY: HierarchyEntityId
		private Int32 _hierarchyEntityId;
		[EntityColumn(AccrualDetailProperty.HierarchyEntityId), Browsable(false)]
		public Int32 HierarchyEntityId
		{
			get { return _hierarchyEntityId; }
			set
			{
				_hierarchyEntityId = value;
				SetColumnDirty(AccrualDetailProperty.HierarchyEntityId);
			}
		}
		#endregion

		#region PROPERTY: ParentHierarchyId
		private Int32? _parentHierarchyId;
		[EntityColumn(AccrualDetailProperty.ParentHierarchyId), Browsable(false)]
		public Int32? ParentHierarchyId
		{
			get { return _parentHierarchyId; }
			set
			{
				_parentHierarchyId = value;
				SetColumnDirty(AccrualDetailProperty.ParentHierarchyId);
			}
		}
		#endregion

		#region PROPERTY: ParentHierarchyEntityId
		private Int32? _parentHierarchyEntityId;
		[EntityColumn(AccrualDetailProperty.ParentHierarchyEntityId), Browsable(false)]
		public Int32? ParentHierarchyEntityId
		{
			get { return _parentHierarchyEntityId; }
			set
			{
				_parentHierarchyEntityId = value;
				SetColumnDirty(AccrualDetailProperty.ParentHierarchyEntityId); 
			}
		}
		#endregion

		#region PROPERTY: Value
		private Decimal _value;
		[EntityColumn(AccrualDetailProperty.Value)]
		public Decimal Value
		{
			get { return _value; }
			set
			{
				_value = value;
				SetColumnDirty(AccrualDetailProperty.Value);
			}
		}
		#endregion

		#region PROPERTY: Rate
		private Decimal? _rate;
		[EntityColumn(AccrualDetailProperty.Rate)]
		public Decimal? Rate
		{
			get { return _rate; }
			set
			{ 
				_rate = value;
				SetColumnDirty(AccrualDetailProperty.Rate); 
			}
		}
		#endregion

		#region PROPERTY: SalesVolume
		private Decimal _salesVolume;
		[EntityColumn(AccrualDetailProperty.SalesVolume), DisplayName("Sales Volume")]
		public Decimal SalesVolume
		{
			get { return _salesVolume; }
			set
			{
				_salesVolume = value; 
				SetColumnDirty(AccrualDetailProperty.SalesVolume); 
			}
		}
		#endregion

		#region PROPERTY: SalesValue
		private Decimal _salesValue;
		[EntityColumn(AccrualDetailProperty.SalesValue), DisplayName("Sales Value")]
		public Decimal SalesValue
		{
			get { return _salesValue; }
			set
			{
				_salesValue = value;
				SetColumnDirty(AccrualDetailProperty.SalesVolume);
			}
		}
		#endregion
		
		#region PROPERTY: InsertSequence
		private Int32 _insertSequence;
		[EntityColumn(AccrualDetailColumn.InsertSequence)]
		[Browsable(false)]
		public Int32 InsertSequence
		{
			get { return _insertSequence; }
			set
			{
				_insertSequence = value;
				SetColumnDirty(AccrualDetailProperty.InsertSequence);
			}
		}
		#endregion
	
		#region RELATED ENTITY PROPERTY: HierarchyLevel
		private Int16 _hierarchyLevel;
		[EntityColumn(HierarchyColumn.Level, DBRebates.Hierarchy, AccrualDetailProperty.HierarchyLevel), Browsable(false)]
		public Int16 HierarchyLevel
		{
			get { return _hierarchyLevel; }
			set { _hierarchyLevel = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: RelatedEntityName
		private String _relatedEntityName;
		[EntityColumn(HierarchyColumn.RelatedEntityName, DBRebates.Hierarchy, AccrualDetailProperty.RelatedEntityName), Browsable(false)]
		public String RelatedEntityName
		{
		    get { return _relatedEntityName; }
		    set { _relatedEntityName = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: InvoiceLinePropertyName
		private String _InvoiceLinePropertyName;
		[EntityColumn(HierarchyColumn.InvoiceLinePropertyName, DBRebates.Hierarchy, AccrualDetailProperty.InvoiceLinePropertyName), Browsable(false)]
		public String InvoiceLinePropertyName
		{
			get { return _InvoiceLinePropertyName; }
			set { _InvoiceLinePropertyName = value; }
		}
		#endregion

		#region RELATED ENTITY: AccrualDetailInvoices
		private ExtendedBindingList<AccrualDetailInvoice> _accrualDetailInvoices;

		public ExtendedBindingList<AccrualDetailInvoice> AccrualDetailInvoices
		{
			get { return _accrualDetailInvoices; }
		}
		#endregion

		#endregion

		#region Constructors

		public AccrualDetail()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.AccrualDetail);

			JoinTable(DBRebates.DatabaseName, DBRebates.Hierarchy, DBRebates.Hierarchy + "." + HierarchyColumn.Id + " = " + DBRebates.AccrualDetail + "." + AccrualDetailColumn.HierarchyId);
			JoinTable(DBRebates.DatabaseName, DBRebates.RebateDetail, DBRebates.RebateDetail + "." + RebateDetailColumn.Id + " = " + DBRebates.AccrualDetail + "." + AccrualDetailColumn.RebateDetailId, EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.RebateDeliveryWindow, DBRebates.RebateDeliveryWindow + "." + RebateDeliveryWindowColumn.Id + " = " + DBRebates.AccrualDetail + "." + AccrualDetailColumn.RebateDeliveryWindowId, EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.RebateLeadTime, DBRebates.RebateLeadTime + "." + RebateLeadTimeColumn.Id + " = " + DBRebates.AccrualDetail + "." + AccrualDetailColumn.RebateLeadTimeId, EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.Attribute, "Attribute1" + "." + AttributeColumn.Id + " = " + DBRebates.RebateDetail + "." + RebateDetailColumn.AttributeId1, "Attribute1", EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.Attribute, "Attribute2" + "." + AttributeColumn.Id + " = " + DBRebates.RebateDetail + "." + RebateDetailColumn.AttributeId2, "Attribute2", EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.Attribute, "Attribute3" + "." + AttributeColumn.Id + " = " + DBRebates.RebateDetail + "." + RebateDetailColumn.AttributeId3, "Attribute3", EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.Attribute, "Attribute4" + "." + AttributeColumn.Id + " = " + DBRebates.RebateDetail + "." + RebateDetailColumn.AttributeId4, "Attribute4", EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.Product, DBRebates.Product + "." + ProductColumn.Id + " = " + DBRebates.RebateDetail + "." + RebateDetailColumn.ProductId, EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.DeliveryWindow, DBRebates.DeliveryWindow + "." + ProductColumn.Id + " = " + DBRebates.RebateDeliveryWindow + "." + RebateDeliveryWindowColumn.DeliveryWindowId, EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.LeadTime, DBRebates.LeadTime + "." + ProductColumn.Id + " = " + DBRebates.RebateLeadTime + "." + RebateLeadTimeColumn.LeadTimeId, EntityJoinType.LeftOuter);

			OrderBy =
				"{" + AccrualDetailProperty.AccrualId + "}, " +
				"{" + AccrualDetailProperty.HierarchyLevel + "}, " +
				"{" + AccrualDetailProperty.HierarchyEntityId + "}";

			_accrualDetailInvoices = new ExtendedBindingList<AccrualDetailInvoice>();
		}

		#endregion

		#region Business Logic

		public void AddHierarchyEntityJoin(Hierarchy hierarchy)
		{
			JoinTable(DBRebates.DatabaseName, hierarchy.RelatedEntityName, "HierarchyEntity.Id = " + DBRebates.AccrualDetail + "." + AccrualDetailColumn.HierarchyEntityId, "HierarchyEntity");
		}

		public AccrualDetail FindParent(ExtendedBindingList<AccrualDetail> accrualDetails, ExtendedBindingList<Hierarchy> hierarchys)
		{
			AccrualDetail parent = null;
			Hierarchy parentHierarchy = hierarchys.GetParent(this.HierarchyLevel);

			if (parentHierarchy != null)
			{
				foreach (AccrualDetail accrualDetail in accrualDetails)
				{
					if (accrualDetail.HierarchyId == parentHierarchy.Id && 
						accrualDetail.HierarchyEntityId == this.ParentHierarchyEntityId &&
						accrualDetail.RebateDetailId == this.RebateDetailId)
					{
						parent = accrualDetail;
						break;
					}
				}
			}

			return parent;
		}

		public static Int32 DeleteAccruals(Int32 divisionAccrualId)
		{
			AccrualDetail accrualDetail = new AccrualDetail();

			accrualDetail.ReadFilter =
				DBRebates.AccrualDetail + "." + AccrualDetailColumn.AccrualId + " in ( " +
					"select " + AccrualColumn.Id + " " +
					"from " + accrualDetail.DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.Accrual) + " " +
					"where " + AccrualColumn.DivisionAccrualId + " = " + divisionAccrualId.ToString() + " " +
					")";

			Int32 result = accrualDetail.DeleteForReadFilter();
			return result;
		}

		#endregion
	}

	public static class AccrualDetailExtensionMethods
	{
		public static AccrualDetail FindByRebateDeliveryWindowId(this IEnumerable<AccrualDetail> accrualDetails, Int32 hierarchyId, Int32 hierarchyEntityId, Int32 rebateDeliveryWindowId)
		{
			AccrualDetail matched = null;

			if (accrualDetails != null)
			{
				foreach (AccrualDetail accrualDetail in accrualDetails)
				{
					if (accrualDetail.HierarchyId == hierarchyId &&
						accrualDetail.HierarchyEntityId == hierarchyEntityId &&
						accrualDetail.RebateDeliveryWindowId == rebateDeliveryWindowId)
					{
						matched = accrualDetail;
						break;
					}
				}
			}

			return matched;
		}

		public static AccrualDetail FindByRebateLeadTimeId(this IEnumerable<AccrualDetail> accrualDetails, Int32 hierarchyId, Int32 hierarchyEntityId, Int32 rebateLeadTimeId)
		{
			AccrualDetail matched = null;

			if (accrualDetails != null)
			{
				foreach (AccrualDetail accrualDetail in accrualDetails)
				{
					if (accrualDetail.HierarchyId == hierarchyId &&
						accrualDetail.HierarchyEntityId == hierarchyEntityId &&
						accrualDetail.RebateLeadTimeId == rebateLeadTimeId)
					{
						matched = accrualDetail;
						break;
					}
				}
			}

			return matched;
		}

		public static AccrualDetail FindByRebateDetailId(this IEnumerable<AccrualDetail> accrualDetails, Int32 hierarchyId, Int32 hierarchyEntityId, Int32 rebateDetailId)
		{
			AccrualDetail accrualDetail = null;

			if (accrualDetails != null)
			{
				accrualDetail = accrualDetails.FirstOrDefault(ad => ad.HierarchyId == hierarchyId && ad.HierarchyEntityId == hierarchyEntityId && ad.RebateDetailId == rebateDetailId);
			}

			return accrualDetail;
		}

		public static void MatchInsertSequences(this IEnumerable<AccrualDetail> accrualDetails, IEnumerable<AccrualDetailInsertSequence> accrualDetailInsertSequences)
		{
			Int32 index = 0;

			foreach (AccrualDetail accrualDetail in accrualDetails.OrderBy(ad => ad.InsertSequence))
			{
				if (accrualDetailInsertSequences.ElementAt(index).Id == 0)
				{
					throw new ArgumentOutOfRangeException("AccrualDetailInsertSequence.Id Equals Zero");
				}

				if (accrualDetail.InsertSequence != accrualDetailInsertSequences.ElementAt(index).InsertSequence)
				{
					throw new ArgumentOutOfRangeException("AccrualDetailInsertSequence.InsertSequence Mismatch");
				}

				accrualDetail.Id = accrualDetailInsertSequences.ElementAt(index).Id;
				index++;
			}
		}

		public static void ReadAccrualDetailInvoice(this IEnumerable<AccrualDetail> accrualDetails)
		{
			if (accrualDetails != null && accrualDetails.Count() > 0)
			{
				// Build list of AccrualDetailId's we need to use
				String inClause =
					"select " + AccrualDetailColumn.Id + " " +
					"from " + DBRebates.AccrualDetail + " " +
					"where " + AccrualDetailColumn.AccrualId + " = " + accrualDetails.First().AccrualId.ToString();

				// Load Accrual Details
				AccrualDetailInvoice accrualDetailInvoice = new AccrualDetailInvoice();
				accrualDetailInvoice.ReadFilter = AccrualDetailInvoiceColumn.AccrualDetailId + " in ( " + inClause + " )";

				ExtendedBindingList<AccrualDetailInvoice> accrualDetailInvoices = (ExtendedBindingList<AccrualDetailInvoice>) accrualDetailInvoice.Read();

				// Now associate Accrual Details with the relevant Accrual record
				foreach (AccrualDetail accrualDetail in accrualDetails)
				{
					accrualDetail.AccrualDetailInvoices.Clear();
					accrualDetail.AccrualDetailInvoices.AddMultiple(accrualDetailInvoices.Where(adi => adi.AccrualDetailId == accrualDetail.Id));
				}
			}
		}
	}


	public class AccrualDetailColumn
	{
		public const string Id = "Id";
		public const string AccrualId = "AccrualId";
		public const string RebateDetailId = "RebateDetailId";
		public const string RebateDeliveryWindowId = "RebateDeliveryWindowId";
		public const string RebateLeadTimeId = "RebateLeadTimeId";
		public const string HierarchyId = "HierarchyId";
		public const string HierarchyEntityId = "HierarchyEntityId";
		public const string ParentHierarchyId = "ParentHierarchyId";
		public const string ParentHierarchyEntityId = "ParentHierarchyEntityId";
		public const string Value = "Value";
		public const string Rate = "Rate";
		public const string SalesVolume = "SalesVolume";
		public const string SalesValue = "SalesValue";
		public const string InsertSequence = "InsertSequence";
	}

	public class AccrualDetailProperty
	{
		public const string HierarchyCode = "HierarchyCode";
		public const string HierarchyName = "HierarchyName";
		public const string AttributeId1 = "AttributeId1";
		public const string AttributeId2 = "AttributeId2";
		public const string AttributeId3 = "AttributeId3";
		public const string AttributeId4 = "AttributeId4";
		public const string AttributeName1 = "AttributeName1";
		public const string AttributeName2 = "AttributeName2";
		public const string AttributeName3 = "AttributeName3";
		public const string AttributeName4 = "AttributeName4";
		public const string ProductId = "ProductId";
		public const string ProductCode = "ProductCode";
		public const string ProductName = "ProductName";
		public const string DeliveryWindowId = "DeliveryWindowId";
		public const string DeliveryWindowCode = "DeliveryWindowCode";
		public const string LeadTimeId = "LeadTimeId";
		public const string LeadTimeCode = "LeadTimeCode";
		public const string PackingTypeEnum = "PackingTypeEnum";
		public const string Id = "Id";
		public const string AccrualId = "AccrualId";
		public const string RebateDetailId = "RebateDetailId";
		public const string RebateDeliveryWindowId = "RebateDeliveryWindowId";
		public const string RebateLeadTimeId = "RebateLeadTimeId";
		public const string HierarchyId = "HierarchyId";
		public const string HierarchyEntityId = "HierarchyEntityId";
		public const string ParentHierarchyId = "ParentHierarchyId";
		public const string ParentHierarchyEntityId = "ParentHierarchyEntityId";
		public const string Value = "Value";
		public const string Rate = "Rate";
		public const string SalesVolume = "SalesVolume";
		public const string SalesValue = "SalesValue";
		public const string InsertSequence = "InsertSequence";
		public const string HierarchyLevel = "HierarchyLevel";
		public const string RelatedEntityName = "RelatedEntityName";
		public const string InvoiceLinePropertyName = "InvoiceLinePropertyName";
		public const string AccrualDetailInvoices = "AccrualDetailInvoices";
	}
}

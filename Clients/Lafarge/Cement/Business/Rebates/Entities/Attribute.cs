﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class Attribute : SqlServerEntity
	{
		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(AttributeColumn.Id, true, true), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{ 
				_id = value;
				SetColumnDirty(AttributeProperty.Id);
			}
		}
		#endregion

		#region PROPERTY: DivisionId
		private Int32 _divisionId;
		[EntityColumn(AttributeColumn.DivisionId, true), Browsable(false)]
		public Int32 DivisionId
		{
			get { return _divisionId; }
			set
			{
				_divisionId = value;
				SetColumnDirty(AttributeProperty.DivisionId);
			}
		}
		#endregion

		#region PROPERTY: JDECode
		private String _jdeCode;
		[EntityColumn(AttributeColumn.JDECode)]
		public String JDECode
		{
			get { return _jdeCode; }
			set
			{
				_jdeCode = value;
				SetColumnDirty(AttributeProperty.JDECode);
			}

		}
		#endregion

		#region PROPERTY: Name
		private String _name;
		[EntityColumn(AttributeColumn.Name)]
		public String Name
		{
			get { return _name; }
			set
			{
				_name = value;
				SetColumnDirty(AttributeProperty.Name);
			}
		}
		#endregion

		#region PROPERTY: Level
		private Int16 _level;
		[EntityColumn(AttributeColumn.Level), Browsable(false)]
		public Int16 Level
		{
			get { return _level; }
			set
			{
				_level = value;
				SetColumnDirty(AttributeProperty.Level);
			}
		}
		#endregion

		#endregion

		#region Constructors

		public Attribute()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.Attribute);
			OrderBy =
				"{" + AttributeProperty.Level + "}, " +
				"{" + AttributeProperty.Name + "}";
		}

		#endregion
	}

	public class AttributeColumn
	{
		public const string Id = "Id";
		public const string DivisionId = "DivisionId";
		public const string JDECode = "JDECode";
		public const string Name = "Name";
		public const string Level = "LevelNumber";
	}

	public class AttributeProperty
	{
		public const string Id = "Id";
		public const string DivisionId = "DivisionId";
		public const string JDECode = "JDECode";
		public const string Name = "Name";
		public const string Level = "Level";
	}
}

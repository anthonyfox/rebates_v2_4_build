﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
    public class AuditHeader : SqlServerEntity, ICloneable
    {
        #region Properties

        #region PROPERTY: Id

        private Int32 _id;

        [EntityColumn(AuditHeaderColumn.Id, true, true), Browsable(true)]
        [DisplayName(@"Header Id")]
        public Int32 Id
        {
            get { return _id; }
            set
            {
                _id = value;

                if (ExistsOnDatabase)
                {
                    SetColumnDirty(AuditHeaderColumn.Id);
                }
            }
        }

        #endregion

        #region PROPERTY: UserName

        private String _userName;

        [EntityColumn(AuditHeaderColumn.UserName)]
        [DisplayName(@"User")]
        public String User
        {
            get { return _userName; }
            set
            {
                _userName = value;
                SetColumnDirty(AuditHeaderColumn.UserName);
            }
        }

        #endregion

        #region PROPERTY: TimeStamp

        private DateTime _timeStamp;

        [EntityColumn(AuditHeaderColumn.TimeStamp, DBDateTime.Year, DBDateTime.Second)]
        [DisplayName(@"Time Stamp")]
        public DateTime TimeStamp
        {
            get { return _timeStamp; }
            set
            {
                _timeStamp = value;
                SetColumnDirty(AuditHeaderColumn.TimeStamp);
            }
        }

        #endregion

        #region PROPERTY: TableName

        private String _tableName;

        [EntityColumn(AuditHeaderColumn.TableName)]
        [DisplayName(@"Table")]
        public String TableName
        {
            get { return _tableName; }
            set
            {
                _tableName = value;
                SetColumnDirty(AuditHeaderColumn.TableName);
            }
        }

        #endregion

        #region PROPERTY: RecordId

        private Int32 _recordId;

        [EntityColumn(AuditHeaderColumn.RecordId)]
        [DisplayName(@"Record Id")]
        public Int32 RecordId
        {
            get { return _recordId; }
            set
            {
                _recordId = value;
                SetColumnDirty(AuditHeaderColumn.RecordId);
            }
        }

        #endregion

        #region PROPERTY: RebateId

        private Int32 _rebateId;

        [EntityColumn(AuditHeaderColumn.RebateId)]
        [DisplayName(@"Rebate Id")]
        public Int32 RebateId
        {
            get { return _rebateId; }
            set
            {
                _rebateId = value;
                SetColumnDirty(AuditHeaderColumn.RebateId);

            }
        }

        #endregion

        #region RELATED ENTITY PROPERTY: CustomerGroupName

        private String _customerGroupName;

        [EntityColumn(CustomerGroupNameViewColumn.GroupName, DBRebates.CustomerGroupNameView, AuditHeaderProperty.CustomerGroupName)]
        [DisplayName(@"Customer Group")]
        public String CustomerGroupName
        {
            get { return _customerGroupName; }
            set
            {
                _customerGroupName = value;
                SetColumnDirty(AuditHeaderColumn.CustomerName);
            }
        }

        #endregion

        #region RELATED ENTITY PROPERTY: CustomerName

        private String _customerName;

        [EntityColumn(CustomerColumn.Name, DBRebates.Customer, AuditHeaderProperty.CustomerName)]
        [DisplayName(@"Customer Name")]
        public String CustomerName
        {
            get { return _customerName; }
            set
            {
                _customerName = value;
                SetColumnDirty(AuditHeaderColumn.CustomerName);
            }
        }

        #endregion

        #region RELATED ENTITY PROPERTY: RebateNumber

        private Int32 _rebateNumber;

        [EntityColumn(RebateColumn.Number, DBRebates.Rebate, AuditHeaderProperty.RebateNumber)]
        [DisplayName(@"Rebate")]
        public Int32 RebateNumber
        {
            get { return _rebateNumber; }
            set
            {
                _rebateNumber = value;
            }
        }

        #endregion

        #region RELATED ENTITY PROPERTY: RebateVersion

        private Int16 _rebateVersion;

        [EntityColumn(RebateColumn.Version, DBRebates.Rebate, AuditHeaderProperty.RebateVersion)]
        [DisplayName(@"Version")]
        public Int16 RebateVersion
        {
            get { return _rebateVersion; }
            set
            {
                _rebateVersion = value;
            }
        }

        #endregion
        #region PROPERTY: DatabaseAction

        private DatabaseActionEnum _databaseActionEnum;

        [EntityColumn(AuditHeaderColumn.DatabaseActionEnum)]
        [EntityColumnConversion("DBInt16ToDatabaseActionEnum", "DatabaseActionEnumToDBDbInt16", "DatabaseActionEnumConvertor", "Evolve.Clients.Lafarge.Cement.Business.Rebates", "Evolve.Clients.Lafarge.Cement.Business")]
        [DisplayName(@"Database Action")]

        public DatabaseActionEnum DatabaseActionEnum
        {
            get { return _databaseActionEnum; }
            set
            {
                _databaseActionEnum = value;
                SetColumnDirty(AuditHeaderColumn.DatabaseActionEnum);

            }
        }

        #endregion

        #region COLLECTION: AuditDetails

        private ExtendedBindingList<AuditDetail> _auditDetails;

        public ExtendedBindingList<AuditDetail> AuditDetails
        {
            get
            {
                // Lazy load the collection
                if (_auditDetails == null)
                {
                    var auditDetail = new AuditDetail();
                    auditDetail.AuditHeaderId = Id;
                    auditDetail.AddReadFilter(AuditDetailProperty.AuditHeaderId, "=");

                    _auditDetails = (ExtendedBindingList<AuditDetail>)auditDetail.Read();

                    // Convert "NULL" into true nulls
                    foreach (var detail in _auditDetails)
                    {
                        if (detail.OldValue == "NULL")
                        {
                            detail.OldValue = null;
                        }
                        if (detail.NewValue == "NULL")
                        {
                            detail.NewValue = null;
                        }
                    }
                }
                return _auditDetails;
            }
        }

        #endregion

        #endregion

        #region Constructors

        public AuditHeader()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.AuditHeader);

            // Join Rebates table. Use OUTER JOIN in case the rebate has been deleted
            JoinTable(DBRebates.DatabaseName, DBRebates.Rebate, DBRebates.Rebate + "." + RebateColumn.Id + " = " + DBRebates.AuditHeader + "." + AuditHeaderColumn.RebateId, EntityJoinType.LeftOuter);

            // Join Customer table to Rebate table. 
            JoinTable(DBRebates.DatabaseName, DBRebates.Customer, DBRebates.Customer + "." + CustomerColumn.Id + " = " + DBRebates.Rebate + "." + AuditHeaderColumn.CustomerID, EntityJoinType.LeftOuter);

            // Join CustomerGroupName view to Rebate table. 
            JoinTable(DBRebates.DatabaseName, DBRebates.CustomerGroupNameView, DBRebates.CustomerGroupNameView + "." + CustomerGroupColumn.Id + " = " + DBRebates.Rebate + "." + AuditHeaderColumn.CustomerGroupID, EntityJoinType.LeftOuter);

            OrderBy = "{" + AuditHeaderProperty.TimeStamp + "}";
        }

        #endregion

        #region ICloneable

        public object Clone()
        {
            // Perform shallow clone to start
            AuditHeader auditHeader = (AuditHeader)this.MemberwiseClone();

            // Now deep clone collections

            // Return the clone
            return auditHeader;
        }


        #endregion

    }


    public class AuditHeaderColumn
    {
        public const string Id = "Id";
        public const string UserName = "UserName";
        public const string TimeStamp = "TimeStamp";
        public const string TableName = "TableName";
        public const string RebateId = "RebateId";
        public const string RecordId = "RecordId";
        public const string CustomerGroupID = "CUSTOMERGROUPID";
        public const string CustomerID = "CUSTOMERID";
        public const string CustomerName = "Name";
        public const string CustomerGroupName = "GROUPNAME";
        public const string DatabaseActionEnum = "DatabaseActionEnum";
    }

    public class AuditHeaderProperty
    {
        public const string Id = "Id";
        public const string UserName = "UserName";
        public const string TimeStamp = "TimeStamp";
        public const string TableName = "TableName";
        public const string RebateId = "RebateId";
        public const string RecordId = "RecordId";
        public const string CustomerGroupID = "CUSTOMERGROUPID";
        public const string CustomerID = "CUSTOMERID";
        public const string CustomerName = "Name";
        public const string CustomerGroupName = "GROUPNAME";
        public const string DatabaseActionEnum = "DatabaseActionEnum";
        public const string RebateNumber = "RebateNumber";
        public const string RebateVersion = "RebateVersion";
    }
}
﻿using System;
using System.ComponentModel;
using System.Linq;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class Hierarchy : SqlServerEntity
	{
		#region Properties
		
		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(HierarchyColumn.Id, true, true), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;
				SetColumnDirty(HierarchyProperty.Id);
			}
		}
		#endregion

		#region PROPERTY: DivisionId
		private Int32 _divisionId;
		[EntityColumn(HierarchyColumn.DivisionId, true), Browsable(false)]
		public Int32 DivisionId
		{
			get { return _divisionId; }
			set
			{
				_divisionId = value;
				SetColumnDirty(HierarchyProperty.DivisionId);
			}
		}
		#endregion

		#region PROPERTY: Level
		private Int16 _level;
		[EntityColumn(HierarchyColumn.Level)]
		public Int16 Level
		{
			get { return _level; }
			set 
			{
				_level = value;
				SetColumnDirty(HierarchyProperty.Level);
			}
		}
		#endregion

		#region PROPERTY: Name
		private String _name;
		[EntityColumn(HierarchyColumn.Name)]
		public String Name
		{
			get { return _name; }
			set 
			{
				_name = value;
				SetColumnDirty(HierarchyProperty.Name);
			}
		}
		#endregion

		#region PROPERTY: RelatedEntityName
		private String _relatedEntityName;
		[EntityColumn(HierarchyColumn.RelatedEntityName)]
		public String RelatedEntityName
		{
			get { return _relatedEntityName; }
			set
			{
				_relatedEntityName = value;
				SetColumnDirty(HierarchyProperty.RelatedEntityName);
			}
		}
		#endregion

		#region PROPERTY: InvoiceLinePropertyName
		private String _invoiceLinePropertyName;
		[EntityColumn(HierarchyColumn.InvoiceLinePropertyName)]
		public String InvoiceLinePropertyName
		{
			get { return _invoiceLinePropertyName; }
			set
			{
				_invoiceLinePropertyName = value;
				SetColumnDirty(HierarchyProperty.InvoiceLinePropertyName);
			}
		}
		#endregion

		#region PROPERTY: CustomerSearchModeEnum
		private CustomerSearchModeEnum _customerSearchModeEnum;
		[EntityColumn(HierarchyColumn.CustomerSearchModeEnum)]
		[EntityColumnConversion("DBInt16ToCustomerSearchModeEnum", "CustomerSearchModeEnumToDBInt16", "CustomerSearchModeEnumConvertor", "Evolve.Clients.Lafarge.Cement.Business.Rebates", "Evolve.Clients.Lafarge.Cement.Business")]
		public CustomerSearchModeEnum CustomerSearchModeEnum
		{
			get { return _customerSearchModeEnum; }
			set
			{
				_customerSearchModeEnum = value;
				SetColumnDirty(HierarchyProperty.CustomerSearchModeEnum);
			}
		}
		#endregion

		#endregion

		#region Constructors

		public Hierarchy()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.Hierarchy);
			OrderBy = "{" + HierarchyProperty.Level + "}";
		}

		#endregion

		#region Business Logic

		public Type RelatedEntityType()
		{
			Type type = null;

			if (_relatedEntityName.Length > 0)
			{
				type = Type.GetType(this.GetType().Namespace + "." + _relatedEntityName);
			}

			return type;
		}

		#endregion
	}

	public static class HierarchyExtensionMethods
	{
		public static Hierarchy Find(this ExtendedBindingList<Hierarchy> hierarchys, Int32 id)
		{
			return hierarchys.FirstOrDefault(h => h.Id == id);
		}

		public static Hierarchy GetParent(this ExtendedBindingList<Hierarchy> hierarchys, Int16 level)
		{
			Hierarchy parent = null;

			foreach (Hierarchy hierarchy in hierarchys)
			{
				if (hierarchy.Level < level)
				{
					if (parent == null)
					{
						parent = hierarchy;
					}
					else if (hierarchy.Level > parent.Level)
					{
						parent = hierarchy;
					}
				}
			}

			return parent;
		}

		public static Hierarchy GetChild(this ExtendedBindingList<Hierarchy> hierarchys, Int16 level)
		{
			Hierarchy child = null;

			foreach (Hierarchy hierarchy in hierarchys)
			{
				if (hierarchy.Level > level)
				{
					if (child == null)
					{
						child = hierarchy;
					}
					else if (hierarchy.Level < child.Level)
					{
						child = hierarchy;
					}
				}
			}

			return child;
		}


	}

	public class HierarchyColumn
	{
		public const string Id = "Id";
		public const string DivisionId = "DivisionId";
		public const string Level = "LevelNumber";
		public const string Name = "Name";
		public const string RelatedEntityName = "RelatedEntityName";
		public const string InvoiceLinePropertyName = "InvoiceLinePropertyName";
		public const string CustomerSearchModeEnum = "CustomerSearchModeEnum";
	}

	public class HierarchyProperty
	{
		public const string Id = "Id";
		public const string DivisionId = "DivisionId";
		public const string Level = "Level";
		public const string Name = "Name";
		public const string RelatedEntityName = "RelatedEntityName";
		public const string InvoiceLinePropertyName = "InvoiceLinePropertyName";
		public const string CustomerSearchModeEnum = "CustomerSearchModeEnum";
	}
}

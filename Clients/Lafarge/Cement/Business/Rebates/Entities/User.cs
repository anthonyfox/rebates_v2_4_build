﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class User : SqlServerEntity
	{
		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(UserColumn.Id, true, true), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;
				SetColumnDirty(UserProperty.Id);
			}
		}
		#endregion

		#region PROPERTY: Code
		private String _code;
		[EntityColumn(UserColumn.Code)]
		public String Code
		{
			get { return _code; }
			set 
			{
				_code = value;
				SetColumnDirty(UserProperty.Code);
			}
		}
		#endregion

		#region PROPERTY: Name
		private String _name;
		[EntityColumn(UserColumn.Name)]
		public String Name
		{
			get { return _name; }
			set 
			{
				_name = value;
				SetColumnDirty(UserProperty.Name);
			}
		}
		#endregion

		#region PROPERTY: Password
		private String _password;
		[EntityColumn(UserColumn.Password)]
		public String Password
		{
			get { return _password; }
			set 
			{
				_password = value;
				SetColumnDirty(UserProperty.Password);
			}
		}
		#endregion

		#region PROPERTY: IsActive
		private Boolean _isActive;
		[EntityColumn(UserColumn.IsActive)]
		public Boolean IsActive
		{
			get { return _isActive; }
			set 
			{
				_isActive = value;
				SetColumnDirty(UserProperty.IsActive);
			}
		}
		#endregion

		#endregion

		#region Related Entities

		#region RELATED ENTITY: UserDivision
		private ExtendedBindingList<UserDivision> _userDivisions;
		public ExtendedBindingList<UserDivision> UserDivisions
		{
			get
			{
				if (_userDivisions == null)
				{
					ReadUserDivision();
				}

				return _userDivisions;
			}
		}
		#endregion

		public void ReadUserDivision()
		{
			UserDivision userDivision = new UserDivision();

			userDivision.UserId = this.Id;
			userDivision.AddReadFilter(UserDivisionProperty.UserId, "=");

			_userDivisions = (ExtendedBindingList<UserDivision>) userDivision.Read();
		}

		public override void ReadEntityCollection()
		{
			base.ReadEntityCollection();
			ReadUserDivision();
		}

		public override void ClearEntityCollection()
		{
			base.ClearEntityCollection();
			_userDivisions = null;
		}

		#endregion

		#region Constructors

		public User()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.User);
		}

		#endregion
	}

	public class UserColumn
	{
		public const string Id = "Id";
		public const string Code = "Code";
		public const string Name = "Name";
		public const string Password = "Password";
		public const string IsActive = "IsActive";
	}

	public class UserProperty
	{
		public const string Id = "Id";
		public const string Code = "Code";
		public const string Name = "Name";
		public const string Password = "Password";
		public const string IsActive = "IsActive";
	}
}

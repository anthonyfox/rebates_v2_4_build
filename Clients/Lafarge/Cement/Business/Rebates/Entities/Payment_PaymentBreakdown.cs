﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class Payment_PaymentBreakdown : SqlServerEntity
	{
		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(Payment_PaymentBreakdownColumn.Id, true, true), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;
				SetColumnDirty(Payment_PaymentBreakdownProperty.Id);
			}
		}
		#endregion

		#region PROPERTY: DivisionId
		private Int32 _divisionId;
		[EntityColumn(Payment_PaymentBreakdownColumn.DivisionId), Browsable(false)]
		public Int32 DivisionId
		{
			get { return _divisionId; }
			set
			{
				_divisionId = value;
				SetColumnDirty(Payment_PaymentBreakdownProperty.DivisionId);
			}
		}
		#endregion

		#region PROPERTY: CreateUserId
		private Int32 _createUserId;
		[EntityColumn(Payment_PaymentBreakdownProperty.CreateUserId), Browsable(false)]
		public Int32 CreateUserId
		{
			get { return _createUserId; }
			set
			{
				_createUserId = value;
				SetColumnDirty(Payment_PaymentBreakdownProperty.CreateUserId);
			}
		}
		#endregion

		#region PROPERTY: CreateTime
		private DateTime _createTime;
		[EntityColumn(Payment_PaymentBreakdownProperty.CreateTime, DBDateTime.Year, DBDateTime.Second), Browsable(false)]
		public DateTime CreateTime
		{
			get { return _createTime; }
			set
			{
				_createTime = value;
				SetColumnDirty(Payment_PaymentBreakdownProperty.CreateTime); 
			}
		}
		#endregion

		#region PROPERTY: ActionUserId
		private Int32? _actionUserId;
		[EntityColumn(Payment_PaymentBreakdownProperty.ActionUserId), Browsable(false)]
		public Int32? ActionUserId
		{
			get { return _actionUserId; }
			set
			{
				_actionUserId = value;
				SetColumnDirty(Payment_PaymentBreakdownProperty.ActionUserId);
			}
		}
		#endregion

		#region PROPERTY: ActionTime
		private DateTime? _actionTime;
		[EntityColumn(Payment_PaymentBreakdownProperty.ActionTime, DBDateTime.Year, DBDateTime.Second), Browsable(false)]
		public DateTime? ActionTime
		{
			get { return _actionTime; }
			set
			{
				_actionTime = value;
				SetColumnDirty(Payment_PaymentBreakdownProperty.ActionTime);
			}
		}
		#endregion

		#region PROPERTY: PaymentStatusEnum
		private PaymentStatusEnum _paymentStatusEnum;
		[EntityColumn(Payment_PaymentBreakdownColumn.PaymentStatusEnum), EntityColumnConversion("DBInt16ToPaymentStatusEnum", "PaymentStatusEnumToDBInt16", "PaymentStatusEnumConvertor", "Evolve.Clients.Lafarge.Cement.Business.Rebates", "Evolve.Clients.Lafarge.Cement.Business"), Browsable(false)]
		public PaymentStatusEnum PaymentStatusEnum
		{
			get { return _paymentStatusEnum; }
			set
			{
				_paymentStatusEnum = value;
				SetColumnDirty(PaymentProperty.PaymentStatusEnum);
			}
		}
		#endregion

		#region PROPERTY: RejectionComment
		private String _rejectionComment;
		[EntityColumn(Payment_PaymentBreakdownProperty.RejectionComment)]
		public String RejectionComment
		{
			get { return _rejectionComment; }
			set
			{
				_rejectionComment = value;
				SetColumnDirty(Payment_PaymentBreakdownProperty.RejectionComment);
			}
		}
		#endregion
		#endregion

		#region Constructors

		public Payment_PaymentBreakdown()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.Payment_PaymentBreakdown);
		}

		#endregion
	}

	public class Payment_PaymentBreakdownColumn
	{
		public const string Id = "Id";
		public const string DivisionId = "DivisionId";
		public const string CreateUserId = "CreateUserId";
		public const string CreateTime = "CreateTime";
		public const string ActionUserId = "ActionUserId";
		public const string ActionTime = "ActionTime";
		public const string PaymentStatusEnum = "PaymentStatusEnum";
		public const string RejectionComment = "RejectionComment";
	}

	public class Payment_PaymentBreakdownProperty
	{
		public const string Id = "Id";
		public const string DivisionId = "DivisionId";
		public const string CreateUserId = "CreateUserId";
		public const string CreateTime = "CreateTime";
		public const string ActionUserId = "ActionUserId";
		public const string ActionTime = "ActionTime";
		public const string PaymentStatusEnum = "PaymentStatusEnum";
		public const string RejectionComment = "RejectionComment";
	}

}

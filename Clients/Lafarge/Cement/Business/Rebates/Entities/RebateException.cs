﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class RebateException : SqlServerEntity, ICloneable
	{
		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(RebateExceptionColumn.Id, true, true)]
		[Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;

				if (this.ExistsOnDatabase)
				{
					SetColumnDirty(RebateExceptionProperty.Id);
				}
			}
		}
		#endregion

		#region PROPERTY: RebateId
		private Int32 _rebateId;
		[EntityColumn(RebateExceptionColumn.RebateId)]
		[Browsable(false)]
		public Int32 RebateId
		{
			get { return _rebateId; }
			set
			{
				_rebateId = value;
				SetColumnDirty(RebateExceptionProperty.RebateId);
			}
		}
		#endregion

		#region PROPERTY: ExceptionTypeEnum
		private ExceptionTypeEnum _exceptionTypeEnum;
		[EntityColumn(RebateExceptionColumn.ExceptionTypeEnum), EntityColumnConversion("DBIntToExceptionTypeEnum", "ExceptionTypeEnumToDBInt", "ExceptionTypeEnumConvertor", "Evolve.Clients.Lafarge.Cement.Business.Rebates", "Evolve.Clients.Lafarge.Cement.Business")]
		[Browsable(false)]
		public ExceptionTypeEnum ExceptionTypeEnum
		{
			get { return _exceptionTypeEnum; }
			set
			{
				_exceptionTypeEnum = value;
				SetColumnDirty(RebateExceptionProperty.ExceptionTypeEnum);
			}
		}
		#endregion

		#region PROPERTY: ExceptionId
		private Int32 _exceptionId;
		[EntityColumn(RebateExceptionColumn.ExceptionId)]
		[Browsable(false)]
		public Int32 ExceptionId
		{
			get { return _exceptionId; }
			set
			{
				_exceptionId = value;
				SetColumnDirty(RebateExceptionProperty.ExceptionId);
			}
		}
		#endregion

		#endregion

		#region Constructors

		public RebateException()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.RebateException);
		}

		#endregion

		#region ICloneable

		public object Clone()
		{
			return this.MemberwiseClone();
		}

		#endregion

		#region Business Logic
		public static Int32 DeleteRebateExceptions(Int32 rebateId, ExceptionTypeEnum exceptionTypeEnum)
		{
			RebateException rebateException = new RebateException();
			rebateException.RebateId = rebateId;
			rebateException.ExceptionTypeEnum = exceptionTypeEnum;
			
			rebateException.AddReadFilter(RebateExceptionProperty.RebateId, "=");
			rebateException.AddReadFilter(RebateExceptionProperty.ExceptionTypeEnum, "=");

			return rebateException.DeleteForReadFilter();
		}
		#endregion
	}

	public class RebateExceptionColumn
	{
		public const string Id = "Id";
		public const string RebateId = "RebateId";
		public const string ExceptionTypeEnum = "ExceptionTypeEnum";
		public const string ExceptionId = "ExceptionId";
	}

	public class RebateExceptionProperty
	{
		public const string Id = "Id";
		public const string RebateId = "RebateId";
		public const string ExceptionTypeEnum = "ExceptionTypeEnum";
		public const string ExceptionId = "ExceptionId";
	}
}

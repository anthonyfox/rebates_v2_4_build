﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class TaxRate : SqlServerEntity, ICloneable
	{
		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(TaxRateColumn.Id, true, true), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;
				SetColumnDirty(TaxRateProperty.Id);
			}
		}
		#endregion

		#region PROPERTY: TaxCodeId
		private Int32 _taxCodeId;
		[EntityColumn(TaxRateColumn.TaxCodeId)]
		[Browsable(false)]
		public Int32 TaxCodeId
		{
			get { return _taxCodeId; }
			set
			{
				_taxCodeId = value;
				SetColumnDirty(TaxRateProperty.TaxCodeId);
			}
		}
		#endregion

		#region RELATED PROPERTY: Code
		private String _code;
		[EntityColumn(TaxCodeColumn.Code, DBRebates.TaxCode, TaxRateProperty.Code)]
		public String Code
		{
			get { return _code; }
			set
			{
				_code = value;
			}
		}
		#endregion

		#region RELATED PROPERTY: IsZeroRate
		private Boolean _isZeroRate;
		[EntityColumn(TaxCodeColumn.IsZeroRate, DBRebates.TaxCode, TaxRateProperty.IsZeroRate)]
		[DisplayName("Zero Rate")]
		public Boolean IsZeroRate
		{
			get { return _isZeroRate; }
			set
			{
				_isZeroRate = value;

				if (_isZeroRate)
				{
					this.Rate = 0;
				}
			}
		}
		#endregion

		#region PROPERTY: Rate
		private Decimal _rate;
		[EntityColumn(TaxRateColumn.Rate)]
		[Browsable(false)]
		public Decimal Rate
		{
			get { return _rate; }
			set
			{
				_rate = value;
				SetColumnDirty(TaxRateProperty.Rate);
			}
		}

		[DisplayName("Rate")]
		public String FormattedRate
		{
			get { return _rate.ToString("0.00"); }
			set { Decimal.TryParse(value, out _rate); }
		}
		#endregion

		#region PROPERTY: EffectiveDate
		private DateTime _effectiveDate = DateTime.Today.AddDays(1);
		[EntityColumn(TaxRateColumn.EffectiveDate, DBDateTimeFrom=DBDateTime.Year, DBDateTimeTo=DBDateTime.Day)]
		[DisplayName("Effective Date")]
		public DateTime EffectiveDate
		{
			get { return _effectiveDate; }
			set
			{
				_effectiveDate = value;
				SetColumnDirty(TaxRateProperty.EffectiveDate);
			}
		}
		#endregion

		#region PROPERTY: CreateUserId
		private Int32 _createUserId;
		[EntityColumn(TaxRateColumn.CreateUserId)]
		[Browsable(false)]
		public Int32 CreateUserId
		{
			get { return _createUserId; }
			set
			{
				_createUserId = value;
				SetColumnDirty(TaxRateProperty.CreateUserId);
			}
		}
		#endregion

		#region PROPERTY: CreateTime
		private DateTime _createTime;
		[EntityColumn(TaxRateColumn.CreateTime, DBDateTimeFrom = DBDateTime.Year, DBDateTimeTo = DBDateTime.Day)]
		[Browsable(false)]
		public DateTime CreateTime
		{
			get { return _createTime; }
			set
			{
				_createTime = value;
				SetColumnDirty(TaxRateProperty.CreateTime);
			}
		}
		#endregion

		#region PROPERTY: UpdateUserId
		private Int32? _updateUserId;
		[EntityColumn(TaxRateColumn.UpdateUserId)]
		[Browsable(false)]
		public Int32? UpdateUserId
		{
			get { return _updateUserId; }
			set
			{
				_updateUserId = value;
				SetColumnDirty(TaxRateProperty.UpdateUserId);
			}
		}
		#endregion

		#region PROPERTY: UpdateTime
		private DateTime? _updateTime;
		[EntityColumn(TaxRateColumn.UpdateTime, DBDateTimeFrom = DBDateTime.Year, DBDateTimeTo = DBDateTime.Day)]
		[Browsable(false)]
		public DateTime? UpdateTime
		{
			get { return _updateTime; }
			set
			{
				_updateTime = value;
				SetColumnDirty(TaxRateProperty.UpdateTime);
			}
		}
		#endregion

		[Browsable(false)]
		public Boolean CanDelete
		{
			get
			{
				return true;
			}
		}
	
		#region PROPERTY: JDETaxRate
		private String _jdeTaxRate;
		[EntityColumn(TaxRateColumn.JDETaxRate)]
		[DisplayName("JDE Rate Code")]
		public String JDETaxRate
		{
			get { return _jdeTaxRate; }
			set
			{
				_jdeTaxRate = value;
				SetColumnDirty(TaxRateProperty.JDETaxRate);
			}
		}
		#endregion

		#region PROPERTY: JDEExplanation
		private String _jdeExplanation;
		[EntityColumn(TaxRateColumn.JDEExplanation)]
		[DisplayName("JDE Explanation")]
		public String JDEExplanation
		{
			get { return _jdeExplanation; }
			set
			{
				_jdeExplanation = value;
				SetColumnDirty(TaxRateProperty.JDEExplanation);
			}
		}
		#endregion

		#region Constructors

		public TaxRate()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.TaxRate);
			JoinTable(DBRebates.DatabaseName, DBRebates.TaxCode, DBRebates.TaxCode + "." + TaxCodeColumn.Id + " = " + DBRebates.TaxRate + "." + TaxRateColumn.TaxCodeId);
			OrderBy = TaxCodeColumn.Code + ", " + TaxRateColumn.EffectiveDate + " desc";
		}

		#endregion

		#region ICloneable

		public Object Clone()
		{
			return (TaxRate) this.MemberwiseClone();
		}

		#endregion

		#region Database Events

		public override int BeforeInsert()
		{
			this.CreateTime = DateTime.Now;
			return base.BeforeInsert();
		}

		public override int BeforeUpdate()
		{
			this.UpdateTime = DateTime.Now;
			return base.BeforeUpdate();
		}

		#endregion

		#region Validation

		public static String ValidateRate(String rateText)
		{
			String error = "";
			Decimal value;

			if (String.IsNullOrEmpty(rateText))
			{
				error = "A rate must be specified";
			}
			else
			{
				if (Decimal.TryParse(rateText, out value))
				{
					if (value < 0)
					{
						error = "Rate must be a positive value";
					}
				}
				else
				{
					error = "Invalid rate, must be a number";
				}
			}

			return error;
		}

		public static String ValidateJDERate(String jdeRate, Int32 taxRateId)
		{
			String error = "";

			if (String.IsNullOrEmpty(jdeRate))
			{
				error = "JDE Rate Code must be specified";
			}
			else
			{
				TaxRate taxRate = new TaxRate();
				taxRate.JDETaxRate = jdeRate;

				ExtendedBindingList<TaxRate> taxRates = (ExtendedBindingList<TaxRate>) taxRate.ReadWithDirtyProperty();

				if (taxRates.Count > 0 && taxRates[0].Id != taxRateId)
				{
					error = "JDE Tax Rate Code must be unique";
				}
			}

			return error;
		}

		#endregion
	}

	public class TaxRateColumn
	{
		public const string Id = "Id";
		public const string TaxCodeId = "TaxCodeId";
		public const string EffectiveDate = "EffectiveDate";
		public const string Rate = "Rate";
		public const string CreateUserId = "CreateUserId";
		public const string CreateTime = "CreateTime";
		public const string UpdateUserId = "UpdateUserId";
		public const string UpdateTime = "UpdateTime";
		public const string JDETaxRate = "JDETaxRate";
		public const string JDEExplanation = "JDEExplanation";
	}

	public class TaxRateProperty
	{
		public const string Id = "Id";
		public const string TaxCodeId = "TaxCodeId";
		public const string Code = "Code";
		public const string IsZeroRate = "IsZeroRate";
		public const string EffectiveDate = "EffectiveDate";
		public const string Rate = "Rate";
		public const string FormattedRate = "FormattedRate";
		public const string CreateUserId = "CreateUserId";
		public const string CreateTime = "CreateTime";
		public const string UpdateUserId = "UpdateUserId";
		public const string UpdateTime = "UpdateTime";
		public const string JDETaxRate = "JDETaxRate";
		public const string JDEExplanation = "JDEExplanation";
	}
}

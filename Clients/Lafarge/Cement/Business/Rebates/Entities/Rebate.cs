﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;


namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class Rebate : SqlServerEntity, ICloneable
	{
		#region Properties

		private const Int16 MIN_VERSION = 2001;
		private const Int16 MAX_VERSION = 2199;

		private const Int16 MIN_PERIOD = 1;
		private const Int16 MAX_PERIOD = 12;

		private String _integerFormat = "#,0";

		#region DATAGRIDVIEW PROPERTY: IsSelected

		private Boolean _isSelected = true;

		[DisplayName("Is Selected")]
		public Boolean IsSelected
		{
			get { return _isSelected; }
			set { _isSelected = value; NotifyPropertyChanged(RebateProperty.IsSelected); }
		}

		#endregion

		#region PROPERTY: PaymentPeriod

		private Int16 _paymentPeriod = 12;
		/// <summary>
		/// Set this value to the accounting period upto which accruals, payments, and closures
		/// should be included in the read (Default = 12)
		/// </summary>
		[Browsable(false)]
		public Int16 PaymentPeriod
		{
			get { return _paymentPeriod; }
			set { _paymentPeriod = value; }
		}

		#endregion

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(RebateColumn.Id, true, true), DisplayName("Rebate Id"), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;

				if (ExistsOnDatabase)
				{
					SetColumnDirty(RebateProperty.Id);
				}
				else
				{
					foreach (RebateDeliveryWindow rebateDeliveryWindow in this.RebateDeliveryWindows)
					{
						rebateDeliveryWindow.ExistsOnDatabase = false;
						rebateDeliveryWindow.RebateId = this.Id;
					}

					foreach (RebateDetail rebateDetail in this.RebateDetails)
					{
						rebateDetail.ExistsOnDatabase = false;
						rebateDetail.RebateId = this.Id;
					}

					foreach (RebateException rebateException in this.RebateExceptions)
					{
						rebateException.ExistsOnDatabase = false;
						rebateException.RebateId = this.Id;
					}

					foreach (RebateLeadTime rebateLeadTime in this.RebateLeadTimes)
					{
						rebateLeadTime.ExistsOnDatabase = false;
						rebateLeadTime.RebateId = this.Id;
					}

					foreach (RebateNote rebateNote in this.RebateNotes)
					{
						rebateNote.ExistsOnDatabase = false;
						rebateNote.RebateId = this.Id;
					}

					foreach (RebateRange rebateRange in this.RebateRanges)
					{
						rebateRange.ExistsOnDatabase = false;
						rebateRange.RebateId = this.Id;
					}

					foreach (RebateVAP rebateVAP in this.RebateVAPs)
					{
						rebateVAP.ExistsOnDatabase = false;
						rebateVAP.RebateId = this.Id;
					}
				}
			}
		}
		#endregion

		#region PROPERTY: DivisionId
		private Int32 _divisionId;
		[EntityColumn(RebateColumn.DivisionId), Browsable(false)]
		public Int32 DivisionId
		{
			get { return _divisionId; }
			set
			{
				_divisionId = value;
				SetColumnDirty(RebateProperty.DivisionId);
			}
		}
		#endregion

		#region PROPERTY: Number
		private Int32 _number;
		[EntityColumn(RebateColumn.Number), DisplayName("Rebate Number")]
		public Int32 Number
		{
			get { return _number; }
			set
			{
				_number = value;
				SetColumnDirty(RebateProperty.Number);
			}
		}
		#endregion

		#region PROPERTY: Version
		private Int16 _version;
		[EntityColumn(RebateColumn.Version), DisplayName("Rebate Version")]
		public Int16 Version
		{
			get { return _version; }
			set
			{
				_version = value;
				SetColumnDirty(RebateProperty.Version);
			}
		}
		#endregion

		#region PROPERTY: RebateTypeId
		private Int32 _rebateTypeId;
		[EntityColumn(RebateColumn.RebateTypeId), Browsable(false)]
		public Int32 RebateTypeId
		{
			get { return _rebateTypeId; }
			set
			{
				_rebateTypeId = value;
				SetColumnDirty(RebateProperty.RebateTypeId);
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: RebateTypeName
		private String _rebateTypeName;
		[EntityColumn(RebateTypeColumn.Name, DBRebates.RebateType, RebateProperty.RebateTypeName)]
		[DisplayName("Type")]
		public String RebateTypeName
		{
			get { return _rebateTypeName; }
			set
			{
				_rebateTypeName = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: HierarchyLevel
		private Int16 _hierarchyLevel;
		[EntityColumn(HierarchyColumn.Level, DBRebates.Hierarchy, RebateProperty.HierarchyLevel)]
		[Browsable(false)]
		public Int16 HierarchyLevel
		{
			get { return _hierarchyLevel; }
			set
			{
				_hierarchyLevel = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: HierarchyName
		private String _hierarchyName;
		[EntityColumn(HierarchyColumn.Name, DBRebates.Hierarchy, RebateProperty.HierarchyName)]
		[DisplayName("Hierarchy")]
		public String HierarchyName
		{
			get { return _hierarchyName; }
			set
			{
				_hierarchyName = value;
			}
		}
		#endregion

		#region DERIVED PROPETY: HierarchyRelatedName
		[DisplayName("Name")]
		public String HierarchyRelatedName
		{
			get
			{
				if (this.HierarchyLevel == 1)
				{
					return this.CustomerGroupName;
				}
				else
				{
					return this.CustomerName;
				}
			}
		}
		#endregion

		#region DERIVED PROPERTY: ParentGroup
		[DisplayName("Parent Group")]
		public String ParentGroup
		{
			get
			{
				String parentGroup = "";

				switch (this.HierarchyLevel)
				{
					case 1:
						// Rebate is setup at Major Group Level
						parentGroup = this.CustomerGroupName;
						break;

					case 2:
						// Rebate is setup at Sold To Level, return the Customer Group Id
						// from the sold to account
						parentGroup = this.SoldToGroupName;
						break;

					case 3:
						// Rebate is setup at Ship To Level, return the ship to accounts parent customer
						// customer group id
						parentGroup = this.ShipToGroupName;
						break;

					default:
						throw new ArgumentException("Unexpected Hierarchy Level (" + this.HierarchyLevel.ToString() + ") for Rebate Id " + this.Id.ToString());
				}

				return parentGroup;
			}
		}
		#endregion
		
		#region PROPERTY: Description
		private String _description;
		[EntityColumn(RebateColumn.Description)]
		public String Description
		{
			get { return _description; }
			set
			{
				_description = (value.Length > 100 ? value.Substring(0, 100) : value);
				SetColumnDirty(RebateProperty.Description);
			}
		}
		#endregion

		#region PROPERTY: PayeeId
		private Int32? _payeeId;
		[EntityColumn(RebateColumn.PayeeId)]
		[Browsable(false)]
		public Int32? PayeeId
		{
			get { return _payeeId; }
			set
			{
				_payeeId = value;
				SetColumnDirty(RebateProperty.PayeeId);
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: PayeeName
		private String _payeeJDECode;
		[EntityColumn(PayeeColumn.JDECode, DBRebates.Payee, RebateProperty.PayeeJDECode)]
		[DisplayName("Payee JDE Code")]
		public String PayeeJDECode
		{
			get { return _payeeJDECode; }
			set
			{
				_payeeJDECode = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: PayeeName
		private String _payeeName;
		[EntityColumn(PayeeColumn.Name, DBRebates.Payee, RebateProperty.PayeeName)]
		[DisplayName("Payee")]
		public String PayeeName
		{
			get { return _payeeName; }
			set
			{
				_payeeName = value;
			}
		}
		#endregion

		#region PROPERTY: StartDate
		private DateTime _startDate;
		[EntityColumn(RebateColumn.StartDate, DBDateTime.Year, DBDateTime.Day), DisplayName("Start Date")]
		public DateTime StartDate
		{
			get { return _startDate; }
			set
			{
				_startDate = value;
				SetColumnDirty(RebateProperty.StartDate);
				this.Version = (Int16) _startDate.Year;
			}
		}
		#endregion

		#region PROPERTY: EndDate
		private DateTime? _endDate;
		[EntityColumn(RebateColumn.EndDate, DBDateTime.Year, DBDateTime.Day), DisplayName("End Date")]
		public DateTime? EndDate
		{
			get { return _endDate; }
			set
			{
				_endDate = value;
				SetColumnDirty(RebateProperty.EndDate);
			}
		}
		#endregion

		#region PROPERTY: PaymentFrequencyId
		private Int32 _paymentFrequencyId;
		[EntityColumn(RebateColumn.PaymentFrequencyId), Browsable(false)]
		public Int32 PaymentFrequencyId
		{
			get { return _paymentFrequencyId; }
			set
			{
				_paymentFrequencyId = value;
				SetColumnDirty(RebateProperty.PaymentFrequencyId);
			}
		}
		#endregion

		#region PROPERTY: EstimatedVolume
		private Int32? _estimatedVolume;
		[EntityColumn(RebateColumn.EstimatedVolume)]
		[DisplayName("Estimated Volume")]
		[Browsable(false)]
		public Int32? EstimatedVolume
		{
			get { return _estimatedVolume; }
			set
			{
				_estimatedVolume = value;
				SetColumnDirty(RebateProperty.EstimatedVolume);
			}
		}
		#endregion

		#region DERIVED PROPERTY: EstimatedVolumeString
		[DisplayName("Estimated Volume")]
		public String EstimatedVolumeString
		{
			get
			{
				if (_estimatedVolume.HasValue)
				{
					return _estimatedVolume.Value.ToString(_integerFormat);
				}

				return "";
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: PaymentFrequencyName
		private String _paymentFrequencyName;
		[EntityColumn(PaymentFrequencyColumn.Name, DBRebates.PaymentFrequency, RebateProperty.PaymentFrequencyName)]
		[DisplayName("Payment Frequency")]
		public String PaymentFrequencyName
		{
			get { return _paymentFrequencyName; }
			set
			{
				_paymentFrequencyName = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: IsForecast
		private Boolean _isForecast;
		[EntityColumn(RebateTypeColumn.IsForecast, DBRebates.RebateType, RebateProperty.IsForecast)]
		[Browsable(false)]
		public Boolean IsForecast
		{
			get { return _isForecast; }
			set
			{
				_isForecast = value;
				SetColumnDirty(RebateProperty.IsForecast);
			}
		}
		#endregion

		#region PROPERTY: IsAdjustment
		private Boolean _isAdjustment = false;
		[EntityColumn(RebateColumn.IsAdjustment)]
		[DisplayName("Adjustment")]
		public Boolean IsAdjustment
		{
			get { return _isAdjustment; }
			set
			{
				_isAdjustment = value;
				SetColumnDirty(RebateProperty.IsAdjustment);
			}
		}
		#endregion

		#region PROPERTY: IsJif
		private Boolean _isJif = false;
		[EntityColumn(RebateColumn.IsJif)]
		[DisplayName("JIF")]
		public Boolean IsJif
		{
			get { return _isJif; }
			set
			{
				_isJif = value;
				SetColumnDirty(RebateProperty.IsJif);
			}
		}
		#endregion

		#region PROPERTY: IsContract
		private Boolean _isContract;
		[EntityColumn(RebateColumn.IsContract)]
		[DisplayName("Contract")]
		public Boolean IsContract
		{
			get { return _isContract; }
			set
			{
				_isContract = value;
				SetColumnDirty(RebateProperty.IsContract);
			}
		}
		#endregion

		#region PROPERTY: ContractText
		private String _contractText;
		[EntityColumn(RebateColumn.ContractText), DisplayName("Contract")]
		public String ContractText
		{
			get { return _contractText; }
			set
			{
				_contractText = (value.Length > 100 ? value.Substring(0, 100) : value);
				SetColumnDirty(RebateProperty.ContractText);
			}
		}
		#endregion

		#region PROPERTY: IsVAP
		private Boolean _isVAP;
		[EntityColumn(RebateColumn.IsVAP)]
		[DisplayName("VAP")]
		public Boolean IsVAP
		{
			get { return _isVAP; }
			set
			{
				_isVAP = value;
				SetColumnDirty(RebateProperty.IsVAP);
			}
		}
		#endregion

		#region PROPERTY: VAPActivationVolume
		private Int32? _vapActivationVolume;
		[EntityColumn(RebateColumn.VAPActivationVolume)]
		[DisplayName("VAP Activation Volume")]
		public Int32? VAPActivationVolume
		{
			get { return _vapActivationVolume; }
			set
			{
				_vapActivationVolume = value;
				SetColumnDirty(RebateProperty.VAPActivationVolume);
			}
		}
		#endregion

		#region PROPERTY: IsGlobalProduct
		private Boolean _isGlobalProduct;
		[EntityColumn(RebateColumn.IsGlobalProduct)]
		[DisplayName("Global Product")]
		[Browsable(false)]
		public Boolean IsGlobalProduct
		{
			get { return _isGlobalProduct; }
			set
			{
				_isGlobalProduct = value;
				SetColumnDirty(RebateProperty.IsGlobalProduct);
			}
		}
		#endregion

		#region PROPERTY: GlobalRate
		private Decimal? _globalRate;
		[EntityColumn(RebateColumn.GlobalRate)]
		[DisplayName("Global Rate")]
		[Browsable(false)]
		public Decimal? GlobalRate
		{
			get { return _globalRate; }
			set
			{
				_globalRate = value;
				SetColumnDirty(RebateProperty.GlobalRate);
			}
		}
		#endregion

		#region PROPERTY: FixedRebateValue
		private Int32? _fixedRebateValue;
		[EntityColumn(RebateColumn.FixedRebateValue)]
		[DisplayName("Fixed Rebate Value")]
		[Browsable(false)]
		public Int32? FixedRebateValue
		{
			get { return _fixedRebateValue; }
			set
			{
				_fixedRebateValue = value;
				SetColumnDirty(RebateProperty.FixedRebateValue);
			}
		}
		#endregion

		#region DERIVED PROPERTY: FixedRebateValueString
		[DisplayName("Fixed Rebate Value")]
		public String FixedRebateValueString
		{
			get
			{
				if (_fixedRebateValue.HasValue)
				{
					return _fixedRebateValue.Value.ToString(_integerFormat);
				}

				return "";
			}
		}
		#endregion

		#region PROPERTY: IsAccrued
		[DisplayName("Is Accrued")]
		[EntityColumn(RebateColumn.IsAccrued)]
		public Boolean IsAccrued
		{
			get { return _accrualValue.HasValue; }
			set { }
		}
		#endregion

		#region FUNCTION PROPERTY: AccrualValue
		private Decimal? _accrualValue;

		[EntityColumn("AccrualTotal(Rebate.Id, {%PaymentPeriod%})", ColumnExpression = EntityColumnExpression.Function, ColumnAlias = "AccrualValue")]
		[DisplayName("Accrual Value")]
		public Decimal? AccrualValue
		{
			get { return _accrualValue; }
			set { _accrualValue = value; }
		}
		#endregion

		#region FUNCTION PROPERTY: PaymentValue

		private Decimal? _paymentValue;

		[EntityColumn("PaymentTotal(Rebate.Id, {%PaymentPeriod%})", ColumnExpression = EntityColumnExpression.Function, ColumnAlias = "PaymentValue")]
		[DisplayName("Payment Value")]
		public Decimal? PaymentValue
		{
			get { return _paymentValue; }
			set { _paymentValue = value; }
		}

		#endregion

		#region FUNCTION PROPERTY: ClosedValue

		private Decimal? _closedValue;

		[EntityColumn("ClosedTotal(Rebate.Id, {%PaymentPeriod%})", ColumnExpression = EntityColumnExpression.Function, ColumnAlias = "ClosedValue")]
		[DisplayName("Closed Value")]
		public Decimal? ClosedValue
		{
			get { return _closedValue; }
			set { _closedValue = value; }
		}

		#endregion

		#region DERIVED PROPERTY: Balance

		public Decimal? Balance
		{
			get
			{
				if (_accrualValue.HasValue)
				{
					Decimal paymentValue = (_paymentValue.HasValue ? _paymentValue.Value : 0);
					Decimal closedValue = (_closedValue.HasValue ? _closedValue.Value : 0);

					return (_accrualValue.Value - paymentValue - closedValue);
				}
				else
				{
					return null;
				}
			}
		}

		#endregion

		#region DERIVED PROPERTY: HasPaymentOutstanding

		[Browsable(false)]
		public Boolean HasPaymentOutstanding
		{
			get { return (this.Balance.HasValue && this.Balance > 0); }
		}

		#endregion

		#region PROPERTY: HierarchyId
		private Int32 _hierarchyId;
		[EntityColumn(RebateColumn.HierarchyId)]
		[Browsable(false)]
		public Int32 HierarchyId
		{
			get { return _hierarchyId; }
			set
			{
				_hierarchyId = value;
				SetColumnDirty(RebateProperty.HierarchyId);
			}
		}
		#endregion

		#region PROPERTY: HierarchyEntityId
		private Int32 _hierarchyEntityId;
		[EntityColumn(RebateColumn.HierarchyEntityId)]
		[Browsable(false)]
		public Int32 HierarchyEntityId
		{
			get { return _hierarchyEntityId; }
			set
			{
				_hierarchyEntityId = value;
				SetColumnDirty(RebateProperty.HierarchyEntityId);
			}
		}
		#endregion

		#region PROPERTY: PricingSubGroupId
		private Int32? _pricingSubGroupId;
		[EntityColumn(RebateColumn.PricingSubGroupId)]
		[Browsable(false)]
		public Int32? PricingSubGroupId
		{
			get { return _pricingSubGroupId; }
			set
			{
				_pricingSubGroupId = value;
				SetColumnDirty(RebateProperty.PricingSubGroupId);
			}
		}
		#endregion

		#region PROPERTY: DeliveryTypeEnum
		private DeliveryTypeEnum _deliveryTypeEnum;
		[EntityColumn(RebateColumn.DeliveryTypeEnum), EntityColumnConversion("DBInt16ToDeliveryTypeEnum", "DeliveryTypeEnumToDBInt16", "DeliveryTypeEnumConvertor", "Evolve.Clients.Lafarge.Cement.Business.Rebates", "Evolve.Clients.Lafarge.Cement.Business")]
		[Browsable(false)]
		public DeliveryTypeEnum DeliveryTypeEnum
		{
			get { return _deliveryTypeEnum; }
			set
			{
				_deliveryTypeEnum = value;
				SetColumnDirty(RebateProperty.DeliveryTypeEnum);
			}
		}
		#endregion

		#region PROPERTY: SiteId
		private Int32? _siteId;
		[EntityColumn(RebateColumn.SiteId)]
		[Browsable(false)]
		public Int32? SiteId
		{
			get { return _siteId; }
			set
			{
				_siteId = value;
				SetColumnDirty(RebateProperty.SiteId);
			}
		}
		#endregion

		#region PROPERTY: CreateUserId
		private Int32 _createUserId;
		[EntityColumn(RebateColumn.CreateUserId)]
		[Browsable(false)]
		public Int32 CreateUserId
		{
			get { return _createUserId; }
			set
			{
				_createUserId = value;
				SetColumnDirty(RebateProperty.CreateUserId);
			}
		}
		#endregion

		#region PROPERTY: CreateTime
		private DateTime _createTime;
		[EntityColumn(RebateColumn.CreateTime, DBDateTime.Year, DBDateTime.Second)]
		[Browsable(false)]
		public DateTime CreateTime
		{
			get { return _createTime; }
			set
			{
				_createTime = value;
				SetColumnDirty(RebateProperty.CreateTime);
			}
		}
		#endregion

		#region PROPERTY: UpdateUserId
		private Int32? _updateUserId;
		[EntityColumn(RebateColumn.UpdateUserId), Browsable(false)]
		public Int32? UpdateUserId
		{
			get { return _updateUserId; }
			set
			{
				_updateUserId = value;
				SetColumnDirty(RebateProperty.UpdateUserId);
			}
		}
		#endregion

		#region PROPERTY: UpdateTime
		private DateTime? _updateTime;
		[EntityColumn(RebateColumn.UpdateTime, DBDateTime.Year, DBDateTime.Second)]
		[Browsable(false)]
		public DateTime? UpdateTime
		{
			get { return _updateTime; }
			set
			{
				_updateTime = value;
				SetColumnDirty(RebateProperty.UpdateTime);
			}
		}
		#endregion

		#region PROPERTY: InterfaceTime
		private DateTime? _interfaceTime;
		[EntityColumn(RebateColumn.InterfaceTime, DBDateTime.Year, DBDateTime.Second)]
		[Browsable(false)]
		public DateTime? InterfaceTime
		{
			get { return _interfaceTime; }
			set
			{
				_interfaceTime = value;
				SetColumnDirty(RebateProperty.InterfaceTime);
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: RelatedEntityName
		private String _relatedEntityName;
		[EntityColumn(HierarchyColumn.RelatedEntityName, DBRebates.Hierarchy, RebateProperty.RelatedEntityName)]
		[Browsable(false)]
		public String RelatedEntityName
		{
			get { return _relatedEntityName; }
			set
			{
				_relatedEntityName = value;
			}
		}
		#endregion


		#region RELATED ENTITY PROPERTY: InvoiceLinePropertyName
		private String _InvoiceLinePropertyName;
		[EntityColumn(HierarchyColumn.InvoiceLinePropertyName, DBRebates.Hierarchy, RebateProperty.InvoiceLinePropertyName)]
		[Browsable(false)]
		public String InvoiceLinePropertyName
		{
			get { return _InvoiceLinePropertyName; }
			set
			{
				_InvoiceLinePropertyName = value;
			}
		}
		#endregion

		#region CALCULATION PROPERTY: EndPeriod (Used during the calculation of Fixed Rebates)

		[Browsable(false)]
		public Int16 EndPeriod
		{
			get { return (Int16) (this.EndDate == null ? 12 : this.EndDate.Value.Month); }
		}

		#endregion

		#region CALCULATION PROPERTY: NumberOfPeriods (Used during the calculation of Fixed Rebates)

		[Browsable(false)]
		public int NumberOfPeriods
		{
			get
			{
				int numberOfPeriods = (this.EndPeriod - this.StartDate.Month) + 1;

				// No take into account number of periods for which we have no sales
				numberOfPeriods -= (SalesVolumes == null ? 0 : SalesVolumes.Count(sv => sv == 0));

				// Return number of periods
				return numberOfPeriods;
			}
		}

		#endregion

		#region CALCULATION PROPERTY: PeriodSalesVolume (Used during the calculation of Fixed Rebates)

		public List<Decimal> SalesVolumes {	get; set; }

		#endregion

		#region CALCULATION PROPERTY: Accruals (Used during the calculation of Fixed Rebates)
		public List<Accrual> Accruals { get; set; }
		#endregion

		#region CALCULATION PROPERTY: PostedAccruals (Used during the calculation of Fixed Rebates)
		public List<List<Accrual>> PostedAccruals { get; set; }
		#endregion

		#region DERIVED PROPERTY: WindowTitle

		[Browsable(false)]
		public String WindowTitle
		{
			get { return "Rebate (" + (_number == 0 ? "New" : _number.ToString() + " Version " + _version.ToString()) + ")"; }
		}

		#endregion

		#region RELATED ENTITY PROPERTY: CustomerGroupName
		private String _customerGroupName;
		[EntityColumn(CustomerGroupColumn.Name, DBRebates.CustomerGroup, RebateProperty.CustomerGroupName)]
		[Browsable(false)]
		public String CustomerGroupName
		{
			get { return _customerGroupName; }
			set
			{
				_customerGroupName = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: CustomerName
		private String _customerName;
		[EntityColumn(CustomerColumn.Name, DBRebates.Customer, RebateProperty.CustomerName)]
		[Browsable(false)]
		public String CustomerName
		{
			get { return _customerName; }
			set
			{
				_customerName = value;
			}
		}
		#endregion

		#region Properties Used for ParentGroup
		#region RELATED ENTITY PROPERTY: SoldToGroupName
		private String _soldToGroupName;
		[EntityColumn(CustomerGroupColumn.Name, DBRebates.SoldToGroup, RebateProperty.SoldToGroupName)]
		[Browsable(false)]
		public String SoldToGroupName
		{
			get { return _soldToGroupName; }
			set { _soldToGroupName = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: ShipToGroupName
		private String _shipToGroupName;
		[EntityColumn(CustomerGroupColumn.Name, DBRebates.ShipToGroup, RebateProperty.ShipToGroupName)]
		[Browsable(false)]
		public String ShipToGroupName
		{
			get { return _shipToGroupName; }
			set { _shipToGroupName = value; }
		}
		#endregion

		#endregion

		#region Properties Used Only to Provide Hierarchy Drill Down Search Functionality

		#region RELATED ENTITY PROPERTY: CustomerGroupId
		private Int32 _customerGroupId;
		[EntityColumn(CustomerColumn.CustomerGroupId, DBRebates.Customer, RebateProperty.CustomerGroupId)]
		[Browsable(false)]
		public Int32 CustomerGroupId
		{
			get { return _customerGroupId; }
			set { _customerGroupId = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: ParentCustomerId
		private Int32 _parentCustomerId;
		[EntityColumn(CustomerColumn.SoldToId, DBRebates.ParentCustomer, RebateProperty.ParentCustomerId)]
		[Browsable(false)]
		public Int32 ParentCustomerId
		{
			get { return _parentCustomerId; }
			set { _parentCustomerId = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: ParentCustomerGroupId
		private Int32 _parentCustomerGroupId;
		[EntityColumn(CustomerColumn.CustomerGroupId, DBRebates.ParentCustomer, RebateProperty.ParentCustomerGroupId)]
		[Browsable(false)]
		public Int32 ParentCustomerGroupId
		{
			get { return _parentCustomerGroupId; }
			set	{ _parentCustomerGroupId = value; }
		}
		#endregion

		[Browsable(false)]
		public Int32 SearchCustomerGroupId
		{
			get
			{
				Int32 searchId = -1;

				switch (this.HierarchyLevel)
				{
					case 1:
						// Rebate is setup at Major Group Level
						searchId = this.HierarchyEntityId;
						break;

					case 2:
						// Rebate is setup at Sold To Level, return the Customer Group Id
						// from the sold to account
						searchId = this.CustomerGroupId;
						break;

					case 3:
						// Rebate is setup at Ship To Level, return the ship to accounts parent customer
						// customer group id
						searchId = this.ParentCustomerGroupId;
						break;

					default:
						throw new ArgumentException("Unexpected Hierarchy Level (" + this.HierarchyLevel.ToString() + ") for Rebate Id " + this.Id.ToString());
				}

				return searchId;
			}
		}

		#endregion

		#endregion

		#region Constructors

		public Rebate()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.Rebate);

			JoinTable(DBRebates.DatabaseName, DBRebates.Hierarchy, DBRebates.Hierarchy + "." + HierarchyColumn.Id + " = " + DBRebates.Rebate + "." + RebateColumn.HierarchyId);
			JoinTable(DBRebates.DatabaseName, DBRebates.RebateType, DBRebates.RebateType + "." + RebateTypeColumn.Id + " = " + DBRebates.Rebate + "." + RebateColumn.RebateTypeId);
			JoinTable(DBRebates.DatabaseName, DBRebates.Payee, DBRebates.Payee + "." + PayeeColumn.Id + " = " + DBRebates.Rebate + "." + RebateColumn.PayeeId, EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.PaymentFrequency, DBRebates.PaymentFrequency + "." + PaymentFrequencyColumn.Id + " = " + DBRebates.Rebate + "." + RebateColumn.PaymentFrequencyId);

			// Join related hierarchy entities (use outer join as we are not sure which one it will be)
			JoinTable(DBRebates.DatabaseName, DBRebates.CustomerGroup, DBRebates.CustomerGroup + "." + CustomerGroupColumn.Id + " = " + DBRebates.Rebate + "." + RebateColumn.HierarchyEntityId, EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.Customer, DBRebates.Customer + "." + CustomerColumn.Id + " = " + DBRebates.Rebate + "." + RebateColumn.HierarchyEntityId, EntityJoinType.LeftOuter);

			// Outer join customer back to customer to read parent customer (Sold To) details for Rebates setup
			// at Ship To level (used for hierarchy drill-down search functionality)
			JoinTable(DBRebates.DatabaseName, DBRebates.Customer, DBRebates.ParentCustomer + "." + CustomerColumn.Id + " = " + DBRebates.Customer + "." + CustomerColumn.SoldToId, DBRebates.ParentCustomer, EntityJoinType.LeftOuter);

			// Joins for ParentGroup Details
			JoinTable(DBRebates.DatabaseName, DBRebates.CustomerGroup, DBRebates.SoldToGroup + "." + CustomerGroupColumn.Id + " = " + DBRebates.Customer + "." + CustomerColumn.CustomerGroupId, DBRebates.SoldToGroup, EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.CustomerGroup, DBRebates.ShipToGroup + "." + CustomerGroupColumn.Id + " = " + DBRebates.ParentCustomer + "." + CustomerColumn.CustomerGroupId, DBRebates.ShipToGroup, EntityJoinType.LeftOuter);

			OrderBy =
				"{" + RebateProperty.Number + "}, " +
				"{" + RebateProperty.Version + "} desc";
		}

		#endregion

		#region ICloneable

		public object Clone()
		{
			// Perform shallow clone to start
			Rebate rebate = (Rebate) this.MemberwiseClone();

			// Now deep clone collections
			if (_rebateDetails != null)	rebate.RebateDetails = _rebateDetails.Clone();
			if (_rebateDeliveryWindows != null)	rebate.RebateDeliveryWindows = _rebateDeliveryWindows.Clone();
			if (_rebateLeadTimes != null) rebate.RebateLeadTimes = _rebateLeadTimes.Clone();
			if (_rebateExceptions != null) rebate.RebateExceptions = _rebateExceptions.Clone();
			if (_rebateRanges != null) rebate.RebateRanges = _rebateRanges.Clone();
			if (_rebateNotes != null) rebate.RebateNotes = _rebateNotes.Clone();
			if (_rebateVAPs != null) rebate.RebateVAPs = _rebateVAPs.Clone();

			// Clear balances
			this.AccrualValue = null;
			this.PaymentValue = null;
			this.ClosedValue = null;

			// Return the clone
			return rebate;
		}

		#endregion

		#region Business Logic

		#region Related Entities

		#region RebateDetails

		private ExtendedBindingList<RebateDetail> _rebateDetails;

		public ExtendedBindingList<RebateDetail> RebateDetails
		{
			get
			{
				if (_rebateDetails == null)
				{
					ReadRebateDetail();
				}

				return _rebateDetails;
			}
			set
			{
				_rebateDetails = value;
			}
		}

		public void ReadRebateDetail()
		{
			RebateDetail rebateDetail = new RebateDetail();
			rebateDetail.RebateId = this.Id;
			rebateDetail.AddReadFilter("RebateId", "=");
			_rebateDetails = (ExtendedBindingList<RebateDetail>) rebateDetail.Read();
		}

		#endregion

		#region RebateDeliveryWindows

		private ExtendedBindingList<RebateDeliveryWindow> _rebateDeliveryWindows;

		public ExtendedBindingList<RebateDeliveryWindow> RebateDeliveryWindows
		{
			get
			{
				if (_rebateDeliveryWindows == null)
				{
					ReadRebateDeliveryWindow();
				}

				return _rebateDeliveryWindows;
			}
			set
			{
				_rebateDeliveryWindows = value;
			}
		}

		public void ReadRebateDeliveryWindow()
		{
			RebateDeliveryWindow rebateDeliveryWindow = new RebateDeliveryWindow();
			rebateDeliveryWindow.RebateId = this.Id;
			rebateDeliveryWindow.AddReadFilter("RebateId", "=");
			_rebateDeliveryWindows = (ExtendedBindingList<RebateDeliveryWindow>) rebateDeliveryWindow.Read();
		}

		#endregion

		#region RebateLeadTimes

		private ExtendedBindingList<RebateLeadTime> _rebateLeadTimes;

		public ExtendedBindingList<RebateLeadTime> RebateLeadTimes
		{
			get
			{
				if (_rebateLeadTimes == null)
				{
					ReadRebateLeadTime();
				}

				return _rebateLeadTimes;
			}
			set
			{
				_rebateLeadTimes = value;
			}
		}

		public void ReadRebateLeadTime()
		{
			RebateLeadTime rebateLeadTime = new RebateLeadTime();
			rebateLeadTime.RebateId = this.Id;
			rebateLeadTime.AddReadFilter("RebateId", "=");
			_rebateLeadTimes = (ExtendedBindingList<RebateLeadTime>) rebateLeadTime.Read();
		}

		#endregion

		#region RebateExceptions

		private ExtendedBindingList<RebateException> _rebateExceptions;

		public ExtendedBindingList<RebateException> RebateExceptions
		{
			get
			{
				if (_rebateExceptions == null)
				{
					ReadRebateException();
				}

				return _rebateExceptions;
			}
			set
			{
				_rebateExceptions = value;
			}
		}

		public void ReadRebateException()
		{
			RebateException rebateException = new RebateException();
			rebateException.RebateId = this.Id;
			rebateException.AddReadFilter(RebateExceptionProperty.RebateId, "=");
			_rebateExceptions = (ExtendedBindingList<RebateException>) rebateException.Read();
		}

		#endregion

		#region RebateRanges

		private ExtendedBindingList<RebateRange> _rebateRanges;

		public ExtendedBindingList<RebateRange> RebateRanges
		{
			get
			{
				if (_rebateRanges == null)
				{
					ReadRebateRange();
				}

				return _rebateRanges;
			}
			set
			{
				_rebateRanges = value;
			}
		}

		public void ReadRebateRange()
		{
			RebateRange rebateRange = new RebateRange();
			rebateRange.RebateId = this.Id;
			rebateRange.AddReadFilter(RebateRangeProperty.RebateId, "=");
			_rebateRanges = (ExtendedBindingList<RebateRange>) rebateRange.Read();
		}

		#endregion

		#region RebateNotes

		private ExtendedBindingList<RebateNote> _rebateNotes;

		public ExtendedBindingList<RebateNote> RebateNotes
		{
			get
			{
				if (_rebateNotes == null)
				{
					ReadRebateNote();
				}

				return _rebateNotes;
			}
			set
			{
				_rebateNotes = value;
			}
		}

		public void ReadRebateNote()
		{
			RebateNote rebateNote = new RebateNote();
			rebateNote.RebateId = this.Id;
			rebateNote.AddReadFilter(RebateNoteProperty.RebateId, "=");
			rebateNote.OrderBy = RebateNoteColumn.CreateTime + " desc";
			_rebateNotes = (ExtendedBindingList<RebateNote>) rebateNote.Read();
		}

		#endregion

		#region RebateVAPs

		private ExtendedBindingList<RebateVAP> _rebateVAPs;

		public ExtendedBindingList<RebateVAP> RebateVAPs
		{
			get
			{
				if (_rebateVAPs == null)
				{
					ReadRebateVAP();
				}

				return _rebateVAPs;
			}
			set
			{
				_rebateVAPs = value;
			}
		}

		public void ReadRebateVAP()
		{
			RebateVAP rebateVAP = new RebateVAP();
			rebateVAP.RebateId = this.Id;
			rebateVAP.AddReadFilter(RebateVAPProperty.RebateId, "=");
			_rebateVAPs = (ExtendedBindingList<RebateVAP>) rebateVAP.Read();
		}

		#endregion

		public override void ReadEntityCollection()
		{
			base.ReadEntityCollection();

			ReadRebateDetail();
			ReadRebateDeliveryWindow();
			ReadRebateLeadTime();
			ReadRebateException();
			ReadRebateRange();
			ReadRebateNote();
			ReadRebateVAP();
		}

		#endregion

		#region Entity Events

		public override int AfterInsert()
		{
			int result = base.AfterInsert();

			if (result >= 0)
			{
				result = SetupPriceEnquiryRates();
			}
				
			if (result >= 0)
			{
				if (_number == 0)
				{
					this.Number = _id;
					result = Update();
				}
			}

			return result;
		}

		public override Int32 BeforeUpdate()
		{
			Int32 result = base.BeforeUpdate();

			if (result >= 0)
			{
				this.InterfaceTime = null;
				result = SetupPriceEnquiryRates();
			}

			return result;
		}

		private Int32 SetupPriceEnquiryRates()
		{
			Int32 result = -1;

			RebateType rebateType = new RebateType();
			rebateType.Id = this.RebateTypeId;

			if (rebateType.ReadCurrent())
			{
				RebateTypeCalculator rebateTypeCalculator = rebateType.GetCalculator(this);

				if (rebateTypeCalculator != null)
				{
					rebateTypeCalculator.CalculatePriceEnquiryRate();
					result = 0;
				}
			}

			return result;
		}

		#endregion

		#region Validation

		#region Validate: Id
		public static String ValidateId(String id)
		{
			return ValidateId(id, false);
		}

		public static String ValidateId(String id, Boolean allowNull)
		{
			String errorText = "";
			Int32 value;

			if (id.Trim() == "")
			{
				if (!allowNull)
				{
					errorText = "Rebate Id must be entered";
				}
			}
			else
			{
				if (Int32.TryParse(id, out value))
				{
					if (value <= 0)
					{
						errorText = "Rebate Id must be greater than zero";
					}
				}
				else
				{
					errorText = "Rebate Id should be numeric";
				}
			}

			return errorText;
		}
		#endregion

		#region Validate: Version
		public static String ValidateVersion(String version)
		{
			return ValidateVersion(version, false);
		}

		public static String ValidateVersion(String version, Boolean allowNull)
		{
			String errorText = "";
			Int16 value;


			if (version.Trim() == "")
			{
				if (!allowNull)
				{
					errorText = "Rebate Version must be entered";
				}
			}
			else
			{
				if (Int16.TryParse(version, out value))
				{
					if (value < MIN_VERSION || value > MAX_VERSION)
					{
						errorText = "Rebate Version must be between " + MIN_VERSION.ToString() + " and " + MAX_VERSION.ToString();
					}
				}
				else
				{
					errorText = "Rebate Version should be numeric";
				}
			}

			return errorText;
		}
		#endregion

		#region Validate: Description
		public static String ValidateDescription(String description)
		{
			return (description.Length > 0 ? "" : "Description must be specified");
		}
		#endregion

		#region Validate: StartDate/EndDate
		public static String ValidateDates(DateTime? startDate, DateTime? endDate, CalculationPeriod lastAccrualPeriod)
		{
			String error = "";

			if (startDate == null)
			{
				error = "Start Date must be specified";
			}
			else
			{
				error = (endDate.HasValue && startDate.Value.Date > endDate.Value.Date ? "Start Date cannot be after End Date" : "");
			}

			if (String.IsNullOrEmpty(error) && lastAccrualPeriod != null && startDate.HasValue)
			{
				error = (startDate.Value.Year < lastAccrualPeriod.Year ? "Rebate cannot be in a previous accrual year" : "");
			}

			return error;
		}
		#endregion

		#region Validate: ContractText
		public static String ValidateContractText(Boolean isContract, String contractText)
		{
			String errorText = "";

			if (isContract && contractText.Length == 0)
			{
				errorText = "Contract must be specified";
			}

			return errorText;
		}
		#endregion

		#region Validate: GlobalRate
		public static String ValidateGlobalRate(Boolean isGlobalProduct, String globalRate)
		{
			String errorText = "";
			Decimal value;

			if (isGlobalProduct)
			{
				if (Decimal.TryParse(globalRate, out value))
				{
					if (value < 0)
					{
						errorText = "Global rate cannot be negative";
					}
				}
				else
				{
					errorText = "Global rate value must be entered";
				}
			}

			return errorText;
		}
		#endregion

		#region Validate: EstimatedVolume
		public static String ValidateEstimatedVolume(Boolean isEstimated, String estimatedVolume)
		{
			String errorText = "";
			Int32 value = 0;

			if (isEstimated)
			{
				if (Int32.TryParse(estimatedVolume.Replace(",", ""), out value))
				{
					if (value < 0)
					{
						errorText = "Estimated volume cannot be negative";
					}
				}
				else
				{
					errorText = "Estimated volume must be entered";
				}
			}

			return errorText;
		}
		#endregion

		#region Validate: FixedRebateValue
		public static String ValidateFixedRebateValue(Boolean isFixed, String fixedRebateValue)
		{
			String errorText = "";
			Int32 value;

			if (isFixed)
			{
				if (Int32.TryParse(fixedRebateValue.Replace(",", ""), out value))
				{
					if (value < 0)
					{
						errorText = "Fixed rebate value cannot be negative";
					}
				}
				else
				{
					errorText = "Fixed rebate value must be entered";
				}
			}

			return errorText;
		}
		#endregion

		#region Validate: Year
		public static String ValidateYear(String version, Boolean allowNull)
		{
			String errorText = "";
			Int16 value;


			if (version.Trim() == "")
			{
				if (!allowNull)
				{
					errorText = "Year must be entered";
				}
			}
			else
			{
				if (Int16.TryParse(version, out value))
				{
					if (value < MIN_VERSION || value > MAX_VERSION)
					{
						errorText = "Year must be between " + MIN_VERSION.ToString() + " and " + MAX_VERSION.ToString();
					}
				}
				else
				{
					errorText = "Year should be numeric";
				}
			}

			return errorText;
		}


		#region Validate: Period
		public static String ValidatePeriod(String period, Boolean allowNull)
		{
			String errorText = "";
			Int16 value;


			if (period.Trim() == "")
			{
				if (!allowNull)
				{
					errorText = "Period must be entered";
				}
			}
			else
			{
				if (Int16.TryParse(period, out value))
				{
					if (value < MIN_PERIOD || value > MAX_PERIOD)
					{
						errorText = "Period must be between " + MIN_PERIOD.ToString() + " and " + MAX_PERIOD.ToString();
					}
				}
				else
				{
					errorText = "Period should be numeric";
				}
			}

			return errorText;
		}
		#endregion
		#endregion

		#region Validate: Rate (GENERIC)
		public static String ValidateRate(String rate)
		{
			return ValidateRate(rate, false);
		}

		public static String ValidateRate(String rate, Boolean isPercentage)
		{
			const Decimal MAX_PERCENTAGE_RATE = 100;
			const Decimal MIN_PERCENTAGE_RATE = -100;

			const Decimal MAX_NORMAL_RATE = 10000000; // 10 Million
			const Decimal MIN_NORMAL_RATE = -10000000; // 10 Million
			
			String errorText = "";
			Decimal value;
			String rateName = isPercentage ? "Percentage Rate" : "Rate";
			String percentageSymbol = isPercentage ? "%" : "";

			if (Decimal.TryParse(rate, out value))
			{
				if (value != 0)
				{
					Decimal minValue = isPercentage ? MIN_PERCENTAGE_RATE : MIN_NORMAL_RATE;
					Decimal maxValue = isPercentage ? MAX_PERCENTAGE_RATE : MAX_NORMAL_RATE;

					if (value < minValue || value > maxValue)
					{
						errorText = rateName + " must be between " + minValue.ToString() + percentageSymbol + " and " + maxValue.ToString() + percentageSymbol;
					}
				}
			}
			else
			{
				errorText = rateName + " must be entered";
			}

			return errorText;
		}
		#endregion
		
		#endregion

		#region Copy Rebate

		public Rebate Copy(User user)
		{
			// Copy the rebate
			Rebate rebate = (Rebate) this.Clone();

			// Reset the id's of rebate and all related data
			rebate.Id = 0;
			rebate.Number = 0;
			rebate.CreateTime = DateTime.Now;
			rebate.CreateUserId = user.Id;
			rebate.UpdateTime = null;
			rebate.UpdateUserId = null;
			rebate.ExistsOnDatabase = false;

			ResetRelatedData(rebate);

			// Create rebate note (remove existing notes as this is just being used as a structure for
			// a new rebate)
			rebate.RebateNotes = new ExtendedBindingList<RebateNote>();
			RebateNote rebateNote = new RebateNote();

			rebateNote.Note = "Copied from Rebate Number " + this.Number.ToString() + " Version " + this.Version.ToString();
			rebateNote.CreateTime = DateTime.Now;
			rebateNote.UserName = user.Name;
			rebateNote.UserId = user.Id;

			rebate.RebateNotes.Add(rebateNote);

			// Return copied rebate
			return rebate;
		}

		#endregion

		#region Year End Rollover

		public String PeriodRollOver(CalculationPeriod accrualPeriod, Int32 userId)
		{
			String error = "";

			if (DataAccessServer.PersistentConnection == null)
			{
				throw new InvalidOperationException("You cannot rollover periods outside of a Persistent Connection");
			}

			if (accrualPeriod.Period == 12)
			{
				// Check if we need to rollover this rebate
				DateTime nextYear = new DateTime(accrualPeriod.Year + 1, 1, 1);
				DateTime yearEnd = nextYear.AddDays(-1);
				Boolean performRollover = true;

				if (this.EndDate.HasValue)
				{
					performRollover = (this.EndDate > yearEnd);
				}

				// Perform rollovers if required
				if (performRollover)
				{
					// Ensure all related collections are populated before creating a clone
					this.ReadEntityCollection();

					// Create a new instance of this object
					Rebate rebate = (Rebate) this.Clone();

					// Now amend the new rebate to be for the next period
					rebate.Id = 0;
					rebate.StartDate = nextYear;
					rebate.EndDate = this.EndDate;
					rebate.Version = (Int16) nextYear.Year;
					rebate.CreateTime = DateTime.Now;
					rebate.CreateUserId = userId;
					rebate.UpdateTime = null;
					rebate.UpdateUserId = null;
					rebate.ExistsOnDatabase = false;

					Int32 result = rebate.AcceptChanges();

					// Set the new identity value (also updates all child entities RebateId)
					rebate.Id = result;

					// Create a note against the new rebate to indicate the rebate has rolled over
					RebateNote rebateNote = new RebateNote();

					rebateNote.RebateId = rebate.Id;
					rebateNote.Note = "End of Year Rollover";
					rebateNote.UserId = userId;
					rebateNote.CreateTime = DateTime.Now;

					rebate.RebateNotes.Add(rebateNote);

					// Now bulk save child details
					if (result >= 0)
					{
						//result = rebate.RebateDeliveryWindows.BulkInsert();
						result = rebate.RebateDeliveryWindows.SqlBulkCopy();
					}

					if (result >= 0)
					{
						//result = rebate.RebateDetails.BulkInsert();
						result = rebate.RebateDetails.SqlBulkCopy();
					}

					if (result >= 0)
					{
						//result = rebate.RebateExceptions.BulkInsert();
						rebate.RebateExceptions.SqlBulkCopy();
					}

					if (result >= 0)
					{
						//result = rebate.RebateLeadTimes.BulkInsert();
						result = rebate.RebateLeadTimes.SqlBulkCopy();
					}

					if (result >= 0)
					{
						//result = rebate.RebateNotes.BulkInsert();
						result = rebate.RebateNotes.SqlBulkCopy();
					}

					if (result >= 0)
					{
						//result = rebate.RebateRanges.BulkInsert();
						result = rebate.RebateRanges.SqlBulkCopy();
					}

					if (result >= 0)
					{
						//result = rebate.RebateVAPs.BulkInsert();
						result = rebate.RebateVAPs.SqlBulkCopy();
					}

					// Now close old rebate
					if (result >= 0)
					{
						this.EndDate = yearEnd;
						result = this.AcceptChanges();
					}
					else
					{
						error = "Failed to update rebate (Rebate Id: " + this.Id.ToString();
					}
				}
			}

			return error;
		}

		private String ResetRelatedData(Rebate rebate)
		{
			String error = "";

			if (error == "") error = ResetRelatedEntityData<RebateDetail>(rebate.Id, rebate.RebateDetails);
			if (error == "") error = ResetRelatedEntityData<RebateException>(rebate.Id, rebate.RebateExceptions);
			if (error == "") error = ResetRelatedEntityData<RebateNote>(rebate.Id, rebate.RebateNotes);
			if (error == "") error = ResetRelatedEntityData<RebateRange>(rebate.Id, rebate.RebateRanges);
			if (error == "") error = ResetRelatedEntityData<RebateVAP>(rebate.Id, rebate.RebateVAPs);
			if (error == "") error = ResetRelatedEntityData<RebateDeliveryWindow>(rebate.Id, rebate.RebateDeliveryWindows);
			if (error == "") error = ResetRelatedEntityData<RebateLeadTime>(rebate.Id, rebate.RebateLeadTimes);

			return error;
		}

		private String ResetRelatedEntityData<T>(Int32 rebateId, ExtendedBindingList<T> relatedData)
		{
			String error = "";

			if (relatedData != null)
			{
				Type itemType = typeof(T);

				// Ensure collection is a collection of valid type
				if (!(typeof(T).IsSubclassOf(typeof(Entity))))
				{
					throw new ArgumentException("Rebate.CopyRelatedEntityData: Member type must be derived from base type Evolve.Libraries.Data.Remoting.Entity");
				}

				PropertyInfo existsOnDatabaseProperty = itemType.GetProperty("ExistsOnDatabase");
				PropertyInfo idProperty = itemType.GetProperty(RebateDetailProperty.Id);

				if (idProperty == null)
				{
					throw new ArgumentException("Rebate.CopyRelatedEntityData: Items must have a property Id");
				}

				PropertyInfo rebateIdProperty = itemType.GetProperty(RebateDetailProperty.RebateId);

				if (rebateIdProperty == null)
				{
					throw new ArgumentException("Rebate.CopyRelatedEntityData: Items must be related to Rebate via RebateId");
				}

				// Ok now we can create the copy of the items in the database
				foreach (Object item in relatedData)
				{
					idProperty.SetValue(item, 0, null);
					existsOnDatabaseProperty.SetValue(item, false, null);
					rebateIdProperty.SetValue(item, rebateId, null);

					if (rebateId != 0)
					{
						Int32 result = ((Entity) item).AcceptChanges();

						if (result < 0)
						{
							error = itemType.Name + ": Failed to copy for rebate rollover";
							break;
						}
					}
				}
			}

			return error;
		}

		#endregion

		#endregion
	}

	#region Rebate Extension Methods

	public static class RebateExtensionMethods
	{
		public static void ReadEntityCollectionDetails(this IList<Rebate> rebates)
		{
			String inClause = "";
			Int32 index = 1;

			if (rebates != null && rebates.Count > 0)
			{
				do
				{
					Int32 itemIndex = index - 1;

					rebates[itemIndex].RebateDetails = new ExtendedBindingList<RebateDetail>();
					rebates[itemIndex].RebateDeliveryWindows = new ExtendedBindingList<RebateDeliveryWindow>();
					rebates[itemIndex].RebateLeadTimes = new ExtendedBindingList<RebateLeadTime>();
					rebates[itemIndex].RebateExceptions = new ExtendedBindingList<RebateException>();
					rebates[itemIndex].RebateRanges = new ExtendedBindingList<RebateRange>();

					inClause += rebates[itemIndex].Id.ToString() + ", ";

					// Oracle seems to limit in clauses to 1000 items
					if (index == rebates.Count || index % 1000 == 0)
					{
						if (inClause.Length > 0)
						{
							inClause = "( " + inClause.Substring(0, inClause.Length - 2) + " )";

							RebateDetail rebateDetail = new RebateDetail();
							rebateDetail.ReadFilter = RebateDetailColumn.RebateId + " in " + inClause + " and " + RebateDetailColumn.IsDeleted + " = 0";
							ExtendedBindingList<RebateDetail> rebateDetails = (ExtendedBindingList<RebateDetail>) rebateDetail.Read();

							RebateDeliveryWindow rebateDeliveryWindow = new RebateDeliveryWindow();
							rebateDeliveryWindow.ReadFilter = RebateDeliveryWindowColumn.RebateId + " in " + inClause;
							ExtendedBindingList<RebateDeliveryWindow> rebateDeliveryWindows = (ExtendedBindingList<RebateDeliveryWindow>) rebateDeliveryWindow.Read();

							RebateLeadTime rebateLeadTime = new RebateLeadTime();
							rebateLeadTime.ReadFilter = RebateLeadTimeColumn.RebateId + " in " + inClause;
							ExtendedBindingList<RebateLeadTime> rebateLeadTimes = (ExtendedBindingList<RebateLeadTime>) rebateLeadTime.Read();

							RebateException rebateException = new RebateException();
							rebateException.ReadFilter = RebateExceptionColumn.RebateId + " in " + inClause;
							ExtendedBindingList<RebateException> rebateExceptions = (ExtendedBindingList<RebateException>) rebateException.Read();

							RebateRange rebateRange = new RebateRange();
							rebateRange.ReadFilter = RebateRangeColumn.RebateId + " in " + inClause;
							ExtendedBindingList<RebateRange> rebateRanges = (ExtendedBindingList<RebateRange>) rebateRange.Read();

							inClause = "";

							Int32 startIndex = (index > 1000 ? index - 1000 : 0);

							for (Int32 populateIndex = startIndex; populateIndex < index; populateIndex++)
							{
								rebates[populateIndex].RebateDetails.AddMultiple(rebateDetails.Where(rd => rd.RebateId == rebates[populateIndex].Id));
								rebates[populateIndex].RebateDeliveryWindows.AddMultiple(rebateDeliveryWindows.Where(rdw => rdw.RebateId == rebates[populateIndex].Id));
								rebates[populateIndex].RebateLeadTimes.AddMultiple(rebateLeadTimes.Where(rlt => rlt.RebateId == rebates[populateIndex].Id));
								rebates[populateIndex].RebateExceptions.AddMultiple(rebateExceptions.Where(re => re.RebateId == rebates[populateIndex].Id));
								rebates[populateIndex].RebateRanges.AddMultiple(rebateRanges.Where(rr => rr.RebateId == rebates[populateIndex].Id));
							}
						}
					}

					index++;
				} while (index <= rebates.Count);
			}
		}
	}

	#endregion

	public class RebateColumn
	{
		public const string Id = "Id";
		public const string DivisionId = "DivisionId";
		public const string Number = "RebateNumber";
		public const string Version = "RebateVersion";
		public const string Description = "Description";
		public const string HierarchyId = "HierarchyId";
		public const string HierarchyEntityId = "HierarchyEntityId";
		public const string PricingSubGroupId = "PricingSubGroupId";
		public const string PayeeId = "PayeeId";
		public const string DeliveryTypeEnum = "DeliveryTypeEnum";
		public const string SiteId = "SiteId";
		public const string StartDate = "StartDate";
		public const string EndDate = "EndDate";
		public const string IsContract = "IsContract";
		public const string ContractText = "ContractText";
		public const string IsVAP = "IsVAP";
		public const string VAPActivationVolume = "VAPActivationVolume";
		public const string PaymentFrequencyId = "PaymentFrequencyId";
		public const string IsGlobalProduct = "IsGlobalProduct";
		public const string GlobalRate = "GlobalRate";
		public const string IsAdjustment = "IsAdjustment";
		public const string IsJif = "IsJif";
		public const string RebateTypeId = "RebateTypeId";
		public const string EstimatedVolume = "EstimatedVolume";
		public const string FixedRebateValue = "FixedRebateValue";
		public const string IsAccrued = "IsAccrued";
		public const string AccrualValue = "AccrualValue";
		public const string PaymentValue = "PaymentValue";
		public const string ClosedValue = "ClosedValue";
		public const string CreateUserId = "CreateUserId";
		public const string CreateTime = "CreateTime";
		public const string UpdateUserId = "UpdateUserId";
		public const string UpdateTime = "UpdateTime";
		public const string InterfaceTime = "InterfaceTime";
        public const string CustomerGroupID = "CUSTOMERGROUPID";
        public const string CustomerID = "CUSTOMERID";
    }

    public class RebateProperty
	{
		public const string IsSelected = "IsSelected";
		public const string Id = "Id";
		public const string DivisionId = "DivisionId";
		public const string Number = "Number";
		public const string Version = "Version";
		public const string Description = "Description";
		public const string HierarchyId = "HierarchyId";
		public const string HierarchyEntityId = "HierarchyEntityId";
		public const string PricingSubGroupId = "PricingSubGroupId";
		public const string PayeeId = "PayeeId";
		public const string PayeeJDECode = "PayeeJDECode";
		public const string PayeeName = "PayeeName";
		public const string DeliveryTypeEnum = "DeliveryTypeEnum";
		public const string SiteId = "SiteId";
		public const string StartDate = "StartDate";
		public const string EndDate = "EndDate";
		public const string IsContract = "IsContract";
		public const string IsContractText = "IsContractText";
		public const string ContractText = "ContractText";
		public const string IsVAP = "IsVAP";
		public const string IsVAPText = "IsVAPText";
		public const string VAPActivationVolume = "VAPActivationVolume";
		public const string IsForecast = "IsForecast";
		public const string PaymentFrequencyId = "PaymentFrequencyId";
		public const string PaymentFrequencyName = "PaymentFrequencyName";
		public const string IsGlobalProduct = "IsGlobalProduct";
		public const string GlobalRate = "GlobalRate";
		public const string IsAdjustment = "IsAdjustment";
		public const string IsJif = "IsJif";
		public const string RebateTypeId = "RebateTypeId";
		public const string RebateTypeName = "RebateTypeName";
		public const string EstimatedVolume = "EstimatedVolume";
		public const string EstimatedVolumeString = "EstimatedVolumeString";
		public const string FixedRebateValue = "FixedRebateValue";
		public const string FixedRebateValueString = "FixedRebateValueString";
		public const string IsAccrued = "IsAccrued";
		public const string IsAccruedText = "IsAccruedText";
		public const string AccrualValue = "AccrualValue";
		public const string PaymentValue = "PaymentValue";
		public const string ClosedValue = "ClosedValue";
		public const string CreateUserId = "CreateUserId";
		public const string CreateTime = "CreateTime";
		public const string UpdateUserId = "UpdateUserId";
		public const string UpdateTime = "UpdateTime";
		public const string InterfaceTime = "InterfaceTime";
		public const string HierarchyLevel = "HierarchyLevel";
		public const string HierarchyName = "HierarchyName";
		public const string RelatedEntityName = "RelatedEntityName";
		public const string InvoiceLinePropertyName = "InvoiceLinePropertyName";
		public const string Balance = "Balance";
		public const string CustomerGroupName = "CustomerGroupName";
		public const string CustomerName = "CustomerName";
		public const string HierarchyRelatedName = "HierarchyRelatedName";
		public const string CustomerGroupId = "CustomerGroupId";
		public const string ParentCustomerId = "ParentCustomerId";
		public const string ParentCustomerGroupId = "ParentCustomerGroupId";
		public const string SoldToGroupName = "SoldToGroupName";
		public const string ShipToGroupName = "ShipToGroupName";
        public const string CustomerGroupID = "CUSTOMERGROUPID";
        public const string CustomerID = "CUSTOMERID";
    }
}

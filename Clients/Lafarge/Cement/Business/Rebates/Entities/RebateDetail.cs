﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class RebateDetail : SqlServerEntity, ICloneable
	{
		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(RebateDetailColumn.Id, true, true)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;

				if (this.ExistsOnDatabase)
				{
					SetColumnDirty(RebateDetailProperty.Id);
				}
			}
		}
		#endregion

		#region PROPERTY: RebateId
		private Int32 _rebateId;
		[EntityColumn(RebateDetailColumn.RebateId), Browsable(false)]
		public Int32 RebateId
		{
			get { return _rebateId; }
			set
			{
				_rebateId = value;
				SetColumnDirty(RebateDetailProperty.RebateId);
			}
		}
		#endregion

		#region PROPERTY: AttributeId1
		private Int32? _attributeId1;
		[EntityColumn(RebateDetailColumn.AttributeId1), Browsable(false), EntityColumnConversion("DBNullToZero", "ZeroToDBNull", "AttributeIdConvertor", "Evolve.Clients.Lafarge.Cement.Business.Rebates", "Evolve.Clients.Lafarge.Cement.Business")]
		public Int32? AttributeId1
		{
			get { return _attributeId1; }
			set
			{
				_attributeId1 = value;
				SetColumnDirty(RebateDetailProperty.AttributeId1);
			}
		}
		#endregion

		#region PROPERTY: AttributeId2
		private Int32? _attributeId2;
		[EntityColumn(RebateDetailColumn.AttributeId2), Browsable(false), EntityColumnConversion("DBNullToZero", "ZeroToDBNull", "AttributeIdConvertor", "Evolve.Clients.Lafarge.Cement.Business.Rebates", "Evolve.Clients.Lafarge.Cement.Business")]
		public Int32? AttributeId2
		{
			get { return _attributeId2; }
			set
			{
				_attributeId2 = value;
				SetColumnDirty(RebateDetailProperty.AttributeId2);
			}
		}
		#endregion

		#region PROPERTY: AttributeId3
		private Int32? _attributeId3;
		[EntityColumn(RebateDetailColumn.AttributeId3), Browsable(false), EntityColumnConversion("DBNullToZero", "ZeroToDBNull", "AttributeIdConvertor", "Evolve.Clients.Lafarge.Cement.Business.Rebates", "Evolve.Clients.Lafarge.Cement.Business")]
		public Int32? AttributeId3
		{
			get { return _attributeId3; }
			set
			{
				_attributeId3 = value;
				SetColumnDirty(RebateDetailProperty.AttributeId3);
			}
		}
		#endregion

		#region PROPERTY: AttributeId4
		private Int32? _attributeId4;
		[EntityColumn(RebateDetailColumn.AttributeId4), Browsable(false), EntityColumnConversion("DBNullToZero", "ZeroToDBNull", "AttributeIdConvertor", "Evolve.Clients.Lafarge.Cement.Business.Rebates", "Evolve.Clients.Lafarge.Cement.Business")]
		public Int32? AttributeId4
		{
			get { return _attributeId4; }
			set
			{
				_attributeId4 = value;
				SetColumnDirty(RebateDetailProperty.AttributeId4);
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: AttributeName1
		private String _attributeName1;
		[EntityColumn(AttributeColumn.Name, "Attribute1", RebateDetailProperty.AttributeName1), DisplayName("Attribute 1")]
		public String AttributeName1
		{
			get { return _attributeName1; }
			set { _attributeName1 = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: AttributeName2
		private String _attributeName2;
		[EntityColumn(AttributeColumn.Name, "Attribute2", RebateDetailProperty.AttributeName2), DisplayName("Attribute 2")]
		public String AttributeName2
		{
			get { return _attributeName2; }
			set { _attributeName2 = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: AttributeName3
		private String _attributeName3;
		[EntityColumn(AttributeColumn.Name, "Attribute3", RebateDetailProperty.AttributeName3), DisplayName("Attribute 3")]
		public String AttributeName3
		{
			get { return _attributeName3; }
			set { _attributeName3 = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: AttributeName4
		private String _attributeName4;
		[EntityColumn(AttributeColumn.Name, "Attribute4", RebateDetailProperty.AttributeName4), DisplayName("Attribute 4")]
		public String AttributeName4
		{
			get { return _attributeName4; }
			set { _attributeName4 = value; }
		}
		#endregion

		#region PROPERTY: ProductId
		private Int32? _productId;
		[EntityColumn(RebateDetailColumn.ProductId), Browsable(false)]
		public Int32? ProductId
		{
			get { return _productId; }
			set
			{
				_productId = value;
				SetColumnDirty(RebateDetailProperty.ProductId);
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: ProductCode
		private String _productCode;
		[EntityColumn(ProductColumn.JDECode, DBRebates.Product, RebateDetailProperty.ProductCode), DisplayName("Product Code")]
		public String ProductCode
		{
			get { return _productCode; }
			set { _productCode = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: ProductName
		private String _productName;
		[EntityColumn(ProductColumn.Name, DBRebates.Product, RebateDetailProperty.ProductName), DisplayName("Product Name")]
		public String ProductName
		{
			get { return _productName; }
			set { _productName = value; }
		}
		#endregion

		#region PROPERTY: Rate
		private Decimal? _rate;
		[EntityColumn(RebateDetailColumn.Rate)]
		public Decimal? Rate
		{
			get { return _rate; }
			set
			{
				_rate = value;
				SetColumnDirty(RebateDetailProperty.Rate);
			}
		}
		#endregion

		#region PROPERTY: PriceEnquiryRate
		private Decimal? _priceEnquiryRate;
		[EntityColumn(RebateDetailColumn.PriceEnquiryRate)]
		[Browsable(false)]
		public Decimal? PriceEnquiryRate
		{
			get { return _priceEnquiryRate; }
			set
			{
				_priceEnquiryRate = value;
				SetColumnDirty(RebateDetailProperty.PriceEnquiryRate);
			}
		}
		#endregion

		#region PROPERTY: IsDeleted
		private Boolean _isDeleted;
		[EntityColumn(RebateDetailColumn.IsDeleted)]
		[Browsable(false)]
		public Boolean IsDeleted
		{
			get { return _isDeleted; }
			set
			{
				_isDeleted = value;
				SetColumnDirty(RebateDetailProperty.IsDeleted);
			}
		}
		#endregion

		#endregion

		#region Constructors

		public RebateDetail()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.RebateDetail);

			JoinTable(DBRebates.DatabaseName, DBRebates.Attribute, "Attribute1" + "." + AttributeColumn.Id + " = " + DBRebates.RebateDetail + "." + RebateDetailColumn.AttributeId1, "Attribute1", EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.Attribute, "Attribute2" + "." + AttributeColumn.Id + " = " + DBRebates.RebateDetail + "." + RebateDetailColumn.AttributeId2, "Attribute2", EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.Attribute, "Attribute3" + "." + AttributeColumn.Id + " = " + DBRebates.RebateDetail + "." + RebateDetailColumn.AttributeId3, "Attribute3", EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.Attribute, "Attribute4" + "." + AttributeColumn.Id + " = " + DBRebates.RebateDetail + "." + RebateDetailColumn.AttributeId4, "Attribute4", EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.Product, DBRebates.Product + "." + ProductColumn.Id + " = " + DBRebates.RebateDetail + "." + RebateDetailColumn.ProductId, EntityJoinType.LeftOuter);

			ReadFilter = "IsDeleted = 0";

			OrderBy =
				"{" + RebateDetailProperty.AttributeName1 + "}, " +
				"{" + RebateDetailProperty.AttributeName2 + "}, " +
				"{" + RebateDetailProperty.AttributeName3 + "}, " +
				"{" + RebateDetailProperty.AttributeName4 + "}, " +
				"{" + RebateDetailProperty.ProductCode + "}";
		}

		#endregion

		#region ICloneable

		public object Clone()
		{
			return this.MemberwiseClone();
		}

		#endregion

		#region Business Logic
		public Int32? AttributeId(Int32 index)
		{
			switch (index)
			{
				case 1:
					return _attributeId1;
				case 2:
					return _attributeId2;
				case 3:
					return _attributeId3;
				case 4:
					return _attributeId4;
			}
			throw new IndexOutOfRangeException();
		}

		#region Validation: Rate
		static public String ValidateRate(String rate, Boolean isPercentage)
		{
			return Rebate.ValidateRate(rate, isPercentage);
		}

		#endregion

		#endregion
	}

	public static class RebateDetailExtensionMethods
	{
		public static void CheckDeletions(this IEnumerable<RebateDetail> rebateDetails)
		{
			foreach (RebateDetail rebateDetail in rebateDetails.Where(rd => rd.Id != 0 && rd.IsMarkedForDeletion))
			{
				AccrualDetail accrualDetail = new AccrualDetail();
				accrualDetail.RebateDetailId = rebateDetail.Id;
				
				Int32 count = accrualDetail.ReadCount();

				if (count > 0)
				{
					// Have to mark rebate detail record as logically deleted as accrual records exist
					rebateDetail.IsDeleted = true;
					rebateDetail.IsMarkedForDeletion = false;
				}
			}
		}
	}

	public static class AttributeIdConvertor
	{
		public static Int32? DBNullToZero(Object fromValue)
		{
			Int32? toValue = 0;

			if (!object.ReferenceEquals(fromValue, System.DBNull.Value))
			{
				toValue = Int32.Parse(fromValue.ToString());
			}
			return toValue;
		}

		public static Object ZeroToDBNull(Int32? fromValue)
		{
			Object toValue = DBNull.Value;

			if (fromValue.HasValue && fromValue != 0)
			{
				toValue = fromValue;
			}
			return toValue;
		}
	}

	public class RebateDetailColumn
	{
		public const string Id = "Id";
		public const string RebateId = "RebateId";
		public const string AttributeId1 = "AttributeId1";
		public const string AttributeId2 = "AttributeId2";
		public const string AttributeId3 = "AttributeId3";
		public const string AttributeId4 = "AttributeId4";
		public const string ProductId = "ProductId";
		public const string Rate = "Rate";
		public const string PriceEnquiryRate = "PriceEnquiryRate";
		public const string IsDeleted = "IsDeleted";
	}

	public class RebateDetailProperty
	{
		public const string Id = "Id";
		public const string RebateId = "RebateId";
		public const string AttributeId1 = "AttributeId1";
		public const string AttributeId2 = "AttributeId2";
		public const string AttributeId3 = "AttributeId3";
		public const string AttributeId4 = "AttributeId4";
		public const string AttributeName1 = "AttributeName1";
		public const string AttributeName2 = "AttributeName2";
		public const string AttributeName3 = "AttributeName3";
		public const string AttributeName4 = "AttributeName4";
		public const string ProductId = "ProductId";
		public const string ProductCode = "ProductCode";
		public const string ProductName = "ProductName";
		public const string Rate = "Rate";
		public const string PriceEnquiryRate = "PriceEnquiryRate";
		public const string IsDeleted = "IsDeleted";
	}
}

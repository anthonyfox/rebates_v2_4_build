﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class AccrualDetailInvoice : SqlServerEntity
	{
		#region Properties

		// Accrual Detail Invoice

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(AccrualDetailInvoiceColumn.Id, true, true), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{ 
				_id = value;

				if (this.ExistsOnDatabase)
				{
					SetColumnDirty(AccrualDetailInvoiceProperty.Id);
				}
			}
		}
		#endregion

		#region PROPERTY: AccrualDetailId
		private Int32 _accrualDetailId;
		[EntityColumn(AccrualDetailInvoiceColumn.AccrualDetailId), Browsable(false)]
		public Int32 AccrualDetailId
		{
			get { return _accrualDetailId; }
			set
			{
				_accrualDetailId = value;

				if (this.ExistsOnDatabase)
				{
					SetColumnDirty(AccrualDetailInvoiceProperty.AccrualDetailId);
				}
			}
		}
		#endregion

		#region PROPERTY: InvoiceId
		private Int32 _invoiceId;
		[EntityColumn(AccrualDetailInvoiceColumn.InvoiceId, true), Browsable(false)]
		public Int32 InvoiceId
		{
			get { return _invoiceId; }
			set
			{
				_invoiceId = value;
				SetColumnDirty(AccrualDetailInvoiceProperty.InvoiceId);
			}
		}
		#endregion

		// Invoice
		
		#region RELATED ENTITY PROPERTY: Company
		private String _company;
		[EntityColumn(InvoiceLineColumn.Company, DBRebates.InvoiceLine)]
		public String Company
		{
			get { return _company; }
			set	{ _company = value;	}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: DocumentNumber
		private String _documentNumber;
		[EntityColumn(InvoiceLineColumn.DocumentNumber, DBRebates.InvoiceLine), DisplayName("Document Number")]
		public String DocumentNumber
		{
			get { return _documentNumber; }
			set	{ _documentNumber = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: DocumentType
		private String _documentType;
		[EntityColumn(InvoiceLineColumn.DocumentType, DBRebates.InvoiceLine), DisplayName("Document Type")]
		public String DocumentType
		{
			get { return _documentType; }
			set	{ _documentType = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: ItemNumber
		private String _itemNumber;
		[EntityColumn(InvoiceLineColumn.LineNumber, DBRebates.InvoiceLine), DisplayName("Item Number")]
		public String ItemNumber
		{
			get { return _itemNumber; }
			set	{ _itemNumber = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: InvoiceDate
		private DateTime _invoiceDate;
		[EntityColumn(InvoiceLineColumn.Date, DBRebates.InvoiceLine, AccrualDetailInvoiceProperty.InvoiceDate), DisplayName("Date")]
		public DateTime InvoiceDate
		{
			get { return _invoiceDate; }
			set { _invoiceDate = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: InvoiceQuantity
		private Decimal _invoiceQuantity;
		[EntityColumn(InvoiceLineColumn.Quantity, DBRebates.InvoiceLine, AccrualDetailInvoiceProperty.InvoiceQuantity), DisplayName("Quantity")]
		public Decimal InvoiceQuantity
		{
			get { return _invoiceQuantity; }
			set { _invoiceQuantity = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: InvoiceAmount
		private Decimal _invoiceAmount;
		[EntityColumn(InvoiceLineColumn.Amount, DBRebates.InvoiceLine, AccrualDetailInvoiceProperty.InvoiceAmount), DisplayName("Amount")]
		public Decimal InvoiceAmount
		{
			get { return _invoiceAmount; }
			set { _invoiceAmount = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: InvoiceDiscountAmount
		private Decimal _invoiceDiscountAmount;
		[EntityColumn(InvoiceLineColumn.DiscountAmount, DBRebates.InvoiceLine, AccrualDetailInvoiceProperty.InvoiceDiscountAmount), DisplayName("Discount Amount")]
		public Decimal InvoiceDiscountAmount
		{
			get { return (_invoiceDiscountAmount < 0 ? 0 : _invoiceDiscountAmount); }
			set { _invoiceDiscountAmount = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: ProductId
		private int _productId;
		[EntityColumn(InvoiceLineColumn.ProductId, DBRebates.InvoiceLine, AccrualDetailInvoiceProperty.ProductId), DisplayName("Product Id")]
		[Browsable(false)]
		public int ProductId
		{
			get { return _productId; }
			set { _productId = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: ProductCode
		private String _productCode;
		[EntityColumn(ProductColumn.JDECode, DBRebates.Product, AccrualDetailInvoiceProperty.ProductCode), DisplayName("Product Code")]
		public String ProductCode
		{
			get { return _productCode; }
			set	{ _productCode = value;	}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: ProductName
		private String _productName;
		[EntityColumn(ProductColumn.Name, DBRebates.Product, AccrualDetailInvoiceProperty.ProductName), DisplayName("Product Name")]
		public String ProductName
		{
			get { return _productName; }
			set	{ _productName = value;	}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: SiteId
		private int _siteId;
		[EntityColumn(InvoiceLineColumn.SiteId, DBRebates.InvoiceLine, AccrualDetailInvoiceProperty.SiteId), DisplayName("Site Id")]
		[Browsable(false)]
		public int SiteId
		{
			get { return _siteId; }
			set { _siteId = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: SiteCode
		private String _siteCode;
		[EntityColumn(SiteColumn.JDECode, DBRebates.Site, AccrualDetailInvoiceProperty.SiteCode), DisplayName("Sold From")]
		public String SiteCode
		{
			get { return _siteCode; }
			set { _siteCode = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: SiteName
		private String _siteName;
		[EntityColumn(SiteColumn.Name, DBRebates.Site, AccrualDetailInvoiceProperty.SiteName), DisplayName("Name")]
		public String SiteName
		{
			get { return _siteName; }
			set { _siteName = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: SoldToId
		private int _soldToId;
		[EntityColumn(InvoiceLineColumn.SoldToId, DBRebates.InvoiceLine, AccrualDetailInvoiceProperty.SoldToId), DisplayName("Sold To Id")]
		[Browsable(false)]
		public int SoldToId
		{
			get { return _soldToId; }
			set { _soldToId = value; }
		}
		#endregion
	
		#region RELATED ENTITY PROPERTY: SoldToCode
		private Int32 _soldToCode;
		[EntityColumn(CustomerColumn.JDECode, "SoldTo", AccrualDetailInvoiceProperty.SoldToCode), DisplayName("Sold To")]
		public Int32 SoldToCode
		{
			get { return _soldToCode; }
			set { _soldToCode = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: SoldToName
		private String _soldToName;
		[EntityColumn(CustomerColumn.Name, "SoldTo", AccrualDetailInvoiceProperty.SoldToName), DisplayName("Name")]
		public String SoldToName
		{
			get { return _soldToName; }
			set { _soldToName = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: ShipToCode
		private Int32 _shipToCode;
		[EntityColumn(CustomerColumn.JDECode, "ShipTo", AccrualDetailInvoiceProperty.ShipToCode), DisplayName("Ship To")]
		public Int32 ShipToCode
		{
			get { return _shipToCode; }
			set { _shipToCode = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: ShipToName
		private String _shipToName;
		[EntityColumn(CustomerColumn.Name, "ShipTo", AccrualDetailInvoiceProperty.ShipToName), DisplayName("Name")]
		public String ShipToName
		{
			get { return _shipToName; }
			set { _shipToName = value; }
		}
		#endregion

		// Calculation Properties

		#region CALCULATION PROPERTY: ShipToId

		[Browsable(false)]
		public int ShipToId { get; set; }

		#endregion

		#region CALCULATION PROPERTY: DeliveryTypeEnum

		[Browsable(false)]
		public DeliveryTypeEnum DeliveryTypeEnum { get; set; }

		#endregion

		#endregion

		#region Constructors

		public AccrualDetailInvoice()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.AccrualDetailInvoice);

			JoinTable(DBRebates.DatabaseName, DBRebates.InvoiceLine, DBRebates.InvoiceLine + "." + InvoiceLineColumn.Id + " = " + DBRebates.AccrualDetailInvoice + "." + AccrualDetailInvoiceColumn.InvoiceId);
			JoinTable(DBRebates.DatabaseName, DBRebates.Product, DBRebates.Product + "." + ProductColumn.Id + " = " + DBRebates.InvoiceLine + "." + InvoiceLineColumn.ProductId);
			JoinTable(DBRebates.DatabaseName, DBRebates.Site, DBRebates.Site + "." + SiteColumn.Id + " = " + DBRebates.InvoiceLine + "." + InvoiceLineColumn.SiteId);
			JoinTable(DBRebates.DatabaseName, DBRebates.Customer, "SoldTo" + "." + CustomerColumn.Id + " = " + DBRebates.InvoiceLine + "." + InvoiceLineColumn.SoldToId, "SoldTo");
			JoinTable(DBRebates.DatabaseName, DBRebates.Customer, "ShipTo" + "." + CustomerColumn.Id + " = " + DBRebates.InvoiceLine + "." + InvoiceLineColumn.ShipToId, "ShipTo"); 
		}

		#endregion

		#region Business Logic

		public static Int32 DeleteAccruals(Int32 divisionAccrualId)
		{
			AccrualDetailInvoice accrualDetailInvoice = new AccrualDetailInvoice();

			accrualDetailInvoice.ReadFilter =
				accrualDetailInvoice.DatabaseTable + "." + AccrualDetailInvoiceColumn.AccrualDetailId + " in ( " +
					"select " + AccrualDetailColumn.Id + " " +
					"from " + accrualDetailInvoice.DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.AccrualDetail) + " " +
					"where " + AccrualDetailColumn.AccrualId + " in ( " +
						"select " + AccrualColumn.Id + " " +
						"from " + accrualDetailInvoice.DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.Accrual) + " " + 
						"where " + AccrualColumn.DivisionAccrualId + " = " + divisionAccrualId.ToString() + " " +
						")" +
					")";

			Int32 result = accrualDetailInvoice.DeleteForReadFilter();
			return result;
		}

		#endregion
	}

	public class AccrualDetailInvoiceColumn
	{
		public const string Id = "Id";
		public const string AccrualDetailId = "AccrualDetailId";
		public const string InvoiceId = "InvoiceId";
	}

	public class AccrualDetailInvoiceProperty
	{
		public const string Id = "Id";
		public const string AccrualDetailId = "AccrualDetailId";
		public const string InvoiceId = "InvoiceId";
		public const string Company = "Company";
		public const string DocumentNumber = "DocumentNumber";
		public const string DocumentType = "DocumentType";
		public const string ItemNumber = "ItemNumber";
		public const string InvoiceDate = "InvoiceDate";
		public const string InvoiceQuantity = "InvoiceQuantity";
		public const string InvoiceAmount = "InvoiceAmount";
		public const string InvoiceDiscountAmount = "InvoiceDiscountAmount";
		public const string ProductId = "ProductId";
		public const string ProductCode = "ProductCode";
		public const string ProductName = "ProductName";
		public const string SiteId = "SiteId";
		public const string SiteCode = "SiteCode";
		public const string SiteName = "SiteName";
		public const string SoldToId = "SoldToId";
		public const string SoldToCode = "SoldToCode";
		public const string SoldToName = "SoldToName";
		public const string ShipToCode = "ShipToCode";
		public const string ShipToName = "ShipToName";
	}
}

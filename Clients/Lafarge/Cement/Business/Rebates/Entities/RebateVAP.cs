﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class RebateVAP : SqlServerEntity, ICloneable
	{
		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(RebateVAPColumn.Id, true, true), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;

				if (this.ExistsOnDatabase)
				{
					SetColumnDirty(RebateVAPProperty.Id);
				}
			}
		}
		#endregion

		#region PROPERTY: RebateId
		private Int32 _rebateId;
		[EntityColumn(RebateVAPColumn.RebateId), Browsable(false)]
		public Int32 RebateId
		{
			get { return _rebateId; }
			set
			{
				_rebateId = value;
				SetColumnDirty(RebateVAPProperty.RebateId);
			}
		}
		#endregion

		#region PROPERTY: ProductId
		private Int32 _productId;
		[EntityColumn(RebateVAPColumn.ProductId), Browsable(false)]
		public Int32 ProductId
		{
			get { return _productId; }
			set
			{
				_productId = value;
				SetColumnDirty(RebateVAPProperty.ProductId);
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: ProductCode
		private String _productCode;
		[EntityColumn(ProductColumn.JDECode, DBRebates.Product, RebateVAPProperty.ProductCode), DisplayName("Product Code")]
		public String ProductCode
		{
			get { return _productCode; }
			set
			{
				_productCode = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: ProductName
		private String _productName;
		[EntityColumn(ProductColumn.Name, DBRebates.Product, RebateVAPProperty.ProductName), DisplayName("Product Name")]
		public String ProductName
		{
			get { return _productName; }
			set
			{
				_productName = value;
			}
		}
		#endregion

		#region PROPERTY: ActivationVolume
		private Int32? _activationVolume;
		[EntityColumn(RebateVAPColumn.ActivationVolume), DisplayName("Activation Volume")]
		public Int32? ActivationVolume
		{
			get { return _activationVolume; }
			set
			{
				_activationVolume = value;
				SetColumnDirty(RebateVAPProperty.ActivationVolume);
			}
		}
		#endregion

		#endregion

		#region Constructors

		public RebateVAP()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.RebateVAP);

			JoinTable(DBRebates.DatabaseName, DBRebates.Product, DBRebates.Product + "." + ProductColumn.Id + " = " + DBRebates.RebateVAP + "." + RebateVAPColumn.ProductId, EntityJoinType.LeftOuter);

			OrderBy =
				"{" + RebateVAPProperty.ProductCode + "}";
		}

		#endregion

		#region ICloneable

		public object Clone()
		{
			return this.MemberwiseClone();
		}

		#endregion

		#region Business Logic
		public static Int32 DeleteRebateVAPs(Int32 rebateId)
		{
			RebateVAP rebateVAP = new RebateVAP();
			rebateVAP.RebateId = rebateId;

			rebateVAP.AddReadFilter(RebateVAPProperty.RebateId, "=");

			return rebateVAP.DeleteForReadFilter();
		}

		#region Validation

		#region Validate: ActivationVolume
		public static String ValidateActivationVolume(String activationVolume)
		{
			return ValidateActivationVolume(activationVolume, false);
		}
		public static String ValidateActivationVolume(String activationVolume, Boolean allowNull)
		{
			const Decimal MAX_QUANTITY = 10000000; // 10 Million
			const Decimal MIN_QUANTITY = 1;
			const string NAME = "Activation Volume";

			String errorText = "";
			Decimal value;

			if (activationVolume.Trim() == "")
			{
				if (!allowNull)
				{
					errorText = NAME + " must be entered";
				}
			}
			else
			{
				if (Decimal.TryParse(activationVolume, out value))
				{
					if (value < MIN_QUANTITY || value > MAX_QUANTITY)
					{
						errorText = NAME + " must be between " + MIN_QUANTITY.ToString() + " and " + MAX_QUANTITY.ToString();
					}
				}
				else
				{
					errorText = "A valid " + NAME + " must be entered";
				}
			}

			return errorText;
		}
		#endregion

		#endregion

		#endregion

	}

	public class RebateVAPColumn
	{
		public const string Id = "Id";
		public const string RebateId = "RebateId";
		public const string ProductId = "ProductId";
		public const string ActivationVolume = "ActivationVolume";
	}

	public class RebateVAPProperty
	{
		public const string Id = "Id";
		public const string RebateId = "RebateId";
		public const string ProductId = "ProductId";
		public const string ProductCode = "ProductCode";
		public const string ProductName = "ProductName";
		public const string ActivationVolume = "ActivationVolume";
	}

}

﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class PaymentFrequency : SqlServerEntity
	{
		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(PaymentFrequencyColumn.Id, true, true), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set 
			{
				_id = value;
				SetColumnDirty(PaymentFrequencyProperty.Id);
			}
		}
		#endregion

		#region PROPERTY: DivisionId
		private Int32 _divisionId;
		[EntityColumn(PaymentFrequencyColumn.DivisionId, true), Browsable(false)]
		public Int32 DivisionId
		{
			get { return _divisionId; }
			set
			{
				_divisionId = value;
				SetColumnDirty(PaymentFrequencyProperty.DivisionId);
			}
		}
		#endregion

		#region PROPERTY: Name
		private String _name;
		[EntityColumn(PaymentFrequencyColumn.Name)]
		public String Name
		{
			get { return _name; }
			set 
			{
				_name = value;
				SetColumnDirty(PaymentFrequencyProperty.Name);
			}
		}
		#endregion

		#endregion

		#region Constructors

		public PaymentFrequency()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.PaymentFrequency);
			OrderBy = "{" + PaymentFrequencyProperty.Id + "}";
		}

		#endregion
	}

	public class PaymentFrequencyColumn
	{
		public const string Id = "Id";
		public const string DivisionId = "DivisionId";
		public const string Name = "Name";
	}

	public class PaymentFrequencyProperty
	{
		public const string Id = "Id";
		public const string DivisionId = "DivisionId";
		public const string Name = "Name";
	}
}

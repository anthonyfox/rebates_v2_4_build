﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class ExceptionType : SqlServerEntity
	{
		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(ExceptionTypeColumn.Id, true, true)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;
				SetColumnDirty(ExceptionTypeProperty.Id);
			}
		}
		#endregion

		#region PROPERTY: Enum
		private ExceptionTypeEnum _enum;
		[EntityColumn(ExceptionTypeColumn.Enum), EntityColumnConversion("DBIntToExceptionTypeEnum", "ExceptionTypeEnumToDBInt", "ExceptionTypeEnumConvertor", "Evolve.Clients.Lafarge.Cement.Business.Rebates", "Evolve.Clients.Lafarge.Cement.Business")]
		public ExceptionTypeEnum Enum
		{
			get { return _enum; }
			set
			{
				_enum = value;
				SetColumnDirty(ExceptionTypeProperty.Enum);
			}
		}
		#endregion

		#region PROPERTY: Name
		private String _name;
		[EntityColumn(ExceptionTypeColumn.Name)]
		public String Name
		{
			get { return _name; }
			set
			{
				_name = value;
				SetColumnDirty(ExceptionTypeProperty.Name);
			}
		}
		#endregion

		#region PROPERTY: RelatedEntityName
		private String _relatedEntityName;
		[EntityColumn(ExceptionTypeColumn.RelatedEntityName)]
		public String RelatedEntityName
		{
			get { return _relatedEntityName; }
			set
			{
				_relatedEntityName = value;
				SetColumnDirty(ExceptionTypeProperty.RelatedEntityName);
			}
		}
		#endregion

		#region PROPERTY: InvoiceLinePropertyName
		private String _InvoiceLinePropertyName;
		[EntityColumn(ExceptionTypeColumn.InvoiceLinePropertyName)]
		public String InvoiceLinePropertyName
		{
			get { return _InvoiceLinePropertyName; }
			set
			{
				_InvoiceLinePropertyName = value;
				SetColumnDirty(ExceptionTypeProperty.InvoiceLinePropertyName);
			}
		}
		#endregion

		#endregion

		#region Constructors

		public ExceptionType()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.ExceptionType);
		}

		#endregion

		#region Business Logic

		public static ExceptionType Find(ExceptionTypeEnum exceptionTypeEnum, ExtendedBindingList<ExceptionType> exceptionTypes)
		{
			ExceptionType exceptionType = null;

			foreach (ExceptionType checkExceptionType in exceptionTypes)
			{
				if (exceptionTypeEnum == checkExceptionType.Enum)
				{
					exceptionType = checkExceptionType;
					break;
				}
			}

			return exceptionType;
		}

		#endregion
	}

	public class ExceptionTypeColumn
	{
		public const string Id = "Id";
		public const string Enum = "Enum";
		public const string Name = "Name";
		public const string RelatedEntityName = "RelatedEntityName";
		public const string InvoiceLinePropertyName = "InvoiceLinePropertyName";
	}

	public class ExceptionTypeProperty
	{
		public const string Id = "Id";
		public const string Enum = "Enum";
		public const string Name = "Name";
		public const string RelatedEntityName = "RelatedEntityName";
		public const string InvoiceLinePropertyName = "InvoiceLinePropertyName";
	}
}

﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class RebateRange : SqlServerEntity, ICloneable
	{
		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(RebateRangeColumn.Id, true, true), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set 
			{
				_id = value;

				if (this.ExistsOnDatabase)
				{
					SetColumnDirty(RebateRangeProperty.Id);
				}
			}
		}
		#endregion

		#region PROPERTY: RebateId
		private Int32 _rebateId;
		[EntityColumn(RebateRangeColumn.RebateId), Browsable(false)]
		public Int32 RebateId
		{
			get { return _rebateId; }
			set 
			{
				_rebateId = value;
				SetColumnDirty(RebateRangeProperty.RebateId);
			}
		}
		#endregion

		#region PROPERTY: AboveQuantity
		private Int32 _aboveQuantity;
		[EntityColumn(RebateRangeColumn.AboveQuantity), DisplayName("Above Quantity")]
		public Int32 AboveQuantity
		{
			get { return _aboveQuantity; }
			set 
			{
				_aboveQuantity = value;
				SetColumnDirty(RebateRangeProperty.AboveQuantity);
			}
		}
		#endregion

		#region PROPERTY: Rate
		private Decimal _rate;
		[EntityColumn(RebateRangeColumn.Rate)]
		public Decimal Rate
		{
			get { return _rate; }
			set 
			{
				_rate = value;
				SetColumnDirty(RebateRangeProperty.Rate);
			}
		}
		#endregion

		#endregion

		#region Constructors

		public RebateRange()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName,DBRebates.RebateRange);
			OrderBy = "{" + RebateRangeProperty.AboveQuantity + "}";
		}

		#endregion

		#region ICloneable

		public object Clone()
		{
			return this.MemberwiseClone();
		}

		#endregion

		#region Business Logic
		public static Int32 DeleteRebateRanges(Int32 rebateId)
		{
			RebateRange rebateRange = new RebateRange();
			rebateRange.RebateId = rebateId;

			rebateRange.AddReadFilter(RebateRangeProperty.RebateId, "=");

			return rebateRange.DeleteForReadFilter();
		}

		#region Validation

		#region Validate: AboveQty
		public static String ValidateAboveQty(String aboveQty, RebateRange rebateRange, ExtendedBindingList<RebateRange> rebateRanges)
		{
			const Decimal MAX_QUANTITY = 10000000; // 10 Million
			const Decimal MIN_QUANTITY = 0;
			const string NAME = "Quantity";

			String errorText = "";
			Decimal value;

			if (Decimal.TryParse(aboveQty, out value))
			{
				if (value < MIN_QUANTITY || value > MAX_QUANTITY)
				{
					errorText = NAME + " must be between " + MIN_QUANTITY.ToString() + " and " + MAX_QUANTITY.ToString();
				}
			}
			else
			{
				errorText = "A valid " + NAME + " must be entered";
			}

			if (String.IsNullOrEmpty(errorText))
			{
				foreach (RebateRange rr in rebateRanges)
				{
					if (rr.AboveQuantity == rebateRange.AboveQuantity && !rr.Equals(rebateRange))
					{
						errorText = "This above quantity " + rr.AboveQuantity.ToString() + " already exists";
					}
				}
			}

			return errorText;
		}

		#endregion

		#region Validate: Rate
		public static String ValidateRate(String rate)
		{
			return Rebate.ValidateRate(rate);
		}
		#endregion

		#endregion
		#endregion
	}

	public class RebateRangeColumn
	{
		public const string Id = "Id";
		public const string RebateId = "RebateId";
		public const string AboveQuantity = "AboveQuantity";
		public const string Rate = "Rate";
	}

	public class RebateRangeProperty
	{
		public const string Id = "Id";
		public const string RebateId = "RebateId";
		public const string AboveQuantity = "AboveQuantity";
		public const string Rate = "Rate";
	}
}

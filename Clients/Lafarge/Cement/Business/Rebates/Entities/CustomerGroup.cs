﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class CustomerGroup : SqlServerEntity
	{
		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(CustomerGroupColumn.Id, true, true), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;
				SetColumnDirty(CustomerGroupProperty.Id);
			}
		}
		#endregion

		#region PROPERTY: DivisionId
		private Int32 _divisionId;
		[EntityColumn(CustomerGroupColumn.DivisionId, true), Browsable(false)]
		public Int32 DivisionId
		{
			get { return _divisionId; }
			set
			{
				_divisionId = value;
				SetColumnDirty(CustomerGroupProperty.DivisionId);
			}
		}
		#endregion

		#region PROPERTY: JDECode
		private String _jdeCode;
		[EntityColumn(CustomerGroupColumn.JDECode), DisplayName("Group")]
		public String JDECode
		{
			get { return _jdeCode; }
			set
			{
				_jdeCode = value;
				SetColumnDirty(CustomerGroupProperty.JDECode);
			}
		}
		#endregion

		#region PROPERTY: Name
		private String _name;
		[EntityColumn(CustomerGroupColumn.Name)]
		public String Name
		{
			get { return _name; }
			set
			{
				_name = value;
				SetColumnDirty(CustomerGroupProperty.Name);
			}
		}
		#endregion

		#endregion

		#region Constructors

		public CustomerGroup()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.CustomerGroup);
			OrderBy = "{" + CustomerGroupProperty.Name + "}";
		}
		#endregion
	}

	public class CustomerGroupColumn
	{
		public const string Id = "Id";
		public const string DivisionId = "DivisionId";
		public const string JDECode = "JDECode";
		public const string Name = "Name";
	}

	public class CustomerGroupProperty
	{
		public const string Id = "Id";
		public const string DivisionId = "DivisionId";
		public const string JDECode = "JDECode";
		public const string Name = "Name";
	}
}

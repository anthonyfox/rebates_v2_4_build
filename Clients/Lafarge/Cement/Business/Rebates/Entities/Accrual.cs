﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class Accrual : SqlServerEntity
	{
		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(AccrualColumn.Id, true, true),  DisplayName("Id"), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;

				if (_accrualDetails != null)
				{
				    foreach (AccrualDetail accrualDetail in this.AccrualDetails.Where(ad => !ad.ExistsOnDatabase))
				    {
				        accrualDetail.AccrualId = _id;
				    }
				}

				if (_accrualInvoices != null)
				{
					foreach (AccrualInvoice accrualInvoice in this.AccrualInvoices.Where(ai => !ai.ExistsOnDatabase))
					{
						accrualInvoice.AccrualId = _id;
					}
				}

				if (this.ExistsOnDatabase)
				{
					SetColumnDirty(AccrualProperty.Id);
				}
			}
		}
		#endregion

		#region PROPERTY: RebateId
		private Int32? _rebateId;
		[EntityColumn(AccrualColumn.RebateId), Browsable(false)]
		public Int32? RebateId
		{
			get { return _rebateId; }
			set
			{ 
				_rebateId = value;
				SetColumnDirty(AccrualProperty.RebateId);
			}
		}
		#endregion

		#region PROPERTY: DivisionAccrualId
		private Int32 _divisionAccrualId;
		[EntityColumn(AccrualColumn.DivisionAccrualId), Browsable(false)]
		public Int32 DivisionAccrualId
		{
			get { return _divisionAccrualId; }
			set
			{
				_divisionAccrualId = value;

				if (this.ExistsOnDatabase)
				{
					SetColumnDirty(AccrualProperty.DivisionAccrualId);
				}
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: PayeeId
		private Int32? _payeeId;
		[EntityColumn(RebateColumn.PayeeId, DBRebates.Rebate, AccrualProperty.PayeeId), Browsable(false)]
		public Int32? PayeeId
		{
			get { return _payeeId; }
			set
			{
				_payeeId = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: RebateNumber
		private Int32 _rebateNumber;
		[EntityColumn(RebateColumn.Number, DBRebates.Rebate, AccrualProperty.RebateNumber), DisplayName("Rebate Number")]
		public Int32 RebateNumber
		{
			get { return _rebateNumber; }
			set
			{
				_rebateNumber = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: RebateVersion
		private Int16 _rebateVersion;
		[EntityColumn(RebateColumn.Version, DBRebates.Rebate, AccrualProperty.RebateVersion), DisplayName("Version")]
		public Int16 RebateVersion
		{
			get { return _rebateVersion; }
			set { _rebateVersion = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: RebateDescription
		private String _rebateDescription;
		[EntityColumn(RebateColumn.Description, DBRebates.Rebate, AccrualProperty.RebateDescription), DisplayName("Description")]
		public String RebateDescription
		{
			get { return _rebateDescription; }
			set { _rebateDescription = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: RebateTypeName
		private String _rebateTypeName;
		[EntityColumn(RebateTypeColumn.Name, DBRebates.RebateType, AccrualProperty.RebateTypeName)]
		[DisplayName("Rebate Type")]
		public String RebateTypeName
		{
			get { return _rebateTypeName; }
			set { _rebateTypeName = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: HierarchyName
		private String _hierarchyName;
		[EntityColumn(HierarchyColumn.Name, DBRebates.Hierarchy, AccrualProperty.HierarchyName)]
		[DisplayName("Hierarchy")]
		public String HierarchyName
		{
			get { return _hierarchyName; }
			set
			{
				_hierarchyName = value;
			}
		}
		#endregion

		#region DERIVED PROPETY: HierarchyRelatedName
		[DisplayName("Name")]
		public String HierarchyRelatedName
		{
			get
			{
				if (this.HierarchyLevel == 1)
				{
					return this.CustomerGroupName;
				}
				else
				{
					return this.CustomerName;
				}
			}
		}
		#endregion

		#region DERIVED PROPERTY: ParentGroup
		[DisplayName("Parent Group")]
		public String ParentGroup
		{
			get
			{
				String parentGroup = "";

				switch (this.HierarchyLevel)
				{
					case 1:
						// Rebate is setup at Major Group Level
						parentGroup = this.CustomerGroupName;
						break;

					case 2:
						// Rebate is setup at Sold To Level, return the Customer Group Id
						// from the sold to account
						parentGroup = this.SoldToGroupName;
						break;

					case 3:
						// Rebate is setup at Ship To Level, return the ship to accounts parent customer
						// customer group id
						parentGroup = this.ShipToGroupName;
						break;

					default:
						throw new ArgumentException("Unexpected Hierarchy Level (" + this.HierarchyLevel.ToString() + ") for Rebate Id " + this.Id.ToString());
				}

				return parentGroup;
			}
		}
		#endregion

		#region PROPERTY: CreateYear
		private Int16 _createYear;
		[EntityColumn(AccrualColumn.CreateYear), Browsable(false)]
		public Int16 CreateYear
		{
			get
			{ 
				return _createYear; 
			}
			set 
			{
				_createYear = value; 
				_creationPeriod = new CalculationPeriod(_createYear, _createPeriod);
				SetColumnDirty(AccrualProperty.CreateYear);
			}
		}
		#endregion

		#region PROPERTY: CreatePeriod
		private Int16 _createPeriod;
		[EntityColumn(AccrualColumn.CreatePeriod), Browsable(false)]
		public Int16 CreatePeriod
		{
			get 
			{ 
				return _createPeriod; 
			}
			set 
			{ 
				_createPeriod = value; 
				_creationPeriod = new CalculationPeriod(_createYear, _createPeriod);
				SetColumnDirty(AccrualProperty.CreatePeriod);
			}
		}
		#endregion

		#region DERIVED PROPERTY: CreationPeriod
		private CalculationPeriod _creationPeriod;
		[DisplayName("Creation Period")]
		public CalculationPeriod CreationPeriod
		{
			get { return _creationPeriod; }
		}
		#endregion

		#region PROPERTY: ActualYear
		private Int16 _actualYear;
		[EntityColumn(AccrualColumn.ActualYear), Browsable(false)]
		public Int16 ActualYear
		{
			get 
			{ 
				return _actualYear; 
			}
			set 
			{ 
				_actualYear = value; 
				_accrualPeriod = new CalculationPeriod(_actualYear, _actualPeriod);
				SetColumnDirty(AccrualProperty.ActualYear);
			}
		}
		#endregion

		#region PROPERTY: ActualPeriod
		private Int16 _actualPeriod;
		[EntityColumn(AccrualColumn.ActualPeriod), Browsable(false)]
		public Int16 ActualPeriod
		{
			get 
			{ 
				return _actualPeriod; 
			}
			set 
			{ 
				_actualPeriod = value; 
				_accrualPeriod = new CalculationPeriod(_actualYear, _actualPeriod);
				SetColumnDirty(AccrualProperty.ActualPeriod);
			}
		}
		#endregion

		#region DERIVED PROPERTY: AccrualPeriod
		private CalculationPeriod _accrualPeriod;
		[DisplayName("Accrual Period")]
		public CalculationPeriod AccrualPeriod
		{
			get { return _accrualPeriod; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: IsDraft
		private Boolean _isDraft;
		[EntityColumn(DivisionAccrualColumn.IsDraft, DBRebates.DivisionAccrual, AccrualProperty.IsDraft), Browsable(false)]
		public Boolean IsDraft
		{
			get { return _isDraft; }
			set { _isDraft = value; }
		}
		#endregion

		#region DERIVED PROPERTY: IsDraftText
		[DisplayName("Is Draft")]
		public String IsDraftText
		{
			get { return (_isDraft ? "Yes" : "No"); }
		}
		#endregion

		#region PROPERTY: Value
		private Decimal _value;
		[EntityColumn(AccrualColumn.Value), DisplayName("Accrual Value")]
		public Decimal Value
		{
			get { return _value; }
			set
			{
				_value = value;
				SetColumnDirty(AccrualProperty.Value);
			}
		}
		#endregion

		#region PROPERTY: Rate
		private Decimal? _rate;
		[EntityColumn(AccrualColumn.Rate), DisplayName("Accrual Rate")]
		public Decimal? Rate
		{
			get { return _rate; }
			set
			{
				_rate = value;
				SetColumnDirty(AccrualProperty.Rate);
			}
		}
		#endregion

		#region PROPERTY: SalesVolume
		private Decimal? _salesVolume;
		[EntityColumn(AccrualColumn.SalesVolume), DisplayName("Sales Volume")]
		public Decimal? SalesVolume
		{
			get { return _salesVolume; }
			set
			{
				_salesVolume = value;
				SetColumnDirty(AccrualProperty.SalesVolume);
			}
		}
		#endregion

		#region PROPERTY: SalesValue
		private Decimal? _salesValue;
		[EntityColumn(AccrualColumn.SalesValue), DisplayName("Sales Value")]
		public Decimal? SalesValue
		{
			get { return _salesValue; }
			set
			{
				_salesValue = value;
				SetColumnDirty(AccrualProperty.SalesValue);
			}
		}
		#endregion

		#region PROPERTY: InterfaceTime
		private DateTime? _interfaceTime;
		[EntityColumn(AccrualColumn.InterfaceTime), Browsable(false)]
		public DateTime? InterfaceTime
		{
			get { return _interfaceTime; }
			set
			{
				_interfaceTime = value;
				SetColumnDirty(AccrualProperty.InterfaceTime);
			}
		}
		#endregion

		#region DERIVED PROPERTY: IsInterfacedText
		[DisplayName("Is Interfaced")]
		public String IsInterfacedText
		{
			get { return (_interfaceTime.HasValue ? "Yes" : "No"); }
		}
		#endregion

		#region PROPERTY: CreateUserId
		private Int32 _createUserId;
		[EntityColumn(AccrualColumn.CreateUserId), Browsable(false)]
		public Int32 CreateUserId
		{
			get { return _createUserId; }
			set
			{
				_createUserId = value;
				SetColumnDirty(AccrualProperty.CreateUserId);
			}
		}
		#endregion

		#region PROPERTY: CreateTime
		private DateTime _createTime;
		[EntityColumn(AccrualColumn.CreateTime, DBDateTime.Year, DBDateTime.Second), Browsable(false)]
		public DateTime CreateTime
		{
			get { return _createTime; }
			set
			{
				_createTime = value;
				SetColumnDirty(AccrualProperty.CreateTime); 
			}
		}
		#endregion

		#region PROPERTY: InsertSequence
		private Int32 _insertSequence;
		[EntityColumn(AccrualColumn.InsertSequence)]
		[Browsable(false)]
		public Int32 InsertSequence
		{
			get { return _insertSequence; }
			set
			{
				_insertSequence = value;
				SetColumnDirty(AccrualProperty.InsertSequence);
			}
		}
		#endregion

		#region PROPERTY: AccrualTypeEnum
		private AccrualTypeEnum _accrualTypeEnum;
		[EntityColumn(DivisionAccrualColumn.AccrualTypeEnum, DBRebates.DivisionAccrual, AccrualProperty.AccrualTypeEnum)]
		[EntityColumnConversion("DBInt16ToAccrualTypeEnum", "AccrualTypeEnumToDBInt16", "AccrualTypeEnumConvertor", "Evolve.Clients.Lafarge.Cement.Business.Rebates", "Evolve.Clients.Lafarge.Cement.Business")]
		[Browsable(false)]
		public AccrualTypeEnum AccrualTypeEnum
		{
			get { return _accrualTypeEnum; }
			set
			{
				_accrualTypeEnum = value;
				SetColumnDirty(AccrualProperty.AccrualTypeEnum);
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: DivisionId
		private Int32 _divisionId;
		[EntityColumn(DivisionAccrualColumn.DivisionId, DBRebates.DivisionAccrual, AccrualProperty.DivisionId), Browsable(false)]
		public Int32 DivisionId
		{
			get { return _divisionId; }
			set { _divisionId = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: HierarchyId
		private Int32 _hierarchyId;
		[EntityColumn(RebateColumn.HierarchyId, DBRebates.Rebate, AccrualProperty.HierarchyId), Browsable(false)]
		public Int32 HierarchyId
		{
			get { return _hierarchyId; }
			set { _hierarchyId = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: HierarchyEntityId
		private Int32 _hierarchyEntityId;
		[EntityColumn(RebateColumn.HierarchyEntityId, DBRebates.Rebate, AccrualProperty.HierarchyEntityId)]
		[Browsable(false)]
		public Int32 HierarchyEntityId
		{
			get { return _hierarchyEntityId; }
			set
			{
				_hierarchyEntityId = value;
				SetColumnDirty(AccrualProperty.HierarchyEntityId);
			}
		}
		#endregion

		#region RELATED ENTITY: AccrualInvoice
		private ExtendedBindingList<AccrualInvoice> _accrualInvoices;
		public ExtendedBindingList<AccrualInvoice> AccrualInvoices
		{
			get { return _accrualInvoices; }
			set { _accrualInvoices = value; }
		}
		#endregion

		#region RELATED ENTITY: AccrualDetails
		private ExtendedBindingList<AccrualDetail> _accrualDetails;
		public ExtendedBindingList<AccrualDetail> AccrualDetails
		{
			get { return _accrualDetails; }
			set { _accrualDetails = value; }
		}
		#endregion

		#region DERIVED PROPERTY: AccrualDetailInvoiceCount
		[Browsable(false)]
		public Int32 AccrualDetailInvoiceCount
		{
			get 
			{
				return _accrualDetails.Sum(ad => ad.AccrualDetailInvoices.Count);
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: HierarchyLevel
		private Int16 _hierarchyLevel;
		[EntityColumn(HierarchyColumn.Level, DBRebates.Hierarchy, AccrualProperty.HierarchyLevel)]
		[Browsable(false)]
		public Int16 HierarchyLevel
		{
			get { return _hierarchyLevel; }
			set
			{
				_hierarchyLevel = value;
			}
		}
		#endregion


		#region RELATED ENTITY PROPERTY: CustomerGroupId
		private Int32 _customerGroupId;
		[EntityColumn(CustomerGroupColumn.Id, DBRebates.CustomerGroup, AccrualProperty.CustomerGroupId)]
		[Browsable(false)]
		public Int32 CustomerGroupId
		{
			get { return _customerGroupId; }
			set
			{
				_customerGroupId = value;
				SetColumnDirty(AccrualProperty.CustomerGroupId);
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: CustomerGroupCode
		private String _customerGroupCode;
		[EntityColumn(CustomerGroupColumn.JDECode, DBRebates.CustomerGroup, AccrualProperty.CustomerGroupCode)]
		[Browsable(false)]
		public String CustomerGroupCode
		{
			get { return _customerGroupCode; }
			set { _customerGroupCode = value; }
		}
		#endregion
	
		#region RELATED ENTITY PROPERTY: CustomerGroupName
		private String _customerGroupName;
		[EntityColumn(CustomerGroupColumn.Name, DBRebates.CustomerGroup, AccrualProperty.CustomerGroupName)]
		[Browsable(false)]
		public String CustomerGroupName
		{
			get { return _customerGroupName; }
			set { _customerGroupName = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: CustomerCode
		private String _customerCode;
		[EntityColumn(CustomerColumn.JDECode, DBRebates.Customer, AccrualProperty.CustomerCode)]
		[Browsable(false)]
		public String CustomerCode
		{
			get { return _customerCode; }
			set { _customerCode = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: CustomerName
		private String _customerName;
		[EntityColumn(CustomerColumn.Name, DBRebates.Customer, AccrualProperty.CustomerName)]
		[Browsable(false)]
		public String CustomerName
		{
			get { return _customerName; }
			set { _customerName = value; }
		}
		#endregion


		#region RELATED ENTITY PROPERTY: SoldToGroupName
		private String _soldToGroupName;
		[EntityColumn(CustomerGroupColumn.Name, DBRebates.SoldToGroup, AccrualProperty.SoldToGroupName)]
		[Browsable(false)]
		public String SoldToGroupName
		{
			get { return _soldToGroupName; }
			set { _soldToGroupName = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: ShipToGroupName
		private String _shipToGroupName;
		[EntityColumn(CustomerGroupColumn.Name, DBRebates.ShipToGroup, AccrualProperty.ShipToGroupName)]
		[Browsable(false)]
		public String ShipToGroupName
		{
			get { return _shipToGroupName; }
			set { _shipToGroupName = value; }
		}
		#endregion

		//#region CALCULATION PROPERTY: AccrualJDEs

		//private List<AccrualJDE> _accrualJDEs = new List<AccrualJDE>();
		//public List<AccrualJDE> AccrualJDEs
		//{
		//	get { return _accrualJDEs; }
		//}

		//#endregion

		#region CALCULATION PROPERTY: RebateTypeId

		[Browsable(false)]
		public Int32 RebateTypeId { get; set; }

		#endregion

		#region CALCULATION PROPERTY: RebateStartDate

		[Browsable(false)]
		public DateTime RebateStartDate { get; set; }

		#endregion

		#region CALCULATION PROPERTY: RebateEndDate

		[Browsable(false)]
		public DateTime RebateEndDate { get; set; }

		#endregion

		#endregion

		#region Constructors

		public Accrual()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.Accrual);

			JoinTable(DBRebates.DatabaseName, DBRebates.Rebate, DBRebates.Rebate + "." + RebateColumn.Id + " = " + DBRebates.Accrual + "." + AccrualColumn.RebateId, EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.RebateType, DBRebates.RebateType + "." + RebateColumn.Id + " = " + DBRebates.Rebate + "." + RebateColumn.RebateTypeId, EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.DivisionAccrual, DBRebates.DivisionAccrual + "." + DivisionAccrualColumn.Id + " = " + DBRebates.Accrual + "." + AccrualColumn.DivisionAccrualId);
			JoinTable(DBRebates.DatabaseName, DBRebates.Hierarchy, DBRebates.Hierarchy + "." + HierarchyColumn.Id + " = " + DBRebates.Rebate + "." + RebateColumn.HierarchyId, EntityJoinType.LeftOuter);

			// Join related hierarchy entities (use outer join as we are not sure which one it will be)
			JoinTable(DBRebates.DatabaseName, DBRebates.CustomerGroup, DBRebates.CustomerGroup + "." + CustomerGroupColumn.Id + " = " + DBRebates.Rebate + "." + RebateColumn.HierarchyEntityId, EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.Customer, DBRebates.Customer + "." + CustomerColumn.Id + " = " + DBRebates.Rebate + "." + RebateColumn.HierarchyEntityId, EntityJoinType.LeftOuter);

			// Outer join customer back to customer to read parent customer (Sold To) details for Rebates setup
			// at Ship To level
			JoinTable(DBRebates.DatabaseName, DBRebates.Customer, DBRebates.ParentCustomer + "." + CustomerColumn.Id + " = " + DBRebates.Customer + "." + CustomerColumn.SoldToId, DBRebates.ParentCustomer, EntityJoinType.LeftOuter);

			// Joins to get parent group details
			JoinTable(DBRebates.DatabaseName, DBRebates.CustomerGroup, DBRebates.SoldToGroup + "." + CustomerGroupColumn.Id + " = " + DBRebates.Customer + "." + CustomerColumn.CustomerGroupId, DBRebates.SoldToGroup, EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.CustomerGroup, DBRebates.ShipToGroup + "." + CustomerGroupColumn.Id + " = " + DBRebates.ParentCustomer + "." + CustomerColumn.CustomerGroupId, DBRebates.ShipToGroup, EntityJoinType.LeftOuter);
			
			_accrualInvoices = new ExtendedBindingList<AccrualInvoice>();
			_accrualDetails = new ExtendedBindingList<AccrualDetail>();
		}

		#endregion

		#region Business Logic

		private void ReadAccrualDetails()
		{
			AccrualDetail accrualDetail = new AccrualDetail();

			accrualDetail.AccrualId = this.Id;
			accrualDetail.AddReadFilter(AccrualDetailProperty.AccrualId, "=");

			ExtendedBindingList<AccrualDetail> accrualDetails = (ExtendedBindingList<AccrualDetail>) accrualDetail.Read();

			if (accrualDetails != null)
			{
				this.AccrualDetails = accrualDetails;
			}
		}

		public void ReverseAccrualAndReset(CalculationPeriod accrualPeriod, ref Int32 insertSequence)
		{
			// Read all details
			ReadAccrualDetails();

			this.CreateYear = accrualPeriod.Year;
			this.CreatePeriod = accrualPeriod.Period;
			this.InsertSequence = ++insertSequence;

			this.AccrualDetails.ReadAccrualDetailInvoice();

			// Reverse
			this.Value = this.Value * -1;

			if (this.Rate.HasValue)
			{
				this.Rate = this.Rate.Value * -1;
			}

			// Reset accrual flags and id's to mark as being new
			this.Id = 0;
			this.ExistsOnDatabase = false;

			foreach (AccrualDetail accrualDetail in this.AccrualDetails)
			{
				accrualDetail.Id = 0;
				accrualDetail.AccrualId = 0;
				accrualDetail.Value = accrualDetail.Value * -1;

				if (accrualDetail.Rate.HasValue)
				{
					accrualDetail.Rate = accrualDetail.Rate.Value * -1;
				}

				accrualDetail.InsertSequence = ++insertSequence;
				accrualDetail.ExistsOnDatabase = false;

				foreach (AccrualDetailInvoice accrualDetailInvoice in accrualDetail.AccrualDetailInvoices)
				{
					accrualDetailInvoice.Id = 0;
					accrualDetailInvoice.AccrualDetailId = 0;
					accrualDetailInvoice.ExistsOnDatabase = false;
				}
			}
		}

		public static Int32 DeleteAccruals(Int32 divisionAccrualId)
		{
			Accrual accrual = new Accrual();

			accrual.ReadFilter = AccrualColumn.DivisionAccrualId + " = " + divisionAccrualId.ToString();

			Int32 result = accrual.DeleteForReadFilter();
			return result;
		}

		#endregion
	}

	public static class AccrualExtensionMethods
	{
		public static void MatchInsertSequences(this IEnumerable<Accrual> accruals, IEnumerable<AccrualInsertSequence> accrualInsertSequences)
		{
			Int32 index = 0;

			foreach (Accrual accrual in accruals.OrderBy(a => a.InsertSequence))
			{
				if (accrualInsertSequences.ElementAt(index).Id == 0)
				{
					throw new ArgumentOutOfRangeException("AccrualInsertSequence.Id Equals Zero");
				}

				if (accrual.InsertSequence != accrualInsertSequences.ElementAt(index).InsertSequence)
				{
					throw new ArgumentOutOfRangeException("AccrualInsertSequence.InsertSequence Mismatch");
				}

				accrual.Id = accrualInsertSequences.ElementAt(index).Id;
				index++;
			}
		}
	}

	public class AccrualColumn
	{
		public const string Id = "Id";
		public const string RebateId = "RebateId";
		public const string DivisionAccrualId = "DivisionAccrualId";
		public const string CreateYear = "CreateYear";
		public const string CreatePeriod = "CreatePeriod";
		public const string ActualYear = "ActualYear";
		public const string ActualPeriod = "ActualPeriod";
		public const string Value = "Value";
		public const string Rate = "Rate";
		public const string SalesVolume = "SalesVolume";
		public const string SalesValue = "SalesValue";
		public const string InterfaceTime = "InterfaceTime";
		public const string CreateUserId = "CreateUserId";
		public const string CreateTime = "CreateTime";
		public const string InsertSequence = "InsertSequence";
	}

	public class AccrualProperty
	{
		public const string Id = "Id";
		public const string RebateId = "RebateId";
		public const string DivisionAccrualId = "DivisionAccrualId";
		public const string PayeeId = "PayeeId";
		public const string RebateNumber = "RebateNumber";
		public const string RebateDescription = "RebateDescription";
		public const string RebateVersion = "RebateVersion";
		public const string RebateTypeId = "RebateTypeId";
		public const string RebateTypeName = "RebateTypeName";
		public const string CreateYear = "CreateYear";
		public const string CreatePeriod = "CreatePeriod";
		public const string ActualYear = "ActualYear";
		public const string ActualPeriod = "ActualPeriod";
		public const string Value = "Value";
		public const string Rate = "Rate";
		public const string SalesVolume = "SalesVolume";
		public const string SalesValue = "SalesValue";
		public const string InterfaceTime = "InterfaceTime";
		public const string IsInterfacedText = "IsInterfacedText";
		public const string CreateUserId = "CreateUserId";
		public const string CreateTime = "CreateTime";
		public const string InsertSequence = "InsertSequence";
		public const string AccrualTypeEnum = "AccrualTypeEnum";
		public const string DivisionId = "DivisionId";
		public const string IsDraft = "IsDraft";
		public const string HierarchyId = "HierarchyId";
		public const string HierarchyEntityId = "HierarchyEntityId";
		public const string AccrualPeriod = "AccrualPeriod";
		public const string CreationPeriod = "CreationPeriod";
		public const string AccrualDetails = "AccrualDetails";
		public const string HierarchyLevel = "HierarchyLevel";
		public const string HierarchyName = "HierarchyName";
		public const string CustomerGroupId = "CustomerGroupId";
		public const string CustomerGroupCode = "CustomerGroupCode";
		public const string CustomerGroupName = "CustomerGroupName";
		public const string CustomerCode = "CustomerCode";
		public const string CustomerName = "CustomerName";
		public const string SoldToGroupName = "SoldToGroupName";
		public const string ShipToGroupName = "ShipToGroupName";
	}
}

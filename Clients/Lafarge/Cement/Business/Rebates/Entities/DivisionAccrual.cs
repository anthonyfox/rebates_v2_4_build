﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class DivisionAccrual : SqlServerEntity
	{
		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(DivisionAccrualColumn.Id, true, true), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;
				SetColumnDirty(DivisionAccrualProperty.Id);
			}
		}
		#endregion

		#region PROPERTY: DivisionId
		private Int32 _divisionId;
		[EntityColumn(DivisionAccrualColumn.DivisionId), Browsable(false)]
		public Int32 DivisionId
		{
			get { return _divisionId; }
			set 
			{
				_divisionId = value;
				SetColumnDirty(DivisionAccrualProperty.DivisionId);
			}
		}
		#endregion

		#region PROPERTY: Year
		private Int16 _year;
		[EntityColumn(DivisionAccrualColumn.Year)]
		public Int16 Year
		{
			get { return _year; }
			set
			{
				_year = value;
				_calculationPeriod = new CalculationPeriod(_year, _period);
				SetColumnDirty(DivisionAccrualProperty.Year);
			}
		}
		#endregion

		#region PROPERTY: Period
		private Int16 _period;
		[EntityColumn(DivisionAccrualColumn.Period)]
		public Int16 Period
		{
			get { return _period; }
			set 
			{
				_period = value;
				_calculationPeriod = new CalculationPeriod(_year, _period);
				SetColumnDirty(DivisionAccrualProperty.Period);
			}
		}
		#endregion

		#region PROPERTY: IsDraft
		private Boolean _isDraft;
		[EntityColumn(DivisionAccrualColumn.IsDraft)]
		public Boolean IsDraft
		{
			get { return _isDraft; }
			set 
			{
				_isDraft = value;
				SetColumnDirty(DivisionAccrualProperty.IsDraft);
			}
		}
		#endregion

		#region PROPERTY: UserId
		private Int32 _userId;
		[EntityColumn(DivisionAccrualColumn.UserId), Browsable(false)]
		public Int32 UserId
		{
			get { return _userId; }
			set 
			{
				_userId = value;
				SetColumnDirty(DivisionAccrualProperty.UserId);
			}
		}
		#endregion

		#region PROPERTY: RunTime
		private DateTime _runTime;
		[EntityColumn(DivisionAccrualColumn.RunTime, DBDateTime.Year, DBDateTime.Second), DisplayName("Run Date")]
		public DateTime RunTime
		{
			get { return _runTime; }
			set 
			{
				_runTime = value;
				SetColumnDirty(DivisionAccrualProperty.RunTime);
			}
		}
		#endregion

		#region PROPERTY: LastRebateAmendTime
		private DateTime? _lastRebateAmendTime;
		[EntityColumn(DivisionAccrualColumn.LastRebateAmendTime, DBDateTime.Year, DBDateTime.Second), DisplayName("Last Rebate Amended")]
		public DateTime? LastRebateAmendTime
		{
			get { return _lastRebateAmendTime; }
			set
			{
				_lastRebateAmendTime = value;
				SetColumnDirty(DivisionAccrualProperty.LastRebateAmendTime);
			}
		}
		#endregion

		#region PROPERTY: AccrualTypeEnum
		private AccrualTypeEnum _accrualTypeEnum;
		[EntityColumn(DivisionAccrualColumn.AccrualTypeEnum)]
		[EntityColumnConversion("DBInt16ToAccrualTypeEnum", "AccrualTypeEnumToDBInt16", "AccrualTypeEnumConvertor", "Evolve.Clients.Lafarge.Cement.Business.Rebates", "Evolve.Clients.Lafarge.Cement.Business")]
		[Browsable(false)]
		public AccrualTypeEnum AccrualTypeEnum
		{
			get { return _accrualTypeEnum; }
			set
			{
				_accrualTypeEnum = value;
				SetColumnDirty(DivisionAccrualProperty.AccrualTypeEnum);
			}
		}
		#endregion

		#region DERIVED PROPERTY: CalculationPeriod

		private CalculationPeriod _calculationPeriod;

		public CalculationPeriod CalculationPeriod
		{
			get { return _calculationPeriod; }
		}

		#endregion

		#endregion

		#region Constructors

		public DivisionAccrual()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.DivisionAccrual);
			OrderBy =
				DivisionAccrualColumn.Year + " desc, " +
				DivisionAccrualColumn.Period + " desc, " +
				DivisionAccrualColumn.RunTime + " desc";
		}

		#endregion

		#region Business Logic

		/// <summary>
		/// Removes existing draft Accruals (Note: This method does not open a persistent
		/// connection, this must be done outside of the method call)
		/// </summary>
		public Int32 DeleteAccruals()
		{
			Int32 result = 0;

			//result = AccrualJDE.DeleteAccruals(this.Id);

			if (result >= 0)
			{
				result = AccrualBI.DeleteAccrualBIs(this.Id);
			}

			if (result >= 0)
			{
				result = AccrualDetailInvoice.DeleteAccruals(this.Id);
			}

			if (result >= 0)
			{
				result = AccrualDetail.DeleteAccruals(this.Id);
			}

			if (result >= 0)
			{
				result = AccrualInvoice.DeleteAccruals(this.Id);
			}

			if (result >= 0)
			{
				result = Accrual.DeleteAccruals(this.Id);
			}

			return result;
		}

		#endregion
	}

	public class DivisionAccrualColumn
	{
		public const string Id = "Id";
		public const string DivisionId = "DivisionId";
		public const string Year = "Year";
		public const string Period = "Period";
		public const string IsDraft = "IsDraft";
		public const string UserId = "UserId";
		public const string RunTime = "RunTime";
		public const string LastRebateAmendTime = "LastRebateAmendTime";
		public const string AccrualTypeEnum = "AccrualTypeEnum";
	}

	public class DivisionAccrualProperty
	{
		public const string Id = "Id";
		public const string DivisionId = "DivisionId";
		public const string Year = "Year";
		public const string Period = "Period";
		public const string IsDraft = "IsDraft";
		public const string UserId = "UserId";
		public const string RunTime = "RunTime";
		public const string LastRebateAmendTime = "LastRebateAmendTime";
		public const string AccrualTypeEnum = "AccrualTypeEnum";
	}
}

﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class Site : SqlServerEntity
	{
		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(SiteColumn.Id, true, true), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;
				SetColumnDirty(SiteProperty.Id);
			}
		}
		#endregion

		#region PROPERTY: DivisionId
		private Int32 _divisionId;
		[EntityColumn(SiteColumn.DivisionId, true), Browsable(false)]
		public Int32 DivisionId
		{
			get { return _divisionId; }
			set
			{
				_divisionId = value;
				SetColumnDirty(SiteProperty.DivisionId);
			}
		}
		#endregion

		#region PROPERTY: JDECode
		private String _jdeCode;
		[EntityColumn(SiteColumn.JDECode)]
		public String JDECode
		{
			get { return _jdeCode; }
			set 
			{
				_jdeCode = value;
				SetColumnDirty(SiteProperty.JDECode);
			}
		}
		#endregion

		#region PROPERTY: Name
		private String _name;
		[EntityColumn(SiteColumn.Name)]
		public String Name
		{
			get { return _name; }
			set 
			{
				_name = value;
				SetColumnDirty(SiteProperty.Name);
			}
		}
		#endregion

		#region PROPERTY: AddressId
		private Int32 _addressId;
		[EntityColumn(SiteColumn.AddressId)]
		public Int32 AddressId
		{
			get { return _addressId; }
			set
			{
				_addressId = value;
				SetColumnDirty(SiteProperty.AddressId);
			}
		}
		#endregion

		#region DERIVED PROPERTY: NameCode
		public String NameCode
		{
			get 
			{ 
				return _name.TrimEnd() + " (" + _jdeCode.TrimEnd() + ")"; 
			}
		}
        #endregion

        #region PROPERTY: JDECodeRebate
        private string _jdeCodeRebate;
        [EntityColumn(SiteColumn.JDECodeRebate), Browsable(false)]
        public string JDECodeRebate
        {
            get { return _jdeCodeRebate; }
            set
            {
                _jdeCodeRebate = value;
                SetColumnDirty(SiteProperty.JDECodeRebate);
            }
        }
        #endregion

        #region PROPERTY: IsActive
        private bool _isActive;
        [EntityColumn(SiteColumn.IsActive), Browsable(false)]
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                SetColumnDirty(SiteProperty.IsActive);
            }
        }
        #endregion

        #endregion

        #region Constructors

        public Site()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.Site);
			OrderBy = "{" + SiteProperty.Name + "}, {" + SiteProperty.JDECode + "}";
		}

		#endregion
	}

	public class SiteColumn
	{
		public const string Id = "Id";
		public const string DivisionId = "DivisionId";
		public const string JDECode = "JDECode";
		public const string Name = "Name";
		public const string AddressId = "AddressId";
		public const string JDECodeRebate = "JDECodeRebate";
		public const string IsActive = "Active";
	}

	public class SiteProperty
	{
		public const string Id = "Id";
		public const string DivisionId = "DivisionId";
		public const string JDECode = "JDECode";
		public const string Name = "Name";
		public const string AddressId = "AddressId";
		public const string JDECodeRebate = "JDECodeRebate";
		public const string IsActive = "IsActive";
	}

}

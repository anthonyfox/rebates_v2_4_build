﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
    public class CustomerGroupNameView : SqlServerEntity
    {
        #region Properties

        #region PROPERTY: Id
        private Int32 _id;
        [EntityColumn(CustomerGroupNameViewColumn.Id, true, true), Browsable(false)]
        public Int32 Id
        {
            get { return _id; }
            set
            {
                _id = value;
                SetColumnDirty(CustomerGroupNameViewProperty.Id);
            }
        }
        #endregion

        #region PROPERTY: DivisionId
        private Int32 _divisionId;
        [EntityColumn(CustomerGroupNameViewColumn.DivisionId, true), Browsable(false)]
        public Int32 DivisionId
        {
            get { return _divisionId; }
            set
            {
                _divisionId = value;
                SetColumnDirty(CustomerGroupNameViewProperty.DivisionId);
            }
        }
        #endregion

        #region PROPERTY: JDECode
        private String _jdeCode;
        [EntityColumn(CustomerGroupNameViewColumn.JDECode), DisplayName("Group")]
        public String JDECode
        {
            get { return _jdeCode; }
            set
            {
                _jdeCode = value;
                SetColumnDirty(CustomerGroupNameViewProperty.JDECode);
            }
        }
        #endregion

        #region PROPERTY: Name
        private String _name;
        [EntityColumn(CustomerGroupNameViewColumn.GroupName)]
        public String Name
        {
            get { return _name; }
            set
            {
                _name = value;
                SetColumnDirty(CustomerGroupNameViewProperty.GroupName);
            }
        }
        #endregion

        #endregion

        #region Constructors

        public CustomerGroupNameView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.CustomerGroupNameView);
            OrderBy = "{" + CustomerGroupNameViewProperty.GroupName + "}";
        }
        #endregion
    }

    public class CustomerGroupNameViewColumn
    {
        public const string Id = "Id";
        public const string DivisionId = "DivisionId";
        public const string JDECode = "JDECode";
        public const string GroupName = "GroupName";
    }

    public class CustomerGroupNameViewProperty
    {
        public const string Id = "Id";
        public const string DivisionId = "DivisionId";
        public const string JDECode = "JDECode";
        public const string GroupName = "GroupName";
    }
}

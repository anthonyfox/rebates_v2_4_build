﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class PaymentStatus : SqlServerEntity
	{
		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(PaymentStatusColumn.Id, true, true), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;
				SetColumnDirty(PaymentStatusProperty.Id);
			}
		}
		#endregion

		#region PROPERTY: Name
		private Int32 _name;
		[EntityColumn(PaymentStatusColumn.Name), Browsable(false)]
		public Int32 Name
		{
			get { return _name; }
			set
			{
				_name = value;
				SetColumnDirty(PaymentStatusProperty.Name);
			}
		}
		#endregion

		#endregion

		#region Constructors

		public PaymentStatus()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.PaymentStatus);
		}

		#endregion
	}

	public class PaymentStatusColumn
	{
		public const string Id = "Id";
		public const string Name = "Name";
	}

	public class PaymentStatusProperty
	{
		public const string Id = "Id";
		public const string Name = "Name";
	}

}

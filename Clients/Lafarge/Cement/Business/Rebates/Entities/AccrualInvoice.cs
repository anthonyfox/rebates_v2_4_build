﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class AccrualInvoice : SqlServerEntity
	{
		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(AccrualInvoiceLineColumn.Id, true, true), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;

				if (this.ExistsOnDatabase)
				{
					SetColumnDirty(AccrualInvoiceLineProperty.Id);
				}
			}
		}
		#endregion

		#region PROPERTY: AccrualId
		private Int32 _accrualId;
		[EntityColumn(AccrualInvoiceLineColumn.AccrualId), Browsable(false)]
		public Int32 AccrualId
		{
			get { return _accrualId; }
			set
			{
				_accrualId = value;

				if (this.ExistsOnDatabase)
				{
					SetColumnDirty(AccrualInvoiceLineProperty.AccrualId);
				}
			}
		}
		#endregion

		#region PROPERTY: InvoiceId
		private Int32 _invoiceId;
		[EntityColumn(AccrualInvoiceLineColumn.InvoiceId, true), Browsable(false)]
		public Int32 InvoiceId
		{
			get { return _invoiceId; }
			set
			{
				_invoiceId = value;
				SetColumnDirty(AccrualInvoiceLineProperty.InvoiceId);
			}
		}
		#endregion

		// Invoice

		#region RELATED ENTITY PROPERTY: Company
		private String _company;
		[EntityColumn(InvoiceLineColumn.Company, DBRebates.InvoiceLine)]
		public String Company
		{
			get { return _company; }
			set { _company = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: DocumentNumber
		private String _documentNumber;
		[EntityColumn(InvoiceLineColumn.DocumentNumber, DBRebates.InvoiceLine), DisplayName("Document Number")]
		public String DocumentNumber
		{
			get { return _documentNumber; }
			set { _documentNumber = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: DocumentType
		private String _documentType;
		[EntityColumn(InvoiceLineColumn.DocumentType, DBRebates.InvoiceLine), DisplayName("Document Type")]
		public String DocumentType
		{
			get { return _documentType; }
			set { _documentType = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: ItemNumber
		private String _itemNumber;
		[EntityColumn(InvoiceLineColumn.LineNumber, DBRebates.InvoiceLine), DisplayName("Item Number")]
		public String ItemNumber
		{
			get { return _itemNumber; }
			set { _itemNumber = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: InvoiceDate
		private DateTime _invoiceDate;
		[EntityColumn(InvoiceLineColumn.Date, DBRebates.InvoiceLine, AccrualInvoiceLineProperty.InvoiceDate), DisplayName("Date")]
		public DateTime InvoiceDate
		{
			get { return _invoiceDate; }
			set { _invoiceDate = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: InvoiceQuantity
		private Decimal _invoiceQuantity;
		[EntityColumn(InvoiceLineColumn.Quantity, DBRebates.InvoiceLine, AccrualInvoiceLineProperty.InvoiceQuantity), DisplayName("Quantity")]
		public Decimal InvoiceQuantity
		{
			get { return _invoiceQuantity; }
			set { _invoiceQuantity = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: InvoiceAmount
		private Decimal _invoiceAmount;
		[EntityColumn(InvoiceLineColumn.Amount, DBRebates.InvoiceLine, AccrualInvoiceLineProperty.InvoiceAmount), DisplayName("Amount")]
		public Decimal InvoiceAmount
		{
			get { return _invoiceAmount; }
			set { _invoiceAmount = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: ProductCode
		private String _productCode;
		[EntityColumn(ProductColumn.JDECode, DBRebates.Product, AccrualInvoiceLineProperty.ProductCode), DisplayName("Product Code")]
		public String ProductCode
		{
			get { return _productCode; }
			set { _productCode = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: ProductName
		private String _productName;
		[EntityColumn(ProductColumn.Name, DBRebates.Product, AccrualInvoiceLineProperty.ProductName), DisplayName("Product Name")]
		public String ProductName
		{
			get { return _productName; }
			set { _productName = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: SiteCode
		private String _siteCode;
		[EntityColumn(SiteColumn.JDECode, DBRebates.Site, AccrualInvoiceLineProperty.SiteCode), DisplayName("Sold From")]
		public String SiteCode
		{
			get { return _siteCode; }
			set { _siteCode = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: SiteName
		private String _siteName;
		[EntityColumn(SiteColumn.Name, DBRebates.Site, AccrualInvoiceLineProperty.SiteName), DisplayName("Name")]
		public String SiteName
		{
			get { return _siteName; }
			set { _siteName = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: SoldToCode
		private Int32 _soldToCode;
		[EntityColumn(CustomerColumn.JDECode, "SoldTo", AccrualInvoiceLineProperty.SoldToCode), DisplayName("Sold To")]
		public Int32 SoldToCode
		{
			get { return _soldToCode; }
			set { _soldToCode = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: SoldToName
		private String _soldToName;
		[EntityColumn(CustomerColumn.Name, "SoldTo", AccrualInvoiceLineProperty.SoldToName), DisplayName("Name")]
		public String SoldToName
		{
			get { return _soldToName; }
			set { _soldToName = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: ShipToCode
		private Int32 _shipToCode;
		[EntityColumn(CustomerColumn.JDECode, "ShipTo", AccrualInvoiceLineProperty.ShipToCode), DisplayName("Sold To")]
		public Int32 ShipToCode
		{
			get { return _shipToCode; }
			set { _shipToCode = value; }
		}
		#endregion

		#region RELATED ENTITY PROPERTY: ShipToName
		private String _shipToName;
		[EntityColumn(CustomerColumn.Name, "ShipTo", AccrualInvoiceLineProperty.ShipToName), DisplayName("Name")]
		public String ShipToName
		{
			get { return _shipToName; }
			set { _shipToName = value; }
		}
		#endregion

		// Calculation Properties

		public int SoldToId { get; set; }
		public int SiteId { get; set; }
		public int ShipToId { get; set; }
		public int ProductId { get; set; }
		public DeliveryTypeEnum DeliveryTypeEnum { get; set; }

		#endregion

		#region Constructors

		public AccrualInvoice()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.AccrualInvoice);

			JoinTable(DBRebates.DatabaseName, DBRebates.InvoiceLine, DBRebates.InvoiceLine + "." + InvoiceLineColumn.Id + " = " + DBRebates.AccrualInvoice + "." + AccrualInvoiceLineColumn.InvoiceId);
			JoinTable(DBRebates.DatabaseName, DBRebates.Product, DBRebates.Product + "." + ProductColumn.Id + " = " + DBRebates.InvoiceLine + "." + InvoiceLineColumn.ProductId);
			JoinTable(DBRebates.DatabaseName, DBRebates.Site, DBRebates.Site + "." + SiteColumn.Id + " = " + DBRebates.InvoiceLine + "." + InvoiceLineColumn.SiteId);
			JoinTable(DBRebates.DatabaseName, DBRebates.Customer, "SoldTo" + "." + CustomerColumn.Id + " = " + DBRebates.InvoiceLine + "." + InvoiceLineColumn.SoldToId, "SoldTo");
			JoinTable(DBRebates.DatabaseName, DBRebates.Customer, "ShipTo" + "." + CustomerColumn.Id + " = " + DBRebates.InvoiceLine + "." + InvoiceLineColumn.ShipToId, "ShipTo");
		}

		#endregion

		#region Business Logic

		public static Int32 DeleteAccruals(Int32 divisionAccrualId)
		{
			AccrualInvoice accrualInvoice = new AccrualInvoice();

			accrualInvoice.ReadFilter =
				accrualInvoice.DatabaseTable + "." + AccrualInvoiceLineColumn.AccrualId + " in ( " +
					"select " + AccrualColumn.Id + " " +
					"from " + accrualInvoice.DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.Accrual) + " " +
					"where " + AccrualColumn.DivisionAccrualId + " = " + divisionAccrualId.ToString() + " " + ")";

			Int32 result = accrualInvoice.DeleteForReadFilter();
			return result;
		}

		#endregion
	}

	public class AccrualInvoiceLineColumn
	{
		public const string Id = "Id";
		public const string AccrualId = "AccrualId";
		public const string InvoiceId = "InvoiceId";
	}

	public class AccrualInvoiceLineProperty
	{
		public const string Id = "Id";
		public const string AccrualId = "AccrualId";
		public const string InvoiceId = "InvoiceId";
		public const string Company = "Company";
		public const string DocumentNumber = "DocumentNumber";
		public const string DocumentType = "DocumentType";
		public const string ItemNumber = "ItemNumber";
		public const string InvoiceDate = "InvoiceDate";
		public const string InvoiceQuantity = "InvoiceQuantity";
		public const string InvoiceAmount = "InvoiceAmount";
		public const string InvoiceDiscountAmount = "InvoiceDiscountAmount";
		public const string ProductCode = "ProductCode";
		public const string ProductName = "ProductName";
		public const string SiteCode = "SiteCode";
		public const string SiteName = "SiteName";
		public const string SoldToCode = "SoldToCode";
		public const string SoldToName = "SoldToName";
		public const string ShipToCode = "ShipToCode";
		public const string ShipToName = "ShipToName";
	}
}

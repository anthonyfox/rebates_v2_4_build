﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class DeliveryWindow : SqlServerEntity, IRebateExtension
	{
		[Browsable(false)]
		public String Title { get { return "Delivery Window"; } }
		[Browsable(false)]
		public String EntityName { get { return "InvoiceLine"; } }
		[Browsable(false)]
		public String EntityTitle { get { return "Invoice Line"; } }
		[Browsable(false)]
		public String PropertyName { get { return InvoiceLineProperty.DeliveryWindowId; } }
		[Browsable(false)]
		public String RebateEntityName { get { return DBRebates.RebateDeliveryWindow; } }
		[Browsable(false)]
		public String RebateEntityTitle { get { return "Rebate"; } }
		[Browsable(false)]
		public String RebatePropertyName { get { return RebateDeliveryWindowProperty.DeliveryWindowId; } }
		[Browsable(false)]
		public Int32 TextBoxWidth { get { return 100; } }
		[Browsable(false)]
		public Int32 MaxLength { get { return 40; } }

		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(DeliveryWindowColumn.Id, true, true), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;
				SetColumnDirty(DeliveryWindowProperty.Id);
			}
		}
		#endregion

		#region PROPERTY: DivisionId
		private Int32 _divisionId;
		[EntityColumn(DeliveryWindowColumn.DivisionId), Browsable(false)]
		public Int32 DivisionId
		{
			get { return _divisionId; }
			set
			{
				_divisionId = value;
				SetColumnDirty(DeliveryWindowProperty.DivisionId);
			}
		}
		#endregion

		#region PROPERTY: JDECode
		private String _jdeCode;
		[EntityColumn(DeliveryWindowColumn.JDECode)]
		[DisplayName("JDE Code")]
		public String JDECode
		{
			get { return _jdeCode; }
			set
			{
				_jdeCode = value;
				SetColumnDirty(DeliveryWindowProperty.JDECode);
			}
		}
		#endregion

		#region PROPERTY: Name
		private String _name;
		[EntityColumn(DeliveryWindowColumn.Name)]
		public String Name
		{
			get { return _name; }
			set
			{
				_name = value;
				SetColumnDirty(DeliveryWindowProperty.Name);
			}
		}
		#endregion

		#region PROPERTY: CreateTime
		private DateTime _createTime;
		[EntityColumn(DeliveryWindowColumn.CreateTime, DBDateTime.Year, DBDateTime.Minute)]
		[Browsable(false)]
		public DateTime CreateTime
		{
			get { return _createTime; }
			set
			{
				_createTime = value;
				SetColumnDirty(DeliveryWindowProperty.CreateTime);
			}
		}
		#endregion

		#region PROPERTY: CreateUserId
		private Int32 _createUserId;
		[EntityColumn(DeliveryWindowColumn.CreateUserId), Browsable(false)]
		public Int32 CreateUserId
		{
			get { return _createUserId; }
			set
			{
				_createUserId = value;
				SetColumnDirty(DeliveryWindowProperty.CreateUserId);
			}
		}
		#endregion

		#region PROPERTY: UpdateTime
		private DateTime _updateTime;
		[EntityColumn(DeliveryWindowColumn.UpdateTime, DBDateTime.Year, DBDateTime.Minute)]
		[Browsable(false)]
		public DateTime UpdateTime
		{
			get { return _updateTime; }
			set
			{
				_updateTime = value;
				SetColumnDirty(DeliveryWindowProperty.UpdateTime);
			}
		}
		#endregion

		#region PROPERTY: UpdateUserId
		private Int32? _updateUserId;
		[EntityColumn(DeliveryWindowColumn.UpdateUserId), Browsable(false)]
		public Int32? UpdateUserId
		{
			get { return _updateUserId; }
			set
			{
				_updateUserId = value;
				SetColumnDirty(DeliveryWindowProperty.UpdateUserId);
			}
		}
		#endregion

		#region PROPERTY: JDECodeName
		public String JDECodeName
		{
			get { return _jdeCode + " - " + _name; }
		}
		#endregion
		
		#endregion

		#region Constructors

		public DeliveryWindow()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.DeliveryWindow);
			OrderBy = DeliveryWindowColumn.JDECode;
		}

		#endregion
	}

	public class DeliveryWindowColumn
	{
		public const string Id = "Id";
		public const string DivisionId = "DivisionId";
		public const string Name = "Name";
		public const string JDECode = "JDECode";
		public const string CreateTime = "CreateTime";
		public const string CreateUserId = "CreateUserId";
		public const string UpdateTime = "UpdateTime";
		public const string UpdateUserId = "UpdateUserId";
	}

	public class DeliveryWindowProperty
	{
		public const string Id = "Id";
		public const string DivisionId = "DivisionId";
		public const string Name = "Name";
		public const string JDECode = "JDECode";
		public const string CreateTime = "CreateTime";
		public const string CreateUserId = "CreateUserId";
		public const string UpdateTime = "UpdateTime";
		public const string UpdateUserId = "UpdateUserId";
		public const string JDECodeName = "JDECodeName";
	}
}

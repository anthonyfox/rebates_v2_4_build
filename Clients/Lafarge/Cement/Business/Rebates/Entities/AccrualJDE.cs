﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Linq;
//using Evolve.Libraries.Data.Remoting;
//using Evolve.Libraries.Utilities;

//namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
//{
//	public class AccrualJDE : SqlServerEntity
//	{
//		#region Properties

//		#region PROPERTY: Id

//		private Int32 _id;

//		[EntityColumn(AccrualJDEColumn.Id, true, true), DisplayName("Id"), Browsable(false)]
//		public Int32 Id
//		{
//			get { return _id; }
//			set
//			{
//				_id = value;
//				SetColumnDirty(AccrualJDEProperty.Id);
//			}
//		}

//		#endregion

//		#region PROPERTY: DivisionAccrualId

//		private Int32 _divisionAccrualId;

//		[EntityColumn(AccrualJDEColumn.DivisionAccrualId), Browsable(false)]
//		public Int32 DivisionAccrualId
//		{
//			get { return _divisionAccrualId; }
//			set
//			{
//				_divisionAccrualId = value;
//				SetColumnDirty(AccrualJDEProperty.DivisionAccrualId);
//			}
//		}

//		#endregion

//		#region RELATED ENTITY PROPERTY: DivisionId

//		private Int32 _divisionId;

//		[EntityColumn(DivisionAccrualColumn.DivisionId, DBRebates.DivisionAccrual, AccrualJDEProperty.DivisionId)]
//		[Browsable(false)]
//		public Int32 DivisionId
//		{
//			get { return _divisionId; }
//			set { _divisionId = value; }
//		}

//		#endregion

//		#region RELATED ENTITY PROPERTY: IsDraft

//		private Boolean _isDraft;

//		[EntityColumn(DivisionAccrualColumn.IsDraft, DBRebates.DivisionAccrual, AccrualJDEProperty.IsDraft), Browsable(false)]
//		public Boolean IsDraft
//		{
//			get { return _isDraft; }
//			set { _isDraft = value; }
//		}

//		#endregion

//		#region RELATED ENTITY PROPERTY: Year

//		private Int16 _year;

//		[EntityColumn(DivisionAccrualColumn.Year, DBRebates.DivisionAccrual, AccrualJDEProperty.Year)]
//		[Browsable(false)]
//		public Int16 Year
//		{
//			get { return _year; }
//			set { _year = value; }
//		}

//		#endregion

//		#region RELATED ENTITY PROPERTY: Period

//		private Int16 _period;

//		[EntityColumn(DivisionAccrualColumn.Period, DBRebates.DivisionAccrual, AccrualJDEProperty.Period)]
//		[Browsable(false)]
//		public Int16 Period
//		{
//			get { return _period; }
//			set { _period = value; }
//		}

//		#endregion

//		#region PROPERTY: ProductId

//		private Int32 _productId;

//		[EntityColumn(AccrualJDEColumn.ProductId), Browsable(false)]
//		public Int32 ProductId
//		{
//			get { return _productId; }
//			set
//			{
//				_productId = value;
//				SetColumnDirty(AccrualJDEProperty.ProductId);
//			}
//		}

//		#endregion

//		#region PROPERTY: SiteId

//		private Int32 _siteId;

//		[EntityColumn(AccrualJDEColumn.SiteId), Browsable(false)]
//		public Int32 SiteId
//		{
//			get { return _siteId; }
//			set
//			{
//				_siteId = value;
//				SetColumnDirty(AccrualJDEProperty.SiteId);
//			}
//		}

//		#endregion

//		#region PROPERTY: SoldToId

//		private Int32 _soldToId;

//		[EntityColumn(AccrualJDEColumn.SoldToId), Browsable(false)]
//		public Int32 SoldToId
//		{
//			get { return _soldToId; }
//			set
//			{
//				_soldToId = value;
//				SetColumnDirty(AccrualJDEProperty.SoldToId);
//			}
//		}

//		#endregion

//		#region PROPERTY: Value

//		private Decimal _value;

//		[EntityColumn(AccrualJDEColumn.Value), Browsable(false)]
//		public Decimal Value
//		{
//			get { return _value; }
//			set
//			{
//				_value = value;
//				SetColumnDirty(AccrualJDEProperty.Value);
//			}
//		}

//		#endregion

//		#region DERIVED PROPERTY: Amount

//		private Decimal _amount;

//		public Decimal Amount
//		{
//			get { return _amount; }
//			set
//			{
//				_amount = value;
//				SetColumnDirty(AccrualJDEProperty.Amount);
//			}
//		}

//		#endregion

//		#region DERIVED PROPERTY: IsReversal

//		private Boolean _isReversal;

//		public Boolean IsReversal
//		{
//			get { return _isReversal; }
//			set	{ _isReversal = value; }
//		}

//		#endregion

//		#endregion

//		#region Constructors

//		public AccrualJDE()
//		{
//			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.AccrualJDE);
//			JoinTable(DBRebates.DatabaseName, DBRebates.DivisionAccrual,
//			          DBRebates.DivisionAccrual + "." + DivisionAccrualColumn.Id + " = " + DBRebates.AccrualJDE + "." +
//			          AccrualJDEColumn.DivisionAccrualId);
//		}

//		#endregion

//		#region Business Logic

//		public static Int32 DeleteAccruals(Int32 divisionAccrualId)
//		{
//			AccrualJDE accrualJDE = new AccrualJDE();

//			accrualJDE.ReadFilter = AccrualJDEColumn.DivisionAccrualId + " = " + divisionAccrualId.ToString();

//			Int32 result = accrualJDE.DeleteForReadFilter();
//			return result;
//		}

//		#endregion
//	}

//	public class AccrualJDEColumn
//	{
//		public const string Id = "Id";
//		public const string DivisionAccrualId = "DivisionAccrualId";
//		public const string ProductId = "ProductId";
//		public const string SiteId = "SiteId";
//		public const string SoldToId = "SoldToId";
//		public const string Value = "Value";
//	}

//	public class AccrualJDEProperty
//	{
//		public const string Id = "Id";
//		public const string DivisionAccrualId = "DivisionAccrualId";
//		public const string DivisionId = "DivisionId";
//		public const string IsDraft = "IsDraft";
//		public const string Year = "Year";
//		public const string Period = "Period";
//		public const string ProductId = "ProductId";
//		public const string SiteId = "SiteId";
//		public const string SoldToId = "SoldToId";
//		public const string Value = "Value";
//		public const string Amount = "Amount";
//		public const string IsReversal = "IsReversal";
//	}
//}

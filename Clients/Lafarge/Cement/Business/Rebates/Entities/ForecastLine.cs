﻿namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class ForecastLine : InvoiceLine
	{
		public ForecastLine()
			: base(DBRebates.ForecastLine)
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.ForecastLine);
		}
	}

	public class ForecastLineColumn : InvoiceLineColumn
	{
	}

	public class ForecastLineProperty : InvoiceLineProperty
	{
	}
}


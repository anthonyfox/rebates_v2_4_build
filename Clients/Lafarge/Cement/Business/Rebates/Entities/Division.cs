﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class Division : SqlServerEntity
	{
		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(DivisionColumn.Id, true, true), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;
				SetColumnDirty(DivisionProperty.Id);
			}
		}
		#endregion

		#region PROPERTY: Name
		private String _name;
		[EntityColumn(DivisionColumn.Name)]
		public String Name
		{
			get { return _name; }
			set
			{
				_name = value;
				SetColumnDirty(DivisionProperty.Name);
			}
		}
		#endregion

		#region PROPERTY: LogoPath
		private String _logoPath;
		[EntityColumn(DivisionColumn.LogoPath)]
		public String LogoPath
		{
			get { return _logoPath; }
			set
			{
				_logoPath = value;
				SetColumnDirty(DivisionProperty.LogoPath);
			}
		}
		#endregion

		#region PROPERTY: LastInvoiceInterface
		private DateTime _lastInvoiceInterface;
		[EntityColumn(DivisionColumn.LastInvoiceInterface)]
		public DateTime LastInvoiceInterface
		{
			get { return _lastInvoiceInterface; }
			set
			{
				_lastInvoiceInterface = value;
				SetColumnDirty(DivisionProperty.LastInvoiceInterface);
			}
		}
		#endregion

		#region PROPERTY: AllowGlobalProduct
		private Boolean _allowGlobalProduct;

		[EntityColumn(DivisionColumn.AllowGlobalProduct)]
		public Boolean AllowGlobalProduct
		{
			get { return _allowGlobalProduct; }
			set
			{
				_allowGlobalProduct = value;
				SetColumnDirty(DivisionProperty.AllowGlobalProduct);
			}
		}
		#endregion

		#region PROPERTY: AllowPaymentAuthorisation
		private Boolean _allowPaymentAuthorisation;

		[EntityColumn(DivisionColumn.AllowPaymentAuthorisation)]
		public Boolean AllowPaymentAuthorisation
		{
			get { return _allowPaymentAuthorisation; }
			set
			{
				_allowPaymentAuthorisation = value;
				SetColumnDirty(DivisionProperty.AllowPaymentAuthorisation);
			}
		}
		#endregion

		#region PROPERTY: IsCalculating
		private Boolean _isCalculating;

		[EntityColumn(DivisionColumn.IsCalculating)]
		public Boolean IsCalculating
		{
			get { return _isCalculating; }
			set
			{
				_isCalculating = value;
				SetColumnDirty(DivisionProperty.IsCalculating);
			}
		}
		#endregion

		#region PROPERTY: CalculationUserId
		private Int32? _calculationUserId;
		[EntityColumn(DivisionColumn.CalculationUserId), Browsable(false)]
		public Int32? CalculationUserId
		{
			get { return _calculationUserId; }
			set
			{
				_calculationUserId = value;
				SetColumnDirty(DivisionProperty.CalculationUserId);
			}
		}
		#endregion

		#region PROPERTY: CalculationTime
		private DateTime? _calculationTime;
		[EntityColumn(DivisionColumn.CalculationTime, DBDateTime.Year, DBDateTime.Second), Browsable(false)]
		public DateTime? CalculationTime
		{
			get { return _calculationTime; }
			set
			{
				_calculationTime = value;
				SetColumnDirty(DivisionProperty.CalculationTime);
			}
		}
		#endregion

		#region PROPERTY: IsForecasting
		private Boolean _isForecasting;

		[EntityColumn(DivisionColumn.IsForecasting)]
		public Boolean IsForecasting
		{
			get { return _isForecasting; }
			set
			{
				_isForecasting = value;
				SetColumnDirty(DivisionProperty.IsForecasting);
			}
		}
		#endregion

		#region PROPERTY: ForecastUserId
		private Int32? _forecastUserId;
		[EntityColumn(DivisionColumn.ForecastUserId), Browsable(false)]
		public Int32? ForecastUserId
		{
			get { return _forecastUserId; }
			set
			{
				_forecastUserId = value;
				SetColumnDirty(DivisionProperty.ForecastUserId);
			}
		}
		#endregion

		#region PROPERTY: ForecastTime
		private DateTime? _forecastTime;
		[EntityColumn(DivisionColumn.ForecastTime, DBDateTime.Year, DBDateTime.Second), Browsable(false)]
		public DateTime? ForecastTime
		{
			get { return _forecastTime; }
			set
			{
				_forecastTime = value;
				SetColumnDirty(DivisionProperty.ForecastTime);
			}
		}
		#endregion

		#region RELATED ENTITY: DivisionAccruals
		private ExtendedBindingList<DivisionAccrual> _divisionAccruals;
		public ExtendedBindingList<DivisionAccrual> DivisionAccruals
		{
			get
			{
				if (_divisionAccruals == null)
				{
					ReadDivisionAccrual();
				}

				return _divisionAccruals;
			}
		}
		#endregion

		#endregion

		#region Constructors

		public Division()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.Division);
		}

		#endregion

		#region Related Entities

		public void ReadDivisionAccrual()
		{
			_divisionAccruals = null;
			DivisionAccrual divisionAccrual = new DivisionAccrual();

			divisionAccrual.DivisionId = this.Id;
			divisionAccrual.AddReadFilter(DivisionAccrualProperty.DivisionId, "=");

			_divisionAccruals = (ExtendedBindingList<DivisionAccrual>) divisionAccrual.Read();
		}

		public override void ReadEntityCollection()
		{
			base.ReadEntityCollection();
			ReadDivisionAccrual();
		}

		public override void  ClearEntityCollection()
		{
			base.ClearEntityCollection();
			_divisionAccruals = null;
		}

		#endregion

		#region Business Logic

		public void RefreshForecastTime()
		{
			DateTime? forecastTime = this.ForecastTime;

			this.ReadCurrent();

			if (this.IsForecasting)
			{
				this.ForecastTime = forecastTime;
			}
		}

		public CalculationPeriod LastAccrualPeriod()
		{
			return AccrualPeriod();
		}

		public CalculationPeriod NextAccrualPeriod()
		{
			CalculationPeriod nextPeriod = AccrualPeriod();
			nextPeriod = nextPeriod.NextPeriod();

			return nextPeriod;
		}


		public DivisionAccrual LastDraftAccrual()
		{
			DivisionAccrual lastDraft = null;

			if (this.DivisionAccruals != null)
			{
				DivisionAccrual divisionAccrual = this.DivisionAccruals
					.OrderByDescending(da => da.Year)
					.ThenByDescending(da => da.Period)
					.Where(da => da.AccrualTypeEnum == AccrualTypeEnum.Actual)
					.FirstOrDefault();

				if (divisionAccrual != null && divisionAccrual.IsDraft)
				{
					lastDraft = divisionAccrual;
				}
			}

			return lastDraft;
		}

		public DateTime CurrentYear()
		{
			CalculationPeriod nextPeriod = this.NextAccrualPeriod();
			return new DateTime(nextPeriod.Year, 1, 1);
		}

		public DivisionAccrual LastAccrual()
		{
			DivisionAccrual lastAccrual = null;

			if (this.DivisionAccruals != null)
			{
				lastAccrual = this.DivisionAccruals
					.OrderByDescending(da => da.Year)
					.ThenByDescending(da => da.Period)
					.FirstOrDefault(da => da.AccrualTypeEnum == AccrualTypeEnum.Actual && da.IsDraft == false);
			}

			return lastAccrual;
		}

		private CalculationPeriod AccrualPeriod()
		{
			CalculationPeriod accrualPeriod = new CalculationPeriod();

			// Find last non-draft accrual
			if (this.DivisionAccruals != null)
			{
				DivisionAccrual divisionAccrual = this.DivisionAccruals
					.OrderByDescending(da => da.Year)
					.ThenByDescending(da => da.Period)
					.FirstOrDefault(da => da.AccrualTypeEnum == AccrualTypeEnum.Actual && !da.IsDraft);

				if (divisionAccrual != null)
				{
					accrualPeriod = new CalculationPeriod(divisionAccrual.Year, divisionAccrual.Period);
				}
			}

			return accrualPeriod;
		}

		public Boolean StartCalculation(User user, AccrualTypeEnum accrualTypeEnum)
		{
			// Get calculation property details
			Type type = this.GetType();

			PropertyInfo propertyIsCalculating = null;
			PropertyInfo propertyCalculationUserId = null;
			PropertyInfo propertyCalculationTime = null;

			switch (accrualTypeEnum)
			{
				case AccrualTypeEnum.Actual:
					propertyIsCalculating = type.GetProperty(DivisionProperty.IsCalculating);
					propertyCalculationUserId = type.GetProperty(DivisionProperty.CalculationUserId);
					propertyCalculationTime = type.GetProperty(DivisionProperty.CalculationTime);
					break;

				case AccrualTypeEnum.Forecast:
					propertyIsCalculating = type.GetProperty(DivisionProperty.IsForecasting);
					propertyCalculationUserId = type.GetProperty(DivisionProperty.ForecastUserId);
					propertyCalculationTime = type.GetProperty(DivisionProperty.ForecastTime);
					break;

				default:
					throw new ArgumentOutOfRangeException("Cannot start calculation, invalid AccrualTypeEnum specified");
			}

			if (propertyIsCalculating == null || propertyCalculationUserId == null || propertyCalculationTime == null)
			{
				throw new ArgumentNullException("Invalid or missing calculation property");
			}

			// Now attempt to start the calculationk
			Boolean isStarted = false;

			this.ReadCurrent();

			if (!((Boolean) propertyIsCalculating.GetValue(this, null)))
			{
				propertyIsCalculating.SetValue(this, true, null);
				propertyCalculationUserId.SetValue(this, user.Id, null);
				propertyCalculationTime.SetValue(this, DateTime.Now, null);

				try
				{
					isStarted = (this.AcceptChanges() >= 0);
				}
				catch
				{
					propertyIsCalculating.SetValue(this, false, null);
				}
			}

			return isStarted;
		}

		public void EndCalculation(User user, AccrualTypeEnum accrualTypeEnum)
		{
			// Get calculation property details
			Type type = this.GetType();

			PropertyInfo propertyIsCalculating = null;
			PropertyInfo propertyCalculationUserId = null;

			switch (accrualTypeEnum)
			{
				case AccrualTypeEnum.Actual:
					propertyIsCalculating = type.GetProperty(DivisionProperty.IsCalculating);
					propertyCalculationUserId = type.GetProperty(DivisionProperty.CalculationUserId);
					break;

				case AccrualTypeEnum.Forecast:
					propertyIsCalculating = type.GetProperty(DivisionProperty.IsForecasting);
					propertyCalculationUserId = type.GetProperty(DivisionProperty.ForecastUserId);
					break;

				default:
					throw new ArgumentOutOfRangeException("Cannot start calculation, invalid AccrualTypeEnum specified");
			}

			if (propertyIsCalculating == null || propertyCalculationUserId == null)
			{
				throw new ArgumentNullException("Invalid or missing calculation property");
			} 
			
			this.ReadCurrent();

			if ((Boolean) propertyIsCalculating.GetValue(this, null) && (Int32) propertyCalculationUserId.GetValue(this, null) == user.Id)
			{
				propertyIsCalculating.SetValue(this, false, null);
				Int32 retry = 0;

				while (this.AcceptChanges() < 0)
				{
					retry++;
					System.Threading.Thread.Sleep(1000);

					if (retry > 5)
					{
						throw new DatabaseException("Failed to release " + accrualTypeEnum.ToString() + " Calculation Lock for Division Id " + this.Id.ToString());
					}
				}
			}
			else if ((Boolean) propertyIsCalculating.GetValue(this, null))
			{
				throw new ArgumentException(accrualTypeEnum.ToString() + " Calculation Lock User Id = " + this.CalculationUserId.ToString() + ", unlock Request User Id = ", user.Id.ToString());
			}
		}

		#endregion
	}

	public class DivisionColumn
	{
		public const string Id = "Id";
		public const string Name = "Name";
		public const string LogoPath= "LogoPath";
		public const string LastInvoiceInterface = "LastInvoiceInterface";
		public const string AllowGlobalProduct = "AllowGlobalProduct";
		public const string AllowPaymentAuthorisation = "AllowPaymentAuthorisation";
		public const string IsCalculating = "IsCalculating";
		public const string CalculationUserId = "CalculationUserId";
		public const string CalculationTime = "CalculationTime";
		public const string IsForecasting = "IsForecasting";
		public const string ForecastUserId = "ForecastUserId";
		public const string ForecastTime = "ForecastTime";
	}

	public class DivisionProperty
	{
		public const string Id = "Id";
		public const string Name = "Name";
		public const string LogoPath= "LogoPath";
		public const string LastInvoiceInterface = "LastInvoiceInterface";
		public const string AllowGlobalProduct = "AllowGlobalProduct";
		public const string AllowPaymentAuthorisation = "AllowPaymentAuthorisation";
		public const string DivisionalAccrruals = "DivisionalAccrruals";
		public const string IsCalculating = "IsCalculating";
		public const string CalculationUserId = "CalculationUserId";
		public const string CalculationTime = "CalculationTime";
		public const string IsForecasting = "IsForecasting";
		public const string ForecastUserId = "ForecastUserId";
		public const string ForecastTime = "ForecastTime";
	}
}

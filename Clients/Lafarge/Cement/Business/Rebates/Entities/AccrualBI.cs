﻿using System;
using Evolve.Libraries.Data.Remoting;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class AccrualBI : SqlServerEntity
	{
		#region Properties

		[EntityColumn(AccrualBIColumn.Id, IsIdentity = true, IsKeyColumn = true)]
		public int Id { get; set; }

		[EntityColumn(AccrualBIColumn.DivisionAccrualId)]
		public int DivisionAccrualId { get; set; }

		[EntityColumn(AccrualBIColumn.SiteId)]
		public int SiteId { get; set; }

		[EntityColumn(AccrualBIColumn.SoldToId)]
		public int SoldToId { get; set; }

		[EntityColumn(AccrualBIColumn.ShipToId)]
		public int ShipToId { get; set; }

		[EntityColumn(AccrualBIColumn.ProductId)]
		public int ProductId { get; set; }

		[EntityColumn(AccrualBIColumn.DeliveryTypeEnum)]
		public DeliveryTypeEnum DeliveryTypeEnum { get; set; }

		[EntityColumn(AccrualBIColumn.RebateTypeId)]
		public int RebateTypeId { get; set; }

		[EntityColumn(AccrualBIColumn.Year)]
		public Int16 Year { get; set; }

		[EntityColumn(AccrualBIColumn.Period)]
		public Int16 Period { get; set; }

		[EntityColumn(AccrualBIColumn.SaleQuantity)]
		public Decimal SaleQuantity { get; set; }

		[EntityColumn(AccrualBIColumn.RebateAmount)]
		public Decimal RebateAmount { get; set; }

		[EntityColumn(AccrualBIColumn.UpdateDateTime)]
		public DateTime UpdateDateTime { get; set; }

		#endregion

		#region Constructors

		public AccrualBI()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.AccrualBI);
		}

		#endregion

		#region Business Logic

		public static Int32 DeleteAccrualBIs(Int32 divisionAccrualId)
		{
			AccrualBI accrualBI = new AccrualBI();

			accrualBI.ReadFilter = AccrualBIColumn.DivisionAccrualId + " = " + divisionAccrualId.ToString();

			Int32 result = accrualBI.DeleteForReadFilter();
			return result;
		}

		#endregion
	}

	public static class AccrualBIColumn
	{
		public const string Id = "Id";
		public const string DivisionAccrualId = "DivisionAccrualId";
		public const string SiteId = "SiteId";
		public const string SoldToId = "SoldToId";
		public const string ShipToId = "ShipToId";
		public const string ProductId = "ProductId";
		public const string DeliveryTypeEnum = "DeliveryTypeEnum";
		public const string RebateTypeId = "RebateTypeId";
		public const string Year = "Year";
		public const string Period = "Period";
		public const string SaleQuantity = "SaleQuantity";
		public const string RebateAmount = "RebateAmount";
		public const string UpdateDateTime = "UpdateDateTime";
	}

	public static class AccrualBIProperty
	{
		public const string Id = "Id";
		public const string DivisionAccrualId = "DivisionAccrualId";
		public const string SoldToId = "SoldToId";
		public const string SiteId = "SiteId";
		public const string ShipToId = "ShipToId";
		public const string ProductId = "ProductId";
		public const string DeliveryTypeEnum = "DeliveryTypeEnum";
		public const string RebateTypeId = "RebateTypeId";
		public const string Year = "Year";
		public const string Period = "Period";
		public const string SaleQuantity = "SaleQuantity";
		public const string RebateAmount = "RebateAmount";
		public const string UpdateDateTime = "UpdateDateTime";
	}
}

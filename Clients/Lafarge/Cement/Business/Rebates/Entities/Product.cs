﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class Product : SqlServerEntity
	{
		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(ProductColumn.Id), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{
				_id = value;
				SetColumnDirty(ProductProperty.Id);
			}
		}
		#endregion

		#region PROPERTY: DivisionId
		private Int32 _divisionId;
		[EntityColumn(ProductColumn.DivisionId), Browsable(false)]
		public Int32 DivisionId
		{
			get { return _divisionId; }
			set
			{
				_divisionId = value;
				SetColumnDirty(ProductProperty.DivisionId);
			}
		}
		#endregion

		#region PROPERTY: JDECode
		private String _jdeCode;
		[EntityColumn(ProductColumn.JDECode), DisplayName("JDECode")]
		public String JDECode
		{
			get { return _jdeCode; }
			set
			{
				_jdeCode = value;
				SetColumnDirty(ProductProperty.JDECode);
			}
		}
		#endregion

		#region PROPERTY: Name
		private String _name;
		[EntityColumn(ProductColumn.Name), DisplayName("Name")]
		public String Name
		{
			get { return _name; }
			set
			{
				_name = value;
				SetColumnDirty(ProductProperty.Name);
			}
		}
		#endregion

		#region PROPERTY: AttributeId1
		private Int32 _attributeId1;
		[EntityColumn(ProductColumn.AttributeId1), Browsable(false)]
		public Int32 AttributeId1
		{
			get { return _attributeId1; }
			set
			{
				_attributeId1 = value;
				SetColumnDirty(ProductProperty.AttributeId1);
			}
		}
		#endregion

		#region PROPERTY: AttributeId2
		private Int32 _attributeId2;
		[EntityColumn(ProductColumn.AttributeId2), Browsable(false)]
		public Int32 AttributeId2
		{
			get { return _attributeId2; }
			set
			{
				_attributeId2 = value;
				SetColumnDirty(ProductProperty.AttributeId2);
			}
		}
		#endregion

		#region PROPERTY: AttributeId3
		private Int32 _attributeId3;
		[EntityColumn(ProductColumn.AttributeId3), Browsable(false)]
		public Int32 AttributeId3
		{
			get { return _attributeId3; }
			set
			{
				_attributeId3 = value;
				SetColumnDirty(ProductProperty.AttributeId3);
			}
		}
		#endregion

		#region PROPERTY: AttributeId4
		private Int32 _attributeId4;
		[EntityColumn(ProductColumn.AttributeId4), Browsable(false)]
		public Int32 AttributeId4
		{
			get { return _attributeId4; }
			set
			{
				_attributeId4 = value;
				SetColumnDirty(ProductProperty.AttributeId4);
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: AttributeName1
		private String _attributeName1;
		[EntityColumn(AttributeColumn.Name, "Attribute1", ProductProperty.AttributeName1), DisplayName("Attribute 1")]
		public String AttributeName1
		{
			get { return _attributeName1; }
			set
			{
				_attributeName1 = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: AttributeName2
		private String _attributeName2;
		[EntityColumn(AttributeColumn.Name, "Attribute2", ProductProperty.AttributeName2), DisplayName("Attribute 2")]
		public String AttributeName2
		{
			get { return _attributeName2; }
			set
			{
				_attributeName2 = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: AttributeName3
		private String _attributeName3;
		[EntityColumn(AttributeColumn.Name, "Attribute3", ProductProperty.AttributeName3), DisplayName("Attribute 3")]
		public String AttributeName3
		{
			get { return _attributeName3; }
			set
			{
				_attributeName3 = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: AttributeName4
		private String _attributeName4;
		[EntityColumn(AttributeColumn.Name, "Attribute4", ProductProperty.AttributeName4), DisplayName("Attribute 4")]
		public String AttributeName4
		{
			get { return _attributeName4; }
			set
			{
				_attributeName4 = value;
			}
		}
        #endregion

        #region PROPERTY: InterfaceDateTime
        private DateTime _interfaceDateTime;
        [EntityColumn(ProductColumn.InterfaceDateTime, DBDateTime.Year, DBDateTime.Second), Browsable(false)]
        public DateTime InterfaceDateTime
        {
            get { return _interfaceDateTime; }
            set
            {
                _interfaceDateTime = value;
                SetColumnDirty(ProductProperty.InterfaceDateTime);
            }
        }
        #endregion

        #region PROPERTY: IsActive
        private bool _isActive;
        [EntityColumn(ProductColumn.IsActive), Browsable(false)]
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                SetColumnDirty(ProductProperty.IsActive);
            }
        }
        #endregion

        #region PROPERTY: IsPurchased
        private bool _isPurchased;
        [EntityColumn(ProductColumn.IsPurchased), Browsable(false)]
        public bool IsPurchased
        {
            get { return _isPurchased; }
            set
            {
                _isPurchased = value;
                SetColumnDirty(ProductProperty.IsPurchased);
            }
        }
        #endregion

        #endregion

        #region Constructors

        public Product()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.Product);

			JoinTable(DBRebates.DatabaseName, DBRebates.Attribute, "Attribute1" + "." + AttributeColumn.Id + " = " + DBRebates.Product + "." + ProductColumn.AttributeId1 + " " +
				"and " + "Attribute1" + "." + AttributeColumn.Level + " = 1 ", "Attribute1", EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.Attribute, "Attribute2" + "." + AttributeColumn.Id + " = " + DBRebates.Product + "." + ProductColumn.AttributeId2 + " " +
				"and " + "Attribute2" + "." + AttributeColumn.Level + " = 2 ", "Attribute2", EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.Attribute, "Attribute3" + "." + AttributeColumn.Id + " = " + DBRebates.Product + "." + ProductColumn.AttributeId3 + " " +
				"and " + "Attribute3" + "." + AttributeColumn.Level + " = 3 ", "Attribute3", EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.Attribute, "Attribute4" + "." + AttributeColumn.Id + " = " + DBRebates.Product + "." + ProductColumn.AttributeId4 + " " +
				"and " + "Attribute4" + "." + AttributeColumn.Level + " = 4 ", "Attribute4", EntityJoinType.LeftOuter);

			OrderBy =
				"{" + ProductProperty.JDECode + "}";
		}

		#endregion

		#region Business Logic

		/// <summary>
		/// Search for customers matching specified criteria
		/// </summary>
		public ExtendedBindingList<Product> Search(String name, Int32 attributeId1, Int32 attributeId2, Int32 attributeId3, Int32 attributeId4, Int32 rowLimit, out Boolean isRowLimitExceeded)
		{
			String filter = "";

			filter += SqlHelper.FilterBuilder(filter, "{" + ProductProperty.DivisionId + "} = " + _divisionId.ToString());

			if (name.Length > 0)
			{
				filter = SqlHelper.FilterBuilder(filter, "{" + ProductProperty.Name + "} " + this.WildcardKeyword() + " '" + name.Replace('*', this.WildcardCharacter()) + this.WildcardCharacter() + "'");
			}

			if (attributeId1 > 0)
			{
				filter = SqlHelper.FilterBuilder(filter, "{" + ProductProperty.AttributeId1 + "} = " + attributeId1.ToString());
			}

			if (attributeId2 > 0)
			{
				filter = SqlHelper.FilterBuilder(filter, "{" + ProductProperty.AttributeId2 + "} = " + attributeId2.ToString());
			}

			if (attributeId3 > 0)
			{
				filter = SqlHelper.FilterBuilder(filter, "{" + ProductProperty.AttributeId3 + "} = " + attributeId3.ToString());
			}

			if (attributeId4 > 0)
			{
				filter = SqlHelper.FilterBuilder(filter, "{" + ProductProperty.AttributeId4 + "} = " + attributeId4.ToString());
			}

			Product product = new Product();
			product.ReadFilter = filter;
			ExtendedBindingList<Product> products = (ExtendedBindingList<Product>) product.Read(rowLimit, out isRowLimitExceeded);

			return products;
		}

		#endregion
	}

	public class ProductColumn
	{
		public const string Id = "Id";
		public const string DivisionId = "DivisionId";
		public const string JDECode = "JDECode";
		public const string Name = "Name";
		public const string AttributeId1 = "AttributeId1";
		public const string AttributeId2 = "AttributeId2";
		public const string AttributeId3 = "AttributeId3";
		public const string AttributeId4 = "AttributeId4";
		public const string InterfaceDateTime = "InterfaceTime";
		public const string IsActive = "Active";
		public const string IsPurchased = "IsPurchased";
	}

	public class ProductProperty
	{
		public const string Id = "Id";
		public const string DivisionId = "DivisionId";
		public const string JDECode = "JDECode";
		public const string Name = "Name";
		public const string AttributeId1 = "AttributeId1";
		public const string AttributeId2 = "AttributeId2";
		public const string AttributeId3 = "AttributeId3";
		public const string AttributeId4 = "AttributeId4";
		public const string AttributeName1 = "AttributeName1";
		public const string AttributeName2 = "AttributeName2";
		public const string AttributeName3 = "AttributeName3";
		public const string AttributeName4 = "AttributeName4";
        public const string InterfaceDateTime = "InterfaceDateTime";
        public const string IsActive = "IsActive";
        public const string IsPurchased = "IsPurchased";
    }
}

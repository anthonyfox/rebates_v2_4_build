﻿using System;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public enum DeliveryTypeEnum
	{
		Null,
		Delivery,
		Collect
	}

	public static class DeliveryTypeEnumConvertor
	{
		public static DeliveryTypeEnum DBInt16ToDeliveryTypeEnum(Object dbObject)
		{
			DeliveryTypeEnum deliveryType = DeliveryTypeEnum.Null;

			if (!Object.ReferenceEquals(dbObject, System.DBNull.Value))
			{
				Int16 value = Convert.ToInt16(dbObject);

				switch (value)
				{
					case 1:
						deliveryType = DeliveryTypeEnum.Delivery;
						break;
					case 2:
						deliveryType = DeliveryTypeEnum.Collect;
						break;
					default:
						deliveryType = DeliveryTypeEnum.Null;
						break;
				}
			}

			return deliveryType;
		}

		public static Object DeliveryTypeEnumToDBInt16(DeliveryTypeEnum deliveryType)
		{
			Object value = DBNull.Value;

			switch (deliveryType)
			{
				case DeliveryTypeEnum.Delivery:
					value = 1;
					break;
				case DeliveryTypeEnum.Collect:
					value = 2;
					break;
			}

			return value;
		}
	}
}

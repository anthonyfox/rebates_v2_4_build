﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public enum CustomerSearchTypeEnum
	{
		Null,
		SoldTo,
		ShipTo,
		SoldToInActive,
		ShipToInActive
	}

	public static class CustomerSearchTypeEnumConvertor
	{
		public static CustomerSearchTypeEnum DBStringToCustomerSearchTypeEnum(Object dbObject)
		{
			CustomerSearchTypeEnum accrualType = CustomerSearchTypeEnum.Null;

			if (!Object.ReferenceEquals(dbObject, System.DBNull.Value))
			{
				String value = dbObject.ToString();

				switch (value)
				{
					case "C":
						accrualType = CustomerSearchTypeEnum.SoldTo;
						break;
					case "XC":
						accrualType = CustomerSearchTypeEnum.SoldToInActive;
						break;
					case "CS":
						accrualType = CustomerSearchTypeEnum.ShipTo;
						break;
					case "XCS":
						accrualType = CustomerSearchTypeEnum.ShipToInActive;
						break;
					default:
						accrualType = CustomerSearchTypeEnum.Null;
						break;
				}
			}

			return accrualType;
		}

		public static Object CustomerSearchTypeEnumToDBString(CustomerSearchTypeEnum customerSearchTypeEnum)
		{
			Object value = DBNull.Value;

			switch (customerSearchTypeEnum)
			{
				case CustomerSearchTypeEnum.SoldTo:
					value = "C";
					break;
				case CustomerSearchTypeEnum.SoldToInActive:
					value = "XC";
					break;
				case CustomerSearchTypeEnum.ShipTo:
					value = "CS";
					break;
				case CustomerSearchTypeEnum.ShipToInActive:
					value = "XCS";
					break;
			}

			return value;
		}
	}
}

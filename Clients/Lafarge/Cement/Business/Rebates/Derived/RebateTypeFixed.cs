﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class RebateTypeFixed : RebateTypeCalculator
	{
		public RebateTypeFixed(Rebate rebate)
			: base(rebate)
		{
			this.IsSuspendedCalculation = true;
		}

		public override void CalculateGlobalProduct()
		{
			// If this is the first period being processed for the rebate initialise the
			// sales volumes list
			if (this.Period.Period == this.Rebate.StartDate.Month)
			{
				InitialiseCollections();
			}

			// Keep track of the sales volumes and accruals per period 
			this.Rebate.SalesVolumes.Add(this.Accrual.SalesVolume ?? 0);
			this.Rebate.Accruals.Add(this.Accrual);

			// Delay calculation of accruals for fixed rebate until the final calculation period
			// as the rate is based on the value for all periods
			if (this.CalculationPeriod.Equals(this.Period))
			{
				foreach (Accrual accrual in this.Rebate.Accruals)
				{
					this.Accrual = accrual;

					// Calculate the relevant rate
					this.Accrual.Rate = GetRate();

					if (this.Accrual.SalesVolume.HasValue)
					{
						this.Accrual.Value = this.Accrual.SalesVolume.Value * this.Accrual.Rate.Value;
					}
					else
					{
						this.Accrual.Value = 0;
					}
				}
			}
		}

		private Decimal GetRate()
		{
			Decimal value = 0;

			if (this.Rebate.FixedRebateValue.HasValue && this.Rebate.NumberOfPeriods > 0)
			{
				value = Math.Round(this.Rebate.FixedRebateValue.Value / (Decimal) this.Rebate.NumberOfPeriods, 3);
				value *= this.CalculationPeriod.Period;

				// If we are processing the final period for the full Rebate value is applied
				if (this.Accrual.AccrualPeriod.Period == this.Rebate.EndPeriod)
				{
					value = this.Rebate.FixedRebateValue.Value;
				}
			} 
			
			Decimal rate = 0;
			Decimal totalSalesVolume = this.Rebate.SalesVolumes.Sum();

			if (totalSalesVolume != 0)
			{
				rate = Math.Round(value / totalSalesVolume, 3);
			}

			return rate;
		}

		public Decimal GetFixedRate()
		{
			Decimal rate = 0;

			if (this.Rebate.FixedRebateValue.HasValue && this.Rebate.EstimatedVolume.HasValue && this.Rebate.EstimatedVolume.Value != 0)
			{
				rate = Math.Round(((Decimal) this.Rebate.FixedRebateValue.Value / (Decimal) this.Rebate.EstimatedVolume.Value), 3);
			}

			return rate;
		}

		public override void CalculateDetail()
		{
			// If this is the first period being processed for the rebate initialise the
			// sales volumes list
			if (this.Period.Period == this.Rebate.StartDate.Month)
			{
				InitialiseCollections();
			}

			// Keep track of the sales volumes and accruals per period 
			this.Rebate.SalesVolumes.Add(this.Accrual.SalesVolume ?? 0);
			this.Rebate.Accruals.Add(this.Accrual);

			// Calculate the ending period of the rebate and use that period if its before the current calculation period
			// otherwise use calcu
			CalculationPeriod rebateEndPeriod = this.CalculationPeriod;

			if (this.Rebate.EndDate.HasValue && this.Rebate.EndDate.Value.Year == this.CalculationPeriod.Year && this.Rebate.EndDate.Value.Month < this.CalculationPeriod.Period)
			{
				rebateEndPeriod = new CalculationPeriod((short)this.Rebate.EndDate.Value.Year, (short)this.Rebate.EndDate.Value.Month);
			}

			// Delay calculation of accruals for fixed rebate until the final calculation period
			// as the rate is based on the value for all periods
			if (this.Period.Equals(rebateEndPeriod))
			{
				// Calculate the relevant rate
				Decimal rate = GetRate();
				Hierarchy baseHierarchy = this.Hierarchys[this.Hierarchys.Count - 1];

				foreach (Accrual accrual in this.Rebate.Accruals)
				{
					Decimal value = 0;
					this.Accrual = accrual;

					// Now apply to the accrual breakdown
					foreach (AccrualDetail accrualDetail in this.Accrual.AccrualDetails)
					{
						if (accrualDetail.HierarchyId == baseHierarchy.Id)
						{
							accrualDetail.Rate = rate;
							accrualDetail.Value = Math.Round(accrualDetail.SalesVolume * rate, 3);

							AccrualDetail parentAccrualDetail = accrualDetail.FindParent(this.Accrual.AccrualDetails, this.Hierarchys);

							while (parentAccrualDetail != null)
							{
								parentAccrualDetail.Rate = rate;
								parentAccrualDetail.Value += accrualDetail.Value;

								parentAccrualDetail = parentAccrualDetail.FindParent(this.Accrual.AccrualDetails, this.Hierarchys);
							}

							this.Accrual.Rate = rate;
							value += accrualDetail.Value;
						}
					}

					this.Accrual.Value = value;
				}
			}
		}

		public override void CalculatePriceEnquiryRate()
		{
			Decimal rate = GetFixedRate();

			foreach (RebateDetail rebateDetail in this.Rebate.RebateDetails)
			{
				rebateDetail.PriceEnquiryRate = rate;
			}
		}

		public override void ValidateData()
		{
			base.ValidateData();

			if (!this.Rebate.FixedRebateValue.HasValue)
			{
				throw new DataException("Fixed Rebate: No fixed rebate value specified");
			}

			if (!this.Rebate.EstimatedVolume.HasValue)
			{
				throw new DataException("Fixed Rebate: No fixed rebate value specified");
			}
		}

		private void InitialiseCollections()
		{
			this.Rebate.SalesVolumes = new List<Decimal>();
			this.Rebate.Accruals = new List<Accrual>();
			this.Rebate.PostedAccruals = new List<List<Accrual>>();
		}
	}
}

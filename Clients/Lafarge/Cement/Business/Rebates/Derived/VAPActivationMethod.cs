﻿using System;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public enum VAPActivationMethod
	{
		Null,
		Global,
		Individual
	}

	public static class VAPActivationMethodConvertor
	{
		public static VAPActivationMethod DBInt16ToVAPActivationMethod(Object dbObject)
		{
			VAPActivationMethod vapActivationMethod = VAPActivationMethod.Null;

			if (!Object.ReferenceEquals(dbObject, DBNull.Value))
			{
				Int16 value = Convert.ToInt16(dbObject);

				switch (value)
				{
					case 1:
						vapActivationMethod = VAPActivationMethod.Global;
						break;
					case 2:
						vapActivationMethod = VAPActivationMethod.Individual;
						break;
				}
			}

			return vapActivationMethod;
		}

		public static Object VAPActivationMethodToDBString(VAPActivationMethod vapActivationMethod)
		{
			Object value = DBNull.Value;

			switch (vapActivationMethod)
			{
				case VAPActivationMethod.Global:
					value = 1;
					break;
				case VAPActivationMethod.Individual:
					value = 2;
					break;
			}

			return value;
		}
	}
}
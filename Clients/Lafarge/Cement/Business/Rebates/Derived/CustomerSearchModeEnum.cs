﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public enum CustomerSearchModeEnum
	{
		Undefined,
		SoldTo,
		ShipTo
	}

	public static class CustomerSearchModeEnumConvertor
	{
		public static CustomerSearchModeEnum DBInt16ToCustomerSearchModeEnum(Object dbObject)
		{
			CustomerSearchModeEnum searchMode = CustomerSearchModeEnum.Undefined;

			if (!Object.ReferenceEquals(dbObject, System.DBNull.Value))
			{
				Int16 value = Convert.ToInt16(dbObject);

				switch (value)
				{
					case 1:
						searchMode = CustomerSearchModeEnum.SoldTo;
						break;
					case 2:
						searchMode = CustomerSearchModeEnum.ShipTo;
						break;
					default:
						searchMode = CustomerSearchModeEnum.Undefined;
						break;
				}
			}

			return searchMode;
		}

		public static Object CustomerSearchModeEnumToDBInt16(CustomerSearchModeEnum customerSearchModeEnum)
		{
			Object value = 0;

			switch (customerSearchModeEnum)
			{
				case CustomerSearchModeEnum.SoldTo:
					value = 1;
					break;
				case CustomerSearchModeEnum.ShipTo:
					value = 2;
					break;
			}

			return value;
		}
	}
}

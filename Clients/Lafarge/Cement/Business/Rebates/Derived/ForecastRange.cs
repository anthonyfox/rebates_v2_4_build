﻿using System;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class ForecastRange : SqlServerEntity
	{
		#region FUNCTION PROPERTY: StartDate
		private DateTime? _startDate;

		[EntityColumn(ForecastLineColumn.Date, ColumnExpression = EntityColumnExpression.Min, ColumnAlias = ForecastRangeProperty.StartDate)]
		public DateTime? StartDate
		{
			get { return _startDate; }
			set { _startDate = value; }
		}
		#endregion

		#region FUNCTION PROPERTY: EndDate
		private DateTime? _endDate;

		[EntityColumn(ForecastLineColumn.Date, ColumnExpression = EntityColumnExpression.Max, ColumnAlias = ForecastRangeProperty.EndDate)]
		public DateTime? EndDate
		{
			get { return _endDate; }
			set { _endDate = value; }
		}
		#endregion

		public ForecastRange()
		{
			this.DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.ForecastLine);
		}

		public static ForecastRange ReadSingle()
		{
			ForecastRange forecastRange = new ForecastRange();
			ExtendedBindingList<ForecastRange> forecastRanges = (ExtendedBindingList<ForecastRange>) forecastRange.Read();

			if (forecastRanges.Count > 0)
			{
				forecastRange = forecastRanges[0];
			}
			else
			{
				forecastRange = null;
			}

			return forecastRange;
		}
	}

	public static class ForecastRangeProperty
	{
		public const string StartDate = "StartDate";
		public const string EndDate = "EndDate";
	}
}

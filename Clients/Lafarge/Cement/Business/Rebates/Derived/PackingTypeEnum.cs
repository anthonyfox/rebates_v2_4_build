﻿using System;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public enum PackingTypeEnum
	{
		Null,
		Pack,
		Bulk
	}

	public static class PackingTypeEnumConvertor
	{
		public static PackingTypeEnum DBInt16ToPackingTypeEnum(Object dbObject)
		{
			PackingTypeEnum packingType = PackingTypeEnum.Null;

			if (!Object.ReferenceEquals(dbObject, System.DBNull.Value))
			{
				Int16 value = Convert.ToInt16(dbObject);

				switch (value)
				{
					case 1:
						packingType = PackingTypeEnum.Pack;
						break;
					case 2:
						packingType = PackingTypeEnum.Bulk;
						break;
					default:
						packingType = PackingTypeEnum.Null;
						break;
				}
			}

			return packingType;
		}

		public static Object PackingTypeEnumToDBInt16(PackingTypeEnum packingType)
		{
			Object value = DBNull.Value;

			switch (packingType)
			{
				case PackingTypeEnum.Pack:
					value = 1;
					break;
				case PackingTypeEnum.Bulk:
					value = 2;
					break;
			}

			return value;
		}
	}
}

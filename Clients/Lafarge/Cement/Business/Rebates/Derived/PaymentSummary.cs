﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Evolve.Libraries.Utilities;
using System.Reflection;
using System.Data;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class PaymentSummary : INotifyPropertyChanged
	{
		private ExtendedBindingList<Rebate> _rebates;
		private Boolean _showAll;
		private Division _division;
		private CalculationPeriod _payThruPeriod;
		private CalculationPeriod _currentPeriod;
		private User _user;
		private List<RebatePaymentSummary> _rebatePaymentSummary;
		private DateTime _saveDate;
		private List<Payment> _payments;
		private BindingList<PaymentBreakdown> _paymentBreakdowns;
		private String _rebateInclude;
		private DateTime _now = DateTime.Now;

		public RebatePaymentSummary this[int index]
		{
			get { return _rebatePaymentSummary[index]; }
		}

		public List<RebatePaymentSummary> List
		{
			get { return _rebatePaymentSummary; }
		}

		public List<Payment> Payments
		{
			get { return _payments; }
		}

		public BindingList<PaymentBreakdown> PaymentBreakdowns
		{
			get { return _paymentBreakdowns; }
		}

		public Decimal AssignedPaymentAmount
		{
			get
			{
				return _paymentBreakdowns.Sum(pb => pb.Value);
			}
		}

		public Decimal UnAssignedPaymentAmount
		{
			get
			{
				return (this.Proposed - this.AssignedPaymentAmount);
			}
		}

		// Payment Summary
		public CalculationPeriod PayThruPeriod
		{
			get { return _payThruPeriod; }
		}

		public Decimal Accrued
		{
			get { return _rebatePaymentSummary.Sum(rps => rps.Accrual.Total); }
		}

		public Decimal Proposed
		{
			get { return _rebatePaymentSummary.Sum(rps => rps.Proposed.Total); }
		}

		public Decimal Pending
		{
			get { return _rebatePaymentSummary.Sum(rps => rps.Pending.Total); }
		}

		public Decimal Paid
		{
			get { return _rebatePaymentSummary.Sum(rps => rps.Paid.Total); }
		}

		public Decimal Closing
		{
			get { return _rebatePaymentSummary.Sum(rps => rps.Close.Total); }
		}

		public Decimal Closed
		{
			get { return _rebatePaymentSummary.Sum(rps => rps.Closed.Total); }
		}

		public Decimal CarriedForward
		{
			get { return this.Accrued - (this.Proposed + this.Pending + this.Paid + this.Closed); }
		}

		public Decimal Balance
		{
			get { return this.Accrued - (this.Proposed + this.Pending + this.Paid + this.Closing + this.Closed); }
		}

		public PaymentSummary (ExtendedBindingList<Rebate> rebates, Boolean showAll, User user, Division division, CalculationPeriod currentPeriod, CalculationPeriod payThruPeriod)
		{
			_rebates = rebates;
			_showAll = showAll;
			_user = user;
			_division = division;
			_currentPeriod = currentPeriod;
			_payThruPeriod = payThruPeriod;

			CreateSummary();
		}

		private void CreateSummary()
		{
			_rebatePaymentSummary = new List<RebatePaymentSummary>();
			_paymentBreakdowns = new ExtendedBindingList<PaymentBreakdown>();
			_paymentBreakdowns.ListChanged += new ListChangedEventHandler(_paymentBreakdowns_ListChanged);

			if (_rebates != null && _rebates.Count(r => r.IsSelected) > 0)
			{
				// Setup accrual read
				Accrual accruals = new Accrual();

				accruals.DivisionId = _division.Id;
				accruals.ActualYear = _payThruPeriod.Year;
				accruals.ActualPeriod = _payThruPeriod.Period;
				accruals.IsDraft = false;
				accruals.AccrualTypeEnum = AccrualTypeEnum.Actual;

				accruals.AddReadFilter(AccrualProperty.DivisionId, "=");
				accruals.AddReadFilter(AccrualProperty.ActualYear, "=");
				accruals.AddReadFilter(AccrualProperty.ActualPeriod, "<=");
				accruals.AddReadFilter(AccrualProperty.IsDraft, "=");
				accruals.AddReadFilter(AccrualProperty.AccrualTypeEnum, "=");

				_rebateInclude = "in ( ";

				foreach (Rebate rebate in _rebates.Where(r => r.IsSelected && (r.HasPaymentOutstanding || _showAll)))
				{
					_rebateInclude += rebate.Id.ToString() + ", ";
				}

				_rebateInclude = _rebateInclude.Substring(0, _rebateInclude.Length - 2) + " )";
				accruals.AddReadFilter(AccrualColumn.RebateId + " " + _rebateInclude);

				accruals.OrderBy =
					AccrualColumn.RebateId + ", " +
					AccrualColumn.ActualYear + ", " +
					AccrualColumn.ActualPeriod + ", " +
					AccrualColumn.Id;

				// Process accruals
				Int32 lastRebateId = -1;

				foreach (Accrual accrual in accruals.Read())
				{
					if (accrual.RebateId.HasValue)
					{
						RebatePaymentSummary rps;

						if (accrual.RebateId != lastRebateId)
						{
							rps = new RebatePaymentSummary(
									_payThruPeriod, 
									accrual.RebateId.Value, 
									accrual.RebateNumber, 
									accrual.RebateVersion, 
									accrual.RebateDescription, 
									accrual.PayeeId, 
									accrual.HierarchyLevel,
									accrual.HierarchyName, 
									(accrual.CustomerGroupCode.Length > 0 ? accrual.CustomerGroupCode : accrual.CustomerCode),
									(accrual.CustomerGroupCode.Length > 0 ? accrual.CustomerGroupName : accrual.CustomerName));

							rps.TotalValueChanged += new EventHandler(rps_TotalValueChanged);
							_rebatePaymentSummary.Add(rps);
						}
						else
						{
							rps = _rebatePaymentSummary[_rebatePaymentSummary.Count - 1];
						}

						rps.RebateId = accrual.RebateId.Value;

						if (rps.Accrual[accrual.ActualPeriod].HasValue)
						{
							rps.Accrual[accrual.ActualPeriod] += accrual.Value;
						}
						else
						{
							rps.Accrual[accrual.ActualPeriod] = accrual.Value;
						}

						lastRebateId = accrual.RebateId.Value;
					}
				}

				ProcessRebatePayments();
				SortRebatePayments();
			}
		}

		void _paymentBreakdowns_ListChanged(object sender, ListChangedEventArgs e)
		{
			NotifyPropertyChanged("AssignedPaymentAmount");
			NotifyPropertyChanged("UnAssignedPaymentAmount");
		}

		private void ResetRebatePayments()
		{
			// Zero all rebate payment summary payment details
			foreach (RebatePaymentSummary rps in _rebatePaymentSummary)
			{
				for (Int32 period = 1; period <= 12; period++)
				{
					rps.Pending[period] = null;
					rps.Paid[period] = null;
				}
			}
		}

		public void ProcessRebatePayments()
		{
			ResetRebatePayments();

			// Process payments
			Payment payments = new Payment();

			payments.DivisionId = _division.Id;
			payments.ActualYear = _payThruPeriod.Year;
			payments.AddReadFilter(PaymentProperty.DivisionId, "=");
			payments.AddReadFilter(PaymentProperty.ActualYear, "=");
			payments.AddReadFilter(PaymentProperty.RebateId + " " + _rebateInclude);

			payments.OrderBy = 
				PaymentColumn.RebateId + ", " +
				PaymentColumn.ActualYear + ", " + 
				PaymentColumn.ActualPeriod + ", " +
				PaymentColumn.Id;

			foreach (Payment payment in payments.Read())
			{
				RebatePaymentSummary rps = _rebatePaymentSummary.FirstOrDefault(rebatePaymentSummary => rebatePaymentSummary.RebateId == payment.RebateId);

				if (rps == null)
				{
					throw new DataException("Payment exists for Rebate with no Accruals (RebateId: " + payment.RebateId.ToString() + ")");
				}

				if (payment.PaymentTypeEnum == PaymentTypeEnum.Payment)
				{
					if (payment.PaymentStatusEnum == PaymentStatusEnum.PendingAuthorisation)
					{
						if (rps.Pending[payment.ActualPeriod].HasValue)
						{
							rps.Pending[payment.ActualPeriod] += payment.Value;
						}
						else
						{
							rps.Pending[payment.ActualPeriod] = payment.Value;
						}
					} 
					else if (payment.PaymentStatusEnum == PaymentStatusEnum.Authorised)
					{
						if (rps.Paid[payment.ActualPeriod].HasValue)
						{
							rps.Paid[payment.ActualPeriod] += payment.Value;
						}
						else
						{
							rps.Paid[payment.ActualPeriod] = payment.Value;
						}
					}
				}
				else if (payment.PaymentTypeEnum == PaymentTypeEnum.Close)
				{
					if (rps.Closed[payment.ActualPeriod].HasValue)
					{
						rps.Closed[payment.ActualPeriod] += payment.Value;
					}
					else
					{
						rps.Closed[payment.ActualPeriod] = payment.Value;
					}
				}
				else
				{
					throw new DataException("Payment exists with invalid PaymentTypeEnum (Payment Id: " + payment.Id.ToString() + ")");
				}
			}
		}

		private void SortRebatePayments()
		{
			_rebatePaymentSummary = _rebatePaymentSummary
				.OrderBy(rps => rps.HierarchyLevel)
				.ThenBy(rps => rps.HierarchyCode)
				.ThenBy(rps => rps.RebateNumber)
				.ToList();
		}

		void rps_TotalValueChanged(object sender, EventArgs e)
		{
			NotifyPropertyChanged("Proposed");
			NotifyPropertyChanged("Closing");
		}

		#region Business Logic

		public void CreatePayments()
		{
			_saveDate = DateTime.Now;
			_payments = new List<Payment>();

			foreach (RebatePaymentSummary rps in _rebatePaymentSummary)
			{
				for (Int16 periodIndex = 1; periodIndex <= _payThruPeriod.Period; periodIndex++)
				{
					CalculationPeriod actualPeriod = new CalculationPeriod(_payThruPeriod.Year, periodIndex);

					// Payments
					if (rps.Proposed[periodIndex].HasValue && rps.Proposed[periodIndex].Value != 0)
					{
						Payment payment = CreatePayment(rps, actualPeriod, PaymentTypeEnum.Payment, rps.Proposed[periodIndex].Value);
						_payments.Add(payment);
					}

					// Closures
					if (rps.Close[periodIndex].HasValue && rps.Close[periodIndex].Value != 0)
					{
						Payment payment = CreatePayment(rps, actualPeriod, PaymentTypeEnum.Close, rps.Close[periodIndex].Value);
						_payments.Add(payment);
					}
				}
			}
		}

		private Payment CreatePayment(RebatePaymentSummary rebatePaymentSummary, CalculationPeriod actualPeriod, PaymentTypeEnum paymentTypeEnum, Decimal value)
		{
			Payment payment = new Payment();

			payment.RebateId = rebatePaymentSummary.RebateId;
			payment.DivisionId = _division.Id;
			payment.CreateYear = (Int16) _now.Year;
			payment.CreatePeriod = (Int16) _now.Month;
			payment.ActualYear = actualPeriod.Year;
			payment.ActualPeriod = actualPeriod.Period;
			payment.Value = value;
			payment.PaymentTypeEnum = paymentTypeEnum;

			return payment;
		}

		public void CreateDefaultPaymentBreakdowns()
		{
			_paymentBreakdowns.Clear();

			foreach (RebatePaymentSummary rps in _rebatePaymentSummary)
			{
				if (rps.Proposed.Total != 0 && rps.PayeeId.HasValue)
				{
					PaymentBreakdown pb = _paymentBreakdowns.FirstOrDefault(paymentBreakdown => paymentBreakdown.PayeeId == rps.PayeeId.Value);

					if (pb == null)
					{
						// Read tax codes and default to zero rate (if it exists)
						TaxCode taxCode = new TaxCode();
						ExtendedBindingList<TaxCode> taxCodes = (ExtendedBindingList<TaxCode>) taxCode.Read();
						
						if (taxCodes == null || taxCodes.Count == 0)
						{
							throw new ArgumentOutOfRangeException("No tax codes have been defined in the Rebates System");
						}

						TaxCode taxCodeForPayment = taxCodes.FirstOrDefault(tc => tc.IsZeroRate);

						if (taxCodeForPayment == null)
						{
							taxCodeForPayment = taxCodes[0];
						}

						pb = new PaymentBreakdown();

						Payee payee = new Payee();
						payee.Id = rps.PayeeId.Value;
						payee.ReadCurrent();

						pb.PayeeId = rps.PayeeId.Value;
						pb.PayeeName = payee.Name;
						pb.Value = rps.Proposed.Total;
						pb.TaxCodeId = taxCodeForPayment.Id;
						pb.TaxCode = taxCodeForPayment.Code;
						pb.TaxDate = DateTime.Today;

						_paymentBreakdowns.Add(pb);
					}
					else
					{
						pb.Value += rps.Proposed.Total;
					}
				}
			}
		}

		public void SendProposedPaymentsForAuthorisation()
		{
			foreach (RebatePaymentSummary rps in this.List)
			{
				rps.SendProposedPaymentsForAuthorisation();
			}

			_payments = null;
			_paymentBreakdowns.Clear();
		}

		#endregion

		#region PropertyChanged

		public event PropertyChangedEventHandler PropertyChanged;

		public void NotifyPropertyChanged(String propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		#endregion
	}

	/// <summary>
	/// Rebate Payment Summary
	/// 
	/// Used to process and calculate payments and write offs (closures) against a specific rebate
	/// </summary>
	public class RebatePaymentSummary : INotifyPropertyChanged
	{
		#region Properties

		#region PROPERTY: PayThruPeriod

		private CalculationPeriod _payThruPeriod;

		public CalculationPeriod PayThruPeriod
		{
			get { return _payThruPeriod; }
			set { _payThruPeriod = value; }
		}

		#endregion

		#region PROPERTY: RebateId

		private Int32 _rebateId;

		public Int32 RebateId
		{
			get { return _rebateId; }
			set { _rebateId = value; }
		}

		#endregion

		#region PROPERTY: PayeeId

		private Int32? _payeeId;

		public Int32? PayeeId
		{
			get { return _payeeId; }
			set { _payeeId = value; }
		}

		#endregion

		#region PROPERTY: RebateNumber

		private Int32 _rebateNumber;

		public Int32 RebateNumber
		{
			get { return _rebateNumber; }
			set { _rebateNumber = value; }
		}

		#endregion

		#region PROPERTY: RebateVersion

		private Int16 _rebateVersion;

		public Int16 RebateVersion
		{
			get { return _rebateVersion; }
			set { _rebateVersion = value; }
		}

		#endregion

		#region PROPERTY: RebateDescription

		private String _rebateDescription;

		public String RebateDescription
		{
			get { return _rebateDescription; }
			set { _rebateDescription = value; }
		}

		#endregion

		#region PROPERTY: HierarchyLevel

		private Int16 _hierarchyLevel;

		public Int16 HierarchyLevel
		{
			get { return _hierarchyLevel; }
			set { _hierarchyLevel = value; }
		}

		#endregion

		#region PROPERTY: HierarchyName
		private String _hierarchyName;
		public String HierarchyName
		{
			get { return _hierarchyName; }
			set
			{
				_hierarchyName = value;
			}
		}
		#endregion

		#region PROPERTY: HierarchyCode

		private String _hierarchyCode;

		public String HierarchyCode
		{
			get { return _hierarchyCode; }
			set { _hierarchyCode = value; }
		}

		#endregion

		#region PROPERTY: HierarchyCodeName

		private String _hierarchyCodeName;

		public String HierarchyCodeName
		{
			get { return _hierarchyCodeName; }
			set { _hierarchyCodeName = value; }
		}

		#endregion

		#region PROPERTY: Accrual

		private PeriodTotal _accrual;

		public PeriodTotal Accrual
		{
			get { return _accrual; }
			set { _accrual = value; }
		}

		public Decimal? Accrual1 { get { return _accrual[1]; } }
		public Decimal? Accrual2 { get { return _accrual[2]; } }
		public Decimal? Accrual3 { get { return _accrual[3]; } }
		public Decimal? Accrual4 { get { return _accrual[4]; } }
		public Decimal? Accrual5 { get { return _accrual[5]; } }
		public Decimal? Accrual6 { get { return _accrual[6]; } }
		public Decimal? Accrual7 { get { return _accrual[7]; } }
		public Decimal? Accrual8 { get { return _accrual[8]; } }
		public Decimal? Accrual9 { get { return _accrual[9]; } }
		public Decimal? Accrual10 { get { return _accrual[10]; } }
		public Decimal? Accrual11 { get { return _accrual[11]; } }
		public Decimal? Accrual12 { get { return _accrual[12]; } }
		public Decimal? AccrualTotal { get { return _accrual.Total; } }

		#endregion

		#region PROPERTY: Propsed Payment

		private PeriodTotal _proposed;

		public PeriodTotal Proposed
		{
			get { return _proposed; }
			set { _proposed = value; }
		}

		public Decimal? Proposed1 { get { return _proposed[1]; } set { _proposed[1] = value; } }
		public Decimal? Proposed2 { get { return _proposed[2]; } set { _proposed[2] = value; } }
		public Decimal? Proposed3 { get { return _proposed[3]; } set { _proposed[3] = value; } }
		public Decimal? Proposed4 { get { return _proposed[4]; } set { _proposed[4] = value; } }
		public Decimal? Proposed5 { get { return _proposed[5]; } set { _proposed[5] = value; } }
		public Decimal? Proposed6 { get { return _proposed[6]; } set { _proposed[6] = value; } }
		public Decimal? Proposed7 { get { return _proposed[7]; } set { _proposed[7] = value; } }
		public Decimal? Proposed8 { get { return _proposed[8]; } set { _proposed[8] = value; } }
		public Decimal? Proposed9 { get { return _proposed[9]; } set { _proposed[9] = value; } }
		public Decimal? Proposed10 { get { return _proposed[10]; } set { _proposed[10] = value; } }
		public Decimal? Proposed11 { get { return _proposed[11]; } set { _proposed[11] = value; } }
		public Decimal? Proposed12 { get { return _proposed[12]; } set { _proposed[12] = value; } }
		public Decimal? ProposedTotal { get { return _proposed.Total; } }

		#endregion

		#region PROPERTY: Pending Authorisation

		private PeriodTotal _pending;

		public PeriodTotal Pending
		{
			get { return _pending; }
			set { _pending = value; }
		}

		public Decimal? Pending1 { get { return _pending[1]; } set { _pending[1] = value; } }
		public Decimal? Pending2 { get { return _pending[2]; } set { _pending[2] = value; } }
		public Decimal? Pending3 { get { return _pending[3]; } set { _pending[3] = value; } }
		public Decimal? Pending4 { get { return _pending[4]; } set { _pending[4] = value; } }
		public Decimal? Pending5 { get { return _pending[5]; } set { _pending[5] = value; } }
		public Decimal? Pending6 { get { return _pending[6]; } set { _pending[6] = value; } }
		public Decimal? Pending7 { get { return _pending[7]; } set { _pending[7] = value; } }
		public Decimal? Pending8 { get { return _pending[8]; } set { _pending[8] = value; } }
		public Decimal? Pending9 { get { return _pending[9]; } set { _pending[9] = value; } }
		public Decimal? Pending10 { get { return _pending[10]; } set { _pending[10] = value; } }
		public Decimal? Pending11 { get { return _pending[11]; } set { _pending[11] = value; } }
		public Decimal? Pending12 { get { return _pending[12]; } set { _pending[12] = value; } }
		public Decimal? PendingTotal { get { return _pending.Total; } }

		#endregion

		#region PROPERTY: Paid

		private PeriodTotal _paid;

		public PeriodTotal Paid
		{
			get { return _paid; }
			set { _paid = value; }
		}

		public Decimal? Paid1 { get { return _paid[1]; } set { _paid[1] = value; } }
		public Decimal? Paid2 { get { return _paid[2]; } set { _paid[2] = value; } }
		public Decimal? Paid3 { get { return _paid[3]; } set { _paid[3] = value; } }
		public Decimal? Paid4 { get { return _paid[4]; } set { _paid[4] = value; } }
		public Decimal? Paid5 { get { return _paid[5]; } set { _paid[5] = value; } }
		public Decimal? Paid6 { get { return _paid[6]; } set { _paid[6] = value; } }
		public Decimal? Paid7 { get { return _paid[7]; } set { _paid[7] = value; } }
		public Decimal? Paid8 { get { return _paid[8]; } set { _paid[8] = value; } }
		public Decimal? Paid9 { get { return _paid[9]; } set { _paid[9] = value; } }
		public Decimal? Paid10 { get { return _paid[10]; } set { _paid[10] = value; } }
		public Decimal? Paid11 { get { return _paid[11]; } set { _paid[11] = value; } }
		public Decimal? Paid12 { get { return _paid[12]; } set { _paid[12] = value; } }
		public Decimal? PaidTotal { get { return _paid.Total; } }

		#endregion
	
		#region PROPERTY: Proposed Closure

		private PeriodTotal _close;

		public PeriodTotal Close
		{
			get { return _close; }
			set { _close = value; }
		}

		public Decimal? Close1 { get { return _close[1]; } set { _close[1] = value; } }
		public Decimal? Close2 { get { return _close[2]; } set { _close[2] = value; } }
		public Decimal? Close3 { get { return _close[3]; } set { _close[3] = value; } }
		public Decimal? Close4 { get { return _close[4]; } set { _close[4] = value; } }
		public Decimal? Close5 { get { return _close[5]; } set { _close[5] = value; } }
		public Decimal? Close6 { get { return _close[6]; } set { _close[6] = value; } }
		public Decimal? Close7 { get { return _close[7]; } set { _close[7] = value; } }
		public Decimal? Close8 { get { return _close[8]; } set { _close[8] = value; } }
		public Decimal? Close9 { get { return _close[9]; } set { _close[9] = value; } }
		public Decimal? Close10 { get { return _close[10]; } set { _close[10] = value; } }
		public Decimal? Close11 { get { return _close[11]; } set { _close[11] = value; } }
		public Decimal? Close12 { get { return _close[12]; } set { _close[12] = value; } }
		public Decimal? CloseTotal { get { return _close.Total; } }

		#endregion

		#region PROPERTY: Closed

		private PeriodTotal _closed;

		public PeriodTotal Closed
		{
			get { return _closed; }
			set { _closed = value; }
		}

		public Decimal? Closed1 { get { return _closed[1]; } set { _closed[1] = value; } }
		public Decimal? Closed2 { get { return _closed[2]; } set { _closed[2] = value; } }
		public Decimal? Closed3 { get { return _closed[3]; } set { _closed[3] = value; } }
		public Decimal? Closed4 { get { return _closed[4]; } set { _closed[4] = value; } }
		public Decimal? Closed5 { get { return _closed[5]; } set { _closed[5] = value; } }
		public Decimal? Closed6 { get { return _closed[6]; } set { _closed[6] = value; } }
		public Decimal? Closed7 { get { return _closed[7]; } set { _closed[7] = value; } }
		public Decimal? Closed8 { get { return _closed[8]; } set { _closed[8] = value; } }
		public Decimal? Closed9 { get { return _closed[9]; } set { _closed[9] = value; } }
		public Decimal? Closed10 { get { return _closed[10]; } set { _closed[10] = value; } }
		public Decimal? Closed11 { get { return _closed[11]; } set { _closed[11] = value; } }
		public Decimal? Closed12 { get { return _closed[12]; } set { _closed[12] = value; } }
		public Decimal? ClosedTotal { get { return _closed.Total; } }

		#endregion

		#region PROPERTY: CarriedForward

		private PeriodTotal _carriedForward;

		public PeriodTotal CarriedForward
		{
			get { return _carriedForward; }
		}

		public Decimal? CarriedForward1 { get { return _carriedForward[1]; } }
		public Decimal? CarriedForward2 { get { return _carriedForward[2]; } }
		public Decimal? CarriedForward3 { get { return _carriedForward[3]; } }
		public Decimal? CarriedForward4 { get { return _carriedForward[4]; } }
		public Decimal? CarriedForward5 { get { return _carriedForward[5]; } }
		public Decimal? CarriedForward6 { get { return _carriedForward[6]; } }
		public Decimal? CarriedForward7 { get { return _carriedForward[7]; } }
		public Decimal? CarriedForward8 { get { return _carriedForward[8]; } }
		public Decimal? CarriedForward9 { get { return _carriedForward[9]; } }
		public Decimal? CarriedForward10 { get { return _carriedForward[10]; } }
		public Decimal? CarriedForward11 { get { return _carriedForward[11]; } }
		public Decimal? CarriedForward12 { get { return _carriedForward[12]; } }
		public Decimal? CarriedForwardTotal { get { return _carriedForward.Total; } }

		#endregion

		#region PROPERTY: Balance

		private PeriodTotal _balance;

		public PeriodTotal Balance
		{
			get { return _balance; }
		}

		public Decimal? Balance1 { get { return _balance[1]; } }
		public Decimal? Balance2 { get { return _balance[2]; } }
		public Decimal? Balance3 { get { return _balance[3]; } }
		public Decimal? Balance4 { get { return _balance[4]; } }
		public Decimal? Balance5 { get { return _balance[5]; } }
		public Decimal? Balance6 { get { return _balance[6]; } }
		public Decimal? Balance7 { get { return _balance[7]; } }
		public Decimal? Balance8 { get { return _balance[8]; } }
		public Decimal? Balance9 { get { return _balance[9]; } }
		public Decimal? Balance10 { get { return _balance[10]; } }
		public Decimal? Balance11 { get { return _balance[11]; } }
		public Decimal? Balance12 { get { return _balance[12]; } }
		public Decimal? BalanceTotal { get { return _balance.Total; } }

		#endregion

		#endregion

		#region Constructors

		public RebatePaymentSummary(CalculationPeriod payThruPeriod, Int32 rebateId, Int32 rebateNumber, Int16 rebateVersion, String rebateDescription, Int32? payeeId, Int16 hierarchyLevel, String hierarchyName, String hierarchyCode, String hierarchyCodeName)
		{
			_payThruPeriod = payThruPeriod;
			_rebateId = rebateId;
			_rebateNumber = rebateNumber;
			_rebateVersion = rebateVersion;
			_rebateDescription = rebateDescription;
			_payeeId = payeeId;
			_hierarchyLevel = hierarchyLevel;
			_hierarchyName = hierarchyName;
			_hierarchyCode = hierarchyCode;
			_hierarchyCodeName = hierarchyCodeName;

			_accrual = new PeriodTotal();
			_proposed = new PeriodTotal();
			_pending = new PeriodTotal();
			_paid = new PeriodTotal();
			_close = new PeriodTotal();
			_closed = new PeriodTotal();
			_carriedForward = new PeriodTotal();
			_balance = new PeriodTotal();

			this.Accrual.PeriodTotalValueChanged += new PeriodTotalValueChangedEventHandler(PeriodTotalValueChanged);
			this.Pending.PeriodTotalValueChanged += new PeriodTotalValueChangedEventHandler(PeriodTotalValueChanged);
			this.Proposed.PeriodTotalValueChanged += new PeriodTotalValueChangedEventHandler(PeriodTotalValueChanged);
			this.Paid.PeriodTotalValueChanged += new PeriodTotalValueChangedEventHandler(PeriodTotalValueChanged);
			this.Close.PeriodTotalValueChanged += new PeriodTotalValueChangedEventHandler(PeriodTotalValueChanged);
			this.Closed.PeriodTotalValueChanged += new PeriodTotalValueChangedEventHandler(PeriodTotalValueChanged);
		}

		#endregion

		#region Events and Handlers

		#region PeriodTotalValueChanged

		public void PeriodTotalValueChanged(object sender, PeriodTotalEventArgs e)
		{
			// Calculate balance
			if (e.Period >= 1 && e.Period <= 12)
			{
				if (_accrual[e.Period].HasValue)
				{
					_carriedForward[e.Period] = _accrual[e.Period].Value - (_paid[e.Period].GetValueOrDefault() + _closed[e.Period].GetValueOrDefault());
					_balance[e.Period] = _accrual[e.Period].Value - (_proposed[e.Period].GetValueOrDefault() + _pending[e.Period].GetValueOrDefault() + _paid[e.Period].GetValueOrDefault() + _close[e.Period].GetValueOrDefault() + _closed[e.Period].GetValueOrDefault());
				}
				else
				{
					_balance[e.Period] = null;
				}

				NotifyPropertyChanged("Proposed" + e.Period.ToString());
				NotifyPropertyChanged("Close" + e.Period.ToString());
				NotifyPropertyChanged("Balance" + e.Period.ToString());
			}

			// Notify the fact that a total value has changed
			NotifyTotalValueChanged();
		}

		#endregion

		#region PropertyChanged

		public event PropertyChangedEventHandler PropertyChanged;

		public void NotifyPropertyChanged(String propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		#endregion

		#region OnTotalValueChanged Event

		public event EventHandler TotalValueChanged;

		protected void NotifyTotalValueChanged()
		{
			if (TotalValueChanged != null)
			{
				TotalValueChanged(this, new EventArgs());
			}
		}

		#endregion

		#endregion

		#region Business Logic

		public Decimal MakePaymentOrClosure(Decimal value, PaymentTypeEnum paymentType)
		{
			if (paymentType == PaymentTypeEnum.Payment)
			{
				this.Proposed.Clear();

				if ((value > this.Proposed.Total) && ((value - this.Proposed.Total) > this.Balance.Total))
				{
					Decimal reduceValue = (value - this.Proposed.Total) - this.Balance.Total;
					reduceValue = (reduceValue > this.Close.Total ? this.Close.Total : reduceValue);

					Decimal newValue = this.Close.Total - reduceValue;
					ApportionPaymentOrClosure(newValue, PaymentTypeEnum.Close);
				}
			}
			else
			{
				this.Close.Clear();
			}

			return ApportionPaymentOrClosure(value, paymentType);
		}

		private Decimal ApportionPaymentOrClosure(Decimal paymentValue, PaymentTypeEnum paymentType)
		{
			for (Int32 period = 1; period <= _payThruPeriod.Period; period++)
			{
				// Get carried forward balance
				Decimal balance = this.Balance[period].GetValueOrDefault();

				// Apply any payments
				if (balance > 0)
				{
					Decimal periodPayment;

					if (balance >= paymentValue)
					{
						periodPayment = paymentValue;
					}
					else
					{
						periodPayment = balance;
					}

					paymentValue -= periodPayment;

					// Now assign to the RebatePaymentSummary
					Decimal? assignPayment = (periodPayment == 0 ? null : (Decimal?) periodPayment);

					if (paymentType == PaymentTypeEnum.Payment)
					{
						this.Proposed[period] = assignPayment;
					}
					else
					{
						this.Close[period] = assignPayment;
					}

					NotifyPropertyChanged(paymentType.ToString() + period.ToString());
				}
			}

			// If the payment type is PaymentType, then allocate any overpayments
			// to the last possible payment period
			if (paymentType == PaymentTypeEnum.Payment && paymentValue > 0)
			{
				for (Int32 period = _payThruPeriod.Period; period >= 1; period--)
				{
					if (this.Accrual[period].HasValue)
					{
						Decimal payment = this.Proposed[period].GetValueOrDefault();
						payment += paymentValue;
						this.Proposed[period] = payment;
						break;
					}
				}
			}

			return paymentValue;
		}

		public void SendProposedPaymentsForAuthorisation()
		{
			for (Int16 period = 1; period <= 12; period++)
			{
				// Proposed Payment -> Awaiting Authorisation
				if (this.Proposed[period].HasValue)
				{
					if (this.Pending[period].HasValue)
					{
						this.Pending[period] += this.Proposed[period];
					}
					else
					{
						this.Pending[period] = this.Proposed[period];
					}

					this.Proposed[period] = null;
				}

				// Proposed Closure -> Closed
				if (this.Close[period].HasValue)
				{
					if (this.Closed[period].HasValue)
					{
						this.Closed[period] += this.Close[period];
					}
					else
					{
						this.Closed[period] = this.Close[period];
					}

					this.Close[period] = null;
				}
			}
		}

		#endregion
	}

	/// <summary>
	/// Period Total
	/// 
	/// Class which is used to store a value for each period (1 - 12), also provides a total, and
	/// allows for indexing via period number instead of array index
	/// </summary>
	public class PeriodTotal
	{
		private Decimal?[] _total = new Decimal?[12];

		public Decimal? this[int period]
		{
			get
			{
				if (period >= 1 && period <= 12)
				{
					return _total[period - 1];
				}
				else
				{
					return null;
				}
			}
			set
			{
				if (period >= 1 && period <= 12)
				{
					_total[period - 1] = value;
					OnPeriodTotalValueChanged(new PeriodTotalEventArgs(period));
				}
			}
		}

		public Decimal Total
		{
			get
			{
				return _total.Sum(d => d.GetValueOrDefault());
			}
		}

		#region OnValueChanged Event

		public event PeriodTotalValueChangedEventHandler PeriodTotalValueChanged;

		protected void OnPeriodTotalValueChanged(PeriodTotalEventArgs e)
		{
			if (PeriodTotalValueChanged != null)
			{
				PeriodTotalValueChanged(this, e);
			}
		}

		#endregion

		#region Business Logic

		public void Clear()
		{
			for (Int32 index = 0; index < _total.Length; index++)
			{
				this[index] = null;
			}
		}

		#endregion
	}

	#region Event Delegates and Custom EventArgs

	public delegate void PeriodTotalValueChangedEventHandler(object sender, PeriodTotalEventArgs e);

	public class PeriodTotalEventArgs : EventArgs
	{
		private Int32 _payThruPeriod;

		public Int32 Period
		{
			get { return _payThruPeriod; }
			set { _payThruPeriod = value; }
		}

		public PeriodTotalEventArgs(Int32 period) : base()
		{
			_payThruPeriod = period;
		}
	}

	#endregion
}

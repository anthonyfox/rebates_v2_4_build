﻿using System;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public enum ExceptionTypeEnum
	{
		Null,
		SoldFrom,
		SoldTo,
		ShipTo
	}

	public static class ExceptionTypeEnumConvertor
	{
		public static ExceptionTypeEnum DBIntToExceptionTypeEnum(Object dbObject)
		{
			ExceptionTypeEnum exceptionTypeEnum = ExceptionTypeEnum.Null;

			if (!Object.ReferenceEquals(dbObject, System.DBNull.Value))
			{
				Int32 value = Convert.ToInt32(dbObject);
				exceptionTypeEnum = (ExceptionTypeEnum) value;
			}

			return exceptionTypeEnum;
		}

		public static Object ExceptionTypeEnumToDBInt(ExceptionTypeEnum exceptionTypeEnum)
		{
			Object value = (Int32) Enum.Parse(typeof(ExceptionTypeEnum), Enum.GetName(typeof(ExceptionTypeEnum), exceptionTypeEnum));
			return value;
		}
	}
}

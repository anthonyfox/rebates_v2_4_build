﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;
using System.ComponentModel;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class RebateLastAmended : SqlServerEntity
	{
		#region Properties

		#region PROPERTY: CreateTime
		private DateTime? _createTime;
		[EntityColumn(RebateColumn.CreateTime, DBDateTime.Year, DBDateTime.Second)]
		public DateTime? CreateTime
		{
			get { return _createTime; }
			set	{ _createTime = value; }
		}
		#endregion

		#region PROPERTY: UpdateTime
		[EntityColumn(RebateColumn.UpdateTime, DBDateTime.Year, DBDateTime.Second)]
		private DateTime? _updateTime;
		public DateTime? UpdateTime
		{
			get { return _updateTime; }
			set	{ _updateTime = value; }
		}
		#endregion

		#region DERIVED PROPERTY: StartDate
		private DateTime _startDate;

		[EntityColumn(RebateColumn.StartDate, DBRebates.Rebate, RebateProperty.StartDate), Browsable(false)]
		public DateTime StartDate
		{
			get { return _startDate; }
			set { _startDate = value; }
		}
		#endregion

		#region DERIVED PROPERTY: LastAmended
		public DateTime? LastAmended
		{
			get 
			{
				if (!this.CreateTime.HasValue)
				{
					return null;
				}
				else if (!this.UpdateTime.HasValue)
				{
					return this.CreateTime;
				}
				else
				{
					if (this.UpdateTime > this.CreateTime)
					{
						return this.UpdateTime;
					}
					else
					{
						return this.CreateTime;
					}
				}
			}
		}
		#endregion

		#endregion

		#region Constructors

		public RebateLastAmended()
		{
		}

		public RebateLastAmended(CalculationPeriod period)
		{
			InitializeComponent(period);
			ExtendedBindingList<RebateLastAmended> lastAmendeds;
			lastAmendeds = (ExtendedBindingList<RebateLastAmended>) this.Read();

			if (lastAmendeds != null && lastAmendeds.Count > 0)
			{
				this.CreateTime = lastAmendeds[0].CreateTime;
				this.UpdateTime = lastAmendeds[0].UpdateTime;
			}
		}

		private void InitializeComponent(CalculationPeriod period)
		{
			// Get max date for rebates which may be affected by this accrual period
			if (period.Period == 12)
			{
				_startDate = new DateTime(period.Year, 12, 31);
			}
			else
			{
				CalculationPeriod nextPeriod = period.NextPeriod();
				_startDate = new DateTime(nextPeriod.Year, nextPeriod.Period, 1).AddDays(-1);
			}

			// Define custom select
			CustomSelect =
				"select " +
					"max(" + RebateColumn.CreateTime + ") as CreateTime, " +
					"max(" + RebateColumn.UpdateTime + ") as UpdateTime " +
				"from " +
					this.DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.Rebate) + " " +
				"where " +
					RebateColumn.Version + " = " + period.Year.ToString() + " " +
				"and " +
					RebateColumn.StartDate + " <= " + this.DBDateText(_startDate);
		}

		#endregion
	}
}


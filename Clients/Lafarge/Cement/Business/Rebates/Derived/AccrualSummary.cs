﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class AccrualSummary : SqlServerEntity
	{
		#region Properties

		#region PROPERTY: ActualPeriod
		private CalculationPeriod _actualPeriod;
		public CalculationPeriod ActualPeriod
		{
			get { return _actualPeriod; }
			set	{_actualPeriod = value; }
		}
		#endregion

		#region PROPERTY: HasDraft
		private Boolean _hasDraft;
		[Browsable(false)]
		public Boolean HasDraft
		{
			get { return _hasDraft; }
			set { _hasDraft = value; }
		}
		#endregion
		
		#region DERIVED PROPERTY: HasDraftText
		
		[DisplayName("Includes Draft Accruals")]
		public String HasDraftText
		{
		    get { return (_hasDraft ? "Yes" : "No"); }
		}

		#endregion

		#region PROPERTY: Value
		private Decimal _value;
		public Decimal Value
		{
			get { return _value; }
			set { _value = value; }
		}
		#endregion

		#region PROPERTY: SalesVolume
		private Decimal _salesVolume;
		[DisplayName("Sales Volume")]
		public Decimal SalesVolume
		{
			get { return _salesVolume; }
			set { _salesVolume = value; }
		}
		#endregion

		#region PROPERTY: SalesValue
		private Decimal _salesValue;
		[DisplayName("Sales Value")]
		public Decimal SalesValue
		{
			get { return _salesValue; }
			set { _salesValue = value; }
		}
		#endregion

		#region PROPERTY: DivisionId
		private Int32 _divisionId;
		[Browsable(false)]
		public Int32 DivisionId
		{
			get { return _divisionId; }
			set { _divisionId = value; }
		}
		#endregion

		#region PROPERTY: YearToDateSalesVolume

		private Decimal _yearToDateSalesVolume;

		[DisplayName("YTD Sales Volume")]
		public Decimal YearToDateSalesVolume
		{
			get { return _yearToDateSalesVolume; }
			set { _yearToDateSalesVolume = value; }
		}

		#endregion

		#region PROPERTY: YearToDateSalesValue

		private Decimal _yearToDateSalesValue;

		[DisplayName("YTD Sales Value")]
		public Decimal YearToDateSalesValue
		{
			get { return _yearToDateSalesValue; }
			set { _yearToDateSalesValue = value; }
		}

		#endregion

		#endregion

		#region Constructors

		public AccrualSummary()
		{
		}

		#endregion

		#region Business Logic

		public ExtendedBindingList<AccrualSummary> CreateSummary(AccrualTypeEnum accrualTypeEnum, CalculationPeriod calculationPeriod, Int32? rebateId, Boolean includeDraft)
		{
			ExtendedBindingList<AccrualSummary> accrualSummaries = new ExtendedBindingList<AccrualSummary>();

			// Load relevant accruals
			Accrual accrual = new Accrual();

			accrual.DivisionId = this.DivisionId;
			accrual.AccrualTypeEnum = accrualTypeEnum;

			accrual.AddReadFilter(AccrualProperty.DivisionId, "=");
			accrual.AddReadFilter(AccrualProperty.AccrualTypeEnum, "=");

			if (calculationPeriod != null)
			{
				accrual.ActualYear = calculationPeriod.Year;
				accrual.ActualPeriod = calculationPeriod.Period;

				accrual.AddReadFilter(AccrualProperty.ActualYear, "=");
				accrual.AddReadFilter(AccrualProperty.ActualPeriod, "<=");
			}

			if (rebateId.HasValue)
			{
				accrual.RebateId = rebateId;
				accrual.AddReadFilter(AccrualProperty.RebateId, "=");
			}

			if (!includeDraft)
			{
				accrual.IsDraft = includeDraft;
				accrual.AddReadFilter(AccrualProperty.IsDraft, "=");
			}

			// Make sure some data which exists erroneously is ignored
			accrual.ReadFilter += " and DivisionAccrual.Year = " + calculationPeriod.Year;

			// Make the rows display in the correct order
			accrual.OrderBy = AccrualColumn.ActualYear + ", " + AccrualColumn.ActualPeriod + ", " + AccrualColumn.RebateId + ", " + AccrualColumn.Id;

			// Process and construct accrual summary list
			Int32 previousPeriod = 0;

			foreach (Accrual processAccrual in accrual.Read())
			{
				AccrualSummary accrualSummary;

				if (processAccrual.ActualPeriod != previousPeriod)
				{
					accrualSummary = new AccrualSummary();

					accrualSummary.ActualPeriod = new CalculationPeriod(processAccrual.ActualYear, processAccrual.ActualPeriod);
					accrualSummary.HasDraft = processAccrual.IsDraft;
					accrualSummary.Value = processAccrual.Value;

					// Carry forward year to date values if not the first period
					Int32 count = accrualSummaries.Count;

					if (accrualSummaries.Count > 0)
					{
						accrualSummary.YearToDateSalesValue = accrualSummaries[count - 1].YearToDateSalesValue;
						accrualSummary.YearToDateSalesVolume = accrualSummaries[count - 1].YearToDateSalesVolume;
					}

					// Add period summary to the collection
					accrualSummaries.Add(accrualSummary);
				}
				else
				{
					accrualSummary = accrualSummaries[accrualSummaries.Count - 1];

					if (processAccrual.IsDraft)
					{
						accrualSummary.HasDraft = true;
					}

					accrualSummary.Value += processAccrual.Value;
				}

				if (processAccrual.SalesValue.HasValue)
				{
					accrualSummary.SalesValue += processAccrual.SalesValue.Value;
					accrualSummary.YearToDateSalesValue += processAccrual.SalesValue.Value;
				}

				if (processAccrual.SalesVolume.HasValue)
				{
					accrualSummary.SalesVolume += processAccrual.SalesVolume.Value;
					accrualSummary.YearToDateSalesVolume += processAccrual.SalesVolume.Value;
				}

				previousPeriod = processAccrual.ActualPeriod;
			}

			return accrualSummaries;
		}

		#endregion
	}


	public class AccrualSummaryProperty
	{
		public const string ActualPeriod = "Period";
		public const string Value = "Value";
		public const string SalesVolume = "SalesVolume";
		public const string SalesValue = "SalesValue";
		public const string HasDraft = "IsDraft";
		public const string HasDraftText = "IsDraftText";
		public const string DivisionId = "DivisionId";
	}
}

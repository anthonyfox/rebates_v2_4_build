﻿using System;
using System.Data;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using System.Linq;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class RebateTypeNormal : RebateTypeCalculator
	{
		public RebateTypeNormal(Rebate rebate)
			: base(rebate)
		{
		}

		public override void CalculateGlobalProduct()
		{
			if (this.Accrual.Rate.HasValue && this.Accrual.SalesVolume.HasValue)
			{
				this.Accrual.Value = Decimal.Round(this.Accrual.SalesVolume.Value * this.Accrual.Rate.Value, 3);
			}
			else
			{
				throw new DataException(this.GetType().Name + ": No Global Product Rate and / or Sales Volume");
			}
		}

		public override void CalculateDetail()
		{
			// Calculate the relevant rate
			Hierarchy baseHierarchy = this.Hierarchys[this.Hierarchys.Count - 1];

			// Now apply to the accrual breakdown
			foreach (AccrualDetail accrualDetail in this.Accrual.AccrualDetails)
			{
				if (accrualDetail.HierarchyId == baseHierarchy.Id)
				{
					if (accrualDetail.Rate.HasValue)
					{
						accrualDetail.Value = Decimal.Round(accrualDetail.SalesVolume * accrualDetail.Rate.Value, 3);
					}
					else
					{
						throw new DataException(this.GetType().Name + ": No product rate (RebateDetailId = " + accrualDetail.RebateDetailId.ToString() + ")");
					}

					AccrualDetail parentAccrualDetail = accrualDetail.FindParent(this.Accrual.AccrualDetails, this.Hierarchys);

					while (parentAccrualDetail != null)
					{
						parentAccrualDetail.Rate = null;
						parentAccrualDetail.Value += accrualDetail.Value;

						parentAccrualDetail = parentAccrualDetail.FindParent(this.Accrual.AccrualDetails, this.Hierarchys);
					}

					this.Accrual.Rate = null;
					this.Accrual.Value += accrualDetail.Value;
				}
			}
		}

		public override void CalculatePriceEnquiryRate()
		{
			foreach (RebateDetail rebateDetail in this.Rebate.RebateDetails)
			{
				rebateDetail.PriceEnquiryRate = rebateDetail.Rate;
			}
		}

		public override void ValidateData()
		{
			base.ValidateData();
		}
	}
}

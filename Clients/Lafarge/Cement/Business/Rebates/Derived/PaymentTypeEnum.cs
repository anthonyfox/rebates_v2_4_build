﻿using System;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public enum PaymentTypeEnum
	{
		Null,
		Payment,
		Close
	}

	public static class PaymentTypeEnumConvertor
	{
		public static PaymentTypeEnum DBInt16ToPaymentTypeEnum(Object dbObject)
		{
			PaymentTypeEnum paymentType = PaymentTypeEnum.Null;

			if (!Object.ReferenceEquals(dbObject, System.DBNull.Value))
			{
				Int16 value = Convert.ToInt16(dbObject);

				switch (value)
				{
					case 1:
						paymentType = PaymentTypeEnum.Payment;
						break;
					case 2:
						paymentType = PaymentTypeEnum.Close;
						break;
					default:
						paymentType = PaymentTypeEnum.Null;
						break;
				}
			}

			return paymentType;
		}

		public static Object PaymentTypeEnumToDBInt16(PaymentTypeEnum paymentType)
		{
			Object value = DBNull.Value;

			switch (paymentType)
			{
				case PaymentTypeEnum.Payment:
					value = 1;
					break;
				case PaymentTypeEnum.Close:
					value = 2;
					break;
			}

			return value;
		}
	}
}

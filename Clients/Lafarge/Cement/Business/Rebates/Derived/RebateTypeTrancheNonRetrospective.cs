﻿using System;
using System.Data;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using System.Linq;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class RebateTypeTrancheNonRetrospective : RebateTypeCalculator
	{
		public RebateTypeTrancheNonRetrospective(Rebate rebate)
			: base(rebate)
		{
		}

		public override void CalculateGlobalProduct()
		{
			Decimal rate = GetRebateRangeRate();

			if (this.Accrual.SalesVolume.HasValue)
			{
				this.Accrual.Rate = rate;
				this.Accrual.Value = Decimal.Round(this.Accrual.SalesVolume.Value * rate, 3);
			}
			else
			{
				throw new DataException("Rebate Type Tranche (Non-Retrospective): No sales volume");
			}
		}

		public override void CalculateDetail()
		{
			// Calculate the relevant rate
			Hierarchy baseHierarchy = this.Hierarchys[this.Hierarchys.Count - 1];
			Decimal rate = GetRebateRangeRate();

			// Now apply to the accrual breakdown
			foreach (AccrualDetail accrualDetail in this.Accrual.AccrualDetails)
			{
				if (accrualDetail.HierarchyId == baseHierarchy.Id)
				{
					accrualDetail.Value = Decimal.Round(accrualDetail.SalesVolume * rate, 3);

					AccrualDetail parentAccrualDetail = accrualDetail.FindParent(this.Accrual.AccrualDetails, this.Hierarchys);

					while (parentAccrualDetail != null)
					{
						parentAccrualDetail.Rate = rate;
						parentAccrualDetail.Value += accrualDetail.Value;

						parentAccrualDetail = parentAccrualDetail.FindParent(this.Accrual.AccrualDetails, this.Hierarchys);
					}

					this.Accrual.Rate = rate;
					this.Accrual.Value += accrualDetail.Value;
				}
			}
		}

		public override void CalculatePriceEnquiryRate()
		{
			Decimal rate = GetRebateRangeRate();

			foreach (RebateDetail rebateDetail in this.Rebate.RebateDetails)
			{
				rebateDetail.PriceEnquiryRate = rate;
			}
		}

		public override void ValidateData()
		{
			base.ValidateData();

			if (!this.Rebate.EstimatedVolume.HasValue)
			{
				throw new DataException("Tranche Rebate (Non-Retrospective): No fixed rebate value specified");
			}
		}

		public Decimal GetRebateRangeRate()
		{
			Decimal rate = 0;
			this.Rebate.RebateRanges.Sort(RebateRangeProperty.AboveQuantity);

			if (this.Rebate.RebateRanges != null && this.Rebate.RebateRanges.Count > 0)
			{
				Decimal value = 0;

				for (Int32 index = this.Rebate.RebateRanges.Count - 1; index >= 0; index--)
				{
					if (this.Rebate.EstimatedVolume > this.Rebate.RebateRanges[index].AboveQuantity)
					{
						Decimal quantity;

						if (index == this.Rebate.RebateRanges.Count - 1)
						{
							quantity = this.Rebate.EstimatedVolume.Value - this.Rebate.RebateRanges[index].AboveQuantity;
						}
						else if (index > 0) 
						{
							quantity = this.Rebate.RebateRanges[index + 1].AboveQuantity - this.Rebate.RebateRanges[index].AboveQuantity;
						}
						else
						{
							quantity = this.Rebate.RebateRanges[index].AboveQuantity;
						}

						value += quantity * this.Rebate.RebateRanges[index].Rate;
					}
				}

				rate = Decimal.Round(value / this.Rebate.EstimatedVolume.Value, 3);
			}

			return rate;
		}
	}
}
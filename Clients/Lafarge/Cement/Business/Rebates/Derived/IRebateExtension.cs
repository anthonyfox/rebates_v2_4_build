﻿using System;
using Evolve.Libraries.Data.Remoting;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public interface IRebateExtension : IEntity
	{
		// Properties
		string Title { get; }
		string EntityName { get; }
		string EntityTitle { get; }
		string PropertyName { get; }
		string RebateEntityName { get; }
		string RebateEntityTitle { get; }
		string RebatePropertyName { get; }
		Int32 TextBoxWidth { get; }
		Int32 MaxLength { get; }

		DateTime CreateTime { get; set; }
		int CreateUserId { get; set; }
		int DivisionId { get; set; }
		int Id { get; set; }
		string JDECode { get; set; }
		string Name { get; set; }
		DateTime UpdateTime { get; set; }
		int? UpdateUserId { get; set; }
	}
}

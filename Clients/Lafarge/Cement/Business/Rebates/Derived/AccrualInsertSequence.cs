﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class AccrualInsertSequence : SqlServerEntity
	{
		public AccrualInsertSequence()
		{
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.Accrual);
			OrderBy = "{" + AccrualDetailProperty.InsertSequence + "}";
			IsReadOnlyEntity = true;
		}

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(AccrualColumn.Id, true, true)]
		[Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set	{ _id = value; }
		}
		#endregion

		#region PROPERTY: DivisionAccrualId
		private Int32 _divisionAccrualId;
		[EntityColumn(AccrualColumn.DivisionAccrualId)]
		[Browsable(false)]
		public Int32 DivisionAccrualId
		{
			get { return _divisionAccrualId; }
			set	{ _divisionAccrualId = value; }
		}
		#endregion

		#region PROPERTY: InsertSequence
		private Int32 _insertSequence;
		[EntityColumn(AccrualColumn.InsertSequence)]
		[Browsable(false)]
		public Int32 InsertSequence
		{
			get { return _insertSequence; }
			set
			{
				_insertSequence = value;
				SetColumnDirty(AccrualProperty.InsertSequence);
			}
		}
		#endregion
	}
}

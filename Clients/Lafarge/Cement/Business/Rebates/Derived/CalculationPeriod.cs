﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class CalculationPeriod : IComparable
	{
		#region Properties

		#region PROPERTY: Year
		private Int16 _year = (Int16) (DateTime.Now.Year - 2);
		public Int16 Year
		{
			get { return _year; }
			set { _year = value; }
		}
		#endregion

		#region PROPERTY: Period
		private Int16 _period = 12;
		public Int16 Period
		{
			get { return _period; }
			set { _period = value; }
		}
		#endregion

		#endregion

		#region Constructors

		public CalculationPeriod()
		{
		}

		public CalculationPeriod(Int16 year, Int16 period)
		{
			_year = year;
			_period = period;
		}

		#endregion

		#region IComparable

		int IComparable.CompareTo(object obj)
		{
			CalculationPeriod calculationPeriod = (CalculationPeriod) obj;
			return String.Compare(this.ToString(), calculationPeriod.ToString());
		}

		#endregion

		#region Methods

		public override String ToString()
		{
			return _year.ToString("0000") + "/" + _period.ToString("00");
		}

		public CalculationPeriod NextPeriod()
		{
			CalculationPeriod nextPeriod;

			if (_period == 12)
			{
				nextPeriod = new CalculationPeriod((Int16) (_year + 1), 1);
			}
			else
			{
				nextPeriod = new CalculationPeriod(_year, (Int16) (_period + 1));
			}

			return nextPeriod;
		}

        public CalculationPeriod PreviousPeriod()
        {
            CalculationPeriod nextPeriod;

            if (_period == 12)
            {
                nextPeriod = new CalculationPeriod((Int16)(_year - 1), 1);
            }
            else
            {
                nextPeriod = new CalculationPeriod(_year, (Int16)(_period - 1));
            }

            return nextPeriod;
        }

        public List<CalculationPeriod> Enumerate()
		{
			List<CalculationPeriod> periods = new List<CalculationPeriod>();

			for (Int16 period = 1; period <= _period; period++)
			{
				periods.Add(new CalculationPeriod(_year, period));
			}

			return periods;
		}

		public List<CalculationPeriod> EnumeratePrevious()
		{
			List<CalculationPeriod> periods = this.Enumerate();
			periods.Remove(periods.Last());
			return periods;
		}

		public static Int32 PeriodCount(DateTime fromDate, DateTime toDate)
		{
			DateTime targetDate = new DateTime(toDate.Year, toDate.Month, 1);
			DateTime startDate = new DateTime(fromDate.Year, fromDate.Month, 1);
			Int32 periodCount = 0;

			while (startDate <= targetDate)
			{
				periodCount++;
				startDate = startDate.AddMonths(1);
			}

			return periodCount;
		}

		public static List<CalculationPeriod> Enumerate(DateTime fromDate, DateTime toDate)
		{
			List<CalculationPeriod> periods = new List<CalculationPeriod>();

			DateTime periodDate = new DateTime(fromDate.Year, fromDate.Month, 1);

			while (periodDate <= toDate)
			{
				periods.Add(new CalculationPeriod((Int16) periodDate.Year, (Int16) periodDate.Month));
				periodDate = periodDate.AddMonths(1);
			}

			return periods;
		}

		public void CalculationPeriodRange(out DateTime fromDate, out DateTime toDate)
		{
			fromDate = new DateTime(_year, _period, 1);
			toDate = new DateTime(_year, _period, 1).AddMonths(1).AddTicks(-1);
		}

		public void CalculationYearRange(out DateTime fromDate, out DateTime toDate)
		{
			fromDate = new DateTime(_year, 1, 1);
			toDate = new DateTime(_year, _period, 1).AddMonths(1).AddTicks(-1);
		}


		public Boolean Equals(CalculationPeriod calculationPeriod)
		{
			return (this.Year == calculationPeriod.Year && this.Period == calculationPeriod.Period);
		}

		#endregion
	}
}

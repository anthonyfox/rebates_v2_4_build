﻿using System;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public enum PaymentStatusEnum
	{
		Null,
		PendingAuthorisation,
		Authorised,
		Rejected
	}

	public static class PaymentStatusEnumConvertor
	{
		public static PaymentStatusEnum DBInt16ToPaymentStatusEnum(Object dbObject)
		{
			PaymentStatusEnum paymentType = PaymentStatusEnum.Null;

			if (!Object.ReferenceEquals(dbObject, System.DBNull.Value))
			{
				Int16 value = Convert.ToInt16(dbObject);

				switch (value)
				{
					case 1:
						paymentType = PaymentStatusEnum.PendingAuthorisation;
						break;
					case 2:
						paymentType = PaymentStatusEnum.Authorised;
						break;
					case 3:
						paymentType = PaymentStatusEnum.Rejected;
						break;
					default:
						paymentType = PaymentStatusEnum.Null;
						break;
				}
			}

			return paymentType;
		}

		public static Object PaymentStatusEnumToDBInt16(PaymentStatusEnum paymentType)
		{
			Object value = DBNull.Value;

			switch (paymentType)
			{
				case PaymentStatusEnum.PendingAuthorisation:
					value = 1;
					break;
				case PaymentStatusEnum.Authorised:
					value = 2;
					break;
				case PaymentStatusEnum.Rejected:
					value = 3;
					break;
			}

			return value;
		}
	}
}

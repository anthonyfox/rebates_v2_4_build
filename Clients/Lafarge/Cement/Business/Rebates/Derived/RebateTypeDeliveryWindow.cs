﻿using System;
using System.Data;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using System.Linq;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class RebateTypeDeliveryWindow : RebateTypeCalculator
	{
		public RebateTypeDeliveryWindow(Rebate rebate)
			: base(rebate)
		{
		}

		public override void CalculateGlobalProduct()
		{
			throw new DataException("RebateTypeDeliveryWindow: Global Products are not valid with Delivery Windows");
		}

		public override void CalculateDetail()
		{
			// Calculate the relevant rate
			Hierarchy baseHierarchy = this.Hierarchys[this.Hierarchys.Count - 1];

			// Now apply to the accrual breakdown
			foreach (AccrualDetail accrualDetail in this.Accrual.AccrualDetails)
			{
				if (accrualDetail.HierarchyId == baseHierarchy.Id)
				{
					if (accrualDetail.Rate.HasValue)
					{
						accrualDetail.Value = Decimal.Round(accrualDetail.SalesVolume * accrualDetail.Rate.Value, 3);
					}
					else
					{
						throw new DataException("RebateTypeDeliveryWindow: No product rate (RebateDeliveryWindowId = " + accrualDetail.RebateDeliveryWindowId.ToString() + ")");
					}

					AccrualDetail parentAccrualDetail = accrualDetail.FindParent(this.Accrual.AccrualDetails, this.Hierarchys);

					while (parentAccrualDetail != null)
					{
						parentAccrualDetail.Rate = null;
						parentAccrualDetail.Value += accrualDetail.Value;

						parentAccrualDetail = parentAccrualDetail.FindParent(this.Accrual.AccrualDetails, this.Hierarchys);
					}

					this.Accrual.Rate = null;
					this.Accrual.Value += accrualDetail.Value;
				}
			}
		}

		public override void CalculatePriceEnquiryRate()
		{
			foreach (RebateDetail rebateDetail in this.Rebate.RebateDetails)
			{
				rebateDetail.PriceEnquiryRate = null;
			}
		}

		public override void ValidateData()
		{
			base.ValidateData();
		}
	}
}

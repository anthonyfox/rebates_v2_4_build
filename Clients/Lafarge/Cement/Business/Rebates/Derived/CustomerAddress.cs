﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class CustomerAddress : SqlServerEntity
	{
		private const String ParentCustomer = "ParentCustomer";
		private const String ParentCustomerGroup = "ParentCustomerGroup";

		#region Properties

		#region PROPERTY: Id
		private Int32 _id;
		[EntityColumn(CustomerColumn.Id), Browsable(false)]
		public Int32 Id
		{
			get { return _id; }
			set
			{ 
				_id = value;
			}
		}
		#endregion

		#region PROPERTY: CustomerGroupId
		private Int32 _customerGroupId;
		[EntityColumn(CustomerColumn.CustomerGroupId)]
		[Browsable(false)]
		public Int32 CustomerGroupId
		{
			get { return _customerGroupId; }
			set
			{
				_customerGroupId = value;
				SetColumnDirty(CustomerAddressProperty.CustomerGroupId);
			}
		}
		#endregion

		#region PROPERTY: SoldToId
		private Int32 _soldToId;
		[EntityColumn(CustomerColumn.SoldToId)]
		[Browsable(false)]
		public Int32 SoldToId
		{
			get { return _soldToId; }
			set
			{
				_soldToId = value;
				SetColumnDirty(CustomerAddressProperty.SoldToId);
			}
		}
		#endregion

		#region PROPERTY: DivisionId
		private Int32? _divisionId = 0;
		[EntityColumn(CustomerGroupColumn.DivisionId, DBRebates.CustomerGroup), Browsable(false)]
		public Int32? DivisionId
		{
			get { return _divisionId; }
			set { _divisionId = value; }
		}
		#endregion

		#region PROPERTY: JDECode
		private string _jdeCode;
		[EntityColumn(CustomerColumn.JDECode), DisplayName("Account")]
		public string JDECode
		{
			get { return _jdeCode; }
			set 
			{
				_jdeCode = value;
			}
		}
		#endregion

		#region PROPERTY: Name
		private String _name;
		[EntityColumn(CustomerColumn.Name)]
		public String Name
		{
			get { return _name; }
			set 
			{
				_name = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: AddressId
		private Int32 _addressId;
		[EntityColumn(AddressColumn.Id, DBRebates.Address, CustomerAddressProperty.AddressId), Browsable(false)]
		public Int32 AddressId
		{
			get { return _addressId; }
			set 
			{
				_addressId = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: Address1
		private String _address1;
		[EntityColumn(AddressColumn.Address1, DBRebates.Address, CustomerAddressProperty.Address1), DisplayName("Address 1")]
		public String Address1
		{
			get { return _address1; }
			set 
			{
				_address1 = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: Address2
		private String _address2;
		[EntityColumn(AddressColumn.Address2, DBRebates.Address, CustomerAddressProperty.Address2), DisplayName("Address 2")]
		public String Address2
		{
			get { return _address2; }
			set 
			{
				_address2 = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: Address3
		private String _address3;
		[EntityColumn(AddressColumn.Address3, DBRebates.Address, CustomerAddressProperty.Address3), DisplayName("Address 3")]
		public String Address3
		{
			get { return _address3; }
			set 
			{
				_address3 = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: Address4
		private String _address4;
		[EntityColumn(AddressColumn.Address4, DBRebates.Address, CustomerAddressProperty.Address4), DisplayName("Address 4")]
		public String Address4
		{
			get { return _address4; }
			set 
			{
				_address4 = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: City
		private String _city;
		[EntityColumn(AddressColumn.City, DBRebates.Address, CustomerAddressProperty.City), DisplayName("City")]
		public String City
		{
			get { return _city; }
			set 
			{
				_city = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: County
		private String _county;
		[EntityColumn(AddressColumn.County, DBRebates.Address, CustomerAddressProperty.County), DisplayName("County")]
		public String County
		{
			get { return _county; }
			set 
			{
				_county = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: PostCode
		private String _postCode;
		[EntityColumn(AddressColumn.PostCode, DBRebates.Address, CustomerAddressProperty.PostCode), DisplayName("Post Code")]
		public String PostCode
		{
			get { return _postCode; }
			set 
			{
				_postCode = value;
			}
		}
		#endregion

		#region RELATED ENTITY PROPERTY: PostCode
		private Int32? _parentDivisionId;
		[EntityColumn(CustomerGroupColumn.DivisionId, ParentCustomerGroup, CustomerAddressProperty.ParentDivisionId)]
		[Browsable(false)]
		public Int32? ParentDivisionId
		{
			get { return _parentDivisionId; }
			set	{ _parentDivisionId = value; }
		}
		#endregion

		#region PROPERTY: SearchType
		private CustomerSearchTypeEnum _customerSearchTypeEnum;
		[EntityColumn(CustomerColumn.CustomerSearchTypeEnum), Browsable(false)]
		[EntityColumnConversion("DBStringToCustomerSearchTypeEnum", "CustomerSearchTypeEnumToDBString", "CustomerSearchTypeEnumConvertor", "Evolve.Clients.Lafarge.Cement.Business.Rebates", "Evolve.Clients.Lafarge.Cement.Business")]
		public CustomerSearchTypeEnum CustomerSearchTypeEnum
		{
			get { return _customerSearchTypeEnum; }
			set
			{
				_customerSearchTypeEnum = value;
				SetColumnDirty(CustomerAddressProperty.CustomerSearchTypeEnum);
			}
		}
		#endregion

		#endregion

		#region Constructors

		public CustomerAddress()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			IsReadOnlyEntity = true;
			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.Customer);

			JoinTable(DBRebates.DatabaseName, DBRebates.Address, DBRebates.Address + "." + AddressColumn.Id + " = " + DBRebates.Customer + "." + CustomerAddressProperty.AddressId, EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.CustomerGroup, DBRebates.Customer + "." + CustomerColumn.CustomerGroupId + " = " + DBRebates.CustomerGroup + "." + CustomerGroupColumn.Id, EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.Customer, DBRebates.Customer + "." + CustomerColumn.SoldToId + " = " + ParentCustomer + "." + CustomerColumn.Id, ParentCustomer, EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.CustomerGroup, ParentCustomer + "." + CustomerColumn.CustomerGroupId + " = " + ParentCustomerGroup + "." + CustomerGroupColumn.Id, ParentCustomerGroup, EntityJoinType.LeftOuter);
		}

		#endregion

		#region Business Logic

		/// <summary>
		/// Search for customers matching specified criteria
		/// </summary>
		public ExtendedBindingList<CustomerAddress> Search(CustomerAddressSearchMode searchMode, Int32? filterId, String name, String address1, String address2, String address3, String address4, String city, String county, String postCode, Int32 rowLimit, out Boolean isRowLimitExceeded)
		{
			String filter = "";

			// Filter for Division
			filter = SqlHelper.FilterBuilder(filter, "({" + CustomerAddressProperty.DivisionId + "} = " + _divisionId.ToString() + " or {" + CustomerAddressProperty.ParentDivisionId + "} = " + _divisionId.ToString() + " or {" + CustomerAddressProperty.DivisionId + "} is null or {" + CustomerAddressProperty.ParentDivisionId + "} is null)");

			// Add search mode filter
			switch (searchMode)
			{
				case CustomerAddressSearchMode.SoldTo:
					filter = SqlHelper.FilterBuilder(filter, 
						"({" + CustomerAddressProperty.CustomerSearchTypeEnum + "} = '" + CustomerSearchTypeEnumConvertor.CustomerSearchTypeEnumToDBString(CustomerSearchTypeEnum.SoldTo) + 
						"' or {" + CustomerAddressProperty.CustomerSearchTypeEnum + "} = '" + CustomerSearchTypeEnumConvertor.CustomerSearchTypeEnumToDBString(CustomerSearchTypeEnum.SoldToInActive) + "')");
					break;

				case CustomerAddressSearchMode.SoldToByGroup:
					filter = SqlHelper.FilterBuilder(filter, "{" + CustomerAddressProperty.CustomerGroupId + "} = " + filterId.Value.ToString());
					filter = SqlHelper.FilterBuilder(filter, 
						"({" + CustomerAddressProperty.CustomerSearchTypeEnum + "} = '" + CustomerSearchTypeEnumConvertor.CustomerSearchTypeEnumToDBString(CustomerSearchTypeEnum.SoldTo) + 
						"' or {" + CustomerAddressProperty.CustomerSearchTypeEnum + "} = '" + CustomerSearchTypeEnumConvertor.CustomerSearchTypeEnumToDBString(CustomerSearchTypeEnum.SoldToInActive) + "')");					
					break;

				case CustomerAddressSearchMode.ShipTo:
					filter = SqlHelper.FilterBuilder(filter, 
						"({" + CustomerAddressProperty.CustomerSearchTypeEnum + "} = '" + CustomerSearchTypeEnumConvertor.CustomerSearchTypeEnumToDBString(CustomerSearchTypeEnum.ShipTo) + 
						"' or {" + CustomerAddressProperty.CustomerSearchTypeEnum + "} = '" + CustomerSearchTypeEnumConvertor.CustomerSearchTypeEnumToDBString(CustomerSearchTypeEnum.ShipToInActive) + "')");
					break;

				case CustomerAddressSearchMode.ShipToByGroup:
					filter = SqlHelper.FilterBuilder(filter, "{" + CustomerAddressProperty.CustomerGroupId + "} = " + filterId.Value.ToString());
					filter = SqlHelper.FilterBuilder(filter, 
						"({" + CustomerAddressProperty.CustomerSearchTypeEnum + "} = '" + CustomerSearchTypeEnumConvertor.CustomerSearchTypeEnumToDBString(CustomerSearchTypeEnum.ShipTo) + 
						"' or {" + CustomerAddressProperty.CustomerSearchTypeEnum + "} = '" + CustomerSearchTypeEnumConvertor.CustomerSearchTypeEnumToDBString(CustomerSearchTypeEnum.ShipToInActive) + "')");
					break;

				case CustomerAddressSearchMode.ShipToByParent:
					filter = SqlHelper.FilterBuilder(filter, "{" + CustomerAddressProperty.SoldToId + "} = " + filterId.Value.ToString());
					filter = SqlHelper.FilterBuilder(filter, 
						"({" + CustomerAddressProperty.CustomerSearchTypeEnum + "} = '" + CustomerSearchTypeEnumConvertor.CustomerSearchTypeEnumToDBString(CustomerSearchTypeEnum.ShipTo) + 
						"' or {" + CustomerAddressProperty.CustomerSearchTypeEnum + "} = '" + CustomerSearchTypeEnumConvertor.CustomerSearchTypeEnumToDBString(CustomerSearchTypeEnum.ShipToInActive) + "')");
					break;

				default:
					throw new ArgumentException("Invalid CustomerAddressSearchMode");
			}

			// Finally filter by user entered criteria
			if (name.Length > 0)
			{
				filter = SqlHelper.FilterBuilder(filter, "{" + CustomerAddressProperty.Name + "} " + this.WildcardKeyword() + " '" + name.Replace('*', this.WildcardCharacter()) + this.WildcardCharacter() + "'");
			}

			if (address1.Length > 0)
			{
				filter = SqlHelper.FilterBuilder(filter, "{" + CustomerAddressProperty.Address1 + "} " + this.WildcardKeyword() + " '" + address1.Replace('*', this.WildcardCharacter()) + this.WildcardCharacter() + "'");
			}

			if (address2.Length > 0)
			{
				filter = SqlHelper.FilterBuilder(filter, "{" + CustomerAddressProperty.Address2 + "} " + this.WildcardKeyword() + " '" + address2.Replace('*', this.WildcardCharacter()) + this.WildcardCharacter() + "'");
			}

			if (address3.Length > 0)
			{
				filter = SqlHelper.FilterBuilder(filter, "{" + CustomerAddressProperty.Address3 + "} " + this.WildcardKeyword() + " '" + address3.Replace('*', this.WildcardCharacter()) + this.WildcardCharacter() + "'");
			}

			if (address4.Length > 0)
			{
				filter = SqlHelper.FilterBuilder(filter, "{" + CustomerAddressProperty.Address4 + "} " + this.WildcardKeyword() + " '" + address4.Replace('*', this.WildcardCharacter()) + this.WildcardCharacter() + "'");
			}
	
			if (city.Length > 0)
			{
				filter = SqlHelper.FilterBuilder(filter, "{" + CustomerAddressProperty.City + "} " + this.WildcardKeyword() + " '" + city.Replace('*', this.WildcardCharacter()) + this.WildcardCharacter() + "'");
			}

			if (county.Length > 0)
			{
				filter = SqlHelper.FilterBuilder(filter, "{" + CustomerAddressProperty.County + "} " + this.WildcardKeyword() + " '" + county.Replace('*', this.WildcardCharacter()) + this.WildcardCharacter() + "'");
			}

			if (postCode.Length > 0)
			{
				filter = SqlHelper.FilterBuilder(filter, "{" + CustomerAddressProperty.PostCode + "} " + this.WildcardKeyword() + " '" + postCode.Replace('*', this.WildcardCharacter()) + this.WildcardCharacter() + "'");
			}

			// Finally do the read
			CustomerAddress customer = new CustomerAddress();
			customer.ReadFilter = filter;
			ExtendedBindingList<CustomerAddress> customers = (ExtendedBindingList<CustomerAddress>) customer.Read(rowLimit, out isRowLimitExceeded);

			return customers;
		}

		#endregion
	}

	public enum CustomerAddressSearchMode
	{
		SoldTo,
		SoldToByGroup,
		ShipTo,
		ShipToByGroup,
		ShipToByParent
	}

	public class CustomerAddressProperty
	{
		public const string Id = "Id";
		public const string CustomerGroupId = "CustomerGroupId";
		public const string SoldToId = "SoldToId";
		public const string DivisionId = "DivisionId";
		public const string JDECode = "JDECode";
		public const string Name = "Name";
		public const string AddressId = "AddressId";
		public const string Address1 = "Address1";
		public const string Address2 = "Address2";
		public const string Address3 = "Address3";
		public const string Address4 = "Address4";
		public const string City = "City";
		public const string County = "County";
		public const string PostCode = "PostCode";
		public const string ParentDivisionId = "ParentDivisionId";
		public const string CustomerSearchTypeEnum = "CustomerSearchTypeEnum";
	}
}

﻿using System;
using System.Data;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using System.Linq;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class RebateTypePercentage : RebateTypeCalculator
	{
		public RebateTypePercentage(Rebate rebate)
			: base(rebate)
		{
		}

		public override void CalculateGlobalProduct()
		{
			if (this.Accrual.Rate.HasValue && this.Accrual.SalesValue.HasValue)
			{
				this.Accrual.Value = Decimal.Round(this.Accrual.SalesValue.Value * (this.Accrual.Rate.Value / 100), 3);
			}
			else
			{
				throw new DataException("RebateTypeFixed: No Global Product Rate or Sales Value");
			}
		}

		public override void CalculateDetail()
		{
			// Calculate the relevant rate
			Hierarchy baseHierarchy = this.Hierarchys[this.Hierarchys.Count - 1];

			// Now apply to the accrual breakdown
			foreach (AccrualDetail accrualDetail in this.Accrual.AccrualDetails)
			{
				if (accrualDetail.HierarchyId == baseHierarchy.Id)
				{
					if (accrualDetail.Rate.HasValue)
					{
						accrualDetail.Value = Decimal.Round(accrualDetail.SalesValue * (accrualDetail.Rate.Value / 100), 3);
					}
					else
					{
						throw new DataException("RebateTypeFixed: No product rate (RebateDetailId = " + accrualDetail.RebateDetailId.ToString() + ")");
					}

					AccrualDetail parentAccrualDetail = accrualDetail.FindParent(this.Accrual.AccrualDetails, this.Hierarchys);

					while (parentAccrualDetail != null)
					{
						parentAccrualDetail.Rate = null;
						parentAccrualDetail.Value += accrualDetail.Value;

						parentAccrualDetail = parentAccrualDetail.FindParent(this.Accrual.AccrualDetails, this.Hierarchys);
					}

					this.Accrual.Rate = null;
					this.Accrual.Value += accrualDetail.Value;
				}
			}
		}

		public override void CalculatePriceEnquiryRate()
		{
			foreach (RebateDetail rebateDetail in this.Rebate.RebateDetails)
			{
				rebateDetail.PriceEnquiryRate = rebateDetail.Rate;
			}
		}
		
		public override void ValidateData()
		{
			base.ValidateData();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;


namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class PaymentReview : SqlServerEntity
	{
		#region Properties

		[EntityColumn(RebateColumn.Number, DBRebates.Rebate, PaymentReviewProperty.RebateNumber)]
		[DisplayName("Rebate Number")]
		public Int32 RebateNumber { get; set; }

		[EntityColumn(RebateColumn.Version, DBRebates.Rebate, PaymentReviewProperty.RebateVersion)]
		[DisplayName("Rebate Version")]
		public Int16 RebateVersion { get; set; }

		[EntityColumn(RebateColumn.RebateTypeId, DBRebates.Rebate, PaymentReviewProperty.RebateTypeId)]
		[Browsable(false)]
		public Int32 RebateTypeId { get; set; }

		[EntityColumn(RebateTypeColumn.Name, DBRebates.RebateType, PaymentReviewProperty.RebateTypeName)]
		[DisplayName("Rebate Type")]
		public String RebateTypeName { get; set; }

		[EntityColumn(RebateColumn.HierarchyId, DBRebates.Rebate, PaymentReviewProperty.HierarchyId)]
		[Browsable(false)]
		public Int32 HierarchyId { get; set; }

		[EntityColumn(RebateColumn.HierarchyEntityId, DBRebates.Rebate, PaymentReviewProperty.HierarchyEntityId)]
		[Browsable(false)]
		public Int32 HierarchyEntityId { get; set; }

		[EntityColumn(HierarchyColumn.Level, DBRebates.Hierarchy, PaymentReviewProperty.HierarchyLevel)]
		[Browsable(false)]
		public Int16 HierarchyLevel { get; set; }

		[EntityColumn(HierarchyColumn.Name, DBRebates.Hierarchy, PaymentReviewProperty.HierarchyName)]
		[DisplayName("Hierarchy")]
		public String HierarchyName { get; set; }

		[EntityColumn(CustomerGroupColumn.Name, DBRebates.CustomerGroup, PaymentReviewProperty.CustomerGroupName)]
		[Browsable(false)]
		public String CustomerGroupName { get; set; }

		[EntityColumn(CustomerColumn.Name, DBRebates.Customer, PaymentReviewProperty.CustomerName)]
		[Browsable(false)]
		public String CustomerName { get; set; }

		[DisplayName("Name")]
		public String HierarchyRelatedName
		{
			get
			{
				if (this.HierarchyLevel == 1)
				{
					return this.CustomerGroupName;
				}
				else
				{
					return this.CustomerName;
				}
			}
		}

		[EntityColumn(CustomerGroupColumn.Name, DBRebates.SoldToGroup, PaymentReviewProperty.SoldToGroupName)]
		[Browsable(false)]
		public String SoldToGroupName { get; set; }

		[EntityColumn(CustomerGroupColumn.Name, DBRebates.ShipToGroup, PaymentReviewProperty.ShipToGroupName)]
		[Browsable(false)]
		public String ShipToGroupName { get; set; }

		[DisplayName("Parent Group")]
		public String ParentGroup
		{
			get
			{
				String parentGroup = "";

				switch (this.HierarchyLevel)
				{
					case 1:
						// Rebate is setup at Major Group Level
						parentGroup = this.CustomerGroupName;
						break;

					case 2:
						// Rebate is setup at Sold To Level, return the Customer Group Id
						// from the sold to account
						parentGroup = this.SoldToGroupName;
						break;

					case 3:
						// Rebate is setup at Ship To Level, return the ship to accounts parent customer
						// customer group id
						parentGroup = this.ShipToGroupName;
						break;

					default:
						throw new ArgumentException("Unexpected Hierarchy Level (" + this.HierarchyLevel.ToString() + ") for Rebate Id " + this.RebateId.ToString());
				}

				return parentGroup;
			}
		}

		[EntityColumn(RebateColumn.Description, DBRebates.Rebate, PaymentReviewProperty.Description)]
		public String Description { get; set; }

		[EntityColumn(RebateColumn.IsContract, DBRebates.Rebate, PaymentReviewProperty.IsContract)]
		[DisplayName("Contract")]
		public Boolean IsContract { get; set; }

		[EntityColumn(RebateColumn.IsJif, DBRebates.Rebate, PaymentReviewProperty.IsJif)]
		[DisplayName("JIF")]
		public Boolean IsJif { get; set; }

		[EntityColumn(RebateColumn.PayeeId, DBRebates.Rebate, PaymentReviewProperty.RebatePayeeId)]
		[Browsable(false)]
		public Int32? RebatePayeeId { get; set; }

		[EntityColumn(PayeeColumn.JDECode, DBRebates.Payee, PaymentReviewProperty.RebatePayeeJDECode)]
		[DisplayName("Header Payee JDE Code")]
		public Int32? RebatePayeeJDECode { get; set; }

		[EntityColumn(PayeeColumn.Name, DBRebates.Payee, PaymentReviewProperty.RebatePayeeName)]
		[DisplayName("Header Payee")]
		public String RebatePayeeName { get; set; }

		[EntityColumn(RebateColumn.StartDate, DBRebates.Rebate, PaymentReviewProperty.StartDate, DBDateTimeFrom=DBDateTime.Year, DBDateTimeTo=DBDateTime.Day)]
		[DisplayName("Start Date")]
		public DateTime StartDate { get; set; }

		[EntityColumn(RebateColumn.EndDate, DBRebates.Rebate, PaymentReviewProperty.EndDate, DBDateTimeFrom = DBDateTime.Year, DBDateTimeTo = DBDateTime.Day)]
		[DisplayName("End Date")]
		public DateTime? EndDate { get; set; }


		[EntityColumn(Payment_PaymentBreakdownColumn.CreateTime, DBRebates.Payment_PaymentBreakdown, PaymentReviewProperty.CreateTime, DBDateTimeFrom = DBDateTime.Year, DBDateTimeTo = DBDateTime.Second)]
		[DisplayName("Created")]
		public DateTime CreateTime { get; set; }

		[EntityColumn(PayeeColumn.Id, DBRebates.PaymentPayee, PaymentReviewProperty.PayeeId)]
		[Browsable(false)]
		public Int32 PayeeId { get; set; }

		[EntityColumn(PayeeColumn.JDECode, DBRebates.PaymentPayee, PaymentReviewProperty.PayeeJDECode)]
		[DisplayName("Payee JDE Code")]
		public Int32 PayeeJDECode { get; set; }

		[EntityColumn(PayeeColumn.Name, DBRebates.PaymentPayee, PaymentReviewProperty.PayeeName)]
		[DisplayName("Payee")]
		public String PayeeName { get; set; }

		[EntityColumn(PaymentBreakdownColumn.TaxDate, DBRebates.PaymentBreakdown, PaymentReviewProperty.TaxDate, DBDateTimeFrom=DBDateTime.Year, DBDateTimeTo=DBDateTime.Day)]
		[DisplayName("Tax Date")]
		public DateTime TaxDate { get; set; }

		[EntityColumn(PaymentBreakdownColumn.TaxCodeId, DBRebates.PaymentBreakdown, PaymentReviewProperty.TaxCodeId)]
		[Browsable(false)]
		public Int32 TaxCodeId { get; set; }

		[EntityColumn(TaxCodeColumn.Code, DBRebates.TaxCode, PaymentReviewProperty.TaxCode)]
		[DisplayName("Tax Code")]
		public String TaxCode { get; set; }

		[EntityColumn(PaymentBreakdownColumn.Value, DBRebates.Payment, PaymentReviewProperty.Value)]
		[DisplayName("Payment Value")]
		public Decimal Value { get; set; }

		[EntityColumn(Payment_PaymentBreakdownColumn.PaymentStatusEnum, DBRebates.Payment_PaymentBreakdown, PaymentReviewProperty.PaymentStatusEnum)]
		[EntityColumnConversion("DBInt16ToPaymentStatusEnum", "PaymentStatusEnumToDBInt16", "PaymentStatusEnumConvertor", "Evolve.Clients.Lafarge.Cement.Business.Rebates", "Evolve.Clients.Lafarge.Cement.Business")]
		[DisplayName("Payment Status")]
		public PaymentStatusEnum PaymentStatusEnum { get; set; }

		[EntityColumn(PaymentBreakdownColumn.Comment, DBRebates.PaymentBreakdown, PaymentReviewProperty.Comments)]
		public String Comments { get; set; }

		[EntityColumn(RebateColumn.Id, DBRebates.Rebate, PaymentReviewProperty.RebateId)]
		[DisplayName("Rebate Id")]
		public Int32 RebateId { get; set; }

		[EntityColumn(PaymentBreakdownColumn.Id, DBRebates.PaymentBreakdown, PaymentReviewProperty.PaymentBreakdownId)]
		[DisplayName("Payment Breakdown Id")]
		public Int32 PaymentBreakdownId { get; set; }


		[EntityColumn(RebateColumn.DeliveryTypeEnum, DBRebates.Rebate, PaymentReviewProperty.DeliveryTypeEnum)]
		[EntityColumnConversion("DBInt16ToDeliveryTypeEnum", "DeliveryTypeEnumToDBInt16", "DeliveryTypeEnumConvertor", "Evolve.Clients.Lafarge.Cement.Business.Rebates", "Evolve.Clients.Lafarge.Cement.Business")]
		[Browsable(false)]
		public DeliveryTypeEnum DeliveryTypeEnum { get; set; }

		[EntityColumn(RebateColumn.SiteId, DBRebates.Rebate, PaymentReviewProperty.SiteId)]
		[Browsable(false)]
		public Int32? SiteId { get; set; }

		[EntityColumn(RebateColumn.IsVAP, DBRebates.Rebate, PaymentReviewProperty.IsVAP)]
		[Browsable(false)]
		public Boolean IsVAP { get; set; }

		[EntityColumn(RebateColumn.IsGlobalProduct, DBRebates.Rebate, PaymentReviewProperty.IsGlobalProduct)]
		[Browsable(false)]
		public Boolean IsGlobalProduct { get; set; }

		[EntityColumn(RebateColumn.PaymentFrequencyId, DBRebates.Rebate, PaymentReviewProperty.PaymentFrequencyId)]
		[Browsable(false)]
		public Int32 PaymentFrequencyId { get; set; }

		[EntityColumn(CustomerColumn.CustomerGroupId, DBRebates.Customer, RebateProperty.CustomerGroupId)]
		[Browsable(false)]
		public Int32 CustomerGroupId { get; set; }

		[EntityColumn(CustomerColumn.CustomerGroupId, DBRebates.ParentCustomer, RebateProperty.ParentCustomerGroupId)]
		[Browsable(false)]
		public Int32 ParentCustomerGroupId { get; set; }

		[Browsable(false)]
		public Int32 SearchCustomerGroupId
		{
			get
			{
				Int32 searchId = -1;

				switch (this.HierarchyLevel)
				{
					case 1:
						// Rebate is setup at Major Group Level
						searchId = this.HierarchyEntityId;
						break;

					case 2:
						// Rebate is setup at Sold To Level, return the Customer Group Id
						// from the sold to account
						searchId = this.CustomerGroupId;
						break;

					case 3:
						// Rebate is setup at Ship To Level, return the ship to accounts parent customer
						// customer group id
						searchId = this.ParentCustomerGroupId;
						break;

					default:
						throw new ArgumentException("Unexpected Hierarchy Level (" + this.HierarchyLevel.ToString() + ") for Rebate Id " + this.RebateId.ToString());
				}

				return searchId;
			}
		}

		#endregion

		#region Constructors

		public PaymentReview()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			this.IsReadOnlyEntity = true;
			this.SelectUnique = true;

			DatabaseTable = DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.Payment_PaymentBreakdown);

			JoinTable(DBRebates.DatabaseName, DBRebates.Payment, DBRebates.Payment + "." + PaymentColumn.Payment_PaymentBreakdownId + " = " + DBRebates.Payment_PaymentBreakdown + "." + Payment_PaymentBreakdownColumn.Id);
			JoinTable(DBRebates.DatabaseName, DBRebates.PaymentBreakdown, DBRebates.PaymentBreakdown + "." + PaymentColumn.Payment_PaymentBreakdownId + " = " + DBRebates.Payment_PaymentBreakdown + "." + Payment_PaymentBreakdownColumn.Id);
			JoinTable(DBRebates.DatabaseName, DBRebates.Rebate, DBRebates.Rebate + "." + RebateColumn.Id + " = " + DBRebates.Payment + "." + PaymentColumn.RebateId);
			JoinTable(DBRebates.DatabaseName, DBRebates.RebateType, DBRebates.RebateType + "." + RebateColumn.Id + " = " + DBRebates.Rebate + "." + RebateColumn.RebateTypeId);
			JoinTable(DBRebates.DatabaseName, DBRebates.Payee, DBRebates.Payee + "." + RebateColumn.Id + " = " + DBRebates.Rebate + "." + RebateColumn.PayeeId, EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.TaxCode, DBRebates.TaxCode + "." + TaxCodeColumn.Id + " = " + DBRebates.PaymentBreakdown + "." + PaymentBreakdownColumn.TaxCodeId);
			JoinTable(DBRebates.DatabaseName, DBRebates.Hierarchy, DBRebates.Hierarchy + "." + HierarchyColumn.Id + " = " + DBRebates.Rebate + "." + RebateColumn.HierarchyId);
			JoinTable(DBRebates.DatabaseName, DBRebates.Payee, DBRebates.PaymentPayee + "." + PayeeColumn.Id + " = " + DBRebates.PaymentBreakdown + "." + PaymentBreakdownColumn.PayeeId, DBRebates.PaymentPayee);

			// Join related hierarchy entities (use outer join as we are not sure which one it will be)
			JoinTable(DBRebates.DatabaseName, DBRebates.CustomerGroup, DBRebates.CustomerGroup + "." + CustomerGroupColumn.Id + " = " + DBRebates.Rebate + "." + RebateColumn.HierarchyEntityId, EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.Customer, DBRebates.Customer + "." + CustomerColumn.Id + " = " + DBRebates.Rebate + "." + RebateColumn.HierarchyEntityId, EntityJoinType.LeftOuter);

			// Outer join customer back to customer to read parent customer (Sold To) details for Rebates setup
			// at Ship To level (used for hierarchy drill-down search functionality)
			JoinTable(DBRebates.DatabaseName, DBRebates.Customer, DBRebates.ParentCustomer + "." + CustomerColumn.Id + " = " + DBRebates.Customer + "." + CustomerColumn.SoldToId, DBRebates.ParentCustomer, EntityJoinType.LeftOuter);

			// Joins for ParentGroup Details
			JoinTable(DBRebates.DatabaseName, DBRebates.CustomerGroup, DBRebates.SoldToGroup + "." + CustomerGroupColumn.Id + " = " + DBRebates.Customer + "." + CustomerColumn.CustomerGroupId, DBRebates.SoldToGroup, EntityJoinType.LeftOuter);
			JoinTable(DBRebates.DatabaseName, DBRebates.CustomerGroup, DBRebates.ShipToGroup + "." + CustomerGroupColumn.Id + " = " + DBRebates.ParentCustomer + "." + CustomerColumn.CustomerGroupId, DBRebates.ShipToGroup, EntityJoinType.LeftOuter);
		}

		#endregion
	}

	public class PaymentReviewProperty
	{
		// Properties Used in Display (or to Derive Display Fields)
		public const string RebateNumber = "RebateNumber";
		public const string RebateVersion = "RebateVersion";
		public const string RebateTypeId = "RebateTypeId";
		public const string RebateTypeName = "RebateTypeName";
		public const string StartDate = "StartDate";
		public const string EndDate = "EndDate";
		public const string Description = "Description";
		public const string RebatePayeeId = "RebatePayeeId";
		public const string RebatePayeeJDECode = "RebatePayeeJDECode";
		public const string RebatePayeeName = "RebatePayeeName";
		public const string HierarchyId = "HierarchyId";
		public const string HierarchyEntityId = "HierarchyEntityId";
		public const string HierarchyLevel = "HierarchyLevel";
		public const string HierarchyName = "HierarchyName";
		public const string CustomerGroupName = "CustomerGroupName";
		public const string CustomerName = "CustomerName";
		public const string SoldToGroupName = "SoldToGroupName";
		public const string ShipToGroupName = "ShipToGroupName";
		public const string ParentGroup = "ParentGroup";
		public const string IsContract = "IsContract";
		public const string IsJif = "IsJif";
		public const string TaxCodeId = "TaxCodeId";
		public const string TaxCode = "TaxCode";
		public const string TaxDate = "TaxDate";
		public const string CreateTime = "CreateTime";
		public const string PayeeId = "PayeeId";
		public const string PayeeJDECode = "PayeeJDECode";
		public const string PayeeName = "PayeeName";
		public const string Value = "Value";
		public const string PaymentBreakdownId = "PaymentBreakdownId";
		public const string RebateId = "RebateId";
		public const string PaymentStatusEnum = "PaymentStatusEnum";
		public const string Comments = "Comments";

		// Properties Only Required for Search Screen
		public const string DeliveryTypeEnum = "DeliveryTypeEnum";
		public const string SiteId = "SiteId";
		public const string IsVAP = "IsVAP";
		public const string IsGlobalProduct = "IsGlobalProduct";
		public const string PaymentFrequencyId = "PaymentFrequencyId";
		public const string SearchCustomerGroupId = "SearchCustomerGroupId";
	}
}

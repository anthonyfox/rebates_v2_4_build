﻿using System;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public enum AccrualTypeEnum
	{
		Null,
		Actual,
		Forecast
	}

	public static class AccrualTypeEnumConvertor
	{
		public static AccrualTypeEnum DBInt16ToAccrualTypeEnum(Object dbObject)
		{
			AccrualTypeEnum accrualType = AccrualTypeEnum.Null;

			if (!Object.ReferenceEquals(dbObject, System.DBNull.Value))
			{
				Int16 value = Convert.ToInt16(dbObject);

				switch (value)
				{
					case 1:
						accrualType = AccrualTypeEnum.Actual;
						break;
					case 2:
						accrualType = AccrualTypeEnum.Forecast;
						break;
					default:
						accrualType = AccrualTypeEnum.Null;
						break;
				}
			}

			return accrualType;
		}

		public static Object AccrualTypeEnumToDBInt16(AccrualTypeEnum accrualType)
		{
			Object value = DBNull.Value;

			switch (accrualType)
			{
				case AccrualTypeEnum.Actual:
					value = 1;
					break;
				case AccrualTypeEnum.Forecast:
					value = 2;
					break;
			}

			return value;
		}
	}
}

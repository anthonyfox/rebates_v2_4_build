﻿using System;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
    public enum DatabaseActionEnum
    {
        Null,
        Insert,
        Update,
        Delete
    }

    public static class DatabaseActionEnumConvertor
    {
        public static DatabaseActionEnum DBInt16ToDatabaseActionEnum(Object dbObject)
        {
            DatabaseActionEnum databaseAction = DatabaseActionEnum.Null;

            if (!Object.ReferenceEquals(dbObject, System.DBNull.Value))
            {
                Int16 value = Convert.ToInt16(dbObject);

                switch (value)
                {
                    case 1:
                        databaseAction = DatabaseActionEnum.Insert;
                        break;
                    case 2:
                        databaseAction = DatabaseActionEnum.Update;
                        break;
                    case 3:
                        databaseAction = DatabaseActionEnum.Delete;
                        break;
                    default:
                        databaseAction = DatabaseActionEnum.Null;
                        break;
                }
            }

            return databaseAction;
        }

        public static Object DatabaseActionEnumToDBInt16(DatabaseActionEnum databaseAction)
        {
            Object value = DBNull.Value;

            switch (databaseAction)
            {
                case DatabaseActionEnum.Insert:
                    value = 1;
                    break;
                case DatabaseActionEnum.Update:
                    value = 2;
                    break;
                case DatabaseActionEnum.Delete:
                    value = 3;
                    break;
            }

            return value;
        }
    }
}

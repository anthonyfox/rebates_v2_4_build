﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Evolve.Clients.Lafarge.Cement.Business.Rebates.Derived;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
    public class AccrualCalculation
    {
        public const bool AreAccrualInvoicesSaved = false;

        private User _user;
        private Division _division;
        private CalculationPeriod _accrualPeriod;
        private AccrualTypeEnum _accrualTypeEnum;
        private Boolean _includeInvoiceAudit;

        private Int16 _stage = 1;
        private Int16 _stages = 0;

        private DivisionAccrual _divisionAccrual;
        private ExtendedBindingList<Rebate> _rebates;
        private ExtendedBindingList<ExceptionType> _exceptionTypes;
        private ExtendedBindingList<Hierarchy> _hierarchys;
        private ExtendedBindingList<RebateType> _rebateTypes;
        private ExtendedBindingList<Accrual> _accruals;
        //private List<AccrualJDE> _accrualJDEs = new List<AccrualJDE>();
        private Type _invoiceType = typeof(InvoiceLine);
        private DateTime? _lastRebateAmendTime = null;
        private Decimal _totalAccrued = 0;

        private Int32 _processed = 0;
        private Int32 _recordsToSave = 0;
        private Int32 _saved = 0;
        private Int32 _insertSequence = 0;
        private Int32 _lastInsertSequence = 0;
        private CalculationPeriod _forecastPeriod = null;

        private Boolean _isBackgroundMode = false;
        public Boolean IsBackgroundMode { get { return _isBackgroundMode; } set { _isBackgroundMode = value; } }
        public Int32 _maximumThreads = 10;
        public Int32 MaximumThreads { get { return _maximumThreads; } set { _maximumThreads = value; } }
        private Int32 _divisionAccrualId = 0;

        #region RebateProcessed Event

        private Hashtable _delegateStore = new Hashtable();
        private static Object _rebateProcessedEventKey = new Object();

        public delegate void RebateProcessedHandler(object sender, EventArgs e);

        public event RebateProcessedHandler RebateProcessed
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            add
            {
                _delegateStore[_rebateProcessedEventKey] = Delegate.Combine((Delegate) _delegateStore[_rebateProcessedEventKey], value);
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            remove
            {
                _delegateStore[_rebateProcessedEventKey] = Delegate.Remove((Delegate) _delegateStore[_rebateProcessedEventKey], value);
            }
        }

        protected void OnRebateProcessed()
        {
            RebateProcessedHandler rebateProcessedHandler = (RebateProcessedHandler) _delegateStore[_rebateProcessedEventKey];

            if (rebateProcessedHandler != null)
            {
                rebateProcessedHandler(this, null);
            }
        }

        #endregion

        public DivisionAccrual DivisionAccrual
        {
            get { return _divisionAccrual; }
        }

        private Int32 Processed
        {
            get
            {
                return _processed;
            }
            set
            {
                _processed = value;
                OnRebateProcessed();
            }
        }

        private Int32 Saved
        {
            get
            {
                return _saved;
            }
            set
            {
                _saved = value;
                OnRebateProcessed();
            }
        }

        public Int16 Stage
        {
            get
            {
                return _stage;
            }
            set
            {
                _stage = value;
                _saved = 0;
                _processed = 0;
                OnRebateProcessed();
            }
        }

        public String StageText
        {
            get
            {
                String text = String.Format("Stage {0} of {1}: ", _stage, _stages);

                if (_accrualTypeEnum == AccrualTypeEnum.Forecast)
                {
                    if (_stage % 2 == 0)
                    {
                        text += "Save";
                    }
                    else
                    {
                        text += "Calculate";
                    }

                    text += " Forecast Accruals " + (_forecastPeriod == null ? "" : "For " + _forecastPeriod.ToString());
                }
                else
                {
                    int period;
                    int multipler = Stage / 2;

                    if (_stage % 2 == 0)
                    {
                        text += "Save";
                        period = multipler;
                    }
                    else
                    {
                        text += "Calculate";
                        period = multipler + 1;
                    }

                    text += " " + _accrualTypeEnum.ToString() + " Accruals for Period " + period.ToString("00");
                }

                return text;
            }
        }

        public Int32 StagePercentageComplete
        {
            get
            {
                Decimal percentage;

                if (_stage % 2 == 0)
                {
                    // Save
                    if (_recordsToSave == 0)
                    {
                        percentage = 0;
                    }
                    else
                    {
                        percentage = ((Decimal) this.Saved / (Decimal) _recordsToSave) * 100;
                    }
                }
                else
                {
                    // Calculate
                    if (_rebates == null || _rebates.Count == 0)
                    {
                        percentage = 0;
                    }
                    else
                    {
                        percentage = ((Decimal) this.Processed / (Decimal) _rebates.Count) * 100;
                    }
                }

                return (Int32) Math.Round(percentage, 0);
            }
        }

        public Int32 PercentComplete
        {
            get
            {
                if (_stage <= _stages)
                {
                    Decimal totalPercent = _stages * 100;
                    Decimal complete = ((_stage - 1) * 100) + this.StagePercentageComplete;
                    Decimal percent = (complete / totalPercent) * 100;

                    return (Int32) Math.Round(percent, 0);
                }
                else
                {
                    return 100;
                }
            }
        }

        #region Constructors

        public AccrualCalculation(User user, Division division, CalculationPeriod accrualPeriod, AccrualTypeEnum accrualTypeEnum, Boolean includeInvoiceAudit)
        {
            _user = user;
            _division = division;
            _accrualPeriod = accrualPeriod;
            _accrualTypeEnum = accrualTypeEnum;
            _includeInvoiceAudit = includeInvoiceAudit;
            this.Stage = 1;

            _stages = (Int16) (_accrualPeriod.Period * 2);

            // Validate all data
            if (_user == null)
            {
                throw new ArgumentNullException("User");
            }

            if (_division == null)
            {
                throw new ArgumentNullException("Division");
            }

            if (_accrualPeriod == null)
            {
                throw new ArgumentNullException("AccrualPeriod");
            }

            if (_accrualTypeEnum != AccrualTypeEnum.Actual && _accrualTypeEnum != AccrualTypeEnum.Forecast)
            {
                throw new ArgumentOutOfRangeException("Accrual Calculations can only be performed for Forecast and Actuals");
            }
        }

        #endregion

        #region Business Logic

        public bool AutoFinalise(int maxYear, int maxPeriod)
        {
            bool autoFinalise = false;

            if (_divisionAccrual != null)
            {
                if (_divisionAccrual.Year <= maxYear && _divisionAccrual.Period <= maxPeriod) autoFinalise = true;

                if (autoFinalise)
                {
                    Logger.Instance.Debug("Auto finalising period");

                    _divisionAccrual.IsDraft = true;
                    _divisionAccrual.SetColumnDirty(DivisionAccrualProperty.IsDraft);

                    int result = _divisionAccrual.AcceptChanges();

                    return (result > 0);
                }
            }

            return false;
        }

        public Boolean Calculate()
        {
            Boolean isComplete = false;

            this.Stage = 1;
            _insertSequence = 0;
            _lastInsertSequence = 0;

            // Open persistent connection for performance reasons
            DataAccessServer.NewPersistentConnection();

            // Get the calculation date range
            DateTime fromDate = DateTime.MinValue;
            DateTime toDate = DateTime.MinValue;
            ForecastRange forecastRange = null;

            if (_accrualTypeEnum == AccrualTypeEnum.Actual)
            {
                _accrualPeriod.CalculationYearRange(out fromDate, out toDate);
            }
            else if (_accrualTypeEnum == AccrualTypeEnum.Forecast)
            {
                forecastRange = ForecastRange.ReadSingle();

                if (forecastRange.StartDate.HasValue && forecastRange.EndDate.HasValue)
                {
                    fromDate = new DateTime(_accrualPeriod.Year, 1, 1);
                    toDate = forecastRange.EndDate.Value;

                    if (fromDate > toDate)
                    {
                        fromDate = DateTime.MinValue;
                    }
                    else
                    {
                        _stages = (Int16) (CalculationPeriod.PeriodCount(forecastRange.StartDate.Value, toDate) * 2);
                        this.Stage = 1;
                    }
                }
            }

            // Process
            if (fromDate != DateTime.MinValue)
            {
                // Load related data
                ExceptionType exceptionType = new ExceptionType();
                _exceptionTypes = (ExtendedBindingList<ExceptionType>) exceptionType.Read();

                Hierarchy hierarchy = new Hierarchy();
                hierarchy.DivisionId = _division.Id;
                hierarchy.AddReadFilter(HierarchyColumn.DivisionId, "=");
                hierarchy.OrderBy = HierarchyColumn.Level;
                _hierarchys = (ExtendedBindingList<Hierarchy>) hierarchy.Read();

                RebateType rebateType = new RebateType();
                _rebateTypes = (ExtendedBindingList<RebateType>) rebateType.Read();

                if (_accrualTypeEnum == AccrualTypeEnum.Actual)
                {
                    // Find last rebate amend time
                    RebateLastAmended rebateLastAmended = new RebateLastAmended(_accrualPeriod);
                    _lastRebateAmendTime = rebateLastAmended.LastAmended;
                }

                // Load relevant rebates
                Rebate rebate = new Rebate();

                if (_accrualTypeEnum == AccrualTypeEnum.Actual)
                {
                    rebate.ReadFilter =
                        "{" + RebateProperty.StartDate + "} >= " + rebate.DBDateText(fromDate) + " and " +
                        "{" + RebateProperty.StartDate + "} <= " + rebate.DBDateText(toDate);

                    // Exlude forecast only rebates when creating actual accruals
                    rebate.ReadFilter += " and {" + RebateProperty.IsForecast + "} = 0";
                }
                else if (_accrualTypeEnum == AccrualTypeEnum.Forecast)
                {
                    rebate.ReadFilter =
                        "{" + RebateProperty.StartDate + "} <= " + rebate.DBDateText(fromDate) + " and " +
                        "{" + RebateProperty.StartDate + "} <= " + rebate.DBDateText(toDate);
                }
                else
                {
                    throw new ArgumentException("Invalid AccrualTypeEnum specified");
                }

                // Order rebates by Hierarchy Id, and Hierarchy Entity Id, in order that we can reduce the number
                // of reads we perform from the invoice table when processing periods
                rebate.OrderBy = "{" + RebateProperty.HierarchyId + "}, {" + RebateProperty.HierarchyEntityId + "}";

                _rebates = (ExtendedBindingList<Rebate>) rebate.Read();

                // Populate child collections
                _rebates.ReadEntityCollectionDetails();

                // Enumerate and calculate for each period in the accrual range (we need to start with a negative posting
                // of everything that has been posted to JDE to date, and add the accruals back in, if nothing has changed the
                // net effect will be nothing, but this must be done to ensure that we reverse out any invoices which may
                // no longer be included in a rebate due to the exclusion rules being changed)
                _accruals = new ExtendedBindingList<Accrual>();

                // Close persistent connection
                DataAccessServer.TerminatePersistentConnection();

                if (_rebates == null || _rebates.Count == 0)
                {
                    if (this.IsBackgroundMode)
                    {
                        Logger.Instance.Information("No rebates found to process");
                    }
                    else
                    {
                        MessageBox.Show("No rebates found to process", _accrualTypeEnum.ToString() + " Accrual Calculations", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    // Process accruals for periods
                    if (_accrualTypeEnum == AccrualTypeEnum.Actual)
                    {
                        bool isFirstPeriod = true;

                        foreach (CalculationPeriod period in _accrualPeriod.Enumerate())
                        {
                            if (this.IsBackgroundMode)
                                Logger.Instance.Information("Calculate rebates for period " + period.ToString());

                            CalculateSinglePeriod(period);

                            // Generate AccrualBI information for Period (Note: _accruals only holds a single periods worth
                            // of data as it is cleared during SaveAccruals)
                            List<AccrualBIWithInvoices> accrualBIs = GenerateAccrualBIData(period);

                            // Save Accruals
                            if (this.IsBackgroundMode)
                                Logger.Instance.Information("Saving rebates for period " + period.ToString());

                            isComplete = SaveAccruals(isFirstPeriod, period.Equals(_accrualPeriod), accrualBIs);
                            if (!isComplete) break;

                            isFirstPeriod = false;
                        }
                    }
                    else if (_accrualTypeEnum == AccrualTypeEnum.Forecast)
                    {
                        Boolean isFirstPeriod = true;

                        // For forecast accruals we use the from and to dates which we have already evaluated
                        // and loop thru every period which falls in between these dates
                        foreach (CalculationPeriod period in CalculationPeriod.Enumerate(forecastRange.StartDate.Value, forecastRange.EndDate.Value))
                        {
                            _forecastPeriod = period;
                            CalculateSinglePeriod(period);

                            if (this.IsBackgroundMode)
                                Logger.Instance.Information("Saving rebates for period " + period.ToString());

                            isComplete = SaveAccruals(isFirstPeriod, false, null);
                            isFirstPeriod = false;

                            if (!isComplete)
                            {
                                break;
                            }

                            _accruals = new ExtendedBindingList<Accrual>();
                        }
                    }
                }
            }

            // Clean up persistent connection if it has been left open
            DataAccessServer.TerminatePersistentConnection();

            return isComplete;
        }

        private void CalculateSinglePeriod(CalculationPeriod period)
        {
            Boolean isAccruals = false;
            Int32 hierarchyId = -1;
            Int32 hierarchyEntityId = -1;
            ExtendedBindingList<InvoiceLine> invoices = null;
            IEnumerable<InvoiceLine> matchingInvoices = null;
            ExtendedBindingList<Accrual> previousAccruals = null;

            // Determine date range for the accrual calculation period
            DateTime fromDate;
            DateTime toDate;

            period.CalculationPeriodRange(out fromDate, out toDate);

            // Read invoices
            InvoiceLine invoice = null;

            if (_accrualTypeEnum == AccrualTypeEnum.Actual)
            {
                invoice = new InvoiceLine();
            }
            else if (_accrualTypeEnum == AccrualTypeEnum.Forecast)
            {
                invoice = new InvoiceLine(DBRebates.ForecastLine);
            }
            else
            {
                throw new ArgumentException("Invalid AccrualTypeEnum specified");
            }

            invoice.ReadFilter =
                InvoiceLineColumn.Date + " >= " + invoice.DBDateText(fromDate) + " and " +
                InvoiceLineColumn.Date + " <= " + invoice.DBDateText(toDate);

            // Initiate invoice read on a background worker thread
            Func<IBindingList> invoiceReadThread = invoice.Read;
            IAsyncResult result = invoiceReadThread.BeginInvoke(null, null);

            // Read previous Accruals if required (also on background worker thread for
            // performance)
            if (_accrualTypeEnum == AccrualTypeEnum.Actual && !period.Equals(_accrualPeriod))
            {
                Func<CalculationPeriod, ExtendedBindingList<Accrual>> previousAccrualsThread = ReadPreviousAccruals;
                IAsyncResult previousAccrualsResult = previousAccrualsThread.BeginInvoke(period, null, null);

                // Await results of accruals read
                previousAccruals = previousAccrualsThread.EndInvoke(previousAccrualsResult);
            }

            // Await results of invoice read
            invoices = (ExtendedBindingList<InvoiceLine>) invoiceReadThread.EndInvoke(result);

            // Process for each Rebate
            this.Processed = 0;

            foreach (Rebate rebate in _rebates)
            {
                if (this.IsBackgroundMode)
                    Logger.Instance.Debug("Calculate Rebate Id " + rebate.Id.ToString());

                Boolean processRebate = true;

                // Check to see if we need to amend the rebate start and end dates for forecast accruals, as we
                // have to assume that the current rebates will be rolled over if the forecast invoice details are
                // in a future year
                if (_accrualTypeEnum == AccrualTypeEnum.Forecast)
                {
                    if ((rebate.EndDate == null || rebate.EndDate.Value.Year >= period.Year) && rebate.StartDate.Year != period.Year)
                    {
                        rebate.StartDate = new DateTime(period.Year, rebate.StartDate.Month, rebate.StartDate.Day);
                    }
                }

                // Check if rebate is applicable
                if (rebate.StartDate > fromDate || (rebate.EndDate.HasValue && rebate.EndDate < fromDate))
                {
                    // Rebate has not started, or already ended. Only process if the accrued value is non-zero (this happens if the start
                    // or end dates have been changed since after a previous accruals run)
                    Decimal accruedValue = (previousAccruals == null ? 0 : previousAccruals.Where(a => a.RebateId == rebate.Id).Sum(a => a.Value));
                    processRebate = (accruedValue != 0);
                }

                if (processRebate)
                {
                    // Read invoices within this date range
                    Hierarchy hierarchy = _hierarchys.Find(rebate.HierarchyId);
                    RebateType rebateType = _rebateTypes.Find(rebate.RebateTypeId);

                    if (rebate.HierarchyId != hierarchyId || hierarchyEntityId != rebate.HierarchyEntityId)
                    {
                        // Change of details will need to read matching invoices
                        hierarchyId = rebate.HierarchyId;
                        hierarchyEntityId = rebate.HierarchyEntityId;

                        // Read matching invoices
                        switch (hierarchy.InvoiceLinePropertyName)
                        {
                            case InvoiceLineProperty.CustomerGroupId:
                                matchingInvoices = invoices.Where(i => i.CustomerGroupId == rebate.HierarchyEntityId);
                                break;
                            case InvoiceLineProperty.SoldToId:
                                matchingInvoices = invoices.Where(i => i.SoldToId == rebate.HierarchyEntityId);
                                break;
                            case InvoiceLineProperty.ShipToId:
                                matchingInvoices = invoices.Where(i => i.ShipToId == rebate.HierarchyEntityId);
                                break;
                            default:
                                throw new ArgumentException("Unknown invoice property name in invoice match");
                        }
                    }

                    // Create accrual record stub for accumulation of values
                    Accrual accrual = new Accrual();

                    accrual.Id = 0;
                    accrual.RebateId = rebate.Id;
                    accrual.DivisionAccrualId = 0;
                    accrual.CreateYear = _accrualPeriod.Year;
                    accrual.CreatePeriod = _accrualPeriod.Period;
                    accrual.ActualYear = period.Year;
                    accrual.ActualPeriod = period.Period;
                    accrual.Value = 0;
                    accrual.Rate = null;
                    accrual.SalesVolume = 0;
                    accrual.SalesValue = 0;
                    accrual.InterfaceTime = null;
                    accrual.CreateUserId = _user.Id;
                    accrual.CreateTime = DateTime.Now;
                    accrual.InsertSequence = ++_insertSequence;

                    // Details required for AccrualBI calculations
                    accrual.RebateTypeId = rebate.RebateTypeId;
                    accrual.RebateStartDate = rebate.StartDate;
                    accrual.RebateEndDate = (rebate.EndDate.HasValue ? rebate.EndDate.Value : new DateTime(period.Year, 12, 31));

                    isAccruals = true;

                    // Process matching invoices and accumulate invoice quantities and values for
                    // matching rebate values (at the lowest possible hierarchy level)
                    foreach (InvoiceLine processInvoice in matchingInvoices)
                    {
                        if (IsInvoiceApplicable(processInvoice, rebate, rebateType))
                        {
                            if (rebate.IsGlobalProduct && !rebateType.IsRebateExtension)
                            {
                                // Global product rebate, accruals can only be done at the rebate level
                                accrual.SalesVolume += processInvoice.Quantity;
                                accrual.SalesValue += processInvoice.Amount;
                                accrual.Rate = rebate.GlobalRate;

                                // Create accrual invoice line
                                AccrualInvoice accrualInvoice = new AccrualInvoice();

                                accrualInvoice.AccrualId = 0;
                                accrualInvoice.InvoiceId = processInvoice.Id;

                                // Set values required to generate AccrualBI breakdown
                                accrualInvoice.SoldToId = invoice.SoldToId;
                                accrualInvoice.SiteId = invoice.SiteId;
                                accrualInvoice.ShipToId = invoice.ShipToId;
                                accrualInvoice.ProductId = invoice.ProductId;
                                accrualInvoice.DeliveryTypeEnum = invoice.DeliveryTypeEnum;

                                accrual.AccrualInvoices.Add(accrualInvoice);
                            }
                            else
                            {
                                if (rebateType.IsDeliveryWindow)
                                {
                                    ProcessDeliveryWindow(processInvoice, rebate, hierarchy, accrual);
                                }
                                else if (rebateType.IsLeadTime)
                                {
                                    ProcessLeadTime(processInvoice, rebate, hierarchy, accrual);
                                }
                                else
                                {
                                    ProcessRebateDetail(processInvoice, rebate, hierarchy, accrual);
                                }
                            }

                            //if (_accrualTypeEnum == AccrualTypeEnum.Actual)
                            //{
                            //	// Now create JDE accruals breakdown, we will store either sales value or quantity (dependent
                            //	// on rebate type) and use this later to apportion the total accrual value across the JDE
                            //	// breakdown once the accrual amount has been calculated
                            //	SetupAccrualJDE(accrual, rebateType, processInvoice.ProductId, processInvoice.SoldToId, processInvoice.SiteId, processInvoice.Quantity, processInvoice.Amount, false);
                            //}
                        }
                    }

                    // Now create the relevant rebate type calculation object and assign all the properties
                    RebateTypeCalculator rebateTypeCalculator = rebateType.GetCalculator(rebate);

                    rebateTypeCalculator.CalculationPeriod = _accrualPeriod;
                    rebateTypeCalculator.Period = period;
                    rebateTypeCalculator.Hierarchys = _hierarchys;
                    rebateTypeCalculator.Accrual = accrual;

                    // Finally perform the calculation
                    if (rebate.IsGlobalProduct)
                    {
                        rebateTypeCalculator.CalculateGlobalProduct();
                    }
                    else
                    {
                        rebateTypeCalculator.CalculateDetail();
                    }

                    if (_accrualTypeEnum == AccrualTypeEnum.Actual)
                    {
                        if (_accrualPeriod.Equals(period))
                        {
                            if (rebateTypeCalculator.IsSuspendedCalculation)
                            {
                                Int32 month = rebate.StartDate.Month;

                                for (Int32 index = 0; month < period.Period; index++)
                                {
                                    Accrual periodAccrual = rebate.Accruals[index];
                                    List<Accrual> postedAccruals = rebate.PostedAccruals[index];

                                    CheckAndCreateReversal(periodAccrual, postedAccruals, rebateType);
                                    month++;
                                }
                            }

                            _accruals.Add(accrual);
                            //ApportionJDEAccrual(accrual.AccrualJDEs, accrual.Value, false);
                        }
                        else
                        {
                            // Finally compare against existing accrual for the period (if it exists) and
                            // decide if we need to create an adjustment or not
                            List<Accrual> postedAccruals = previousAccruals.Where(pa => pa.RebateId == rebate.Id).ToList();

                            if (rebateTypeCalculator.IsSuspendedCalculation)
                            {
                                rebate.PostedAccruals.Add(postedAccruals);
                            }
                            else
                            {
                                CheckAndCreateReversal(accrual, postedAccruals, rebateType);
                            }
                        }
                    }
                    else if (_accrualTypeEnum == AccrualTypeEnum.Forecast)
                    {
                        _accruals.Add(accrual);
                    }
                }

                this.Processed++;
            }

            // Post zero accrual for period if nothing to accrue
            if (!isAccruals)
            {
                Accrual accrual = new Accrual();

                accrual.Id = 0;
                accrual.RebateId = null;
                accrual.DivisionAccrualId = 0;
                accrual.CreateYear = _accrualPeriod.Year;
                accrual.CreatePeriod = _accrualPeriod.Period;
                accrual.ActualYear = period.Year;
                accrual.ActualPeriod = period.Period;
                accrual.Value = 0;
                accrual.Rate = null;
                accrual.SalesVolume = 0;
                accrual.SalesValue = 0;
                accrual.InterfaceTime = null;
                accrual.CreateUserId = _user.Id;
                accrual.CreateTime = DateTime.Now;
                accrual.InsertSequence = ++_insertSequence;

                _accruals.Add(accrual);
            }

            this.Stage++;

            // Perform clean up to release memory
            invoices = null;
            matchingInvoices = null;

            if (previousAccruals != null)
            {
                previousAccruals.Clear();
                previousAccruals = null;
            }

            GC.Collect();
        }

        //private void SetupAccrualJDE(Accrual accrual, RebateType rebateType, int productId, int soldToId, int siteId, decimal quantity, decimal amount, Boolean isReversal)
        //{
        //	AccrualJDE accrualJDE = accrual.AccrualJDEs.FirstOrDefault(a => a.ProductId == productId && a.SiteId == siteId && a.SoldToId == soldToId && a.IsReversal == isReversal);

        //	if (accrualJDE == null)
        //	{
        //		accrualJDE = new AccrualJDE();

        //		accrualJDE.ProductId = productId;
        //		accrualJDE.SiteId = siteId;
        //		accrualJDE.SoldToId = soldToId;
        //		accrualJDE.Amount = 0;
        //		accrualJDE.IsReversal = isReversal;

        //		accrual.AccrualJDEs.Add(accrualJDE);
        //	}

        //	if (rebateType.IsValueBasedCalculation)
        //	{
        //		accrualJDE.Amount += amount;
        //	}
        //	else
        //	{
        //		accrualJDE.Amount += quantity;
        //	}
        //}

        private void CheckAndCreateReversal(Accrual accrual, List<Accrual> postedAccruals, RebateType rebateType)
        {
            if (postedAccruals != null)
            {
                Decimal accrualValuePosted = postedAccruals.Sum(a => a.Value);

                if (accrualValuePosted != accrual.Value)
                {
                    if (accrualValuePosted != 0 && postedAccruals.Count > 0)
                    {
                        // Reverse the last accrual details we have read and reset the flags as if its a new
                        // accrual
                        postedAccruals.Last().ReverseAccrualAndReset(_accrualPeriod, ref _insertSequence);

                        // Add reversal
                        _accruals.Add(postedAccruals.Last());

                        // Add new accrual details to the main collection
                        accrual.InsertSequence = ++_insertSequence;

                        foreach (AccrualDetail accrualDetail in accrual.AccrualDetails)
                        {
                            accrualDetail.InsertSequence = ++_insertSequence;
                        }

                        _accruals.Add(accrual);
                    }
                    else
                    {
                        _accruals.Add(accrual);
                    }

                    //// Create reversal entries in AccrualJDE to ensure that we always reverse correctly
                    //// even if the invoice matching has changed
                    //if (postedAccruals != null && postedAccruals.Count > 0)
                    //{
                    //	foreach (AccrualDetail postedAccrualDetail in postedAccruals.Last().AccrualDetails)
                    //	{
                    //		foreach (AccrualDetailInvoice postedAccrualDetailInvoice in postedAccrualDetail.AccrualDetailInvoices)
                    //		{
                    //			SetupAccrualJDE(accrual, rebateType, postedAccrualDetailInvoice.ProductId, postedAccrualDetailInvoice.SoldToId, postedAccrualDetailInvoice.SiteId, postedAccrualDetailInvoice.InvoiceQuantity, postedAccrualDetailInvoice.InvoiceAmount, true);
                    //		}
                    //	}

                    //	ApportionJDEAccrual(accrual.AccrualJDEs, accrualValuePosted * -1, true);
                    //}

                    //// Now assign the adjustment to the accrual JDE records
                    //ApportionJDEAccrual(accrual.AccrualJDEs, accrual.Value, false);
                }
            }
        }

        private ExtendedBindingList<Accrual> ReadPreviousAccruals(CalculationPeriod period)
        {
            Accrual accrual = new Accrual();

            accrual.ActualYear = period.Year;
            accrual.ActualPeriod = period.Period;
            accrual.IsDraft = false;
            accrual.AccrualTypeEnum = _accrualTypeEnum;

            accrual.AddReadFilter(AccrualProperty.ActualYear, "=");
            accrual.AddReadFilter(AccrualProperty.ActualPeriod, "=");
            accrual.AddReadFilter(AccrualProperty.IsDraft, "=");
            accrual.AddReadFilter(AccrualProperty.AccrualTypeEnum, "=");

            // Additional condition to ensure some old data which still exists (and has been moved to year 0001) is not
            // picked up
            accrual.ReadFilter += " and DivisionAccrual.Year = " + period.Year;

            // Order previous accruals in the order in which they should take effect (reversals posted
            // before new record)
            accrual.OrderBy =
                AccrualColumn.CreateYear + ", " +
                AccrualColumn.CreatePeriod + ", " +
                AccrualColumn.ActualPeriod + ", " +
                AccrualColumn.InsertSequence;

            return (ExtendedBindingList<Accrual>) accrual.Read();
        }

        private Boolean IsInvoiceApplicable(InvoiceLine invoice, Rebate rebate, RebateType rebateType)
        {
            // Check invoice date
            if (rebate.StartDate > invoice.Date || rebate.EndDate < invoice.Date)
            {
                return false;
            }

            // Check delivery type if required
            if (rebate.DeliveryTypeEnum != DeliveryTypeEnum.Null && invoice.DeliveryTypeEnum != rebate.DeliveryTypeEnum)
            {
                return false;
            }

            // Check if we need to filter on supply location
            if (rebate.SiteId != null && rebate.SiteId.Value != invoice.SiteId)
            {
                return false;
            }

            // Apply pricing sub-group criteria if required
            if (rebate.PricingSubGroupId.HasValue && invoice.PricingSubGroupId != rebate.PricingSubGroupId)
            {
                return false;
            }

            // Check if the invoice is applicable to this rebates rebate type
            if ((rebateType.IsValueBasedCalculation && invoice.IsApplicableValue) ||
                (!rebateType.IsValueBasedCalculation && invoice.IsApplicableQuantity))
            {
                // Check that this invoice is not excluded via the rebate exceptions (for
                // example sold from exceptions)
                foreach (RebateException rebateException in rebate.RebateExceptions)
                {
                    ExceptionType exceptionType = ExceptionType.Find(rebateException.ExceptionTypeEnum, _exceptionTypes);

                    if (exceptionType == null)
                    {
                        throw new DataException("Rebate Exception: Invalid exception type enum found (" + rebateException.ExceptionTypeEnum.ToString() + ")");
                    }

                    // Determine invoices field to compare
                    PropertyInfo propertyInfo = invoice.GetType().GetProperty(exceptionType.InvoiceLinePropertyName);

                    if (propertyInfo == null)
                    {
                        throw new DataException("Rebate Exception: Invalid invoice property name found (" + exceptionType.InvoiceLinePropertyName + ")");
                    }

                    // Finally compare invoice to rebate exception details
                    Int32 invoiceExceptionId = (Int32) propertyInfo.GetValue(invoice, null);

                    if (rebateException.ExceptionId == invoiceExceptionId)
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }

            // May need to exclude the invoice by delivery window
            if (rebateType.IsDeliveryWindow)
            {
                if (invoice.DeliveryWindowId.HasValue && invoice.PackingTypeEnum != PackingTypeEnum.Null)
                {
                    return (rebate.RebateDeliveryWindows.Count(rdw => rdw.DeliveryWindowId == invoice.DeliveryWindowId && rdw.PackingTypeEnum == invoice.PackingTypeEnum) > 0);
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        private void ProcessDeliveryWindow(InvoiceLine invoice, Rebate rebate, Hierarchy hierarchy, Accrual accrual)
        {
            RebateDeliveryWindow rebateDeliveryWindow = rebate.RebateDeliveryWindows.FirstOrDefault(rdw => rdw.DeliveryWindowId == invoice.DeliveryWindowId && rdw.PackingTypeEnum == invoice.PackingTypeEnum);

            if (rebateDeliveryWindow != null)
            {
                foreach (Hierarchy hierarchyBreakdown in _hierarchys)
                {
                    Int32 hierarchyEntityId = (Int32) _invoiceType.GetProperty(hierarchyBreakdown.InvoiceLinePropertyName).GetValue(invoice, null);
                    Int32? parentHierarchyEntityId = null;
                    Hierarchy parentHierarchy = _hierarchys.GetParent(hierarchyBreakdown.Level);

                    if (parentHierarchy != null)
                    {
                        parentHierarchyEntityId = (Int32) _invoiceType.GetProperty(parentHierarchy.InvoiceLinePropertyName).GetValue(invoice, null);
                    }

                    if (hierarchyBreakdown.Level >= hierarchy.Level)
                    {
                        AccrualDetail accrualDetail = accrual.AccrualDetails.FindByRebateDeliveryWindowId(hierarchyBreakdown.Id, hierarchyEntityId, rebateDeliveryWindow.Id);

                        if (accrualDetail == null)
                        {
                            accrualDetail = new AccrualDetail();

                            accrualDetail.Id = 0;
                            accrualDetail.AccrualId = 0;
                            accrualDetail.RebateDeliveryWindowId = rebateDeliveryWindow.Id;
                            accrualDetail.HierarchyId = hierarchyBreakdown.Id;
                            accrualDetail.HierarchyLevel = hierarchyBreakdown.Level;
                            accrualDetail.HierarchyEntityId = hierarchyEntityId;
                            accrualDetail.ParentHierarchyId = (hierarchyBreakdown.Level == hierarchy.Level ? null : (Int32?) parentHierarchy.Id);
                            accrualDetail.ParentHierarchyEntityId = (hierarchyBreakdown.Level == hierarchy.Level ? null : parentHierarchyEntityId);
                            accrualDetail.Value = 0;
                            accrualDetail.Rate = rebateDeliveryWindow.Rate;
                            accrualDetail.SalesVolume = 0;
                            accrualDetail.SalesValue = 0;
                            accrualDetail.InsertSequence = ++_insertSequence;

                            accrual.AccrualDetails.Add(accrualDetail);
                        }

                        // Aggregate values
                        accrualDetail.SalesVolume += invoice.Quantity;
                        accrualDetail.SalesValue += invoice.Amount;

                        if (hierarchyBreakdown.Level == hierarchy.Level)
                        {
                            accrual.SalesVolume += invoice.Quantity;
                            accrual.SalesValue += invoice.Amount;
                        }

                        // Create invoice breakdown
                        if (_includeInvoiceAudit)
                        {
                            AccrualDetailInvoice accrualDetailInvoice = new AccrualDetailInvoice();

                            accrualDetailInvoice.Id = 0;
                            accrualDetailInvoice.AccrualDetailId = 0;
                            accrualDetailInvoice.InvoiceId = invoice.Id;

                            // Set values required to generate AccrualBI breakdown
                            accrualDetailInvoice.SoldToId = invoice.SoldToId;
                            accrualDetailInvoice.SiteId = invoice.SiteId;
                            accrualDetailInvoice.ShipToId = invoice.ShipToId;
                            accrualDetailInvoice.ProductId = invoice.ProductId;
                            accrualDetailInvoice.DeliveryTypeEnum = invoice.DeliveryTypeEnum;
                            accrualDetailInvoice.InvoiceQuantity = invoice.Quantity;

                            accrualDetail.AccrualDetailInvoices.Add(accrualDetailInvoice);
                        }
                    }
                }
            }
        }

        private void ProcessLeadTime(InvoiceLine invoice, Rebate rebate, Hierarchy hierarchy, Accrual accrual)
        {
            RebateLeadTime rebateLeadTime = rebate.RebateLeadTimes.FirstOrDefault(rdw => rdw.LeadTimeId == invoice.LeadTimeId && rdw.PackingTypeEnum == invoice.PackingTypeEnum);

            if (rebateLeadTime != null)
            {
                foreach (Hierarchy hierarchyBreakdown in _hierarchys)
                {
                    Int32 hierarchyEntityId = (Int32) _invoiceType.GetProperty(hierarchyBreakdown.InvoiceLinePropertyName).GetValue(invoice, null);
                    Int32? parentHierarchyEntityId = null;
                    Hierarchy parentHierarchy = _hierarchys.GetParent(hierarchyBreakdown.Level);

                    if (parentHierarchy != null)
                    {
                        parentHierarchyEntityId = (Int32) _invoiceType.GetProperty(parentHierarchy.InvoiceLinePropertyName).GetValue(invoice, null);
                    }

                    if (hierarchyBreakdown.Level >= hierarchy.Level)
                    {
                        AccrualDetail accrualDetail = accrual.AccrualDetails.FindByRebateLeadTimeId(hierarchyBreakdown.Id, hierarchyEntityId, rebateLeadTime.Id);

                        if (accrualDetail == null)
                        {
                            accrualDetail = new AccrualDetail();

                            accrualDetail.Id = 0;
                            accrualDetail.AccrualId = 0;
                            accrualDetail.RebateLeadTimeId = rebateLeadTime.Id;
                            accrualDetail.HierarchyId = hierarchyBreakdown.Id;
                            accrualDetail.HierarchyLevel = hierarchyBreakdown.Level;
                            accrualDetail.HierarchyEntityId = hierarchyEntityId;
                            accrualDetail.ParentHierarchyId = (hierarchyBreakdown.Level == hierarchy.Level ? null : (Int32?) parentHierarchy.Id);
                            accrualDetail.ParentHierarchyEntityId = (hierarchyBreakdown.Level == hierarchy.Level ? null : parentHierarchyEntityId);
                            accrualDetail.Value = 0;
                            accrualDetail.Rate = rebateLeadTime.Rate;
                            accrualDetail.SalesVolume = 0;
                            accrualDetail.SalesValue = 0;
                            accrualDetail.InsertSequence = ++_insertSequence;

                            accrual.AccrualDetails.Add(accrualDetail);
                        }

                        // Aggregate values
                        accrualDetail.SalesVolume += invoice.Quantity;
                        accrualDetail.SalesValue += invoice.Amount;

                        if (hierarchyBreakdown.Level == hierarchy.Level)
                        {
                            accrual.SalesVolume += invoice.Quantity;
                            accrual.SalesValue += invoice.Amount;
                        }

                        // Create invoice breakdown
                        if (_includeInvoiceAudit)
                        {
                            AccrualDetailInvoice accrualDetailInvoice = new AccrualDetailInvoice();

                            accrualDetailInvoice.Id = 0;
                            accrualDetailInvoice.AccrualDetailId = 0;
                            accrualDetailInvoice.InvoiceId = invoice.Id;

                            // Set values required to generate AccrualBI breakdown
                            accrualDetailInvoice.SoldToId = invoice.SoldToId;
                            accrualDetailInvoice.SiteId = invoice.SiteId;
                            accrualDetailInvoice.ShipToId = invoice.ShipToId;
                            accrualDetailInvoice.ProductId = invoice.ProductId;
                            accrualDetailInvoice.DeliveryTypeEnum = invoice.DeliveryTypeEnum;
                            accrualDetailInvoice.InvoiceQuantity = invoice.Quantity;

                            accrualDetail.AccrualDetailInvoices.Add(accrualDetailInvoice);
                        }
                    }
                }
            }
        }

        private void ProcessRebateDetail(InvoiceLine invoice, Rebate rebate, Hierarchy hierarchy, Accrual accrual)
        {
            // Product details exist against the rebate
            List<RebateDetail> matched = invoice.MatchToRebateDetail(rebate.RebateDetails);

            foreach (Hierarchy hierarchyBreakdown in _hierarchys)
            {
                Int32 hierarchyEntityId = (Int32) _invoiceType.GetProperty(hierarchyBreakdown.InvoiceLinePropertyName).GetValue(invoice, null);
                Int32? parentHierarchyEntityId = null;
                Hierarchy parentHierarchy = _hierarchys.GetParent(hierarchyBreakdown.Level);

                if (parentHierarchy != null)
                {
                    parentHierarchyEntityId = (Int32) _invoiceType.GetProperty(parentHierarchy.InvoiceLinePropertyName).GetValue(invoice, null);
                }

                if (hierarchyBreakdown.Level >= hierarchy.Level)
                {
                    foreach (RebateDetail rebateDetail in matched)
                    {
                        // Find the matching accrual detail breakdown
                        AccrualDetail accrualDetail = accrual.AccrualDetails.FindByRebateDetailId(hierarchyBreakdown.Id, hierarchyEntityId, rebateDetail.Id);

                        if (accrualDetail == null)
                        {
                            accrualDetail = new AccrualDetail();

                            accrualDetail.Id = 0;
                            accrualDetail.AccrualId = 0;
                            accrualDetail.RebateDetailId = rebateDetail.Id;
                            accrualDetail.HierarchyId = hierarchyBreakdown.Id;
                            accrualDetail.HierarchyLevel = hierarchyBreakdown.Level;
                            accrualDetail.HierarchyEntityId = hierarchyEntityId;
                            accrualDetail.ParentHierarchyId = (hierarchyBreakdown.Level == hierarchy.Level ? null : (Int32?) parentHierarchy.Id);
                            accrualDetail.ParentHierarchyEntityId = (hierarchyBreakdown.Level == hierarchy.Level ? null : parentHierarchyEntityId);
                            accrualDetail.Value = 0;
                            accrualDetail.Rate = rebateDetail.Rate;
                            accrualDetail.SalesVolume = 0;
                            accrualDetail.SalesValue = 0;
                            accrualDetail.InsertSequence = ++_insertSequence;

                            accrual.AccrualDetails.Add(accrualDetail);
                        }

                        // Aggregate values
                        accrualDetail.SalesVolume += invoice.Quantity;
                        accrualDetail.SalesValue += invoice.Amount;

                        if (hierarchyBreakdown.Level == hierarchy.Level)
                        {
                            accrual.SalesVolume += invoice.Quantity;
                            accrual.SalesValue += invoice.Amount;
                        }

                        // Create invoice breakdown
                        if (_includeInvoiceAudit)
                        {
                            AccrualDetailInvoice accrualDetailInvoice = new AccrualDetailInvoice();

                            accrualDetailInvoice.Id = 0;
                            accrualDetailInvoice.AccrualDetailId = 0;
                            accrualDetailInvoice.InvoiceId = invoice.Id;

                            // Set values required to generate AccrualBI breakdown
                            accrualDetailInvoice.SoldToId = invoice.SoldToId;
                            accrualDetailInvoice.SiteId = invoice.SiteId;
                            accrualDetailInvoice.ShipToId = invoice.ShipToId;
                            accrualDetailInvoice.ProductId = invoice.ProductId;
                            accrualDetailInvoice.DeliveryTypeEnum = invoice.DeliveryTypeEnum;
                            accrualDetailInvoice.InvoiceQuantity = invoice.Quantity;

                            accrualDetail.AccrualDetailInvoices.Add(accrualDetailInvoice);
                        }
                    }
                }
            }
        }

        //private void ApportionJDEAccrual(List<AccrualJDE> accrualJDEs, Decimal value, Boolean isReversal)
        //{
        //	if (accrualJDEs.Any(a => a.IsReversal == isReversal))
        //	{
        //		Decimal totalAmount = accrualJDEs.Where(a => a.IsReversal == isReversal).Sum(a => a.Amount);
        //		Decimal outstandingValue = value;
        //		AccrualJDE lastAccrualJDE = null;

        //		foreach (AccrualJDE accrualJDE in accrualJDEs.Where(a => a.IsReversal == isReversal))
        //		{
        //			Decimal apportionValue = 0;

        //			if (totalAmount != 0)
        //			{
        //				Decimal percentage = accrualJDE.Amount / totalAmount;
        //				apportionValue = Decimal.Round(value * percentage, 3);
        //			}

        //			accrualJDE.Value += apportionValue;
        //			outstandingValue -= apportionValue;

        //			lastAccrualJDE = accrualJDE;
        //		}

        //		// Finally prevent any round errors by assigning any left over amounts
        //		// to the final record
        //		lastAccrualJDE.Value += outstandingValue;

        //		// Finally combine these new AccrualJDE records into the global set for the whole
        //		// accrual run
        //		foreach (AccrualJDE accrualJDE in accrualJDEs.Where(a => a.IsReversal == isReversal))
        //		{
        //			AccrualJDE globalAccrualJDE = _accrualJDEs.FirstOrDefault(a => a.ProductId == accrualJDE.ProductId && a.SiteId == accrualJDE.SiteId && a.SoldToId == accrualJDE.SoldToId);

        //			if (globalAccrualJDE == null)
        //			{
        //				globalAccrualJDE = new AccrualJDE();

        //				globalAccrualJDE.ProductId = accrualJDE.ProductId;
        //				globalAccrualJDE.SiteId = accrualJDE.SiteId;
        //				globalAccrualJDE.SoldToId = accrualJDE.SoldToId;
        //				globalAccrualJDE.Value = 0;

        //				_accrualJDEs.Add(globalAccrualJDE);
        //			}

        //			globalAccrualJDE.Value += accrualJDE.Value;
        //		}
        //	}
        //}

        private Boolean SaveAccruals(bool isFirstPeriod, bool isFinalCalculationPeriod, List<AccrualBIWithInvoices> accrualBIs)
        {
            DateTime runTime = DateTime.Now;
            Boolean retry = false;
            String errorMessage = "";

            do
            {
                ExtendedBindingList<AccrualInvoice> allAccrualInvoices = new ExtendedBindingList<AccrualInvoice>();
                ExtendedBindingList<AccrualDetail> allAccrualDetails = new ExtendedBindingList<AccrualDetail>();
                ExtendedBindingList<AccrualDetailInvoice> allAccrualDetailInvoices = new ExtendedBindingList<AccrualDetailInvoice>();

                // Calculate records to save (including DivisionAccrual record)
                _recordsToSave = _accruals.Count;
                _recordsToSave += _accruals.Sum(a => a.AccrualDetails.Count);
                _recordsToSave += accrualBIs?.Count ?? 0;
                //_recordsToSave += (_accrualTypeEnum == AccrualTypeEnum.Actual && isFinalCalculationPeriod ? _accrualJDEs.Count : 0);

                if (AreAccrualInvoicesSaved)
                {
                    _recordsToSave += _accruals.Sum(a => a.AccrualInvoices.Count);
                    _recordsToSave += _accruals.Sum(a => a.AccrualDetails.Sum(ad => ad.AccrualDetailInvoices.Count));
                }

                if (isFirstPeriod)
                {
                    _recordsToSave++;
                }

                this.Saved = 0;

                // Now save
                try
                {
                    Int32 result = 0;

                    //_totalAccrued += _accruals.Sum(a => a.Value);

                    //if (isFinalCalculationPeriod && _accrualTypeEnum == AccrualTypeEnum.Actual)
                    //{
                    //	Decimal totalJDEAccrual = _accrualJDEs.Sum(a => a.Value);

                    //	if (_totalAccrued != totalJDEAccrual)
                    //	{
                    //		result = -1;
                    //		errorMessage = "Total accrual value does not equal total accrual JDE value";
                    //	}
                    //}

                    if (result >= 0 && isFirstPeriod)
                    {
                        DataAccessServer.NewPersistentConnection();
                        DataAccessServer.PersistentConnection.OpenConnection();
                        DataAccessServer.PersistentConnection.BeginTransaction();

                        // Clear entity collection first to ensure re-read incase anything has changed
                        _division.ClearEntityCollection();

                        foreach (DivisionAccrual divisionAccrual in _division.DivisionAccruals)
                        {
                            if ((_accrualTypeEnum == AccrualTypeEnum.Actual && divisionAccrual.IsDraft) ||
                                (_accrualTypeEnum == AccrualTypeEnum.Forecast && divisionAccrual.AccrualTypeEnum == AccrualTypeEnum.Forecast))
                            {
                                result = divisionAccrual.DeleteAccruals();

                                if (result < 0)
                                {
                                    errorMessage = "DeleteAccrual (" + result.ToString() + ")";
                                    break;
                                }
                            }
                        }

                        DataAccessServer.PersistentConnection.CommitTransaction();
                        DataAccessServer.TerminatePersistentConnection();
                    }

                    // Create new DivisionAccrual audit record
                    if (result >= 0 && isFirstPeriod)
                    {
                        _divisionAccrual = new DivisionAccrual();

                        _divisionAccrual.Id = 0;
                        _divisionAccrual.DivisionId = _division.Id;
                        _divisionAccrual.Year = _accrualPeriod.Year;
                        _divisionAccrual.Period = _accrualPeriod.Period;
                        _divisionAccrual.IsDraft = (_accrualTypeEnum == AccrualTypeEnum.Actual);
                        _divisionAccrual.UserId = _user.Id;
                        _divisionAccrual.RunTime = runTime;
                        _divisionAccrual.LastRebateAmendTime = _lastRebateAmendTime;
                        _divisionAccrual.AccrualTypeEnum = _accrualTypeEnum;

                        result = _divisionAccrual.AcceptChanges();

                        if (result > 0)
                        {
                            this.Saved++;
                        }
                        else
                        {
                            errorMessage = "DivisionAccrual (" + result.ToString() + ")";
                            result = -1;
                        }
                    }

                    // Now save new draft accruals
                    if (result >= 0)
                    {
                        // Assign division accrual id to my accruals records
                        foreach (Accrual accrual in _accruals)
                        {
                            accrual.DivisionAccrualId = _divisionAccrual.Id;
                        }

                        // Bulk insert accruals (for maximum performance especially on Oracle)
                        //result = _accruals.MultiThreadedBulkInsert(this.MaximumThreads);
                        result = _accruals.SqlBulkCopy();

                        if (result == _accruals.Count)
                        {
                            this.Saved += _accruals.Count;

                            // Now read back the accrual inserted accruals to get the Id's
                            AccrualInsertSequence accrualInsertSequence = new AccrualInsertSequence();
                            accrualInsertSequence.DivisionAccrualId = _divisionAccrual.Id;
                            accrualInsertSequence.AddReadFilter(AccrualProperty.DivisionAccrualId, "=");

                            accrualInsertSequence.InsertSequence = _lastInsertSequence;
                            accrualInsertSequence.AddReadFilter("InsertSequence", ">");

                            ExtendedBindingList<AccrualInsertSequence> accrualInsertSequences = (ExtendedBindingList<AccrualInsertSequence>) accrualInsertSequence.Read();

                            // Match insert sequences to get the actual database id into the Accrual object (which will
                            // in turn update the AccrualId on all associated AccrualDetail objects
                            _accruals.MatchInsertSequences(accrualInsertSequences);

                            // Build global lists of AccrualInvoice, AccrualDetail, and AccrualDetailInvoice to allow
                            // us to maximise the bulk insert performance
                            foreach (Accrual accrual in _accruals)
                            {
                                allAccrualInvoices.AddMultiple(accrual.AccrualInvoices);
                                allAccrualDetails.AddMultiple(accrual.AccrualDetails);

                                foreach (AccrualDetail accrualDetail in accrual.AccrualDetails)
                                {
                                    allAccrualDetailInvoices.AddMultiple(accrualDetail.AccrualDetailInvoices);
                                }
                            }
                        }
                        else
                        {
                            errorMessage = "Accrual (" + result.ToString() + ", " + _accruals.Count.ToString() + ")";
                            result = -1;
                        }
                    }

                    // Bulk insert AccrualInvoice
                    if (result >= 0 && AreAccrualInvoicesSaved)
                    {
                        //result = allAccrualInvoices.MultiThreadedBulkInsert(this.MaximumThreads);
                        result = allAccrualInvoices.SqlBulkCopy();

                        if (result == allAccrualInvoices.Count)
                        {
                            this.Saved += allAccrualInvoices.Count;
                        }
                        else
                        {
                            errorMessage = "AccrualInvoice (" + result.ToString() + ", " + allAccrualInvoices.Count.ToString() + ")"; ;
                            result = -1;
                        }
                    }

                    // Bulk insert AccrualDetail
                    if (result >= 0)
                    {
                        int count = allAccrualDetails.Count(ad => ad.AccrualId == 0);
                        //result = allAccrualDetails.MultiThreadedBulkInsert(this.MaximumThreads);
                        result = allAccrualDetails.SqlBulkCopy();

                        if (result == allAccrualDetails.Count)
                        {
                            this.Saved += allAccrualDetails.Count;

                            // Now read back the AccrualDetail records and match the sequence numbers
                            // to get the database id's
                            // Now read back the accrual inserted accruals to get the Id's
                            AccrualDetailInsertSequence accrualDetailInsertSequence = new AccrualDetailInsertSequence();
                            accrualDetailInsertSequence.DivisionAccrualId = _divisionAccrual.Id;
                            accrualDetailInsertSequence.AddReadFilter(AccrualProperty.DivisionAccrualId, "=");

                            accrualDetailInsertSequence.InsertSequence = _lastInsertSequence;
                            accrualDetailInsertSequence.AddReadFilter("InsertSequence", ">");

                            ExtendedBindingList<AccrualDetailInsertSequence> accrualDetailInsertSequences = (ExtendedBindingList<AccrualDetailInsertSequence>) accrualDetailInsertSequence.Read();

                            // Match insert sequences to get the actual database id into the Accrual object (which will
                            // inturn update the AccrualId on all associated AccrualDetail objects
                            allAccrualDetails.MatchInsertSequences(accrualDetailInsertSequences);
                        }
                        else
                        {
                            errorMessage = "AccrualDetail (" + result.ToString() + ", " + allAccrualDetails.Count.ToString() + ")";
                            result = -1;
                        }
                    }

                    // Bulk insert AccrualDetailInvoice
                    if (result >= 0 && AreAccrualInvoicesSaved)
                    {
                        //result = allAccrualDetailInvoices.MultiThreadedBulkInsert(this.MaximumThreads);
                        result = allAccrualDetailInvoices.SqlBulkCopy();

                        if (result == allAccrualDetailInvoices.Count)
                        {
                            this.Saved += allAccrualDetailInvoices.Count;
                        }
                        else
                        {
                            errorMessage = "AccrualDetailInvoice (" + result.ToString() + ", " + allAccrualDetailInvoices.Count.ToString() + ")";
                            result = -1;
                        }
                    }

                    // Bulk insert AccrualBIs
                    if (result >= 0 && accrualBIs != null)
                    {
                        accrualBIs.ForEach(abi =>
                            {
                                abi.DivisionAccrualId = _divisionAccrual.Id;
                                abi.UpdateDateTime = DateTime.Now;
                                abi.SaleQuantity = abi.InvoiceQuantities.Sum(iq => iq.Quantity);
                            });

                        result = accrualBIs.SqlBulkCopy();

                        if (result == accrualBIs.Count)
                        {
                            this.Saved += accrualBIs.Count;
                        }
                        else
                        {
                            errorMessage = "AccrualBI (" + result.ToString() + ", " + accrualBIs.Count.ToString() + ")";
                            result = -1;
                        }
                    }

                    //// Finally save Accrual JDE values
                    //if (result >= 0 && _accrualTypeEnum == AccrualTypeEnum.Actual && isFinalCalculationPeriod)
                    //{
                    //	foreach (AccrualJDE accrualJDE in _accrualJDEs)
                    //	{
                    //		accrualJDE.DivisionAccrualId = _divisionAccrual.Id;
                    //	}

                    //	//result = _accrualJDEs.MultiThreadedBulkInsert(this.MaximumThreads);
                    //	result = _accrualJDEs.SqlBulkCopy();

                    //	if (result == _accrualJDEs.Count)
                    //	{
                    //		this.Saved += _accrualJDEs.Count;
                    //	}
                    //	else
                    //	{
                    //		errorMessage = "AccrualJDE (" + result.ToString() + ", " + _accrualJDEs.Count.ToString() + ")";
                    //		result = -1;
                    //	}
                    //}

                    if (result >= 0 && String.IsNullOrEmpty(errorMessage))
                    {
                        _divisionAccrual.SetColumnClean();
                        _divisionAccrual.ExistsOnDatabase = true;
                    }
                    else
                    {
                        this.Stage = 1;
                    }
                }
                catch (DatabaseException databaseException)
                {
                    if (databaseException.IsLockError)
                    {
                        if (this.IsBackgroundMode)
                        {
                            retry = true;
                        }
                        else
                        {
                            if (MessageBox.Show("Database is locked, do you wish to retry ?", "Database Locked", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
                            {
                                retry = true;
                            }
                        }
                    }
                    else
                    {
                        errorMessage = databaseException.Message;
                    }
                }

                // Clean up
                if (allAccrualDetailInvoices != null)
                {
                    allAccrualDetailInvoices.Clear();
                    allAccrualDetailInvoices = null;
                }

                if (allAccrualDetails != null)
                {
                    allAccrualDetails.Clear();
                    allAccrualDetails = null;
                }

                if (allAccrualInvoices != null)
                {
                    allAccrualInvoices.Clear();
                    allAccrualInvoices = null;
                }

                GC.Collect();
            } while (retry);

            if (errorMessage.Length > 0)
            {
                if (this.IsBackgroundMode)
                {
                    Logger.Instance.Information("Failed to save changes (" + errorMessage + ")");
                }
                else
                {
                    MessageBox.Show("Failed to save changes\n" + errorMessage, "Save", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            _lastInsertSequence = _insertSequence;
            this.Stage++;

            if (_accruals != null)
            {
                _accruals.Clear();
                _accruals = null;
            }

            GC.Collect();

            _accruals = new ExtendedBindingList<Accrual>();
            return (errorMessage.Length == 0);
        }

        public static AccrualCalculationStatus CanFinalise(DivisionAccrual divisionAccrual)
        {
            AccrualCalculationStatus status = AccrualCalculationStatus.Fail;
            RebateLastAmended rebateAmended = new RebateLastAmended(divisionAccrual.CalculationPeriod);

            if (!rebateAmended.LastAmended.HasValue)
            {
                status = AccrualCalculationStatus.Ok;
            }
            else
            {
                if (!divisionAccrual.LastRebateAmendTime.HasValue || rebateAmended.LastAmended.Value != divisionAccrual.LastRebateAmendTime.Value)
                {
                    if (divisionAccrual.Period < 12)
                    {
                        status = AccrualCalculationStatus.Warn;
                    }
                }
                else
                {
                    status = AccrualCalculationStatus.Ok;
                }
            }

            return status;
        }

        public static Boolean Finalise(DivisionAccrual divisionAccrual)
        {
            Boolean isFinalised = false;

            if (divisionAccrual != null)
            {
                Boolean retry = false;
                DataAccessServer.NewPersistentConnection();

                do
                {
                    try
                    {
                        String error = "";

                        divisionAccrual.IsDraft = false;
                        divisionAccrual.AcceptChanges();

                        if (divisionAccrual.Period == 12)
                        {
                            CalculationPeriod accrualPeriod = new CalculationPeriod(divisionAccrual.Year, divisionAccrual.Period);

                            Rebate rebate = new Rebate();

                            DateTime fromDate = new DateTime(divisionAccrual.Year, 1, 1);
                            DateTime toDate = new DateTime(divisionAccrual.Year, 12, 31);

                            rebate.AddReadFilterBetween(RebateProperty.StartDate, fromDate, toDate);
                            ExtendedBindingList<Rebate> rebates = (ExtendedBindingList<Rebate>) rebate.Read();

                            foreach (Rebate r in rebates)
                            {
                                error = r.PeriodRollOver(accrualPeriod, divisionAccrual.UserId);

                                if (error != "")
                                {
                                    break;
                                }
                            }
                        }

                        isFinalised = (error == "");
                    }
                    catch (DatabaseException databaseException)
                    {
                        DataAccessServer.PersistentConnection.RollbackTransaction();

                        if (databaseException.IsLockError)
                        {
                            if (MessageBox.Show("Database is locked, do you wish to retry ?", "Database Locked", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
                            {
                                retry = true;
                            }
                        }
                    }
                    finally
                    {
                        if (isFinalised)
                        {
                            DataAccessServer.PersistentConnection.CommitTransaction();
                        }

                        DataAccessServer.TerminatePersistentConnection();
                    }
                } while (retry);
            }

            return isFinalised;
        }

        #endregion

        private List<AccrualBIWithInvoices> GenerateAccrualBIData(CalculationPeriod period)
        {
            var accrualBIs = new List<AccrualBIWithInvoices>();

            foreach (Accrual accrual in _accruals.Where(a => a.RebateId.HasValue))
            {
                if (accrual.AccrualInvoices.Any())
                {
                    if (accrual.SalesVolume.HasValue)
                    {
                        // Global Products rebate
                        foreach (AccrualInvoice accrualInvoice in accrual.AccrualInvoices)
                        {
                            var accrualBI = GetAccrualBi(
                                accrualBIs,
                                accrualInvoice.SoldToId,
                                accrualInvoice.SiteId,
                                accrualInvoice.ShipToId,
                                accrualInvoice.ProductId,
                                accrualInvoice.DeliveryTypeEnum,
                                accrual.RebateTypeId,
                                accrual.RebateStartDate,
                                accrual.RebateEndDate,
                                period);

                            accrualBI.SaleQuantity += accrualInvoice.InvoiceQuantity;
                            accrualBI.RebateAmount += (((1 / accrual.SalesVolume.Value) * accrualInvoice.InvoiceQuantity) / 100) * accrual.Value;

                            if (!accrualBI.InvoiceQuantities.Select(iq => iq.InvoiceLineId)
                                    .Contains(accrualInvoice.InvoiceId))
                            {
                                accrualBI.InvoiceQuantities.Add(new InvoiceQuantity(accrualInvoice.InvoiceId, accrualInvoice.InvoiceQuantity));
                            }
                        }
                    }
                }
                else if (accrual.AccrualDetails.Any())
                {
                    // Standard detail based rebate
                    foreach (AccrualDetail accrualDetail in accrual.AccrualDetails
                        .Where(ad => !ad.ParentHierarchyId.HasValue)
                        .Where( ad => !ad.ParentHierarchyEntityId.HasValue)
                        .Where(ad => ad.SalesVolume > 0))
                    {
                        AccrualBIWithInvoices accrualBI = null;
                        decimal totalAllocated = 0;

                        foreach (AccrualDetailInvoice accrualDetailInvoice in accrualDetail.AccrualDetailInvoices)
                        {
                            accrualBI = GetAccrualBi(
                                accrualBIs,
                                accrualDetailInvoice.SoldToId,
                                accrualDetailInvoice.SiteId,
                                accrualDetailInvoice.ShipToId,
                                accrualDetailInvoice.ProductId,
                                accrualDetailInvoice.DeliveryTypeEnum,
                                accrual.RebateTypeId,
                                accrual.RebateStartDate,
                                accrual.RebateEndDate,
                                period);

                            accrualBI.SaleQuantity += accrualDetailInvoice.InvoiceQuantity;

                            // Replace formula with identical mathmatical formula, but less prone to rounding errors
                            //decimal amount = Math.Round((((100 / accrualDetail.SalesVolume) * accrualDetailInvoice.InvoiceQuantity) / 100) * accrualDetail.Value, 2);
                            decimal amount =
                                Math.Round(
                                    accrualDetailInvoice.InvoiceQuantity*accrualDetail.Value/accrualDetail.SalesVolume,
                                    3);
                            accrualBI.RebateAmount += amount;
                            totalAllocated += amount;

                            if (!accrualBI.InvoiceQuantities.Select(iq => iq.InvoiceLineId)
                                    .Contains(accrualDetailInvoice.InvoiceId))
                            {
                                accrualBI.InvoiceQuantities.Add(new InvoiceQuantity(accrualDetailInvoice.InvoiceId, accrualDetailInvoice.InvoiceQuantity));
                            }
                        }

                        decimal difference = accrualDetail.Value - totalAllocated;

                        if (accrualBI != null && difference != 0)
                            accrualBI.RebateAmount += difference;
                    }
                }
            }

            return accrualBIs;
        }

        private AccrualBIWithInvoices GetAccrualBi(List<AccrualBIWithInvoices> accrualBis, int soldToId, int siteId, int shipToId, int productId, DeliveryTypeEnum deliveryTypeEnum, int rebateTypeId, DateTime rebateStartDate, DateTime rebateEndDate, CalculationPeriod period)
        {
            AccrualBIWithInvoices accrualBi = accrualBis.FirstOrDefault(a =>
                a.SoldToId == soldToId &&
                a.SiteId == siteId &&
                a.ShipToId == shipToId &&
                a.ProductId == productId &&
                a.DeliveryTypeEnum == deliveryTypeEnum &&
                a.RebateTypeId == rebateTypeId &&
                a.Year == period.Year &&
                a.Period == period.Period);

            if (accrualBi == null)
            {
                accrualBi = new AccrualBIWithInvoices()
                {
                    SoldToId = soldToId,
                    SiteId = siteId,
                    ShipToId = shipToId,
                    ProductId = productId,
                    DeliveryTypeEnum = deliveryTypeEnum,
                    RebateTypeId = rebateTypeId,
                    Year = period.Year,
                    Period = period.Period
                };

                accrualBis.Add(accrualBi);
            }

            return accrualBi;
        }
    }

    public enum AccrualCalculationStatus
    {
        Ok,
        Fail,
        Warn
    }
}
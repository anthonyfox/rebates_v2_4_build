﻿namespace Evolve.Clients.Lafarge.Cement.Business.Rebates.Derived
{
    public class InvoiceQuantity
    {        public InvoiceQuantity(int invoiceLineId, decimal quantity)
        {
            InvoiceLineId = invoiceLineId;
            Quantity = quantity;
        }
        public int InvoiceLineId { get;  }
        public decimal Quantity { get;  }
    }
}
﻿using System;
using System.ComponentModel;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class RebateExceptionCustomer : RebateException
	{
		#region Properties

		#region RELATED ENTITY PROPERTY: Code

		private string _jdeCode;
		[EntityColumn(RebateExceptionCustomerProperty.JDECode, DBRebates.Customer, CustomerColumn.JDECode), DisplayName("Account")]
		public string JDECode
		{
			get { return _jdeCode; }
			set
			{
				_jdeCode = value;
			}
		}

		#endregion

		#region RELATED ENTITY PROPERTY: Name

		private String _name;
		[EntityColumn(RebateExceptionCustomerProperty.Name, DBRebates.Customer, CustomerColumn.Name)]
		public String Name
		{
			get { return _name; }
			set
			{
				_name = value;
				SetColumnDirty("Name");
			}
		}

		#endregion

		#region RELATED ENTITY PROPERTY: Address1

		private String _address1;
		[EntityColumn(RebateExceptionCustomerProperty.Address1, DBRebates.Address, AddressColumn.Address1), DisplayName("Address 1")]
		public String Address1
		{
			get { return _address1; }
			set
			{
				_address1 = value;
				SetColumnDirty("Address1");
			}
		}

		#endregion

		#region RELATED ENTITY PROPERTY: Address2

		private String _address2;
		[EntityColumn(RebateExceptionCustomerProperty.Address2, DBRebates.Address, AddressColumn.Address2), DisplayName("Address 2")]
		public String Address2
		{
			get { return _address2; }
			set
			{
				_address2 = value;
				SetColumnDirty("Address2");
			}
		}

		#endregion

		#region RELATED ENTITY PROPERTY: Address3

		private String _address3;
		[EntityColumn(RebateExceptionCustomerProperty.Address3, DBRebates.Address, AddressColumn.Address3), DisplayName("Address 3")]
		public String Address3
		{
			get { return _address3; }
			set
			{
				_address3 = value;
				SetColumnDirty("Address3");
			}
		}

		#endregion

		#region RELATED ENTITY PROPERTY: Address4

		private String _address4;
		[EntityColumn(RebateExceptionCustomerProperty.Address4, DBRebates.Address, AddressColumn.Address4), DisplayName("Address 4")]
		public String Address4
		{
			get { return _address4; }
			set
			{
				_address4 = value;
				SetColumnDirty("Address4");
			}
		}

		#endregion

		#region RELATED ENTITY PROPERTY: City

		private String _city;
		[EntityColumn(RebateExceptionCustomerProperty.City, DBRebates.Address, AddressColumn.City), DisplayName("City")]
		public String City
		{
			get { return _city; }
			set
			{
				_city = value;
				SetColumnDirty("City");
			}
		}

		#endregion

		#region RELATED ENTITY PROPERTY: County

		private String _county;
		[EntityColumn(RebateExceptionCustomerProperty.County, DBRebates.Address, AddressColumn.County), DisplayName("County")]
		public String County
		{
			get { return _county; }
			set
			{
				_county = value;
				SetColumnDirty("County");
			}
		}

		#endregion

		#region RELATED ENTITY PROPERTY: PostCode

		private String _postCode;
		[EntityColumn(RebateExceptionCustomerProperty.PostCode, DBRebates.Address, AddressColumn.PostCode), DisplayName("Post Code")]
		public String PostCode
		{
			get { return _postCode; }
			set
			{
				_postCode = value;
				SetColumnDirty("PostCode");
			}
		}

		#endregion

		#endregion

		#region Constructors

		public RebateExceptionCustomer() : base()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			JoinTable(DBRebates.DatabaseName, DBRebates.Customer, DBRebates.Customer + "." + CustomerColumn.Id + " = " + DBRebates.RebateException + "." + RebateExceptionColumn.ExceptionId);
			JoinTable(DBRebates.DatabaseName, DBRebates.Address, DBRebates.Address + "." + AddressColumn.Id + " = " + DBRebates.Customer + "." + CustomerColumn.AddressId, EntityJoinType.LeftOuter);
		}

		#endregion
	}

	public class RebateExceptionCustomerColumn
	{
		public const string Id = "Id";
		public const string RebateId = "RebateId";
		public const string ExceptionTypeEnum = "ExceptionTypeEnum";
		public const string ExceptionId = "ExceptionId";
	}

	public class RebateExceptionCustomerProperty
	{
		public const string Id = "Id";
		public const string RebateId = "RebateId";
		public const string ExceptionTypeEnum = "ExceptionTypeEnum";
		public const string ExceptionId = "ExceptionId";
		public const string JDECode = "JDECode";
		public const string Name = "Name";
		public const string Address1 = "Address1";
		public const string Address2 = "Address2";
		public const string Address3 = "Address3";
		public const string Address4 = "Address4";
		public const string City = "City";
		public const string County = "County";
		public const string PostCode = "PostCode";
	}
}

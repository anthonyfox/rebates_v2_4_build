﻿using System.Collections.Generic;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates.Derived
{
    // ReSharper disable once InconsistentNaming
    public class AccrualBIWithInvoices : AccrualBI
    {
        public AccrualBIWithInvoices()
        {
            InvoiceQuantities = new List<InvoiceQuantity>();
        }
        public List<InvoiceQuantity> InvoiceQuantities { get; }
    }
}
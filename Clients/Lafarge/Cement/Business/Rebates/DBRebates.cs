﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evolve.Clients.Lafarge.Cement.Business.Rebates
{
	public class DBRebates
	{
		public static string DatabaseName = "";

		public const string Accrual = "Accrual";
		public const string AccrualBI = "AccrualBI";
		public const string AccrualDetail = "AccrualDetail";
		public const string AccrualDetailInvoice = "AccrualDetailInvoice";
		public const string AccrualInvoice = "AccrualInvoice";
		public const string AccrualJDE = "AccrualJDE";
		public const string Address = "Address";
		public const string Attribute = "Attribute";
	    public const string AuditHeader = "AuditHeader";
	    public const string AuditDetail = "AuditDetail";
		public const string Customer = "Customer";
		public const string CustomerGroup = "CustomerGroup";
        public const string CustomerGroupNameView = "CustomerGroupNameView";
        public const string DeliveryWindow = "DeliveryWindow";
		public const string Division = "Division";
		public const string DivisionAccrual = "DivisionAccrual";
		public const string ExceptionType = "ExceptionType";
		public const string Hierarchy = "Hierarchy";
		public const string InvoiceLine = "InvoiceLine";
		public const string ForecastLine = "ForecastLine";
		public const string LeadTime = "LeadTime";
		public const string Payee = "Payee";
		public const string Payment = "Payment";
		public const string PaymentBreakdown = "PaymentBreakdown";
		public const string Payment_PaymentBreakdown = "Payment_PaymentBreakdown";
		public const string PaymentFrequency = "PaymentFrequency";
		public const string PaymentFrequencyReminder = "PaymentFrequencyReminder";
		public const string PaymentStatus = "PaymentStatus";
		public const string PricingSubGroup = "PricingSubGroup";
		public const string Product = "Product";
		public const string Rebate = "Rebate";
		public const string RebateDeliveryWindow = "RebateDeliveryWindow";
		public const string RebateLeadTime = "RebateLeadTime";
		public const string RebateDetail = "RebateDetail";
		public const string RebateException = "RebateException";
		public const string RebateNote = "RebateNote";
		public const string RebateRange = "RebateRange";
		public const string RebateType = "RebateType";
		public const string RebateVAP = "RebateVAP";
		public const string Site = "Site";
		public const string TaxCode = "TaxCode";
		public const string TaxRate = "TaxRate";
		public const string User = "Users"; // User is a reserved word on MySQL databases
		public const string UserDivision = "UserDivision";

		// Used for join alias names
		public const string ShipToCustomer = "ShipToCustomer";
		public const string SoldToCustomer = "SoldToCustomer";

		public const string ParentCustomer = "ParentCustomer";
		public const string SoldToGroup = "SoldToGroup";
		public const string ShipToGroup = "ShipToGroup";

		public const string PaymentPayee = "PaymentPayee";
	}
}

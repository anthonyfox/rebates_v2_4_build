namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates
{
	partial class ProductFind
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this._products = new System.Windows.Forms.DataGridView();
			this._search = new System.Windows.Forms.Button();
			this._criteria = new System.Windows.Forms.GroupBox();
			this._lblAttribute4 = new System.Windows.Forms.Label();
			this._lblAttribute3 = new System.Windows.Forms.Label();
			this._lblAttribute2 = new System.Windows.Forms.Label();
			this._lblAttribute1 = new System.Windows.Forms.Label();
			this._attribute4 = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Attribute();
			this._attribute3 = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Attribute();
			this._attribute2 = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Attribute();
			this._attribute1 = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Attribute();
			this.buttonOk = new System.Windows.Forms.Button();
			this._clear = new System.Windows.Forms.Button();
			this._name = new System.Windows.Forms.TextBox();
			this._nameLabel = new System.Windows.Forms.Label();
			this._results = new System.Windows.Forms.GroupBox();
			((System.ComponentModel.ISupportInitialize) (this._products)).BeginInit();
			this._criteria.SuspendLayout();
			this._results.SuspendLayout();
			this.SuspendLayout();
			// 
			// _products
			// 
			this._products.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this._products.BackgroundColor = System.Drawing.SystemColors.Control;
			this._products.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._products.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
			this._products.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this._products.GridColor = System.Drawing.SystemColors.Control;
			this._products.Location = new System.Drawing.Point(9, 19);
			this._products.Name = "_products";
			this._products.Size = new System.Drawing.Size(645, 255);
			this._products.TabIndex = 0;
			// 
			// _search
			// 
			this._search.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._search.Location = new System.Drawing.Point(592, 85);
			this._search.Name = "_search";
			this._search.Size = new System.Drawing.Size(62, 23);
			this._search.TabIndex = 14;
			this._search.Text = "Search";
			this._search.UseVisualStyleBackColor = true;
			this._search.Click += new System.EventHandler(this._search_Click);
			// 
			// _criteria
			// 
			this._criteria.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this._criteria.Controls.Add(this._lblAttribute4);
			this._criteria.Controls.Add(this._lblAttribute3);
			this._criteria.Controls.Add(this._lblAttribute2);
			this._criteria.Controls.Add(this._lblAttribute1);
			this._criteria.Controls.Add(this._attribute4);
			this._criteria.Controls.Add(this._attribute3);
			this._criteria.Controls.Add(this._attribute2);
			this._criteria.Controls.Add(this._attribute1);
			this._criteria.Controls.Add(this.buttonOk);
			this._criteria.Controls.Add(this._clear);
			this._criteria.Controls.Add(this._name);
			this._criteria.Controls.Add(this._nameLabel);
			this._criteria.Controls.Add(this._search);
			this._criteria.Location = new System.Drawing.Point(12, 5);
			this._criteria.Name = "_criteria";
			this._criteria.Size = new System.Drawing.Size(660, 143);
			this._criteria.TabIndex = 0;
			this._criteria.TabStop = false;
			this._criteria.Text = "Search Criteria";
			// 
			// _lblAttribute4
			// 
			this._lblAttribute4.AutoSize = true;
			this._lblAttribute4.Location = new System.Drawing.Point(56, 115);
			this._lblAttribute4.Name = "_lblAttribute4";
			this._lblAttribute4.Size = new System.Drawing.Size(13, 13);
			this._lblAttribute4.TabIndex = 23;
			this._lblAttribute4.Text = "4";
			// 
			// _lblAttribute3
			// 
			this._lblAttribute3.AutoSize = true;
			this._lblAttribute3.Location = new System.Drawing.Point(56, 91);
			this._lblAttribute3.Name = "_lblAttribute3";
			this._lblAttribute3.Size = new System.Drawing.Size(13, 13);
			this._lblAttribute3.TabIndex = 22;
			this._lblAttribute3.Text = "3";
			// 
			// _lblAttribute2
			// 
			this._lblAttribute2.AutoSize = true;
			this._lblAttribute2.Location = new System.Drawing.Point(56, 68);
			this._lblAttribute2.Name = "_lblAttribute2";
			this._lblAttribute2.Size = new System.Drawing.Size(13, 13);
			this._lblAttribute2.TabIndex = 21;
			this._lblAttribute2.Text = "2";
			// 
			// _lblAttribute1
			// 
			this._lblAttribute1.AutoSize = true;
			this._lblAttribute1.Location = new System.Drawing.Point(14, 46);
			this._lblAttribute1.Name = "_lblAttribute1";
			this._lblAttribute1.Size = new System.Drawing.Size(55, 13);
			this._lblAttribute1.TabIndex = 20;
			this._lblAttribute1.Text = "Attribute 1";
			// 
			// _attribute4
			// 
			this._attribute4.DivisionId = 0;
			this._attribute4.IncludeBlank = false;
			this._attribute4.Level = null;
			this._attribute4.Location = new System.Drawing.Point(75, 111);
			this._attribute4.Name = "_attribute4";
			this._attribute4.Size = new System.Drawing.Size(332, 22);
			this._attribute4.TabIndex = 19;
			// 
			// _attribute3
			// 
			this._attribute3.DivisionId = 0;
			this._attribute3.IncludeBlank = false;
			this._attribute3.Level = null;
			this._attribute3.Location = new System.Drawing.Point(75, 88);
			this._attribute3.Name = "_attribute3";
			this._attribute3.Size = new System.Drawing.Size(332, 22);
			this._attribute3.TabIndex = 18;
			// 
			// _attribute2
			// 
			this._attribute2.DivisionId = 0;
			this._attribute2.IncludeBlank = false;
			this._attribute2.Level = null;
			this._attribute2.Location = new System.Drawing.Point(75, 64);
			this._attribute2.Name = "_attribute2";
			this._attribute2.Size = new System.Drawing.Size(332, 22);
			this._attribute2.TabIndex = 17;
			// 
			// _attribute1
			// 
			this._attribute1.DivisionId = 0;
			this._attribute1.IncludeBlank = false;
			this._attribute1.Level = null;
			this._attribute1.Location = new System.Drawing.Point(75, 41);
			this._attribute1.Name = "_attribute1";
			this._attribute1.Size = new System.Drawing.Size(332, 22);
			this._attribute1.TabIndex = 16;
			// 
			// buttonOk
			// 
			this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonOk.Location = new System.Drawing.Point(592, 114);
			this.buttonOk.Name = "buttonOk";
			this.buttonOk.Size = new System.Drawing.Size(62, 23);
			this.buttonOk.TabIndex = 15;
			this.buttonOk.Text = "Ok";
			this.buttonOk.UseVisualStyleBackColor = true;
			// 
			// _clear
			// 
			this._clear.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._clear.Location = new System.Drawing.Point(502, 85);
			this._clear.Name = "_clear";
			this._clear.Size = new System.Drawing.Size(84, 23);
			this._clear.TabIndex = 13;
			this._clear.Text = "Clear Criteria";
			this._clear.UseVisualStyleBackColor = true;
			this._clear.Click += new System.EventHandler(this._clear_Click);
			// 
			// _name
			// 
			this._name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this._name.Location = new System.Drawing.Point(75, 19);
			this._name.Name = "_name";
			this._name.Size = new System.Drawing.Size(200, 20);
			this._name.TabIndex = 1;
			// 
			// _nameLabel
			// 
			this._nameLabel.AutoSize = true;
			this._nameLabel.Location = new System.Drawing.Point(34, 22);
			this._nameLabel.Name = "_nameLabel";
			this._nameLabel.Size = new System.Drawing.Size(35, 13);
			this._nameLabel.TabIndex = 0;
			this._nameLabel.Text = "Name";
			// 
			// _results
			// 
			this._results.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this._results.Controls.Add(this._products);
			this._results.Location = new System.Drawing.Point(12, 154);
			this._results.Name = "_results";
			this._results.Size = new System.Drawing.Size(660, 285);
			this._results.TabIndex = 1;
			this._results.TabStop = false;
			this._results.Text = "Results";
			// 
			// ProductFind
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(684, 445);
			this.Controls.Add(this._results);
			this.Controls.Add(this._criteria);
			this.Name = "ProductFind";
			this.Text = "Find Product";
			this.Load += new System.EventHandler(this.ProductFind_Load);
			((System.ComponentModel.ISupportInitialize) (this._products)).EndInit();
			this._criteria.ResumeLayout(false);
			this._criteria.PerformLayout();
			this._results.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.DataGridView _products;
		private System.Windows.Forms.Button _search;
		private System.Windows.Forms.GroupBox _criteria;
		private System.Windows.Forms.GroupBox _results;
		private System.Windows.Forms.Label _nameLabel;
		private System.Windows.Forms.TextBox _name;
		private System.Windows.Forms.Button _clear;
		private System.Windows.Forms.Button buttonOk;
		private System.Windows.Forms.Label _lblAttribute4;
		private System.Windows.Forms.Label _lblAttribute3;
		private System.Windows.Forms.Label _lblAttribute2;
		private System.Windows.Forms.Label _lblAttribute1;
		private UserControls.Rebates.Attribute _attribute4;
		private UserControls.Rebates.Attribute _attribute3;
		private UserControls.Rebates.Attribute _attribute2;
		private UserControls.Rebates.Attribute _attribute1;

	}
}
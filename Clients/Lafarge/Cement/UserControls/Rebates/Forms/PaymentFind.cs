﻿using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Forms;

namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates
{
	public partial class PaymentFind : Form
	{
		public delegate void DisplayResultsHandler(object sender, EventArgs e);
		private Hashtable _delegateStore = new Hashtable();
		private static Object _displayResultsEventKey = new Object();
		private ExtendedBindingList<PaymentReview> _payments = null;

		public ExtendedBindingList<PaymentReview> Payments
		{
			get { return _payments; }
		}

		public event DisplayResultsHandler DisplayResults
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			add
			{
				_delegateStore[_displayResultsEventKey] = Delegate.Combine((Delegate)_delegateStore[_displayResultsEventKey], value);
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			remove
			{
				_delegateStore[_displayResultsEventKey] = Delegate.Remove((Delegate)_delegateStore[_displayResultsEventKey], value);
			}
		}

		protected void OnDisplayResults()
		{
			DisplayResultsHandler displayResultsHandler = (DisplayResultsHandler)_delegateStore[_displayResultsEventKey];

			if (displayResultsHandler != null)
			{
				displayResultsHandler(this, new EventArgs());
			}
		}

		public PaymentFind()
		{
			// This constructor only exixts to allow design mode to work
			// the PaymentFind will only perform correctly if called with Division
			if (LicenseManager.UsageMode != LicenseUsageMode.Designtime)
			{
				throw new InvalidOperationException("RebateFind must be instantiated with a instance of Division");
			} 
			
			InitializeComponent();
		}

		public PaymentFind(Division division)
		{
			InitializeComponent();

			// Setup events
			this.Load += new EventHandler(PaymentFind_Load);

			// Setup Controls
			_ucHierarchy.DivisionId = division.Id;
			_ucHierarchy.IncludeAllOption = true;
			_ucHierarchy.Populate();

			_ucCustomerGroup.DivisionId = division.Id;
			_ucCustomerGroup.IncludeBlankOption = true;
			_ucCustomerGroup.Populate();

			_ucCustomer.DivisionId = division.Id;
			_ucPayee.DivisionId = division.Id;
			_ucPaymentPayee.DivisionId = division.Id;

			_ucSoldFrom.DivisionId = division.Id;
			_ucSoldFrom.IncludeAllOption = true;
			_ucSoldFrom.Populate();

			_ucRebateType.IncludeAllOption = true;
			_ucRebateType.Populate();

			_ucPaymentFrequency.DivisionId = division.Id;
			_ucPaymentFrequency.IncludeAllOption = true;
			_ucPaymentFrequency.Populate();

			_ucTaxCode.IncludeAllOption = true;
			_ucTaxCode.Populate();

			// Set all the Either/Yes/No radio buttons to "All"
			_isContractAll.Checked = true;
			_isVAPAll.Checked = true;
			_isJIFAll.Checked = true;
			_isGlobalProductAll.Checked = true;

			// Setup event handler
			_txtRebateNumber.Validating += new CancelEventHandler(_txtRebateId_Validating);
			_txtVersion.Validating += new CancelEventHandler(_txtVersion_Validating);
			_txtYear.Validating += new CancelEventHandler(_txtYear_Validating);
			_txtPeriod.Validating += new CancelEventHandler(_txtPeriod_Validating);
			_txtRebateDetailId.Validating += new CancelEventHandler(_txtRebateDetailId_Validating);
			_ucHierarchy.ComboBox.SelectedIndexChanged += new EventHandler(_hierarchy_SelectedIndexChanged);
			_ucRebateType.ComboBox.SelectedIndexChanged += new EventHandler(_rebateType_SelectedIndexChanged);
			_dtpTaxDateFrom.ValueChanged += _dtpTaxDateFrom_ValueChanged;
			_dtpTaxDateFrom.LostFocus += _dtpTaxDateFrom_LostFocus;
			_dtpTaxDateTo.ValueChanged += _dtpTaxDateTo_ValueChanged;
			_btnSearch.Click += new EventHandler(_btnSearch_Click);

			// Set Global Product Visibility
			_lblIsGlobalProduct.Visible = division.AllowGlobalProduct;
			_pnlIsGlobalProduct.Visible = division.AllowGlobalProduct;

			// Initialise
			SetupHierarchy();

			_dtpTaxDateFrom.Value = null;
			_dtpTaxDateTo.Value = null;
			_dtpTaxDateTo.Enabled = false;
		}

		#region Validation

		#region VALIDATE: Rebate Id
		void _txtRebateId_Validating(object sender, CancelEventArgs e)
		{
			TextBox textBox = (TextBox)sender;

			_errorProvider.SetError(_txtRebateNumber, Rebate.ValidateId(textBox.Text, true));
		}
		#endregion

		#region VALIDATE: Rebate Version
		void _txtVersion_Validating(object sender, CancelEventArgs e)
		{
			TextBox textBox = (TextBox)sender;

			_errorProvider.SetError(_txtVersion, Rebate.ValidateVersion(textBox.Text, true));
			ConfigureVersionYearPeriodFields();
		}
		#endregion


		#region VALIDATE: Year
		void _txtYear_Validating(object sender, CancelEventArgs e)
		{
			TextBox textBox = (TextBox)sender;

			_errorProvider.SetError(textBox, Rebate.ValidateYear(textBox.Text, true));
			ConfigureVersionYearPeriodFields();
		}

		#endregion

		#region VALIDATE: Period
		void _txtPeriod_Validating(object sender, CancelEventArgs e)
		{
			TextBox textBox = (TextBox)sender;

			_errorProvider.SetError(textBox, Rebate.ValidatePeriod(textBox.Text, true));
		}
		#endregion

		#region VALIDATE: RebateDetailId
		void _txtRebateDetailId_Validating(object sender, CancelEventArgs e)
		{
			String error = "";

			if (!String.IsNullOrEmpty(_txtRebateDetailId.Text))
			{
				Int32 value = 0;

				if (!Int32.TryParse(_txtRebateDetailId.Text, out value))
				{
					error = "Rebate Detail Id must be an integer number";
				}
			}

			_errorProvider.SetError(_txtRebateDetailId, error);
		}
		#endregion

		#region Confifure the Version, Year and period text boxes
		public void ConfigureVersionYearPeriodFields()
		{
			_txtVersion.Enabled = (_txtYear.Text.Trim().Length == 0); // No version if Year entered
			_txtYear.Enabled = (_txtVersion.Text.Trim().Length == 0); // No Year if Version entered
			_txtPeriod.Enabled = (_txtYear.Text.Trim().Length > 0); // No period unless Year entered

			// Now clear the text on the fields if they are disabled
			if (!_txtVersion.Enabled)
			{
				_txtVersion.Text = "";
			}
			if (!_txtYear.Enabled)
			{
				_txtYear.Text = "";
			}
			if (!_txtPeriod.Enabled)
			{
				_txtPeriod.Text = "";
			}
		}
		#endregion

		#region VALIDATE: Tax Date
		void _dtpTaxDateFrom_ValueChanged(object sender, EventArgs e)
		{
			ValidateTaxDates();
		}

		void _dtpTaxDateFrom_LostFocus(object sender, EventArgs e)
		{
			ValidateTaxDates();
		}

		void _dtpTaxDateTo_ValueChanged(object sender, EventArgs e)
		{
			ValidateTaxDates();
		}

		private void ValidateTaxDates()
		{
			String error = "";

			if (_dtpTaxDateFrom.Value == null)
			{
				_dtpTaxDateTo.Value = null;
			}

			_dtpTaxDateTo.Enabled = (_dtpTaxDateFrom.Value != null);

			if (_dtpTaxDateTo.Value != null && _dtpTaxDateFrom.Value != null)
			{
				if (_dtpTaxDateTo.Value.Value < _dtpTaxDateFrom.Value.Value)
				{
					error = "To date cannot be less than from date (leave blank for open ended search)";
				}
			}

			_errorProvider.SetError(_dtpTaxDateTo, error);
		}

		#endregion

		#endregion

		#region Customer Hierarchy
		void _hierarchy_SelectedIndexChanged(object sender, EventArgs e)
		{
			SetupHierarchy();
		}
		#endregion

		#region RebateType
		void _rebateType_SelectedIndexChanged(object sender, EventArgs e)
		{
			Boolean showDetailId = true;

			if (_ucRebateType.SelectedValue != null && _ucRebateType.SelectedValue.IsRebateExtension)
			{
				showDetailId = false;
			}

			_lblRebateDetailId.Visible = showDetailId;
			_txtRebateDetailId.Visible = showDetailId;
		}
		#endregion

		private void PaymentFind_Load(object sender, EventArgs e)
		{
			if (this.MdiParent != null)
			{
				this.Icon = this.MdiParent.Icon;
			}
		}

		void _btnSearch_Click(object sender, EventArgs e)
		{
			// Validate controls
			this.ValidateChildren();
			Control invalidControl = ValidateControl.ControlWithError(this, _errorProvider);

			if (invalidControl == null) // Controls valid
			{
				RefreshList();
				this.Cursor = Cursors.Default;

				if (_payments.Count == 0)
				{
					MessageBox.Show("No payments found", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					OnDisplayResults();
				}
			}
			else // Invalid controls
			{
				// Set focus to first invalid control
				FormUtilities.SetFocus(invalidControl);
			}
		}

		#region Business Logic

		public void RefreshList()
		{
			// Create add filter for paymentReview search adding in all the various paprameters from the form
			PaymentReview paymentReview = new PaymentReview();

			if (_txtRebateNumber.Text.Trim() != "")
			{
				paymentReview.RebateNumber = Int32.Parse(_txtRebateNumber.Text);
				paymentReview.AddReadFilter(PaymentReviewProperty.RebateNumber, "=");
			}

			if (_txtVersion.Text.Trim() != "")
			{
				paymentReview.RebateVersion = Int16.Parse(_txtVersion.Text);
				paymentReview.AddReadFilter(PaymentReviewProperty.RebateVersion, "=");
			}

			if (_txtDescription.Text.Trim() != "")
			{
				paymentReview.Description = _txtDescription.Text.Replace('*', paymentReview.WildcardCharacter()) + paymentReview.WildcardCharacter().ToString();
				paymentReview.AddReadFilter(PaymentReviewProperty.Description, paymentReview.WildcardKeyword());
			}

			if (_ucPayee.SelectedValue != null)
			{
				paymentReview.RebatePayeeId = _ucPayee.SelectedValue.Id;
				paymentReview.AddReadFilter(PaymentReviewProperty.RebatePayeeId, "=");
			}

			if (_ucDeliveryType.SelectedValue != DeliveryTypeEnum.Null)
			{
				paymentReview.DeliveryTypeEnum = _ucDeliveryType.SelectedValue;
				paymentReview.AddReadFilter(PaymentReviewProperty.DeliveryTypeEnum, "=");
			}

			if (!_ucSoldFrom.IsAllSelected)
			{
				paymentReview.SiteId = _ucSoldFrom.SelectedValue.Id;
				paymentReview.AddReadFilter(PaymentReviewProperty.SiteId, "=");
			}

			if (_txtYear.Text.Trim() != "")
			{
				Int16 year = Int16.Parse(_txtYear.Text);
				DateTime periodStartDate;
				DateTime periodEndDate;

				if (_txtPeriod.Text.Trim() == "")
				{
					periodStartDate = new DateTime(year, 1, 1); // 1st January
					periodEndDate = new DateTime(year, 12, 31); // 31st December
				}
				else
				{
					Int16 period = Int16.Parse(_txtPeriod.Text);
					periodStartDate = new DateTime(year, period, 1); // 1st of the month
					periodEndDate = new DateTime(year, period, DateTime.DaysInMonth(year, period)); // Last day of the month
				}

				paymentReview.StartDate = periodEndDate;
				paymentReview.AddReadFilter(PaymentReviewProperty.StartDate, "<=");

				paymentReview.EndDate = periodStartDate;
				String condition = paymentReview.SqlCondition(PaymentReviewProperty.EndDate, ">=");
				paymentReview.EndDate = null;
				condition += " or " + paymentReview.SqlCondition(PaymentReviewProperty.EndDate, "=");

				paymentReview.AddReadFilter("(" + condition + ")");
			}

			if (!_isContractAll.Checked)
			{
				paymentReview.IsContract = _isContractYes.Checked;
				paymentReview.AddReadFilter(PaymentReviewProperty.IsContract, "=");
			}

			if (!_isVAPAll.Checked)
			{
				paymentReview.IsVAP = _isVAPYes.Checked;
				paymentReview.AddReadFilter(PaymentReviewProperty.IsVAP, "=");
			}

			if (!_isJIFAll.Checked)
			{
				paymentReview.IsJif = _isJIFYes.Checked;
				paymentReview.AddReadFilter(PaymentReviewProperty.IsJif, "=");
			}

			if (!_isGlobalProductAll.Checked)
			{
				paymentReview.IsGlobalProduct = _isGlobalProductYes.Checked;
				paymentReview.AddReadFilter(PaymentReviewProperty.IsGlobalProduct, "=");
			}

			if (!_ucRebateType.IsAllSelected)
			{
				paymentReview.RebateTypeId = _ucRebateType.SelectedValue.Id;
				paymentReview.AddReadFilter(PaymentReviewProperty.RebateTypeId, "=");
			}

			if (!_ucPaymentFrequency.IsAllSelected)
			{
				paymentReview.PaymentFrequencyId = _ucPaymentFrequency.SelectedValue.Id;
				paymentReview.AddReadFilter(PaymentReviewProperty.PaymentFrequencyId, "=");
			}

			if (_ucPaymentPayee.SelectedValue != null)
			{
				paymentReview.PayeeId = _ucPaymentPayee.SelectedValue.Id;
				paymentReview.AddReadFilter(PaymentReviewProperty.PayeeId, "=");
			}

			if (_dtpTaxDateFrom.Value != null)
			{
				if (_dtpTaxDateTo.Value == null)
				{
					paymentReview.TaxDate = _dtpTaxDateFrom.Value.Value;
					paymentReview.AddReadFilter(PaymentReviewProperty.TaxDate, ">=");
				}
				else
				{
					paymentReview.AddReadFilterBetween(PaymentReviewProperty.TaxDate, _dtpTaxDateFrom.Value.Value, _dtpTaxDateTo.Value.Value);
				}
			}

			if (_ucTaxCode.SelectedValue != null && _ucTaxCode.SelectedValue.Id > 0)
			{
				paymentReview.TaxCodeId = _ucTaxCode.SelectedValue.Id;
				paymentReview.AddReadFilter(PaymentReviewProperty.TaxCodeId, "=");
			}

			if (_ucPaymentStatus.SelectedValue != PaymentStatusEnum.Null)
			{
				paymentReview.PaymentStatusEnum = _ucPaymentStatus.SelectedValue;
				paymentReview.AddReadFilter(PaymentReviewProperty.PaymentStatusEnum, "=");
			}

			// Add paymentReview detail id filter if required
			if (_txtRebateDetailId.Visible && !String.IsNullOrEmpty(_txtRebateDetailId.Text))
			{
				String extendFilter = "{" + PaymentReviewProperty.RebateId + "} in " +
					"( select " + RebateDetailColumn.RebateId + " from " + paymentReview.DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.RebateDetail) + " where " + RebateDetailColumn.Id + " = " + _txtRebateDetailId.Text + " )";
				paymentReview.ReadFilter = SqlHelper.FilterBuilder(paymentReview.ReadFilter, extendFilter);
			}

			// Read collection of paymentReviews based on the search criteria
			this.Cursor = Cursors.WaitCursor;

			ExtendedBindingList<PaymentReview> payments = (ExtendedBindingList<PaymentReview>)paymentReview.Read();

			if (!_ucHierarchy.IsAllSelected && _ucHierarchy.SelectedValue != null)
			{
				Int32 customerGroupId = -1;
				Business.Rebates.Hierarchy hierarchy = _ucHierarchy.SelectedValue;

				switch (hierarchy.RelatedEntityName)
				{
					case "CustomerGroup":
						if (_ucCustomerGroup.SelectedValue != null && !_ucCustomerGroup.IsAllSelected)
						{
							customerGroupId = _ucCustomerGroup.SelectedValue.Id;
						}

						break;

					case "Customer":
						if (_ucCustomer.SelectedValue != null)
						{
							if (hierarchy.Level == 2)
							{
								// Sold To
								customerGroupId = _ucCustomer.SelectedValue.CustomerGroupId;
							}
							else if (hierarchy.Level == 3)
							{
								// Ship To
								Business.Rebates.Customer customer = new Business.Rebates.Customer();
								customer.Id = _ucCustomer.SelectedValue.SoldToId;

								if (customer.ReadCurrent())
								{
									customerGroupId = customer.CustomerGroupId;
								}
							}
						}

						break;
				}

				_payments = new ExtendedBindingList<PaymentReview>();
				_payments.AddMultiple(payments.Where(p => p.SearchCustomerGroupId == customerGroupId));
			}
			else
			{
				_payments = payments;
			}
		}

		private void SetupHierarchy()
		{
			// Based on the related entity, activate the relevant control
			Evolve.Clients.Lafarge.Cement.Business.Rebates.Hierarchy hierarchy = (Evolve.Clients.Lafarge.Cement.Business.Rebates.Hierarchy)_ucHierarchy.ComboBox.SelectedValue;

			_ucCustomer.Visible = false;
			_ucCustomerGroup.Visible = false;

			if (hierarchy != null)
			{
				switch (hierarchy.RelatedEntityName)
				{
					case "Customer":
						_ucCustomer.Visible = true;
						_ucCustomer.CustomerSearchModeEnum = hierarchy.CustomerSearchModeEnum;
						break;
					case "CustomerGroup":
						_ucCustomerGroup.Visible = true;
						break;
				}
			}
		}

		#endregion
	}
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Forms;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Libraries.Utilities;
using System.Data.SqlClient;



namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Forms
{
    public partial class AuditFind : Form
    {
        public delegate void DisplayResultsHandler(object sender, EventArgs e);

        private Hashtable _delegateStore = new Hashtable();
        private static object _displayResultsEventKey = new object();

        private ExtendedBindingList<AuditHeader> _auditHeaders = null;

        public ExtendedBindingList<AuditHeader> AuditHeaders
        {
            get { return _auditHeaders; }
        }

        public event DisplayResultsHandler DisplayResults
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            add
            {
                _delegateStore[_displayResultsEventKey] =
                    Delegate.Combine((Delegate) _delegateStore[_displayResultsEventKey], value);
            }
            [MethodImpl(MethodImplOptions.Synchronized)]
            remove
            {
                _delegateStore[_displayResultsEventKey] =
                    Delegate.Remove((Delegate) _delegateStore[_displayResultsEventKey], value);
            }
        }

        protected void OnDisplayResults()
        {
            DisplayResultsHandler displayResultsHandler =
                (DisplayResultsHandler) _delegateStore[_displayResultsEventKey];

            //if (displayResultsHandler != null)
            {
                displayResultsHandler(this, new EventArgs());
            }
        }

        public AuditFind()
        {
            // This constructor only exists to allow design mode to work.
            // The AuditFind will only perform correctly if called with Division

            if (LicenseManager.UsageMode != LicenseUsageMode.Designtime)
            {
                throw new InvalidOperationException("AuditFind must be instantiated with an instance of Division");
            }

            InitializeComponent();
        }

        public AuditFind(Division division)
        {
            InitializeComponent();

            // Set max/min dates for the datetime pickers
            _dtpFromDate.MaxDate = DateTime.Today;
            _dtpFromDate.MinDate = new DateTime(2000, 1, 1);

            _dtpToDate.MaxDate = DateTime.Today;
            _dtpToDate.MinDate = new DateTime(2000, 1, 1);

            // Search button
            _btnSearch.Click += new EventHandler(_btnSearch_Click);
        }

        void frmRebateFind_Load(object sender, EventArgs e)
        {
            if (this.MdiParent != null)
            {
                this.Icon = this.MdiParent.Icon;
            }
        }

        void _btnSearch_Click(object sender, EventArgs e)
        {
            this.ValidateChildren();
            Control invalidControl = ValidateControl.ControlWithError(this, _errorProvider);

            if (invalidControl == null) // Controls valid
            {
                RefreshList();
                this.Cursor = Cursors.Default;

                if (_auditHeaders.Count == 0)
                {
                    MessageBox.Show(@"No audits found", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    OnDisplayResults();
                }
            }
            else // Invalid controls
            {
                // Set focus to first invalid control
                FormUtilities.SetFocus(invalidControl);
            }
        }

        #region Business Logic

        public void RefreshList()
        {
            AuditHeader auditHeader = new AuditHeader();

            // NOTE: The between function wants a datetime, so the start date needs to start at midnight
            // and the end date needs to end at 23:59:59

            auditHeader.AddReadFilter(auditHeader.SqlConditionBetween(
                propertyName: AuditHeaderProperty.TimeStamp, 
                fromValue: _dtpFromDate.Value.Date,
                toValue: _dtpToDate.Value.Date.AddDays(1).AddSeconds(-1)));

            this.Cursor = Cursors.WaitCursor;

            _auditHeaders = (ExtendedBindingList<AuditHeader>) auditHeader.Read();
        }

        #endregion


    
    }
}
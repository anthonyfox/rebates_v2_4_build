using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;

namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates
{
	public partial class CustomerFind : Form
	{
		private CustomerSearchModeEnum _searchMode = CustomerSearchModeEnum.Undefined;
		private Int32 _divisionId = 0;
		private Boolean _multiSelect = false;
		private Int32 _searchResultLimit = 500;
		private List<CustomerAddress> _selectedCustomers;
		private Int32? _customerGroupId;
		private Int32? _parentCustomerId;

		// Filter properties
		public Int32? CustomerGroupId
		{
			get { return _customerGroupId; }
			set { _customerGroupId = value; }
		}

		public Int32? ParentCustomerId
		{
			get { return _parentCustomerId; }
			set { _parentCustomerId = value; }
		}

		public Int32 DivisionId
		{
			get { return _divisionId; }
		}

		public Int32 SearchResultLimit
		{
			get { return _searchResultLimit; }
			set { _searchResultLimit = value; }
		}

		public List<CustomerAddress> SelectedCustomers
		{
			get { return _selectedCustomers; }
		}

		public CustomerFind(CustomerSearchModeEnum searchMode, Int32 divisionId, Boolean multiSelect)
		{
			// Basic Initialisation
			_searchMode = searchMode;
			_divisionId = divisionId;
			_multiSelect = multiSelect;

			InitializeComponent();

			// Set Form Text
			String formText = "Find %Mode% Account";

			switch (searchMode)
			{
				case CustomerSearchModeEnum.ShipTo:
					formText = formText.Replace("%Mode%", "Ship To");
					break;
				case CustomerSearchModeEnum.SoldTo:
					formText = formText.Replace("%Mode%", "Sold To");
					break;
				default:
					formText = formText.Replace("%Mode%", searchMode.ToString());
					break;
			}

			this.Text = formText;

			// Create Events
			_customers.KeyPress += new KeyPressEventHandler(_customers_KeyPress);
			_customers.CellContentDoubleClick += new DataGridViewCellEventHandler(_customers_CellContentDoubleClick);
			buttonOk.Click += new EventHandler(buttonAccept_Click);
		}

		private void CustomerFind_Load(object sender, EventArgs e)
		{
			_customers.MultiSelect = true;
			_customers.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			_customers.ReadOnly = true;
			_customers.AllowUserToAddRows = false;
			_customers.AllowUserToDeleteRows = false;
			_customers.AllowUserToOrderColumns = false;
			_customers.Visible = false;

			if (_searchMode == CustomerSearchModeEnum.Undefined)
			{
				throw new ArgumentException("CustomerSearchMode is undefined");
			}
		}

		void _customers_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
		{
			CustomerSelected();
		}

		void _customers_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == (char) Keys.Return)
			{
				CustomerSelected();
			}
		}

		void _clear_Click(object sender, EventArgs e)
		{
			_name.Text = "";
			_address1.Text = "";
			_address2.Text = "";
			_address3.Text = "";
			_address4.Text = "";
			_city.Text = "";
			_county.Text = "";
			_postCode.Text = "";

			_customers.Visible = false;
			_name.Focus();
		}

		private void _search_Click(object sender, EventArgs e)
		{
			_search.Enabled = false;
			this.Cursor = Cursors.WaitCursor;

			Boolean isRowLimitExceeded = false;
			CustomerAddress customer = new CustomerAddress();
			customer.DivisionId = _divisionId;

			// Determine search mode and filter details
			CustomerAddressSearchMode addressSearchMode;
			Int32? filterId = null;

			if (_searchMode == CustomerSearchModeEnum.SoldTo)
			{
				if (_customerGroupId.HasValue)
				{
					addressSearchMode = CustomerAddressSearchMode.SoldToByGroup;
					filterId = _customerGroupId;
				}
				else
				{
					addressSearchMode = CustomerAddressSearchMode.SoldTo;
				}
			}
			else
			{
				if (_customerGroupId.HasValue)
				{
					addressSearchMode = CustomerAddressSearchMode.ShipToByGroup;
					filterId = _customerGroupId;
				}
				else if (_parentCustomerId.HasValue)
				{
					addressSearchMode = CustomerAddressSearchMode.ShipToByParent;
					filterId = _parentCustomerId;
				}
				else
				{
					addressSearchMode = CustomerAddressSearchMode.ShipTo;
				}
			}

			_customers.DataSource = customer.Search(addressSearchMode, filterId, _name.Text, _address1.Text, _address2.Text, _address3.Text, _address4.Text, _city.Text, _county.Text, _postCode.Text, _searchResultLimit, out isRowLimitExceeded);
			_customers.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
			_customers.Visible = true;
			_customers.Focus();

			// Warn if truncated results set
			if (isRowLimitExceeded)
			{
				MessageBox.Show("Only the first " + _searchResultLimit.ToString() + " rows have been displayed", "Customer Search", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}

			Application.DoEvents();
			this.Cursor = Cursors.Default;
			_search.Enabled = true;
		}

		private void CustomerSelected()
		{
			_selectedCustomers = new List<CustomerAddress>();

			if (_multiSelect)
			{
				foreach (DataGridViewRow row in _customers.SelectedRows)
				{
					_selectedCustomers.Add((CustomerAddress) row.DataBoundItem);
				}
			} 
			else if (_customers.CurrentRow != null)
			{
				_selectedCustomers.Add((CustomerAddress) _customers.CurrentRow.DataBoundItem);
			}

			if (_selectedCustomers.Count == 0)
			{
				_selectedCustomers = null;
			}

			this.Close();
		}

		void buttonAccept_Click(object sender, EventArgs e)
		{
			if (_customers.Visible && _customers.SelectedRows.Count > 0)
			{
				CustomerSelected();
			}
		}
	}
}
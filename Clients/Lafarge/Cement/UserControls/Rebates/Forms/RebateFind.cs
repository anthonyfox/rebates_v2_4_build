﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates
{
	public partial class RebateFind : Form
	{
		public delegate void DisplayResultsHandler(object sender, EventArgs e);
		private Hashtable _delegateStore = new Hashtable();
		private static Object _displayResultsEventKey = new Object();

		private ExtendedBindingList<Rebate> _rebates = null;

		public ExtendedBindingList<Rebate> Rebates
		{
			get { return _rebates; }
		}

		public event DisplayResultsHandler DisplayResults
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			add
			{
				_delegateStore[_displayResultsEventKey] = Delegate.Combine((Delegate) _delegateStore[_displayResultsEventKey], value);
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			remove
			{
				_delegateStore[_displayResultsEventKey] = Delegate.Remove((Delegate) _delegateStore[_displayResultsEventKey], value);
			}
		}

		protected void OnDisplayResults()
		{
			DisplayResultsHandler displayResultsHandler = (DisplayResultsHandler) _delegateStore[_displayResultsEventKey];

			if (displayResultsHandler != null)
			{
				displayResultsHandler(this, new EventArgs());
			}
		}

		public RebateFind()
		{
			// This constructor only exixts to allow design mode to work
			// the RebateFind will only perform correctly if called with Division

			if (LicenseManager.UsageMode != LicenseUsageMode.Designtime)
			{
				throw new InvalidOperationException("RebateFind must be instantiated with a instance of Division");
			}

			InitializeComponent();
		}

		public RebateFind(Division division)
		{
			InitializeComponent();

			// Setup events
			this.Load += new EventHandler(frmRebateFind_Load);

			// Setup Controls
			_ucHierarchy.DivisionId = division.Id;
			_ucHierarchy.IncludeAllOption = true;
			_ucHierarchy.Populate();

			_ucCustomerGroup.DivisionId = division.Id;
			_ucCustomerGroup.IncludeBlankOption = true;
			_ucCustomerGroup.Populate();

			_ucCustomer.DivisionId = division.Id;
			_ucPayee.DivisionId = division.Id;

			_ucSoldFrom.DivisionId = division.Id;
			_ucSoldFrom.IncludeAllOption = true;
			_ucSoldFrom.Populate();

			_ucRebateType.IncludeAllOption = true;
			_ucRebateType.Populate();

			_ucPaymentFrequency.DivisionId = division.Id;
			_ucPaymentFrequency.IncludeAllOption = true;
			_ucPaymentFrequency.Populate();

			// Set all the Either/Yes/No radio buttons to "All"
			_isContractAll.Checked = true;
			_isVAPAll.Checked = true;
			_isJIFAll.Checked = true;
			_isGlobalProductAll.Checked = true;

			// Setup event handler
			_txtRebateNumber.Validating += new CancelEventHandler(_txtRebateId_Validating);
			_txtVersion.Validating += new CancelEventHandler(_txtVersion_Validating);
			_txtYear.Validating += new CancelEventHandler(_txtYear_Validating);
			_txtPeriod.Validating += new CancelEventHandler(_txtPeriod_Validating);
			_txtRebateDetailId.Validating += new CancelEventHandler(_txtRebateDetailId_Validating);
			_ucHierarchy.ComboBox.SelectedIndexChanged += new EventHandler(_hierarchy_SelectedIndexChanged);
			_ucRebateType.ComboBox.SelectedIndexChanged += new EventHandler(_rebateType_SelectedIndexChanged);
			_btnSearch.Click += new EventHandler(_btnSearch_Click);

			// Set Global Product Visibility
			_lblIsGlobalProduct.Visible = division.AllowGlobalProduct;
			_pnlIsGlobalProduct.Visible = division.AllowGlobalProduct;

			// Initialise
			SetupHierarchy();
		}

		void frmRebateFind_Load(object sender, EventArgs e)
		{
			if (this.MdiParent != null)
			{
				this.Icon = this.MdiParent.Icon;
			}
		}

		#region Validation

		#region VALIDATE: Rebate Id
		void _txtRebateId_Validating(object sender, CancelEventArgs e)
		{
			TextBox textBox = (TextBox) sender;

			_errorProvider.SetError(_txtRebateNumber, Rebate.ValidateId(textBox.Text, true));
		}
		#endregion

		#region VALIDATE: Rebate Version
		void _txtVersion_Validating(object sender, CancelEventArgs e)
		{
			TextBox textBox = (TextBox) sender;

			_errorProvider.SetError(_txtVersion, Rebate.ValidateVersion(textBox.Text, true));
			ConfigureVersionYearPeriodFields();
		}
		#endregion


		#region VALIDATE: Year
		void _txtYear_Validating(object sender, CancelEventArgs e)
		{
			TextBox textBox = (TextBox) sender;

			_errorProvider.SetError(textBox, Rebate.ValidateYear(textBox.Text, true));
			ConfigureVersionYearPeriodFields();
		}

		#endregion

		#region VALIDATE: Period
		void _txtPeriod_Validating(object sender, CancelEventArgs e)
		{
			TextBox textBox = (TextBox) sender;

			_errorProvider.SetError(textBox, Rebate.ValidatePeriod(textBox.Text, true));
		}
		#endregion

		#region VALIDATE: RebateDetailId
		void _txtRebateDetailId_Validating(object sender, CancelEventArgs e)
		{
			String error = "";

			if (!String.IsNullOrEmpty(_txtRebateDetailId.Text))
			{
				Int32 value = 0;

				if (!Int32.TryParse(_txtRebateDetailId.Text, out value))
				{
					error = "Rebate Detail Id must be an integer number";
				}
			}

			_errorProvider.SetError(_txtRebateDetailId, error);
		}
		#endregion
		
		#region Confifure the Version, Year and period text boxes
		public void ConfigureVersionYearPeriodFields()
		{
			_txtVersion.Enabled = (_txtYear.Text.Trim().Length == 0); // No version if Year entered
			_txtYear.Enabled = (_txtVersion.Text.Trim().Length == 0); // No Year if Version entered
			_txtPeriod.Enabled = (_txtYear.Text.Trim().Length > 0); // No period unless Year entered

			// Now clear the text on the fields if they are disabled
			if (!_txtVersion.Enabled)
			{
				_txtVersion.Text = "";
			}
			if (!_txtYear.Enabled)
			{
				_txtYear.Text = "";
			}
			if (!_txtPeriod.Enabled)
			{
				_txtPeriod.Text = "";
			}
		}
		#endregion

		#endregion

		#region Customer Hierarchy
		void _hierarchy_SelectedIndexChanged(object sender, EventArgs e)
		{
			SetupHierarchy();
		}
		#endregion

		#region RebateType
		void _rebateType_SelectedIndexChanged(object sender, EventArgs e)
		{
			Boolean showDetailId = true;

			if (_ucRebateType.SelectedValue != null && _ucRebateType.SelectedValue.IsRebateExtension)
			{
				showDetailId = false;
			}

			_lblRebateDetailId.Visible = showDetailId;
			_txtRebateDetailId.Visible = showDetailId;
		}
		#endregion

		void _btnSearch_Click(object sender, EventArgs e)
		{
			// Validate controls
			this.ValidateChildren();
			Control invalidControl = ValidateControl.ControlWithError(this, _errorProvider);

			if (invalidControl == null) // Controls valid
			{
				RefreshList();
				this.Cursor = Cursors.Default;

				if (_rebates.Count == 0)
				{
					MessageBox.Show("No rebates found", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					OnDisplayResults();
				}
			}
			else // Invalid controls
			{
				// Set focus to first invalid control
				FormUtilities.SetFocus(invalidControl);
			}
		}

		#region Business Logic

		public void RefreshList()
		{
			// Create add filter for rebate search adding in all the various paprameters from the form
			Rebate rebate = new Rebate();

			if (_txtRebateNumber.Text.Trim() != "")
			{
				rebate.Number = Int32.Parse(_txtRebateNumber.Text);
				rebate.AddReadFilter(RebateProperty.Number, "=");
			}

			if (_txtVersion.Text.Trim() != "")
			{
				rebate.Version = Int16.Parse(_txtVersion.Text);
				rebate.AddReadFilter(RebateProperty.Version, "=");
			}

			if (_txtDescription.Text.Trim() != "")
			{
				rebate.Description = _txtDescription.Text.Replace('*', rebate.WildcardCharacter()) + rebate.WildcardCharacter().ToString();
				rebate.AddReadFilter(RebateProperty.Description, rebate.WildcardKeyword());
			}

			if (_ucPayee.SelectedValue != null)
			{
				rebate.PayeeId = _ucPayee.SelectedValue.Id;
				rebate.AddReadFilter(RebateProperty.PayeeId, "=");
			}

			if (_ucDeliveryType.SelectedValue != DeliveryTypeEnum.Null)
			{
				rebate.DeliveryTypeEnum = _ucDeliveryType.SelectedValue;
				rebate.AddReadFilter(RebateProperty.DeliveryTypeEnum, "=");
			}

			if (!_ucSoldFrom.IsAllSelected)
			{
				rebate.SiteId = _ucSoldFrom.SelectedValue.Id;
				rebate.AddReadFilter(RebateProperty.SiteId, "=");
			}

			if (_txtYear.Text.Trim() != "")
			{
				Int16 year = Int16.Parse(_txtYear.Text);
				DateTime periodStartDate;
				DateTime periodEndDate;

				if (_txtPeriod.Text.Trim() == "")
				{
					periodStartDate = new DateTime(year, 1, 1); // 1st January
					periodEndDate = new DateTime(year, 12, 31); // 31st December
				}
				else
				{
					Int16 period = Int16.Parse(_txtPeriod.Text);
					periodStartDate = new DateTime(year, period, 1); // 1st of the month
					periodEndDate = new DateTime(year, period, DateTime.DaysInMonth(year, period)); // Last day of the month
				}

				rebate.StartDate = periodEndDate;
				rebate.AddReadFilter(RebateProperty.StartDate, "<=");

				rebate.EndDate = periodStartDate;
				String condition = rebate.SqlCondition(RebateProperty.EndDate, ">=");
				rebate.EndDate = null;
				condition += " or " + rebate.SqlCondition(RebateProperty.EndDate, "=");

				rebate.AddReadFilter("(" + condition + ")");
			}

			if (!_isContractAll.Checked)
			{
				rebate.IsContract = _isContractYes.Checked;
				rebate.AddReadFilter(RebateProperty.IsContract, "=");
			}

			if (!_isVAPAll.Checked)
			{
				rebate.IsVAP = _isVAPYes.Checked;
				rebate.AddReadFilter(RebateProperty.IsVAP, "=");
			}

			if (!_isJIFAll.Checked)
			{
				rebate.IsJif = _isJIFYes.Checked;
				rebate.AddReadFilter(RebateProperty.IsJif, "=");
			}

			if (!_isGlobalProductAll.Checked)
			{
				rebate.IsGlobalProduct = _isGlobalProductYes.Checked;
				rebate.AddReadFilter(RebateProperty.IsGlobalProduct, "=");
			}

			if (!_ucRebateType.IsAllSelected)
			{
				rebate.RebateTypeId = _ucRebateType.SelectedValue.Id;
				rebate.AddReadFilter(RebateProperty.RebateTypeId, "=");
			}

			if (!_ucPaymentFrequency.IsAllSelected)
			{
				rebate.PaymentFrequencyId = _ucPaymentFrequency.SelectedValue.Id;
				rebate.AddReadFilter(RebateProperty.PaymentFrequencyId, "=");
			}

			// Add rebate detail id filter if required
			if (_txtRebateDetailId.Visible && !String.IsNullOrEmpty(_txtRebateDetailId.Text))
			{
				String extendFilter = "{" + RebateProperty.Id + "} in " +
					"( select " + RebateDetailColumn.RebateId + " from " + rebate.DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.RebateDetail) + " where " + RebateDetailColumn.Id + " = " + _txtRebateDetailId.Text + " )";
				rebate.ReadFilter = SqlHelper.FilterBuilder(rebate.ReadFilter, extendFilter);
			}

			// Allow for additional search criteria in derived classes (Payments find for instance)
			AddExtendedSearchCriteria(rebate);

			// Read collection of rebates based on the search criteria
			this.Cursor = Cursors.WaitCursor;

			ExtendedBindingList<Rebate> rebates = (ExtendedBindingList<Rebate>) rebate.Read();

			if (!_ucHierarchy.IsAllSelected && _ucHierarchy.SelectedValue != null)
			{
				Int32 customerGroupId = -1;
				Business.Rebates.Hierarchy hierarchy = _ucHierarchy.SelectedValue;

				switch (hierarchy.RelatedEntityName)
				{
					case "CustomerGroup":
						if (_ucCustomerGroup.SelectedValue != null && !_ucCustomerGroup.IsAllSelected)
						{
							customerGroupId = _ucCustomerGroup.SelectedValue.Id;
						}

						break;

					case "Customer":
						if (_ucCustomer.SelectedValue != null)
						{
							if (hierarchy.Level == 2)
							{
								// Sold To
								customerGroupId = _ucCustomer.SelectedValue.CustomerGroupId;
							}
							else if (hierarchy.Level == 3)
							{
								// Ship To
								Business.Rebates.Customer customer = new Business.Rebates.Customer();
								customer.Id = _ucCustomer.SelectedValue.SoldToId;

								if (customer.ReadCurrent())
								{
									customerGroupId = customer.CustomerGroupId;
								}
							}
						}

						break;
				}

				_rebates = new ExtendedBindingList<Rebate>();
				_rebates.AddMultiple(rebates.Where(r => r.SearchCustomerGroupId == customerGroupId));
			}
			else
			{
				_rebates = rebates;
			}
		}

		protected virtual void AddExtendedSearchCriteria(Rebate rebate)
		{
		}

		private void SetupHierarchy()
		{
			// Based on the related entity, activate the relevant control
			Evolve.Clients.Lafarge.Cement.Business.Rebates.Hierarchy hierarchy = (Evolve.Clients.Lafarge.Cement.Business.Rebates.Hierarchy) _ucHierarchy.ComboBox.SelectedValue;

			_ucCustomer.Visible = false;
			_ucCustomerGroup.Visible = false;

			if (hierarchy != null)
			{
				switch (hierarchy.RelatedEntityName)
				{
					case "Customer":
						_ucCustomer.Visible = true;
						_ucCustomer.CustomerSearchModeEnum = hierarchy.CustomerSearchModeEnum;
						break;
					case "CustomerGroup":
						_ucCustomerGroup.Visible = true;
						break;
				}
			}
		}

		#endregion
	}
}

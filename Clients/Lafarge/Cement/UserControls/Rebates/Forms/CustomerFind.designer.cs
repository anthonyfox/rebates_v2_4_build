namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates
{
	partial class CustomerFind
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this._addressLabel = new System.Windows.Forms.Label();
			this._address2 = new System.Windows.Forms.TextBox();
			this._customers = new System.Windows.Forms.DataGridView();
			this._search = new System.Windows.Forms.Button();
			this._criteria = new System.Windows.Forms.GroupBox();
			this.buttonOk = new System.Windows.Forms.Button();
			this._county = new System.Windows.Forms.TextBox();
			this._countyLabel = new System.Windows.Forms.Label();
			this._city = new System.Windows.Forms.TextBox();
			this._cityLabel = new System.Windows.Forms.Label();
			this._clear = new System.Windows.Forms.Button();
			this._postCodeLabel = new System.Windows.Forms.Label();
			this._name = new System.Windows.Forms.TextBox();
			this._nameLabel = new System.Windows.Forms.Label();
			this._postCode = new System.Windows.Forms.TextBox();
			this._address4 = new System.Windows.Forms.TextBox();
			this._address3 = new System.Windows.Forms.TextBox();
			this._address1 = new System.Windows.Forms.TextBox();
			this._results = new System.Windows.Forms.GroupBox();
			((System.ComponentModel.ISupportInitialize) (this._customers)).BeginInit();
			this._criteria.SuspendLayout();
			this._results.SuspendLayout();
			this.SuspendLayout();
			// 
			// _addressLabel
			// 
			this._addressLabel.AutoSize = true;
			this._addressLabel.Location = new System.Drawing.Point(24, 44);
			this._addressLabel.Name = "_addressLabel";
			this._addressLabel.Size = new System.Drawing.Size(45, 13);
			this._addressLabel.TabIndex = 2;
			this._addressLabel.Text = "Address";
			// 
			// _address2
			// 
			this._address2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this._address2.Location = new System.Drawing.Point(75, 63);
			this._address2.Name = "_address2";
			this._address2.Size = new System.Drawing.Size(200, 20);
			this._address2.TabIndex = 4;
			// 
			// _customers
			// 
			this._customers.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this._customers.BackgroundColor = System.Drawing.SystemColors.ControlDark;
			this._customers.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._customers.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
			this._customers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this._customers.GridColor = System.Drawing.SystemColors.Control;
			this._customers.Location = new System.Drawing.Point(9, 19);
			this._customers.Name = "_customers";
			this._customers.Size = new System.Drawing.Size(645, 198);
			this._customers.TabIndex = 0;
			// 
			// _search
			// 
			this._search.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._search.Location = new System.Drawing.Point(592, 144);
			this._search.Name = "_search";
			this._search.Size = new System.Drawing.Size(62, 23);
			this._search.TabIndex = 14;
			this._search.Text = "Search";
			this._search.UseVisualStyleBackColor = true;
			this._search.Click += new System.EventHandler(this._search_Click);
			// 
			// _criteria
			// 
			this._criteria.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this._criteria.Controls.Add(this.buttonOk);
			this._criteria.Controls.Add(this._county);
			this._criteria.Controls.Add(this._countyLabel);
			this._criteria.Controls.Add(this._city);
			this._criteria.Controls.Add(this._cityLabel);
			this._criteria.Controls.Add(this._clear);
			this._criteria.Controls.Add(this._postCodeLabel);
			this._criteria.Controls.Add(this._name);
			this._criteria.Controls.Add(this._nameLabel);
			this._criteria.Controls.Add(this._postCode);
			this._criteria.Controls.Add(this._address4);
			this._criteria.Controls.Add(this._address3);
			this._criteria.Controls.Add(this._address1);
			this._criteria.Controls.Add(this._search);
			this._criteria.Controls.Add(this._addressLabel);
			this._criteria.Controls.Add(this._address2);
			this._criteria.Location = new System.Drawing.Point(12, 5);
			this._criteria.Name = "_criteria";
			this._criteria.Size = new System.Drawing.Size(660, 200);
			this._criteria.TabIndex = 0;
			this._criteria.TabStop = false;
			this._criteria.Text = "Search Criteria";
			// 
			// buttonOk
			// 
			this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonOk.Location = new System.Drawing.Point(592, 170);
			this.buttonOk.Name = "buttonOk";
			this.buttonOk.Size = new System.Drawing.Size(62, 23);
			this.buttonOk.TabIndex = 15;
			this.buttonOk.Text = "Ok";
			this.buttonOk.UseVisualStyleBackColor = true;
			// 
			// _county
			// 
			this._county.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this._county.Location = new System.Drawing.Point(75, 151);
			this._county.Name = "_county";
			this._county.Size = new System.Drawing.Size(200, 20);
			this._county.TabIndex = 10;
			// 
			// _countyLabel
			// 
			this._countyLabel.AutoSize = true;
			this._countyLabel.Location = new System.Drawing.Point(29, 154);
			this._countyLabel.Name = "_countyLabel";
			this._countyLabel.Size = new System.Drawing.Size(40, 13);
			this._countyLabel.TabIndex = 9;
			this._countyLabel.Text = "County";
			// 
			// _city
			// 
			this._city.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this._city.Location = new System.Drawing.Point(75, 129);
			this._city.Name = "_city";
			this._city.Size = new System.Drawing.Size(200, 20);
			this._city.TabIndex = 8;
			// 
			// _cityLabel
			// 
			this._cityLabel.AutoSize = true;
			this._cityLabel.Location = new System.Drawing.Point(45, 132);
			this._cityLabel.Name = "_cityLabel";
			this._cityLabel.Size = new System.Drawing.Size(24, 13);
			this._cityLabel.TabIndex = 7;
			this._cityLabel.Text = "City";
			// 
			// _clear
			// 
			this._clear.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._clear.Location = new System.Drawing.Point(502, 144);
			this._clear.Name = "_clear";
			this._clear.Size = new System.Drawing.Size(84, 23);
			this._clear.TabIndex = 13;
			this._clear.Text = "Clear Criteria";
			this._clear.UseVisualStyleBackColor = true;
			this._clear.Click += new System.EventHandler(this._clear_Click);
			// 
			// _postCodeLabel
			// 
			this._postCodeLabel.AutoSize = true;
			this._postCodeLabel.Location = new System.Drawing.Point(13, 176);
			this._postCodeLabel.Name = "_postCodeLabel";
			this._postCodeLabel.Size = new System.Drawing.Size(56, 13);
			this._postCodeLabel.TabIndex = 11;
			this._postCodeLabel.Text = "Post Code";
			// 
			// _name
			// 
			this._name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this._name.Location = new System.Drawing.Point(75, 19);
			this._name.Name = "_name";
			this._name.Size = new System.Drawing.Size(200, 20);
			this._name.TabIndex = 1;
			// 
			// _nameLabel
			// 
			this._nameLabel.AutoSize = true;
			this._nameLabel.Location = new System.Drawing.Point(34, 22);
			this._nameLabel.Name = "_nameLabel";
			this._nameLabel.Size = new System.Drawing.Size(35, 13);
			this._nameLabel.TabIndex = 0;
			this._nameLabel.Text = "Name";
			// 
			// _postCode
			// 
			this._postCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this._postCode.Location = new System.Drawing.Point(75, 173);
			this._postCode.Name = "_postCode";
			this._postCode.Size = new System.Drawing.Size(80, 20);
			this._postCode.TabIndex = 12;
			// 
			// _address4
			// 
			this._address4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this._address4.Location = new System.Drawing.Point(75, 107);
			this._address4.Name = "_address4";
			this._address4.Size = new System.Drawing.Size(200, 20);
			this._address4.TabIndex = 6;
			// 
			// _address3
			// 
			this._address3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this._address3.Location = new System.Drawing.Point(75, 85);
			this._address3.Name = "_address3";
			this._address3.Size = new System.Drawing.Size(200, 20);
			this._address3.TabIndex = 5;
			// 
			// _address1
			// 
			this._address1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this._address1.Location = new System.Drawing.Point(75, 41);
			this._address1.Name = "_address1";
			this._address1.Size = new System.Drawing.Size(200, 20);
			this._address1.TabIndex = 3;
			// 
			// _results
			// 
			this._results.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this._results.Controls.Add(this._customers);
			this._results.Location = new System.Drawing.Point(12, 211);
			this._results.Name = "_results";
			this._results.Size = new System.Drawing.Size(660, 228);
			this._results.TabIndex = 1;
			this._results.TabStop = false;
			this._results.Text = "Results";
			// 
			// CustomerFind
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(684, 445);
			this.Controls.Add(this._results);
			this.Controls.Add(this._criteria);
			this.Name = "CustomerFind";
			this.Text = "Find Customer";
			this.Load += new System.EventHandler(this.CustomerFind_Load);
			((System.ComponentModel.ISupportInitialize) (this._customers)).EndInit();
			this._criteria.ResumeLayout(false);
			this._criteria.PerformLayout();
			this._results.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label _addressLabel;
		private System.Windows.Forms.TextBox _address2;
		private System.Windows.Forms.DataGridView _customers;
		private System.Windows.Forms.Button _search;
		private System.Windows.Forms.GroupBox _criteria;
		private System.Windows.Forms.GroupBox _results;
		private System.Windows.Forms.TextBox _address1;
		private System.Windows.Forms.Label _nameLabel;
		private System.Windows.Forms.TextBox _postCode;
		private System.Windows.Forms.TextBox _address4;
		private System.Windows.Forms.TextBox _address3;
		private System.Windows.Forms.TextBox _name;
		private System.Windows.Forms.Label _postCodeLabel;
		private System.Windows.Forms.Button _clear;
		private System.Windows.Forms.TextBox _county;
		private System.Windows.Forms.Label _countyLabel;
		private System.Windows.Forms.TextBox _city;
		private System.Windows.Forms.Label _cityLabel;
		private System.Windows.Forms.Button buttonOk;

	}
}
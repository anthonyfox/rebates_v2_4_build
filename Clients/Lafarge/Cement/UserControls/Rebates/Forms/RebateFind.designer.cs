﻿namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates
{
	partial class RebateFind
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this._lblVersion = new System.Windows.Forms.Label();
			this._lblPeriod = new System.Windows.Forms.Label();
			this._lblRebateDate = new System.Windows.Forms.Label();
			this._txtDescription = new System.Windows.Forms.TextBox();
			this._txtRebateNumber = new System.Windows.Forms.TextBox();
			this._lblDescription = new System.Windows.Forms.Label();
			this._lblRebateNumber = new System.Windows.Forms.Label();
			this._txtVersion = new System.Windows.Forms.TextBox();
			this._isContractAll = new System.Windows.Forms.RadioButton();
			this._isContractYes = new System.Windows.Forms.RadioButton();
			this._isContractNo = new System.Windows.Forms.RadioButton();
			this._pnlIsContract = new System.Windows.Forms.Panel();
			this._lblIsContract = new System.Windows.Forms.Label();
			this._lblIsVAP = new System.Windows.Forms.Label();
			this._pnlIsVAP = new System.Windows.Forms.Panel();
			this._isVAPAll = new System.Windows.Forms.RadioButton();
			this._isVAPNo = new System.Windows.Forms.RadioButton();
			this._isVAPYes = new System.Windows.Forms.RadioButton();
			this._lblIsGlobalProduct = new System.Windows.Forms.Label();
			this._pnlIsGlobalProduct = new System.Windows.Forms.Panel();
			this._isGlobalProductAll = new System.Windows.Forms.RadioButton();
			this._isGlobalProductNo = new System.Windows.Forms.RadioButton();
			this._isGlobalProductYes = new System.Windows.Forms.RadioButton();
			this._btnSearch = new System.Windows.Forms.Button();
			this._errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
			this._txtYear = new System.Windows.Forms.TextBox();
			this._txtPeriod = new System.Windows.Forms.TextBox();
			this._txtRebateDetailId = new System.Windows.Forms.TextBox();
			this._lblRebateDetailId = new System.Windows.Forms.Label();
			this._ucPaymentFrequency = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.PaymentFrequency();
			this._ucRebateType = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.RebateType();
			this._ucCustomerGroup = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.CustomerGroup();
			this._ucSoldFrom = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Site();
			this._ucHierarchy = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Hierarchy();
			this._ucDeliveryType = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.DeliveryType();
			this._ucPayee = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Payee();
			this._ucCustomer = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Customer();
			this._lblIsJIF = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this._isJIFAll = new System.Windows.Forms.RadioButton();
			this._isJIFNo = new System.Windows.Forms.RadioButton();
			this._isJIFYes = new System.Windows.Forms.RadioButton();
			this._pnlIsContract.SuspendLayout();
			this._pnlIsVAP.SuspendLayout();
			this._pnlIsGlobalProduct.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this._errorProvider)).BeginInit();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// _lblVersion
			// 
			this._lblVersion.AutoSize = true;
			this._lblVersion.Location = new System.Drawing.Point(211, 23);
			this._lblVersion.Name = "_lblVersion";
			this._lblVersion.Size = new System.Drawing.Size(42, 13);
			this._lblVersion.TabIndex = 2;
			this._lblVersion.Text = "Version";
			// 
			// _lblPeriod
			// 
			this._lblPeriod.AutoSize = true;
			this._lblPeriod.Location = new System.Drawing.Point(211, 217);
			this._lblPeriod.Name = "_lblPeriod";
			this._lblPeriod.Size = new System.Drawing.Size(37, 13);
			this._lblPeriod.TabIndex = 14;
			this._lblPeriod.Text = "Period";
			// 
			// _lblRebateDate
			// 
			this._lblRebateDate.AutoSize = true;
			this._lblRebateDate.Location = new System.Drawing.Point(59, 218);
			this._lblRebateDate.Name = "_lblRebateDate";
			this._lblRebateDate.Size = new System.Drawing.Size(74, 13);
			this._lblRebateDate.TabIndex = 12;
			this._lblRebateDate.Text = "Active In Year";
			// 
			// _txtDescription
			// 
			this._txtDescription.Location = new System.Drawing.Point(139, 46);
			this._txtDescription.Name = "_txtDescription";
			this._txtDescription.Size = new System.Drawing.Size(300, 20);
			this._txtDescription.TabIndex = 5;
			// 
			// _txtRebateNumber
			// 
			this._txtRebateNumber.Location = new System.Drawing.Point(139, 20);
			this._txtRebateNumber.Name = "_txtRebateNumber";
			this._txtRebateNumber.Size = new System.Drawing.Size(50, 20);
			this._txtRebateNumber.TabIndex = 1;
			// 
			// _lblDescription
			// 
			this._lblDescription.AutoSize = true;
			this._lblDescription.Location = new System.Drawing.Point(73, 49);
			this._lblDescription.Name = "_lblDescription";
			this._lblDescription.Size = new System.Drawing.Size(60, 13);
			this._lblDescription.TabIndex = 4;
			this._lblDescription.Text = "Description";
			// 
			// _lblRebateNumber
			// 
			this._lblRebateNumber.AutoSize = true;
			this._lblRebateNumber.Location = new System.Drawing.Point(51, 23);
			this._lblRebateNumber.Name = "_lblRebateNumber";
			this._lblRebateNumber.Size = new System.Drawing.Size(82, 13);
			this._lblRebateNumber.TabIndex = 0;
			this._lblRebateNumber.Text = "Rebate Number";
			// 
			// _txtVersion
			// 
			this._txtVersion.Location = new System.Drawing.Point(259, 20);
			this._txtVersion.Name = "_txtVersion";
			this._txtVersion.Size = new System.Drawing.Size(50, 20);
			this._txtVersion.TabIndex = 3;
			// 
			// _isContractAll
			// 
			this._isContractAll.AutoSize = true;
			this._isContractAll.Location = new System.Drawing.Point(3, 3);
			this._isContractAll.Name = "_isContractAll";
			this._isContractAll.Size = new System.Drawing.Size(36, 17);
			this._isContractAll.TabIndex = 0;
			this._isContractAll.TabStop = true;
			this._isContractAll.Text = "All";
			this._isContractAll.UseVisualStyleBackColor = true;
			// 
			// _isContractYes
			// 
			this._isContractYes.AutoSize = true;
			this._isContractYes.Location = new System.Drawing.Point(56, 3);
			this._isContractYes.Name = "_isContractYes";
			this._isContractYes.Size = new System.Drawing.Size(43, 17);
			this._isContractYes.TabIndex = 1;
			this._isContractYes.TabStop = true;
			this._isContractYes.Text = "Yes";
			this._isContractYes.UseVisualStyleBackColor = true;
			// 
			// _isContractNo
			// 
			this._isContractNo.AutoSize = true;
			this._isContractNo.Location = new System.Drawing.Point(105, 3);
			this._isContractNo.Name = "_isContractNo";
			this._isContractNo.Size = new System.Drawing.Size(39, 17);
			this._isContractNo.TabIndex = 2;
			this._isContractNo.TabStop = true;
			this._isContractNo.Text = "No";
			this._isContractNo.UseVisualStyleBackColor = true;
			// 
			// _pnlIsContract
			// 
			this._pnlIsContract.Controls.Add(this._isContractAll);
			this._pnlIsContract.Controls.Add(this._isContractNo);
			this._pnlIsContract.Controls.Add(this._isContractYes);
			this._pnlIsContract.Location = new System.Drawing.Point(139, 240);
			this._pnlIsContract.Name = "_pnlIsContract";
			this._pnlIsContract.Size = new System.Drawing.Size(150, 25);
			this._pnlIsContract.TabIndex = 60;
			// 
			// _lblIsContract
			// 
			this._lblIsContract.AutoSize = true;
			this._lblIsContract.Location = new System.Drawing.Point(75, 245);
			this._lblIsContract.Name = "_lblIsContract";
			this._lblIsContract.Size = new System.Drawing.Size(58, 13);
			this._lblIsContract.TabIndex = 16;
			this._lblIsContract.Text = "Is Contract";
			// 
			// _lblIsVAP
			// 
			this._lblIsVAP.AutoSize = true;
			this._lblIsVAP.Location = new System.Drawing.Point(94, 268);
			this._lblIsVAP.Name = "_lblIsVAP";
			this._lblIsVAP.Size = new System.Drawing.Size(39, 13);
			this._lblIsVAP.TabIndex = 17;
			this._lblIsVAP.Text = "Is VAP";
			// 
			// _pnlIsVAP
			// 
			this._pnlIsVAP.Controls.Add(this._isVAPAll);
			this._pnlIsVAP.Controls.Add(this._isVAPNo);
			this._pnlIsVAP.Controls.Add(this._isVAPYes);
			this._pnlIsVAP.Location = new System.Drawing.Point(139, 263);
			this._pnlIsVAP.Name = "_pnlIsVAP";
			this._pnlIsVAP.Size = new System.Drawing.Size(150, 25);
			this._pnlIsVAP.TabIndex = 62;
			// 
			// _isVAPAll
			// 
			this._isVAPAll.AutoSize = true;
			this._isVAPAll.Location = new System.Drawing.Point(3, 3);
			this._isVAPAll.Name = "_isVAPAll";
			this._isVAPAll.Size = new System.Drawing.Size(36, 17);
			this._isVAPAll.TabIndex = 0;
			this._isVAPAll.TabStop = true;
			this._isVAPAll.Text = "All";
			this._isVAPAll.UseVisualStyleBackColor = true;
			// 
			// _isVAPNo
			// 
			this._isVAPNo.AutoSize = true;
			this._isVAPNo.Location = new System.Drawing.Point(105, 3);
			this._isVAPNo.Name = "_isVAPNo";
			this._isVAPNo.Size = new System.Drawing.Size(39, 17);
			this._isVAPNo.TabIndex = 2;
			this._isVAPNo.TabStop = true;
			this._isVAPNo.Text = "No";
			this._isVAPNo.UseVisualStyleBackColor = true;
			// 
			// _isVAPYes
			// 
			this._isVAPYes.AutoSize = true;
			this._isVAPYes.Location = new System.Drawing.Point(56, 3);
			this._isVAPYes.Name = "_isVAPYes";
			this._isVAPYes.Size = new System.Drawing.Size(43, 17);
			this._isVAPYes.TabIndex = 1;
			this._isVAPYes.TabStop = true;
			this._isVAPYes.Text = "Yes";
			this._isVAPYes.UseVisualStyleBackColor = true;
			// 
			// _lblIsGlobalProduct
			// 
			this._lblIsGlobalProduct.AutoSize = true;
			this._lblIsGlobalProduct.Location = new System.Drawing.Point(58, 316);
			this._lblIsGlobalProduct.Name = "_lblIsGlobalProduct";
			this._lblIsGlobalProduct.Size = new System.Drawing.Size(75, 13);
			this._lblIsGlobalProduct.TabIndex = 19;
			this._lblIsGlobalProduct.Text = "Global Rebate";
			// 
			// _pnlIsGlobalProduct
			// 
			this._pnlIsGlobalProduct.Controls.Add(this._isGlobalProductAll);
			this._pnlIsGlobalProduct.Controls.Add(this._isGlobalProductNo);
			this._pnlIsGlobalProduct.Controls.Add(this._isGlobalProductYes);
			this._pnlIsGlobalProduct.Location = new System.Drawing.Point(139, 311);
			this._pnlIsGlobalProduct.Name = "_pnlIsGlobalProduct";
			this._pnlIsGlobalProduct.Size = new System.Drawing.Size(150, 25);
			this._pnlIsGlobalProduct.TabIndex = 68;
			// 
			// _isGlobalProductAll
			// 
			this._isGlobalProductAll.AutoSize = true;
			this._isGlobalProductAll.Location = new System.Drawing.Point(3, 3);
			this._isGlobalProductAll.Name = "_isGlobalProductAll";
			this._isGlobalProductAll.Size = new System.Drawing.Size(36, 17);
			this._isGlobalProductAll.TabIndex = 0;
			this._isGlobalProductAll.TabStop = true;
			this._isGlobalProductAll.Text = "All";
			this._isGlobalProductAll.UseVisualStyleBackColor = true;
			// 
			// _isGlobalProductNo
			// 
			this._isGlobalProductNo.AutoSize = true;
			this._isGlobalProductNo.Location = new System.Drawing.Point(105, 3);
			this._isGlobalProductNo.Name = "_isGlobalProductNo";
			this._isGlobalProductNo.Size = new System.Drawing.Size(39, 17);
			this._isGlobalProductNo.TabIndex = 2;
			this._isGlobalProductNo.TabStop = true;
			this._isGlobalProductNo.Text = "No";
			this._isGlobalProductNo.UseVisualStyleBackColor = true;
			// 
			// _isGlobalProductYes
			// 
			this._isGlobalProductYes.AutoSize = true;
			this._isGlobalProductYes.Location = new System.Drawing.Point(56, 3);
			this._isGlobalProductYes.Name = "_isGlobalProductYes";
			this._isGlobalProductYes.Size = new System.Drawing.Size(43, 17);
			this._isGlobalProductYes.TabIndex = 1;
			this._isGlobalProductYes.TabStop = true;
			this._isGlobalProductYes.Text = "Yes";
			this._isGlobalProductYes.UseVisualStyleBackColor = true;
			// 
			// _btnSearch
			// 
			this._btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._btnSearch.Location = new System.Drawing.Point(453, 392);
			this._btnSearch.Name = "_btnSearch";
			this._btnSearch.Size = new System.Drawing.Size(75, 23);
			this._btnSearch.TabIndex = 24;
			this._btnSearch.Text = "Search";
			this._btnSearch.UseVisualStyleBackColor = true;
			// 
			// _errorProvider
			// 
			this._errorProvider.ContainerControl = this;
			// 
			// _txtYear
			// 
			this._txtYear.Location = new System.Drawing.Point(139, 214);
			this._txtYear.Name = "_txtYear";
			this._txtYear.Size = new System.Drawing.Size(50, 20);
			this._txtYear.TabIndex = 13;
			// 
			// _txtPeriod
			// 
			this._txtPeriod.Location = new System.Drawing.Point(249, 214);
			this._txtPeriod.Name = "_txtPeriod";
			this._txtPeriod.Size = new System.Drawing.Size(20, 20);
			this._txtPeriod.TabIndex = 15;
			// 
			// _txtRebateDetailId
			// 
			this._txtRebateDetailId.Location = new System.Drawing.Point(138, 368);
			this._txtRebateDetailId.Name = "_txtRebateDetailId";
			this._txtRebateDetailId.Size = new System.Drawing.Size(50, 20);
			this._txtRebateDetailId.TabIndex = 22;
			// 
			// _lblRebateDetailId
			// 
			this._lblRebateDetailId.AutoSize = true;
			this._lblRebateDetailId.Location = new System.Drawing.Point(48, 371);
			this._lblRebateDetailId.Name = "_lblRebateDetailId";
			this._lblRebateDetailId.Size = new System.Drawing.Size(84, 13);
			this._lblRebateDetailId.TabIndex = 21;
			this._lblRebateDetailId.Text = "Rebate Detail Id";
			// 
			// _ucPaymentFrequency
			// 
			this._ucPaymentFrequency.AllItemText = "All";
			this._ucPaymentFrequency.DivisionId = 0;
			this._ucPaymentFrequency.IncludeAllOption = false;
			this._ucPaymentFrequency.Location = new System.Drawing.Point(25, 392);
			this._ucPaymentFrequency.Name = "_ucPaymentFrequency";
			this._ucPaymentFrequency.Size = new System.Drawing.Size(325, 23);
			this._ucPaymentFrequency.TabIndex = 23;
			// 
			// _ucRebateType
			// 
			this._ucRebateType.IncludeAllOption = false;
			this._ucRebateType.Location = new System.Drawing.Point(57, 342);
			this._ucRebateType.Name = "_ucRebateType";
			this._ucRebateType.Size = new System.Drawing.Size(325, 23);
			this._ucRebateType.TabIndex = 20;
			// 
			// _ucCustomerGroup
			// 
			this._ucCustomerGroup.AlignComboBoxToControl = null;
			this._ucCustomerGroup.ComboBoxWidth = 279;
			this._ucCustomerGroup.ControlSpacing = 6;
			this._ucCustomerGroup.DivisionId = 0;
			this._ucCustomerGroup.IncludeBlankOption = false;
			this._ucCustomerGroup.LabelText = "Customer Group";
			this._ucCustomerGroup.Location = new System.Drawing.Point(47, 101);
			this._ucCustomerGroup.Name = "_ucCustomerGroup";
			this._ucCustomerGroup.ReadOnly = false;
			this._ucCustomerGroup.Size = new System.Drawing.Size(441, 22);
			this._ucCustomerGroup.TabIndex = 7;
			this._ucCustomerGroup.TextBoxWidth = 52;
			// 
			// _ucSoldFrom
			// 
			this._ucSoldFrom.AlignComboBoxToControl = null;
			this._ucSoldFrom.Code = null;
			this._ucSoldFrom.ComboBoxWidth = 279;
			this._ucSoldFrom.ControlSpacing = 6;
			this._ucSoldFrom.DivisionId = 0;
			this._ucSoldFrom.IncludeAllOption = false;
			this._ucSoldFrom.LabelText = "Sold From";
			this._ucSoldFrom.Location = new System.Drawing.Point(76, 186);
			this._ucSoldFrom.Name = "_ucSoldFrom";
			this._ucSoldFrom.ReadOnly = false;
			this._ucSoldFrom.Size = new System.Drawing.Size(435, 22);
			this._ucSoldFrom.TabIndex = 11;
			this._ucSoldFrom.TextBoxWidth = 65;
			// 
			// _ucHierarchy
			// 
			this._ucHierarchy.AllItemText = "All";
			this._ucHierarchy.DivisionId = 0;
			this._ucHierarchy.IncludeAllOption = false;
			this._ucHierarchy.Location = new System.Drawing.Point(34, 72);
			this._ucHierarchy.Name = "_ucHierarchy";
			this._ucHierarchy.Size = new System.Drawing.Size(325, 23);
			this._ucHierarchy.TabIndex = 6;
			// 
			// _ucDeliveryType
			// 
			this._ucDeliveryType.Location = new System.Drawing.Point(55, 157);
			this._ucDeliveryType.Name = "_ucDeliveryType";
			this._ucDeliveryType.SelectedValue = Evolve.Clients.Lafarge.Cement.Business.Rebates.DeliveryTypeEnum.Null;
			this._ucDeliveryType.Size = new System.Drawing.Size(325, 23);
			this._ucDeliveryType.TabIndex = 10;
			// 
			// _ucPayee
			// 
			this._ucPayee.AccountNumber = null;
			this._ucPayee.DivisionId = 0;
			this._ucPayee.LabelText = "Payee";
			this._ucPayee.Location = new System.Drawing.Point(96, 129);
			this._ucPayee.MultiSelect = false;
			this._ucPayee.Name = "_ucPayee";
			this._ucPayee.Size = new System.Drawing.Size(432, 26);
			this._ucPayee.TabIndex = 9;
			// 
			// _ucCustomer
			// 
			this._ucCustomer.AccountNumber = null;
			this._ucCustomer.CustomerGroupId = null;
			this._ucCustomer.CustomerSearchModeEnum = Evolve.Clients.Lafarge.Cement.Business.Rebates.CustomerSearchModeEnum.SoldTo;
			this._ucCustomer.DivisionId = 0;
			this._ucCustomer.LabelText = "Customer";
			this._ucCustomer.Location = new System.Drawing.Point(79, 102);
			this._ucCustomer.MultiSelect = false;
			this._ucCustomer.Name = "_ucCustomer";
			this._ucCustomer.ParentCustomerId = null;
			this._ucCustomer.Size = new System.Drawing.Size(432, 26);
			this._ucCustomer.TabIndex = 8;
			// 
			// _lblIsJIF
			// 
			this._lblIsJIF.AutoSize = true;
			this._lblIsJIF.Location = new System.Drawing.Point(99, 292);
			this._lblIsJIF.Name = "_lblIsJIF";
			this._lblIsJIF.Size = new System.Drawing.Size(32, 13);
			this._lblIsJIF.TabIndex = 18;
			this._lblIsJIF.Text = "Is JIF";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this._isJIFAll);
			this.panel1.Controls.Add(this._isJIFNo);
			this.panel1.Controls.Add(this._isJIFYes);
			this.panel1.Location = new System.Drawing.Point(139, 287);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(150, 25);
			this.panel1.TabIndex = 70;
			// 
			// _isJIFAll
			// 
			this._isJIFAll.AutoSize = true;
			this._isJIFAll.Location = new System.Drawing.Point(3, 3);
			this._isJIFAll.Name = "_isJIFAll";
			this._isJIFAll.Size = new System.Drawing.Size(36, 17);
			this._isJIFAll.TabIndex = 0;
			this._isJIFAll.TabStop = true;
			this._isJIFAll.Text = "All";
			this._isJIFAll.UseVisualStyleBackColor = true;
			// 
			// _isJIFNo
			// 
			this._isJIFNo.AutoSize = true;
			this._isJIFNo.Location = new System.Drawing.Point(105, 3);
			this._isJIFNo.Name = "_isJIFNo";
			this._isJIFNo.Size = new System.Drawing.Size(39, 17);
			this._isJIFNo.TabIndex = 2;
			this._isJIFNo.TabStop = true;
			this._isJIFNo.Text = "No";
			this._isJIFNo.UseVisualStyleBackColor = true;
			// 
			// _isJIFYes
			// 
			this._isJIFYes.AutoSize = true;
			this._isJIFYes.Location = new System.Drawing.Point(56, 3);
			this._isJIFYes.Name = "_isJIFYes";
			this._isJIFYes.Size = new System.Drawing.Size(43, 17);
			this._isJIFYes.TabIndex = 1;
			this._isJIFYes.TabStop = true;
			this._isJIFYes.Text = "Yes";
			this._isJIFYes.UseVisualStyleBackColor = true;
			// 
			// RebateFind
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(539, 434);
			this.Controls.Add(this._lblIsJIF);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this._txtRebateDetailId);
			this.Controls.Add(this._lblRebateDetailId);
			this.Controls.Add(this._txtPeriod);
			this.Controls.Add(this._txtYear);
			this.Controls.Add(this._ucPaymentFrequency);
			this.Controls.Add(this._btnSearch);
			this.Controls.Add(this._lblIsGlobalProduct);
			this.Controls.Add(this._pnlIsGlobalProduct);
			this.Controls.Add(this._lblIsVAP);
			this.Controls.Add(this._pnlIsVAP);
			this.Controls.Add(this._lblIsContract);
			this.Controls.Add(this._pnlIsContract);
			this.Controls.Add(this._txtVersion);
			this.Controls.Add(this._ucRebateType);
			this.Controls.Add(this._lblVersion);
			this.Controls.Add(this._ucCustomerGroup);
			this.Controls.Add(this._lblPeriod);
			this.Controls.Add(this._lblRebateDate);
			this.Controls.Add(this._ucSoldFrom);
			this.Controls.Add(this._ucHierarchy);
			this.Controls.Add(this._ucDeliveryType);
			this.Controls.Add(this._ucPayee);
			this.Controls.Add(this._ucCustomer);
			this.Controls.Add(this._txtDescription);
			this.Controls.Add(this._txtRebateNumber);
			this.Controls.Add(this._lblDescription);
			this.Controls.Add(this._lblRebateNumber);
			this.Name = "RebateFind";
			this.Text = "Find Rebate";
			this._pnlIsContract.ResumeLayout(false);
			this._pnlIsContract.PerformLayout();
			this._pnlIsVAP.ResumeLayout(false);
			this._pnlIsVAP.PerformLayout();
			this._pnlIsGlobalProduct.ResumeLayout(false);
			this._pnlIsGlobalProduct.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this._errorProvider)).EndInit();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		protected UserControls.Rebates.RebateType _ucRebateType;
		protected System.Windows.Forms.Label _lblVersion;
		protected UserControls.Rebates.CustomerGroup _ucCustomerGroup;
		protected System.Windows.Forms.Label _lblPeriod;
		protected System.Windows.Forms.Label _lblRebateDate;
		protected UserControls.Rebates.Site _ucSoldFrom;
		protected UserControls.Rebates.Hierarchy _ucHierarchy;
		protected UserControls.Rebates.DeliveryType _ucDeliveryType;
		protected UserControls.Rebates.Payee _ucPayee;
		protected UserControls.Rebates.Customer _ucCustomer;
		protected System.Windows.Forms.TextBox _txtDescription;
		protected System.Windows.Forms.TextBox _txtRebateNumber;
		protected System.Windows.Forms.Label _lblDescription;
		protected System.Windows.Forms.Label _lblRebateNumber;
		protected System.Windows.Forms.TextBox _txtVersion;
		protected System.Windows.Forms.RadioButton _isContractAll;
		protected System.Windows.Forms.RadioButton _isContractYes;
		protected System.Windows.Forms.RadioButton _isContractNo;
		protected System.Windows.Forms.Panel _pnlIsContract;
		protected System.Windows.Forms.Label _lblIsContract;
		protected System.Windows.Forms.Label _lblIsVAP;
		protected System.Windows.Forms.Panel _pnlIsVAP;
		protected System.Windows.Forms.RadioButton _isVAPAll;
		protected System.Windows.Forms.RadioButton _isVAPNo;
		protected System.Windows.Forms.RadioButton _isVAPYes;
		protected System.Windows.Forms.Label _lblIsGlobalProduct;
		protected System.Windows.Forms.Panel _pnlIsGlobalProduct;
		protected System.Windows.Forms.RadioButton _isGlobalProductAll;
		protected System.Windows.Forms.RadioButton _isGlobalProductNo;
		protected System.Windows.Forms.RadioButton _isGlobalProductYes;
		protected System.Windows.Forms.Button _btnSearch;
		protected UserControls.Rebates.PaymentFrequency _ucPaymentFrequency;
		protected System.Windows.Forms.ErrorProvider _errorProvider;
		protected System.Windows.Forms.TextBox _txtPeriod;
		protected System.Windows.Forms.TextBox _txtYear;
		protected System.Windows.Forms.TextBox _txtRebateDetailId;
		protected System.Windows.Forms.Label _lblRebateDetailId;
		protected System.Windows.Forms.Label _lblIsJIF;
		protected System.Windows.Forms.Panel panel1;
		protected System.Windows.Forms.RadioButton _isJIFAll;
		protected System.Windows.Forms.RadioButton _isJIFNo;
		protected System.Windows.Forms.RadioButton _isJIFYes;
	}
}
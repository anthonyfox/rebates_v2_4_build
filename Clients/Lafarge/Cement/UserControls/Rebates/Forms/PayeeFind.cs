﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates
{
	public partial class PayeeFind : Form
	{
		private Int32 _divisionId = 0;
		private Boolean _multiSelect = false;
		private Int32 _searchResultLimit = 500;
		private List<Business.Rebates.Payee> _selectedPayees;

		public Int32 DivisionId
		{
			get { return _divisionId; }
		}

		public Int32 SearchResultLimit
		{
			get { return _searchResultLimit; }
			set { _searchResultLimit = value; }
		}

		public List<Business.Rebates.Payee> SelectedPayees
		{
			get { return _selectedPayees; }
		}

		public PayeeFind(Int32 divisionId, Boolean multiSelect)
		{
			_divisionId = divisionId;
			_multiSelect = multiSelect;

			InitializeComponent();

			_payees.KeyPress += new KeyPressEventHandler(_payees_KeyPress);
			_payees.CellContentDoubleClick += new DataGridViewCellEventHandler(_payees_CellContentDoubleClick);
			buttonOk.Click += new EventHandler(buttonAccept_Click);
		}

		private void PayeeFind_Load(object sender, EventArgs e)
		{
			_payees.MultiSelect = true;
			_payees.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			_payees.ReadOnly = true;
			_payees.AllowUserToAddRows = false;
			_payees.AllowUserToDeleteRows = false;
			_payees.AllowUserToOrderColumns = false;
			_payees.Visible = false;
		}

		void _payees_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
		{
			PayeeSelected();
		}

		void _payees_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == (char) Keys.Return)
			{
				PayeeSelected();
			}
		}

		void _clear_Click(object sender, EventArgs e)
		{
			_name.Text = "";
			_address1.Text = "";
			_address2.Text = "";
			_address3.Text = "";
			_address4.Text = "";
			_city.Text = "";
			_county.Text = "";
			_postCode.Text = "";
			_taxNumber.Text = "";

			_payees.Visible = false;
			_name.Focus();
		}

		private void _search_Click(object sender, EventArgs e)
		{
			_search.Enabled = false;
			this.Cursor = Cursors.WaitCursor;

			Boolean isRowLimitExceeded = false;
			Business.Rebates.Payee payee = new Business.Rebates.Payee();
			payee.DivisionId = _divisionId;

			_payees.DataSource = payee.Search(_name.Text, _address1.Text, _address2.Text, _address3.Text, _address4.Text, _city.Text, _county.Text, _postCode.Text, _taxNumber.Text, _searchResultLimit, out isRowLimitExceeded);
			_payees.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
			_payees.Visible = true;
			_payees.Focus();

			// Warn if truncated results set
			if (isRowLimitExceeded)
			{
				MessageBox.Show("Only the first " + _searchResultLimit.ToString() + " rows have been displayed", "Payee Search", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}

			Application.DoEvents();
			this.Cursor = Cursors.Default;
			_search.Enabled = true;
		}

		private void PayeeSelected()
		{
			_selectedPayees = new List<Business.Rebates.Payee>();

			if (_multiSelect)
			{
				foreach (DataGridViewRow row in _payees.SelectedRows)
				{
					_selectedPayees.Add((Business.Rebates.Payee) row.DataBoundItem);
				}
			}
			else if (_payees.CurrentRow != null)
			{
				_selectedPayees.Add((Business.Rebates.Payee) _payees.CurrentRow.DataBoundItem);
			}

			if (_selectedPayees.Count == 0)
			{
				_selectedPayees = null;
			}

			this.Close();
		}

		void buttonAccept_Click(object sender, EventArgs e)
		{
			if (_payees.Visible && _payees.SelectedRows.Count > 0)
			{
				PayeeSelected();
			}
		}
	}
}

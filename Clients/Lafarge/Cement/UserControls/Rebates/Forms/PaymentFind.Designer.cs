﻿namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates
{
	partial class PaymentFind
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this._lblIsJIF = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this._isJIFAll = new System.Windows.Forms.RadioButton();
			this._isJIFNo = new System.Windows.Forms.RadioButton();
			this._isJIFYes = new System.Windows.Forms.RadioButton();
			this._txtRebateDetailId = new System.Windows.Forms.TextBox();
			this._lblRebateDetailId = new System.Windows.Forms.Label();
			this._txtPeriod = new System.Windows.Forms.TextBox();
			this._txtYear = new System.Windows.Forms.TextBox();
			this._btnSearch = new System.Windows.Forms.Button();
			this._lblIsGlobalProduct = new System.Windows.Forms.Label();
			this._pnlIsGlobalProduct = new System.Windows.Forms.Panel();
			this._isGlobalProductAll = new System.Windows.Forms.RadioButton();
			this._isGlobalProductNo = new System.Windows.Forms.RadioButton();
			this._isGlobalProductYes = new System.Windows.Forms.RadioButton();
			this._lblIsVAP = new System.Windows.Forms.Label();
			this._pnlIsVAP = new System.Windows.Forms.Panel();
			this._isVAPAll = new System.Windows.Forms.RadioButton();
			this._isVAPNo = new System.Windows.Forms.RadioButton();
			this._isVAPYes = new System.Windows.Forms.RadioButton();
			this._lblIsContract = new System.Windows.Forms.Label();
			this._pnlIsContract = new System.Windows.Forms.Panel();
			this._isContractAll = new System.Windows.Forms.RadioButton();
			this._isContractNo = new System.Windows.Forms.RadioButton();
			this._isContractYes = new System.Windows.Forms.RadioButton();
			this._txtVersion = new System.Windows.Forms.TextBox();
			this._lblVersion = new System.Windows.Forms.Label();
			this._lblPeriod = new System.Windows.Forms.Label();
			this._lblRebateDate = new System.Windows.Forms.Label();
			this._txtDescription = new System.Windows.Forms.TextBox();
			this._txtRebateNumber = new System.Windows.Forms.TextBox();
			this._lblDescription = new System.Windows.Forms.Label();
			this._lblRebateNumber = new System.Windows.Forms.Label();
			this._errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
			this._lblTaxDate = new System.Windows.Forms.Label();
			this._lblTo = new System.Windows.Forms.Label();
			this._lblTaxCode = new System.Windows.Forms.Label();
			this._dtpTaxDateTo = new Evolve.Libraries.Controls.Generic.DateTimePickerNull();
			this._dtpTaxDateFrom = new Evolve.Libraries.Controls.Generic.DateTimePickerNull();
			this._ucPaymentStatus = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.PaymentStatus();
			this._ucTaxCode = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Lookups.TaxCode();
			this._ucPaymentPayee = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Payee();
			this._ucPaymentFrequency = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.PaymentFrequency();
			this._ucRebateType = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.RebateType();
			this._ucCustomerGroup = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.CustomerGroup();
			this._ucSoldFrom = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Site();
			this._ucHierarchy = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Hierarchy();
			this._ucDeliveryType = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.DeliveryType();
			this._ucPayee = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Payee();
			this._ucCustomer = new Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Customer();
			this.panel1.SuspendLayout();
			this._pnlIsGlobalProduct.SuspendLayout();
			this._pnlIsVAP.SuspendLayout();
			this._pnlIsContract.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this._errorProvider)).BeginInit();
			this.SuspendLayout();
			// 
			// _lblIsJIF
			// 
			this._lblIsJIF.AutoSize = true;
			this._lblIsJIF.Location = new System.Drawing.Point(95, 295);
			this._lblIsJIF.Name = "_lblIsJIF";
			this._lblIsJIF.Size = new System.Drawing.Size(32, 13);
			this._lblIsJIF.TabIndex = 89;
			this._lblIsJIF.Text = "Is JIF";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this._isJIFAll);
			this.panel1.Controls.Add(this._isJIFNo);
			this.panel1.Controls.Add(this._isJIFYes);
			this.panel1.Location = new System.Drawing.Point(135, 290);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(150, 25);
			this.panel1.TabIndex = 99;
			// 
			// _isJIFAll
			// 
			this._isJIFAll.AutoSize = true;
			this._isJIFAll.Location = new System.Drawing.Point(3, 3);
			this._isJIFAll.Name = "_isJIFAll";
			this._isJIFAll.Size = new System.Drawing.Size(36, 17);
			this._isJIFAll.TabIndex = 0;
			this._isJIFAll.TabStop = true;
			this._isJIFAll.Text = "All";
			this._isJIFAll.UseVisualStyleBackColor = true;
			// 
			// _isJIFNo
			// 
			this._isJIFNo.AutoSize = true;
			this._isJIFNo.Location = new System.Drawing.Point(105, 3);
			this._isJIFNo.Name = "_isJIFNo";
			this._isJIFNo.Size = new System.Drawing.Size(39, 17);
			this._isJIFNo.TabIndex = 2;
			this._isJIFNo.TabStop = true;
			this._isJIFNo.Text = "No";
			this._isJIFNo.UseVisualStyleBackColor = true;
			// 
			// _isJIFYes
			// 
			this._isJIFYes.AutoSize = true;
			this._isJIFYes.Location = new System.Drawing.Point(56, 3);
			this._isJIFYes.Name = "_isJIFYes";
			this._isJIFYes.Size = new System.Drawing.Size(43, 17);
			this._isJIFYes.TabIndex = 1;
			this._isJIFYes.TabStop = true;
			this._isJIFYes.Text = "Yes";
			this._isJIFYes.UseVisualStyleBackColor = true;
			// 
			// _txtRebateDetailId
			// 
			this._txtRebateDetailId.Location = new System.Drawing.Point(134, 371);
			this._txtRebateDetailId.Name = "_txtRebateDetailId";
			this._txtRebateDetailId.Size = new System.Drawing.Size(50, 20);
			this._txtRebateDetailId.TabIndex = 93;
			// 
			// _lblRebateDetailId
			// 
			this._lblRebateDetailId.AutoSize = true;
			this._lblRebateDetailId.Location = new System.Drawing.Point(44, 374);
			this._lblRebateDetailId.Name = "_lblRebateDetailId";
			this._lblRebateDetailId.Size = new System.Drawing.Size(84, 13);
			this._lblRebateDetailId.TabIndex = 92;
			this._lblRebateDetailId.Text = "Rebate Detail Id";
			// 
			// _txtPeriod
			// 
			this._txtPeriod.Location = new System.Drawing.Point(245, 217);
			this._txtPeriod.Name = "_txtPeriod";
			this._txtPeriod.Size = new System.Drawing.Size(20, 20);
			this._txtPeriod.TabIndex = 86;
			// 
			// _txtYear
			// 
			this._txtYear.Location = new System.Drawing.Point(135, 217);
			this._txtYear.Name = "_txtYear";
			this._txtYear.Size = new System.Drawing.Size(50, 20);
			this._txtYear.TabIndex = 84;
			// 
			// _btnSearch
			// 
			this._btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._btnSearch.Location = new System.Drawing.Point(485, 525);
			this._btnSearch.Name = "_btnSearch";
			this._btnSearch.Size = new System.Drawing.Size(75, 23);
			this._btnSearch.TabIndex = 95;
			this._btnSearch.Text = "Search";
			this._btnSearch.UseVisualStyleBackColor = true;
			// 
			// _lblIsGlobalProduct
			// 
			this._lblIsGlobalProduct.AutoSize = true;
			this._lblIsGlobalProduct.Location = new System.Drawing.Point(54, 319);
			this._lblIsGlobalProduct.Name = "_lblIsGlobalProduct";
			this._lblIsGlobalProduct.Size = new System.Drawing.Size(75, 13);
			this._lblIsGlobalProduct.TabIndex = 90;
			this._lblIsGlobalProduct.Text = "Global Rebate";
			// 
			// _pnlIsGlobalProduct
			// 
			this._pnlIsGlobalProduct.Controls.Add(this._isGlobalProductAll);
			this._pnlIsGlobalProduct.Controls.Add(this._isGlobalProductNo);
			this._pnlIsGlobalProduct.Controls.Add(this._isGlobalProductYes);
			this._pnlIsGlobalProduct.Location = new System.Drawing.Point(135, 314);
			this._pnlIsGlobalProduct.Name = "_pnlIsGlobalProduct";
			this._pnlIsGlobalProduct.Size = new System.Drawing.Size(150, 25);
			this._pnlIsGlobalProduct.TabIndex = 98;
			// 
			// _isGlobalProductAll
			// 
			this._isGlobalProductAll.AutoSize = true;
			this._isGlobalProductAll.Location = new System.Drawing.Point(3, 3);
			this._isGlobalProductAll.Name = "_isGlobalProductAll";
			this._isGlobalProductAll.Size = new System.Drawing.Size(36, 17);
			this._isGlobalProductAll.TabIndex = 0;
			this._isGlobalProductAll.TabStop = true;
			this._isGlobalProductAll.Text = "All";
			this._isGlobalProductAll.UseVisualStyleBackColor = true;
			// 
			// _isGlobalProductNo
			// 
			this._isGlobalProductNo.AutoSize = true;
			this._isGlobalProductNo.Location = new System.Drawing.Point(105, 3);
			this._isGlobalProductNo.Name = "_isGlobalProductNo";
			this._isGlobalProductNo.Size = new System.Drawing.Size(39, 17);
			this._isGlobalProductNo.TabIndex = 2;
			this._isGlobalProductNo.TabStop = true;
			this._isGlobalProductNo.Text = "No";
			this._isGlobalProductNo.UseVisualStyleBackColor = true;
			// 
			// _isGlobalProductYes
			// 
			this._isGlobalProductYes.AutoSize = true;
			this._isGlobalProductYes.Location = new System.Drawing.Point(56, 3);
			this._isGlobalProductYes.Name = "_isGlobalProductYes";
			this._isGlobalProductYes.Size = new System.Drawing.Size(43, 17);
			this._isGlobalProductYes.TabIndex = 1;
			this._isGlobalProductYes.TabStop = true;
			this._isGlobalProductYes.Text = "Yes";
			this._isGlobalProductYes.UseVisualStyleBackColor = true;
			// 
			// _lblIsVAP
			// 
			this._lblIsVAP.AutoSize = true;
			this._lblIsVAP.Location = new System.Drawing.Point(90, 271);
			this._lblIsVAP.Name = "_lblIsVAP";
			this._lblIsVAP.Size = new System.Drawing.Size(39, 13);
			this._lblIsVAP.TabIndex = 88;
			this._lblIsVAP.Text = "Is VAP";
			// 
			// _pnlIsVAP
			// 
			this._pnlIsVAP.Controls.Add(this._isVAPAll);
			this._pnlIsVAP.Controls.Add(this._isVAPNo);
			this._pnlIsVAP.Controls.Add(this._isVAPYes);
			this._pnlIsVAP.Location = new System.Drawing.Point(135, 266);
			this._pnlIsVAP.Name = "_pnlIsVAP";
			this._pnlIsVAP.Size = new System.Drawing.Size(150, 25);
			this._pnlIsVAP.TabIndex = 97;
			// 
			// _isVAPAll
			// 
			this._isVAPAll.AutoSize = true;
			this._isVAPAll.Location = new System.Drawing.Point(3, 3);
			this._isVAPAll.Name = "_isVAPAll";
			this._isVAPAll.Size = new System.Drawing.Size(36, 17);
			this._isVAPAll.TabIndex = 0;
			this._isVAPAll.TabStop = true;
			this._isVAPAll.Text = "All";
			this._isVAPAll.UseVisualStyleBackColor = true;
			// 
			// _isVAPNo
			// 
			this._isVAPNo.AutoSize = true;
			this._isVAPNo.Location = new System.Drawing.Point(105, 3);
			this._isVAPNo.Name = "_isVAPNo";
			this._isVAPNo.Size = new System.Drawing.Size(39, 17);
			this._isVAPNo.TabIndex = 2;
			this._isVAPNo.TabStop = true;
			this._isVAPNo.Text = "No";
			this._isVAPNo.UseVisualStyleBackColor = true;
			// 
			// _isVAPYes
			// 
			this._isVAPYes.AutoSize = true;
			this._isVAPYes.Location = new System.Drawing.Point(56, 3);
			this._isVAPYes.Name = "_isVAPYes";
			this._isVAPYes.Size = new System.Drawing.Size(43, 17);
			this._isVAPYes.TabIndex = 1;
			this._isVAPYes.TabStop = true;
			this._isVAPYes.Text = "Yes";
			this._isVAPYes.UseVisualStyleBackColor = true;
			// 
			// _lblIsContract
			// 
			this._lblIsContract.AutoSize = true;
			this._lblIsContract.Location = new System.Drawing.Point(71, 248);
			this._lblIsContract.Name = "_lblIsContract";
			this._lblIsContract.Size = new System.Drawing.Size(58, 13);
			this._lblIsContract.TabIndex = 87;
			this._lblIsContract.Text = "Is Contract";
			// 
			// _pnlIsContract
			// 
			this._pnlIsContract.Controls.Add(this._isContractAll);
			this._pnlIsContract.Controls.Add(this._isContractNo);
			this._pnlIsContract.Controls.Add(this._isContractYes);
			this._pnlIsContract.Location = new System.Drawing.Point(135, 243);
			this._pnlIsContract.Name = "_pnlIsContract";
			this._pnlIsContract.Size = new System.Drawing.Size(150, 25);
			this._pnlIsContract.TabIndex = 96;
			// 
			// _isContractAll
			// 
			this._isContractAll.AutoSize = true;
			this._isContractAll.Location = new System.Drawing.Point(3, 3);
			this._isContractAll.Name = "_isContractAll";
			this._isContractAll.Size = new System.Drawing.Size(36, 17);
			this._isContractAll.TabIndex = 0;
			this._isContractAll.TabStop = true;
			this._isContractAll.Text = "All";
			this._isContractAll.UseVisualStyleBackColor = true;
			// 
			// _isContractNo
			// 
			this._isContractNo.AutoSize = true;
			this._isContractNo.Location = new System.Drawing.Point(105, 3);
			this._isContractNo.Name = "_isContractNo";
			this._isContractNo.Size = new System.Drawing.Size(39, 17);
			this._isContractNo.TabIndex = 2;
			this._isContractNo.TabStop = true;
			this._isContractNo.Text = "No";
			this._isContractNo.UseVisualStyleBackColor = true;
			// 
			// _isContractYes
			// 
			this._isContractYes.AutoSize = true;
			this._isContractYes.Location = new System.Drawing.Point(56, 3);
			this._isContractYes.Name = "_isContractYes";
			this._isContractYes.Size = new System.Drawing.Size(43, 17);
			this._isContractYes.TabIndex = 1;
			this._isContractYes.TabStop = true;
			this._isContractYes.Text = "Yes";
			this._isContractYes.UseVisualStyleBackColor = true;
			// 
			// _txtVersion
			// 
			this._txtVersion.Location = new System.Drawing.Point(255, 23);
			this._txtVersion.Name = "_txtVersion";
			this._txtVersion.Size = new System.Drawing.Size(50, 20);
			this._txtVersion.TabIndex = 74;
			// 
			// _lblVersion
			// 
			this._lblVersion.AutoSize = true;
			this._lblVersion.Location = new System.Drawing.Point(207, 26);
			this._lblVersion.Name = "_lblVersion";
			this._lblVersion.Size = new System.Drawing.Size(42, 13);
			this._lblVersion.TabIndex = 73;
			this._lblVersion.Text = "Version";
			// 
			// _lblPeriod
			// 
			this._lblPeriod.AutoSize = true;
			this._lblPeriod.Location = new System.Drawing.Point(207, 220);
			this._lblPeriod.Name = "_lblPeriod";
			this._lblPeriod.Size = new System.Drawing.Size(37, 13);
			this._lblPeriod.TabIndex = 85;
			this._lblPeriod.Text = "Period";
			// 
			// _lblRebateDate
			// 
			this._lblRebateDate.AutoSize = true;
			this._lblRebateDate.Location = new System.Drawing.Point(55, 221);
			this._lblRebateDate.Name = "_lblRebateDate";
			this._lblRebateDate.Size = new System.Drawing.Size(74, 13);
			this._lblRebateDate.TabIndex = 83;
			this._lblRebateDate.Text = "Active In Year";
			// 
			// _txtDescription
			// 
			this._txtDescription.Location = new System.Drawing.Point(135, 49);
			this._txtDescription.Name = "_txtDescription";
			this._txtDescription.Size = new System.Drawing.Size(300, 20);
			this._txtDescription.TabIndex = 76;
			// 
			// _txtRebateNumber
			// 
			this._txtRebateNumber.Location = new System.Drawing.Point(135, 23);
			this._txtRebateNumber.Name = "_txtRebateNumber";
			this._txtRebateNumber.Size = new System.Drawing.Size(50, 20);
			this._txtRebateNumber.TabIndex = 72;
			// 
			// _lblDescription
			// 
			this._lblDescription.AutoSize = true;
			this._lblDescription.Location = new System.Drawing.Point(69, 52);
			this._lblDescription.Name = "_lblDescription";
			this._lblDescription.Size = new System.Drawing.Size(60, 13);
			this._lblDescription.TabIndex = 75;
			this._lblDescription.Text = "Description";
			// 
			// _lblRebateNumber
			// 
			this._lblRebateNumber.AutoSize = true;
			this._lblRebateNumber.Location = new System.Drawing.Point(47, 26);
			this._lblRebateNumber.Name = "_lblRebateNumber";
			this._lblRebateNumber.Size = new System.Drawing.Size(82, 13);
			this._lblRebateNumber.TabIndex = 71;
			this._lblRebateNumber.Text = "Rebate Number";
			// 
			// _errorProvider
			// 
			this._errorProvider.ContainerControl = this;
			// 
			// _lblTaxDate
			// 
			this._lblTaxDate.AutoSize = true;
			this._lblTaxDate.Location = new System.Drawing.Point(76, 472);
			this._lblTaxDate.Name = "_lblTaxDate";
			this._lblTaxDate.Size = new System.Drawing.Size(51, 13);
			this._lblTaxDate.TabIndex = 101;
			this._lblTaxDate.Text = "Tax Date";
			// 
			// _lblTo
			// 
			this._lblTo.AutoSize = true;
			this._lblTo.Location = new System.Drawing.Point(237, 472);
			this._lblTo.Name = "_lblTo";
			this._lblTo.Size = new System.Drawing.Size(20, 13);
			this._lblTo.TabIndex = 104;
			this._lblTo.Text = "To";
			// 
			// _lblTaxCode
			// 
			this._lblTaxCode.AutoSize = true;
			this._lblTaxCode.Location = new System.Drawing.Point(75, 501);
			this._lblTaxCode.Name = "_lblTaxCode";
			this._lblTaxCode.Size = new System.Drawing.Size(53, 13);
			this._lblTaxCode.TabIndex = 107;
			this._lblTaxCode.Text = "Tax Code";
			// 
			// _dtpTaxDateTo
			// 
			this._dtpTaxDateTo.CustomFormat = " ";
			this._dtpTaxDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this._dtpTaxDateTo.Location = new System.Drawing.Point(259, 469);
			this._dtpTaxDateTo.Name = "_dtpTaxDateTo";
			this._dtpTaxDateTo.Size = new System.Drawing.Size(99, 20);
			this._dtpTaxDateTo.TabIndex = 103;
			this._dtpTaxDateTo.Value = new System.DateTime(2013, 3, 28, 12, 8, 28, 338);
			this._dtpTaxDateTo.ValueNotNull = new System.DateTime(2013, 3, 28, 12, 8, 28, 338);
			// 
			// _dtpTaxDateFrom
			// 
			this._dtpTaxDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this._dtpTaxDateFrom.Location = new System.Drawing.Point(134, 469);
			this._dtpTaxDateFrom.Name = "_dtpTaxDateFrom";
			this._dtpTaxDateFrom.Size = new System.Drawing.Size(99, 20);
			this._dtpTaxDateFrom.TabIndex = 102;
			this._dtpTaxDateFrom.Value = new System.DateTime(2013, 3, 28, 12, 8, 28, 338);
			this._dtpTaxDateFrom.ValueNotNull = new System.DateTime(2013, 3, 28, 12, 8, 28, 338);
			// 
			// _ucPaymentStatus
			// 
			this._ucPaymentStatus.Location = new System.Drawing.Point(41, 522);
			this._ucPaymentStatus.Name = "_ucPaymentStatus";
			this._ucPaymentStatus.SelectedValue = Evolve.Clients.Lafarge.Cement.Business.Rebates.PaymentStatusEnum.Null;
			this._ucPaymentStatus.Size = new System.Drawing.Size(325, 23);
			this._ucPaymentStatus.TabIndex = 106;
			// 
			// _ucTaxCode
			// 
			this._ucTaxCode.IncludeAllOption = false;
			this._ucTaxCode.IsZeroRateOnly = false;
			this._ucTaxCode.Location = new System.Drawing.Point(134, 496);
			this._ucTaxCode.Name = "_ucTaxCode";
			this._ucTaxCode.Size = new System.Drawing.Size(152, 21);
			this._ucTaxCode.TabIndex = 105;
			// 
			// _ucPaymentPayee
			// 
			this._ucPaymentPayee.AccountNumber = null;
			this._ucPaymentPayee.DivisionId = 0;
			this._ucPaymentPayee.LabelText = "Payment Payee";
			this._ucPaymentPayee.Location = new System.Drawing.Point(47, 442);
			this._ucPaymentPayee.MultiSelect = false;
			this._ucPaymentPayee.Name = "_ucPaymentPayee";
			this._ucPaymentPayee.Size = new System.Drawing.Size(460, 26);
			this._ucPaymentPayee.TabIndex = 100;
			// 
			// _ucPaymentFrequency
			// 
			this._ucPaymentFrequency.AllItemText = "All";
			this._ucPaymentFrequency.DivisionId = 0;
			this._ucPaymentFrequency.IncludeAllOption = false;
			this._ucPaymentFrequency.Location = new System.Drawing.Point(21, 395);
			this._ucPaymentFrequency.Name = "_ucPaymentFrequency";
			this._ucPaymentFrequency.Size = new System.Drawing.Size(325, 23);
			this._ucPaymentFrequency.TabIndex = 94;
			// 
			// _ucRebateType
			// 
			this._ucRebateType.IncludeAllOption = false;
			this._ucRebateType.Location = new System.Drawing.Point(53, 345);
			this._ucRebateType.Name = "_ucRebateType";
			this._ucRebateType.Size = new System.Drawing.Size(325, 23);
			this._ucRebateType.TabIndex = 91;
			// 
			// _ucCustomerGroup
			// 
			this._ucCustomerGroup.AlignComboBoxToControl = null;
			this._ucCustomerGroup.ComboBoxWidth = 279;
			this._ucCustomerGroup.ControlSpacing = 6;
			this._ucCustomerGroup.DivisionId = 0;
			this._ucCustomerGroup.IncludeBlankOption = false;
			this._ucCustomerGroup.LabelText = "Customer Group";
			this._ucCustomerGroup.Location = new System.Drawing.Point(43, 104);
			this._ucCustomerGroup.Name = "_ucCustomerGroup";
			this._ucCustomerGroup.ReadOnly = false;
			this._ucCustomerGroup.Size = new System.Drawing.Size(441, 22);
			this._ucCustomerGroup.TabIndex = 78;
			this._ucCustomerGroup.TextBoxWidth = 52;
			// 
			// _ucSoldFrom
			// 
			this._ucSoldFrom.AlignComboBoxToControl = null;
			this._ucSoldFrom.Code = null;
			this._ucSoldFrom.ComboBoxWidth = 279;
			this._ucSoldFrom.ControlSpacing = 6;
			this._ucSoldFrom.DivisionId = 0;
			this._ucSoldFrom.IncludeAllOption = false;
			this._ucSoldFrom.LabelText = "Sold From";
			this._ucSoldFrom.Location = new System.Drawing.Point(72, 189);
			this._ucSoldFrom.Name = "_ucSoldFrom";
			this._ucSoldFrom.ReadOnly = false;
			this._ucSoldFrom.Size = new System.Drawing.Size(435, 22);
			this._ucSoldFrom.TabIndex = 82;
			this._ucSoldFrom.TextBoxWidth = 65;
			// 
			// _ucHierarchy
			// 
			this._ucHierarchy.AllItemText = "All";
			this._ucHierarchy.DivisionId = 0;
			this._ucHierarchy.IncludeAllOption = false;
			this._ucHierarchy.Location = new System.Drawing.Point(30, 75);
			this._ucHierarchy.Name = "_ucHierarchy";
			this._ucHierarchy.Size = new System.Drawing.Size(325, 23);
			this._ucHierarchy.TabIndex = 77;
			// 
			// _ucDeliveryType
			// 
			this._ucDeliveryType.Location = new System.Drawing.Point(51, 160);
			this._ucDeliveryType.Name = "_ucDeliveryType";
			this._ucDeliveryType.SelectedValue = Evolve.Clients.Lafarge.Cement.Business.Rebates.DeliveryTypeEnum.Null;
			this._ucDeliveryType.Size = new System.Drawing.Size(325, 23);
			this._ucDeliveryType.TabIndex = 81;
			// 
			// _ucPayee
			// 
			this._ucPayee.AccountNumber = null;
			this._ucPayee.DivisionId = 0;
			this._ucPayee.LabelText = "Header Payee";
			this._ucPayee.Location = new System.Drawing.Point(54, 132);
			this._ucPayee.MultiSelect = false;
			this._ucPayee.Name = "_ucPayee";
			this._ucPayee.Size = new System.Drawing.Size(460, 26);
			this._ucPayee.TabIndex = 80;
			// 
			// _ucCustomer
			// 
			this._ucCustomer.AccountNumber = null;
			this._ucCustomer.CustomerGroupId = null;
			this._ucCustomer.CustomerSearchModeEnum = Evolve.Clients.Lafarge.Cement.Business.Rebates.CustomerSearchModeEnum.SoldTo;
			this._ucCustomer.DivisionId = 0;
			this._ucCustomer.LabelText = "Customer";
			this._ucCustomer.Location = new System.Drawing.Point(75, 105);
			this._ucCustomer.MultiSelect = false;
			this._ucCustomer.Name = "_ucCustomer";
			this._ucCustomer.ParentCustomerId = null;
			this._ucCustomer.Size = new System.Drawing.Size(432, 26);
			this._ucCustomer.TabIndex = 79;
			// 
			// PaymentFind
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(572, 560);
			this.Controls.Add(this._lblTaxCode);
			this.Controls.Add(this._ucPaymentStatus);
			this.Controls.Add(this._ucTaxCode);
			this.Controls.Add(this._lblTo);
			this.Controls.Add(this._dtpTaxDateTo);
			this.Controls.Add(this._dtpTaxDateFrom);
			this.Controls.Add(this._lblTaxDate);
			this.Controls.Add(this._ucPaymentPayee);
			this.Controls.Add(this._lblIsJIF);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this._txtRebateDetailId);
			this.Controls.Add(this._lblRebateDetailId);
			this.Controls.Add(this._txtPeriod);
			this.Controls.Add(this._txtYear);
			this.Controls.Add(this._ucPaymentFrequency);
			this.Controls.Add(this._btnSearch);
			this.Controls.Add(this._lblIsGlobalProduct);
			this.Controls.Add(this._pnlIsGlobalProduct);
			this.Controls.Add(this._lblIsVAP);
			this.Controls.Add(this._pnlIsVAP);
			this.Controls.Add(this._lblIsContract);
			this.Controls.Add(this._pnlIsContract);
			this.Controls.Add(this._txtVersion);
			this.Controls.Add(this._ucRebateType);
			this.Controls.Add(this._lblVersion);
			this.Controls.Add(this._ucCustomerGroup);
			this.Controls.Add(this._lblPeriod);
			this.Controls.Add(this._lblRebateDate);
			this.Controls.Add(this._ucSoldFrom);
			this.Controls.Add(this._ucHierarchy);
			this.Controls.Add(this._ucDeliveryType);
			this.Controls.Add(this._ucPayee);
			this.Controls.Add(this._ucCustomer);
			this.Controls.Add(this._txtDescription);
			this.Controls.Add(this._txtRebateNumber);
			this.Controls.Add(this._lblDescription);
			this.Controls.Add(this._lblRebateNumber);
			this.Name = "PaymentFind";
			this.Text = "Find Payments";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this._pnlIsGlobalProduct.ResumeLayout(false);
			this._pnlIsGlobalProduct.PerformLayout();
			this._pnlIsVAP.ResumeLayout(false);
			this._pnlIsVAP.PerformLayout();
			this._pnlIsContract.ResumeLayout(false);
			this._pnlIsContract.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this._errorProvider)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		protected System.Windows.Forms.Label _lblIsJIF;
		protected System.Windows.Forms.Panel panel1;
		protected System.Windows.Forms.RadioButton _isJIFAll;
		protected System.Windows.Forms.RadioButton _isJIFNo;
		protected System.Windows.Forms.RadioButton _isJIFYes;
		protected System.Windows.Forms.TextBox _txtRebateDetailId;
		protected System.Windows.Forms.Label _lblRebateDetailId;
		protected System.Windows.Forms.TextBox _txtPeriod;
		protected System.Windows.Forms.TextBox _txtYear;
		protected PaymentFrequency _ucPaymentFrequency;
		protected System.Windows.Forms.Button _btnSearch;
		protected System.Windows.Forms.Label _lblIsGlobalProduct;
		protected System.Windows.Forms.Panel _pnlIsGlobalProduct;
		protected System.Windows.Forms.RadioButton _isGlobalProductAll;
		protected System.Windows.Forms.RadioButton _isGlobalProductNo;
		protected System.Windows.Forms.RadioButton _isGlobalProductYes;
		protected System.Windows.Forms.Label _lblIsVAP;
		protected System.Windows.Forms.Panel _pnlIsVAP;
		protected System.Windows.Forms.RadioButton _isVAPAll;
		protected System.Windows.Forms.RadioButton _isVAPNo;
		protected System.Windows.Forms.RadioButton _isVAPYes;
		protected System.Windows.Forms.Label _lblIsContract;
		protected System.Windows.Forms.Panel _pnlIsContract;
		protected System.Windows.Forms.RadioButton _isContractAll;
		protected System.Windows.Forms.RadioButton _isContractNo;
		protected System.Windows.Forms.RadioButton _isContractYes;
		protected System.Windows.Forms.TextBox _txtVersion;
		protected RebateType _ucRebateType;
		protected System.Windows.Forms.Label _lblVersion;
		protected CustomerGroup _ucCustomerGroup;
		protected System.Windows.Forms.Label _lblPeriod;
		protected System.Windows.Forms.Label _lblRebateDate;
		protected Site _ucSoldFrom;
		protected Hierarchy _ucHierarchy;
		protected DeliveryType _ucDeliveryType;
		protected Payee _ucPayee;
		protected Customer _ucCustomer;
		protected System.Windows.Forms.TextBox _txtDescription;
		protected System.Windows.Forms.TextBox _txtRebateNumber;
		protected System.Windows.Forms.Label _lblDescription;
		protected System.Windows.Forms.Label _lblRebateNumber;
		private System.Windows.Forms.ErrorProvider _errorProvider;
		private System.Windows.Forms.Label _lblTo;
		private Libraries.Controls.Generic.DateTimePickerNull _dtpTaxDateTo;
		private Libraries.Controls.Generic.DateTimePickerNull _dtpTaxDateFrom;
		private System.Windows.Forms.Label _lblTaxDate;
		protected Payee _ucPaymentPayee;
		private Lookups.TaxCode _ucTaxCode;
		private System.Windows.Forms.Label _lblTaxCode;
		private PaymentStatus _ucPaymentStatus;
	}
}
﻿namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Forms
{
    partial class AuditFind
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this._lblFromDate = new System.Windows.Forms.Label();
            this._lblToDate = new System.Windows.Forms.Label();
            this._dtpToDate = new System.Windows.Forms.DateTimePicker();
            this._btnSearch = new System.Windows.Forms.Button();
            this._errorProvider = new System.Windows.Forms.ErrorProvider();
            ((System.ComponentModel.ISupportInitialize)(this._errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // _dtpFromDate
            // 
            this._dtpFromDate.Location = new System.Drawing.Point(63, 13);
            this._dtpFromDate.Name = "_dtpFromDate";
            this._dtpFromDate.Size = new System.Drawing.Size(209, 20);
            this._dtpFromDate.TabIndex = 0;
            // 
            // _lblFromDate
            // 
            this._lblFromDate.AutoSize = true;
            this._lblFromDate.Location = new System.Drawing.Point(12, 13);
            this._lblFromDate.Name = "_lblFromDate";
            this._lblFromDate.Size = new System.Drawing.Size(30, 13);
            this._lblFromDate.TabIndex = 1;
            this._lblFromDate.Text = "From";
            // 
            // _lblToDate
            // 
            this._lblToDate.AutoSize = true;
            this._lblToDate.Location = new System.Drawing.Point(13, 39);
            this._lblToDate.Name = "_lblToDate";
            this._lblToDate.Size = new System.Drawing.Size(20, 13);
            this._lblToDate.TabIndex = 2;
            this._lblToDate.Text = "To";
            // 
            // _dtpToDate
            // 
            this._dtpToDate.Location = new System.Drawing.Point(63, 39);
            this._dtpToDate.Name = "_dtpToDate";
            this._dtpToDate.Size = new System.Drawing.Size(209, 20);
            this._dtpToDate.TabIndex = 3;
            // 
            // _btnSearch
            // 
            this._btnSearch.Location = new System.Drawing.Point(98, 76);
            this._btnSearch.Name = "_btnSearch";
            this._btnSearch.Size = new System.Drawing.Size(91, 23);
            this._btnSearch.TabIndex = 4;
            this._btnSearch.Text = "Search";
            this._btnSearch.UseVisualStyleBackColor = true;
            // 
            // _errorProvider
            // 
            this._errorProvider.ContainerControl = this;
            // 
            // AuditFind
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 111);
            this.Controls.Add(this._btnSearch);
            this.Controls.Add(this._dtpToDate);
            this.Controls.Add(this._lblToDate);
            this.Controls.Add(this._lblFromDate);
            this.Controls.Add(this._dtpFromDate);
            this.Name = "AuditFind";
            this.Text = "Find Audit v2.3";
            ((System.ComponentModel.ISupportInitialize)(this._errorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker _dtpFromDate;
        private System.Windows.Forms.Label _lblFromDate;
        private System.Windows.Forms.Label _lblToDate;
        private System.Windows.Forms.DateTimePicker _dtpToDate;
        private System.Windows.Forms.Button _btnSearch;
        private System.Windows.Forms.ErrorProvider _errorProvider;
    }
}
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;

namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates
{
	public partial class ProductFind : Form
	{
		private Int32 _divisionId = 0;
		private Boolean _multiSelect = false;
		private String _productCode = "";
		private String _productName;
		private Int32 _searchResultLimit = 500;
		private List<Evolve.Clients.Lafarge.Cement.Business.Rebates.Product> _selectedProducts;

		public Int32 DivisionId
		{
			get { return _divisionId; }
		}

		public Int32 SearchResultLimit
		{
			get { return _searchResultLimit; }
			set { _searchResultLimit = value; }
		}

		public List<Evolve.Clients.Lafarge.Cement.Business.Rebates.Product> SelectedProducts
		{
			get { return _selectedProducts; }
		}

		public ProductFind(Int32 divisionId, Boolean multiSelect)
		{
			_divisionId = divisionId;
			_multiSelect = multiSelect;

			InitializeComponent();

			_products.KeyPress += new KeyPressEventHandler(_customers_KeyPress);
			_products.CellContentDoubleClick += new DataGridViewCellEventHandler(_customers_CellContentDoubleClick);
			buttonOk.Click += new EventHandler(buttonAccept_Click);
		}

		private void ProductFind_Load(object sender, EventArgs e)
		{
			_products.MultiSelect = _multiSelect;
			_products.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			_products.ReadOnly = true;
			_products.AllowUserToAddRows = false;
			_products.AllowUserToDeleteRows = false;
			_products.AllowUserToOrderColumns = false;
			_products.Visible = false;

			_attribute1.DivisionId = _divisionId;
			_attribute1.IncludeBlank = true;
			_attribute1.Level = 1;
			_attribute1.Populate();
			_attribute1.Leave += new EventHandler(_attribute_Leave);

			_attribute2.DivisionId = _divisionId;
			_attribute2.IncludeBlank = true;
			_attribute2.Level = 2;
			_attribute2.Populate();
			_attribute2.Leave += new EventHandler(_attribute_Leave);

			_attribute3.DivisionId = _divisionId;
			_attribute3.IncludeBlank = true;
			_attribute3.Level = 3;
			_attribute3.Populate();
			_attribute3.Leave += new EventHandler(_attribute_Leave);
	
			_attribute4.DivisionId = _divisionId;
			_attribute4.IncludeBlank = true;
			_attribute4.Level = 4;
			_attribute4.Populate();
			_attribute4.Leave += new EventHandler(_attribute_Leave);
		}

		void _attribute_Leave(object sender, EventArgs e)
		{
			Attribute attribute = (Attribute) sender;

			if (attribute.SelectedValue == null)
			{
				attribute.ComboBox.SelectedIndex = 0;
			}
		}

		void _customers_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
		{
			ProductSelected();
		}

		void _customers_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == (char) Keys.Return)
			{
				ProductSelected();
			}
		}

		void _clear_Click(object sender, EventArgs e)
		{
			_name.Text = "";

			_products.Visible = false;
			_attribute1.ComboBox.SelectedIndex = 0;
			_attribute2.ComboBox.SelectedIndex = 0;
			_attribute3.ComboBox.SelectedIndex = 0;
			_attribute4.ComboBox.SelectedIndex = 0;

			_name.Focus();
		}

		private void _search_Click(object sender, EventArgs e)
		{
			_search.Enabled = false;
			this.Cursor = Cursors.WaitCursor;

			Boolean isRowLimitExceeded = false;
			Evolve.Clients.Lafarge.Cement.Business.Rebates.Product product = new Evolve.Clients.Lafarge.Cement.Business.Rebates.Product();
			product.DivisionId = _divisionId;

			_products.DataSource = product.Search(_name.Text, _attribute1.SelectedValue.Id, _attribute2.SelectedValue.Id, _attribute3.SelectedValue.Id, _attribute4.SelectedValue.Id, _searchResultLimit, out isRowLimitExceeded);
			_products.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
			_products.Visible = true;
			_products.Focus();

			// Warn if truncated results set
			if (isRowLimitExceeded)
			{
				MessageBox.Show("Only the first " + _searchResultLimit.ToString() + " rows have been displayed", "Product Search", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}

			Application.DoEvents();
			this.Cursor = Cursors.Default;
			_search.Enabled = true;
		}

		private void ProductSelected()
		{
			_selectedProducts = new List<Evolve.Clients.Lafarge.Cement.Business.Rebates.Product>();

			foreach (DataGridViewRow row in _products.SelectedRows)
			{
				_selectedProducts.Add((Evolve.Clients.Lafarge.Cement.Business.Rebates.Product) row.DataBoundItem);
			}

			if (_products.SelectedRows.Count == 1)
			{
				_productCode = _products.CurrentRow.Cells[ProductProperty.JDECode].Value.ToString();
				_productName = _products.CurrentRow.Cells[ProductProperty.Name].Value.ToString();
			}

			if (_selectedProducts.Count == 0)
			{
				_selectedProducts = null;
			}

			this.Close();
		}

		void buttonAccept_Click(object sender, EventArgs e)
		{
			if (_products.Visible && _products.SelectedRows.Count > 0)
			{
				ProductSelected();
			}
		}
	}
}
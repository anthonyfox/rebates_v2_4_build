﻿using System;
using System.Linq;
using System.Windows.Forms;
using Evolve.Libraries.Controls.Generic;
using Evolve.Libraries.Utilities;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;

namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates
{
	public partial class RebateType : UserControl
	{
		#region Properties

		#region PROPERTY: IncludeAllOption
		private Boolean _includeAllOption = false;

		public Boolean IncludeAllOption
		{
			get { return _includeAllOption; }
			set { _includeAllOption = value; }
		}
		#endregion

		#region DERIVED PROPERTY: ComboBox
		public ComboBox ComboBox
		{
			get 
			{ 
				return _comboBox; 
			}
		}
		#endregion

		#region DERIVED PROPERTY: SelectedValue
		public Business.Rebates.RebateType SelectedValue
		{
			get
			{
				return (Business.Rebates.RebateType) _comboBox.SelectedValue;
			}
		}
		#endregion

		#region DERIVED PROPERTY: IsAllSelected
		public Boolean IsAllSelected
		{
			get
			{
				return this.SelectedValue != null && this.SelectedValue.Id == 0;
			}
		}
		#endregion

		#endregion

		#region Constructors

		public RebateType()
		{
			InitializeComponent();

			_comboBox.DisplayMember = RebateTypeProperty.Name;
		}
		#endregion

		#region Business Logic
		public void Populate()
		{
			Evolve.Clients.Lafarge.Cement.Business.Rebates.RebateType rebateType = new Evolve.Clients.Lafarge.Cement.Business.Rebates.RebateType();
			ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.RebateType> rebateTypes = (ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.RebateType>) rebateType.Read();

			if (_includeAllOption)
			{
				Evolve.Clients.Lafarge.Cement.Business.Rebates.RebateType rebateTypeAll = new Business.Rebates.RebateType();

				rebateTypeAll.Id = 0;
				rebateTypeAll.Name = "All";

				rebateTypes.Insert(0, rebateTypeAll);
			}

			_comboBox.DataSource = rebateTypes;

			if (_includeAllOption)
			{
				ComboBox.SelectedIndex = 0;
			}
		}

		public void SetDefault()
		{
			for (Int32 index = 0; index < ComboBox.Items.Count; index++)
			{
				Evolve.Clients.Lafarge.Cement.Business.Rebates.RebateType rebateType = (Evolve.Clients.Lafarge.Cement.Business.Rebates.RebateType) ComboBox.Items[index];

				if (rebateType.IsDefault)
				{
					_comboBox.SelectedIndex = index;
					break;
				}
			}
		}

		public Boolean FindAndSelectId(Int32? id)
		{
			if (id == null)
			{
				id = 0;
			}

			Boolean found = false;
			if (ComboBox != null)
			{
				for (Int32 index = 0; index < ComboBox.Items.Count; index++)
				{
					Evolve.Clients.Lafarge.Cement.Business.Rebates.RebateType rebateType = (Evolve.Clients.Lafarge.Cement.Business.Rebates.RebateType) ComboBox.Items[index];

					if (rebateType.Id == id)
					{
						ComboBox.SelectedIndex = index;
						found = true;
						break;
					}
				}
			}
			return found;
		}
		#endregion
	}
}

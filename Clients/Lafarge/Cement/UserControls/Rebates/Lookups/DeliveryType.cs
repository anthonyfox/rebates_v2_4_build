﻿using System;
using System.Windows.Forms;
using Evolve.Libraries.Controls.Generic;
using Evolve.Libraries.Utilities;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;

namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates
{
	public partial class DeliveryType : UserControl
	{
		public DeliveryTypeEnum SelectedValue
		{
			get
			{
				if (_comboBox.SelectedValue == null)
				{
					return DeliveryTypeEnum.Null;
				}
				else
				{
					return (DeliveryTypeEnum) _comboBox.SelectedValue;
				}
			}

			set
			{
				_comboBox.SelectedValue = value;
			}
		}

		public ComboBox ComboBox
		{
			get { return _comboBox; }
		}

		public DeliveryType()
		{
			InitializeComponent();
			EnumUtilities.ComboBox(_comboBox, typeof(DeliveryTypeEnum), true, "All");
		}
	}
}

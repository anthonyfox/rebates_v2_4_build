﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates
{
	public partial class VAPActivationMethod : UserControl
	{
		public ComboBox ComboBox
		{
			get { return _comboBox; }
		}

		public Business.Rebates.VAPActivationMethod SelectedValue
		{
			get { return (Business.Rebates.VAPActivationMethod) _comboBox.SelectedValue; }
			set { _comboBox.SelectedValue = value; }
		}

		public VAPActivationMethod()
		{
			InitializeComponent();

			EnumUtilities.ComboBox(_comboBox, typeof(Evolve.Clients.Lafarge.Cement.Business.Rebates.VAPActivationMethod), false);
		}
	}
}

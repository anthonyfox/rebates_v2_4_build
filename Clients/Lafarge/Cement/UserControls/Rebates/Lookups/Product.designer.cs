﻿namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates
{
	partial class Product
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblProduct = new System.Windows.Forms.Label();
			this.txtCode = new System.Windows.Forms.TextBox();
			this.txtName = new System.Windows.Forms.TextBox();
			this.btnFind = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// lblProduct
			// 
			this.lblProduct.AutoSize = true;
			this.lblProduct.Location = new System.Drawing.Point(3, 6);
			this.lblProduct.Name = "lblProduct";
			this.lblProduct.Size = new System.Drawing.Size(44, 13);
			this.lblProduct.TabIndex = 0;
			this.lblProduct.Text = "Product";
			// 
			// txtCode
			// 
			this.txtCode.Location = new System.Drawing.Point(60, 3);
			this.txtCode.Name = "txtCode";
			this.txtCode.Size = new System.Drawing.Size(52, 20);
			this.txtCode.TabIndex = 1;
			// 
			// txtName
			// 
			this.txtName.Enabled = false;
			this.txtName.Location = new System.Drawing.Point(118, 3);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(279, 20);
			this.txtName.TabIndex = 2;
			// 
			// btnFind
			// 
			this.btnFind.Location = new System.Drawing.Point(402, 3);
			this.btnFind.Name = "btnFind";
			this.btnFind.Size = new System.Drawing.Size(24, 20);
			this.btnFind.TabIndex = 3;
			this.btnFind.Text = "...";
			this.btnFind.UseVisualStyleBackColor = true;
			// 
			// Product
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.btnFind);
			this.Controls.Add(this.txtName);
			this.Controls.Add(this.txtCode);
			this.Controls.Add(this.lblProduct);
			this.Size = new System.Drawing.Size(432, 26);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblProduct;
		private System.Windows.Forms.TextBox txtCode;
		private System.Windows.Forms.TextBox txtName;
		private System.Windows.Forms.Button btnFind;
	}
}

﻿using System;
using System.Windows.Forms;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Libraries.Controls.Generic;
using Evolve.Libraries.Utilities;
using System.Linq;

namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Lookups
{
	public partial class TaxCode : UserControl
	{
		public Boolean IncludeAllOption { get; set; }

		#region DERIVED PROPERTY: SelectedValue
		public Business.Rebates.TaxCode SelectedValue
		{
			get { return (Business.Rebates.TaxCode) _comboBox.SelectedValue; }
		}

		public ComboBox ComboBox
		{
			get { return _comboBox; }
		}

		private Boolean _isZeroRateOnly = false;
		public Boolean IsZeroRateOnly
		{
			get { return _isZeroRateOnly; }
			set
			{
				_isZeroRateOnly = value;

				if (_isZeroRateOnly)
				{
					SetZeroRateDefault();
				}

				_comboBox.Enabled = !_isZeroRateOnly;
			}
		}
		#endregion

		public TaxCode()
		{
			InitializeComponent();
			_comboBox.DisplayMember = TaxCodeProperty.Code;
		}

		#region Business Logic
		public void Populate()
		{
			Evolve.Clients.Lafarge.Cement.Business.Rebates.TaxCode taxCode = new Evolve.Clients.Lafarge.Cement.Business.Rebates.TaxCode();
			ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.TaxCode> taxCodes = (ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.TaxCode>) taxCode.Read();

			if (this.IncludeAllOption)
			{
				taxCodes.Insert(0, new Business.Rebates.TaxCode());
			}

			_comboBox.DataSource = taxCodes;

			if (!this.IncludeAllOption)
			{
				SetZeroRateDefault();
			}
		}


		private void SetZeroRateDefault()
		{
			ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.TaxCode> taxCodes = (ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.TaxCode>) _comboBox.DataSource;

			if (taxCodes.Count > 0)
			{
				Evolve.Clients.Lafarge.Cement.Business.Rebates.TaxCode zeroRate = taxCodes.FirstOrDefault(tc => tc.IsZeroRate);

				if (zeroRate != null)
				{
					FindAndSelectId(zeroRate.Id);
				}
			}
		}

		public Boolean FindAndSelectId(Int32 id)
		{
			Boolean found = false;

			if (_comboBox != null)
			{
				for (Int32 index = 0; index < _comboBox.Items.Count; index++)
				{
					Evolve.Clients.Lafarge.Cement.Business.Rebates.TaxCode taxCode = (Evolve.Clients.Lafarge.Cement.Business.Rebates.TaxCode) _comboBox.Items[index];

					if (taxCode.Id == id)
					{
						_comboBox.SelectedIndex = index;
						found = true;
						break;
					}
				}

				if (!found && _comboBox.Items.Count > 0)
				{
					_comboBox.SelectedIndex = 0;
				}
			}

			return found;
		}
		#endregion
	}
}

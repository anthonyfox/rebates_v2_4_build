﻿using System.Windows.Forms;
using Evolve.Libraries.Utilities;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;

namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates
{
	public partial class PackingType : UserControl
	{
		public PackingTypeEnum SelectedValue
		{
			get
			{
				if (_comboBox.SelectedValue == null)
				{
					return PackingTypeEnum.Null;
				}
				else
				{
					return (PackingTypeEnum) _comboBox.SelectedValue;
				}
			}
		}

		public ComboBox ComboBox
		{
			get { return _comboBox; }
		}

		public PackingType()
		{
			InitializeComponent();
			EnumUtilities.ComboBox(_comboBox, typeof(PackingTypeEnum), false);
		}
	}
}

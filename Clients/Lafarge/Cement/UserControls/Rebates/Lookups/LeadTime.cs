﻿using System;
using System.Windows.Forms;
using System.Linq;
using System.Collections.Generic;
using Evolve.Libraries.Controls.Generic;
using Evolve.Libraries.Utilities;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;

namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates
{
	public partial class LeadTime : ExtendedUserControl
	{
		#region Properties

		#region DERIVED PROPERTY: LeadTimesUnfiltered
		private ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.LeadTime> _leadTimesUnfiltered = null;

		private ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.LeadTime> LeadTimesUnfiltered
		{
			get
			{
				if (_leadTimesUnfiltered == null)
				{
					Evolve.Clients.Lafarge.Cement.Business.Rebates.LeadTime leadTime = new Evolve.Clients.Lafarge.Cement.Business.Rebates.LeadTime();
					leadTime.DivisionId = DivisionId;
					_leadTimesUnfiltered = (ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.LeadTime>) leadTime.ReadWithDirtyProperty();
				}
				return _leadTimesUnfiltered;
			}
		}
		#endregion

		#region PROPERTY: DivisionId
		private Int32 _divisionId;
		public Int32 DivisionId
		{
			get { return _divisionId; }
			set { _divisionId = value; }
		}
		#endregion

		#region PROPERTY: ExcludeIds
		private List<Int32> _excludeIds;
		public List<Int32> ExcludeIds
		{
			get
			{
				if (_excludeIds == null)
				{
					_excludeIds = new List<Int32>();
				}
				return _excludeIds;
			}
			set { _excludeIds = value; }
		}

		#endregion


		#region PROPERTY: IncludeAllOption
		private Boolean _includeAllOption = false;

		public Boolean IncludeAllOption
		{
			get { return _includeAllOption; }
			set { _includeAllOption = value; }
		}
		#endregion

		#region DERIVED PROPERTY: ComboBox
		public ComboBox ComboBox
		{
			get 
			{ 
				return _comboBox; 
			}
		}
		#endregion

		#region DERIVED PROPERTY: SelectedValue
		public Business.Rebates.LeadTime SelectedValue
		{
			get
			{
				return (Business.Rebates.LeadTime) _comboBox.SelectedValue;
			}
		}
		#endregion

		#region DERIVED PROPERTY: IsAllSelected
		public Boolean IsAllSelected
		{
			get
			{
				return this.SelectedValue != null && this.SelectedValue.Id == 0;
			}
		}
		#endregion

		#endregion

		#region Constructors

		public LeadTime()
		{
			InitializeComponent();

			_comboBox.DisplayMember = LeadTimeProperty.JDECodeName;
		}
		#endregion

		#region Business Logic
		public void Populate()
		{
			_comboBox.SelectedText = "";
			ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.LeadTime> leadTimes = new ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.LeadTime>();

			foreach(Evolve.Clients.Lafarge.Cement.Business.Rebates.LeadTime leadTime in LeadTimesUnfiltered)
			{
				if(!this.ExcludeIds.Contains(leadTime.Id))
				{
					leadTimes.Add(leadTime);
				}
			}

			if (_includeAllOption)
			{
				Evolve.Clients.Lafarge.Cement.Business.Rebates.LeadTime leadTimeAll = new Business.Rebates.LeadTime();

				leadTimeAll.Id = 0;
				leadTimeAll.Name = "All";

				leadTimes.Insert(0, leadTimeAll);
			}

			_comboBox.DataSource = leadTimes;

			if (_includeAllOption)
			{
				ComboBox.SelectedIndex = 0;
			}
		}
		public Boolean FindAndSelectId(Int32? id)
		{
			if (id == null)
			{
				id = 0;
			}

			Boolean found = false;
			if (ComboBox != null)
			{
				for (Int32 index = 0; index < ComboBox.Items.Count; index++)
				{
					Evolve.Clients.Lafarge.Cement.Business.Rebates.LeadTime rebateType = (Evolve.Clients.Lafarge.Cement.Business.Rebates.LeadTime) ComboBox.Items[index];

					if (rebateType.Id == id)
					{
						ComboBox.SelectedIndex = index;
						found = true;
						break;
					}
				}
			}
			return found;
		}
		#endregion
	}
}

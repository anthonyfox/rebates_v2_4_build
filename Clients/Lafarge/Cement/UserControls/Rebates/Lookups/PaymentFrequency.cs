﻿using System;
using System.Windows.Forms;
using Evolve.Libraries.Controls.Generic;
using Evolve.Libraries.Utilities;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;

namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates
{
	public partial class PaymentFrequency : UserControl
	{
		#region Properties

		#region DERIVED PROPERTY: ComboBox
		public ComboBox ComboBox
		{
			get { return _comboBox; }
		}
		#endregion

		#region PROPERTY: DivisionId
		private Int32 _divisionId = 0;
		public Int32 DivisionId
		{
			get { return _divisionId; }
			set { _divisionId = value; }
		}
		#endregion

		#region PROPERTY: AllItemText
		private String _allItemText = "All";

		public String AllItemText
		{
			get { return _allItemText; }
			set { _allItemText = value; }
		}
		#endregion

		#region PROPERTY: IncludeAllOption
		private Boolean _includeAllOption = false;
		public Boolean IncludeAllOption
		{
			get { return _includeAllOption; }
			set { _includeAllOption = value; }
		}
		#endregion

		#region DERIVED PROPERTY: SelectedValue
		public Business.Rebates.PaymentFrequency SelectedValue
		{
			get { return (Business.Rebates.PaymentFrequency) _comboBox.SelectedValue; }
		}
		#endregion

		#region DERIVED PROPERTY: IsAllSelected
		public Boolean IsAllSelected
		{
			get
			{
				return this.SelectedValue != null && this.SelectedValue.Id == 0;
			}
		}
		#endregion

		#endregion

		public PaymentFrequency()
		{
			InitializeComponent();

			_comboBox.DisplayMember = PaymentFrequencyProperty.Name;
			_comboBox.ValueMember = PaymentFrequencyProperty.Id;
		}

		public void Populate()
		{
			Evolve.Clients.Lafarge.Cement.Business.Rebates.PaymentFrequency paymentFrequency = new Evolve.Clients.Lafarge.Cement.Business.Rebates.PaymentFrequency();
			paymentFrequency.DivisionId = _divisionId;
			paymentFrequency.AddReadFilter(PaymentFrequencyProperty.DivisionId, "=");
			ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.PaymentFrequency> paymentFrequencys = (ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.PaymentFrequency>) paymentFrequency.Read();

			if (_includeAllOption)
			{
				Evolve.Clients.Lafarge.Cement.Business.Rebates.PaymentFrequency paymentFrequencyAll = new Business.Rebates.PaymentFrequency();

				paymentFrequencyAll.DivisionId = _divisionId;
				paymentFrequencyAll.Id = 0;
				paymentFrequencyAll.Name = _allItemText;

				paymentFrequencys.Insert(0, paymentFrequencyAll);
			}

			_comboBox.DataSource = paymentFrequencys;

			if (_includeAllOption)
			{
				ComboBox.SelectedIndex = 0;
			}
		}
		public Boolean FindAndSelectId(Int32? id)
		{
			if (id == null)
			{
				id = 0;
			}

			Boolean found = false;
			if (ComboBox != null)
			{
				for (Int32 index = 0; index < ComboBox.Items.Count; index++)
				{
					Evolve.Clients.Lafarge.Cement.Business.Rebates.PaymentFrequency paymentFrequency = (Evolve.Clients.Lafarge.Cement.Business.Rebates.PaymentFrequency) ComboBox.Items[index];

					if (paymentFrequency.Id == id)
					{
						ComboBox.SelectedIndex = index;
						found = true;
						break;
					}
				}
			}
			return found;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates
{
	public partial class Attribute : UserControl
	{
		private Int32 _divisionId = 0;
		private Int16? _level = null;
		private Boolean _includeBlank = false;

		public Int32 DivisionId
		{
			get { return _divisionId; }
			set { _divisionId = value; }
		}

		public Int16? Level
		{
			get { return _level; }
			set { _level = value; }
		}

		public Boolean IncludeBlank
		{
			get { return _includeBlank; }
			set { _includeBlank = value; }
		}

		public ComboBox ComboBox
		{
			get { return _comboBox; }
		}

		public Business.Rebates.Attribute SelectedValue
		{
			get 
			{
				if (_includeBlank && _comboBox.SelectedValue == null)
				{
					_comboBox.SelectedIndex = 0;
				}

				return (Business.Rebates.Attribute) _comboBox.SelectedValue; 
			}
		}

		public Attribute()
		{
			InitializeComponent();

			_comboBox.DisplayMember = "Name";
		}

		public void Populate()
		{
			Evolve.Clients.Lafarge.Cement.Business.Rebates.Attribute attribute = new Evolve.Clients.Lafarge.Cement.Business.Rebates.Attribute();

			attribute.DivisionId = _divisionId;
			attribute.AddReadFilter("DivisionId", "=");

			if (_level.HasValue)
			{
				attribute.Level = (Int16) _level;
				attribute.AddReadFilter("Level", "=");
			}

			ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.Attribute> attributes = (ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.Attribute>) attribute.Read();

			if (_includeBlank)
			{
				Evolve.Clients.Lafarge.Cement.Business.Rebates.Attribute attributeBlank = new Business.Rebates.Attribute();

				attributeBlank.Id = 0;
				attributeBlank.DivisionId = _divisionId;
				attributeBlank.Name = "";

				if (_level.HasValue)
				{
					attributeBlank.Level = (Int16) _level;
				}

				attributes.Insert(0, attributeBlank);
			}

			_comboBox.DataSource = attributes;
		}
	}
}

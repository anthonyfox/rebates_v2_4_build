﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates
{
	public partial class Customer : UserControl
	{
		private CustomerSearchModeEnum _searchMode = CustomerSearchModeEnum.Undefined;
		private Int32 _controlSpacing = Evolve.Libraries.Controls.Constants.Control.Spacing;
		private Int32 _divisionId = 0;
		private Boolean _multiSelect = false;
		private List<CustomerAddress> _selectedCustomers;
		private Int32? _customerGroupId;
		private Int32? _parentCustomerId;

		// Filter properties
		public Int32? CustomerGroupId
		{
			get { return _customerGroupId; }
			set { _customerGroupId = value; }
		}

		public Int32? ParentCustomerId
		{
			get { return _parentCustomerId; }
			set { _parentCustomerId = value; }
		}

		// Create an event handlers for single and multi customer selection
		public event EventHandler CustomerSelected;
		public event EventHandler MultiCustomerSelected;

		protected void OnCustomerSelected(EventArgs e)
		{
			if (CustomerSelected != null)
			{
				CustomerSelected(this, e);
			}
		}

		protected void OnMultiCustomerSelected(EventArgs e)
		{
			if (MultiCustomerSelected != null)
			{
				MultiCustomerSelected(this, e);
			}
		}

		// Properties

		public CustomerAddress SelectedValue
		{
			get { return (_selectedCustomers != null && _selectedCustomers.Count > 0 ? _selectedCustomers[0] : null); }
		}

		public List<CustomerAddress> SelectedValues
		{
			get { return _selectedCustomers; }
		}

		public String LabelText
		{
			get { return lblCustomer.Text; }
			set { lblCustomer.Text = value; InitializeControl(); }
		}

		public Int32? AccountNumber
		{
			get
			{
				if (txtName.Text != "")
				{
					return Int32.Parse(txtAccountNumber.Text);
				}
				else
				{
					return null;
				}
			}

			set
			{
				txtAccountNumber.Text = value.ToString();
				ReadCustomer();
			}
		}

		public Int32 Id
		{
			set
			{
				// Attempt to read customer details
				CustomerAddress customer = new CustomerAddress();

				customer.Id = value;
				customer.AddReadFilter(CustomerAddressProperty.Id, "=");

				ExtendedBindingList<CustomerAddress> customers = (ExtendedBindingList<CustomerAddress>) customer.Read();

				if (customers != null && customers.Count > 0)
				{
					txtName.Text = customers[0].Name;

				    int jdeCodeInt;
				    txtAccountNumber.Text = int.TryParse(customers[0].JDECode, out jdeCodeInt)
                        ? FormatAccountNumber(jdeCodeInt)
                        : customers[0].JDECode;

					_selectedCustomers = new List<CustomerAddress>();
					_selectedCustomers.Add(customers[0]);
					IsInActiveCustomer(customers[0]);
				}
				else
				{
					IsInActiveCustomer(null);
					txtName.Text = "";
					txtAccountNumber.Text = "";
				}
			}
		}

		public CustomerSearchModeEnum CustomerSearchModeEnum
		{
			get { return _searchMode; }
			set { _searchMode = value; }
		}

		public Int32 DivisionId
		{
			get { return _divisionId; }
			set { _divisionId = value; }
		}

		public Boolean MultiSelect
		{
			get { return _multiSelect; }
			set { _multiSelect = value; }
		}

		public String CustomerName
		{
			get { return txtName.Text; }
		}

		public TextBox AccountNumberTextBox
		{
			get { return txtAccountNumber; }
		}

		public Button Find
		{
			get { return btnFind; }
		}

		public Customer()
		{
			InitializeComponent();
			InitializeControl();

			txtAccountNumber.Validating += new CancelEventHandler(txtAccountNumber_Validating);
			btnFind.Click += new EventHandler(btnFind_Click);
		}

		void txtAccountNumber_Validating(object sender, CancelEventArgs e)
		{
			ReadCustomer();
		}

		void btnFind_Click(object sender, EventArgs e)
		{
			if (_searchMode == CustomerSearchModeEnum.Undefined)
			{
				throw new ArgumentException("CustomerSearchMode is Undefined");
			}

			CustomerFind customerFind = new CustomerFind(_searchMode, _divisionId, _multiSelect);
			customerFind.CustomerGroupId = this.CustomerGroupId;
			customerFind.ParentCustomerId = this.ParentCustomerId;
			customerFind.Icon = ControlUtilities.GetIcon(this);
			customerFind.ShowDialog();

			if (customerFind.SelectedCustomers != null && customerFind.SelectedCustomers.Count > 0)
			{
				this.Validate();

                int jdeCodeInt;
                txtAccountNumber.Text = int.TryParse(customerFind.SelectedCustomers[0].JDECode, out jdeCodeInt)
                    ? FormatAccountNumber(jdeCodeInt)
                    : customerFind.SelectedCustomers[0].JDECode;

                txtName.Text = customerFind.SelectedCustomers[0].Name;

				_selectedCustomers = customerFind.SelectedCustomers;

				if (customerFind.SelectedCustomers.Count == 1)
				{
					OnCustomerSelected(new EventArgs());
				}
				else
				{
					OnMultiCustomerSelected(new EventArgs());
				}
			}
		}

		public void InitializeControl()
		{
			// Re-position controls based on the label size
			Point point = txtAccountNumber.Location;
			point.X = lblCustomer.Location.X + lblCustomer.Size.Width + _controlSpacing;
			txtAccountNumber.Location = point;

			point = txtName.Location;
			point.X = txtAccountNumber.Location.X + txtAccountNumber.Size.Width + _controlSpacing;

			txtName.Location = point;

			point = btnFind.Location;
			point.X = txtName.Location.X + txtName.Size.Width + _controlSpacing;

			btnFind.Location = point;

			point = _lblInActive.Location;
			point.X = btnFind.Location.X + btnFind.Width + _controlSpacing;

			_lblInActive.Location = point;
		}

	    public void ReadCustomer()
	    {
	        Int32 accountNumber;
	        txtName.Text = "";

	        _selectedCustomers = null;

	        if (Int32.TryParse(txtAccountNumber.Text, out accountNumber))
	        {
	            // Format displayed account number
	            txtAccountNumber.Text = FormatAccountNumber(accountNumber);
	        }

	        // Attempt to read customer details
	        CustomerAddress customer = new Evolve.Clients.Lafarge.Cement.Business.Rebates.CustomerAddress();

	        customer.ReadFilter = "{" + CustomerAddressProperty.JDECode + "} = '" + txtAccountNumber.Text.Trim() + "'";
	        customer.ReadFilter +=
	            " and ({" + CustomerAddressProperty.DivisionId + "} = " + _divisionId.ToString() +
	            " or {" + CustomerAddressProperty.ParentDivisionId + "} = " + _divisionId.ToString() +
	            " or {" + CustomerAddressProperty.DivisionId + "} is null" +
	            " or {" + CustomerAddressProperty.ParentDivisionId + "} is null )";

	        switch (_searchMode)
	        {
	            case CustomerSearchModeEnum.ShipTo:
	                customer.ReadFilter += " " +
	                                       "and (" +
	                                       customer.DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.Customer) + "." +
	                                       CustomerColumn.CustomerSearchTypeEnum + " = '" +
	                                       CustomerSearchTypeEnumConvertor.CustomerSearchTypeEnumToDBString(
	                                           CustomerSearchTypeEnum.ShipTo) + "' " +
	                                       "or " + customer.DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.Customer) +
	                                       "." + CustomerColumn.CustomerSearchTypeEnum + " = '" +
	                                       CustomerSearchTypeEnumConvertor.CustomerSearchTypeEnumToDBString(
	                                           CustomerSearchTypeEnum.ShipToInActive) + "')";
	                break;

	            case CustomerSearchModeEnum.SoldTo:
	                customer.ReadFilter += " " +
	                                       "and (" +
	                                       customer.DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.Customer) + "." +
	                                       CustomerColumn.CustomerSearchTypeEnum + " = '" +
	                                       CustomerSearchTypeEnumConvertor.CustomerSearchTypeEnumToDBString(
	                                           CustomerSearchTypeEnum.SoldTo) + "' " +
	                                       "or " + customer.DatabaseTableSyntax(DBRebates.DatabaseName, DBRebates.Customer) +
	                                       "." + CustomerColumn.CustomerSearchTypeEnum + " = '" +
	                                       CustomerSearchTypeEnumConvertor.CustomerSearchTypeEnumToDBString(
	                                           CustomerSearchTypeEnum.SoldToInActive) + "')";
	                break;
	        }

	        ExtendedBindingList<CustomerAddress> customers = (ExtendedBindingList<CustomerAddress>) customer.Read();

	        if (customers != null && customers.Count > 0)
	        {
	            txtName.Text = customers[0].Name;

	            _selectedCustomers = new List<CustomerAddress>();
	            _selectedCustomers.Add(customers[0]);

	            IsInActiveCustomer(customers[0]);
	            OnCustomerSelected(new EventArgs());
	        }
	        else
	        {
	            txtAccountNumber.Text = "";
	        }
	    }

	    private void IsInActiveCustomer(CustomerAddress customerAddress)
		{
			if (customerAddress == null)
			{
				_lblInActive.Visible = false;
			}
			else
			{
				Boolean isVisible = (customerAddress.CustomerSearchTypeEnum == CustomerSearchTypeEnum.ShipToInActive || customerAddress.CustomerSearchTypeEnum == CustomerSearchTypeEnum.SoldToInActive);
				_lblInActive.Visible = isVisible;
			}
		}

		private String FormatAccountNumber(Int32 accountNumber)
		{
			return accountNumber.ToString();
		}
	}
}

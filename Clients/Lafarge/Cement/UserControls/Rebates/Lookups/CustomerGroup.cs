using System;
using System.Windows.Forms;
using Evolve.Libraries.Controls.Generic;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates
{
	public partial class CustomerGroup : LabelTextBoxComboBox
	{
		#region Properties

		#region PROPERTY: IncludeBlankOption
		private Boolean _includeBlankOption = false;
		public Boolean IncludeBlankOption
		{
			get { return _includeBlankOption; }
			set { _includeBlankOption = value; }
		}
		#endregion

		#region DERIVED PROPERTY: SelectedValue
		public Evolve.Clients.Lafarge.Cement.Business.Rebates.CustomerGroup SelectedValue
		{
			get
			{
				Evolve.Clients.Lafarge.Cement.Business.Rebates.CustomerGroup customerGroup = (Evolve.Clients.Lafarge.Cement.Business.Rebates.CustomerGroup) ComboBox.SelectedItem;
				return (customerGroup == null || customerGroup.Id == 0 ? null : customerGroup);
			}
		}
		#endregion

		#region PROPERTY: DivisionId
		private Int32 _divisionId = 0;
		public Int32 DivisionId
		{
			get { return _divisionId; }
			set { _divisionId = value; }
		}
		#endregion

		#region DERIVED PROPERTY: IsAllSelected
		public Boolean IsAllSelected
		{
			get
			{
				return this.SelectedValue != null && this.SelectedValue.Id == 0;
			}
		}
		#endregion

		#endregion

		#region Constructors
		public CustomerGroup()
		{
			InitializeComponent();

			ComboBox.DisplayMember = "Name";
			ComboBox.ValueMember = "JDECode";

			Label.Text = "Customer Group";
			InitializeControl();
		}
		#endregion

		public void Populate()
		{
			Evolve.Clients.Lafarge.Cement.Business.Rebates.CustomerGroup customerGroup = new Evolve.Clients.Lafarge.Cement.Business.Rebates.CustomerGroup();
			customerGroup.DivisionId = _divisionId;
			customerGroup.AddReadFilter(CustomerGroupProperty.DivisionId, "=");

			ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.CustomerGroup> customerGroups = (ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.CustomerGroup>) customerGroup.Read();

			if (_includeBlankOption)
			{
				Evolve.Clients.Lafarge.Cement.Business.Rebates.CustomerGroup customerGroupAll = new Business.Rebates.CustomerGroup();

				customerGroupAll.Id = 0;
				customerGroupAll.Name = "";

				customerGroups.Insert(0, customerGroupAll);
			}

			ComboBox.DataSource = customerGroups;

			if (_includeBlankOption)
			{
				ComboBox.SelectedIndex = 0;
			}
		}

		public Boolean FindAndSelectId(Int32 id)
		{
			Boolean found = false;
			if (ComboBox != null)
			{
				for (Int32 index = 0; index < ComboBox.Items.Count; index++)
				{
					Evolve.Clients.Lafarge.Cement.Business.Rebates.CustomerGroup customerGroup = (Evolve.Clients.Lafarge.Cement.Business.Rebates.CustomerGroup) ComboBox.Items[index];

					if (customerGroup.Id == id)
					{
						ComboBox.SelectedIndex = index;
						found = true;
						break;
					}
				}
			}
			return found;
		}
	}
}

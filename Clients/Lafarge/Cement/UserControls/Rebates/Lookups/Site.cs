using System;
using System.Windows.Forms;
using Evolve.Libraries.Controls.Generic;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates
{
	public partial class Site : LabelTextBoxComboBox
	{
		#region DERIVED PROPERTY: SelectedValue
		public Business.Rebates.Site SelectedValue
		{
			get
			{
				ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.Site> sites = (ExtendedBindingList <Evolve.Clients.Lafarge.Cement.Business.Rebates.Site>) ComboBox.DataSource;

				if (sites == null)
				{
					return null;
				}
				else
				{
					return sites[ComboBox.SelectedIndex];
				}
			}
		}
		#endregion

		#region PROPERTY: Code
		public String Code
		{
			get
			{
				return (String) ComboBox.SelectedValue;
			}

			set
			{
				ComboBox.SelectedValue = value;
			}
		}
		#endregion

		#region PROPERTY: DivisionId
		private Int32 _divisionId = 0;
		public Int32 DivisionId
		{
			get { return _divisionId; }
			set { _divisionId = value; }
		}
		#endregion

		#region PROPERTY: IncludeAllOption
		private Boolean _includeAllOption = false;
		public Boolean IncludeAllOption
		{
			get { return _includeAllOption; }
			set { _includeAllOption = value; }
		}
		#endregion

		#region DERIVED PROPERTY: IsAllSelected
		public Boolean IsAllSelected
		{
			get
			{
				return this.SelectedValue != null && this.SelectedValue.Id == 0;
			}
		}
		#endregion

		public Site()
		{
			InitializeComponent();

			ComboBox.ValueMember = SiteProperty.JDECode;
			ComboBox.DisplayMember = SiteProperty.Name;

			Label.Text = "Sold From";
			TextBoxWidth = 65;
		}

		public void Populate()
		{
			Evolve.Clients.Lafarge.Cement.Business.Rebates.Site site = new Evolve.Clients.Lafarge.Cement.Business.Rebates.Site();
	
			site.DivisionId = _divisionId;
			site.AddReadFilter(SiteProperty.DivisionId, "=");

			ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.Site> sites = (ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.Site>) site.Read();

			if (_includeAllOption)
			{
				Evolve.Clients.Lafarge.Cement.Business.Rebates.Site siteAll = new Evolve.Clients.Lafarge.Cement.Business.Rebates.Site();

				siteAll.Id = 0;
				siteAll.DivisionId = _divisionId;
				siteAll.Name = "All";

				sites.Insert(0, siteAll);
			}

			ComboBox.DataSource = sites;

			if (_includeAllOption)
			{
				ComboBox.SelectedIndex = 0;
			}
		}
		public Boolean FindAndSelectId(Int32? id)
		{
			if (id == null)
			{
				id = 0;
			}

			Boolean found = false;
			if (ComboBox != null)
			{
				for (Int32 index = 0; index < ComboBox.Items.Count; index++)
				{
					Evolve.Clients.Lafarge.Cement.Business.Rebates.Site site = (Evolve.Clients.Lafarge.Cement.Business.Rebates.Site) ComboBox.Items[index];

					if (site.Id == id)
					{
						ComboBox.SelectedIndex = index;
						found = true;
						break;
					}
				}
			}
			return found;
		}
	}
}
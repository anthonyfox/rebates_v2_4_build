﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates
{
	public partial class Product : UserControl
	{
		private Int32 _controlSpacing = Evolve.Libraries.Controls.Constants.Control.Spacing;
		private Int32 _divisionId = 0;
		private Boolean _multiSelect = false;
		private List<Evolve.Clients.Lafarge.Cement.Business.Rebates.Product> _selectedProducts;

		// Create an event handler for product selection
		public event EventHandler ProductSelected;
		public event EventHandler MultiProductSelected;

		protected void OnProductSelected(EventArgs e)
		{
			if (ProductSelected != null)
			{
				ProductSelected(this, e);
			}
		}

		protected void OnMultiProductSelected(EventArgs e)
		{
			if (MultiProductSelected != null)
			{
				MultiProductSelected(this, e);
			}
		}

		// Properties
		public Evolve.Clients.Lafarge.Cement.Business.Rebates.Product SelectedProduct
		{
			get { return (_selectedProducts != null && _selectedProducts.Count > 0 ? _selectedProducts[0] : null); }
		}
		
		public List<Evolve.Clients.Lafarge.Cement.Business.Rebates.Product> SelectedValues
		{
			get { return _selectedProducts; }
		}

		public String LabelText
		{
			get { return lblProduct.Text; }
			set { lblProduct.Text = value; InitializeControl(); }
		}

		public Int32 DivisionId
		{
			get { return _divisionId; }
			set { _divisionId = value; }
		}

		public Boolean MultiSelect
		{
			get { return _multiSelect; }
			set { _multiSelect = value; }
		}

		public Button Find
		{
			get { return btnFind; }
		}

		public Product()
		{
			InitializeComponent();
			InitializeControl();

			txtCode.Validating += new CancelEventHandler(txtCode_Validating);
			btnFind.Click += new EventHandler(btnFind_Click);
		}

		void txtCode_Validating(object sender, CancelEventArgs e)
		{
			ReadProduct();
		}

		void btnFind_Click(object sender, EventArgs e)
		{
			ProductFind productFind = new ProductFind(_divisionId, _multiSelect);
			productFind.Icon = ControlUtilities.GetIcon(this);
			productFind.ShowDialog();

			if (productFind.SelectedProducts != null && productFind.SelectedProducts.Count > 0)
			{
				_selectedProducts = productFind.SelectedProducts;

				txtCode.Text = productFind.SelectedProducts[0].JDECode;
				txtName.Text = productFind.SelectedProducts[0].Name;

				if (productFind.SelectedProducts.Count == 1)
				{
					OnProductSelected(new EventArgs());
				}
				else
				{
					OnMultiProductSelected(new EventArgs());
				}
			}
		}

		public void InitializeControl()
		{
			// Re-position controls based on the label size
			Point point = txtCode.Location;
			point.X = lblProduct.Location.X + lblProduct.Size.Width + _controlSpacing;
			txtCode.Location = point;

			point = txtName.Location;
			point.X = txtCode.Location.X + txtCode.Size.Width + _controlSpacing;

			txtName.Location = point;

			point = btnFind.Location;
			point.X = txtName.Location.X + txtName.Size.Width + _controlSpacing;

			btnFind.Location = point;
		}

		public void ReadProduct()
		{
			_selectedProducts = null;

			// Attempt to read customer details
			Evolve.Clients.Lafarge.Cement.Business.Rebates.Product product = new Evolve.Clients.Lafarge.Cement.Business.Rebates.Product();

			product.JDECode = txtCode.Text;
			product.DivisionId = _divisionId;

			product.AddReadFilter(ProductProperty.JDECode, "=");
			product.AddReadFilter(ProductProperty.DivisionId, "=");

			ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.Product> products = (ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.Product>) product.Read();

			if (products.Count > 0)
			{
				txtName.Text = products[0].Name;

				_selectedProducts = new List<Evolve.Clients.Lafarge.Cement.Business.Rebates.Product>();
				_selectedProducts.Add(products[0]);

				OnProductSelected(new EventArgs());
			}
		}

		public void Clear()
		{
			txtCode.Text = "";
			txtName.Text = "";
			_selectedProducts = null;
		}
	}
}

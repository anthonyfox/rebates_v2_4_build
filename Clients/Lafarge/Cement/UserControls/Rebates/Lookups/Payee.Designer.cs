﻿namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates
{
	partial class Payee
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnFind = new System.Windows.Forms.Button();
			this.txtName = new System.Windows.Forms.TextBox();
			this.txtAccountNumber = new System.Windows.Forms.TextBox();
			this.lblPayee = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// btnFind
			// 
			this.btnFind.Location = new System.Drawing.Point(386, 0);
			this.btnFind.Name = "btnFind";
			this.btnFind.Size = new System.Drawing.Size(24, 20);
			this.btnFind.TabIndex = 7;
			this.btnFind.Text = "...";
			this.btnFind.UseVisualStyleBackColor = true;
			// 
			// txtName
			// 
			this.txtName.Enabled = false;
			this.txtName.Location = new System.Drawing.Point(101, 0);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(279, 20);
			this.txtName.TabIndex = 6;
			// 
			// txtAccountNumber
			// 
			this.txtAccountNumber.Location = new System.Drawing.Point(43, 0);
			this.txtAccountNumber.Name = "txtAccountNumber";
			this.txtAccountNumber.Size = new System.Drawing.Size(52, 20);
			this.txtAccountNumber.TabIndex = 5;
			// 
			// lblPayee
			// 
			this.lblPayee.AutoSize = true;
			this.lblPayee.Location = new System.Drawing.Point(0, 3);
			this.lblPayee.Name = "lblPayee";
			this.lblPayee.Size = new System.Drawing.Size(37, 13);
			this.lblPayee.TabIndex = 4;
			this.lblPayee.Text = "Payee";
			// 
			// Payee
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.btnFind);
			this.Controls.Add(this.txtName);
			this.Controls.Add(this.txtAccountNumber);
			this.Controls.Add(this.lblPayee);
			this.Name = "Payee";
			this.Size = new System.Drawing.Size(417, 20);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnFind;
		private System.Windows.Forms.TextBox txtName;
		private System.Windows.Forms.TextBox txtAccountNumber;
		private System.Windows.Forms.Label lblPayee;
	}
}

﻿namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates
{
	partial class Customer
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblCustomer = new System.Windows.Forms.Label();
			this.txtAccountNumber = new System.Windows.Forms.TextBox();
			this.txtName = new System.Windows.Forms.TextBox();
			this.btnFind = new System.Windows.Forms.Button();
			this._lblInActive = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// lblCustomer
			// 
			this.lblCustomer.AutoSize = true;
			this.lblCustomer.Location = new System.Drawing.Point(3, 3);
			this.lblCustomer.Name = "lblCustomer";
			this.lblCustomer.Size = new System.Drawing.Size(51, 13);
			this.lblCustomer.TabIndex = 0;
			this.lblCustomer.Text = "Customer";
			// 
			// txtAccountNumber
			// 
			this.txtAccountNumber.Location = new System.Drawing.Point(60, 0);
			this.txtAccountNumber.Name = "txtAccountNumber";
			this.txtAccountNumber.Size = new System.Drawing.Size(52, 20);
			this.txtAccountNumber.TabIndex = 1;
			// 
			// txtName
			// 
			this.txtName.Enabled = false;
			this.txtName.Location = new System.Drawing.Point(118, 0);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(279, 20);
			this.txtName.TabIndex = 2;
			// 
			// btnFind
			// 
			this.btnFind.Location = new System.Drawing.Point(402, 0);
			this.btnFind.Name = "btnFind";
			this.btnFind.Size = new System.Drawing.Size(24, 20);
			this.btnFind.TabIndex = 3;
			this.btnFind.Text = "...";
			this.btnFind.UseVisualStyleBackColor = true;
			// 
			// _lblInActive
			// 
			this._lblInActive.AutoSize = true;
			this._lblInActive.ForeColor = System.Drawing.Color.Red;
			this._lblInActive.Location = new System.Drawing.Point(432, 3);
			this._lblInActive.Name = "_lblInActive";
			this._lblInActive.Size = new System.Drawing.Size(300, 13);
			this._lblInActive.TabIndex = 4;
			this._lblInActive.Text = "WARNING: This account has been made in-active within JDE";
			// 
			// Customer
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this._lblInActive);
			this.Controls.Add(this.btnFind);
			this.Controls.Add(this.txtName);
			this.Controls.Add(this.txtAccountNumber);
			this.Controls.Add(this.lblCustomer);
			this.Name = "Customer";
			this.Size = new System.Drawing.Size(735, 20);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblCustomer;
		private System.Windows.Forms.TextBox txtAccountNumber;
		private System.Windows.Forms.TextBox txtName;
		private System.Windows.Forms.Button btnFind;
		private System.Windows.Forms.Label _lblInActive;
	}
}

﻿using System;
using System.Windows.Forms;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Libraries.Controls.Generic;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates
{
	public partial class Hierarchy : UserControl
	{
		#region Properties

		#region PROPERTY: AllItemText
		private String _allItemText = "All";

		public String AllItemText
		{
			get { return _allItemText; }
			set { _allItemText = value; }
		}
		#endregion

		#region PROPERTY: IncludeAllOption
		private Boolean _includeAllOption = false;

		public Boolean IncludeAllOption
		{
			get { return _includeAllOption; }
			set { _includeAllOption = value; }
		}
		#endregion

		#region PROPERTY: DivisionId
		private Int32 _divisionId = 0;
		public Int32 DivisionId
		{
			get { return _divisionId; }
			set { _divisionId = value; }
		}
		#endregion

		#region DERIVED PROPERTY: ComboBox
		public ComboBox ComboBox
		{
			get { return _comboBox; }
		}
		#endregion

		#region DERIVED PROPERTY: SelectedValue
		public Business.Rebates.Hierarchy SelectedValue
		{
			get { return (Business.Rebates.Hierarchy) _comboBox.SelectedValue; }
		}
		#endregion

		#region DERIVED PROPERTY: IsAllSelected
		public Boolean IsAllSelected
		{
			get
			{
				return this.SelectedValue != null && this.SelectedValue.Id == 0;
			}
		}
		#endregion

		#endregion

		#region Constructor
		public Hierarchy()
		{
			InitializeComponent();

			_comboBox.DisplayMember = HierarchyProperty.Name;
		}
		#endregion

		#region Business Logic
		public void Populate()
		{
			Evolve.Clients.Lafarge.Cement.Business.Rebates.Hierarchy hierarchy = new Evolve.Clients.Lafarge.Cement.Business.Rebates.Hierarchy();
			hierarchy.DivisionId = _divisionId;
			hierarchy.AddReadFilter(HierarchyProperty.DivisionId, "=");
			ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.Hierarchy> hierarchies = (ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.Hierarchy>) hierarchy.Read();

			if (_includeAllOption)
			{
				Evolve.Clients.Lafarge.Cement.Business.Rebates.Hierarchy hierarchyAll = new Business.Rebates.Hierarchy();

				hierarchyAll.Id = 0;
				hierarchyAll.Name = _allItemText;

				hierarchies.Insert(0, hierarchyAll);
			}

			ComboBox.DataSource = hierarchies;

			if (_includeAllOption)
			{
				ComboBox.SelectedIndex = 0;
			}
		}

		public Boolean FindAndSelectId(Int32 id)
		{
			Boolean found = false;
			if (ComboBox != null)
			{
				for (Int32 index = 0; index < ComboBox.Items.Count; index++)
				{
					Evolve.Clients.Lafarge.Cement.Business.Rebates.Hierarchy hierarchy = (Evolve.Clients.Lafarge.Cement.Business.Rebates.Hierarchy) ComboBox.Items[index];

					if (hierarchy.Id == id)
					{
						ComboBox.SelectedIndex = index;
						found = true;
						break;
					}
				}
			}
			return found;
		}
		#endregion
	}
}

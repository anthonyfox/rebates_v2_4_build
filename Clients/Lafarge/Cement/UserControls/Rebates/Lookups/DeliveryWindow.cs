﻿using System;
using System.Windows.Forms;
using System.Linq;
using System.Collections.Generic;
using Evolve.Libraries.Controls.Generic;
using Evolve.Libraries.Utilities;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;

namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates
{
	public partial class DeliveryWindow : ExtendedUserControl
	{
		#region Properties

		#region DERIVED PROPERTY: DeliveryWindowsUnfiltered
		private ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.DeliveryWindow> _deliveryWindowsUnfiltered = null;

		private ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.DeliveryWindow> DeliveryWindowsUnfiltered
		{
			get
			{
				if (_deliveryWindowsUnfiltered == null)
				{
					Evolve.Clients.Lafarge.Cement.Business.Rebates.DeliveryWindow deliveryWindow = new Evolve.Clients.Lafarge.Cement.Business.Rebates.DeliveryWindow();
					deliveryWindow.DivisionId = DivisionId;
					_deliveryWindowsUnfiltered = (ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.DeliveryWindow>) deliveryWindow.ReadWithDirtyProperty();
				}
				return _deliveryWindowsUnfiltered;
			}
		}
		#endregion

		#region PROPERTY: DivisionId
		private Int32 _divisionId;
		public Int32 DivisionId
		{
			get { return _divisionId; }
			set { _divisionId = value; }
		}
		#endregion

		#region PROPERTY: ExcludeIds
		private List<Int32> _excludeIds;
		public List<Int32> ExcludeIds
		{
			get
			{
				if (_excludeIds == null)
				{
					_excludeIds = new List<Int32>();
				}
				return _excludeIds;
			}
			set { _excludeIds = value; }
		}

		#endregion


		#region PROPERTY: IncludeAllOption
		private Boolean _includeAllOption = false;

		public Boolean IncludeAllOption
		{
			get { return _includeAllOption; }
			set { _includeAllOption = value; }
		}
		#endregion

		#region DERIVED PROPERTY: ComboBox
		public ComboBox ComboBox
		{
			get 
			{ 
				return _comboBox; 
			}
		}
		#endregion

		#region DERIVED PROPERTY: SelectedValue
		public Business.Rebates.DeliveryWindow SelectedValue
		{
			get
			{
				return (Business.Rebates.DeliveryWindow) _comboBox.SelectedValue;
			}
		}
		#endregion

		#region DERIVED PROPERTY: IsAllSelected
		public Boolean IsAllSelected
		{
			get
			{
				return this.SelectedValue != null && this.SelectedValue.Id == 0;
			}
		}
		#endregion

		#endregion

		#region Constructors

		public DeliveryWindow()
		{
			InitializeComponent();

			_comboBox.DisplayMember = DeliveryWindowProperty.JDECodeName;
		}
		#endregion

		#region Business Logic
		public void Populate()
		{
			_comboBox.SelectedText = "";
			ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.DeliveryWindow> deliveryWindows = new ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.DeliveryWindow>();

			foreach(Evolve.Clients.Lafarge.Cement.Business.Rebates.DeliveryWindow deliveryWindow in DeliveryWindowsUnfiltered)
			{
				if(!this.ExcludeIds.Contains(deliveryWindow.Id))
				{
					deliveryWindows.Add(deliveryWindow);
				}
			}

			if (_includeAllOption)
			{
				Evolve.Clients.Lafarge.Cement.Business.Rebates.DeliveryWindow deliveryWindowAll = new Business.Rebates.DeliveryWindow();

				deliveryWindowAll.Id = 0;
				deliveryWindowAll.Name = "All";

				deliveryWindows.Insert(0, deliveryWindowAll);
			}

			_comboBox.DataSource = deliveryWindows;

			if (_includeAllOption)
			{
				ComboBox.SelectedIndex = 0;
			}
		}
		public Boolean FindAndSelectId(Int32? id)
		{
			if (id == null)
			{
				id = 0;
			}

			Boolean found = false;
			if (ComboBox != null)
			{
				for (Int32 index = 0; index < ComboBox.Items.Count; index++)
				{
					Evolve.Clients.Lafarge.Cement.Business.Rebates.DeliveryWindow rebateType = (Evolve.Clients.Lafarge.Cement.Business.Rebates.DeliveryWindow) ComboBox.Items[index];

					if (rebateType.Id == id)
					{
						ComboBox.SelectedIndex = index;
						found = true;
						break;
					}
				}
			}
			return found;
		}
		#endregion
	}
}

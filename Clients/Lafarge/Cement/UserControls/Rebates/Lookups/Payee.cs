﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates
{
	public partial class Payee : UserControl
	{
		private Int32 _controlSpacing = Evolve.Libraries.Controls.Constants.Control.Spacing;
		private Int32 _divisionId = 0;
		private Boolean _multiSelect = false;
		private List<Business.Rebates.Payee> _selectedPayees;

		// Create an event handlers for single and multi payee selection
		public event EventHandler PayeeSelected;
		public event EventHandler MultiPayeeSelected;

		protected void OnPayeeSelected(EventArgs e)
		{
			if (PayeeSelected != null)
			{
				PayeeSelected(this, e);
			}
		}

		protected void OnMultiPayeeSelected(EventArgs e)
		{
			if (MultiPayeeSelected != null)
			{
				MultiPayeeSelected(this, e);
			}
		}

		// Properties

		public Business.Rebates.Payee SelectedValue
		{
			get { return (_selectedPayees != null && _selectedPayees.Count > 0 ? _selectedPayees[0] : null); }
		}

		public List<Business.Rebates.Payee> SelectedValues
		{
			get { return _selectedPayees; }
		}

		public String LabelText
		{
			get { return lblPayee.Text; }
			set { lblPayee.Text = value; InitializeControl(); }
		}

		public Int32? AccountNumber
		{
			get
			{
				if (txtName.Text != "")
				{
					return Int32.Parse(txtAccountNumber.Text);
				}
				else
				{
					return null;
				}
			}

			set
			{
				txtAccountNumber.Text = value.ToString();
				ReadPayee();
			}
		}

		public Int32 Id
		{
			set
			{
				// Attempt to payee details
				Business.Rebates.Payee payee = new Business.Rebates.Payee();

				payee.Id = value;
				payee.AddReadFilter(PayeeProperty.Id, "=");

				ExtendedBindingList<Business.Rebates.Payee> payees = (ExtendedBindingList<Business.Rebates.Payee>) payee.Read();

				if (payees != null && payees.Count > 0)
				{
					txtName.Text = payees[0].Name;
					txtAccountNumber.Text = FormatAccountNumber(payees[0].JDECode);

					_selectedPayees = new List<Business.Rebates.Payee>();
					_selectedPayees.Add(payees[0]);
				}
				else
				{
					txtName.Text = "";
					txtAccountNumber.Text = "";
				}
			}
		}

		public Int32 DivisionId
		{
			get { return _divisionId; }
			set { _divisionId = value; }
		}

		public Boolean MultiSelect
		{
			get { return _multiSelect; }
			set { _multiSelect = value; }
		}

		public String PayeeName
		{
			get { return txtName.Text; }
		}

		public TextBox AccountNumberTextBox
		{
			get { return txtAccountNumber; }
		}

		public Button Find
		{
			get { return btnFind; }
		}

		public Payee()
		{
			InitializeComponent();
			InitializeControl();

			txtAccountNumber.Validating += new CancelEventHandler(txtAccountNumber_Validating);
			btnFind.Click += new EventHandler(btnFind_Click);
		}

		void txtAccountNumber_Validating(object sender, CancelEventArgs e)
		{
			ReadPayee();
		}

		void btnFind_Click(object sender, EventArgs e)
		{
			PayeeFind payeeFind = new PayeeFind(_divisionId, _multiSelect);
			payeeFind.Icon = ControlUtilities.GetIcon(this);
			payeeFind.ShowDialog();

			if (payeeFind.SelectedPayees != null && payeeFind.SelectedPayees.Count > 0)
			{
				txtAccountNumber.Text = FormatAccountNumber(payeeFind.SelectedPayees[0].JDECode);
				txtName.Text = payeeFind.SelectedPayees[0].Name;

				_selectedPayees = payeeFind.SelectedPayees;

				if (payeeFind.SelectedPayees.Count == 1)
				{
					OnPayeeSelected(new EventArgs());
				}
				else
				{
					OnMultiPayeeSelected(new EventArgs());
				}

			}
		}

		public void InitializeControl()
		{
			// Re-position controls based on the label size
			Point point = txtAccountNumber.Location;
			point.X = lblPayee.Location.X + lblPayee.Size.Width + _controlSpacing;
			txtAccountNumber.Location = point;

			point = txtName.Location;
			point.X = txtAccountNumber.Location.X + txtAccountNumber.Size.Width + _controlSpacing;

			txtName.Location = point;

			point = btnFind.Location;
			point.X = txtName.Location.X + txtName.Size.Width + _controlSpacing;

			btnFind.Location = point;
		}

		public void ReadPayee()
		{
			Int32 accountNumber;
			txtName.Text = "";

			_selectedPayees = null;

			if (Int32.TryParse(txtAccountNumber.Text, out accountNumber))
			{
				// Format displayed account number
				txtAccountNumber.Text = FormatAccountNumber(accountNumber); ;

				// Attempt to read payee details
				Business.Rebates.Payee payee = new Business.Rebates.Payee();

				payee.JDECode = accountNumber;
				payee.DivisionId = _divisionId;
				payee.AddReadFilter(PayeeProperty.JDECode, "=");
				payee.AddReadFilter(PayeeProperty.DivisionId, "=");

				ExtendedBindingList<Business.Rebates.Payee> payees = (ExtendedBindingList<Business.Rebates.Payee>) payee.Read();

				if (payees != null && payees.Count > 0)
				{
					txtName.Text = payees[0].Name;

					_selectedPayees = new List<Business.Rebates.Payee>();
					_selectedPayees.Add(payees[0]);

					OnPayeeSelected(new EventArgs());
				}
				else
				{
					txtAccountNumber.Text = "";
				}
			}
			else
			{
				txtAccountNumber.Text = "";
			}
		}

		private String FormatAccountNumber(Int32 accountNumber)
		{
			return accountNumber.ToString("000000");
		}
	}
}

﻿namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates
{
	partial class RebateType
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this._label = new System.Windows.Forms.Label();
			this._comboBox = new System.Windows.Forms.ComboBox();
			this.SuspendLayout();
			// 
			// _label
			// 
			this._label.AutoSize = true;
			this._label.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this._label.Location = new System.Drawing.Point(6, 4);
			this._label.Name = "_label";
			this._label.Size = new System.Drawing.Size(69, 13);
			this._label.TabIndex = 0;
			this._label.Text = "Rebate Type";
			// 
			// _comboBox
			// 
			this._comboBox.FormattingEnabled = true;
			this._comboBox.Location = new System.Drawing.Point(81, 1);
			this._comboBox.Name = "_comboBox";
			this._comboBox.Size = new System.Drawing.Size(200, 21);
			this._comboBox.TabIndex = 1;
			// 
			// RebateType
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this._comboBox);
			this.Controls.Add(this._label);
			this.Name = "RebateType";
			this.Size = new System.Drawing.Size(325, 23);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label _label;
		private System.Windows.Forms.ComboBox _comboBox;

	}
}

﻿using System;
using System.Windows.Forms;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates.Lookups
{
	public partial class PricingSubGroup : UserControl
	{
		private Int32 _divisionId;

		public Int32 DivisionId
		{
			get { return _divisionId; }
			set { _divisionId = value; }
		}

		private Boolean _includeAllOption = false;

		public Boolean IncludeAllOption
		{
			get { return _includeAllOption; }
			set { _includeAllOption = value; }
		}

		public Evolve.Clients.Lafarge.Cement.Business.Rebates.PricingSubGroup SelectedValue
		{
			get { return (Evolve.Clients.Lafarge.Cement.Business.Rebates.PricingSubGroup) _comboBox.SelectedValue; }

		}

		public ComboBox ComboBox
		{
			get { return _comboBox; }
		}

		public PricingSubGroup()
		{
			InitializeComponent();
			_comboBox.DisplayMember = PricingSubGroupProperty.JDECodeName;
		}

		public void Populate()
		{
			Evolve.Clients.Lafarge.Cement.Business.Rebates.PricingSubGroup pricingSubGroup = new Evolve.Clients.Lafarge.Cement.Business.Rebates.PricingSubGroup();
			pricingSubGroup.DivisionId = _divisionId;
			pricingSubGroup.AddReadFilter(PricingSubGroupProperty.DivisionId, "=");

			ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.PricingSubGroup> pricingSubGroups = (ExtendedBindingList<Evolve.Clients.Lafarge.Cement.Business.Rebates.PricingSubGroup>) pricingSubGroup.Read();

			if (_includeAllOption)
			{
				Evolve.Clients.Lafarge.Cement.Business.Rebates.PricingSubGroup pricingSubGroupAll = new Business.Rebates.PricingSubGroup();

				pricingSubGroupAll.Id = 0;
				pricingSubGroupAll.Name = "";

				pricingSubGroups.Insert(0, pricingSubGroupAll);
			}

			_comboBox.DataSource = pricingSubGroups;

			if (_includeAllOption)
			{
				_comboBox.SelectedIndex = 0;
			}
		}

		public Boolean FindAndSelectId(Int32? id)
		{
			Boolean found = false;

			if (id.HasValue)
			{
				for (Int32 index = 0; index < _comboBox.Items.Count; index++)
				{
					if (((Evolve.Clients.Lafarge.Cement.Business.Rebates.PricingSubGroup) _comboBox.Items[index]).Id == id.Value)
					{
						_comboBox.SelectedIndex = index;
						found = true;
						break;
					}
				}
			}

			return found;
		}
	}
}

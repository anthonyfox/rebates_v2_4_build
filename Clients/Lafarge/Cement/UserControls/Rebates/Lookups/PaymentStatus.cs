﻿using System;
using System.Windows.Forms;
using Evolve.Libraries.Controls.Generic;
using Evolve.Libraries.Utilities;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;

namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates
{
	public partial class PaymentStatus : UserControl
	{
		public PaymentStatusEnum SelectedValue
		{
			get
			{
				if (_comboBox.SelectedValue == null)
				{
					return PaymentStatusEnum.Null;
				}
				else
				{
					return (PaymentStatusEnum) _comboBox.SelectedValue;
				}
			}

			set
			{
				_comboBox.SelectedValue = value;
			}
		}

		public ComboBox ComboBox
		{
			get { return _comboBox; }
		}

		public PaymentStatus()
		{
			InitializeComponent();
			EnumUtilities.ComboBox(_comboBox, typeof(PaymentStatusEnum), true, "All");
		}
	}
}

﻿using System;
using System.Reflection;
using System.Windows.Forms;
using Evolve.Clients.Lafarge.Cement.Business.Rebates;
using System.Drawing;
using Evolve.Libraries.Utilities;

namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates
{
	public partial class RebatePaymentSummary : UserControl
	{
		private Business.Rebates.RebatePaymentSummary _rps;
		private String _numberFormat = "#,0.00";
		private Boolean _isReadOnly = false;
		private String _textBoxTextBefore = "";

		#region Display Accrual Event

		public delegate void DisplayAccrualClickHandler(object sender, EventArgs e);
		public event DisplayAccrualClickHandler DisplayAccrualClick;

		protected void OnDisplayAccrualClick(EventArgs e)
		{
			if (DisplayAccrualClick != null)
			{
				DisplayAccrualClick(this, e);
			}
		}

		#endregion

		#region Payment Amount Changed Event

		public delegate void PaymentAmountChangedHandler(object sender, EventArgs e);
		public event PaymentAmountChangedHandler PaymentAmountChanged;

		protected void OnPaymentAmountChanged(EventArgs e)
		{
			if (PaymentAmountChanged != null)
			{
				PaymentAmountChanged(this, e);
			}
		}
	
		#endregion

		public Business.Rebates.RebatePaymentSummary DataSource
		{
			get 
			{ 
				return _rps; 
			}
			set 
			{ 
				_rps = value;
				BindControls(); 
			}
		}

		public Boolean IsReadOnly
		{
			get
			{
				return !_isReadOnly;
			}
			set
			{
				_isReadOnly = value;

				_lblPaymentEntry.Visible = !_isReadOnly;
				_txtProposed.Visible = !_isReadOnly;
				_lblCloseEntry.Visible = !_isReadOnly;
				_txtClose.Visible = !_isReadOnly;
			}
		}

		public RebatePaymentSummary()
		{
			InitializeComponent();

			_txtProposed.GotFocus += new EventHandler(Payment_GotFocus);
			_txtProposed.LostFocus += new EventHandler(Payment_LostFocus);
			_txtProposed.Leave += new EventHandler(Payment_Leave);
			_txtProposed.Tag = PaymentTypeEnum.Payment;

			_txtClose.GotFocus += new EventHandler(Payment_GotFocus);
			_txtClose.LostFocus += new EventHandler(Payment_LostFocus);
			_txtClose.Leave += new EventHandler(Payment_Leave);
			_txtClose.Tag = PaymentTypeEnum.Close;

			_txtBalance1.TextChanged += new EventHandler(_txtBalance_TextChanged);
			_txtBalance2.TextChanged += new EventHandler(_txtBalance_TextChanged);
			_txtBalance3.TextChanged += new EventHandler(_txtBalance_TextChanged);
			_txtBalance4.TextChanged += new EventHandler(_txtBalance_TextChanged);
			_txtBalance5.TextChanged += new EventHandler(_txtBalance_TextChanged);
			_txtBalance6.TextChanged += new EventHandler(_txtBalance_TextChanged);
			_txtBalance7.TextChanged += new EventHandler(_txtBalance_TextChanged);
			_txtBalance8.TextChanged += new EventHandler(_txtBalance_TextChanged);
			_txtBalance9.TextChanged += new EventHandler(_txtBalance_TextChanged);
			_txtBalance10.TextChanged += new EventHandler(_txtBalance_TextChanged);
			_txtBalance11.TextChanged += new EventHandler(_txtBalance_TextChanged);
			_txtBalance12.TextChanged += new EventHandler(_txtBalance_TextChanged);

			_lnkAccrual.Click += new EventHandler(_lnkAccrual_Click);
		}

		private void BindControls()
		{
			String text = "Rebate Payment Summary";

			foreach (Control control in this._grpRebate.Controls)
			{
				if (control is TextBox)
				{
					if (control.Name != "_txtProposed" && control.Name != "_txtClose")
					{
						String propertyName = control.Name.Substring(4);
						control.DataBindings.Clear();

						if (_rps == null)
						{
							control.Text = "";
						}
						else
						{
							control.DataBindings.Add("Text", _rps, propertyName, true, DataSourceUpdateMode.OnPropertyChanged, null, _numberFormat);
						}
					}
				}
			}

			_txtProposed.DataBindings.Clear();
			_txtClose.DataBindings.Clear();

			if (_rps != null)
			{
				_txtProposed.DataBindings.Add("Text", _rps, "ProposedTotal", true, DataSourceUpdateMode.OnPropertyChanged, null, _numberFormat);
				_txtClose.DataBindings.Add("Text", _rps, "CloseTotal", true, DataSourceUpdateMode.OnPropertyChanged, null, _numberFormat);

				text += ": " + _rps.RebateNumber.ToString() + " - " + _rps.RebateDescription;
			}

			_grpRebate.Text = text;
		}

		void Payment_GotFocus(object sender, EventArgs e)
		{
			if (sender is TextBox)
			{
				TextBox textBox = (TextBox) sender;
				_textBoxTextBefore = textBox.Text;
			}
		}

		void Payment_LostFocus(object sender, EventArgs e)
		{
			if (sender is TextBox)
			{
				TextBox textBox = (TextBox) sender;
				Decimal value;

				if (textBox.Text != "")
				{
					if (!Decimal.TryParse(textBox.Text, out value))
					{
						textBox.Text = _textBoxTextBefore;
					}
				}
			}
		}

		void Payment_Leave(object sender, EventArgs e)
		{
			if (_rps != null)
			{
				TextBox textBox = (TextBox) sender;
				Decimal value = TextBoxValue(textBox);

				_rps.MakePaymentOrClosure(value, (PaymentTypeEnum) textBox.Tag);

				if (textBox.Text != _textBoxTextBefore)
				{
					OnPaymentAmountChanged(new EventArgs());
				}
			}
		}

		void _txtBalance_TextChanged(object sender, EventArgs e)
		{
			TextBox txtBalance = (TextBox) sender;
			Decimal value;

			if (Decimal.TryParse(txtBalance.Text, out value))
			{
				if (value <= 0)
				{
					txtBalance.BackColor = Color.LightGreen;
				}
				else
				{
					txtBalance.BackColor = Color.Orange;
				}
			}
			else
			{
				txtBalance.BackColor = _grpRebate.BackColor;
			}
		}

		void _lnkAccrual_Click(object sender, EventArgs e)
		{
			OnDisplayAccrualClick(new EventArgs());
		}

		private Decimal TextBoxValue(TextBox textBox)
		{
			Decimal value = 0;
			Decimal.TryParse(textBox.Text, out value);
			textBox.Text = value.ToString(_numberFormat);

			return value;
		}

		//private void ApportionPaymentOrClosure(Decimal paymentValue, PaymentTypeEnum paymentType)
		//{
		//    paymentValue = _rps.ApportionPaymentOrClosure(paymentValue, paymentType);
		//}

		//public void QuickPay()
		//{
		//    Decimal paymentValue = _rps.Proposed.Total + _rps.Balance.Total;
		//    QuickPayByAmount(paymentValue);
		//}

		//public void QuickPayByAmount(Decimal paymentValue)
		//{
		//    paymentValue = Decimal.Round(paymentValue, 3);
		//    ApportionPaymentOrClosure(paymentValue, PaymentTypeEnum.Payment);
		//    _txtProposed.Text = paymentValue.ToString(_numberFormat);
		//}

		//public void QuickPayByPercentage(Decimal percent)
		//{
		//    Decimal paymentValue = _rps.Balance.Total * percent / 100;
		//    QuickPayByAmount(paymentValue);
		//}

		//public void SendProposedPaymentsForAuthorisation()
		//{
		//    _rps.SendProposedPaymentsForAuthorisation();
			
		//    _txtProposed.Text = "";
		//    _txtClose.Text = "";

		//    _rps.ApportionPaymentOrClosure(0, PaymentTypeEnum.Payment);
		//    _rps.ApportionPaymentOrClosure(0, PaymentTypeEnum.Close);
		//}

		//public void ClearPayments()
		//{
		//    _txtProposed.Text = "";
		//    _rps.Proposed.Clear();
		//}
	}
}

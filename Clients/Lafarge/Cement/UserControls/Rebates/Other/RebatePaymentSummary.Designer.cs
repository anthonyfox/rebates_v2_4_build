﻿namespace Evolve.Clients.Lafarge.Cement.UserControls.Rebates
{
	partial class RebatePaymentSummary
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this._txtAccrual1 = new System.Windows.Forms.TextBox();
			this._txtAccrual2 = new System.Windows.Forms.TextBox();
			this._txtAccrual3 = new System.Windows.Forms.TextBox();
			this._txtAccrual4 = new System.Windows.Forms.TextBox();
			this._txtAccrual5 = new System.Windows.Forms.TextBox();
			this._txtAccrual6 = new System.Windows.Forms.TextBox();
			this._txtAccrual7 = new System.Windows.Forms.TextBox();
			this._txtAccrual8 = new System.Windows.Forms.TextBox();
			this._txtAccrual11 = new System.Windows.Forms.TextBox();
			this._txtAccrual10 = new System.Windows.Forms.TextBox();
			this._txtAccrual9 = new System.Windows.Forms.TextBox();
			this._txtAccrual12 = new System.Windows.Forms.TextBox();
			this._txtProposed12 = new System.Windows.Forms.TextBox();
			this._txtProposed9 = new System.Windows.Forms.TextBox();
			this._txtProposed10 = new System.Windows.Forms.TextBox();
			this._txtProposed11 = new System.Windows.Forms.TextBox();
			this._txtProposed8 = new System.Windows.Forms.TextBox();
			this._txtProposed7 = new System.Windows.Forms.TextBox();
			this._txtProposed6 = new System.Windows.Forms.TextBox();
			this._txtProposed5 = new System.Windows.Forms.TextBox();
			this._txtProposed4 = new System.Windows.Forms.TextBox();
			this._txtProposed3 = new System.Windows.Forms.TextBox();
			this._txtProposed2 = new System.Windows.Forms.TextBox();
			this._txtProposed1 = new System.Windows.Forms.TextBox();
			this._lblPayment = new System.Windows.Forms.Label();
			this._txtClosed12 = new System.Windows.Forms.TextBox();
			this._txtClosed9 = new System.Windows.Forms.TextBox();
			this._txtClosed10 = new System.Windows.Forms.TextBox();
			this._txtClosed11 = new System.Windows.Forms.TextBox();
			this._txtClosed8 = new System.Windows.Forms.TextBox();
			this._txtClosed7 = new System.Windows.Forms.TextBox();
			this._txtClosed6 = new System.Windows.Forms.TextBox();
			this._txtClosed5 = new System.Windows.Forms.TextBox();
			this._txtClosed4 = new System.Windows.Forms.TextBox();
			this._txtClosed3 = new System.Windows.Forms.TextBox();
			this._txtClosed2 = new System.Windows.Forms.TextBox();
			this._txtClosed1 = new System.Windows.Forms.TextBox();
			this._lblProposedClosure = new System.Windows.Forms.Label();
			this._txtBalance12 = new System.Windows.Forms.TextBox();
			this._txtBalance9 = new System.Windows.Forms.TextBox();
			this._txtBalance10 = new System.Windows.Forms.TextBox();
			this._txtBalance11 = new System.Windows.Forms.TextBox();
			this._txtBalance8 = new System.Windows.Forms.TextBox();
			this._txtBalance7 = new System.Windows.Forms.TextBox();
			this._txtBalance6 = new System.Windows.Forms.TextBox();
			this._txtBalance5 = new System.Windows.Forms.TextBox();
			this._txtBalance4 = new System.Windows.Forms.TextBox();
			this._txtBalance3 = new System.Windows.Forms.TextBox();
			this._txtBalance2 = new System.Windows.Forms.TextBox();
			this._txtBalance1 = new System.Windows.Forms.TextBox();
			this._lblBalance = new System.Windows.Forms.Label();
			this._txtProposed = new System.Windows.Forms.TextBox();
			this._txtClose = new System.Windows.Forms.TextBox();
			this._errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
			this._txtPaid12 = new System.Windows.Forms.TextBox();
			this._txtPaid9 = new System.Windows.Forms.TextBox();
			this._txtPaid10 = new System.Windows.Forms.TextBox();
			this._txtPaid11 = new System.Windows.Forms.TextBox();
			this._txtPaid8 = new System.Windows.Forms.TextBox();
			this._txtPaid7 = new System.Windows.Forms.TextBox();
			this._txtPaid6 = new System.Windows.Forms.TextBox();
			this._txtPaid5 = new System.Windows.Forms.TextBox();
			this._txtPaid4 = new System.Windows.Forms.TextBox();
			this._txtPaid3 = new System.Windows.Forms.TextBox();
			this._txtPaid2 = new System.Windows.Forms.TextBox();
			this._txtPaid1 = new System.Windows.Forms.TextBox();
			this._lblAwaitingAuthorisation = new System.Windows.Forms.Label();
			this._txtClose12 = new System.Windows.Forms.TextBox();
			this._txtClose9 = new System.Windows.Forms.TextBox();
			this._txtClose10 = new System.Windows.Forms.TextBox();
			this._txtClose11 = new System.Windows.Forms.TextBox();
			this._txtClose8 = new System.Windows.Forms.TextBox();
			this._txtClose7 = new System.Windows.Forms.TextBox();
			this._txtClose6 = new System.Windows.Forms.TextBox();
			this._txtClose5 = new System.Windows.Forms.TextBox();
			this._txtClose4 = new System.Windows.Forms.TextBox();
			this._txtClose3 = new System.Windows.Forms.TextBox();
			this._txtClose2 = new System.Windows.Forms.TextBox();
			this._txtClose1 = new System.Windows.Forms.TextBox();
			this._lblClosed = new System.Windows.Forms.Label();
			this._lblPeriod1 = new System.Windows.Forms.Label();
			this._lblPeriod2 = new System.Windows.Forms.Label();
			this._lblPeriod3 = new System.Windows.Forms.Label();
			this._lblPeriod4 = new System.Windows.Forms.Label();
			this._lblPeriod5 = new System.Windows.Forms.Label();
			this._lblPeriod6 = new System.Windows.Forms.Label();
			this._lblPeriod7 = new System.Windows.Forms.Label();
			this._lblPeriod8 = new System.Windows.Forms.Label();
			this._lblPeriod9 = new System.Windows.Forms.Label();
			this._lblPeriod10 = new System.Windows.Forms.Label();
			this._lblPeriod11 = new System.Windows.Forms.Label();
			this._lblPeriod12 = new System.Windows.Forms.Label();
			this._lblPeriod = new System.Windows.Forms.Label();
			this._lblPaymentEntry = new System.Windows.Forms.Label();
			this._lblCloseEntry = new System.Windows.Forms.Label();
			this._grpRebate = new System.Windows.Forms.GroupBox();
			this._txtPendingTotal = new System.Windows.Forms.TextBox();
			this._txtPending12 = new System.Windows.Forms.TextBox();
			this._txtPending9 = new System.Windows.Forms.TextBox();
			this._txtPending10 = new System.Windows.Forms.TextBox();
			this._txtPending11 = new System.Windows.Forms.TextBox();
			this._txtPending8 = new System.Windows.Forms.TextBox();
			this._txtPending7 = new System.Windows.Forms.TextBox();
			this._lblPaid = new System.Windows.Forms.Label();
			this._txtPending6 = new System.Windows.Forms.TextBox();
			this._txtPending1 = new System.Windows.Forms.TextBox();
			this._txtPending5 = new System.Windows.Forms.TextBox();
			this._txtPending2 = new System.Windows.Forms.TextBox();
			this._txtPending4 = new System.Windows.Forms.TextBox();
			this._txtPending3 = new System.Windows.Forms.TextBox();
			this._txtProposedTotal = new System.Windows.Forms.TextBox();
			this._lblYearToDate = new System.Windows.Forms.Label();
			this._txtPaidTotal = new System.Windows.Forms.TextBox();
			this._txtBalanceTotal = new System.Windows.Forms.TextBox();
			this._txtClosedTotal = new System.Windows.Forms.TextBox();
			this._txtCloseTotal = new System.Windows.Forms.TextBox();
			this._txtAccrualTotal = new System.Windows.Forms.TextBox();
			this._lnkAccrual = new System.Windows.Forms.LinkLabel();
			((System.ComponentModel.ISupportInitialize) (this._errorProvider)).BeginInit();
			this._grpRebate.SuspendLayout();
			this.SuspendLayout();
			// 
			// _txtAccrual1
			// 
			this._txtAccrual1.Enabled = false;
			this._txtAccrual1.Location = new System.Drawing.Point(138, 57);
			this._txtAccrual1.Name = "_txtAccrual1";
			this._txtAccrual1.Size = new System.Drawing.Size(63, 20);
			this._txtAccrual1.TabIndex = 15;
			this._txtAccrual1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtAccrual2
			// 
			this._txtAccrual2.Enabled = false;
			this._txtAccrual2.Location = new System.Drawing.Point(207, 57);
			this._txtAccrual2.Name = "_txtAccrual2";
			this._txtAccrual2.Size = new System.Drawing.Size(63, 20);
			this._txtAccrual2.TabIndex = 16;
			this._txtAccrual2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtAccrual3
			// 
			this._txtAccrual3.Enabled = false;
			this._txtAccrual3.Location = new System.Drawing.Point(276, 57);
			this._txtAccrual3.Name = "_txtAccrual3";
			this._txtAccrual3.Size = new System.Drawing.Size(63, 20);
			this._txtAccrual3.TabIndex = 17;
			this._txtAccrual3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtAccrual4
			// 
			this._txtAccrual4.Enabled = false;
			this._txtAccrual4.Location = new System.Drawing.Point(345, 57);
			this._txtAccrual4.Name = "_txtAccrual4";
			this._txtAccrual4.Size = new System.Drawing.Size(63, 20);
			this._txtAccrual4.TabIndex = 18;
			this._txtAccrual4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtAccrual5
			// 
			this._txtAccrual5.Enabled = false;
			this._txtAccrual5.Location = new System.Drawing.Point(414, 57);
			this._txtAccrual5.Name = "_txtAccrual5";
			this._txtAccrual5.Size = new System.Drawing.Size(63, 20);
			this._txtAccrual5.TabIndex = 19;
			this._txtAccrual5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtAccrual6
			// 
			this._txtAccrual6.Enabled = false;
			this._txtAccrual6.Location = new System.Drawing.Point(483, 57);
			this._txtAccrual6.Name = "_txtAccrual6";
			this._txtAccrual6.Size = new System.Drawing.Size(63, 20);
			this._txtAccrual6.TabIndex = 20;
			this._txtAccrual6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtAccrual7
			// 
			this._txtAccrual7.Enabled = false;
			this._txtAccrual7.Location = new System.Drawing.Point(552, 57);
			this._txtAccrual7.Name = "_txtAccrual7";
			this._txtAccrual7.Size = new System.Drawing.Size(63, 20);
			this._txtAccrual7.TabIndex = 21;
			this._txtAccrual7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtAccrual8
			// 
			this._txtAccrual8.Enabled = false;
			this._txtAccrual8.Location = new System.Drawing.Point(621, 57);
			this._txtAccrual8.Name = "_txtAccrual8";
			this._txtAccrual8.Size = new System.Drawing.Size(63, 20);
			this._txtAccrual8.TabIndex = 22;
			this._txtAccrual8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtAccrual11
			// 
			this._txtAccrual11.Enabled = false;
			this._txtAccrual11.Location = new System.Drawing.Point(828, 57);
			this._txtAccrual11.Name = "_txtAccrual11";
			this._txtAccrual11.Size = new System.Drawing.Size(63, 20);
			this._txtAccrual11.TabIndex = 25;
			this._txtAccrual11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtAccrual10
			// 
			this._txtAccrual10.Enabled = false;
			this._txtAccrual10.Location = new System.Drawing.Point(759, 57);
			this._txtAccrual10.Name = "_txtAccrual10";
			this._txtAccrual10.Size = new System.Drawing.Size(63, 20);
			this._txtAccrual10.TabIndex = 24;
			this._txtAccrual10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtAccrual9
			// 
			this._txtAccrual9.Enabled = false;
			this._txtAccrual9.Location = new System.Drawing.Point(690, 57);
			this._txtAccrual9.Name = "_txtAccrual9";
			this._txtAccrual9.Size = new System.Drawing.Size(63, 20);
			this._txtAccrual9.TabIndex = 23;
			this._txtAccrual9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtAccrual12
			// 
			this._txtAccrual12.Enabled = false;
			this._txtAccrual12.Location = new System.Drawing.Point(897, 57);
			this._txtAccrual12.Name = "_txtAccrual12";
			this._txtAccrual12.Size = new System.Drawing.Size(63, 20);
			this._txtAccrual12.TabIndex = 26;
			this._txtAccrual12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtProposed12
			// 
			this._txtProposed12.Enabled = false;
			this._txtProposed12.Location = new System.Drawing.Point(897, 80);
			this._txtProposed12.Name = "_txtProposed12";
			this._txtProposed12.Size = new System.Drawing.Size(63, 20);
			this._txtProposed12.TabIndex = 40;
			this._txtProposed12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtProposed9
			// 
			this._txtProposed9.Enabled = false;
			this._txtProposed9.Location = new System.Drawing.Point(690, 80);
			this._txtProposed9.Name = "_txtProposed9";
			this._txtProposed9.Size = new System.Drawing.Size(63, 20);
			this._txtProposed9.TabIndex = 37;
			this._txtProposed9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtProposed10
			// 
			this._txtProposed10.Enabled = false;
			this._txtProposed10.Location = new System.Drawing.Point(759, 80);
			this._txtProposed10.Name = "_txtProposed10";
			this._txtProposed10.Size = new System.Drawing.Size(63, 20);
			this._txtProposed10.TabIndex = 38;
			this._txtProposed10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtProposed11
			// 
			this._txtProposed11.Enabled = false;
			this._txtProposed11.Location = new System.Drawing.Point(828, 80);
			this._txtProposed11.Name = "_txtProposed11";
			this._txtProposed11.Size = new System.Drawing.Size(63, 20);
			this._txtProposed11.TabIndex = 39;
			this._txtProposed11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtProposed8
			// 
			this._txtProposed8.Enabled = false;
			this._txtProposed8.Location = new System.Drawing.Point(621, 80);
			this._txtProposed8.Name = "_txtProposed8";
			this._txtProposed8.Size = new System.Drawing.Size(63, 20);
			this._txtProposed8.TabIndex = 36;
			this._txtProposed8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtProposed7
			// 
			this._txtProposed7.Enabled = false;
			this._txtProposed7.Location = new System.Drawing.Point(552, 80);
			this._txtProposed7.Name = "_txtProposed7";
			this._txtProposed7.Size = new System.Drawing.Size(63, 20);
			this._txtProposed7.TabIndex = 35;
			this._txtProposed7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtProposed6
			// 
			this._txtProposed6.Enabled = false;
			this._txtProposed6.Location = new System.Drawing.Point(483, 80);
			this._txtProposed6.Name = "_txtProposed6";
			this._txtProposed6.Size = new System.Drawing.Size(63, 20);
			this._txtProposed6.TabIndex = 34;
			this._txtProposed6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtProposed5
			// 
			this._txtProposed5.Enabled = false;
			this._txtProposed5.Location = new System.Drawing.Point(414, 80);
			this._txtProposed5.Name = "_txtProposed5";
			this._txtProposed5.Size = new System.Drawing.Size(63, 20);
			this._txtProposed5.TabIndex = 33;
			this._txtProposed5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtProposed4
			// 
			this._txtProposed4.Enabled = false;
			this._txtProposed4.Location = new System.Drawing.Point(345, 80);
			this._txtProposed4.Name = "_txtProposed4";
			this._txtProposed4.Size = new System.Drawing.Size(63, 20);
			this._txtProposed4.TabIndex = 32;
			this._txtProposed4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtProposed3
			// 
			this._txtProposed3.Enabled = false;
			this._txtProposed3.Location = new System.Drawing.Point(276, 80);
			this._txtProposed3.Name = "_txtProposed3";
			this._txtProposed3.Size = new System.Drawing.Size(63, 20);
			this._txtProposed3.TabIndex = 31;
			this._txtProposed3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtProposed2
			// 
			this._txtProposed2.Enabled = false;
			this._txtProposed2.Location = new System.Drawing.Point(207, 80);
			this._txtProposed2.Name = "_txtProposed2";
			this._txtProposed2.Size = new System.Drawing.Size(63, 20);
			this._txtProposed2.TabIndex = 30;
			this._txtProposed2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtProposed1
			// 
			this._txtProposed1.Enabled = false;
			this._txtProposed1.Location = new System.Drawing.Point(138, 80);
			this._txtProposed1.Name = "_txtProposed1";
			this._txtProposed1.Size = new System.Drawing.Size(63, 20);
			this._txtProposed1.TabIndex = 29;
			this._txtProposed1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _lblPayment
			// 
			this._lblPayment.AutoSize = true;
			this._lblPayment.Location = new System.Drawing.Point(36, 83);
			this._lblPayment.Name = "_lblPayment";
			this._lblPayment.Size = new System.Drawing.Size(96, 13);
			this._lblPayment.TabIndex = 28;
			this._lblPayment.Text = "Proposed Payment";
			// 
			// _txtClosed12
			// 
			this._txtClosed12.Enabled = false;
			this._txtClosed12.Location = new System.Drawing.Point(897, 172);
			this._txtClosed12.Name = "_txtClosed12";
			this._txtClosed12.Size = new System.Drawing.Size(63, 20);
			this._txtClosed12.TabIndex = 96;
			this._txtClosed12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtClosed9
			// 
			this._txtClosed9.Enabled = false;
			this._txtClosed9.Location = new System.Drawing.Point(690, 172);
			this._txtClosed9.Name = "_txtClosed9";
			this._txtClosed9.Size = new System.Drawing.Size(63, 20);
			this._txtClosed9.TabIndex = 93;
			this._txtClosed9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtClosed10
			// 
			this._txtClosed10.Enabled = false;
			this._txtClosed10.Location = new System.Drawing.Point(759, 172);
			this._txtClosed10.Name = "_txtClosed10";
			this._txtClosed10.Size = new System.Drawing.Size(63, 20);
			this._txtClosed10.TabIndex = 94;
			this._txtClosed10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtClosed11
			// 
			this._txtClosed11.Enabled = false;
			this._txtClosed11.Location = new System.Drawing.Point(828, 172);
			this._txtClosed11.Name = "_txtClosed11";
			this._txtClosed11.Size = new System.Drawing.Size(63, 20);
			this._txtClosed11.TabIndex = 95;
			this._txtClosed11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtClosed8
			// 
			this._txtClosed8.Enabled = false;
			this._txtClosed8.Location = new System.Drawing.Point(621, 172);
			this._txtClosed8.Name = "_txtClosed8";
			this._txtClosed8.Size = new System.Drawing.Size(63, 20);
			this._txtClosed8.TabIndex = 92;
			this._txtClosed8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtClosed7
			// 
			this._txtClosed7.Enabled = false;
			this._txtClosed7.Location = new System.Drawing.Point(552, 172);
			this._txtClosed7.Name = "_txtClosed7";
			this._txtClosed7.Size = new System.Drawing.Size(63, 20);
			this._txtClosed7.TabIndex = 91;
			this._txtClosed7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtClosed6
			// 
			this._txtClosed6.Enabled = false;
			this._txtClosed6.Location = new System.Drawing.Point(483, 172);
			this._txtClosed6.Name = "_txtClosed6";
			this._txtClosed6.Size = new System.Drawing.Size(63, 20);
			this._txtClosed6.TabIndex = 90;
			this._txtClosed6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtClosed5
			// 
			this._txtClosed5.Enabled = false;
			this._txtClosed5.Location = new System.Drawing.Point(414, 172);
			this._txtClosed5.Name = "_txtClosed5";
			this._txtClosed5.Size = new System.Drawing.Size(63, 20);
			this._txtClosed5.TabIndex = 89;
			this._txtClosed5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtClosed4
			// 
			this._txtClosed4.Enabled = false;
			this._txtClosed4.Location = new System.Drawing.Point(345, 172);
			this._txtClosed4.Name = "_txtClosed4";
			this._txtClosed4.Size = new System.Drawing.Size(63, 20);
			this._txtClosed4.TabIndex = 88;
			this._txtClosed4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtClosed3
			// 
			this._txtClosed3.Enabled = false;
			this._txtClosed3.Location = new System.Drawing.Point(276, 172);
			this._txtClosed3.Name = "_txtClosed3";
			this._txtClosed3.Size = new System.Drawing.Size(63, 20);
			this._txtClosed3.TabIndex = 87;
			this._txtClosed3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtClosed2
			// 
			this._txtClosed2.Enabled = false;
			this._txtClosed2.Location = new System.Drawing.Point(207, 172);
			this._txtClosed2.Name = "_txtClosed2";
			this._txtClosed2.Size = new System.Drawing.Size(63, 20);
			this._txtClosed2.TabIndex = 86;
			this._txtClosed2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtClosed1
			// 
			this._txtClosed1.Enabled = false;
			this._txtClosed1.Location = new System.Drawing.Point(138, 172);
			this._txtClosed1.Name = "_txtClosed1";
			this._txtClosed1.Size = new System.Drawing.Size(63, 20);
			this._txtClosed1.TabIndex = 85;
			this._txtClosed1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _lblProposedClosure
			// 
			this._lblProposedClosure.AutoSize = true;
			this._lblProposedClosure.Location = new System.Drawing.Point(42, 152);
			this._lblProposedClosure.Name = "_lblProposedClosure";
			this._lblProposedClosure.Size = new System.Drawing.Size(90, 13);
			this._lblProposedClosure.TabIndex = 70;
			this._lblProposedClosure.Text = "Proposed Closure";
			// 
			// _txtBalance12
			// 
			this._txtBalance12.Enabled = false;
			this._txtBalance12.Location = new System.Drawing.Point(897, 206);
			this._txtBalance12.Name = "_txtBalance12";
			this._txtBalance12.Size = new System.Drawing.Size(63, 20);
			this._txtBalance12.TabIndex = 110;
			this._txtBalance12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtBalance9
			// 
			this._txtBalance9.Enabled = false;
			this._txtBalance9.Location = new System.Drawing.Point(690, 206);
			this._txtBalance9.Name = "_txtBalance9";
			this._txtBalance9.Size = new System.Drawing.Size(63, 20);
			this._txtBalance9.TabIndex = 107;
			this._txtBalance9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtBalance10
			// 
			this._txtBalance10.Enabled = false;
			this._txtBalance10.Location = new System.Drawing.Point(759, 206);
			this._txtBalance10.Name = "_txtBalance10";
			this._txtBalance10.Size = new System.Drawing.Size(63, 20);
			this._txtBalance10.TabIndex = 108;
			this._txtBalance10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtBalance11
			// 
			this._txtBalance11.Enabled = false;
			this._txtBalance11.Location = new System.Drawing.Point(828, 206);
			this._txtBalance11.Name = "_txtBalance11";
			this._txtBalance11.Size = new System.Drawing.Size(63, 20);
			this._txtBalance11.TabIndex = 109;
			this._txtBalance11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtBalance8
			// 
			this._txtBalance8.Enabled = false;
			this._txtBalance8.Location = new System.Drawing.Point(621, 206);
			this._txtBalance8.Name = "_txtBalance8";
			this._txtBalance8.Size = new System.Drawing.Size(63, 20);
			this._txtBalance8.TabIndex = 106;
			this._txtBalance8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtBalance7
			// 
			this._txtBalance7.Enabled = false;
			this._txtBalance7.Location = new System.Drawing.Point(552, 206);
			this._txtBalance7.Name = "_txtBalance7";
			this._txtBalance7.Size = new System.Drawing.Size(63, 20);
			this._txtBalance7.TabIndex = 105;
			this._txtBalance7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtBalance6
			// 
			this._txtBalance6.Enabled = false;
			this._txtBalance6.Location = new System.Drawing.Point(483, 206);
			this._txtBalance6.Name = "_txtBalance6";
			this._txtBalance6.Size = new System.Drawing.Size(63, 20);
			this._txtBalance6.TabIndex = 104;
			this._txtBalance6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtBalance5
			// 
			this._txtBalance5.Enabled = false;
			this._txtBalance5.Location = new System.Drawing.Point(414, 206);
			this._txtBalance5.Name = "_txtBalance5";
			this._txtBalance5.Size = new System.Drawing.Size(63, 20);
			this._txtBalance5.TabIndex = 103;
			this._txtBalance5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtBalance4
			// 
			this._txtBalance4.Enabled = false;
			this._txtBalance4.Location = new System.Drawing.Point(345, 206);
			this._txtBalance4.Name = "_txtBalance4";
			this._txtBalance4.Size = new System.Drawing.Size(63, 20);
			this._txtBalance4.TabIndex = 102;
			this._txtBalance4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtBalance3
			// 
			this._txtBalance3.Enabled = false;
			this._txtBalance3.Location = new System.Drawing.Point(276, 206);
			this._txtBalance3.Name = "_txtBalance3";
			this._txtBalance3.Size = new System.Drawing.Size(63, 20);
			this._txtBalance3.TabIndex = 101;
			this._txtBalance3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtBalance2
			// 
			this._txtBalance2.Enabled = false;
			this._txtBalance2.Location = new System.Drawing.Point(207, 206);
			this._txtBalance2.Name = "_txtBalance2";
			this._txtBalance2.Size = new System.Drawing.Size(63, 20);
			this._txtBalance2.TabIndex = 100;
			this._txtBalance2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtBalance1
			// 
			this._txtBalance1.Enabled = false;
			this._txtBalance1.Location = new System.Drawing.Point(138, 206);
			this._txtBalance1.Name = "_txtBalance1";
			this._txtBalance1.Size = new System.Drawing.Size(63, 20);
			this._txtBalance1.TabIndex = 99;
			this._txtBalance1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _lblBalance
			// 
			this._lblBalance.AutoSize = true;
			this._lblBalance.Location = new System.Drawing.Point(86, 209);
			this._lblBalance.Name = "_lblBalance";
			this._lblBalance.Size = new System.Drawing.Size(46, 13);
			this._lblBalance.TabIndex = 98;
			this._lblBalance.Text = "Balance";
			// 
			// _txtProposed
			// 
			this._txtProposed.Location = new System.Drawing.Point(1058, 80);
			this._txtProposed.Name = "_txtProposed";
			this._txtProposed.Size = new System.Drawing.Size(63, 20);
			this._txtProposed.TabIndex = 113;
			this._txtProposed.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtClose
			// 
			this._txtClose.Location = new System.Drawing.Point(1058, 149);
			this._txtClose.Name = "_txtClose";
			this._txtClose.Size = new System.Drawing.Size(63, 20);
			this._txtClose.TabIndex = 115;
			this._txtClose.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _errorProvider
			// 
			this._errorProvider.ContainerControl = this;
			// 
			// _txtPaid12
			// 
			this._txtPaid12.Enabled = false;
			this._txtPaid12.Location = new System.Drawing.Point(897, 126);
			this._txtPaid12.Name = "_txtPaid12";
			this._txtPaid12.Size = new System.Drawing.Size(63, 20);
			this._txtPaid12.TabIndex = 68;
			this._txtPaid12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtPaid9
			// 
			this._txtPaid9.Enabled = false;
			this._txtPaid9.Location = new System.Drawing.Point(690, 126);
			this._txtPaid9.Name = "_txtPaid9";
			this._txtPaid9.Size = new System.Drawing.Size(63, 20);
			this._txtPaid9.TabIndex = 65;
			this._txtPaid9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtPaid10
			// 
			this._txtPaid10.Enabled = false;
			this._txtPaid10.Location = new System.Drawing.Point(759, 126);
			this._txtPaid10.Name = "_txtPaid10";
			this._txtPaid10.Size = new System.Drawing.Size(63, 20);
			this._txtPaid10.TabIndex = 66;
			this._txtPaid10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtPaid11
			// 
			this._txtPaid11.Enabled = false;
			this._txtPaid11.Location = new System.Drawing.Point(828, 126);
			this._txtPaid11.Name = "_txtPaid11";
			this._txtPaid11.Size = new System.Drawing.Size(63, 20);
			this._txtPaid11.TabIndex = 67;
			this._txtPaid11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtPaid8
			// 
			this._txtPaid8.Enabled = false;
			this._txtPaid8.Location = new System.Drawing.Point(621, 126);
			this._txtPaid8.Name = "_txtPaid8";
			this._txtPaid8.Size = new System.Drawing.Size(63, 20);
			this._txtPaid8.TabIndex = 64;
			this._txtPaid8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtPaid7
			// 
			this._txtPaid7.Enabled = false;
			this._txtPaid7.Location = new System.Drawing.Point(552, 126);
			this._txtPaid7.Name = "_txtPaid7";
			this._txtPaid7.Size = new System.Drawing.Size(63, 20);
			this._txtPaid7.TabIndex = 63;
			this._txtPaid7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtPaid6
			// 
			this._txtPaid6.Enabled = false;
			this._txtPaid6.Location = new System.Drawing.Point(483, 126);
			this._txtPaid6.Name = "_txtPaid6";
			this._txtPaid6.Size = new System.Drawing.Size(63, 20);
			this._txtPaid6.TabIndex = 62;
			this._txtPaid6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtPaid5
			// 
			this._txtPaid5.Enabled = false;
			this._txtPaid5.Location = new System.Drawing.Point(414, 126);
			this._txtPaid5.Name = "_txtPaid5";
			this._txtPaid5.Size = new System.Drawing.Size(63, 20);
			this._txtPaid5.TabIndex = 61;
			this._txtPaid5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtPaid4
			// 
			this._txtPaid4.Enabled = false;
			this._txtPaid4.Location = new System.Drawing.Point(345, 126);
			this._txtPaid4.Name = "_txtPaid4";
			this._txtPaid4.Size = new System.Drawing.Size(63, 20);
			this._txtPaid4.TabIndex = 60;
			this._txtPaid4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtPaid3
			// 
			this._txtPaid3.Enabled = false;
			this._txtPaid3.Location = new System.Drawing.Point(276, 126);
			this._txtPaid3.Name = "_txtPaid3";
			this._txtPaid3.Size = new System.Drawing.Size(63, 20);
			this._txtPaid3.TabIndex = 59;
			this._txtPaid3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtPaid2
			// 
			this._txtPaid2.Enabled = false;
			this._txtPaid2.Location = new System.Drawing.Point(207, 126);
			this._txtPaid2.Name = "_txtPaid2";
			this._txtPaid2.Size = new System.Drawing.Size(63, 20);
			this._txtPaid2.TabIndex = 58;
			this._txtPaid2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtPaid1
			// 
			this._txtPaid1.Enabled = false;
			this._txtPaid1.Location = new System.Drawing.Point(138, 126);
			this._txtPaid1.Name = "_txtPaid1";
			this._txtPaid1.Size = new System.Drawing.Size(63, 20);
			this._txtPaid1.TabIndex = 57;
			this._txtPaid1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _lblAwaitingAuthorisation
			// 
			this._lblAwaitingAuthorisation.AutoSize = true;
			this._lblAwaitingAuthorisation.Location = new System.Drawing.Point(21, 106);
			this._lblAwaitingAuthorisation.Name = "_lblAwaitingAuthorisation";
			this._lblAwaitingAuthorisation.Size = new System.Drawing.Size(111, 13);
			this._lblAwaitingAuthorisation.TabIndex = 42;
			this._lblAwaitingAuthorisation.Text = "Awaiting Authorisation";
			// 
			// _txtClose12
			// 
			this._txtClose12.Enabled = false;
			this._txtClose12.Location = new System.Drawing.Point(897, 149);
			this._txtClose12.Name = "_txtClose12";
			this._txtClose12.Size = new System.Drawing.Size(63, 20);
			this._txtClose12.TabIndex = 82;
			this._txtClose12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtClose9
			// 
			this._txtClose9.Enabled = false;
			this._txtClose9.Location = new System.Drawing.Point(690, 149);
			this._txtClose9.Name = "_txtClose9";
			this._txtClose9.Size = new System.Drawing.Size(63, 20);
			this._txtClose9.TabIndex = 79;
			this._txtClose9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtClose10
			// 
			this._txtClose10.Enabled = false;
			this._txtClose10.Location = new System.Drawing.Point(759, 149);
			this._txtClose10.Name = "_txtClose10";
			this._txtClose10.Size = new System.Drawing.Size(63, 20);
			this._txtClose10.TabIndex = 80;
			this._txtClose10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtClose11
			// 
			this._txtClose11.Enabled = false;
			this._txtClose11.Location = new System.Drawing.Point(828, 149);
			this._txtClose11.Name = "_txtClose11";
			this._txtClose11.Size = new System.Drawing.Size(63, 20);
			this._txtClose11.TabIndex = 81;
			this._txtClose11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtClose8
			// 
			this._txtClose8.Enabled = false;
			this._txtClose8.Location = new System.Drawing.Point(621, 149);
			this._txtClose8.Name = "_txtClose8";
			this._txtClose8.Size = new System.Drawing.Size(63, 20);
			this._txtClose8.TabIndex = 78;
			this._txtClose8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtClose7
			// 
			this._txtClose7.Enabled = false;
			this._txtClose7.Location = new System.Drawing.Point(552, 149);
			this._txtClose7.Name = "_txtClose7";
			this._txtClose7.Size = new System.Drawing.Size(63, 20);
			this._txtClose7.TabIndex = 77;
			this._txtClose7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtClose6
			// 
			this._txtClose6.Enabled = false;
			this._txtClose6.Location = new System.Drawing.Point(483, 149);
			this._txtClose6.Name = "_txtClose6";
			this._txtClose6.Size = new System.Drawing.Size(63, 20);
			this._txtClose6.TabIndex = 76;
			this._txtClose6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtClose5
			// 
			this._txtClose5.Enabled = false;
			this._txtClose5.Location = new System.Drawing.Point(414, 149);
			this._txtClose5.Name = "_txtClose5";
			this._txtClose5.Size = new System.Drawing.Size(63, 20);
			this._txtClose5.TabIndex = 75;
			this._txtClose5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtClose4
			// 
			this._txtClose4.Enabled = false;
			this._txtClose4.Location = new System.Drawing.Point(345, 149);
			this._txtClose4.Name = "_txtClose4";
			this._txtClose4.Size = new System.Drawing.Size(63, 20);
			this._txtClose4.TabIndex = 74;
			this._txtClose4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtClose3
			// 
			this._txtClose3.Enabled = false;
			this._txtClose3.Location = new System.Drawing.Point(276, 149);
			this._txtClose3.Name = "_txtClose3";
			this._txtClose3.Size = new System.Drawing.Size(63, 20);
			this._txtClose3.TabIndex = 73;
			this._txtClose3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtClose2
			// 
			this._txtClose2.Enabled = false;
			this._txtClose2.Location = new System.Drawing.Point(207, 149);
			this._txtClose2.Name = "_txtClose2";
			this._txtClose2.Size = new System.Drawing.Size(63, 20);
			this._txtClose2.TabIndex = 72;
			this._txtClose2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtClose1
			// 
			this._txtClose1.Enabled = false;
			this._txtClose1.Location = new System.Drawing.Point(138, 149);
			this._txtClose1.Name = "_txtClose1";
			this._txtClose1.Size = new System.Drawing.Size(63, 20);
			this._txtClose1.TabIndex = 71;
			this._txtClose1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _lblClosed
			// 
			this._lblClosed.AutoSize = true;
			this._lblClosed.Location = new System.Drawing.Point(93, 175);
			this._lblClosed.Name = "_lblClosed";
			this._lblClosed.Size = new System.Drawing.Size(39, 13);
			this._lblClosed.TabIndex = 84;
			this._lblClosed.Text = "Closed";
			// 
			// _lblPeriod1
			// 
			this._lblPeriod1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
			this._lblPeriod1.Location = new System.Drawing.Point(138, 39);
			this._lblPeriod1.Name = "_lblPeriod1";
			this._lblPeriod1.Size = new System.Drawing.Size(63, 15);
			this._lblPeriod1.TabIndex = 1;
			this._lblPeriod1.Text = "1";
			this._lblPeriod1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// _lblPeriod2
			// 
			this._lblPeriod2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
			this._lblPeriod2.Location = new System.Drawing.Point(207, 39);
			this._lblPeriod2.Name = "_lblPeriod2";
			this._lblPeriod2.Size = new System.Drawing.Size(63, 15);
			this._lblPeriod2.TabIndex = 2;
			this._lblPeriod2.Text = "2";
			this._lblPeriod2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// _lblPeriod3
			// 
			this._lblPeriod3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
			this._lblPeriod3.Location = new System.Drawing.Point(276, 39);
			this._lblPeriod3.Name = "_lblPeriod3";
			this._lblPeriod3.Size = new System.Drawing.Size(63, 15);
			this._lblPeriod3.TabIndex = 3;
			this._lblPeriod3.Text = "3";
			this._lblPeriod3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// _lblPeriod4
			// 
			this._lblPeriod4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
			this._lblPeriod4.Location = new System.Drawing.Point(345, 39);
			this._lblPeriod4.Name = "_lblPeriod4";
			this._lblPeriod4.Size = new System.Drawing.Size(63, 15);
			this._lblPeriod4.TabIndex = 4;
			this._lblPeriod4.Text = "4";
			this._lblPeriod4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// _lblPeriod5
			// 
			this._lblPeriod5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
			this._lblPeriod5.Location = new System.Drawing.Point(414, 39);
			this._lblPeriod5.Name = "_lblPeriod5";
			this._lblPeriod5.Size = new System.Drawing.Size(63, 15);
			this._lblPeriod5.TabIndex = 5;
			this._lblPeriod5.Text = "5";
			this._lblPeriod5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// _lblPeriod6
			// 
			this._lblPeriod6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
			this._lblPeriod6.Location = new System.Drawing.Point(483, 39);
			this._lblPeriod6.Name = "_lblPeriod6";
			this._lblPeriod6.Size = new System.Drawing.Size(63, 15);
			this._lblPeriod6.TabIndex = 6;
			this._lblPeriod6.Text = "6";
			this._lblPeriod6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// _lblPeriod7
			// 
			this._lblPeriod7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
			this._lblPeriod7.Location = new System.Drawing.Point(552, 39);
			this._lblPeriod7.Name = "_lblPeriod7";
			this._lblPeriod7.Size = new System.Drawing.Size(63, 15);
			this._lblPeriod7.TabIndex = 7;
			this._lblPeriod7.Text = "7";
			this._lblPeriod7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// _lblPeriod8
			// 
			this._lblPeriod8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
			this._lblPeriod8.Location = new System.Drawing.Point(618, 39);
			this._lblPeriod8.Name = "_lblPeriod8";
			this._lblPeriod8.Size = new System.Drawing.Size(63, 15);
			this._lblPeriod8.TabIndex = 8;
			this._lblPeriod8.Text = "8";
			this._lblPeriod8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// _lblPeriod9
			// 
			this._lblPeriod9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
			this._lblPeriod9.Location = new System.Drawing.Point(687, 39);
			this._lblPeriod9.Name = "_lblPeriod9";
			this._lblPeriod9.Size = new System.Drawing.Size(63, 15);
			this._lblPeriod9.TabIndex = 9;
			this._lblPeriod9.Text = "9";
			this._lblPeriod9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// _lblPeriod10
			// 
			this._lblPeriod10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
			this._lblPeriod10.Location = new System.Drawing.Point(759, 39);
			this._lblPeriod10.Name = "_lblPeriod10";
			this._lblPeriod10.Size = new System.Drawing.Size(63, 15);
			this._lblPeriod10.TabIndex = 10;
			this._lblPeriod10.Text = "10";
			this._lblPeriod10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// _lblPeriod11
			// 
			this._lblPeriod11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
			this._lblPeriod11.Location = new System.Drawing.Point(828, 39);
			this._lblPeriod11.Name = "_lblPeriod11";
			this._lblPeriod11.Size = new System.Drawing.Size(63, 15);
			this._lblPeriod11.TabIndex = 11;
			this._lblPeriod11.Text = "11";
			this._lblPeriod11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// _lblPeriod12
			// 
			this._lblPeriod12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
			this._lblPeriod12.Location = new System.Drawing.Point(897, 39);
			this._lblPeriod12.Name = "_lblPeriod12";
			this._lblPeriod12.Size = new System.Drawing.Size(63, 15);
			this._lblPeriod12.TabIndex = 12;
			this._lblPeriod12.Text = "12";
			this._lblPeriod12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// _lblPeriod
			// 
			this._lblPeriod.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
			this._lblPeriod.Location = new System.Drawing.Point(138, 15);
			this._lblPeriod.Name = "_lblPeriod";
			this._lblPeriod.Size = new System.Drawing.Size(822, 24);
			this._lblPeriod.TabIndex = 0;
			this._lblPeriod.Text = "Period";
			this._lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// _lblPaymentEntry
			// 
			this._lblPaymentEntry.Location = new System.Drawing.Point(1058, 60);
			this._lblPaymentEntry.Name = "_lblPaymentEntry";
			this._lblPaymentEntry.Size = new System.Drawing.Size(63, 18);
			this._lblPaymentEntry.TabIndex = 112;
			this._lblPaymentEntry.Text = "Payment";
			this._lblPaymentEntry.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// _lblCloseEntry
			// 
			this._lblCloseEntry.Location = new System.Drawing.Point(1058, 130);
			this._lblCloseEntry.Name = "_lblCloseEntry";
			this._lblCloseEntry.Size = new System.Drawing.Size(63, 18);
			this._lblCloseEntry.TabIndex = 114;
			this._lblCloseEntry.Text = "Close";
			this._lblCloseEntry.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// _grpRebate
			// 
			this._grpRebate.Controls.Add(this._txtPendingTotal);
			this._grpRebate.Controls.Add(this._txtPending12);
			this._grpRebate.Controls.Add(this._txtPending9);
			this._grpRebate.Controls.Add(this._txtPending10);
			this._grpRebate.Controls.Add(this._txtPending11);
			this._grpRebate.Controls.Add(this._txtPending8);
			this._grpRebate.Controls.Add(this._txtPending7);
			this._grpRebate.Controls.Add(this._lblPaid);
			this._grpRebate.Controls.Add(this._txtPending6);
			this._grpRebate.Controls.Add(this._txtPending1);
			this._grpRebate.Controls.Add(this._txtPending5);
			this._grpRebate.Controls.Add(this._txtPending2);
			this._grpRebate.Controls.Add(this._txtPending4);
			this._grpRebate.Controls.Add(this._txtPending3);
			this._grpRebate.Controls.Add(this._txtProposedTotal);
			this._grpRebate.Controls.Add(this._lblYearToDate);
			this._grpRebate.Controls.Add(this._txtPaidTotal);
			this._grpRebate.Controls.Add(this._txtBalanceTotal);
			this._grpRebate.Controls.Add(this._txtClosedTotal);
			this._grpRebate.Controls.Add(this._txtCloseTotal);
			this._grpRebate.Controls.Add(this._txtAccrualTotal);
			this._grpRebate.Controls.Add(this._lnkAccrual);
			this._grpRebate.Controls.Add(this._lblPeriod);
			this._grpRebate.Controls.Add(this._txtProposed11);
			this._grpRebate.Controls.Add(this._txtClose2);
			this._grpRebate.Controls.Add(this._lblCloseEntry);
			this._grpRebate.Controls.Add(this._txtClose3);
			this._grpRebate.Controls.Add(this._lblPaymentEntry);
			this._grpRebate.Controls.Add(this._txtProposed10);
			this._grpRebate.Controls.Add(this._txtProposed8);
			this._grpRebate.Controls.Add(this._lblPeriod1);
			this._grpRebate.Controls.Add(this._txtClose1);
			this._grpRebate.Controls.Add(this._txtBalance8);
			this._grpRebate.Controls.Add(this._txtClose4);
			this._grpRebate.Controls.Add(this._txtBalance7);
			this._grpRebate.Controls.Add(this._txtProposed9);
			this._grpRebate.Controls.Add(this._txtBalance11);
			this._grpRebate.Controls.Add(this._txtProposed7);
			this._grpRebate.Controls.Add(this._txtBalance6);
			this._grpRebate.Controls.Add(this._lblClosed);
			this._grpRebate.Controls.Add(this._txtClose5);
			this._grpRebate.Controls.Add(this._txtBalance10);
			this._grpRebate.Controls.Add(this._txtProposed12);
			this._grpRebate.Controls.Add(this._lblPeriod12);
			this._grpRebate.Controls.Add(this._txtProposed6);
			this._grpRebate.Controls.Add(this._txtBalance5);
			this._grpRebate.Controls.Add(this._txtPaid12);
			this._grpRebate.Controls.Add(this._txtAccrual1);
			this._grpRebate.Controls.Add(this._txtClose6);
			this._grpRebate.Controls.Add(this._txtBalance9);
			this._grpRebate.Controls.Add(this._lblProposedClosure);
			this._grpRebate.Controls.Add(this._lblPeriod11);
			this._grpRebate.Controls.Add(this._txtProposed5);
			this._grpRebate.Controls.Add(this._txtBalance4);
			this._grpRebate.Controls.Add(this._txtPaid9);
			this._grpRebate.Controls.Add(this._txtAccrual2);
			this._grpRebate.Controls.Add(this._txtClose7);
			this._grpRebate.Controls.Add(this._txtBalance12);
			this._grpRebate.Controls.Add(this._txtClosed1);
			this._grpRebate.Controls.Add(this._lblPeriod10);
			this._grpRebate.Controls.Add(this._txtProposed4);
			this._grpRebate.Controls.Add(this._txtBalance3);
			this._grpRebate.Controls.Add(this._txtPaid10);
			this._grpRebate.Controls.Add(this._txtAccrual3);
			this._grpRebate.Controls.Add(this._txtClose8);
			this._grpRebate.Controls.Add(this._txtBalance2);
			this._grpRebate.Controls.Add(this._txtClosed2);
			this._grpRebate.Controls.Add(this._lblPeriod9);
			this._grpRebate.Controls.Add(this._txtProposed3);
			this._grpRebate.Controls.Add(this._txtBalance1);
			this._grpRebate.Controls.Add(this._txtPaid11);
			this._grpRebate.Controls.Add(this._txtAccrual4);
			this._grpRebate.Controls.Add(this._txtClose11);
			this._grpRebate.Controls.Add(this._txtProposed);
			this._grpRebate.Controls.Add(this._txtClosed3);
			this._grpRebate.Controls.Add(this._lblPeriod8);
			this._grpRebate.Controls.Add(this._txtProposed2);
			this._grpRebate.Controls.Add(this._lblBalance);
			this._grpRebate.Controls.Add(this._txtPaid8);
			this._grpRebate.Controls.Add(this._txtAccrual5);
			this._grpRebate.Controls.Add(this._txtClose10);
			this._grpRebate.Controls.Add(this._txtClose);
			this._grpRebate.Controls.Add(this._txtClosed4);
			this._grpRebate.Controls.Add(this._lblPeriod7);
			this._grpRebate.Controls.Add(this._txtProposed1);
			this._grpRebate.Controls.Add(this._txtClosed12);
			this._grpRebate.Controls.Add(this._txtPaid7);
			this._grpRebate.Controls.Add(this._txtAccrual6);
			this._grpRebate.Controls.Add(this._txtClose9);
			this._grpRebate.Controls.Add(this._lblAwaitingAuthorisation);
			this._grpRebate.Controls.Add(this._txtClosed5);
			this._grpRebate.Controls.Add(this._lblPeriod6);
			this._grpRebate.Controls.Add(this._lblPayment);
			this._grpRebate.Controls.Add(this._txtClosed9);
			this._grpRebate.Controls.Add(this._txtPaid6);
			this._grpRebate.Controls.Add(this._txtAccrual7);
			this._grpRebate.Controls.Add(this._txtClose12);
			this._grpRebate.Controls.Add(this._txtPaid1);
			this._grpRebate.Controls.Add(this._txtClosed6);
			this._grpRebate.Controls.Add(this._lblPeriod5);
			this._grpRebate.Controls.Add(this._txtAccrual12);
			this._grpRebate.Controls.Add(this._txtClosed10);
			this._grpRebate.Controls.Add(this._txtPaid5);
			this._grpRebate.Controls.Add(this._txtAccrual8);
			this._grpRebate.Controls.Add(this._txtAccrual9);
			this._grpRebate.Controls.Add(this._txtPaid2);
			this._grpRebate.Controls.Add(this._txtClosed7);
			this._grpRebate.Controls.Add(this._lblPeriod4);
			this._grpRebate.Controls.Add(this._lblPeriod2);
			this._grpRebate.Controls.Add(this._txtClosed11);
			this._grpRebate.Controls.Add(this._txtPaid4);
			this._grpRebate.Controls.Add(this._txtAccrual11);
			this._grpRebate.Controls.Add(this._txtAccrual10);
			this._grpRebate.Controls.Add(this._txtPaid3);
			this._grpRebate.Controls.Add(this._txtClosed8);
			this._grpRebate.Controls.Add(this._lblPeriod3);
			this._grpRebate.Dock = System.Windows.Forms.DockStyle.Fill;
			this._grpRebate.Location = new System.Drawing.Point(0, 0);
			this._grpRebate.Name = "_grpRebate";
			this._grpRebate.Size = new System.Drawing.Size(1155, 245);
			this._grpRebate.TabIndex = 0;
			this._grpRebate.TabStop = false;
			this._grpRebate.Text = "Rebate Payment Summary";
			// 
			// _txtPendingTotal
			// 
			this._txtPendingTotal.Enabled = false;
			this._txtPendingTotal.Location = new System.Drawing.Point(978, 103);
			this._txtPendingTotal.Name = "_txtPendingTotal";
			this._txtPendingTotal.Size = new System.Drawing.Size(63, 20);
			this._txtPendingTotal.TabIndex = 55;
			this._txtPendingTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtPending12
			// 
			this._txtPending12.Enabled = false;
			this._txtPending12.Location = new System.Drawing.Point(897, 103);
			this._txtPending12.Name = "_txtPending12";
			this._txtPending12.Size = new System.Drawing.Size(63, 20);
			this._txtPending12.TabIndex = 54;
			this._txtPending12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtPending9
			// 
			this._txtPending9.Enabled = false;
			this._txtPending9.Location = new System.Drawing.Point(690, 103);
			this._txtPending9.Name = "_txtPending9";
			this._txtPending9.Size = new System.Drawing.Size(63, 20);
			this._txtPending9.TabIndex = 51;
			this._txtPending9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtPending10
			// 
			this._txtPending10.Enabled = false;
			this._txtPending10.Location = new System.Drawing.Point(759, 103);
			this._txtPending10.Name = "_txtPending10";
			this._txtPending10.Size = new System.Drawing.Size(63, 20);
			this._txtPending10.TabIndex = 52;
			this._txtPending10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtPending11
			// 
			this._txtPending11.Enabled = false;
			this._txtPending11.Location = new System.Drawing.Point(828, 103);
			this._txtPending11.Name = "_txtPending11";
			this._txtPending11.Size = new System.Drawing.Size(63, 20);
			this._txtPending11.TabIndex = 53;
			this._txtPending11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtPending8
			// 
			this._txtPending8.Enabled = false;
			this._txtPending8.Location = new System.Drawing.Point(621, 103);
			this._txtPending8.Name = "_txtPending8";
			this._txtPending8.Size = new System.Drawing.Size(63, 20);
			this._txtPending8.TabIndex = 50;
			this._txtPending8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtPending7
			// 
			this._txtPending7.Enabled = false;
			this._txtPending7.Location = new System.Drawing.Point(552, 103);
			this._txtPending7.Name = "_txtPending7";
			this._txtPending7.Size = new System.Drawing.Size(63, 20);
			this._txtPending7.TabIndex = 49;
			this._txtPending7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _lblPaid
			// 
			this._lblPaid.AutoSize = true;
			this._lblPaid.Location = new System.Drawing.Point(104, 129);
			this._lblPaid.Name = "_lblPaid";
			this._lblPaid.Size = new System.Drawing.Size(28, 13);
			this._lblPaid.TabIndex = 56;
			this._lblPaid.Text = "Paid";
			// 
			// _txtPending6
			// 
			this._txtPending6.Enabled = false;
			this._txtPending6.Location = new System.Drawing.Point(483, 103);
			this._txtPending6.Name = "_txtPending6";
			this._txtPending6.Size = new System.Drawing.Size(63, 20);
			this._txtPending6.TabIndex = 48;
			this._txtPending6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtPending1
			// 
			this._txtPending1.Enabled = false;
			this._txtPending1.Location = new System.Drawing.Point(138, 103);
			this._txtPending1.Name = "_txtPending1";
			this._txtPending1.Size = new System.Drawing.Size(63, 20);
			this._txtPending1.TabIndex = 43;
			this._txtPending1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtPending5
			// 
			this._txtPending5.Enabled = false;
			this._txtPending5.Location = new System.Drawing.Point(414, 103);
			this._txtPending5.Name = "_txtPending5";
			this._txtPending5.Size = new System.Drawing.Size(63, 20);
			this._txtPending5.TabIndex = 47;
			this._txtPending5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtPending2
			// 
			this._txtPending2.Enabled = false;
			this._txtPending2.Location = new System.Drawing.Point(207, 103);
			this._txtPending2.Name = "_txtPending2";
			this._txtPending2.Size = new System.Drawing.Size(63, 20);
			this._txtPending2.TabIndex = 44;
			this._txtPending2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtPending4
			// 
			this._txtPending4.Enabled = false;
			this._txtPending4.Location = new System.Drawing.Point(345, 103);
			this._txtPending4.Name = "_txtPending4";
			this._txtPending4.Size = new System.Drawing.Size(63, 20);
			this._txtPending4.TabIndex = 46;
			this._txtPending4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtPending3
			// 
			this._txtPending3.Enabled = false;
			this._txtPending3.Location = new System.Drawing.Point(276, 103);
			this._txtPending3.Name = "_txtPending3";
			this._txtPending3.Size = new System.Drawing.Size(63, 20);
			this._txtPending3.TabIndex = 45;
			this._txtPending3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtProposedTotal
			// 
			this._txtProposedTotal.Enabled = false;
			this._txtProposedTotal.Location = new System.Drawing.Point(978, 80);
			this._txtProposedTotal.Name = "_txtProposedTotal";
			this._txtProposedTotal.Size = new System.Drawing.Size(63, 20);
			this._txtProposedTotal.TabIndex = 41;
			this._txtProposedTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _lblYearToDate
			// 
			this._lblYearToDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
			this._lblYearToDate.Location = new System.Drawing.Point(978, 39);
			this._lblYearToDate.Name = "_lblYearToDate";
			this._lblYearToDate.Size = new System.Drawing.Size(63, 15);
			this._lblYearToDate.TabIndex = 13;
			this._lblYearToDate.Text = "YTD";
			this._lblYearToDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// _txtPaidTotal
			// 
			this._txtPaidTotal.Enabled = false;
			this._txtPaidTotal.Location = new System.Drawing.Point(978, 126);
			this._txtPaidTotal.Name = "_txtPaidTotal";
			this._txtPaidTotal.Size = new System.Drawing.Size(63, 20);
			this._txtPaidTotal.TabIndex = 69;
			this._txtPaidTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtBalanceTotal
			// 
			this._txtBalanceTotal.Enabled = false;
			this._txtBalanceTotal.Location = new System.Drawing.Point(978, 206);
			this._txtBalanceTotal.Name = "_txtBalanceTotal";
			this._txtBalanceTotal.Size = new System.Drawing.Size(63, 20);
			this._txtBalanceTotal.TabIndex = 111;
			this._txtBalanceTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtClosedTotal
			// 
			this._txtClosedTotal.Enabled = false;
			this._txtClosedTotal.Location = new System.Drawing.Point(978, 172);
			this._txtClosedTotal.Name = "_txtClosedTotal";
			this._txtClosedTotal.Size = new System.Drawing.Size(63, 20);
			this._txtClosedTotal.TabIndex = 97;
			this._txtClosedTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtCloseTotal
			// 
			this._txtCloseTotal.Enabled = false;
			this._txtCloseTotal.Location = new System.Drawing.Point(978, 149);
			this._txtCloseTotal.Name = "_txtCloseTotal";
			this._txtCloseTotal.Size = new System.Drawing.Size(63, 20);
			this._txtCloseTotal.TabIndex = 83;
			this._txtCloseTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _txtAccrualTotal
			// 
			this._txtAccrualTotal.Enabled = false;
			this._txtAccrualTotal.Location = new System.Drawing.Point(978, 57);
			this._txtAccrualTotal.Name = "_txtAccrualTotal";
			this._txtAccrualTotal.Size = new System.Drawing.Size(63, 20);
			this._txtAccrualTotal.TabIndex = 27;
			this._txtAccrualTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _lnkAccrual
			// 
			this._lnkAccrual.AutoSize = true;
			this._lnkAccrual.Location = new System.Drawing.Point(89, 60);
			this._lnkAccrual.Name = "_lnkAccrual";
			this._lnkAccrual.Size = new System.Drawing.Size(43, 13);
			this._lnkAccrual.TabIndex = 14;
			this._lnkAccrual.TabStop = true;
			this._lnkAccrual.Text = "Accrual";
			// 
			// RebatePaymentSummary
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this._grpRebate);
			this.Name = "RebatePaymentSummary";
			this.Size = new System.Drawing.Size(1155, 245);
			((System.ComponentModel.ISupportInitialize) (this._errorProvider)).EndInit();
			this._grpRebate.ResumeLayout(false);
			this._grpRebate.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TextBox _txtAccrual1;
		private System.Windows.Forms.TextBox _txtAccrual2;
		private System.Windows.Forms.TextBox _txtAccrual3;
		private System.Windows.Forms.TextBox _txtAccrual4;
		private System.Windows.Forms.TextBox _txtAccrual5;
		private System.Windows.Forms.TextBox _txtAccrual6;
		private System.Windows.Forms.TextBox _txtAccrual7;
		private System.Windows.Forms.TextBox _txtAccrual8;
		private System.Windows.Forms.TextBox _txtAccrual11;
		private System.Windows.Forms.TextBox _txtAccrual10;
		private System.Windows.Forms.TextBox _txtAccrual9;
		private System.Windows.Forms.TextBox _txtAccrual12;
		private System.Windows.Forms.TextBox _txtProposed12;
		private System.Windows.Forms.TextBox _txtProposed9;
		private System.Windows.Forms.TextBox _txtProposed10;
		private System.Windows.Forms.TextBox _txtProposed11;
		private System.Windows.Forms.TextBox _txtProposed8;
		private System.Windows.Forms.TextBox _txtProposed7;
		private System.Windows.Forms.TextBox _txtProposed6;
		private System.Windows.Forms.TextBox _txtProposed5;
		private System.Windows.Forms.TextBox _txtProposed4;
		private System.Windows.Forms.TextBox _txtProposed3;
		private System.Windows.Forms.TextBox _txtProposed2;
		private System.Windows.Forms.TextBox _txtProposed1;
		private System.Windows.Forms.Label _lblPayment;
		private System.Windows.Forms.TextBox _txtClosed12;
		private System.Windows.Forms.TextBox _txtClosed9;
		private System.Windows.Forms.TextBox _txtClosed10;
		private System.Windows.Forms.TextBox _txtClosed11;
		private System.Windows.Forms.TextBox _txtClosed8;
		private System.Windows.Forms.TextBox _txtClosed7;
		private System.Windows.Forms.TextBox _txtClosed6;
		private System.Windows.Forms.TextBox _txtClosed5;
		private System.Windows.Forms.TextBox _txtClosed4;
		private System.Windows.Forms.TextBox _txtClosed3;
		private System.Windows.Forms.TextBox _txtClosed2;
		private System.Windows.Forms.TextBox _txtClosed1;
		private System.Windows.Forms.Label _lblProposedClosure;
		private System.Windows.Forms.TextBox _txtBalance12;
		private System.Windows.Forms.TextBox _txtBalance9;
		private System.Windows.Forms.TextBox _txtBalance10;
		private System.Windows.Forms.TextBox _txtBalance11;
		private System.Windows.Forms.TextBox _txtBalance8;
		private System.Windows.Forms.TextBox _txtBalance7;
		private System.Windows.Forms.TextBox _txtBalance6;
		private System.Windows.Forms.TextBox _txtBalance5;
		private System.Windows.Forms.TextBox _txtBalance4;
		private System.Windows.Forms.TextBox _txtBalance3;
		private System.Windows.Forms.TextBox _txtBalance2;
		private System.Windows.Forms.TextBox _txtBalance1;
		private System.Windows.Forms.Label _lblBalance;
		private System.Windows.Forms.TextBox _txtProposed;
		private System.Windows.Forms.TextBox _txtClose;
		private System.Windows.Forms.ErrorProvider _errorProvider;
		private System.Windows.Forms.TextBox _txtClose12;
		private System.Windows.Forms.TextBox _txtClose9;
		private System.Windows.Forms.TextBox _txtClose10;
		private System.Windows.Forms.TextBox _txtClose11;
		private System.Windows.Forms.TextBox _txtClose8;
		private System.Windows.Forms.TextBox _txtClose7;
		private System.Windows.Forms.TextBox _txtClose6;
		private System.Windows.Forms.TextBox _txtClose5;
		private System.Windows.Forms.TextBox _txtClose4;
		private System.Windows.Forms.TextBox _txtClose3;
		private System.Windows.Forms.TextBox _txtClose2;
		private System.Windows.Forms.TextBox _txtClose1;
		private System.Windows.Forms.Label _lblClosed;
		private System.Windows.Forms.TextBox _txtPaid12;
		private System.Windows.Forms.TextBox _txtPaid9;
		private System.Windows.Forms.TextBox _txtPaid10;
		private System.Windows.Forms.TextBox _txtPaid11;
		private System.Windows.Forms.TextBox _txtPaid8;
		private System.Windows.Forms.TextBox _txtPaid7;
		private System.Windows.Forms.TextBox _txtPaid6;
		private System.Windows.Forms.TextBox _txtPaid5;
		private System.Windows.Forms.TextBox _txtPaid4;
		private System.Windows.Forms.TextBox _txtPaid3;
		private System.Windows.Forms.TextBox _txtPaid2;
		private System.Windows.Forms.TextBox _txtPaid1;
		private System.Windows.Forms.Label _lblAwaitingAuthorisation;
		private System.Windows.Forms.Label _lblPeriod1;
		private System.Windows.Forms.Label _lblPeriod;
		private System.Windows.Forms.Label _lblPeriod12;
		private System.Windows.Forms.Label _lblPeriod11;
		private System.Windows.Forms.Label _lblPeriod10;
		private System.Windows.Forms.Label _lblPeriod9;
		private System.Windows.Forms.Label _lblPeriod8;
		private System.Windows.Forms.Label _lblPeriod7;
		private System.Windows.Forms.Label _lblPeriod6;
		private System.Windows.Forms.Label _lblPeriod5;
		private System.Windows.Forms.Label _lblPeriod4;
		private System.Windows.Forms.Label _lblPeriod3;
		private System.Windows.Forms.Label _lblPeriod2;
		private System.Windows.Forms.Label _lblCloseEntry;
		private System.Windows.Forms.Label _lblPaymentEntry;
		private System.Windows.Forms.GroupBox _grpRebate;
		private System.Windows.Forms.LinkLabel _lnkAccrual;
		private System.Windows.Forms.TextBox _txtProposedTotal;
		private System.Windows.Forms.Label _lblYearToDate;
		private System.Windows.Forms.TextBox _txtPaidTotal;
		private System.Windows.Forms.TextBox _txtBalanceTotal;
		private System.Windows.Forms.TextBox _txtClosedTotal;
		private System.Windows.Forms.TextBox _txtCloseTotal;
		private System.Windows.Forms.TextBox _txtAccrualTotal;
		private System.Windows.Forms.TextBox _txtPendingTotal;
		private System.Windows.Forms.TextBox _txtPending12;
		private System.Windows.Forms.TextBox _txtPending9;
		private System.Windows.Forms.TextBox _txtPending10;
		private System.Windows.Forms.TextBox _txtPending11;
		private System.Windows.Forms.TextBox _txtPending8;
		private System.Windows.Forms.TextBox _txtPending7;
		private System.Windows.Forms.Label _lblPaid;
		private System.Windows.Forms.TextBox _txtPending6;
		private System.Windows.Forms.TextBox _txtPending1;
		private System.Windows.Forms.TextBox _txtPending5;
		private System.Windows.Forms.TextBox _txtPending2;
		private System.Windows.Forms.TextBox _txtPending4;
		private System.Windows.Forms.TextBox _txtPending3;
	}
}

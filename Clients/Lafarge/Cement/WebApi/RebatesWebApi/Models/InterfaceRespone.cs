﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Configuration;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace RebatesWebApi.Models
{
    public class InterfaceResponse
    {

        public InterfaceResponse()
        {
            
        }

        public InterfaceResponse(InterfaceStatus status,
            int? statusCode,
            DateTime timeStamp,
            int errorCount,
            List<ItemLineResponse> itemLineResponses,
            InterfaceRejectionType interfaceRejectionType)
        {
            Status = status;
            StatusCode = statusCode;
            TimeStamp = timeStamp;
            ErrorCount = errorCount;
            ItemLineResponses = itemLineResponses;
            InterfaceRejectionType = interfaceRejectionType;
        }

        public InterfaceStatus Status { get; set; }

        public int? StatusCode { get; set; }

        public DateTime TimeStamp { get; set; }

        public int ErrorCount { get; set; }

        public List<ItemLineResponse> ItemLineResponses { get; set; }

        public InterfaceRejectionType InterfaceRejectionType { get; set; }

    }

    public class ItemLineResponse
    {

        public ItemLineResponse()
        {
            
        }

        public string Id { get; set; }

        public int ErrorCount { get; set; }


        public List<Error> Errors{ get; set; }

    }

    public class Error
    {

        public Error()
        {
            
        }
        public Error(string errorMessage)
        {
            ErrorMessage = errorMessage;
        }
        public string ErrorMessage { get; set; }


        
    }

    public enum InterfaceRejectionType
    {
        Customer,
        InvoiceLine,
        Product,
        Plant,
        None,
        InternalServerError

    }


    public enum InterfaceStatus
    {
        Unknown,
        Failed,
        Passed
    }
}

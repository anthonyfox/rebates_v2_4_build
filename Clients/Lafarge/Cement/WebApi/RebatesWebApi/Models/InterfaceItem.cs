﻿using System.ComponentModel.DataAnnotations;

namespace RebatesWebApi.Models
{
    public class InterfaceItem
    {
        /// <summary>
        /// Relates to Product.JdeCode where DivisionId = (select first Id from Division)
        /// 
        /// Insert / Update Product record as required
        /// </summary>
        [Required]
        [StringLength(20)]
        public string ErpCode { get; set; }

        /// <summary>
        /// Relates to Product.Name (for record identified via JdeCode)
        /// </summary>
        [Required]
        [StringLength(40)]
        public string Name { get; set; }

        /// <summary>
        /// Releates to Product.AttributeId1, lookup to Attribute table where Level = 1 and DivisionId = (select first Id from Division)
        /// 
        /// Insert / Update Attribute record as required
        /// </summary>
        [StringLength(22)]
        public string AttributeJdeCode1 { get; set; }
        [StringLength(40)]
        public string AttributeName1 { get; set; }

        /// <summary>
        /// Releates to Product.AttributeId2, lookup to Attribute table where Level = 2 and DivisionId = (select first Id from Division)
        /// 
        /// Insert / Update Attribute record as required
        /// </summary>
        [StringLength(22)]
        public string AttributeJdeCode2 { get; set; }
        [StringLength(40)]
        public string AttributeName2 { get; set; }

        /// <summary>
        /// Releates to Product.AttributeId3, lookup to Attribute table where Level = 3 and DivisionId = (select first Id from Division)
        /// 
        /// Insert / Update Attribute record as required
        /// </summary>
        [StringLength(22)]
        public string AttributeJdeCode3 { get; set; }
        [StringLength(40)]
        public string AttributeName3 { get; set; }

        /// <summary>
        /// Releates to Product.AttributeId4, lookup to Attribute table where Level = 4 and DivisionId = (select first Id from Division)
        /// 
        /// Insert / Update Attribute record as required
        /// </summary>
        [StringLength(22)]
        public string AttributeJdeCode4 { get; set; }
        [StringLength(40)]
        public string AttributeName4 { get; set; }

        /// <summary>
        /// Releates to Product.IsPurchased
        /// </summary>
        public bool IsPurchased { get; set; }
        
    }
}
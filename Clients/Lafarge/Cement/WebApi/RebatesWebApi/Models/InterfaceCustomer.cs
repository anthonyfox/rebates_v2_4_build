﻿using System.ComponentModel.DataAnnotations;

namespace RebatesWebApi.Models
{
	/// <summary>
	/// Use the following details to Insert / Update records in the following tables:
	///
	/// Rebates.Customer
	/// Rebates.Address
	/// Rebates.CustomerGroup
	/// Rebates.PricingSubGroup
	/// </summary>
    public class InterfaceCustomer
    {
		/// <summary>
		/// Relates to Customer.JdeCode where DivisionId = (select first Id from Division)
		///
		/// Insert / Update Customer record as required
		/// </summary>
		[Required]
        [StringLength(20)]
        public string ErpCode { get; set; }

		/// <summary>
		/// Relates to Customer.Name (for record identified via JdeCode)
		/// </summary>
		[Required]
		[StringLength(60)]
		public string Name { get; set; }

		/// <summary>
		/// Relates to Customer.SearchType (for record identified via JdeCode)
		///
		/// C   = Sold To
		/// XC  = Sold To (Deleted
		/// CS  = Ship To
		/// XCS = Ship To (Deleted)
		/// </summary>
		[Required]
		[StringLength(3)]
		public string SearchType { get; set; }

		/// <summary>
		/// Relates to Address.Address1 (for record identified via JdeCode)
		/// </summary>
		[Required]
		[StringLength(60)]
		public string Address1 { get; set; }

		/// <summary>
		/// Relates to Address.Address2 (for record identified via JdeCode)
		/// </summary>
		[StringLength(60)]
		public string Address2 { get; set; }

		/// <summary>
		/// Relates to Address.Address3 (for record identified via JdeCode)
		/// </summary>
		[StringLength(60)]
		public string Address3 { get; set; }

		/// <summary>
		/// Relates to Address.Address4 (for record identified via JdeCode)
		/// </summary>
		[StringLength(60)]
		public string Address4 { get; set; }

		/// <summary>
		/// Relates to Address.City (for record identified via JdeCode)
		/// </summary>
		[StringLength(40)]
		public string City { get; set; }

		/// <summary>
		/// Relates to Address.County (for record identified via JdeCode)
		/// </summary>
		[StringLength(40)]
		public string County { get; set; }

		/// <summary>
		/// Relates to Address.PostCode (for record identified via JdeCode)
		/// </summary>
		[StringLength(15)]
		public string PostCode { get; set; }

		/// <summary>
		/// Relates to Address.CountryCode (for record identified via JdeCode)
		/// </summary>
		[StringLength(3)]
		public string CountryCode { get; set; }


		/// <summary>
		/// Relates to CustomerGroup.JdeCode
		/// </summary>
		[StringLength(20)]
		public string CustomerGroupErpCode { get; set; }

		/// <summary>
		/// Relates to CustomerGroup.Name
		/// </summary>
		[StringLength(50)]
		public string CustomerGroupName { get; set; }


		/// <summary>
		/// Relates to PricingSubGroup.JdeCode
		/// </summary>
		[StringLength(3)]
		public string PricingSubGroupErpCode { get; set; }

		/// <summary>
		/// Relates to PricingSubGroup.Name
		/// </summary>
		[StringLength(40)]
		public string PricingSubGroupName { get; set; }


		/// <summary>
		/// Relates to Customer.JdeCode (where SearchType in ( 'C', 'XC' )
		/// </summary>
		[StringLength(15)]
		public string SoldToCode { get; set; }


	}
}

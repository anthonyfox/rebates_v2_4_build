﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace RebatesWebApi.Models
{
    
    public class InterfacePlant
    {
        /// <summary>
		/// Relates to Site.JdeCode where DivisionId = (select first Id from Division)
		/// 
		/// Insert / Update Product record as required
		/// </summary>
		[Required]
		[StringLength(10)]
        [DataMember(Name = "ErpCode")]
		public string ErpCode { get; set; }

		/// <summary>
		/// Relates to Site.Name (for record identified via ErpCode)
		/// </summary>
		[Required]
		[StringLength(30)]
		public string Name { get; set; }

		/// <summary>
		/// Relates to Address.Address1 (for record identified via ErpCode - Ignore non-numeric characters)
		/// </summary>
		[Required]
		[StringLength(60)]
		public string Address1 { get; set; }

		/// <summary>
		/// Relates to Address.Address2 (for record identified via ErpCode - Ignore non-numeric characters)
		/// </summary>
		[StringLength(60)]
		public string Address2 { get; set; }

		/// <summary>
		/// Relates to Address.Address3 (for record identified via ErpCode - Ignore non-numeric characters)
		/// </summary>
		[StringLength(60)]
		public string Address3 { get; set; }

		/// <summary>
		/// Relates to Address.Address4 (for record identified via ErpCode - Ignore non-numeric characters)
		/// </summary>
		[StringLength(60)]
		public string Address4 { get; set; }

		/// <summary>
		/// Relates to Address.City (for record identified via ErpCode - Ignore non-numeric characters)
		/// </summary>
		[StringLength(40)]
		public string City { get; set; }

		/// <summary>
		/// Relates to Address.County (for record identified via ErpCode - Ignore non-numeric characters)
		/// </summary>
		[StringLength(40)]
		public string County { get; set; }

		/// <summary>
		/// Relates to Address.PostCode (for record identified via ErpCode - Ignore non-numeric characters)
		/// </summary>
		[StringLength(15)]
		public string PostCode { get; set; }

		/// <summary>
		/// Relates to Address.CountryCode (for record identified via ErpCode - Ignore non-numeric characters)
		/// </summary>
		[StringLength(3)]
		public string CountryCode { get; set; }

		/// <summary>
		/// Releates to Site.JdeCodeRebate
		/// </summary>
		[StringLength(10)]
		public string RebateErpCode { get; set; }

        
	}
}

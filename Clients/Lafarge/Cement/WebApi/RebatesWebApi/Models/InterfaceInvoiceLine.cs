﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace RebatesWebApi.Models
{
    /// <summary>
    /// Use the following details to Insert records into the table Rebates.InvoiceLine
    /// </summary>
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    public class InterfaceInvoiceLine
    {
        /// <summary>
        /// Relates to InvoiceLine.Company
        /// </summary>
        [Required]
        [StringLength(10)]
        public string Company { get; set; }

        /// <summary>
        /// Relates to InvoiceLine.DocumentNumber
        /// </summary>
        [Required]
        [StringLength(15)]
        public string DocumentNumber { get; set; }

        /// <summary>
        /// Relates to InvoiceLine.DocumentType
        /// </summary>
        [Required]
        [StringLength(5)]
        public string DocumentType { get; set; }

        /// <summary>
        /// Relates to InvoiceLine.LineNumber
        /// </summary>
        [Required]
        [StringLength(5)]
        public string LineNumber { get; set; }

        /// <summary>
        /// Relates to InvoiceLine.SaleDate
        /// </summary>
        [Required]
        public DateTime SaleDate { get; set; }

        /// <summary>
        /// Relates to InvoiceLine.Quantity
        /// </summary>
        [Required]
        public Decimal Quantity { get; set; }

        /// <summary>
        /// Relates to InvoiceLine.Amount
        /// </summary>
        [Required]
        public Decimal Amount { get; set; }

        /// <summary>
        /// Relates to InvoiceLine.DiscountAmount
        /// </summary>
        [Required]
        public Decimal DiscountAmount { get; set; }

        /// <summary>
        /// Relates to Product.JdeCode, use to retrieve InvoiceLine.ProductId
        /// </summary>
        [Required]
        [StringLength(20)]
        public string ProductErpCode { get; set; }

        /// <summary>
        /// Relates to Site.JdeCode, use to retrieve InvoiceLine.SiteId
        /// </summary>
        [Required]
        [StringLength(10)]
        public string PlantErpCode { get; set; }

        /// <summary>
        /// Relates to Customer.JdeCode (where SearchType in ( 'C', 'XC' )), use to retrieve InvoiceLine.SoldToId
        /// </summary>
        [Required]
        [StringLength(20)]
        public string SoldToErpCode { get; set; }

        /// <summary>
        /// Relates to Customer.JdeCode (where SearchType in ( 'CS', 'XCS' )), use to retrieve InvoiceLine.ShipToId
        /// </summary>
        [Required]
        [StringLength(20)]
        public string ShipToErpCode { get; set; }

        /// <summary>
        /// Relates to InvoiceLine.DeliveryTypeEnum
        ///
        /// 1 = Delivery
        /// 2 = Collect
        /// </summary>
        [Required]
        [Range(1, 2)]
        public int DeliveryType { get; set; }

        /// <summary>
        /// Relates to InvoiceLine.IsApplicableQuantity
        ///
        /// If True, then invoice will be used against Sales Quantity based rebate calculations
        /// </summary>
        [Required]
        public bool IsApplicableQuantity { get; set; }

        /// <summary>
        /// Relates to InvoiceLine.IsApplicableValue
        ///
        /// If True, then invoice will be used against Invoice Value based rebate calculations
        /// </summary>
        [Required]
        public bool IsApplicableValue { get; set; }

        /// <summary>
        /// Relates to DeliveryWindow.JdeCocde, use to retrieve InvoiceLine.DeliveryWindowId
        /// </summary>
        //[StringLength(40)]
        //public string DeliveryWindowErpCode { get; set; }

        /// <summary>
        /// Relates to LeadTime.JdeCocde, use to retrieve InvoiceLine.LeadTimeId
        /// </summary>
        //[StringLength(40)]
        //public string LeadTimeErpCode { get; set; }

        /// <summary>
        /// Relates to InvoiceLine.PackingTypeEnum
        ///
        /// 0 = Unknown
        /// 1 = Pack
        /// 2 = Bulk
        /// </summary>
        [Range(0, 2)]
        public short? PackingType { get; set; }

        /// <summary>
        /// Relates to InvoiceLine.OrderNumber
        /// </summary>
        [Required]
        [StringLength(15)]
        public string OrderNumber { get; set; }

        /// <summary>
        /// Relates to InvoiceLine.OrderType
        /// </summary>
        [Required]
        [StringLength(5)]
        public string OrderType { get; set; }

        /// <summary>
        /// Relates to InvoiceLine.OrderLineNumber
        /// </summary>
        [Required]
        [StringLength(10)]
        public string OrderLineNumber { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Permissions;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using Models;
using RebatesWebApi.Models;
using RebatesWebApi.Utilities;

namespace RebatesWebApi.Controllers
{
    [EnableCors("*", "*", "Post")]
    public class InvoiceLineTwoController : ApiController
    {
        [ResponseType(typeof(InterfaceResponse))]
        public IHttpActionResult Post([FromBody] List<InterfaceInvoiceLine> invoiceLines)
        {
            bool successfulInvoiceLines = true;

            //List for all successful invoice lines
            var invoiceLineSuccesses = new List<INVOICELINE>();

            InterfaceResponse interFaceResponse = new InterfaceResponse(
               status: InterfaceStatus.Unknown,
               statusCode: 0,
               timeStamp: DateTime.Now,
               errorCount: 0,
               itemLineResponses: new List<ItemLineResponse>(),
               interfaceRejectionType: InterfaceRejectionType.InvoiceLine);


            try
            {
                //if we are passed a null object
                if (invoiceLines == null)
                {
                    //return httpsstatus code no content
                    return Content(HttpStatusCode.NoContent, "No content supplied");
                }

                using (var context = new RebateEntities())
                {
                    // Get the delivery windows list
                    var deliverWindows = (from p in context.DELIVERYWINDOWs select p).ToList();

                    //Get a list of lead times
                    var leadTimes = (from p in context.LEADTIMEs select p).ToList();

                    //loop round the invoice items and check for validation errors
                    foreach (var interfaceInvoiceLine in invoiceLines)
                    {
                        var itemLineResponse = new ItemLineResponse();

                        //new up a ItemLineResponse error list
                        itemLineResponse.Errors = new List<Error>();

                        // Check if already exists
                        var existingInvoice = context.INVOICELINEs.FirstOrDefault(p => p.COMPANY == interfaceInvoiceLine.Company
                                                    && p.DOCUMENTNUMBER == interfaceInvoiceLine.DocumentNumber
                                                    && p.DOCUMENTTYPE == interfaceInvoiceLine.DocumentType
                                                    && p.LINENUMBER == interfaceInvoiceLine.LineNumber);

                        PRODUCT product = context.PRODUCTs.FirstOrDefault(p => p.JDECODE == interfaceInvoiceLine.ProductErpCode && p.ACTIVE);
                        CUSTOMER soldTo = context.CUSTOMERs.FirstOrDefault(c => c.JDECODE == interfaceInvoiceLine.SoldToErpCode && (c.SEARCHTYPE == "C" || c.SEARCHTYPE == "XC") && c.ACTIVE);
                        CUSTOMER shipTo = context.CUSTOMERs.FirstOrDefault(c => c.JDECODE == interfaceInvoiceLine.ShipToErpCode && (c.SEARCHTYPE == "CS" || c.SEARCHTYPE == "XCS") && c.ACTIVE);
                        SITE site = context.SITEs.FirstOrDefault(s => s.JDECODE == interfaceInvoiceLine.PlantErpCode && s.ACTIVE);

                        //Check for any model state validation errors for the invoice line
                        List<ValidationResult> errors = new List<ValidationResult>();
                        Validator.TryValidateObject(interfaceInvoiceLine, new ValidationContext(interfaceInvoiceLine, null, null), errors, true);

                        if (errors.Any() || existingInvoice != null || product == null || soldTo == null || shipTo == null || site == null)
                        {
                            if (errors.Any())
                            {
                                foreach (var validationResult in errors)
                                {
                                    //add the error message to the itemLineResponse
                                    itemLineResponse.Errors.Add(new Error(validationResult.ErrorMessage));

                                    itemLineResponse.ErrorCount += 1;
                                    interFaceResponse.ErrorCount += 1;
                                }
                            }
                            else
                            {
                                if (existingInvoice != null)
                                {
                                    itemLineResponse.Errors.Add(new Error("Invoice Line Already In DB"));
                                    itemLineResponse.Id =
                                        $"{interfaceInvoiceLine.DocumentType.Trim()}/{interfaceInvoiceLine.DocumentNumber.Trim()}/{interfaceInvoiceLine.LineNumber.Trim()}";
                                    itemLineResponse.ErrorCount += 1;

                                    interFaceResponse.ErrorCount += 1;
                                }
                                else
                                {
                                    if (product == null)
                                    {
                                        itemLineResponse.Errors.Add(new Error("Bad or missing Product details"));
                                        itemLineResponse.Id = $"{interfaceInvoiceLine.DocumentType.Trim()}/{interfaceInvoiceLine.DocumentNumber.Trim()}/{interfaceInvoiceLine.LineNumber.Trim()}";
                                        itemLineResponse.ErrorCount += 1;

                                        interFaceResponse.ErrorCount += 1;
                                    }

                                    if (soldTo == null)
                                    {
                                        itemLineResponse.Errors.Add(new Error("Bad or missing SoldTo details"));
                                        itemLineResponse.Id = $"{interfaceInvoiceLine.DocumentType.Trim()}/{interfaceInvoiceLine.DocumentNumber.Trim()}/{interfaceInvoiceLine.LineNumber.Trim()}";
                                        itemLineResponse.ErrorCount += 1;

                                        interFaceResponse.ErrorCount += 1;
                                    }

                                    if (shipTo == null)
                                    {
                                        itemLineResponse.Errors.Add(new Error("Bad or missing ShipTo details"));
                                        itemLineResponse.Id = $"{interfaceInvoiceLine.DocumentType.Trim()}/{interfaceInvoiceLine.DocumentNumber.Trim()}/{interfaceInvoiceLine.LineNumber.Trim()}";
                                        itemLineResponse.ErrorCount += 1;

                                        interFaceResponse.ErrorCount += 1;
                                    }
                                    if (site == null)
                                    {
                                        itemLineResponse.Errors.Add(new Error("Bad or missing Plant details"));
                                        itemLineResponse.Id = $"{interfaceInvoiceLine.DocumentType.Trim()}/{interfaceInvoiceLine.DocumentNumber.Trim()}/{interfaceInvoiceLine.LineNumber.Trim()}";
                                        itemLineResponse.ErrorCount += 1;

                                        interFaceResponse.ErrorCount += 1;
                                    }
                                }
                            }

                            //Add the Status Code to the interfaceResponse and set the failed message
                            interFaceResponse.StatusCode = 400;
                            interFaceResponse.Status = InterfaceStatus.Failed;

                            successfulInvoiceLines = false;

                            interFaceResponse.ItemLineResponses.Add(itemLineResponse);
                        }
                        else
                        {
                            //invoice line does not exists
                            INVOICELINE proccessedInvoiceLine;

                            //Process Invoice Line
                            proccessedInvoiceLine = processInvoiceLine(
                                interfaceInvoiceLine: interfaceInvoiceLine,
                                productId: product.ID,
                                soldToId: soldTo.ID,
                                shipToId: shipTo.ID,
                                siteId: site.ID,
                                context: context,
                                deliverywindows: deliverWindows,
                                leadTimes: leadTimes,
                                invoiceLineSuccesses: invoiceLineSuccesses);
                        }
                    }

                    if (successfulInvoiceLines == false)
                    {
                        // We return created as SAP struggles to cope with interface statuses bad request
                        return Content(HttpStatusCode.Created, interFaceResponse);
                    }

                    if (invoiceLineSuccesses.Any())
                    {
                        using (var dbconnection = context.Database.Connection)
                        {
                            dbconnection.Open();

                            var dbTransaction = dbconnection.BeginTransaction();
                            try
                            {
                                var sqlBulkCopy = EfUtilities.CreateSqlBulkCopyObject(dbContext: context,
                                    sqlConnection: (SqlConnection)dbconnection,
                                    sqlTransaction: (SqlTransaction)dbTransaction,
                                    modelType: invoiceLineSuccesses.First().GetType(),
                                    batchSize: 100,
                                    bulkCopyTimeout: 60,
                                    options: SqlBulkCopyOptions.Default);

                                sqlBulkCopy.WriteToServer(EfUtilities.EnumerableToDataTable(
                                    items: invoiceLineSuccesses,
                                    type: invoiceLineSuccesses.First().GetType()));

                                dbTransaction.Commit();
                                dbconnection.Close();
                            }
                            catch (Exception ex)
                            {
                                // Rollback all changes to the db
                                dbTransaction.Rollback();

                                if (dbconnection.State == ConnectionState.Open)
                                {
                                    dbconnection.Close();
                                }

                                interFaceResponse.InterfaceRejectionType = InterfaceRejectionType.InternalServerError;
                                interFaceResponse.StatusCode = 500;
                                interFaceResponse.Status = InterfaceStatus.Failed;
                                interFaceResponse.ErrorCount += 1;

                                ItemLineResponse itemLineResponse = new ItemLineResponse();

                                itemLineResponse.Errors = new List<Error>
                                    {
                                        new Error(ex.InnerException.Message.ToString())
                                    };

                                interFaceResponse.ItemLineResponses.Add(itemLineResponse);

                                // Return the error
                                return Content(HttpStatusCode.InternalServerError, interFaceResponse);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                interFaceResponse.InterfaceRejectionType = InterfaceRejectionType.InternalServerError;
                interFaceResponse.StatusCode = 500;
                interFaceResponse.Status = InterfaceStatus.Failed;

                return Content(HttpStatusCode.InternalServerError, interFaceResponse);
            }

            //Add the successful criteria to the interface response
            interFaceResponse.Status = InterfaceStatus.Passed;
            interFaceResponse.StatusCode = 201;

            return Content(HttpStatusCode.Created, interFaceResponse);
        }

        private INVOICELINE processInvoiceLine(
            InterfaceInvoiceLine interfaceInvoiceLine,
            int productId, int soldToId, int shipToId,
            int siteId,
            RebateEntities context,
            List<DELIVERYWINDOW> deliverywindows,
            List<LEADTIME> leadTimes,
            List<INVOICELINE> invoiceLineSuccesses)
        {

            int? deliveryWindowId = null;
            int? leadTimeId = null;

            var newInvoiceLine = NewInvoiceLine(interfaceInvoiceLine, productId, soldToId, shipToId, siteId, deliveryWindowId, leadTimeId);

            //Add the invoice line to
            invoiceLineSuccesses.Add(newInvoiceLine);

            return newInvoiceLine;
        }


        private static INVOICELINE NewInvoiceLine(InterfaceInvoiceLine interfaceInvoiceLine,
            int productId,
            int soldToId,
            int shipToId,
            int siteId,
            int? deliveryWindowId,
            int? leadTimeId)
        {
            var newInvoiceLine = new INVOICELINE
            {
                DIVISIONID = 1,
                COMPANY = interfaceInvoiceLine.Company,
                DOCUMENTNUMBER = interfaceInvoiceLine.DocumentNumber,
                DOCUMENTTYPE = interfaceInvoiceLine.DocumentType,
                SALEDATE = interfaceInvoiceLine.SaleDate,
                QUANTITY = interfaceInvoiceLine.Quantity,
                AMOUNT = interfaceInvoiceLine.Amount,
                DISCOUNTAMOUNT = interfaceInvoiceLine.DiscountAmount,
                DELIVERYTYPEENUM = interfaceInvoiceLine.DeliveryType,
                ISAPPLICABLEQUANTITY = interfaceInvoiceLine.IsApplicableQuantity,
                DELIVERYWINDOWID = deliveryWindowId, // Get value
                PRODUCTID = productId,
                SOLDTOID = soldToId,
                SHIPTOID = shipToId,
                SITEID = siteId,
                LEADTIMEID = leadTimeId, //Get value
                PACKINGTYPEENUM = interfaceInvoiceLine.PackingType.GetValueOrDefault(0),
                INTERFACETIME = DateTime.Now,
                ORDERNUMBER = interfaceInvoiceLine.OrderNumber,
                ORDERLINENUMBER = interfaceInvoiceLine.OrderLineNumber,
                ORDERDOCTYPE = interfaceInvoiceLine.OrderType,
                LINENUMBER = interfaceInvoiceLine.LineNumber,



            };
            return newInvoiceLine;
        }
    }
}

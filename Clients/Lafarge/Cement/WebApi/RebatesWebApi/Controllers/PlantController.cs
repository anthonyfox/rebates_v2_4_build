﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using Models;
using RebatesWebApi.Models;

namespace RebatesWebApi.Controllers
{
    [EnableCors("*", "*", "Post")]
    public class PlantController : ApiController
    {
        /// <summary>
        /// Interface For posting plants does not require Https
        /// </summary>
        /// <param name="plants"></param>
        /// <returns></returns>
        [ResponseType(typeof(InterfaceResponse))]
        //[RequireHttps]
        public IHttpActionResult Post([FromBody] List<InterfacePlant> plants)
        {

            var interFaceResponse = new InterfaceResponse(
                status: InterfaceStatus.Unknown,
                statusCode: 0,
                timeStamp: DateTime.Now,
                errorCount: 0,
                itemLineResponses: new List<ItemLineResponse>(),
                interfaceRejectionType: InterfaceRejectionType.Plant);

            try
            {
                //If content body is null
                if (plants == null)
                {
                    //return HttpStatusCode code no content
                    return Content(HttpStatusCode.NoContent, "No content supplied");
                }

                using (var context = new RebateEntities())
                {
                    // get a list of site/ address
                    var itemLineResponse = new ItemLineResponse();

                    //loop round each plant and process
                    foreach (var interfacePlant in plants)
                    {

                        //new up a ItemLineResponse error list
                        itemLineResponse.Errors = new List<Error>();

                        //Check for any validation errors against the model attributes per invoice line
                        List<ValidationResult> errors = new List<ValidationResult>();
                        Validator.TryValidateObject(interfacePlant, new ValidationContext(interfacePlant, null, null), errors, true);

                        //If the plant has a model state error
                        if (errors.Any())
                        {

                            //Add the Status Code to the interfaceResponse and set the failed message
                            interFaceResponse.StatusCode = 400;
                            interFaceResponse.Status = InterfaceStatus.Failed;

                            foreach (var validationResult in errors)
                            {
                                //add the error message to the itemLineResponse
                                itemLineResponse.Errors.Add(
                                   new Error(validationResult.ErrorMessage));

                                itemLineResponse.ErrorCount += 1;
                                interFaceResponse.ErrorCount += 1;

                            }

                            //Add the ID
                            itemLineResponse.Id = interfacePlant.ErpCode;

                            // add ItemLine response to the interface response
                            interFaceResponse.ItemLineResponses.Add(itemLineResponse);


                        }
                        else
                        {
                            //No model state errors process the plant
                            ProcessPlant(interfacePlant: interfacePlant, context: context);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Uncaught exception return internal server error + and exception message
                return Content(HttpStatusCode.InternalServerError, ex.Message);
            }

            if (interFaceResponse.ItemLineResponses.Any())
            {

                //We have errors but we return status ok with a list of error objects (SAP does not handle 400 response)
                // We return created as SAP struggles to cope with interface statuses
                return Content(HttpStatusCode.Created, interFaceResponse);
            }
            else
            {
                //Add the successful criteria to the interface response
                interFaceResponse.Status = InterfaceStatus.Passed;
                interFaceResponse.StatusCode = 201;

                //no exceptions return a 201 and the interface response
                return Content(HttpStatusCode.Created, interFaceResponse);
            }

        }

        private void ProcessPlant(InterfacePlant interfacePlant, RebateEntities context)
        {
            // Check to see if the site exists
            SITE exisitingSite = context.SITEs.FirstOrDefault(p => p.JDECODE == interfacePlant.ErpCode.ToString());

            //if the site does not exist create a new site
            if (exisitingSite == null)
            {
                var newSite = new SITE
                {
                    DIVISIONID = 1,
                    JDECODE = interfacePlant.ErpCode,
                    NAME = interfacePlant.Name,
                    JDECODEREBATE = null,
                    ACTIVE = true,
                    ADDRESS = new ADDRESS
                    {
                        JDECODE = interfacePlant.ErpCode,
                        ADDRESS1 = interfacePlant.Address1,
                        ADDRESS2 = interfacePlant.Address2 ?? "",
                        ADDRESS3 = interfacePlant.Address3 ?? "",
                        ADDRESS4 = interfacePlant.Address4 ?? "",
                        CITY = interfacePlant.City ?? "",
                        COUNTY = interfacePlant.County ?? "",
                        POSTCODE = interfacePlant.PostCode ?? "",
                        COUNTRYCODE = interfacePlant.CountryCode ?? ""

                    }
                };


                //Add new Plant to the DB
                context.SITEs.Add(newSite);
                context.SaveChanges();

            }
            else
            {
                //Update Site and Address
                SITE siteToUpdate = context.SITEs.FirstOrDefault(p => p.JDECODE == interfacePlant.ErpCode.ToString());

                if (siteToUpdate != null)
                {
                    siteToUpdate.DIVISIONID = 1;
                    siteToUpdate.JDECODE = interfacePlant.ErpCode.ToString();
                    siteToUpdate.NAME = interfacePlant.Name;
                    siteToUpdate.JDECODEREBATE = null;
                    siteToUpdate.ACTIVE = true;
                    //siteToUpdate.ADDRESS.JDECODE = intErpCode;
                    siteToUpdate.ADDRESS.ADDRESS1 = interfacePlant.Address1;
                    siteToUpdate.ADDRESS.ADDRESS2 = interfacePlant.Address2;
                    siteToUpdate.ADDRESS.ADDRESS3 = interfacePlant.Address3;
                    siteToUpdate.ADDRESS.ADDRESS4 = interfacePlant.Address4;
                    siteToUpdate.ADDRESS.CITY = interfacePlant.City ?? "";
                    siteToUpdate.ADDRESS.COUNTY = interfacePlant.County ?? "";
                    siteToUpdate.ADDRESS.POSTCODE = interfacePlant.PostCode ?? "";
                    siteToUpdate.ADDRESS.COUNTRYCODE = interfacePlant.CountryCode ?? "";

                    //Save updated plant to the DB
                    context.SaveChanges();
                }

            }
        }
    }
}

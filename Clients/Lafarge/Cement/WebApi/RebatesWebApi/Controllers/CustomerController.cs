﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using Models;
using RebatesWebApi.Models;
using System.Linq;


namespace RebatesWebApi.Controllers
{
    ///
    [EnableCors("*", "*", "Post")]
    public class CustomerController : ApiController
    {
        /// <summary>
        /// Interface for posting customers does not require Https
        /// </summary>
        /// <param name="customers"></param>
        /// <returns></returns>
        [ResponseType(typeof (InterfaceResponse))]
        //[RequireHttps]
        public IHttpActionResult Post([FromBody] List<InterfaceCustomer> customers)
        {
            //New up the response object
            var interFaceResponse = new InterfaceResponse(
                status: InterfaceStatus.Unknown,
                statusCode: 0,
                timeStamp: DateTime.Now,
                errorCount: 0,
                itemLineResponses: new List<ItemLineResponse>(),
                interfaceRejectionType: InterfaceRejectionType.Customer);

            CUSTOMER soldTo = null;
            CUSTOMERGROUP customerGroup = null;
            PRICINGSUBGROUP pricingsubgroup = null;

            try
            {
                if (customers == null)
                {
                    return Content(HttpStatusCode.NoContent, "No Content Supplied");
                }

                using (var context = new RebateEntities())
                {

                    foreach (var interfaceCustomer in customers)
                    {
                        var itemLineResponse = new ItemLineResponse();

                        //new up a ItemLineResponse error list
                        itemLineResponse.Errors = new List<Error>();

                        //Check Id's
                        if (!string.IsNullOrWhiteSpace(interfaceCustomer.SoldToCode))
                        {
                            soldTo = context.CUSTOMERs.FirstOrDefault(p => p.JDECODE == interfaceCustomer.SoldToCode);

                            if (soldTo == null)
                            {
                                interFaceResponse.StatusCode = 400;
                                interFaceResponse.Status = InterfaceStatus.Failed;
                                itemLineResponse.Errors.Add(new Error("SoldTo Not Found"));
                                itemLineResponse.ErrorCount += 1;
                            }
                            else if (!(soldTo.SEARCHTYPE == "C" || soldTo.SEARCHTYPE == "XC"))
                            {
                                interFaceResponse.StatusCode = 400;
                                interFaceResponse.Status = InterfaceStatus.Failed;
                                itemLineResponse.Errors.Add(new Error("Specified SoldTo customer is not a SoldTo account"));
                                itemLineResponse.ErrorCount += 1;
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(interfaceCustomer.CustomerGroupErpCode))
                        {
                            customerGroup =
                                context.CUSTOMERGROUPs.FirstOrDefault(p => p.JDECODE == interfaceCustomer.CustomerGroupErpCode);

                            if (customerGroup == null)
                            {
                                interFaceResponse.StatusCode = 400;
                                interFaceResponse.Status = InterfaceStatus.Failed;
                                itemLineResponse.Errors.Add(new Error("Customer Group Not Found"));
                                itemLineResponse.ErrorCount += 1;
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(interfaceCustomer.PricingSubGroupErpCode))
                        {
                            pricingsubgroup =
                                context.PRICINGSUBGROUPs.FirstOrDefault(p => p.JDECODE == interfaceCustomer.PricingSubGroupErpCode);

                            if (pricingsubgroup == null)
                            {
                                interFaceResponse.StatusCode = 400;
                                interFaceResponse.Status = InterfaceStatus.Failed;
                                itemLineResponse.Errors.Add(new Error("Pricing Sub Group Not Found"));
                                itemLineResponse.ErrorCount += 1;
                            }
                        }

                        if (string.IsNullOrWhiteSpace(interfaceCustomer.SoldToCode) && (interfaceCustomer.SearchType == "CS" || interfaceCustomer.SearchType == "XCS"))
                        {
                            interFaceResponse.StatusCode = 400;
                            interFaceResponse.Status = InterfaceStatus.Failed;
                            itemLineResponse.Errors.Add(new Error("Ship To account must have a Sold To"));
                            itemLineResponse.ErrorCount += 1;
                        }

                        List<ValidationResult> errors = new List<ValidationResult>();
                        Validator.TryValidateObject(interfaceCustomer, new ValidationContext(interfaceCustomer, null, null), errors, true);

                        if (errors.Any())
                        {
                            //Add the Status Code to the interfaceResponse and set the failed message
                            interFaceResponse.StatusCode = 400;
                            interFaceResponse.Status = InterfaceStatus.Failed;

                            foreach (var validationResult in errors)
                            {
                                //add the error message to the itemLineResponse
                                itemLineResponse.Errors.Add(new Error(validationResult.ErrorMessage));

                                itemLineResponse.ErrorCount++;
                                itemLineResponse.Id = interfaceCustomer.ErpCode;
                            }


                            //Add the ItemLine response to the response object
                            interFaceResponse.ItemLineResponses.Add(itemLineResponse);
                        }
                        else
                        {
                            ProcessCustomer(interfaceCustomer: interfaceCustomer,
                                context: context,
                                customer: soldTo,
                                customerGroup: customerGroup,
                                pricingsubgroup: pricingsubgroup);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }

            if (interFaceResponse.ItemLineResponses.Any())
            {
                // We return created as SAP struggles to cope with interface statuses
                return Content(HttpStatusCode.Created, interFaceResponse);
            }

            //Add the successful criteria toe the interface response
            interFaceResponse.Status = InterfaceStatus.Passed;
            interFaceResponse.StatusCode = 201;

            //no exceptions return a 201 and the interface response
            return Content(HttpStatusCode.Created, interFaceResponse);
        }

        private void ProcessCustomer(InterfaceCustomer interfaceCustomer,
            RebateEntities context,
            CUSTOMER customer,
            CUSTOMERGROUP customerGroup,
            PRICINGSUBGROUP pricingsubgroup )
        {
            // Check to see if existing Customer
            // Might be two customers with the same JDE Code, but only one of them is active.
            // If that is the case then update the active one.
            CUSTOMER existingCustomer = context.CUSTOMERs.FirstOrDefault(p => p.JDECODE == interfaceCustomer.ErpCode && p.ACTIVE);

            if (existingCustomer == null)
            {
                existingCustomer = context.CUSTOMERs.FirstOrDefault(p => p.JDECODE == interfaceCustomer.ErpCode);
            }

            if (existingCustomer == null)
            {
                // add new customer
                var newCustomer = new CUSTOMER
                {
                    JDECODE = interfaceCustomer.ErpCode,
                    NAME = interfaceCustomer.Name,
                    ADDRESS = new ADDRESS
                    {
                        JDECODE = interfaceCustomer.ErpCode,
                        ADDRESS1 = interfaceCustomer.Address1,
                        ADDRESS2 = interfaceCustomer.Address2 ?? "",
                        ADDRESS3 = interfaceCustomer.Address3 ?? "",
                        ADDRESS4 = interfaceCustomer.Address4 ?? "",
                        CITY = interfaceCustomer.City ?? "",
                        COUNTY = interfaceCustomer.County ?? "",
                        POSTCODE = interfaceCustomer.PostCode ?? "",
                        COUNTRYCODE = interfaceCustomer.CountryCode ?? ""
                    },
                    INTERFACETIME = DateTime.Now,
                    SEARCHTYPE = interfaceCustomer.SearchType,
                    ACTIVE = true,
                    SOLDTOID = customer?.ID,
                    CUSTOMERGROUPID = customerGroup?.ID,
                    PRICINGSUBGROUPID =  pricingsubgroup?.ID

                };



                context.CUSTOMERs.Add(newCustomer);
                context.SaveChanges();
            }
            else
            {
                CUSTOMER customerToUpdate =
                    context.CUSTOMERs.FirstOrDefault(p => p.JDECODE == interfaceCustomer.ErpCode);

                if (customerToUpdate != null)
                {
                    customerToUpdate.JDECODE = interfaceCustomer.ErpCode;
                    customerToUpdate.NAME = interfaceCustomer.Name;
                    customerToUpdate.ADDRESS.JDECODE = interfaceCustomer.ErpCode;
                    customerToUpdate.ADDRESS.ADDRESS1 = interfaceCustomer.Address1;
                    customerToUpdate.ADDRESS.ADDRESS2 = interfaceCustomer.Address2 ?? "";
                    customerToUpdate.ADDRESS.ADDRESS3 = interfaceCustomer.Address3 ?? "";
                    customerToUpdate.ADDRESS.ADDRESS4 = interfaceCustomer.Address4 ?? "";
                    customerToUpdate.ADDRESS.CITY = interfaceCustomer.City ?? "";
                    customerToUpdate.ADDRESS.COUNTY = interfaceCustomer.County ?? "";
                    customerToUpdate.ADDRESS.POSTCODE = interfaceCustomer.PostCode ?? "";
                    customerToUpdate.ADDRESS.COUNTRYCODE = interfaceCustomer.CountryCode ?? "";
                    customerToUpdate.SOLDTOID = customer?.ID;
                    customerToUpdate.INTERFACETIME = DateTime.Now;
                    customerToUpdate.SEARCHTYPE = interfaceCustomer.SearchType;
                    customerToUpdate.ACTIVE = true;
                    customerToUpdate.CUSTOMERGROUPID = customerGroup?.ID;
                    customerToUpdate.PRICINGSUBGROUPID = pricingsubgroup?.ID;
                }
                context.SaveChanges();
            }
        }
    }
}

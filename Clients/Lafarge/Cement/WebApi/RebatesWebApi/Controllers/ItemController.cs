﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using Microsoft.Ajax.Utilities;
using Models;
using RebatesWebApi.Attribute;
using RebatesWebApi.Models;

namespace RebatesWebApi.Controllers
{
    [EnableCors("*", "*", "Post")]
    public class ItemController : ApiController
    {
        /// <summary>
        /// Interface for posting items does not require Https
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        [ResponseType(typeof(InterfaceResponse))]
        //[RequireHttps]
        public IHttpActionResult Post([FromBody] List<InterfaceItem> items)
        {
            //New up the response object
            var interFaceResponse = new InterfaceResponse(
                status: InterfaceStatus.Unknown,
                statusCode: 0,
                timeStamp: DateTime.Now,
                errorCount: 0,
                itemLineResponses: new List<ItemLineResponse>(),
                interfaceRejectionType: InterfaceRejectionType.Product);

            try
            {
                //if no items a re posted return a httpstatus of no content
                if (items == null)
                {
                    return Content(HttpStatusCode.NoContent, "No Content Supplied");
                }

                using (var context = new RebateEntities())
                {
                    var itemLineResponse = new ItemLineResponse();

                    //loop round each item
                    foreach (var interfaceItem in items)
                    {
                        //new up a ItemLineResponse error list
                        itemLineResponse.Errors = new List<Error>();

                        //Check for any validation errors against the model attributes per invoice line
                        List<ValidationResult> errors = new List<ValidationResult>();
                        Validator.TryValidateObject(interfaceItem, new ValidationContext(interfaceItem, null, null), errors, true);


                        //if we have any modelstate errors
                        if (errors.Any())
                        {

                            interFaceResponse.StatusCode = 400;
                            interFaceResponse.Status = InterfaceStatus.Failed;

                            foreach (var validationResult in errors)
                            {
                                //add the error message to the rejection object
                                itemLineResponse.Errors.Add(
                                   new Error(validationResult.ErrorMessage));

                                itemLineResponse.Id = interfaceItem.ErpCode;

                                itemLineResponse.ErrorCount += 1;
                                interFaceResponse.ErrorCount += 1;

                            }

                            //Add the ID
                            itemLineResponse.Id = interfaceItem.ErpCode;

                            // Add to rejection list
                            interFaceResponse.ItemLineResponses.Add(itemLineResponse);

                        }
                        else
                        {
                            //Add or update attributes from the item
                            processAttributes(item: interfaceItem, context: context);

                            // Process the item itself
                            processProducts(item: interfaceItem, context: context);

                        }
                    }

                }

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex.Message);
            }

            //If we have errors in the rejection list
            if (interFaceResponse.ItemLineResponses.Any())
            {
                // Return bad content
                // We return created as SAP struggles to cope with interface statuses
                return Content(HttpStatusCode.Created, interFaceResponse);

            }
            else
            {
                interFaceResponse.StatusCode = 201;
                interFaceResponse.Status = InterfaceStatus.Passed;

                //No errors return created status
                return Content(HttpStatusCode.Created, interFaceResponse);

            }
        }

        private void processAttributes(InterfaceItem item, RebateEntities context)
        {

            Type itemType = item.GetType();

            for (short levelNumber = 1; levelNumber <= 4; levelNumber++)
            {
                PropertyInfo jdeCodeInfo = itemType.GetProperty("AttributeJdeCode" + levelNumber);
                PropertyInfo jdeName = itemType.GetProperty("AttributeName" + levelNumber);

                string jdeCode = jdeCodeInfo.GetValue(item, null).ToString();
                string name = jdeName.GetValue(item, null).ToString();

                //Process each attribute 1- 4
                if (!jdeCode.IsNullOrWhiteSpace())
                {

                    ATTRIBUTE exisitingAttribute =
                        context.ATTRIBUTEs
                            .Where(p => p.LEVELNUMBER == levelNumber)
                            .FirstOrDefault(p => p.JDECODE == jdeCode);


                    //using (var context = new RebateEntities())
                    //{

                    if (exisitingAttribute == null)
                    {
                        // Does not exist Add to db
                        var newAttribute = new ATTRIBUTE
                        {
                            DIVISIONID = 1,
                            JDECODE = jdeCode,
                            NAME = name,
                            LEVELNUMBER = levelNumber,
                            INTERFACETIME = DateTime.Now
                        };
                        context.ATTRIBUTEs.Add(newAttribute);
                        context.SaveChanges();

                    }// Update name if changed
                    else if (exisitingAttribute.NAME != name)
                    {
                        exisitingAttribute.NAME = name;
                        //ATTRIBUTE attributeToUpdate = (from p in context.ATTRIBUTEs where  p.ID == exisitingAttribute.ID select new ATTRIBUTE {ID = p.ID, NAME = p.NAME}).FirstOrDefault();
                        //if (attributeToUpdate != null) attributeToUpdate.NAME = name;
                        context.SaveChanges();
                    }
                    //}

                }
            }
        }

        private void processProducts(InterfaceItem item, RebateEntities context)
        {
            // Search for an existing product in the collection
            PRODUCT existingProduct = context.PRODUCTs.SingleOrDefault(p => p.JDECODE == item.ErpCode);


            if (existingProduct == null)
            {
                // Does Not exit Add to the db
                var newProduct = new PRODUCT
                {
                    DIVISIONID = 1,
                    NAME = item.Name,
                    JDECODE = item.ErpCode,
                    ISPURCHASED = item.IsPurchased,
                    INTERFACETIME = DateTime.Now,
                    ACTIVE = true
                };

                if (!item.AttributeJdeCode1.IsNullOrWhiteSpace())
                {
                    newProduct.ATTRIBUTEID1 = (int?)
                        (context.ATTRIBUTEs.FirstOrDefault(
                            a => a.JDECODE == item.AttributeJdeCode1 && a.LEVELNUMBER == 1)?.ID);
                }

                if (!item.AttributeJdeCode2.IsNullOrWhiteSpace())
                {
                    newProduct.ATTRIBUTEID2 = (int?)
                        (context.ATTRIBUTEs.FirstOrDefault(
                            a => a.JDECODE == item.AttributeJdeCode2 && a.LEVELNUMBER == 2)?.ID);
                }

                if (!item.AttributeJdeCode3.IsNullOrWhiteSpace())
                {
                    newProduct.ATTRIBUTEID3 = (int?)
                        (context.ATTRIBUTEs.FirstOrDefault(
                            a => a.JDECODE == item.AttributeJdeCode3 && a.LEVELNUMBER == 3)?.ID);
                }

                if (!item.AttributeJdeCode4.IsNullOrWhiteSpace())
                {
                    newProduct.ATTRIBUTEID4 = (int?)
                        (context.ATTRIBUTEs.FirstOrDefault(
                            a => a.JDECODE == item.AttributeJdeCode4 && a.LEVELNUMBER == 4)?.ID);
                }

                // Add new product to the DB
                context.PRODUCTs.Add(newProduct);
                context.SaveChanges();
            }
            else
            {
                // Does Exist update to new values
                PRODUCT productToUpdate = context.PRODUCTs.SingleOrDefault(p => p.JDECODE == item.ErpCode);

                productToUpdate.DIVISIONID = 1;
                productToUpdate.NAME = item.Name;

                if (!item.AttributeJdeCode1.IsNullOrWhiteSpace())
                {
                    productToUpdate.ATTRIBUTEID1 = (int?)
                        (context.ATTRIBUTEs.FirstOrDefault(
                            a => a.JDECODE == item.AttributeJdeCode1 && a.LEVELNUMBER == 1)?.ID);
                }

                if (!item.AttributeJdeCode2.IsNullOrWhiteSpace())
                {
                    productToUpdate.ATTRIBUTEID2 = (int?)
                        (context.ATTRIBUTEs.FirstOrDefault(
                            a => a.JDECODE == item.AttributeJdeCode2 && a.LEVELNUMBER == 2)?.ID);
                }

                if (!item.AttributeJdeCode3.IsNullOrWhiteSpace())
                {
                    productToUpdate.ATTRIBUTEID3 = (int?)
                        (context.ATTRIBUTEs.FirstOrDefault(
                            a => a.JDECODE == item.AttributeJdeCode3 && a.LEVELNUMBER == 3)?.ID);
                }

                if (!item.AttributeJdeCode4.IsNullOrWhiteSpace())
                {
                    productToUpdate.ATTRIBUTEID4 = (int?)
                        (context.ATTRIBUTEs.FirstOrDefault(
                            a => a.JDECODE == item.AttributeJdeCode4 && a.LEVELNUMBER == 4)?.ID);
                }

                productToUpdate.ISPURCHASED = item.IsPurchased;
                productToUpdate.INTERFACETIME = DateTime.Now;
                productToUpdate.ACTIVE = true;

                //Update product to the DB
                context.SaveChanges();

            }

        }
    }
}

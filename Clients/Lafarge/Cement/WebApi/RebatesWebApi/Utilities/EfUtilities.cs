﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Mapping;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace RebatesWebApi.Utilities
{
    public class EfUtilities
    {
        /// <summary>
        /// Get the table name from an entity framework model
        /// </summary>
        /// <param name="context"></param>
        /// <param name="modelType"></param>
        /// <returns></returns>
        public static EntitySet GetTable(
            DbContext context,
            Type modelType)
        {
            var metadata = ((IObjectContextAdapter)context).ObjectContext.MetadataWorkspace;

            // Get the part of the model that contains info about the actual CLR types
            var objectItemCollection = ((ObjectItemCollection)metadata.GetItemCollection(DataSpace.OSpace));

            // Get the entity type from the model that maps to the CLR type
            var entityType = metadata
                .GetItems<EntityType>(DataSpace.OSpace)
                .Single(e => objectItemCollection.GetClrType(e) == modelType);

            // Get the entity set that uses this entity type
            var entitySet = metadata
                .GetItems<EntityContainer>(DataSpace.CSpace)
                .Single()
                .EntitySets
                .Single(s => s.ElementType.Name == entityType.Name);

            // Find the mapping between conceptual and storage model for this entity set
            var mapping = metadata.GetItems<EntityContainerMapping>(DataSpace.CSSpace)
                .Single()
                .EntitySetMappings
                .Single(s => s.EntitySet == entitySet);

            // Find the storage entity set (table) that the entity is mapped
            var table = mapping
                .EntityTypeMappings.Single()
                .Fragments.Single()
                .StoreEntitySet;

            return table;
        }

        public static SqlBulkCopy CreateSqlBulkCopyObject(
            DbContext dbContext,
            SqlConnection sqlConnection,
            SqlTransaction sqlTransaction,
            Type modelType,
            int batchSize = 1000,
            int bulkCopyTimeout = 60,
            SqlBulkCopyOptions options = SqlBulkCopyOptions.Default)
        {
            EntitySet table = GetTable(dbContext, modelType);

            SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(sqlConnection, options, sqlTransaction)
            {
                DestinationTableName = (string)table.MetadataProperties["Table"].Value ?? table.Name,
                BatchSize = batchSize,
                BulkCopyTimeout = bulkCopyTimeout
            };

            PropertyDescriptorCollection propertyDescriptors = TypeDescriptor.GetProperties(modelType);

            foreach (PropertyInfo propertyInfo in modelType.GetProperties().OrderBy(p => p.MetadataToken))
            {
                if (IsMapableColumn(propertyDescriptors, propertyInfo))
                {
                    // Create DataTable to SqlTable mapping
                    PropertyDescriptor propertyDescriptor = propertyDescriptors[propertyInfo.Name];
                    ColumnAttribute columnAttribute =
                        (ColumnAttribute)propertyDescriptor.Attributes[typeof(ColumnAttribute)];
                    string columnName = (columnAttribute == null || columnAttribute.Name == null)
                        ? propertyInfo.Name 
                        : columnAttribute.Name;

                    // Ensure property / column exists in the entity
                    if (table.ElementType.Properties.Any(p => p.Name == columnName))
                        sqlBulkCopy.ColumnMappings.Add(propertyInfo.Name, columnName);
                }
            }

            return sqlBulkCopy;
        }

        /// <summary>
        /// Create a DataTable from an IEnumerable collection
        /// </summary>
        /// <param name="items"></param>
        /// <returns>DataTable</returns>
        public static DataTable EnumerableToDataTable<T>(IEnumerable<T> items)
        {
            return EnumerableToDataTable(items, typeof(T));
        }

        /// <summary>
        /// Create a DataTable from an IEnumerable collection
        /// </summary>
        /// <param name="items"></param>
        /// <param name="type"></param>
        /// <returns>DataTable</returns>
        public static DataTable EnumerableToDataTable(
            IEnumerable items,
            Type type)
        {
            var dataTable = new DataTable();
            PropertyDescriptorCollection propertyDescriptors = TypeDescriptor.GetProperties(type);

            foreach (var propertyInfo in type.GetProperties().OrderBy(p => p.MetadataToken))
            {
                if (IsMapableColumn(propertyDescriptors, propertyInfo))
                {
                    var dataColumn = new DataColumn(propertyInfo.Name,
                        Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType);
                    dataTable.Columns.Add(dataColumn);
                }
            }

            foreach (var item in items)
            {
                var dataRow = dataTable.NewRow();

                foreach (var propertyInfo in type.GetProperties().OrderBy(p => p.MetadataToken))
                {
                    if (dataTable.Columns[propertyInfo.Name] != null)
                        dataRow[propertyInfo.Name] = propertyInfo.GetValue(item) ?? DBNull.Value;
                }

                dataTable.Rows.Add(dataRow);
            }

            return dataTable;
        }

        private static bool IsMapableColumn(
            PropertyDescriptorCollection propertyDescriptors,
            PropertyInfo propertyInfo)
        {
            // Get property descriptor details for model property
            PropertyDescriptor propertyDescriptor = propertyDescriptors[propertyInfo.Name];

            if (propertyDescriptor == null)
                return false;

            // Ignore not mapped columns
            NotMappedAttribute notMappedAttribute =
                (NotMappedAttribute)propertyDescriptor.Attributes[typeof(NotMappedAttribute)];

            if (notMappedAttribute != null)
                return false;

            // Ignore properties with the SqlBulkCopyIgnore Attribute
            SqlBulkCopyIgnore sqlBulkCopyIgnore =
                (SqlBulkCopyIgnore)propertyDescriptor.Attributes[typeof(SqlBulkCopyIgnore)];

            if (sqlBulkCopyIgnore != null)
                return false;

            // Ignore collections
            if (propertyInfo.PropertyType.GetInterface("IEnumerable") != null &&
                propertyInfo.PropertyType != typeof(string))
                return false;

            return true;
        }

        

        public static bool IsAuditableColumn(
            PropertyDescriptorCollection propertyDescriptors,
            PropertyInfo propertyInfo)
        {
            // Get property descriptor details for model property
            var propertyDescriptor = propertyDescriptors[propertyInfo.Name];

            if (propertyDescriptor == null)
                return false;

            // Ignore Key attribute
            if (propertyDescriptor.Attributes[typeof(KeyAttribute)] != null) return false;

            // Ignore not mapped columns
            if (propertyDescriptor.Attributes[typeof(NotMappedAttribute)] != null) return false;

            // Ignore properties with the AuditIgnore Attribute
            if (propertyDescriptor.Attributes[typeof(AuditIgnore)] != null) return false;

            // Ignore properties with ForeignKeyAttribute
            if (propertyDescriptor.Attributes[typeof(ForeignKeyAttribute)] != null) return false;

            // Ignore collections
            if (propertyInfo.PropertyType.GetInterface("IEnumerable") != null &&
                propertyInfo.PropertyType != typeof(string))
                return false;

            return true;
        }
    }




    public class SqlBulkCopyIgnore : System.Attribute
    {
    }

    public class AuditIgnore : System.Attribute
    {
    }
}

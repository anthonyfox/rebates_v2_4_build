using System;

namespace RebatesWebApi.Areas.HelpPage.ModelDescriptions
{
    public class ParameterAnnotation
    {
        public System.Attribute AnnotationAttribute { get; set; }

        public string Documentation { get; set; }
    }
}
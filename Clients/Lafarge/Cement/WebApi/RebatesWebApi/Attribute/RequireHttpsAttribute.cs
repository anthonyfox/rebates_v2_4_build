﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace RebatesWebApi.Attribute
{
    public class RequireHttpsAttribute : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {

            if (actionContext.Request.RequestUri.Scheme != Uri.UriSchemeHttps)
            {
                actionContext.Response = new HttpResponseMessage(statusCode: HttpStatusCode.Forbidden)
                {
                    ReasonPhrase = "Https Required"


                };
            }
            else
            {
                base.OnAuthorization(actionContext);
            }
        }
    }
}


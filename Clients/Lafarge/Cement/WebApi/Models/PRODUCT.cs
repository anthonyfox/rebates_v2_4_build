namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PRODUCT")]
    public partial class PRODUCT
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PRODUCT()
        {
            INVOICELINEs = new HashSet<INVOICELINE>();
        }

        public int ID { get; set; }

        public int DIVISIONID { get; set; }

        [Required]
        [StringLength(20)]
        public string JDECODE { get; set; }

        [Required]
        [StringLength(40)]
        public string NAME { get; set; }

        public int? ATTRIBUTEID1 { get; set; }

        public int? ATTRIBUTEID2 { get; set; }

        public int? ATTRIBUTEID3 { get; set; }

        public int? ATTRIBUTEID4 { get; set; }

        public bool ISPURCHASED { get; set; }

        public DateTime INTERFACETIME { get; set; }

        public bool ACTIVE { get; set; }

        public virtual ATTRIBUTE ATTRIBUTE { get; set; }

        public virtual ATTRIBUTE ATTRIBUTE1 { get; set; }

        public virtual ATTRIBUTE ATTRIBUTE2 { get; set; }

        public virtual ATTRIBUTE ATTRIBUTE3 { get; set; }

        public virtual DIVISION DIVISION { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INVOICELINE> INVOICELINEs { get; set; }
    }
}

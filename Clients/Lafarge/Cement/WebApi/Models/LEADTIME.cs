namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LEADTIME")]
    public partial class LEADTIME
    {
        public int ID { get; set; }

        public int DIVISIONID { get; set; }

        [Required]
        [StringLength(40)]
        public string JDECODE { get; set; }

        [Required]
        [StringLength(40)]
        public string NAME { get; set; }

        public int CREATEUSERID { get; set; }

        public DateTime CREATETIME { get; set; }

        public int? UPDATEUSERID { get; set; }

        public DateTime? UPDATETIME { get; set; }

        public virtual DIVISION DIVISION { get; set; }
    }
}

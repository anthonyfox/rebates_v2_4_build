namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CUSTOMERGROUP")]
    public partial class CUSTOMERGROUP
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CUSTOMERGROUP()
        {
            CUSTOMERs = new HashSet<CUSTOMER>();
        }

        public int ID { get; set; }

        public int DIVISIONID { get; set; }

        [Required]
        [StringLength(20)]
        public string JDECODE { get; set; }

        [Required]
        [StringLength(50)]
        public string NAME { get; set; }

        public DateTime INTERFACETIME { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CUSTOMER> CUSTOMERs { get; set; }

        public virtual DIVISION DIVISION { get; set; }
    }
}

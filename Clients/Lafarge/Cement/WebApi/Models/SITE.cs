namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SITE")]
    public partial class SITE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SITE()
        {
            INVOICELINEs = new HashSet<INVOICELINE>();
        }

        public int ID { get; set; }

        public int DIVISIONID { get; set; }

        [Required]
        [StringLength(10)]
        public string JDECODE { get; set; }

        [Required]
        [StringLength(30)]
        public string NAME { get; set; }

        public int? ADDRESSID { get; set; }

        [StringLength(10)]
        public string JDECODEREBATE { get; set; }

        public bool ACTIVE { get; set; }

        public virtual ADDRESS ADDRESS { get; set; }

        public virtual DIVISION DIVISION { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INVOICELINE> INVOICELINEs { get; set; }
    }
}

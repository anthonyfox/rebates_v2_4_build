namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DIVISION")]
    public partial class DIVISION
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DIVISION()
        {
            ATTRIBUTEs = new HashSet<ATTRIBUTE>();
            CUSTOMERGROUPs = new HashSet<CUSTOMERGROUP>();
            DELIVERYWINDOWs = new HashSet<DELIVERYWINDOW>();
            INVOICELINEs = new HashSet<INVOICELINE>();
            LEADTIMEs = new HashSet<LEADTIME>();
            PRICINGSUBGROUPs = new HashSet<PRICINGSUBGROUP>();
            PRODUCTs = new HashSet<PRODUCT>();
            SITEs = new HashSet<SITE>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(20)]
        public string NAME { get; set; }

        [Required]
        [StringLength(200)]
        public string LOGOPATH { get; set; }

        public DateTime LASTINVOICEINTERFACE { get; set; }

        public bool ALLOWGLOBALPRODUCT { get; set; }

        public bool ALLOWPAYMENTAUTHORISATION { get; set; }

        public bool ISCALCULATING { get; set; }

        public int? CALCULATIONUSERID { get; set; }

        public DateTime? CALCULATIONTIME { get; set; }

        public bool ISFORECASTING { get; set; }

        public int? FORECASTUSERID { get; set; }

        public DateTime? FORECASTTIME { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ATTRIBUTE> ATTRIBUTEs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CUSTOMERGROUP> CUSTOMERGROUPs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DELIVERYWINDOW> DELIVERYWINDOWs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INVOICELINE> INVOICELINEs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LEADTIME> LEADTIMEs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRICINGSUBGROUP> PRICINGSUBGROUPs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRODUCT> PRODUCTs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SITE> SITEs { get; set; }
    }
}

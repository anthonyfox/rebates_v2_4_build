using System.Diagnostics.CodeAnalysis;

namespace Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("INVOICELINE")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    public class INVOICELINE
    {
        public int ID { get; set; }

        public int DIVISIONID { get; set; }

        [Required]
        [StringLength(10)]
        public string COMPANY { get; set; }

        [Required]
        [StringLength(15)]
        public string DOCUMENTNUMBER { get; set; }

        [Required]
        [StringLength(5)]
        public string DOCUMENTTYPE { get; set; }

        [Required]
        [StringLength(5)]
        public string LINENUMBER { get; set; }

        public DateTime SALEDATE { get; set; }

        [Column(TypeName = "numeric")]
        public decimal QUANTITY { get; set; }

        [Column(TypeName = "numeric")]
        public decimal AMOUNT { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? DISCOUNTAMOUNT { get; set; }

        public int PRODUCTID { get; set; }

        public int SITEID { get; set; }

        public int SOLDTOID { get; set; }

        public int SHIPTOID { get; set; }

        public int DELIVERYTYPEENUM { get; set; }

        public bool ISAPPLICABLEQUANTITY { get; set; }

        public bool ISAPPLICABLEVALUE { get; set; }

        public int? DELIVERYWINDOWID { get; set; }

        public int? LEADTIMEID { get; set; }

        public short PACKINGTYPEENUM { get; set; }

        public DateTime INTERFACETIME { get; set; }

        [Required]
        [StringLength(15)]
        public string ORDERNUMBER { get; set; }

        [Required]
        [StringLength(10)]
        public string ORDERLINENUMBER { get; set; }

        [StringLength(5)]
        public string ORDERDOCTYPE { get; set; }

        public virtual CUSTOMER CUSTOMER { get; set; }

        public virtual CUSTOMER CUSTOMER1 { get; set; }

        public virtual DIVISION DIVISION { get; set; }

        public virtual PRODUCT PRODUCT { get; set; }

        public virtual SITE SITE { get; set; }
    }
}

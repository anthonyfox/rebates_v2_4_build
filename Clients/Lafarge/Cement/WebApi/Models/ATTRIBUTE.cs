namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ATTRIBUTE")]
    public partial class ATTRIBUTE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ATTRIBUTE()
        {
            PRODUCTs = new HashSet<PRODUCT>();
            PRODUCTs1 = new HashSet<PRODUCT>();
            PRODUCTs2 = new HashSet<PRODUCT>();
            PRODUCTs3 = new HashSet<PRODUCT>();
        }

        public int ID { get; set; }

        public int DIVISIONID { get; set; }

        [Required]
        [StringLength(22)]
        public string JDECODE { get; set; }

        [Required]
        [StringLength(40)]
        public string NAME { get; set; }

        public short LEVELNUMBER { get; set; }

        public DateTime INTERFACETIME { get; set; }

        public virtual DIVISION DIVISION { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRODUCT> PRODUCTs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRODUCT> PRODUCTs1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRODUCT> PRODUCTs2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRODUCT> PRODUCTs3 { get; set; }
    }
}

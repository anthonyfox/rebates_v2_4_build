namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ADDRESS")]
    public partial class ADDRESS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ADDRESS()
        {
            CUSTOMERs = new HashSet<CUSTOMER>();
            SITEs = new HashSet<SITE>();
        }

        public int ID { get; set; }


        [Required]
        [StringLength(20)]
        public string JDECODE { get; set; }

        [Required]
        [StringLength(60)]
        public string ADDRESS1 { get; set; }

        [StringLength(60)]
        public string ADDRESS2 { get; set; }

        [StringLength(60)]
        public string ADDRESS3 { get; set; }

        [StringLength(60)]
        public string ADDRESS4 { get; set; }

        [StringLength(40)]
        public string CITY { get; set; }

        [StringLength(40)]
        public string COUNTY { get; set; }

        [StringLength(15)]
        public string POSTCODE { get; set; }

        [StringLength(3)]
        public string COUNTRYCODE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CUSTOMER> CUSTOMERs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SITE> SITEs { get; set; }
    }
}

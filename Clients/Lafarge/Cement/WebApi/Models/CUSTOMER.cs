namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CUSTOMER")]
    public partial class CUSTOMER
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CUSTOMER()
        {
            CUSTOMER1 = new HashSet<CUSTOMER>();
            INVOICELINEs = new HashSet<INVOICELINE>();
            INVOICELINEs1 = new HashSet<INVOICELINE>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(20)]
        public string JDECODE { get; set; }

        [Required]
        [StringLength(60)]
        public string NAME { get; set; }

        public int ADDRESSID { get; set; }

        public int? CUSTOMERGROUPID { get; set; }

        public int? PRICINGSUBGROUPID { get; set; }

        public int? SOLDTOID { get; set; }

        public DateTime INTERFACETIME { get; set; }

        [Required]
        [StringLength(3)]
        public string SEARCHTYPE { get; set; }

        public bool ACTIVE { get; set; }

        public virtual ADDRESS ADDRESS { get; set; }

        public virtual CUSTOMERGROUP CUSTOMERGROUP { get; set; }

        public virtual PRICINGSUBGROUP PRICINGSUBGROUP { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CUSTOMER> CUSTOMER1 { get; set; }

        public virtual CUSTOMER CUSTOMER2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INVOICELINE> INVOICELINEs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INVOICELINE> INVOICELINEs1 { get; set; }
    }
}

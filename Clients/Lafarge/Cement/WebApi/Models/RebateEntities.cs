namespace Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class RebateEntities : DbContext
    {
        public RebateEntities() :base("name=" + Environment.MachineName + "_RebateEntities" )
        {
            
        }
       


        public virtual DbSet<ADDRESS> ADDRESSes { get; set; }
        public virtual DbSet<ATTRIBUTE> ATTRIBUTEs { get; set; }
        public virtual DbSet<CUSTOMER> CUSTOMERs { get; set; }
        public virtual DbSet<CUSTOMERGROUP> CUSTOMERGROUPs { get; set; }
        public virtual DbSet<DELIVERYWINDOW> DELIVERYWINDOWs { get; set; }
        public virtual DbSet<DIVISION> DIVISIONs { get; set; }
        public virtual DbSet<INVOICELINE> INVOICELINEs { get; set; }
        public virtual DbSet<LEADTIME> LEADTIMEs { get; set; }
        public virtual DbSet<PRICINGSUBGROUP> PRICINGSUBGROUPs { get; set; }
        public virtual DbSet<PRODUCT> PRODUCTs { get; set; }
        public virtual DbSet<SITE> SITEs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ADDRESS>()
                .Property(e => e.ADDRESS1)
                .IsUnicode(false);

            modelBuilder.Entity<ADDRESS>()
                .Property(e => e.ADDRESS2)
                .IsUnicode(false);

            modelBuilder.Entity<ADDRESS>()
                .Property(e => e.ADDRESS3)
                .IsUnicode(false);

            modelBuilder.Entity<ADDRESS>()
                .Property(e => e.ADDRESS4)
                .IsUnicode(false);

            modelBuilder.Entity<ADDRESS>()
                .Property(e => e.CITY)
                .IsUnicode(false);

            modelBuilder.Entity<ADDRESS>()
                .Property(e => e.COUNTY)
                .IsUnicode(false);

            modelBuilder.Entity<ADDRESS>()
                .Property(e => e.POSTCODE)
                .IsUnicode(false);

            modelBuilder.Entity<ADDRESS>()
                .Property(e => e.COUNTRYCODE)
                .IsUnicode(false);

            modelBuilder.Entity<ADDRESS>()
                .HasMany(e => e.CUSTOMERs)
                .WithRequired(e => e.ADDRESS)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ATTRIBUTE>()
                .Property(e => e.JDECODE)
                .IsUnicode(false);

            modelBuilder.Entity<ATTRIBUTE>()
                .Property(e => e.NAME)
                .IsUnicode(false);

            modelBuilder.Entity<ATTRIBUTE>()
                .HasMany(e => e.PRODUCTs)
                .WithOptional(e => e.ATTRIBUTE)
                .HasForeignKey(e => e.ATTRIBUTEID1);

            modelBuilder.Entity<ATTRIBUTE>()
                .HasMany(e => e.PRODUCTs1)
                .WithOptional(e => e.ATTRIBUTE1)
                .HasForeignKey(e => e.ATTRIBUTEID2);

            modelBuilder.Entity<ATTRIBUTE>()
                .HasMany(e => e.PRODUCTs2)
                .WithOptional(e => e.ATTRIBUTE2)
                .HasForeignKey(e => e.ATTRIBUTEID3);

            modelBuilder.Entity<ATTRIBUTE>()
                .HasMany(e => e.PRODUCTs3)
                .WithOptional(e => e.ATTRIBUTE3)
                .HasForeignKey(e => e.ATTRIBUTEID4);

            modelBuilder.Entity<CUSTOMER>()
                .Property(e => e.NAME)
                .IsUnicode(false);

            modelBuilder.Entity<CUSTOMER>()
                .Property(e => e.SEARCHTYPE)
                .IsUnicode(false);

            modelBuilder.Entity<CUSTOMER>()
                .HasMany(e => e.CUSTOMER1)
                .WithOptional(e => e.CUSTOMER2)
                .HasForeignKey(e => e.SOLDTOID);

            modelBuilder.Entity<CUSTOMER>()
                .HasMany(e => e.INVOICELINEs)
                .WithRequired(e => e.CUSTOMER)
                .HasForeignKey(e => e.SHIPTOID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CUSTOMER>()
                .HasMany(e => e.INVOICELINEs1)
                .WithRequired(e => e.CUSTOMER1)
                .HasForeignKey(e => e.SOLDTOID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CUSTOMERGROUP>()
                .Property(e => e.JDECODE)
                .IsUnicode(false);

            modelBuilder.Entity<CUSTOMERGROUP>()
                .Property(e => e.NAME)
                .IsUnicode(false);

            modelBuilder.Entity<DELIVERYWINDOW>()
                .Property(e => e.JDECODE)
                .IsUnicode(false);

            modelBuilder.Entity<DELIVERYWINDOW>()
                .Property(e => e.NAME)
                .IsUnicode(false);

            modelBuilder.Entity<DIVISION>()
                .Property(e => e.NAME)
                .IsUnicode(false);

            modelBuilder.Entity<DIVISION>()
                .Property(e => e.LOGOPATH)
                .IsUnicode(false);

            modelBuilder.Entity<DIVISION>()
                .HasMany(e => e.ATTRIBUTEs)
                .WithRequired(e => e.DIVISION)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DIVISION>()
                .HasMany(e => e.CUSTOMERGROUPs)
                .WithRequired(e => e.DIVISION)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DIVISION>()
                .HasMany(e => e.DELIVERYWINDOWs)
                .WithRequired(e => e.DIVISION)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DIVISION>()
                .HasMany(e => e.INVOICELINEs)
                .WithRequired(e => e.DIVISION)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DIVISION>()
                .HasMany(e => e.LEADTIMEs)
                .WithRequired(e => e.DIVISION)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DIVISION>()
                .HasMany(e => e.PRICINGSUBGROUPs)
                .WithRequired(e => e.DIVISION)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DIVISION>()
                .HasMany(e => e.PRODUCTs)
                .WithRequired(e => e.DIVISION)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DIVISION>()
                .HasMany(e => e.SITEs)
                .WithRequired(e => e.DIVISION)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<INVOICELINE>()
                .Property(e => e.COMPANY)
                .IsUnicode(false);

            modelBuilder.Entity<INVOICELINE>()
                .Property(e => e.DOCUMENTNUMBER)
                .IsUnicode(false);

            modelBuilder.Entity<INVOICELINE>()
                .Property(e => e.DOCUMENTTYPE)
                .IsUnicode(false);

            modelBuilder.Entity<INVOICELINE>()
                .Property(e => e.LINENUMBER)
                .IsUnicode(false);

            modelBuilder.Entity<INVOICELINE>()
                .Property(e => e.QUANTITY)
                .HasPrecision(10, 3);

            modelBuilder.Entity<INVOICELINE>()
                .Property(e => e.ORDERLINENUMBER)
                .IsUnicode(false);

            modelBuilder.Entity<INVOICELINE>()
                .Property(e => e.ORDERDOCTYPE)
                .IsUnicode(false);

            modelBuilder.Entity<LEADTIME>()
                .Property(e => e.JDECODE)
                .IsUnicode(false);

            modelBuilder.Entity<LEADTIME>()
                .Property(e => e.NAME)
                .IsUnicode(false);

            modelBuilder.Entity<PRICINGSUBGROUP>()
                .Property(e => e.JDECODE)
                .IsUnicode(false);

            modelBuilder.Entity<PRICINGSUBGROUP>()
                .Property(e => e.NAME)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCT>()
                .Property(e => e.JDECODE)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCT>()
                .Property(e => e.NAME)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCT>()
                .HasMany(e => e.INVOICELINEs)
                .WithRequired(e => e.PRODUCT)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SITE>()
                .Property(e => e.JDECODE)
                .IsUnicode(false);

            modelBuilder.Entity<SITE>()
                .Property(e => e.NAME)
                .IsUnicode(false);

            modelBuilder.Entity<SITE>()
                .Property(e => e.JDECODEREBATE)
                .IsUnicode(false);

            modelBuilder.Entity<SITE>()
                .HasMany(e => e.INVOICELINEs)
                .WithRequired(e => e.SITE)
                .WillCascadeOnDelete(false);
        }
    }
}

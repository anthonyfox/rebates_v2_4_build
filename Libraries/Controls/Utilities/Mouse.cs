using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Evolve.Libraries.Controls.Utilities
{
	public class Mouse
	{
		[DllImport("user32.dll")]
		private static extern bool TrackMouseEvent(ref MouseEvent lpEventTrack);

		private const uint TME_HOVER = 0x00000001;
		private const uint TME_LEAVE = 0x00000002;
		private const uint HOVER_DEFAULT = 0xFFFFFFFF;

		private struct MouseEvent
		{
			public uint cbSize;
			public uint dwFlags;
			public IntPtr hwndTrack;
			public uint dwHoverTime;
		}

		public static void ResetHoverEvent(Control control)
		{
			MouseEvent mouseEvent = new MouseEvent();
			
			mouseEvent.hwndTrack = control.Handle;
			mouseEvent.dwFlags = TME_HOVER;
			mouseEvent.dwHoverTime = HOVER_DEFAULT;
			mouseEvent.cbSize = (uint) Marshal.SizeOf(mouseEvent);
			
			TrackMouseEvent(ref mouseEvent);
		}
	}
}

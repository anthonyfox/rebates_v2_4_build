﻿using System;
using System.Collections;

namespace Evolve.Libraries.Controls.Utilities
{
	public class ComboBoxItem
	{
		private Object _code;
		private Object _description;

		public Object Code
		{
			get { return _code; }
			set { _code = value; }
		}

		public Object Description
		{
			get { return _description; }
			set { _description = value; }
		}

		public ComboBoxItem(Object code, Object description)
		{
			_code = code;
			_description = description;
		}
	}

	[Serializable]
	public class ComboBoxItemCollection : CollectionBase
	{
		public ComboBoxItemCollection()
		{
		}

		public ComboBoxItemCollection(ComboBoxItemCollection comboBoxItems)
		{
			foreach (ComboBoxItem comboBoxItem in comboBoxItems)
			{
				this.Add(comboBoxItem);
			}
		}

		public virtual ComboBoxItem this[Int32 index]
		{
			get { return (ComboBoxItem) this.List[index]; }
			set { this.List[index] = value; }
		}

		public virtual void Add(ComboBoxItem comboBoxItem)
		{
			if (!this.Contains(comboBoxItem))
			{
				this.List.Add(comboBoxItem);
			}
		}

		public virtual Boolean Contains(ComboBoxItem comboBoxItem)
		{
			return this.List.Contains(comboBoxItem);
		}

		public virtual Int32 IndexOf(ComboBoxItem comboBoxItem)
		{
			return this.List.IndexOf(comboBoxItem);
		}

		public virtual void Remove(ComboBoxItem comboBoxItem)
		{
			if (this.Contains(comboBoxItem))
			{
				this.List.Remove(comboBoxItem);
			}
		}

		public virtual void Insert(Int32 index, ComboBoxItem comboBoxItem)
		{
			this.List.Insert(index, comboBoxItem);
		}

		protected override void OnValidate(Object value)
		{
			base.OnValidate(value);

			if (!(value is ComboBoxItem))
			{
				throw new ArgumentException("Collection only supports ComboBoxItem objects.");
			}
		}
	}
}

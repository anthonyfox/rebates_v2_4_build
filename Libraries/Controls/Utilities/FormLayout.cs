using System.Windows.Forms;
using Evolve.Libraries.Controls.Generic;

namespace Evolve.Libraries.Controls.Utilities
{
	public class FormLayout
	{
		public static void LabelTextBoxComboBoxControls(Control control)
		{
			if (control.GetType().BaseType.Name == "LabelTextBoxComboBox")
			{
				((LabelTextBoxComboBox) control).InitializeControl();
			}
			else
			{
				if (control.HasChildren)
				{
					foreach (Control childControl in control.Controls)
					{
						LabelTextBoxComboBoxControls(childControl);
					}
				}
			}
		}
	}
}
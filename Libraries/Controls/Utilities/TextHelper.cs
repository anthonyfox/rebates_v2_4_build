﻿using System.Drawing;
using System.Windows.Forms;

namespace Evolve.Libraries.Controls.Utilities
{
	public static class TextHelper
	{
		public static StringAlignment TranslateAligment(HorizontalAlignment aligment)
		{
			switch (aligment)
			{
				case HorizontalAlignment.Left:
					return StringAlignment.Near;
				case HorizontalAlignment.Right:
					return StringAlignment.Far;
				default:
					return StringAlignment.Center;
			}
		}

		public static HorizontalAlignment TranslateGridColumnAligment(DataGridViewContentAlignment aligment)
		{
			if (aligment == DataGridViewContentAlignment.BottomLeft || aligment == DataGridViewContentAlignment.MiddleLeft || aligment == DataGridViewContentAlignment.TopLeft)
			{
				return HorizontalAlignment.Left;
			}
			else if (aligment == DataGridViewContentAlignment.BottomRight || aligment == DataGridViewContentAlignment.MiddleRight || aligment == DataGridViewContentAlignment.TopRight)
			{
				return HorizontalAlignment.Right;
			}
			else
			{
				return HorizontalAlignment.Left;
			}
		}

		public static TextFormatFlags TranslateAligmentToFlag(HorizontalAlignment aligment)
		{
			switch (aligment)
			{
				case HorizontalAlignment.Left:
					return TextFormatFlags.Left;
				case HorizontalAlignment.Right:
					return TextFormatFlags.Right;
				default:
					return TextFormatFlags.HorizontalCenter;
			}
		}

		public static TextFormatFlags TranslateTrimmingToFlag(StringTrimming trimming)
		{
			switch (trimming)
			{
				case StringTrimming.EllipsisCharacter:
					return TextFormatFlags.EndEllipsis;
				case StringTrimming.EllipsisPath:
					return TextFormatFlags.PathEllipsis;
				case StringTrimming.EllipsisWord:
					return TextFormatFlags.WordEllipsis;
				case StringTrimming.Word:
					return TextFormatFlags.WordBreak;
				default:
					return TextFormatFlags.Default;
			}
		}
	}
}

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Evolve.Libraries.Controls.Generic
{
	public partial class LabelComboBox : UserControl
	{
		private Int32 _controlSpacing = Constants.Control.Spacing;
		private LicenseUsageMode _usageMode;

		public new Boolean Enabled
		{
			get { return _comboBox.Enabled; }
			set { _comboBox.Enabled = value; }
		}

		[Browsable(false)]
		public LicenseUsageMode UsageMode
		{
			get { return _usageMode; }
		}

		[Browsable(false)]
		public Label Label
		{
			get { return _label; }
			set { _label = value; }
		}

		[Browsable(false)]
		public ComboBoxWithReadOnly ComboBox
		{
			get { return _comboBox; }
			set { _comboBox = value; }
		}

		[Category("Appearance")]
		public Int32 ControlSpacing
		{
			get { return _controlSpacing; }
			set { _controlSpacing = value; InitializeControl(); }
		}

		[Category("Appearance")]
		public String LabelText
		{
			get { return _label.Text; }
			set { _label.Text = value; InitializeControl(); }
		}

		[Category("Appearance")]
		public Int32 ComboBoxWidth
		{
			get { return _comboBox.Width; }
			set { _comboBox.Width = value; InitializeControl(); }
		}

		[Category("Behavior")]
		public Boolean ReadOnly
		{
			//get { return !_comboBox.Enabled; }
			//set { _comboBox.Enabled = !value; }
			get { return _comboBox.ReadOnly; }
			set { _comboBox.ReadOnly = value; }
		}

		public LabelComboBox()
		{
			// Save the LicenseManagerUsageMode as this only appears to be reliable in the constructor
			_usageMode = LicenseManager.UsageMode;
			InitializeComponent();
		}

		public void InitializeControl()
		{
			if (_label.Visible)
			{
				// Resize controls based on the label size
				Point point = _comboBox.Location;
				point.X = _label.Location.X + _label.Size.Width + _controlSpacing;
				_comboBox.Location = point;
			}
			else
			{
				_comboBox.Location = _label.Location;
			}
		}
	}
}

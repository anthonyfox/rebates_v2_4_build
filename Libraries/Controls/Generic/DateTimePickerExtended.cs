﻿using System;
using System.Collections;
using System
.Runtime.CompilerServices;using System.Windows.Forms;

namespace Evolve.Libraries.Controls.Generic
{
	public class DateTimePickerExtended : DateTimePicker
	{
		private Boolean _isDropped = false;

		#region DateChanged Event

		private Hashtable _delegateStore = new Hashtable();
		private static Object _dateChangedEventKey = new Object();

		public delegate void DateChangedHandler(object sender, EventArgs e);

		public event DateChangedHandler DateChanged
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			add
			{
				_delegateStore[_dateChangedEventKey] = Delegate.Combine((Delegate) _delegateStore[_dateChangedEventKey], value);
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			remove
			{
				_delegateStore[_dateChangedEventKey] = Delegate.Remove((Delegate) _delegateStore[_dateChangedEventKey], value);
			}
		}

		protected void OnDateChanged()
		{
			DateChangedHandler dateChangedHandler = (DateChangedHandler) _delegateStore[_dateChangedEventKey];

			if (dateChangedHandler != null)
			{
				dateChangedHandler(this, null);
			}
		}

		#endregion

		public DateTimePickerExtended()
		{
			this.DropDown += new EventHandler(DateTimePickerExtended_DropDown);
			this.CloseUp += new EventHandler(DateTimePickerExtended_CloseUp);
			this.ValueChanged += new EventHandler(DateTimePickerExtended_ValueChanged);
		}

		void DateTimePickerExtended_ValueChanged(object sender, EventArgs e)
		{
			if (!_isDropped)
			{
				OnDateChanged();
			}
		}

		void DateTimePickerExtended_CloseUp(object sender, EventArgs e)
		{
			_isDropped = false;
			OnDateChanged();
		}

		void DateTimePickerExtended_DropDown(object sender, EventArgs e)
		{
			_isDropped = true;
		}
	}
}

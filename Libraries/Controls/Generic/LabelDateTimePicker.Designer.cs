namespace Evolve.Libraries.Controls.Generic
{
	partial class LabelDateTimePicker
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this._label = new System.Windows.Forms.Label();
			this._dateTimePicker = new System.Windows.Forms.DateTimePicker();
			this._errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
			((System.ComponentModel.ISupportInitialize) (this._errorProvider)).BeginInit();
			this.SuspendLayout();
			// 
			// _label
			// 
			this._label.AutoSize = true;
			this._label.Location = new System.Drawing.Point(17, 4);
			this._label.Name = "_label";
			this._label.Size = new System.Drawing.Size(33, 13);
			this._label.TabIndex = 0;
			this._label.Text = "Label";
			// 
			// _dateTimePicker
			// 
			this._dateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this._dateTimePicker.Location = new System.Drawing.Point(56, 0);
			this._dateTimePicker.Name = "_dateTimePicker";
			this._dateTimePicker.Size = new System.Drawing.Size(96, 20);
			this._dateTimePicker.TabIndex = 1;
			// 
			// _errorProvider
			// 
			this._errorProvider.ContainerControl = this;
			// 
			// LabelDateTimePicker
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this._dateTimePicker);
			this.Controls.Add(this._label);
			this.Name = "LabelDateTimePicker";
			this.Size = new System.Drawing.Size(294, 20);
			((System.ComponentModel.ISupportInitialize) (this._errorProvider)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label _label;
		private System.Windows.Forms.DateTimePicker _dateTimePicker;
		private System.Windows.Forms.ErrorProvider _errorProvider;
	}
}

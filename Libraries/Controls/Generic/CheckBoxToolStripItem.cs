﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Collections;
using System.Runtime.CompilerServices;

namespace Evolve.Libraries.Controls.Generic
{
	public class CheckBoxToolStripItem : ToolStripControlHost
    {
        private FlowLayoutPanel _controlPanel;
        private CheckBox _checkBox = new CheckBox();

		public delegate void CheckedChangedEventHandler(object sender, EventArgs e);

		private Hashtable _delegateStore = new Hashtable();
		private static Object _checkedChangedEventKey = new Object();
		
		public event CheckedChangedEventHandler CheckedChanged
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			add
			{
				_delegateStore[_checkedChangedEventKey] = Delegate.Combine((Delegate) _delegateStore[_checkedChangedEventKey], value);
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			remove
			{
				_delegateStore[_checkedChangedEventKey] = Delegate.Remove((Delegate) _delegateStore[_checkedChangedEventKey], value);
			}
		}

		protected void OnCheckedChanged()
		{
			CheckedChangedEventHandler checkedChangedEventHandler = (CheckedChangedEventHandler) _delegateStore[_checkedChangedEventKey];

			if (checkedChangedEventHandler != null)
			{
				checkedChangedEventHandler(this, null);
			}
		}

        public CheckBoxToolStripItem()
            : base(new FlowLayoutPanel())
        {
            // Set up the FlowLayouPanel.
            _controlPanel = (FlowLayoutPanel)base.Control;
            _controlPanel.BackColor = Color.Transparent;
 
            // Add two child controls.
            _controlPanel.AutoSize = true;
            _controlPanel.Controls.Add(_checkBox);
        }

        public bool Checked
        {
            get { return _checkBox.Checked; }
            set { _checkBox.Checked = value; }
        }

        protected override void OnSubscribeControlEvents(Control control)
        {
            base.OnSubscribeControlEvents(control);
			_checkBox.CheckedChanged += new EventHandler(CheckedChangedInternal);
        }
 
        protected override void OnUnsubscribeControlEvents(Control control)
        {
            base.OnUnsubscribeControlEvents(control);
            _checkBox.CheckedChanged -= new EventHandler(CheckedChangedInternal);
        }

		private void CheckedChangedInternal(object sender, EventArgs e)
		{
			OnCheckedChanged();
		}
    }
}

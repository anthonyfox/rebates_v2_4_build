﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Evolve.Libraries.Controls.Generic
{
	public class TextBoxTime : TextBox
	{
		private Int16 _timeInMinutes = 0;

		public Int16 TimeInMinutes
		{
			get
			{
				return _timeInMinutes;
			}
			set
			{
				_timeInMinutes = value; this.Text = GetTimeText(true);
			}
		}

		public TextBoxTime()
		{
			this.KeyPress += new KeyPressEventHandler(TextBoxTime_KeyPress);
			this.Validating += new System.ComponentModel.CancelEventHandler(TextBoxTime_Validating);
			this.Enter += new EventHandler(TextBoxTime_Enter);
			this.MaxLength = 5;
			this.Width = 34;
		}

		void TextBoxTime_Enter(object sender, EventArgs e)
		{
			TextBoxTime textBoxTime = (TextBoxTime) sender;

			Int32 hour = textBoxTime.TimeInMinutes / 60;
			Int32 minutes = textBoxTime.TimeInMinutes % 60;

			textBoxTime.Text = hour.ToString("00") + minutes.ToString("00");
		}
		
		private String GetTimeText(Boolean includeColon)
		{
			Int16 hours = (Int16) (this.TimeInMinutes / 60);
			Int16 minutes = (Int16) (this.TimeInMinutes % 60);

			return hours.ToString("00") + (includeColon ? ":" : "") + minutes.ToString("00");
		}

		public static Int16 GetTimeInMinutes(String text)
		{
			String shortText = ShortTimeText(text);
			Int16 hours = Convert.ToInt16(shortText.Substring(0, 2));
			Int16 minutes = Convert.ToInt16(shortText.Substring(2, 2));

			return (Int16) (hours * 60 + minutes);
		}

		void TextBoxTime_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			ParseTimeText(true);
		}

		protected void ParseTimeText(Boolean includeColon)
		{
			String timeText = ShortTimeText(this.Text);
			Regex regexTime = new Regex("[0-2][0-9][0-5][0-9]");
			Boolean isValidTime = regexTime.IsMatch(timeText);

			if (isValidTime)
			{
				Int16 hours = Int16.Parse(timeText.Substring(0, 2));
				Int16 minutes = Int16.Parse(timeText.Substring(2, 2));

				this.TimeInMinutes = (Int16) (hours * 60 + minutes);
				this.Text = GetTimeText(includeColon);

				OnValueChanged(new EventArgs());
			}
		}

		void TextBoxTime_KeyPress(object sender, KeyPressEventArgs e)
		{
			Char[] valid = new Char[] {
				'0',
				'1',
				'2',
				'3',
				'4',
				'5',
				'6',
				'7',
				'8',
				'9',
				':',
				'\b' };

			if (valid.Contains(e.KeyChar))
			{
				// Prevent more than one : character
				e.Handled = (e.KeyChar == ':' && this.Text.Contains(':'));
			}
			else
			{
				// Suppress key
				e.Handled = true;
			}

			if (!e.Handled)
			{
				// Ensure overwrite mode
				this.SelectionLength = 1;
			}
		}

		private static String ShortTimeText(String text)
		{
			return text.Trim().Replace(":", "").PadLeft(4, '0').Substring(0, 4);
		}

		#region INotifyPropertyChanged

		public event EventHandler ValueChanged;
		protected virtual void OnValueChanged(EventArgs e)
		{
			if (ValueChanged != null)
			{
				ValueChanged(this, e);
			}
		}

		#endregion
	}

	public class DataGridViewTextBoxTimeColumn : DataGridViewColumn
	{
		public DataGridViewTextBoxTimeColumn()
			: base(new DataGridViewTextBoxTimeCell())
		{
		}

		public override DataGridViewCell CellTemplate
		{
			get
			{
				return base.CellTemplate;
			}
			set
			{
				// Ensure that the cell used for the template is a CalendarCell.
				if (value != null &&
					!value.GetType().IsAssignableFrom(typeof(DataGridViewTextBoxTimeCell)))
				{
					throw new InvalidCastException("Must be a DataGridViewTextBoxTimeCell");
				}

				base.CellTemplate = value;
			}
		}
	}

	public class DataGridViewTextBoxTimeCell : DataGridViewTextBoxCell
	{
		public DataGridViewTextBoxTimeCell()
			: base()
		{
		}

		public override void InitializeEditingControl(int rowIndex, object initialFormattedValue, DataGridViewCellStyle dataGridViewCellStyle)
		{
			// Set the value of the editing control to the current cell value.
			base.InitializeEditingControl(rowIndex, initialFormattedValue, dataGridViewCellStyle);
			TextBoxTimeEditingControl ctl = DataGridView.EditingControl as TextBoxTimeEditingControl;
			DataGridViewTextBoxTimeColumn owningColumn = (DataGridViewTextBoxTimeColumn) this.OwningColumn;

			if (this.RowIndex > -1 && this.ColumnIndex > -1 && this.Value != null)
			{
				ctl.TimeInMinutes = (Int16) this.Value;
			}
		}

		public override Type EditType
		{
			get
			{
				// Return the type of the editing contol that CalendarCell uses.
				return typeof(TextBoxTimeEditingControl);
			}
		}

		public override Type ValueType
		{
			get
			{
				// Return the type of the value that CalendarCell contains.
				return typeof(Int16);
			}
		}

		public override object DefaultNewRowValue
		{
			get
			{
				return (Int16) 0;
			}
		}

		protected override object GetFormattedValue(object value, int rowIndex, ref DataGridViewCellStyle cellStyle, TypeConverter valueTypeConverter, TypeConverter formattedValueTypeConverter, DataGridViewDataErrorContexts context)
		{
			Int16 hours = 0;
			Int16 minutes = 0;

			if (value != null)
			{
				hours = (Int16) ((Int16) value / 60);
				minutes = (Int16) ((Int16) value % 60);
			}
			  
			return hours.ToString("00") + ":" + minutes.ToString("00");
		}

		public override object ParseFormattedValue(object formattedValue, DataGridViewCellStyle cellStyle, TypeConverter formattedValueTypeConverter, TypeConverter valueTypeConverter)
		{
			return TextBoxTime.GetTimeInMinutes(formattedValue.ToString());
		}
	}

	internal class TextBoxTimeEditingControl : TextBoxTime, IDataGridViewEditingControl
	{
		DataGridView dataGridView;
		bool valueChanged = false;
		int rowIndex;

		public TextBoxTimeEditingControl()
		{
		}

		// Implements the IDataGridViewEditingControl.EditingControlFormattedValue 
		// property.
		public object EditingControlFormattedValue
		{
			get
			{
				this.ParseTimeText(false);
				return this.Text;
			}

			set
			{
				if (value is String)
				{
					this.TimeInMinutes = TextBoxTime.GetTimeInMinutes((String) value);
				}
			}
		}

		// Implements the 
		// IDataGridViewEditingControl.GetEditingControlFormattedValue method.
		public object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
		{
			return EditingControlFormattedValue;
		}

		// Implements the 
		// IDataGridViewEditingControl.ApplyCellStyleToEditingControl method.
		public void ApplyCellStyleToEditingControl(DataGridViewCellStyle dataGridViewCellStyle)
		{
			this.Font = dataGridViewCellStyle.Font;
		}

		// Implements the IDataGridViewEditingControl.EditingControlRowIndex 
		// property.
		public int EditingControlRowIndex
		{
			get
			{
				return rowIndex;
			}
			set
			{
				rowIndex = value;
			}
		}

		// Implements the IDataGridViewEditingControl.EditingControlWantsInputKey 
		// method.
		public bool EditingControlWantsInputKey(Keys key, bool dataGridViewWantsInputKey)
		{
			// Let the DateTimePicker handle the keys listed.
			switch (key & Keys.KeyCode)
			{
				case Keys.Left:
				case Keys.Up:
				case Keys.Down:
				case Keys.Right:
				case Keys.Home:
				case Keys.End:
				case Keys.PageDown:
				case Keys.PageUp:
					return true;
				default:
					return false;
			}
		}

		// Implements the IDataGridViewEditingControl.PrepareEditingControlForEdit 
		// method.
		public void PrepareEditingControlForEdit(bool selectAll)
		{
			// No preparation needs to be done.
		}

		// Implements the IDataGridViewEditingControl
		// .RepositionEditingControlOnValueChange property.
		public bool RepositionEditingControlOnValueChange
		{
			get
			{
				return false;
			}
		}

		// Implements the IDataGridViewEditingControl
		// .EditingControlDataGridView property.
		public DataGridView EditingControlDataGridView
		{
			get
			{
				return dataGridView;
			}
			set
			{
				dataGridView = value;
			}
		}

		// Implements the IDataGridViewEditingControl
		// .EditingControlValueChanged property.
		public bool EditingControlValueChanged
		{
			get
			{
				return valueChanged;
			}
			set
			{
				valueChanged = value;
			}
		}

		// Implements the IDataGridViewEditingControl
		// .EditingPanelCursor property.
		public Cursor EditingPanelCursor
		{
			get
			{
				return base.Cursor;
			}
		}


		protected override void OnValueChanged(EventArgs eventargs)
		{
			// Notify the DataGridView that the contents of the cell
			// have changed.
			valueChanged = true;
			this.EditingControlDataGridView.NotifyCurrentCellDirty(true);
			base.OnValueChanged(eventargs);
		}
	}
}

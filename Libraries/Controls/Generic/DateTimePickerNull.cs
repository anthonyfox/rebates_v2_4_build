﻿using System;
using System.Windows.Forms;


namespace Evolve.Libraries.Controls.Generic
{
	public class DateTimePickerNull : DateTimePicker
	{
		private DateTimePickerFormat format = DateTimePickerFormat.Long;
		private String customFormat = null;
		private Boolean isNull = false;

		public DateTimePickerNull() : base()
		{
		}

		public DateTime ValueNotNull
		{
			get	{ return base.Value; }
			set	{ this.Value = value; }
		}

		public new DateTime? Value
		{
			get
			{
				if (isNull)
				{
					return null;
				}
				else
				{
					return base.Value;
				}
			}

			set
			{
				if (value == null || value == DateTime.MinValue)
				{
					if (isNull == false)
					{
						format = this.Format;
						customFormat = this.CustomFormat;
						isNull = true;
					}

					this.Format = DateTimePickerFormat.Custom;
					this.CustomFormat = " ";
				}
				else
				{
					if (isNull)
					{
						this.Format = format;
						this.CustomFormat = customFormat;
						isNull = false;
					}

					base.Value = (DateTime) value.Value;
				}
			}
		}

		protected override void OnValueChanged(EventArgs eventArgs)
		{
			this.Value = base.Value;
			base.OnValueChanged(eventArgs);
		}

		protected override void OnCloseUp(EventArgs eventArgs)
		{
			if (Control.MouseButtons == MouseButtons.None)
			{
				if (isNull)
				{
					this.Format = format;
					this.CustomFormat = customFormat;
					isNull = false;
				}
			}

			base.OnCloseUp(eventArgs);
		}

		protected override void OnKeyDown(KeyEventArgs e)
		{
			base.OnKeyDown(e);

			if (e.KeyCode == Keys.Delete)
			{
				this.Value = DateTime.MinValue;
			}
		}
	}


}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Evolve.Libraries.Controls.Utilities;

namespace Evolve.Libraries.Controls.Generic
{
	public partial class SummaryControlContainer : Control
	{
		#region Declare variables

		private Hashtable _sumHashtable;
		private DataGridViewSummary _dataGridView;
		private Label _sumHeaderLabel;

		private Int32 _initialHeight;
		public Int32 InitialHeight
		{
			get { return _initialHeight; }
			set { _initialHeight = value; }
		}

		private bool _lastVisibleState;
		public bool LastVisibleState
		{
			get { return _lastVisibleState; }
			set { _lastVisibleState = value; }
		}

		private Color _summaryRowBackColor;
		public Color SummaryRowBackColor
		{
			get { return _summaryRowBackColor; }
			set { _summaryRowBackColor = value; }
		}

		/// <summary>
		/// Event is raised when visibility changes and the
		/// LastVisibleState is not the new visible state
		/// </summary>
		public event EventHandler VisibilityChanged;

		#endregion

		#region Constructors

		public SummaryControlContainer(DataGridViewSummary dataGridView)
		{
			if (dataGridView == null)
			{
				throw new ArgumentException("DataGridView is null");
			}

			_dataGridView = dataGridView;

			_sumHashtable = new Hashtable();
			_sumHeaderLabel = new Label();

			_dataGridView.CreateSummary += new EventHandler(_dataGridView_CreateSummary);
			_dataGridView.RowsAdded += new DataGridViewRowsAddedEventHandler(_dataGridView_RowsAdded);
			_dataGridView.RowsRemoved += new DataGridViewRowsRemovedEventHandler(_dataGridView_RowsRemoved);
			_dataGridView.CellValueChanged += new DataGridViewCellEventHandler(_dataGridView_CellValueChanged);

			_dataGridView.Scroll += new ScrollEventHandler(_dataGridView_Scroll);
			_dataGridView.ColumnWidthChanged += new DataGridViewColumnEventHandler(_dataGridView_ColumnWidthChanged);
			_dataGridView.RowHeadersWidthChanged += new EventHandler(_dataGridView_RowHeadersWidthChanged);

			VisibleChanged += new EventHandler(SummaryControlContainer_VisibleChanged);

			_dataGridView.ColumnAdded += new DataGridViewColumnEventHandler(_dataGridView_ColumnAdded);
			_dataGridView.ColumnRemoved += new DataGridViewColumnEventHandler(_dataGridView_ColumnRemoved);
			_dataGridView.ColumnStateChanged += new DataGridViewColumnStateChangedEventHandler(_dataGridView_ColumnStateChanged);
			_dataGridView.ColumnDisplayIndexChanged += new DataGridViewColumnEventHandler(_dataGridView_ColumnDisplayIndexChanged);

		}

		private void _dataGridView_ColumnDisplayIndexChanged(Object sender, DataGridViewColumnEventArgs e)
		{
			RecreateSumBoxes();
		}

		private void _dataGridView_ColumnStateChanged(Object sender, DataGridViewColumnStateChangedEventArgs e)
		{
			ResizeSumBoxes();
		}

		private void _dataGridView_ColumnRemoved(Object sender, DataGridViewColumnEventArgs e)
		{
			RecreateSumBoxes();
		}

		private void _dataGridView_ColumnAdded(Object sender, DataGridViewColumnEventArgs e)
		{
			RecreateSumBoxes();
		}

		private void _dataGridView_CellValueChanged(Object sender, DataGridViewCellEventArgs e)
		{
			ReadOnlyTextBox readOnlyTextBox = (ReadOnlyTextBox) _sumHashtable[_dataGridView.Columns[e.ColumnIndex]];

			if (readOnlyTextBox != null)
			{
				if (readOnlyTextBox.IsSummary)
				{
					CalculateSummaries();
				}
			}
		}

		private void _dataGridView_RowsAdded(Object sender, DataGridViewRowsAddedEventArgs e)
		{
			CalculateSummaries();
		}

		private void _dataGridView_RowsRemoved(Object sender, DataGridViewRowsRemovedEventArgs e)
		{
			CalculateSummaries();
		}

		private void SummaryControlContainer_VisibleChanged(Object sender, EventArgs e)
		{
			if (_lastVisibleState != this.Visible)
			{
				OnVisiblityChanged(sender, e);
			}
		}

		protected void OnVisiblityChanged(Object sender, EventArgs e)
		{
			if (VisibilityChanged != null)
			{
				VisibilityChanged(sender, e);
			}

			_lastVisibleState = this.Visible;
		}

		#endregion

		#region Events and delegates

		private void _dataGridView_CreateSummary(Object sender, EventArgs e)
		{
			RecreateSumBoxes();
			CalculateSummaries();
		}

		private void _dataGridView_Scroll(Object sender, ScrollEventArgs e)
		{
			ResizeSumBoxes();
		}

		private void _dataGridView_ColumnWidthChanged(Object sender, DataGridViewColumnEventArgs e)
		{
			ResizeSumBoxes();
		}

		private void _dataGridView_RowHeadersWidthChanged(Object sender, EventArgs e)
		{
			ResizeSumBoxes();
		}

		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);
			ResizeSumBoxes();
		}

		private void _dataGridView_Resize(Object sender, EventArgs e)
		{
			ResizeSumBoxes();
		}

		#endregion

		#region Functions

		/// <summary>
		/// Checks if passed Object is of type of Int32 eger
		/// </summary>
		/// <param name="o">object</param>
		/// <returns>true/ false</returns>
		protected bool IsInteger(Object o)
		{
			if (o is Int64)
			{
				return true;
			}
			if (o is Int32)
			{
				return true;
			}
			if (o is Int16)
			{
				return true;
			}

			return false;
		}

		/// <summary>
		/// Checks if passed Object is of type of decimal/ double
		/// </summary>
		/// <param name="o">object</param>
		/// <returns>true/ false</returns>
		protected bool IsDecimal(Object o)
		{
			if (o is Decimal)
			{
				return true;
			}
			if (o is Single)
			{
				return true;
			}
			if (o is Double)
			{
				return true;
			}

			return false;
		}

		/// <summary>
		/// Enable manual refresh of the SummaryDataGridView
		/// </summary>
		internal void RefreshSummary()
		{
			CalculateSummaries();
		}

		/// <summary>
		/// Calculate the Sums of the summary columns
		/// </summary>
		private void CalculateSummaries()
		{
			foreach (ReadOnlyTextBox readOnlyTextBox in _sumHashtable.Values)
			{
				if (readOnlyTextBox.IsSummary)
				{
					readOnlyTextBox.Tag = 0;
					readOnlyTextBox.Text = "0";
					readOnlyTextBox.Invalidate();
				}
			}

			if (_dataGridView.SummaryColumns != null && _dataGridView.SummaryColumns.Length > 0 && _sumHashtable.Count > 0)
			{
				foreach (DataGridViewRow _dataGridViewRow in _dataGridView.Rows)
				{
					foreach (DataGridViewCell _dataGridViewCell in _dataGridViewRow.Cells)
					{
						foreach (DataGridViewColumn _dataGridViewColumn in _sumHashtable.Keys)
						{
							if (_dataGridViewCell.OwningColumn.Equals(_dataGridViewColumn))
							{
								ReadOnlyTextBox sumBox = (ReadOnlyTextBox) _sumHashtable[_dataGridViewColumn];

								if (sumBox != null && sumBox.IsSummary)
								{
									if (_dataGridViewCell.Value != null && !(_dataGridViewCell.Value is DBNull))
									{
										if (IsInteger(_dataGridViewCell.Value))
										{
											sumBox.Tag = Convert.ToInt64(sumBox.Tag) + Convert.ToInt64(_dataGridViewCell.Value);
										}
										else if (IsDecimal(_dataGridViewCell.Value))
										{
											sumBox.Tag = Convert.ToDecimal(sumBox.Tag) + Convert.ToDecimal(_dataGridViewCell.Value);
										}

										sumBox.Text = string.Format("{0}", sumBox.Tag);
										sumBox.Invalidate();
									}
								}
							}
						}
					}
				}
			}
		}

		/// <summary>
		/// Create summary boxes for each Column of the DataGridView        
		/// </summary>
		private void RecreateSumBoxes()
		{
			ReadOnlyTextBox sumBox;

			foreach (Control control in _sumHashtable.Values)
			{
				this.Controls.Remove(control);
			}

			_sumHashtable.Clear();

			Int32 iCnt = 0;
			List<DataGridViewColumn> sortedColumns = SortedColumns;

			foreach (DataGridViewColumn _dataGridViewColumn in sortedColumns)
			{
				sumBox = new ReadOnlyTextBox();
				_sumHashtable.Add(_dataGridViewColumn, sumBox);

				sumBox.Visible = false;
				sumBox.Top = 0;
				sumBox.Height = _dataGridView.RowTemplate.Height;
				sumBox.BorderColor = _dataGridView.GridColor;

				if (_summaryRowBackColor == null || _summaryRowBackColor == Color.Empty)
				{
					sumBox.BackColor = _dataGridView.DefaultCellStyle.BackColor;
				}
				else
				{
					sumBox.BackColor = _summaryRowBackColor;
				}

				sumBox.BringToFront();

				if (_dataGridView.ColumnCount - iCnt == 1)
				{
					sumBox.IsLastColumn = true;
				}

				if (_dataGridView.SummaryColumns != null && _dataGridView.SummaryColumns.Length > 0)
				{
					for (Int32 iCntX = 0; iCntX < _dataGridView.SummaryColumns.Length; iCntX++)
					{
						if (_dataGridView.SummaryColumns[iCntX] == _dataGridViewColumn.DataPropertyName ||
							_dataGridView.SummaryColumns[iCntX] == _dataGridViewColumn.Name)
						{
							_dataGridViewColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
							_dataGridViewColumn.CellTemplate.Style.Format = _dataGridView.FormatString;

							sumBox.TextAlign = TextHelper.TranslateGridColumnAligment(_dataGridViewColumn.DefaultCellStyle.Alignment);
							sumBox.IsSummary = true;
							sumBox.FormatString = _dataGridViewColumn.CellTemplate.Style.Format;

							if (_dataGridViewColumn.ValueType == typeof(System.Int32) || _dataGridViewColumn.ValueType == typeof(System.Int16) ||
								_dataGridViewColumn.ValueType == typeof(System.Int64) || _dataGridViewColumn.ValueType == typeof(System.Single) ||
								_dataGridViewColumn.ValueType == typeof(System.Double) || _dataGridViewColumn.ValueType == typeof(System.Single) ||
								_dataGridViewColumn.ValueType == typeof(System.Decimal))
							{
								sumBox.Tag = System.Activator.CreateInstance(_dataGridViewColumn.ValueType);
							}
						}
					}
				}

				sumBox.BringToFront();
				this.Controls.Add(sumBox);

				iCnt++;
			}

			if (_dataGridView.DisplaySumRowHeader)
			{
				_sumHeaderLabel.Font = new Font(_dataGridView.DefaultCellStyle.Font, _dataGridView.SumRowHeaderTextBold ? FontStyle.Bold : FontStyle.Regular);
				_sumHeaderLabel.Anchor = AnchorStyles.Left;
				_sumHeaderLabel.TextAlign = ContentAlignment.MiddleLeft;
				_sumHeaderLabel.Height = _sumHeaderLabel.Font.Height;
				_sumHeaderLabel.Top = Convert.ToInt32(Convert.ToDouble(this.InitialHeight - _sumHeaderLabel.Height) / 2F);
				_sumHeaderLabel.Text = _dataGridView.SumRowHeaderText;

				_sumHeaderLabel.ForeColor = _dataGridView.DefaultCellStyle.ForeColor;
				_sumHeaderLabel.Margin = new Padding(0);
				_sumHeaderLabel.Padding = new Padding(0);
				_sumHeaderLabel.Visible = (_dataGridView.SummaryRowVisible && _dataGridView.DisplaySumRowHeader);

				this.Controls.Add(_sumHeaderLabel);
			}

			CalculateSummaries();
			ResizeSumBoxes();
		}

		/// <summary>
		/// Order the columns in the way they are displayed
		/// </summary>
		private List<DataGridViewColumn> SortedColumns
		{
			get
			{
				List<DataGridViewColumn> result = new List<DataGridViewColumn>();
				DataGridViewColumn column = _dataGridView.Columns.GetFirstColumn(DataGridViewElementStates.None);

				if (column == null)
				{
					return result;
				}

				result.Add(column);

				while ((column = _dataGridView.Columns.GetNextColumn(column, DataGridViewElementStates.None, DataGridViewElementStates.None)) != null)
				{
					result.Add(column);
				}

				return result;
			}
		}

		/// <summary>
		/// Resize the summary Boxes depending on the 
		/// width of the Columns of the DataGridView
		/// </summary>
		private void ResizeSumBoxes()
		{
			this.SuspendLayout();

			if (_sumHashtable.Count > 0)
			{
				try
				{
					Int32 rowHeaderWidth = _dataGridView.RowHeadersVisible ? _dataGridView.RowHeadersWidth - 1 : 0;
					Int32 sumLabelWidth = _dataGridView.RowHeadersVisible ? _dataGridView.RowHeadersWidth - 1 : 0;
					Int32 curPos = rowHeaderWidth;

					if (_dataGridView.DisplaySumRowHeader && sumLabelWidth > 0)
					{
						if (_dataGridView.RightToLeft == RightToLeft.Yes)
						{
							if (_sumHeaderLabel.Dock != DockStyle.Right)
							{
								_sumHeaderLabel.Dock = DockStyle.Right;
							}
						}
						else
						{
							if (_sumHeaderLabel.Dock != DockStyle.Left)
							{
								_sumHeaderLabel.Dock = DockStyle.Left;
							}
						}
					}
					else
					{
						if (_sumHeaderLabel.Visible)
						{
							_sumHeaderLabel.Visible = false;
						}
					}

					Int32 iCnt = 0;
					Rectangle oldBounds;

					foreach (DataGridViewColumn _dataGridViewColumn in SortedColumns)
					{
						ReadOnlyTextBox sumBox = (ReadOnlyTextBox) _sumHashtable[_dataGridViewColumn];

						if (sumBox != null)
						{
							oldBounds = sumBox.Bounds;

							if (!_dataGridViewColumn.Visible)
							{
								sumBox.Visible = false;
								continue;
							}

							Int32 from = curPos - _dataGridView.HorizontalScrollingOffset;
							Int32 width = _dataGridViewColumn.Width + (iCnt == 0 ? 0 : 0);

							if (from < rowHeaderWidth)
							{
								width -= rowHeaderWidth - from;
								from = rowHeaderWidth;
							}

							if (from + width > this.Width)
							{
								width = this.Width - from;
							}

							if (width < 4)
							{
								if (sumBox.Visible)
								{
									sumBox.Visible = false;
								}
							}
							else
							{
								if (this.RightToLeft == RightToLeft.Yes)
								{
									from = this.Width - from - width;
								}

								if (sumBox.Left != from || sumBox.Width != width)
								{
									sumBox.SetBounds(from, 0, width, 0, BoundsSpecified.X | BoundsSpecified.Width);
								}

								if (!sumBox.Visible && sumBox.IsSummary)
								{
									sumBox.Visible = true;
								}
							}

							curPos += _dataGridViewColumn.Width + (iCnt == 0 ? 0 : 0);

							if (oldBounds != sumBox.Bounds)
							{
								sumBox.Invalidate();
							}

						}

						iCnt++;
					}
				}
				finally
				{
					this.ResumeLayout();
				}
			}
		}

		#endregion
	}
}

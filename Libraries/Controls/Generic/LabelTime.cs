﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Evolve.Libraries.Controls.Generic
{
	public class LabelTime : LabelTextBox
	{
		private DateTime? _dateTime;

		public Boolean IsValidTime
		{
			get
			{
				Regex regex = new Regex("[0-2][0-9]:[0-9][0-9]");
				return regex.IsMatch(TextBox.Text);
			}
		}

		public DateTime? Time
		{
			get { return GetTime(); }
			set { SetTime(value); }
		}

		public new Boolean Enabled
		{
			get { return TextBox.Enabled; }
			set { TextBox.Enabled = value; }
		}

		public LabelTime()
		{
			TextBoxWidth = 35;

			TextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(TextBox_KeyDown);
			TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(TextBox_KeyPress);
			TextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(TextBox_KeyUp);
			TextBox.Leave += new EventHandler(TextBox_Leave);
		}

		void TextBox_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Delete)
			{
				TextBox.Text = "";
			}
		}

		void TextBox_KeyPress(object sender, KeyPressEventArgs e)
		{
			Int32 position = TextBox.SelectionStart;
			Boolean isValidKey = IsValidKey(position, e.KeyChar);

			if (isValidKey && position < TextBox.Text.Length)
			{
				TextBox.SelectionLength = 1;
			}

			e.Handled = !isValidKey;
		}

		void TextBox_KeyUp(object sender, KeyEventArgs e)
		{
			Char[] chars = TextBox.Text.ToCharArray();
			Int32 position = TextBox.SelectionStart;

			if (chars.Length >= 2 && chars[0] == '2' && chars.Length >= 2 && chars[1] >= '4')
			{
				chars[1] = '0';
			}

			TextBox.Text = "";

			foreach (Char character in chars)
			{
				TextBox.Text += character;
			}

			if (TextBox.Text.Length == 2)
			{
				TextBox.Text += ":";
			}

			if (position == 2)
			{
				if (e.KeyCode == Keys.Left)
				{
					TextBox.SelectionStart = 1;
				}
				else
				{
					TextBox.SelectionStart = 3;
				}
			}
			else
			{
				TextBox.SelectionStart = position;
			}
		}

		void TextBox_Leave(object sender, EventArgs e)
		{
			if (TextBox.Text.Length > 0 && TextBox.Text.Length < 5)
			{
				String[] parts = TextBox.Text.Split(new char[] { ':' });

				Int16 hours = 0;
				Int16 minutes = 0;

				if (parts.Length >= 1)
				{
					Int16.TryParse(parts[0], out hours);
				}

				if (parts.Length >= 2)
				{
					Int16.TryParse(parts[1], out minutes);
				}

				TextBox.Text = hours.ToString("00") + ":" + minutes.ToString("00");
			}
		}

		private Boolean IsValidKey(Int32 position, Char keyChar)
		{
			Char[] chars = TextBox.Text.ToCharArray();
			Boolean isValidKey = false;

			switch (position)
			{
				case 0:
					isValidKey = (keyChar >= '0' && keyChar <= '2');
					break;

				case 1:
					if (chars[0] == '2')
					{
						isValidKey = (keyChar >= '0' && keyChar <= '3');
					}
					else
					{
						isValidKey = (keyChar >= '0' && keyChar <= '9');
					}

					break;

				case 2:
					isValidKey = (keyChar == ':');
					break;

				case 3:
					isValidKey = (keyChar >= '0' && keyChar <= '5');
					break;

				case 4:
					isValidKey = (keyChar >= '0' && keyChar <= '9');
					break;
			}

			return isValidKey;
		}

		private DateTime? GetTime()
		{
			DateTime? dateTime = null;

			if (this.IsValidTime)
			{
				Int32 hour = Int32.Parse(TextBox.Text.Substring(0, 2));
				Int32 minute = Int32.Parse(TextBox.Text.Substring(3, 2));

				if (_dateTime.HasValue)
				{
					dateTime = new DateTime(_dateTime.Value.Year, _dateTime.Value.Month, _dateTime.Value.Day, hour, minute, 0);
				}
				else
				{
					dateTime = new DateTime(1, 1, 1, hour, minute, 0);
				}
			}

			return dateTime;
		}

		private void SetTime(DateTime? dateTime)
		{
			if (dateTime != null)
			{
				_dateTime = dateTime;
				TextBox.Text = dateTime.Value.Hour.ToString("00") + ":" + dateTime.Value.Minute.ToString("00");
			}
		}
	}
}

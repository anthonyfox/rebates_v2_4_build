﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Evolve.Libraries.Controls.Utilities;

namespace Evolve.Libraries.Controls.Generic
{
	public partial class ReadOnlyTextBox : Control
	{
		private StringFormat _format;

		public ReadOnlyTextBox()
		{
			InitializeComponent();

			_format = new StringFormat(StringFormatFlags.NoWrap | StringFormatFlags.FitBlackBox | StringFormatFlags.MeasureTrailingSpaces);
			_format.LineAlignment = StringAlignment.Center;

			this.Height = 10;
			this.Width = 10;

			this.Padding = new Padding(2);
		}

		public ReadOnlyTextBox(IContainer container)
		{
			container.Add(this);
			InitializeComponent();

			this.TextChanged += new EventHandler(ReadOnlyTextBox_TextChanged);
		}

		private void ReadOnlyTextBox_TextChanged(object sender, EventArgs e)
		{
			if (!string.IsNullOrEmpty(_formatString) && !string.IsNullOrEmpty(Text))
			{
				Text = string.Format(_formatString, Text);
			}
		}

		private Color borderColor = Color.Black;

		private Boolean _isSummary;

		public Boolean IsSummary
		{
			get { return _isSummary; }
			set { _isSummary = value; }
		}

		private Boolean _isLastColumn;

		public Boolean IsLastColumn
		{
			get { return _isLastColumn; }
			set { _isLastColumn = value; }
		}

		private String _formatString;

		public String FormatString
		{
			get { return _formatString; }
			set { _formatString = value; }
		}


		private HorizontalAlignment _textAlign = HorizontalAlignment.Left;

		[DefaultValue(HorizontalAlignment.Left)]
		public HorizontalAlignment TextAlign
		{
			get { return _textAlign; }
			set
			{
				_textAlign = value;
				SetFormatFlags();
			}
		}

		private StringTrimming _trimming = StringTrimming.None;

		[DefaultValue(StringTrimming.None)]
		public StringTrimming Trimming
		{
			get { return _trimming; }
			set
			{
				_trimming = value;
				SetFormatFlags();
			}
		}

		private void SetFormatFlags()
		{
			_format.Alignment = TextHelper.TranslateAligment(_textAlign);
			_format.Trimming = _trimming;
		}

		public Color BorderColor
		{
			get { return borderColor; }
			set { borderColor = value; }
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			if (this.Visible)
			{
				Int32 subWidth = 0;
				Rectangle textBounds;

				if (!string.IsNullOrEmpty(_formatString) && !string.IsNullOrEmpty(Text))
				{
					Text = String.Format("{0:" + _formatString + "}", Convert.ToDecimal(Text));
				}

				textBounds = new Rectangle(this.ClientRectangle.X + 2, this.ClientRectangle.Y + 2, this.ClientRectangle.Width - 2, this.ClientRectangle.Height - 2);

				using (Pen pen = new Pen(borderColor))
				{
					if (_isLastColumn)
					{
						subWidth = 1;
					}

					e.Graphics.FillRectangle(new SolidBrush(this.BackColor), this.ClientRectangle);
					e.Graphics.DrawRectangle(pen, this.ClientRectangle.X, this.ClientRectangle.Y, this.ClientRectangle.Width - subWidth, this.ClientRectangle.Height - 1);
					e.Graphics.DrawString(Text, Font, Brushes.Black, textBounds, _format);
				}
			}
		}
	}
}

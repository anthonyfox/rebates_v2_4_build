﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;
using System.ComponentModel;
using System.Windows.Forms;

namespace Evolve.Libraries.Controls.Generic
{
	public class ExtendedDataGridView : DataGridView
	{
		#region Properties

		private bool _skipReadOnlyCells = false;

		[Category("Behavior")]
		[Description("Specifies if the DataGridView is to past and prevent entry to ReadOnly cells")]
		public bool SkipReadOnlyCells
		{
			get { return _skipReadOnlyCells; }
			set { _skipReadOnlyCells = value; }
		}

		#endregion

		#region ValidatingSkipCell Event

		private Hashtable _delegateStore = new Hashtable();
		private static Object _validatingSkipCellEventKey = new Object();

		public delegate bool ValidatingSkipCellHandler(object sender, DataGridViewCellEventArgs e);

		public event ValidatingSkipCellHandler ValidatingSkipCell
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			add
			{
				_delegateStore[_validatingSkipCellEventKey] = Delegate.Combine((Delegate) _delegateStore[_validatingSkipCellEventKey], value);
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			remove
			{
				_delegateStore[_validatingSkipCellEventKey] = Delegate.Remove((Delegate) _delegateStore[_validatingSkipCellEventKey], value);
			}
		}

		protected bool OnValidatingSkipCell(int columnIndex, int rowIndex)
		{
			bool skipCell = false;
			ValidatingSkipCellHandler validatingSkipCellHandler = (ValidatingSkipCellHandler) _delegateStore[_validatingSkipCellEventKey];

			if (validatingSkipCellHandler != null)
			{
				skipCell = validatingSkipCellHandler(this, new DataGridViewCellEventArgs(columnIndex, rowIndex));
			}

			return skipCell;
		}

		#endregion

		#region Constructors

		public ExtendedDataGridView()
			: base()
		{
			this.CellEnter += new DataGridViewCellEventHandler(DataGridView_CellEnter);
			this.LostFocus += new EventHandler(DataGridView_LostFocus);
		}

		#endregion

		#region Event Handlers

		void DataGridView_LostFocus(object sender, EventArgs e)
		{
			if (this.CurrentCell != null)
			{
				this.CurrentCell.Selected = false;
			}
		}

		void DataGridView_CellEnter(object sender, DataGridViewCellEventArgs e)
		{
			if (SkipCell(e.ColumnIndex, e.RowIndex))
			{
				SendKeys.Send("{Tab}");
			}
		}

		#endregion

		#region Private Methods

		private bool SkipCell(int columnIndex, int rowIndex)
		{
			bool skipCell = this.Columns[columnIndex].ReadOnly;

			if (!skipCell)
			{
				skipCell = OnValidatingSkipCell(columnIndex, rowIndex);
			}

			return skipCell;
		}

		#endregion
	}
}

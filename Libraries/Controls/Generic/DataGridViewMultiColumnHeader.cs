﻿using System;
using System.Collections;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;

namespace Evolve.Libraries.Controls.Generic
{
	public class DataGridViewMultiColumnHeader : DataGridView
	{
		// Properties
		private Int32 _headerCellLines = 2;
		private Int32 _doublePrecision = 2;
		private Boolean _doubleShowMinusSign = true;
		private Boolean _showZeroAsBlank = false;
		private MultiColumnHeaderCollection _multiColumnHeaders = new MultiColumnHeaderCollection();
		private String _documentName = "";
		private String _documentTitle = "";
		private float _documentHeaderFontSize = 8f;
		private float _documentFontSize = 4f;
		private Int32 _documentRowHeight = 8;

		// Printing variables
		private Int32 _printRowIndex = 0;
		private Int32 _printEndIndex;
		private String _printTime = "";
		private Int32 _printRowsPerPage = 90;
		private Int32 _numberOfPages;

		public Int32 HeaderCellLines
		{
		    get { return _headerCellLines; }
		    set { _headerCellLines = value; }
		}

		public MultiColumnHeaderCollection MultiColumnHeaders
		{
			get { return _multiColumnHeaders; }
			set { _multiColumnHeaders = value; }
		}

		public Int32 DoublePrecision
		{
			get { return _doublePrecision; }
			set { _doublePrecision = value; }
		}

		public Boolean DoubleShowMinusSign
		{
			get { return _doubleShowMinusSign; }
			set { _doubleShowMinusSign = value; }
		}

		public Boolean ShowZeroAsBlank
		{
			get { return _showZeroAsBlank; }
			set { _showZeroAsBlank = value; }
		}

		public String DocumentName
		{
			get { return _documentName; }
			set { _documentName = value; }
		}

		public String DocumentTitle
		{
			get { return _documentTitle; }
			set { _documentTitle = value; }
		}

		public DataGridViewMultiColumnHeader()
		{
		}

		public void InitializeComponent()
		{
			this.EnableHeadersVisualStyles = false;
			this.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
			this.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
			this.ColumnHeadersHeight = this.ColumnHeadersHeight * (_headerCellLines + 1);
			this.AllowUserToOrderColumns = false;
			this.AllowUserToResizeColumns = false;
			this.AllowUserToResizeRows = false;
			this.CellPainting += new DataGridViewCellPaintingEventHandler(DataGridViewMultiColumnHeader_CellPainting);
		}

		void DataGridViewMultiColumnHeader_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
		{
			StringFormat format = new StringFormat();
			format.LineAlignment = StringAlignment.Center;

			if (e.RowIndex == -1 && e.ColumnIndex > -1)
			{
				SolidBrush backgroundBrush = new SolidBrush(e.CellStyle.BackColor);
				SolidBrush foregroundBrush = new SolidBrush(e.CellStyle.ForeColor);

				format.Alignment = StringAlignment.Center;

				// Paint standard header text (by adjusting rectangle to lower segment of the
				// header)
				e.PaintBackground(e.CellBounds, true);

				Rectangle rectangle = e.CellBounds;
				rectangle.X += 1;
				rectangle.Y += e.CellBounds.Height / _headerCellLines;
				rectangle.Height = e.CellBounds.Height / _headerCellLines;
				rectangle.Width -= 2;

				e.PaintContent(rectangle);

				// Now paint custom header parts
				MultiColumnHeader multiColumnHeader = _multiColumnHeaders.Find(e.ColumnIndex);

				if (multiColumnHeader != null)
				{
					if (e.ColumnIndex > multiColumnHeader.StartColumn)
					{
						rectangle.X = rectangle.X - CalculateWidth(multiColumnHeader.StartColumn, e.ColumnIndex - 1);
					}

					rectangle.Y = 2;
					rectangle.Width = CalculateWidth(multiColumnHeader.StartColumn, multiColumnHeader.EndColumn) - 2;

					e.Graphics.FillRectangle(backgroundBrush, rectangle);
					e.Graphics.DrawString(multiColumnHeader.HeaderText, this.ColumnHeadersDefaultCellStyle.Font, foregroundBrush, rectangle, format);
				}

				// Must tell datagridview that we have handled the event in order to prevent
				// base methods re-drawing
				e.Handled = true;
			}

			if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
			{
				Boolean paintContent;
				String content = CellContents(this[e.ColumnIndex, e.RowIndex], format, out paintContent);
				e.PaintBackground(e.CellBounds, true);

				if (paintContent)
				{
					SolidBrush foregroundBrush = new SolidBrush(this.DefaultCellStyle.ForeColor);

					Rectangle rectangle = e.CellBounds;
					rectangle.X += 2;
					rectangle.Width -= 4;

					e.Graphics.DrawString(content, this.DefaultCellStyle.Font, foregroundBrush, rectangle, format);
				}

				e.Handled = !paintContent;
			}
		}

		private Int32 CalculateWidth(Int32 startColumn, Int32 endColumn)
		{
			Int32 width = 0;

			for (Int32 column = startColumn; column <= endColumn; column++)
			{
				if (this.Columns[column].Visible)
				{
					width += this.Columns[column].Width;
				}
			}

			return width;
		}

		private String CellContents(DataGridViewCell cell, StringFormat format, out Boolean paintContent)
		{
			String content = "";
			Boolean checkForZero = false;
			Object value = cell.Value;

			paintContent = false;

			if (value != null)
			{
				Type type = value.GetType();
				content = value.ToString();

				paintContent = true;

				switch (type.Name)
				{
					case "Int16":
						format.Alignment = StringAlignment.Far;
						paintContent = (Int16) value != Int16.MinValue;
						checkForZero = true;
						break;

					case "Int32":
						format.Alignment = StringAlignment.Far;
						paintContent = (Int32) value != Int32.MinValue;
						checkForZero = true;
						break;

					case "Decimal":
						format.Alignment = StringAlignment.Far;
						paintContent = (Decimal) value != Decimal.MinValue;
						checkForZero = true;
						break;

					case "Double":
						format.Alignment = StringAlignment.Far;
						paintContent = (Double) value != Double.MinValue;
						checkForZero = true;
						content = Math.Round((Double) value, _doublePrecision).ToString("F" + _doublePrecision.ToString());

						if (!_doubleShowMinusSign)
						{
							content = content.Replace("-", "");
						}

						break;

					case "DateTime":
						format.Alignment = StringAlignment.Center;
						paintContent = (DateTime) value != DateTime.MinValue;
						content = ((DateTime) value).ToShortDateString();
						break;

					default:
						format.Alignment = StringAlignment.Near;
						break;
				}

				if (paintContent && checkForZero && _showZeroAsBlank)
				{
					paintContent = (Double.Parse(content) != 0);
				}
			}

			return content;
		}

		public void Print()
		{
			_numberOfPages = (Int32) Math.Ceiling((Decimal) this.Rows.Count / _printRowsPerPage);
			_numberOfPages = (_numberOfPages == 0 ? 1 : _numberOfPages);
			_printRowIndex = 0;

			PrintDialog printDialog = new PrintDialog();
			printDialog.PrinterSettings.DefaultPageSettings.PaperSize = new PaperSize("A4", 825, 1175);
			printDialog.PrinterSettings.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);
			printDialog.PrinterSettings.DefaultPageSettings.Landscape = true;
			printDialog.PrinterSettings.MinimumPage = 1;
			printDialog.PrinterSettings.MaximumPage = _numberOfPages;
			printDialog.PrinterSettings.FromPage = 1;
			printDialog.PrinterSettings.ToPage = _numberOfPages;
			printDialog.AllowSomePages = true;
			printDialog.AllowPrintToFile = true;
			printDialog.UseEXDialog = true;

			if (printDialog.ShowDialog() == DialogResult.OK)
			{
				_printTime = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + ":" + DateTime.Now.Second.ToString();
				_printRowIndex = _printRowsPerPage * (printDialog.PrinterSettings.FromPage - 1);
				_printEndIndex = _printRowsPerPage * printDialog.PrinterSettings.ToPage - 1;

				PrintDocument printDocument = new PrintDocument();
				printDocument.PrinterSettings = printDialog.PrinterSettings;
				printDocument.PrintPage += new PrintPageEventHandler(printDocument_PrintPage);
				printDocument.DocumentName = _documentName;
				printDocument.Print();
			}
		}

		void printDocument_PrintPage(object sender, PrintPageEventArgs e)
		{
			Font boldItalic = new Font("Arial", _documentHeaderFontSize, FontStyle.Bold | FontStyle.Italic);
			Font font = new Font("Arial", _documentFontSize);
			SolidBrush solidBrush = new SolidBrush(Color.Black);
			Pen pen = new Pen(solidBrush);
			Point startPoint = new Point(30, 1);
			StringFormat format = new StringFormat();
			Size size;
			Rectangle rectangle;

			format.LineAlignment = StringAlignment.Center;
			format.Alignment = StringAlignment.Near;

			// Header Text
			String pageOfPages = "Page " + ((_printRowIndex / _printRowsPerPage) + 1).ToString() + " of " + _numberOfPages.ToString();
			size = new Size(CalculateWidth(0, this.Columns.Count - 1) / 2 - 10, 20);
			rectangle = new Rectangle(startPoint, size);

			e.Graphics.DrawString(_documentTitle, boldItalic, solidBrush, rectangle, format);
			format.Alignment = StringAlignment.Center;
			e.Graphics.DrawString(_documentName.ToUpper(), boldItalic, solidBrush, rectangle, format);
			format.Alignment = StringAlignment.Far;
			e.Graphics.DrawString(_printTime + "  " + pageOfPages, boldItalic, solidBrush, rectangle, format);

			// Column Headers
			startPoint = new Point(startPoint.X, 31);
			Point currentPoint = startPoint;

			format.Alignment = StringAlignment.Center;
			format.LineAlignment = StringAlignment.Far;

			foreach (DataGridViewColumn column in this.Columns)
			{
				// Get custom header information
				MultiColumnHeader multiColumnHeader = _multiColumnHeaders.Find(column.Index);

				if (multiColumnHeader != null && column.Index == multiColumnHeader.StartColumn)
				{
					size = new Size();
					size.Height = _documentRowHeight;
					size.Width = CalculateWidth(multiColumnHeader.StartColumn, multiColumnHeader.EndColumn) / 2;

					rectangle = new Rectangle(currentPoint, size);
					e.Graphics.DrawString(multiColumnHeader.HeaderText, font, solidBrush, rectangle, format);
				}

				// Now print standard header details
				Point headerPoint = new Point();
				headerPoint.X = currentPoint.X;
				headerPoint.Y = currentPoint.Y + _documentRowHeight;

				size = new Size(column.Width / 2, _documentRowHeight * 2);
				rectangle = new Rectangle(headerPoint, size);

				if ((multiColumnHeader != null && column.Index == multiColumnHeader.StartColumn) || column.Index == 0)
				{
					e.Graphics.DrawLine(pen, headerPoint.X, headerPoint.Y - _documentRowHeight, currentPoint.X, currentPoint.Y + (_documentRowHeight * 3));
				}
				else
				{
					e.Graphics.DrawLine(pen, headerPoint.X, headerPoint.Y, currentPoint.X, currentPoint.Y + (_documentRowHeight * 3));
				}

				e.Graphics.DrawString(column.HeaderText, font, solidBrush, rectangle, format);

				currentPoint.X += size.Width;

				// Draw right hand column line on last column
				if (column.Index == this.Columns.Count - 1)
				{
					headerPoint.X = currentPoint.X;
					headerPoint.Y -= _documentRowHeight;

					e.Graphics.DrawLine(pen, headerPoint.X, headerPoint.Y, currentPoint.X, currentPoint.Y + (_documentRowHeight * 3));
				}
			}

			// Draw top and bottom header lines
			currentPoint.Y += 3 * _documentRowHeight;
			e.Graphics.DrawLine(pen, startPoint.X, startPoint.Y, currentPoint.X, startPoint.Y);
			e.Graphics.DrawLine(pen, startPoint.X, currentPoint.Y, currentPoint.X, currentPoint.Y);

			// Now print data
			if (this.Rows.Count > 0)
			{
				format.LineAlignment = StringAlignment.Center;

				for (Int32 index = 0; index < _printRowsPerPage; index++)
				{
					// Draw current row
					currentPoint.X = startPoint.X;

					for (Int32 columnIndex = 0; columnIndex < this.Columns.Count; columnIndex++)
					{
						e.Graphics.DrawLine(pen, currentPoint.X, currentPoint.Y, currentPoint.X, currentPoint.Y + _documentRowHeight);

						Boolean paintContent;
						String content = CellContents(this[columnIndex, _printRowIndex], format, out paintContent);

						if (paintContent)
						{
							size = new Size(this.Columns[columnIndex].Width / 2, _documentRowHeight);
							rectangle = new Rectangle(currentPoint, size);
							e.Graphics.DrawString(content, font, solidBrush, rectangle, format);
						}

						currentPoint.X += this.Columns[columnIndex].Width / 2;
					}

					e.Graphics.DrawLine(pen, currentPoint.X, currentPoint.Y, currentPoint.X, currentPoint.Y + _documentRowHeight);

					// Increment current row pointer and print location
					_printRowIndex++;
					currentPoint.Y += _documentRowHeight;

					if (_printRowIndex >= this.Rows.Count)
					{
						e.Graphics.DrawLine(pen, startPoint.X, currentPoint.Y, currentPoint.X, currentPoint.Y);
						break;
					}
				}
			}

			e.HasMorePages = (_printRowIndex < this.Rows.Count && _printRowIndex < _printEndIndex);
		}
	}

	#region MultiColumnHeader

	[Serializable]
	public class MultiColumnHeader
	{
		private String _headerText = "";
		private Int32 _startColumn = -1;
		private Int32 _endColumn = -1;

		public String HeaderText
		{
			get { return _headerText; }
			set { _headerText = value; }
		}

		public Int32 StartColumn
		{
			get { return _startColumn; }
			set { _startColumn = value; }
		}

		public Int32 EndColumn
		{
			get { return _endColumn; }
			set { _endColumn = value; }
		}

		public MultiColumnHeader()
		{
		}

		public MultiColumnHeader(String headerText, Int32 startColumn, Int32 endColumn)
		{
			_headerText = headerText;
			_startColumn = startColumn;
			_endColumn = endColumn;
		}
	}

	[Serializable]
	public class MultiColumnHeaderCollection : CollectionBase
	{
		private MultiColumnHeader _multiColumnHeader = null;

		public MultiColumnHeaderCollection()
		{
		}

		public MultiColumnHeaderCollection(MultiColumnHeader multiColumnHeader)
		{
			this._multiColumnHeader = multiColumnHeader;
		}

		public MultiColumnHeaderCollection(MultiColumnHeaderCollection multiColumnHeaders)
		{
			foreach (MultiColumnHeader multiColumnHeader in multiColumnHeaders)
			{
				this.Add(multiColumnHeader);
			}
		}

		//Implementation of ICollection interface
		public virtual Boolean IsSynchronized
		{
			get { return false; }
		}

		public virtual object SyncRoot
		{
			get { return this.List.SyncRoot; }
		}

		public virtual void CopyTo(MultiColumnHeader[] array, Int32 index)
		{
			this.List.CopyTo(array, index);
		}

		//Implementation of IList interface
		public virtual Boolean IsFixedSize
		{
			get { return false; }
		}

		public virtual Boolean IsReadOnly
		{
			get { return false; }
		}

		public virtual MultiColumnHeader this[Int32 index]
		{
			get { return (MultiColumnHeader) this.List[index]; }
			set { this.List[index] = value; }
		}

		public virtual void Add(MultiColumnHeader multiColumnHeader)
		{
			if (!this.Contains(multiColumnHeader))
			{
				this.List.Add(multiColumnHeader);
			}
		}

		public virtual Boolean Contains(MultiColumnHeader multiColumnHeader)
		{
			return this.List.Contains(multiColumnHeader);
		}

		public virtual Int32 IndexOf(MultiColumnHeader multiColumnHeader)
		{
			return this.List.IndexOf(multiColumnHeader);
		}

		public virtual void Remove(MultiColumnHeader multiColumnHeader)
		{
			if (this.Contains(multiColumnHeader))
			{
				this.List.Remove(multiColumnHeader);
			}
		}

		public virtual void Insert(Int32 index, MultiColumnHeader multiColumnHeader)
		{
			this.List.Insert(index, multiColumnHeader);
		}

		// Ensure correct types are passed
		protected override void OnValidate(Object value)
		{
			base.OnValidate(value);

			if (!(value is MultiColumnHeader))
			{
				throw new ArgumentException("Collection only supports MultiColumnHeader objects.");
			}
		}

		public MultiColumnHeader Find(Int32 columnIndex)
		{
			MultiColumnHeader multiColumnHeader = null;

			for (Int32 index = 0; index < this.List.Count; index++)
			{
				if (columnIndex >= ((MultiColumnHeader) this.List[index]).StartColumn && columnIndex <= ((MultiColumnHeader) this.List[index]).EndColumn)
				{
					multiColumnHeader = (MultiColumnHeader) this.List[index];
					break;
				}
			}

			return multiColumnHeader;
		}
	}

	#endregion
}

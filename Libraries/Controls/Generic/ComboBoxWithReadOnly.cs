﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Evolve.Libraries.Controls.Generic
{
	/// <summary>
	/// Represents a Windows ComboBox control. It enhances the .NET standard ComboBox control
	/// with a ReadOnly mode
	/// </summary>
	///
	[ComVisible(false)]
	public class ComboBoxWithReadOnly : ComboBox
	{
		#region Member variables

		/// <summary>
		/// Member variables
		/// </summary>
		private Control _readOnlyControl;
		private Boolean _isReadOnly = false;
		private Boolean _visible = true;
		private ReadOnlyControlType _readOnlyControlType = ReadOnlyControlType.Label;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets a value indicating whether the control is read-only
		/// </summary>
		/// <value>
		/// <b>true</b> if the ComboBox is read-only; otherwise, <b>false</b>. The default is <b>false</b>
		/// </value>
		/// <remarks>
		/// When this property is set to <b>true</b>, the contents of the control cannot be changed 
		/// by the user at runtime. With this property set to <b>true</b>, you can still set the value
		/// in code. You can use this feature instead of disabling the control with the Enabled
		/// property to allow the contents to be copied
		/// </remarks>
		[Browsable(true)]
		[DefaultValue(false)]
		[Category("Behavior")]
		[Description("Controls whether the value in the combobox control can be changed or not")]
		public Boolean ReadOnly
		{
			get 
			{ 
				return _isReadOnly; 
			}
			set
			{
				if (value != _isReadOnly)
				{
					_isReadOnly = value;
					ShowControl();
				}
			}
		}

		/// <summary>
		/// Gets or sets a value indicating wether the control is displayed
		/// </summary>
		/// <value><b>true</b> if the control is displayed; otherwise, <b>false</b>
		/// The default is <b>true</b></value>
		public new Boolean Visible
		{
			get 
			{ 
				return _visible; 
			}
			set
			{
				_visible = value;
				ShowControl();
			}
		}

		/// <summary>
		/// Gets or sets a value indicating which control type will be displayed when
		/// the ReadOnly property is set to true
		/// </summary>
		/// <value><b>ReadOnlyControlType</b></value>
		public ReadOnlyControlType ReadOnlyControl
		{
			get { return _readOnlyControlType; }
			set { _readOnlyControlType = value; }
		}

		/// <summary>
		/// Conceals the control from the user
		/// </summary>
		/// <remarks>
		/// Hiding the control is equvalent to setting the <see cref="Visible"/> property to <b>false</b>
		/// After the <b>Hide</b> method is called, the <b>Visible</b> property returns a value of 
		/// <b>false</b> until the <see cref="Show"/> method is called
		/// </remarks>
		public new void Hide()
		{
			Visible = false;
		}

		/// <summary>
		/// Displays the control to the user.
		/// </summary>
		/// <remarks>
		/// Showing the control is equivalent to setting the <see cref="Visible"/> property to <b>true</b>.
		/// After the <b>Show</b> method is called, the <b>Visible</b> property returns a value of 
		/// <b>true</b> until the <see cref="Hide"/> method is called.
		/// </remarks>
		public new void Show()
		{
			Visible = true;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Default constructor
		/// </summary>
		public ComboBoxWithReadOnly()
		{
			if (_readOnlyControlType == ReadOnlyControlType.Label)
			{
				_readOnlyControl = new Label();
			}
			else
			{
				_readOnlyControl = new TextBox();
			}
		}

		#endregion

		#region Private methods
		/// <summary>
		/// Initializes the embedded TextBox with the default values from the ComboBox
		/// </summary>
		private void AddTextbox()
		{
			TextBox textBox = (TextBox) _readOnlyControl;
			
			textBox.ReadOnly = true;
			textBox.Location = this.Location;
			textBox.Size = this.Size;
			textBox.Dock = this.Dock;
			textBox.Anchor = this.Anchor;
			textBox.Enabled = base.Enabled;
			textBox.Visible = this.Visible;
			textBox.RightToLeft = this.RightToLeft;
			textBox.Font = this.Font;
			textBox.Text = this.Text;
			textBox.TabStop = this.TabStop;
			textBox.TabIndex = this.TabIndex;
		}

		/// <summary>
		/// Initializes the embedded Label with the default values from the ComboBox
		/// </summary>
		private void AddLabel()
		{
			Label label = (Label) _readOnlyControl;

			label.Location = this.Location;
			label.Size = this.Size;
			label.Dock = this.Dock;
			label.Anchor = this.Anchor;
			label.Enabled = base.Enabled;
			label.Visible = this.Visible;
			label.RightToLeft = this.RightToLeft;
			label.Font = this.Font;
			label.Text = this.Text;
			label.TabStop = this.TabStop;
			label.TabIndex = this.TabIndex;
			label.BorderStyle = BorderStyle.Fixed3D;
			label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
		}

		/// <summary>
		/// Shows either the ComboBox or the TextBox or nothing, depending on the state
		/// of the ReadOnly, Enable and Visible flags.
		/// </summary>
		private void ShowControl()
		{
			if (_isReadOnly)
			{
				_readOnlyControl.Visible = _visible && this.Enabled;
				base.Visible = _visible && !this.Enabled;
				_readOnlyControl.Text = this.Text;
			}
			else
			{
				_readOnlyControl.Visible = false;
				base.Visible = _visible;
			}
		}

		#endregion

		#region Event Handlers

		/// <summary>
		/// This member overrides <see cref="Control.OnParentChanged"/>
		/// </summary>
		/// <param name="e"></param>
		protected override void OnParentChanged(EventArgs e)
		{
			base.OnParentChanged(e);

			if (Parent != null)
			{
				if (_readOnlyControlType == ReadOnlyControlType.Label)
				{
					AddLabel();
				}
				else
				{
					AddTextbox();
				}
			}

			_readOnlyControl.Parent = this.Parent;
		}

		/// <summary>
		/// This member overrides <see cref="ReadOnlyComboBox.OnSelectedIndexChanged"/>.
		/// </summary>
		protected override void OnSelectedIndexChanged(EventArgs e)
		{
			base.OnSelectedIndexChanged(e);

			if (this.SelectedItem == null)
			{
				_readOnlyControl.Text = "";
			}
			else
			{
				_readOnlyControl.Text = this.SelectedItem.ToString();
			}
		}

		/// <summary>
		/// This member overrides <see cref="ReadOnlyComboBox.OnDropDownStyleChanged"/>.
		/// </summary>
		protected override void OnDropDownStyleChanged(EventArgs e)
		{
			base.OnDropDownStyleChanged(e);
			_readOnlyControl.Text = this.Text;
		}

		/// <summary>
		/// This member overrides <see cref="ReadOnlyComboBox.OnFontChanged"/>.
		/// </summary>
		/// <param name="e"></param>
		protected override void OnFontChanged(EventArgs e)
		{
			base.OnFontChanged(e);
			_readOnlyControl.Font = this.Font;
		}

		/// <summary>
		/// This member overrides <see cref="ReadOnlyComboBox.OnResize"/>.
		/// </summary>
		/// <param name="e"></param>
		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);
			_readOnlyControl.Size = this.Size;
		}

		/// <summary>
		/// This member overrides <see cref="Control.OnDockChanged"/>.
		/// </summary>
		/// <param name="e"></param>
		protected override void OnDockChanged(EventArgs e)
		{
			base.OnDockChanged(e);
			_readOnlyControl.Dock = this.Dock;
		}

		/// <summary>
		/// This member overrides <see cref="Control.OnEnabledChanged"/>.
		/// </summary>
		/// <param name="e"></param>
		protected override void OnEnabledChanged(EventArgs e)
		{
			base.OnEnabledChanged(e);
			ShowControl();
		}

		/// <summary>
		/// This member overrides <see cref="Control.OnRightToLeftChanged"/>.
		/// </summary>
		/// <param name="e"></param>
		protected override void OnRightToLeftChanged(EventArgs e)
		{
			base.OnRightToLeftChanged(e);
			_readOnlyControl.RightToLeft = this.RightToLeft;
		}

		/// <summary>
		/// This member overrides <see cref="Control.OnTextChanged"/>.
		/// </summary>
		/// <param name="e"></param>
		protected override void OnTextChanged(EventArgs e)
		{
			base.OnTextChanged(e);
			_readOnlyControl.Text = this.Text;
		}

		/// <summary>
		/// This member overrides <see cref="Control.OnLocationChanged"/>.
		/// </summary>
		/// <param name="e"></param>
		protected override void OnLocationChanged(EventArgs e)
		{
			base.OnLocationChanged(e);
			_readOnlyControl.Location = this.Location;
		}

		/// <summary>
		/// This member overrides <see cref="Control.OnTabIndexChanged"/>.
		/// </summary>
		/// <param name="e"></param>
		protected override void OnTabIndexChanged(EventArgs e)
		{
			base.OnTabIndexChanged(e);
			_readOnlyControl.TabIndex = this.TabIndex;
		}

		#endregion
	}

	/// <summary>
	/// Defines the type used on Evolves custom controls for when the ReadOnly property is set
	/// </summary>
	public enum ReadOnlyControlType
	{
		Label,
		TextBox
	}
}

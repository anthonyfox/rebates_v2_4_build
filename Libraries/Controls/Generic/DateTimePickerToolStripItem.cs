﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Collections;
using System.Runtime.CompilerServices;

namespace Evolve.Libraries.Controls.Generic
{
	public class DateTimePickerToolStripItem : ToolStripControlHost
	{
		private FlowLayoutPanel _controlPanel;
		private DateTimePicker _dateTimePicker = new DateTimePicker();

		public delegate void ValueChangedEventHandler(object sender, EventArgs e);

		private Hashtable _delegateStore = new Hashtable();
		private static Object _valueChangedEventKey = new Object();

		public event ValueChangedEventHandler ValueChanged
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			add
			{
				_delegateStore[_valueChangedEventKey] = Delegate.Combine((Delegate) _delegateStore[_valueChangedEventKey], value);
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			remove
			{
				_delegateStore[_valueChangedEventKey] = Delegate.Remove((Delegate) _delegateStore[_valueChangedEventKey], value);
			}
		}

		protected void OnValueChanged()
		{
			ValueChangedEventHandler valueChangedEventHandler = (ValueChangedEventHandler) _delegateStore[_valueChangedEventKey];

			if (valueChangedEventHandler != null)
			{
				valueChangedEventHandler(this, null);
			}
		}

		public DateTimePickerToolStripItem()
			: base(new FlowLayoutPanel())
		{
			// Set up the FlowLayouPanel.
			_controlPanel = (FlowLayoutPanel) base.Control;
			_controlPanel.BackColor = Color.Transparent;

			// Add two child controls.
			_controlPanel.AutoSize = true;
			_controlPanel.Controls.Add(_dateTimePicker);
		}

		public DateTime Value
		{
			get { return _dateTimePicker.Value; }
			set { _dateTimePicker.Value = value; }
		}

		protected override void OnSubscribeControlEvents(Control control)
		{
			base.OnSubscribeControlEvents(control);
			_dateTimePicker.ValueChanged += new EventHandler(ValueChangedInternal);
		}

		protected override void OnUnsubscribeControlEvents(Control control)
		{
			base.OnUnsubscribeControlEvents(control);
			_dateTimePicker.ValueChanged -= new EventHandler(ValueChangedInternal);
		}

		private void ValueChangedInternal(object sender, EventArgs e)
		{
			OnValueChanged();
		}
	}
}

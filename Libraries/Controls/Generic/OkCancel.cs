﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace Evolve.Libraries.Controls.Generic
{
	public partial class OkCancel : UserControl
	{
		public delegate void ButtonClickHandler(object sender, EventArgs e);

		private Hashtable _delegateStore = new Hashtable();
		private DialogResult _dialogResult = DialogResult.None;
		private static Object _buttonClickEventKey = new Object();

		public DialogResult DialogResult
		{
			get { return _dialogResult; }
		}

		public event ButtonClickHandler ButtonClick
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			add
			{
				_delegateStore[_buttonClickEventKey] = Delegate.Combine((Delegate) _delegateStore[_buttonClickEventKey], value);
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			remove
			{
				_delegateStore[_buttonClickEventKey] = Delegate.Remove((Delegate) _delegateStore[_buttonClickEventKey], value);
			}
		}

		protected void OnButtonClick()
		{
			ButtonClickHandler buttonClickHandler = (ButtonClickHandler) _delegateStore[_buttonClickEventKey];

			if (buttonClickHandler != null)
			{
				buttonClickHandler(this, null);
			}
		}

		public OkCancel()
		{
			InitializeComponent();

			this.TabStop = true;
			this.CausesValidation = false;
			btnOk.CausesValidation = true;
			btnCancel.CausesValidation = false;
		}

		private void btnOk_Click(object sender, EventArgs e)
		{
			_dialogResult = DialogResult.OK;
			OnButtonClick();
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			_dialogResult = DialogResult.Cancel;
			OnButtonClick();
		}
	}
}

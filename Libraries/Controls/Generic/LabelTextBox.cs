using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Evolve.Libraries.Controls.Generic
{
	public partial class LabelTextBox : UserControl
	{
		private Int32 _controlSpacing = Constants.Control.Spacing;

		public LabelTextBox()
		{
			InitializeComponent();
		}

		[Category("Appearance")]
		public String LabelText
		{
			get { return _label.Text; }
			set { _label.Text = value; InitializeControl(); }
		}

        [Category("Appearance")]
        public override String Text
        {
            get { return _textBox.Text; }
            set { _textBox.Text = value; }
        }

		[Category("Appearance")]
		public Int32 TextBoxWidth
		{
			get { return _textBox.Width; }
			set { _textBox.Width = value; InitializeControl();}
		}

		[Category("Appearance")]
		public Int32 ControlSpacing
		{
			get { return _controlSpacing; }
			set { _controlSpacing = value; InitializeControl(); }
		}

		public new Boolean Enabled
		{
			get { return _textBox.Enabled; }
			set { _textBox.Enabled = value; }
		}

		[Category("Layout")]
		public Point TextBoxLocation
		{
			get 
			{
				Point point = this.Location;

				point.X += _textBox.Location.X;
				point.Y += _textBox.Location.Y;

				return point;
			}
		}

		[Category("Behavior")]
		public Boolean ReadOnly
		{
			get { return _textBox.ReadOnly; }
			set { _textBox.ReadOnly = value; }
		}

		[Browsable(false)]
		public TextBox TextBox
		{
			get { return _textBox; }
			set { _textBox = value; }
		}

		[Browsable(false)]
		public Label Label
		{
			get { return _label; }
			set { _label = value; }
		}

		[Browsable(false)]
		public ErrorProvider ErrorProvider
		{
			get { return _errorProvider; }
			set { _errorProvider = value; }
		}

		public void InitializeControl()
		{
			if (_label.Visible)
			{
				// Resize controls based on the label size
				Point point = _textBox.Location;
				point.X = _label.Location.X + _label.Size.Width + _controlSpacing;
				_textBox.Location = point;
				_textBox.Invalidate();
			}
			else
			{
				_textBox.Location = _label.Location;
			}
		}

	}
}

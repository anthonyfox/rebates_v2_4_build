﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace Evolve.Libraries.Controls.Generic
{
	public delegate void CheckBoxClickedHandler(bool state);

	public class DataGridViewCheckBoxHeaderCellEventArgs : EventArgs
	{
		private bool _isChecked;

		public bool Checked
		{
			get { return _isChecked; }
		}

		public DataGridViewCheckBoxHeaderCellEventArgs(bool isChecked)
		{
			_isChecked = isChecked;
		}
	}

	public class DataGridViewCheckBoxHeaderCell : DataGridViewColumnHeaderCell
	{
		private Point _checkBoxLocation;
		private Size _checkBoxSize;
		private bool _checked = false;
		private Point _cellLocation = new Point();

		public event CheckBoxClickedHandler OnCheckBoxClicked;

		public DataGridViewCheckBoxHeaderCell()
		{
		}

		public DataGridViewCheckBoxHeaderCell(bool isChecked)
		{
			_checked = isChecked;
		}

		protected override void Paint(Graphics graphics, 
									  Rectangle clipBounds,
									  Rectangle cellBounds,
									  int rowIndex,
									  DataGridViewElementStates dataGridViewElementState,
									  object value,
									  object formattedValue,
									  string errorText,
								  	  DataGridViewCellStyle cellStyle,
									  DataGridViewAdvancedBorderStyle advancedBorderStyle,
									  DataGridViewPaintParts paintParts)
		{
			value = "";
			formattedValue = "";
			base.Paint(graphics, clipBounds, cellBounds, rowIndex, dataGridViewElementState, value,	formattedValue, errorText, cellStyle, advancedBorderStyle, paintParts);

			Size s = CheckBoxRenderer.GetGlyphSize(graphics, CheckBoxState.UncheckedNormal);
			Point p = new Point();
			p.X = cellBounds.Location.X + (cellBounds.Width / 2) - (s.Width / 2);
			p.Y = cellBounds.Location.Y + (cellBounds.Height / 2) - (s.Height / 2);
			
			_cellLocation = cellBounds.Location;
			_checkBoxLocation = p;
			_checkBoxSize = s;

			CheckBoxRenderer.DrawCheckBox(graphics, _checkBoxLocation, (_checked ? CheckBoxState.CheckedNormal : CheckBoxState.UncheckedNormal));
		}


		protected override void OnMouseClick(DataGridViewCellMouseEventArgs e)
		{
			Point p = new Point(e.X + _cellLocation.X, e.Y + _cellLocation.Y);

			if (p.X >= _checkBoxLocation.X && p.X <= _checkBoxLocation.X + _checkBoxSize.Width	&& p.Y >= _checkBoxLocation.Y && p.Y <= _checkBoxLocation.Y + _checkBoxSize.Height)
			{
				_checked = !_checked;

				if (OnCheckBoxClicked != null)
				{
					OnCheckBoxClicked(_checked);
					this.DataGridView.InvalidateCell(this);
				}
			}

			base.OnMouseClick(e);
		}
	}
}
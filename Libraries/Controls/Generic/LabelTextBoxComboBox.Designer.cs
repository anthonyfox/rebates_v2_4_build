namespace Evolve.Libraries.Controls.Generic
{
	partial class LabelTextBoxComboBox
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this._label = new System.Windows.Forms.Label();
			this._comboBox = new Evolve.Libraries.Controls.Generic.ComboBoxWithReadOnly();
			this._textBox = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// _label
			// 
			this._label.AutoSize = true;
			this._label.Location = new System.Drawing.Point(3, 4);
			this._label.Name = "_label";
			this._label.Size = new System.Drawing.Size(33, 13);
			this._label.TabIndex = 0;
			this._label.Text = "Label";
			this._label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// _comboBox
			// 
			this._comboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
			this._comboBox.FormattingEnabled = true;
			this._comboBox.Location = new System.Drawing.Point(100, 1);
			this._comboBox.Name = "_comboBox";
			this._comboBox.ReadOnlyControl = Evolve.Libraries.Controls.Generic.ReadOnlyControlType.Label;
			this._comboBox.Size = new System.Drawing.Size(279, 21);
			this._comboBox.TabIndex = 2;
			this._comboBox.SelectedIndexChanged += new System.EventHandler(this.comboBox_SelectedIndexChanged);
			// 
			// _textBox
			// 
			this._textBox.Location = new System.Drawing.Point(42, 1);
			this._textBox.Name = "_textBox";
			this._textBox.Size = new System.Drawing.Size(52, 20);
			this._textBox.TabIndex = 1;
			this._textBox.Leave += new System.EventHandler(this.textBox_Leave);
			// 
			// LabelTextBoxComboBox
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this._textBox);
			this.Controls.Add(this._comboBox);
			this.Controls.Add(this._label);
			this.Name = "LabelTextBoxComboBox";
			this.Size = new System.Drawing.Size(681, 22);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label _label;
		private Evolve.Libraries.Controls.Generic.ComboBoxWithReadOnly _comboBox;
		private System.Windows.Forms.TextBox _textBox;
	}
}

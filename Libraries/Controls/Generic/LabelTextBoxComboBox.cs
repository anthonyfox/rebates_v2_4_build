using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace Evolve.Libraries.Controls.Generic
{
	public partial class LabelTextBoxComboBox : UserControl
	{
		private Int32 _controlSpacing = Constants.Control.Spacing;
		private LabelTextBoxComboBox _alignComboBoxToControl;
		private LicenseUsageMode _usageMode;

		public new Boolean Enabled
		{
			get { return _comboBox.Enabled; }
			set { _textBox.Enabled = value; _comboBox.Enabled = value; }
		}

		[Browsable(false)]
		public LicenseUsageMode UsageMode
		{
			get { return _usageMode; }
		}

		[Browsable(false)]
		public Label Label
		{
			get { return _label; }
			set { _label = value; }
		}

		[Browsable(false)]
		public TextBox TextBox
		{
			get { return _textBox; }
			set { _textBox = value; }
		}

		[Browsable(false)]
		public ComboBoxWithReadOnly ComboBox
		{
			get { return _comboBox; }
			set { _comboBox = value; }
		}

		[Category("Layout")]
		public Point TextBoxLocation
		{
			get
			{
				Point point = this.Location;

				point.X += _textBox.Location.X;
				point.Y += _textBox.Location.Y;

				return point;
			}
		}

		[Category("Layout")]
		public Point ComboBoxLocation
		{
			get
			{
				Point point = this.Location;

				point.X += _comboBox.Location.X;
				point.Y += _comboBox.Location.Y;

				return point;
			}
		}

		[Category("Layout")]
		public LabelTextBoxComboBox AlignComboBoxToControl
		{
			get	{ return _alignComboBoxToControl; }
			set	{ _alignComboBoxToControl = value;	InitializeControl(); }
		}

		[Category("Appearance")]
		public Int32 ControlSpacing
		{
			get { return _controlSpacing; }
			set { _controlSpacing = value; }
		}

		[Category("Appearance")]
		public String LabelText
		{
			get { return _label.Text; }
			set { _label.Text = value; InitializeControl(); }
		}

		[Category("Appearance")]
		public Int32 TextBoxWidth
		{
			get { return _textBox.Width; }
			set { _textBox.Width = value; InitializeControl(); }
		}

		[Category("Appearance")]
		public Int32 ComboBoxWidth
		{
			get { return _comboBox.Width; }
			set { _comboBox.Width = value; InitializeControl(); }
		}

		[Category("Behavior")]
		public Boolean ReadOnly
		{
			get { return _textBox.ReadOnly; }
			set { _textBox.ReadOnly = value; _comboBox.ReadOnly = value; }
		}

		public LabelTextBoxComboBox()
		{
			// Store the LicenseManagerUsageMode as this only seems to be reliable in the constructor
			_usageMode = LicenseManager.UsageMode;
			InitializeComponent();
		}

		public void InitializeControl()
		{
			// Re-position controls based on the label size
			Point point = _textBox.Location;
			point.X = _label.Location.X + _label.Size.Width + _controlSpacing;
			_textBox.Location = point;

			point = _comboBox.Location;

			if (_alignComboBoxToControl == null)
			{
				point.X = _textBox.Location.X + _textBox.Size.Width + _controlSpacing;
			}
			else
			{
				point.X = _alignComboBoxToControl.ComboBoxLocation.X - this.Location.X;
			}

			_comboBox.Location = point;

			if (_comboBox.SelectedValue == null)
			{
				_textBox.Text = "";
			}
			else
			{
				_textBox.Text = _comboBox.SelectedValue.ToString();
			}
		}

		private void comboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (_comboBox.SelectedValue == null)
			{
				_textBox.Text = "";
			}
			else
			{
				_textBox.Text = _comboBox.SelectedValue.ToString();
			}
		}

		private void textBox_Leave(object sender, EventArgs e)
		{
			if (_textBox.Text.Length == 0)
			{
				// User entered blank, set the value to current selection
				if (_comboBox.Items.Count > 0 && _comboBox.SelectedValue != null)
				{
					_textBox.Text = _comboBox.SelectedValue.ToString();
				}
				else
				{
					_textBox.Text = "";
				}
			}
			else
			{
                if (ComboBox.SelectedValue == null || _textBox.Text != _comboBox.SelectedValue.ToString())
				{
					// User has changed the value, check to see if its valid
					Int32 index = 0;
					IEnumerator items = _comboBox.Items.GetEnumerator();

					while (items.MoveNext())
					{
						// Check the current item in the list against selected value
						Object current = items.Current;
						Object value;

						switch (_comboBox.DataSource.GetType().Name)
						{
							case "DataTable":
								value = ((DataRowView) current).Row[_comboBox.ValueMember];
								break;
							default:
								value = current.GetType().GetProperty(_comboBox.ValueMember).GetValue(current, null);
								break;
						}

						if (value != null && _textBox.Text == value.ToString())
						{
							// Match found, changed combobox selected index to the relevant entry
							_comboBox.SelectedIndex = index;
							break;
						}

						index++;
					}

					if (index == _comboBox.Items.Count)
					{
						// Incorrect entry, reset value to original selection
						if (_comboBox.SelectedValue == null)
						{
							_textBox.Text = "";
						}
						else
						{
							_textBox.Text = _comboBox.SelectedValue.ToString();
						}
					}
				}
			}
		}
	}
}

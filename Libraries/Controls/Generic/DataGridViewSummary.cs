﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace Evolve.Libraries.Controls.Generic
{
	public partial class DataGridViewSummary : DataGridView, ISupportInitialize
	{
		#region Browsable Properties

		/// <summary>
		/// If true a row header at the left side 
		/// of the summary boxes is displayed.
		/// </summary>
		private Boolean _displaySumRowHeader;

		[Browsable(true)]
		[Category("Summary")]
		public Boolean DisplaySumRowHeader
		{
			get { return _displaySumRowHeader; }
			set { _displaySumRowHeader = value; }
		}

		/// <summary>
		/// Text displayed in the row header
		/// of the summary row.
		/// </summary>
		private String _sumRowHeaderText;

		[Browsable(true)]
		[Category("Summary")]
		public String SumRowHeaderText
		{
			get { return _sumRowHeaderText; }
			set { _sumRowHeaderText = value; }
		}

		/// <summary>
		/// Text displayed in the row header
		/// of the summary row.
		/// </summary>
		private Boolean _sumRowHeaderTextBold;

		[Browsable(true)]
		[Category("Summary")]
		public Boolean SumRowHeaderTextBold
		{
			get { return _sumRowHeaderTextBold; }
			set { _sumRowHeaderTextBold = value; }
		}

		/// <summary>
		/// Add columns to sum up in text form
		/// </summary>
		private String[] _summaryColumns;

		[Browsable(true)]
		[Category("Summary")]
		public String[] SummaryColumns
		{
			get { return _summaryColumns; }
			set { _summaryColumns = value; }
		}

		/// <summary>
		/// Display the summary Row
		/// </summary>
		private Boolean _summaryRowVisible;

		[Browsable(true)]
		[Category("Summary")]
		public Boolean SummaryRowVisible
		{
			get
			{
				return _summaryRowVisible;
			}
			set
			{
				_summaryRowVisible = value;

				if (_summaryControl != null && _spacePanel != null)
				{
					_summaryControl.Visible = value;
					_spacePanel.Visible = value;
				}
			}
		}

		private Int32 _summaryRowSpace = 0;

		[Browsable(true)]
		[Category("Summary")]
		public Int32 SummaryRowSpace
		{
			get { return _summaryRowSpace; }
			set { _summaryRowSpace = value; }
		}

		private String _formatString = "F02";

		[Browsable(true)]
		[Category("Summary")]
		[DefaultValue("F02")]
		public String FormatString
		{
			get { return _formatString; }
			set { _formatString = value; }
		}

		[Browsable(true)]
		[Category("Summary")]
		public Color SummaryRowBackColor
		{
			get { return _summaryControl.SummaryRowBackColor; }
			set { _summaryControl.SummaryRowBackColor = value; }
		}

		/// <summary>
		/// advoid user from setting the scrollbars manually
		/// </summary>
		[Browsable(false)]
		public new ScrollBars ScrollBars
		{
			get { return base.ScrollBars; }
			set { base.ScrollBars = value; }
		}

		#endregion

		#region Declare variables

		public event EventHandler CreateSummary;

		private HScrollBar _hScrollBar;
		private SummaryControlContainer _summaryControl;
		private Panel _panel;
		private Panel _spacePanel;
		private TextBox _refBox;

		#endregion

		#region Constructor

		public DataGridViewSummary()
		{
			InitializeComponent();

			_refBox = new TextBox();
			_panel = new Panel();
			_spacePanel = new Panel();
			_hScrollBar = new HScrollBar();

			_summaryControl = new SummaryControlContainer(this);
			_summaryControl.VisibilityChanged += new EventHandler(_summaryControl_VisibilityChanged);

			Resize += new EventHandler(DataGridControlSum_Resize);
			RowHeadersWidthChanged += new EventHandler(DataGridControlSum_Resize);
			ColumnAdded += new DataGridViewColumnEventHandler(DataGridControlSum_ColumnAdded);
			ColumnRemoved += new DataGridViewColumnEventHandler(DataGridControlSum_ColumnRemoved);

			_hScrollBar.Scroll += new ScrollEventHandler(_hScrollBar_Scroll);
			_hScrollBar.VisibleChanged += new EventHandler(_hScrollBar_VisibleChanged);

			_hScrollBar.Top += _summaryControl.Bottom;
			_hScrollBar.Minimum = 0;
			_hScrollBar.Maximum = 0;
			_hScrollBar.Value = 0;
		}

		#endregion

		#region Public Functions

		/// <summary>
		/// Refresh the summary
		/// </summary>
		public void RefreshSummary()
		{
			if (this._summaryControl != null)
			{
				this._summaryControl.RefreshSummary();
			}
		}

		#endregion

		#region Calculate Columns and Scrollbars Width

		private void DataGridControlSum_ColumnRemoved(object sender, DataGridViewColumnEventArgs e)
		{
			CalculateColumnsWidth();
			_summaryControl.Width = _columnsWidth;
			_hScrollBar.Maximum = Convert.ToInt32(_columnsWidth);
			ResizeHScrollBar();
		}

		private void DataGridControlSum_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
		{
			CalculateColumnsWidth();
			_summaryControl.Width = _columnsWidth;
			_hScrollBar.Maximum = Convert.ToInt32(_columnsWidth);
			ResizeHScrollBar();
		}

		Int32 _columnsWidth = 0;

		/// <summary>
		/// Calculate the width of all visible columns
		/// </summary>
		private void CalculateColumnsWidth()
		{
			_columnsWidth = 0;

			for (Int32 index = 0; index < this.ColumnCount; index++)
			{
				if (Columns[index].Visible)
				{
					if (Columns[index].AutoSizeMode == DataGridViewAutoSizeColumnMode.Fill)
					{
						_columnsWidth += Columns[index].MinimumWidth;
					}
					else
					{
						_columnsWidth += Columns[index].Width;
					}
				}
			}
		}

		#endregion

		#region Other Events and delegates

		/// <summary>
		/// Moves viewable area of DataGridView according to the position of the scrollbar
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void _hScrollBar_Scroll(object sender, ScrollEventArgs e)
		{
			Int32 position = Convert.ToInt32((Convert.ToDouble(e.NewValue) / (Convert.ToDouble(_hScrollBar.Maximum) / Convert.ToDouble(Columns.Count))));

			while (!Columns[position].Visible)
			{
				position++;
			}

			if (position < Columns.Count)
			{
				FirstDisplayedScrollingColumnIndex = position;
			}
		}

		public void CreateSummaryRow()
		{
			OnCreateSummary(this, EventArgs.Empty);
		}

		/// <summary>
		/// Calls the CreateSummary event
		/// </summary>
		private void OnCreateSummary(object sender, EventArgs e)
		{
			if (CreateSummary != null)
				CreateSummary(sender, e);
		}

		#endregion

		#region Adjust summaryControl, scrollbar

		/// <summary>
		/// Position the summaryControl under the
		/// DataGridView
		/// </summary>
		private void AdjustSumControlToGrid()
		{
			if (_summaryControl != null && Parent != null)
			{
				_summaryControl.Top = Height + _summaryRowSpace;
				_summaryControl.Left = Left;
				_summaryControl.Width = Width;
			}
		}

		/// <summary>
		/// Position the hScrollbar under the summaryControl
		/// </summary>
		private void AdjustScrollbarToSummaryControl()
		{
			if (Parent != null)
			{
				_hScrollBar.Top = _refBox.Height + 2;
				_hScrollBar.Width = Width;
				_hScrollBar.Left = Left;

				ResizeHScrollBar();
			}
		}

		/// <summary>
		/// Resizes the horizontal scrollbar acording
		/// to the with of the client size and maximum size of the scrollbar
		/// </summary>
		private void ResizeHScrollBar()
		{
			Int32 vScrollbarWidth = 0;

			if (VerticalScrollBar.Visible)
			{
				vScrollbarWidth = VerticalScrollBar.Width;
			}

			int rowHeaderWith = RowHeadersVisible ? RowHeadersWidth : 0;

			if (_columnsWidth > 0)
			{
				// This is neccessary if AutoGenerateColumns = true because DataGridControlSum_ColumnAdded won't be fired
				if (_hScrollBar.Maximum == 0)
				{
					_hScrollBar.Maximum = _columnsWidth;
				}

				// Calculate how much of the columns are visible in %
				Int32 scrollBarWidth = Convert.ToInt32(Convert.ToDouble(ClientSize.Width - RowHeadersWidth - vScrollbarWidth) / (Convert.ToDouble(_hScrollBar.Maximum) / 100F));

				if (scrollBarWidth > 100 || _columnsWidth + rowHeaderWith < ClientSize.Width)
				{
					if (_hScrollBar.Visible)
					{
						_hScrollBar.Visible = false;
					}
				}
				else if (scrollBarWidth > 0)
				{
					if (!_hScrollBar.Visible)
					{
						_hScrollBar.Visible = true;
					}

					_hScrollBar.LargeChange = _hScrollBar.Maximum / 100 * scrollBarWidth;
					_hScrollBar.SmallChange = _hScrollBar.LargeChange / 5;
				}
			}
		}

		private void DataGridControlSum_Resize(object sender, EventArgs e)
		{
			if (Parent != null)
			{
				CalculateColumnsWidth();
				ResizeHScrollBar();
				AdjustSumControlToGrid();
				AdjustScrollbarToSummaryControl();
			}
		}

		/// <summary>
		/// Recalculate the width of the summary control according to 
		/// the state of the scrollbar
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void _hScrollBar_VisibleChanged(object sender, EventArgs e)
		{
			if (Parent != null)
			{
				// Only perform operation if parent is visible
				if (Parent.Visible)
				{
					Int32 height;

					if (_hScrollBar.Visible)
					{
						height = _summaryControl.InitialHeight + _hScrollBar.Height;
					}
					else
					{
						height = _summaryControl.InitialHeight;
					}

					if (_summaryControl.Height != height && _summaryControl.Visible)
					{
						_summaryControl.Height = height;
						this.Height = _panel.Height - _summaryControl.Height - _summaryRowSpace;
					}
				}
			}
		}

		/// <summary>
		/// Recalculate the height of the DataGridView
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void _summaryControl_VisibilityChanged(object sender, EventArgs e)
		{
			if (!_summaryControl.Visible)
			{
				this.ScrollBars = ScrollBars.Both;
				this.Height = _panel.Height;
			}
			else
			{
				this.Height = _panel.Height - _summaryControl.Height - _summaryRowSpace;
				this.ScrollBars = ScrollBars.Vertical;
			}
		}

		/// <summary>
		/// When the DataGridView is visible for the first time a panel is created.
		/// The DataGridView is then removed from the parent control and added as 
		/// child to the newly created panel
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ChangeParent()
		{
			if (!DesignMode && Parent != null)
			{
				_summaryControl.InitialHeight = this._refBox.Height;
				_summaryControl.Height = _summaryControl.InitialHeight;
				_summaryControl.BackColor = this.BackgroundColor;
				_summaryControl.ForeColor = Color.Transparent;
				_summaryControl.RightToLeft = this.RightToLeft;
				_panel.Bounds = this.Bounds;
				_panel.BackColor = this.BackgroundColor;

				_panel.Dock = this.Dock;
				_panel.Anchor = this.Anchor;
				_panel.Padding = this.Padding;
				_panel.Margin = this.Margin;
				_panel.Top = this.Top;
				_panel.Left = this.Left;
				_panel.BorderStyle = this.BorderStyle;

				Margin = new Padding(0);
				Padding = new Padding(0);
				Top = 0;
				Left = 0;

				_summaryControl.Dock = DockStyle.Bottom;
				this.Dock = DockStyle.Fill;

				if (this.Parent is TableLayoutPanel)
				{
					int rowSpan, colSpan;

					TableLayoutPanel tlp = this.Parent as TableLayoutPanel;
					TableLayoutPanelCellPosition cellPos = tlp.GetCellPosition(this);

					rowSpan = tlp.GetRowSpan(this);
					colSpan = tlp.GetColumnSpan(this);

					tlp.Controls.Remove(this);
					tlp.Controls.Add(_panel, cellPos.Column, cellPos.Row);
					tlp.SetRowSpan(_panel, rowSpan);
					tlp.SetColumnSpan(_panel, colSpan);
				}
				else
				{
					Control parent = this.Parent;

					// Remove DataGridView from ParentControls
					parent.Controls.Remove(this);
					parent.Controls.Add(_panel);
				}

				this.BorderStyle = BorderStyle.None;

				_panel.BringToFront();


				_hScrollBar.Top = _refBox.Height + 2;
				_hScrollBar.Width = this.Width;
				_hScrollBar.Left = this.Left;

				_summaryControl.Controls.Add(_hScrollBar);
				_hScrollBar.BringToFront();
				_panel.Controls.Add(this);

				_spacePanel = new Panel();
				_spacePanel.BackColor = _panel.BackColor;
				_spacePanel.Height = _summaryRowSpace;
				_spacePanel.Dock = DockStyle.Bottom;

				_panel.Controls.Add(_spacePanel);
				_panel.Controls.Add(_summaryControl);

				ResizeHScrollBar();
				AdjustSumControlToGrid();
				AdjustScrollbarToSummaryControl();

				ResizeHScrollBar();
			}
		}

		#endregion

		#region ISupportInitialzie

		public void BeginInit()
		{
		}

		public void EndInit()
		{
			ChangeParent();
		}

		#endregion
	}
}

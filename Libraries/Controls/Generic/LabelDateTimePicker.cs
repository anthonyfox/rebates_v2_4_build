using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Evolve.Libraries.Controls.Generic
{
	public partial class LabelDateTimePicker : UserControl
	{
		private Int32 _controlSpacing = Constants.Control.Spacing;

		public LabelDateTimePicker()
		{
			InitializeComponent();
		}

		[Category("Appearance")]
		public String LabelText
		{
			get { return _label.Text; }
			set { _label.Text = value; InitializeControl(); }
		}

		[Category("Appearance")]
		public Int32 DateTimePickerWidth
		{
			get { return _dateTimePicker.Width; }
			set { _dateTimePicker.Width = value; InitializeControl();}
		}

		[Category("Appearance")]
		public Int32 ControlSpacing
		{
			get { return _controlSpacing; }
			set { _controlSpacing = value; InitializeControl(); }
		}

		[Browsable(false)]
		public Label Label
		{
			get { return _label; }
			set { _label = value; }
		}

		[Browsable(false)]
		public DateTimePicker DateTimePicker
		{
			get { return _dateTimePicker; }
			set { _dateTimePicker = value; }
		}

		[Browsable(false)]
		public ErrorProvider ErrorProvider
		{
			get { return _errorProvider; }
			set { _errorProvider = value; }
		}

		[Category("Behavior")]
		public Boolean ReadOnly
		{
			get { return !_dateTimePicker.Enabled; }
			set { _dateTimePicker.Enabled = !value; }
		}

		public void InitializeControl()
		{
			// Resize controls based on the label size
			Point point = _dateTimePicker.Location;
			point.X = _label.Location.X + _label.Size.Width + _controlSpacing;
			_dateTimePicker.Location = point;
		}

	}
}

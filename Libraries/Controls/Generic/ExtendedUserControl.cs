﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Evolve.Libraries.Controls.Generic
{
	public partial class ExtendedUserControl : UserControl
	{
		#region Properties
		public bool IsInDesignMode
		{
			get { return this.DesignMode; }
		}

		public bool IsInRuntimeMode
		{
			get { return !IsInDesignMode; }
		}
		#endregion

		public ExtendedUserControl()
		{
			InitializeComponent();
		}
	}
}

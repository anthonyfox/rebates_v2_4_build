namespace Evolve.Libraries.Controls.Generic
{
	partial class LabelTextBox
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this._label = new System.Windows.Forms.Label();
			this._textBox = new System.Windows.Forms.TextBox();
			this._errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
			((System.ComponentModel.ISupportInitialize) (this._errorProvider)).BeginInit();
			this.SuspendLayout();
			// 
			// _label
			// 
			this._label.AutoSize = true;
			this._label.Location = new System.Drawing.Point(4, 3);
			this._label.Name = "_label";
			this._label.Size = new System.Drawing.Size(33, 13);
			this._label.TabIndex = 0;
			this._label.Text = "Label";
			// 
			// _textBox
			// 
			this._textBox.Location = new System.Drawing.Point(43, 0);
			this._textBox.Name = "_textBox";
			this._textBox.Size = new System.Drawing.Size(100, 20);
			this._textBox.TabIndex = 1;
			// 
			// _errorProvider
			// 
			this._errorProvider.ContainerControl = this;
			// 
			// LabelTextBox
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this._textBox);
			this.Controls.Add(this._label);
			this.Name = "LabelTextBox";
			this.Size = new System.Drawing.Size(622, 20);
			((System.ComponentModel.ISupportInitialize) (this._errorProvider)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label _label;
		private System.Windows.Forms.TextBox _textBox;
		private System.Windows.Forms.ErrorProvider _errorProvider;
	}
}

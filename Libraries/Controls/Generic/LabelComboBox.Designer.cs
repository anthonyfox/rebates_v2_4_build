namespace Evolve.Libraries.Controls.Generic
{
	partial class LabelComboBox
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this._label = new System.Windows.Forms.Label();
			this._comboBox = new Evolve.Libraries.Controls.Generic.ComboBoxWithReadOnly();
			this.SuspendLayout();
			// 
			// _label
			// 
			this._label.AutoSize = true;
			this._label.Location = new System.Drawing.Point(3, 3);
			this._label.Name = "_label";
			this._label.Size = new System.Drawing.Size(33, 13);
			this._label.TabIndex = 0;
			this._label.Text = "Label";
			this._label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// _comboBox
			// 
			this._comboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
			this._comboBox.FormattingEnabled = true;
			this._comboBox.Location = new System.Drawing.Point(42, 0);
			this._comboBox.Name = "_comboBox";
			this._comboBox.ReadOnlyControl = Evolve.Libraries.Controls.Generic.ReadOnlyControlType.Label;
			this._comboBox.Size = new System.Drawing.Size(279, 21);
			this._comboBox.TabIndex = 1;
			// 
			// LabelComboBox
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this._comboBox);
			this.Controls.Add(this._label);
			this.Name = "LabelComboBox";
			this.Size = new System.Drawing.Size(681, 21);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label _label;
		private Evolve.Libraries.Controls.Generic.ComboBoxWithReadOnly _comboBox;
	}
}

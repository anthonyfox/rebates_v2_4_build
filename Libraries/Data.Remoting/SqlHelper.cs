using System;
using System.Data;
using System.Text.RegularExpressions;

namespace Evolve.Libraries.Data.Remoting
{
	public static class SqlHelper
	{
		#region General helper methods

		public static String GetNativeTypeDescription(Type type)
		{
			String typeDescription = type.Name;

			if (type.Name.StartsWith("Nullable"))
			{
				String fullName = type.FullName;
				String search = "[[System.";

				fullName = fullName.Remove(0, fullName.IndexOf(search) + search.Length);
				typeDescription = fullName.Remove(fullName.IndexOf(',')) + "Nullable";
			}

			return typeDescription;
		}

		public static String ConditionBuilder(String condition, String append)
		{
			if (condition != null)
			{
				if (condition.ToLower().Contains("where "))
				{
					condition += " and " + append;
				}
				else
				{
					condition += " where " + append;
				}
			}

			return condition;
		}

		public static String FilterBuilder(String filter, String append)
		{
			if (filter != null)
			{
				if (filter.Length > 0)
				{
					filter += " and ";
				}

				filter += append;
			}

			return filter;
		}

		#endregion

		#region DB to .NET data type conversions

		public static String DBDecimalToString(Object dbObject)
		{
			String value = "";

			if (!Object.ReferenceEquals(dbObject, System.DBNull.Value))
			{
				if (dbObject is Decimal)
				{
					value = dbObject.ToString();
				}
			}

			return value;
		}

		public static Int16 DBDecimalToInt16(Object dbObject)
		{
			Int16 value = Int16.MinValue;

			if (!Object.ReferenceEquals(dbObject, System.DBNull.Value))
			{
				if (dbObject is Decimal)
				{
					value = Convert.ToInt16(dbObject);
					Decimal remainder = (Decimal) dbObject - (Decimal) value;

					if (remainder != 0)
					{
						throw new DataException("Decimal to Int16 conversion for non-exact value");
					}
				}
			}

			return value;
		}

		public static Int16? DBDecimalToInt16Nullable(Object dbObject)
		{
			Int16? value = null;

			if (!Object.ReferenceEquals(dbObject, System.DBNull.Value))
			{
				if (dbObject is Decimal)
				{
					value = Convert.ToInt16(dbObject);
					Decimal remainder = (Decimal) dbObject - (Decimal) value;

					if (remainder != 0)
					{
						throw new DataException("Decimal to Int16 conversion for non-exact value");
					}
				}
			}

			return value;
		}

		public static Int32 DBDecimalToInt32(Object dbObject)
		{
			Int32 value = Int32.MinValue;

			if (!Object.ReferenceEquals(dbObject, System.DBNull.Value))
			{
				if (dbObject is Decimal)
				{
					value = Convert.ToInt32(dbObject);
					Decimal remainder = (Decimal) dbObject - (Decimal) value;

					if (remainder != 0)
					{
						throw new DataException("Decimal to Int32 conversion for non-exact value");
					}
				}
			}

			return value;
		}

		public static Int32? DBDecimalToInt32Nullable(Object dbObject)
		{
			Int32? value = null;

			if (!Object.ReferenceEquals(dbObject, System.DBNull.Value))
			{
				if (dbObject is Decimal)
				{
					value = Convert.ToInt32(dbObject);
					Decimal remainder = (Decimal) dbObject - (Decimal) value;

					if (remainder != 0)
					{
						throw new DataException("Decimal to Int32 conversion for non-exact value");
					}
				}
			}

			return value;
		}

		public static Boolean DBDecimalToBoolean(Object dbObject)
		{
			Boolean value = false;

			if (!Object.ReferenceEquals(dbObject, System.DBNull.Value))
			{
				if (dbObject is Decimal)
				{
					value = Convert.ToBoolean(dbObject);
				}
			}

			return value;
		}


		public static String DBStringToString(Object dbObject)
		{
			String value = "";

			if (!Object.ReferenceEquals(dbObject, System.DBNull.Value))
			{
				if (dbObject is String)
				{
					value = dbObject.ToString().TrimEnd();
				}
			}

			return value;
		}

		public static Char DBStringToChar(Object dbObject)
		{
			Char value = Char.MinValue;

			if (!Object.ReferenceEquals(dbObject, System.DBNull.Value) && dbObject is String)
			{
				String stringValue = dbObject.ToString();

				if (stringValue.Length == 1)
				{
					value = stringValue[0];
				}
			}

			return value;
		}

		public static Char? DBStringToCharNullable(Object dbObject)
		{
			Char? value = null;

			if (!Object.ReferenceEquals(dbObject, System.DBNull.Value) && dbObject is String)
			{
				String stringValue = dbObject.ToString();

				if (stringValue.Length == 1)
				{
					value = stringValue[0];
				}
			}

			return value;
		}

		public static Boolean? DBBooleanToBooleanNullable(Object dbObject)
		{
			Boolean? value = null;

			if (!Object.ReferenceEquals(dbObject, System.DBNull.Value))
			{
				if (dbObject is Boolean)
				{
					value = Convert.ToBoolean(dbObject);
				}
			}

			return value;
		}
		
		public static Boolean DBStringToBoolean(Object dbObject)
		{
			Char value = Char.MinValue;
			Boolean booleanValue;

			if (!Object.ReferenceEquals(dbObject, System.DBNull.Value))
			{
				if (dbObject is String)
				{
					String stringValue = dbObject.ToString();

					if (stringValue.Length == 1)
					{
						value = stringValue[0];
					}
				}
			}

			booleanValue = (value.ToString().ToLower() == "y");
			return booleanValue;
		}

		public static Int16 DBInt16ToInt16(Object dbObject)
		{
			if ((!Object.ReferenceEquals(dbObject, System.DBNull.Value)) && dbObject is Int16)
			{
				return Convert.ToInt16(dbObject);
			}
			else
			{
				return Int16.MinValue;
			}
		}

		public static Int16? DBInt16ToInt16Nullable(Object dbObject)
		{
			if ((!Object.ReferenceEquals(dbObject, System.DBNull.Value)) && dbObject is Int16)
			{
				return Convert.ToInt16(dbObject);
			}
			else
			{
				return null;
			}
		}

		public static Boolean DBInt16ToBoolean(Object dbObject)
		{
			if ((!Object.ReferenceEquals(dbObject, System.DBNull.Value)) && dbObject is Int16)
			{
				return Convert.ToBoolean(dbObject);
			}
			else
			{
				return false;
			}
		}

		public static Int32 DBInt32ToInt32(Object dbObject)
		{
			if ((!Object.ReferenceEquals(dbObject, System.DBNull.Value)) && dbObject is Int32)
			{
				return Convert.ToInt32(dbObject);
			}
			else
			{
				return Int32.MinValue;
			}
		}

		public static Int32? DBInt32ToInt32Nullable(Object dbObject)
		{
			if ((!Object.ReferenceEquals(dbObject, System.DBNull.Value)) && dbObject is Int32)
			{
				return Convert.ToInt32(dbObject);
			}
			else
			{
				return null;
			}
		}

		public static String DBInt32ToString(Object dbObject)
		{
			if ((!Object.ReferenceEquals(dbObject, System.DBNull.Value)) && dbObject is Int32)
			{
				return Convert.ToInt32(dbObject).ToString();
			}
			else
			{
				return Int32.MinValue.ToString();
			}
		}

		public static DateTime DBDateTimeToDateTime(Object dbObject)
		{
			if ((!Object.ReferenceEquals(dbObject, System.DBNull.Value)) && dbObject is DateTime)
			{
				return Convert.ToDateTime(dbObject);
			}
			else
			{
				return DateTime.MinValue;
			}
		}

		public static DateTime? DBDateTimeToDateTimeNullable(Object dbObject)
		{
			if ((!Object.ReferenceEquals(dbObject, System.DBNull.Value)) && dbObject is DateTime)
			{
				DateTime dateTime = Convert.ToDateTime(dbObject);

				// Informix DateTime's with no year assume 1200 not 0001 as per .NET so we
				// need to change this to allow all date comparisons to work correctly
				if (dateTime.Year == 1200)
				{
					dateTime = dateTime.AddYears(-1199);
				}

				return dateTime;
			}
			else
			{
				return null;
			}
		}

		public static TimeSpan DBTimeSpanToTimeSpan(Object dbObject)
		{
			if ((!Object.ReferenceEquals(dbObject, System.DBNull.Value)) && dbObject is TimeSpan)
			{
				return (TimeSpan) dbObject;
			}
			else
			{
				return TimeSpan.MinValue;
			}
		}

		public static TimeSpan? DBTimeSpanToTimeSpanNullable(Object dbObject)
		{
			if ((!Object.ReferenceEquals(dbObject, System.DBNull.Value)) && dbObject is TimeSpan)
			{
				return (TimeSpan) dbObject;
			}
			else
			{
				return null;
			}
		}

		public static TimeSpan DBDateTimeToTimeSpan(Object dbObject)
		{
			if ((!Object.ReferenceEquals(dbObject, System.DBNull.Value)) && dbObject is DateTime)
			{
				DateTime dateTime = Convert.ToDateTime(dbObject);
				TimeSpan timeSpan = new TimeSpan(dateTime.Hour, dateTime.Minute, dateTime.Second);

				return timeSpan;
			}
			else
			{
				return new TimeSpan();
			}
		}

		public static TimeSpan? DBDateTimeToTimeSpanNullable(Object dbObject)
		{
			if ((!Object.ReferenceEquals(dbObject, System.DBNull.Value)) && dbObject is DateTime)
			{
				DateTime dateTime = Convert.ToDateTime(dbObject);
				TimeSpan? timeSpan = new TimeSpan(dateTime.Hour, dateTime.Minute, dateTime.Second);

				return timeSpan;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// Compatability method for Informix OLE connections using later that I-Connect 2.80
		/// </summary>
		/// <param name="dbObject">The database value</param>
		/// <returns></returns>
		public static DateTime DBTimeSpanToDateTime(Object dbObject)
		{
			DateTime dateTime = DateTime.MinValue.AddYears(1199);

			if ((!Object.ReferenceEquals(dbObject, System.DBNull.Value)) && dbObject is TimeSpan)
			{
				dateTime = dateTime.Add((TimeSpan) dbObject);
			}

			return dateTime;
		}

		/// <summary>
		/// Compatability method for Informix OLE connections using later that I-Connect 2.80
		/// </summary>
		/// <param name="dbObject">The database value</param>
		/// <returns></returns>
		public static DateTime? DBTimeSpanToDateTimeNullable(Object dbObject)
		{
			DateTime? dateTime = null;

			if ((!Object.ReferenceEquals(dbObject, System.DBNull.Value)) && dbObject is TimeSpan)
			{
				dateTime = DateTime.MinValue.AddYears(1199).Add((TimeSpan) dbObject);
			}

			return dateTime;
		}

		public static Decimal DBDecimalToDecimal(Object dbObject)
		{
			if ((!Object.ReferenceEquals(dbObject, System.DBNull.Value)) && dbObject is Decimal)
			{
				return Convert.ToDecimal(dbObject);
			}
			else
			{
				return Decimal.MinValue;
			}
		}

		public static Decimal? DBDecimalToDecimalNullable(Object dbObject)
		{
			if ((!Object.ReferenceEquals(dbObject, System.DBNull.Value)) && dbObject is Decimal)
			{
				return (Decimal?) Convert.ToDecimal(dbObject);
			}
			else
			{
				return null;
			}
		}

		public static Double DBDecimalToDouble(Object dbObject)
		{
			if ((!Object.ReferenceEquals(dbObject, System.DBNull.Value)) && dbObject is Decimal)
			{
				return Convert.ToDouble(dbObject);
			}
			else
			{
				return Double.MinValue;
			}
		}

		public static Boolean DBBooleanToBoolean(Object dbObject)
		{
			if ((!Object.ReferenceEquals(dbObject, System.DBNull.Value)) && dbObject is Boolean)
			{
				return Convert.ToBoolean(dbObject);
			}
			else
			{
				return false;
			}
		}

		public static TimeSpan DBStringToTimeSpan(Object dbObject)
		{
			TimeSpan timeSpan = TimeSpan.MinValue;

			if ((!Object.ReferenceEquals(dbObject, System.DBNull.Value)) && dbObject is String)
			{
				String value = dbObject.ToString().TrimStart();
				String[] parts = value.Split(new char[] { ' ', ':', '.' });

				Int32 days = 0;
				Int32 hours = 0;
				Int32 minutes = 0;
				Int32 seconds = 0;
				Int32 milliseconds = 0;

				Int32 part = 1;

				if (value.Contains("."))
				{
					// Have milliseconds
					part = 0;
				}
				else
				{
					if (value.Contains(":"))
					{
						String[] colonParts = value.Split(new char[] { ':' });

						if (colonParts.Length == 2)
						{
							// Contains hours and minutes only
							part = 2;
						}
					}
					else
					{
						// Must be days only
						part = 4;
					}
				}

				for (Int32 index = parts.Length - 1; index >= 0; index--)
				{
					switch (part)
					{
						case 0:
							milliseconds = Int32.Parse(parts[index]);
							break;
						case 1:
							seconds = Int32.Parse(parts[index]);
							break;
						case 2:
							minutes = Int32.Parse(parts[index]);
							break;
						case 3:
							hours = Int32.Parse(parts[index]);
							break;
						case 4:
							days = Int32.Parse(parts[index]);
							break;
					}

					part++;
				}

				timeSpan = new TimeSpan(days, hours, minutes, seconds, milliseconds);
			}

			return timeSpan;
		}

		public static DateTime DBStringToDateTime(Object dbObject)
		{
			DateTime dateTime = DateTime.MinValue;

			if ((!Object.ReferenceEquals(dbObject, System.DBNull.Value)) && dbObject is String)
			{
				String value = dbObject.ToString().TrimStart();
				Regex regexDate = new Regex("^(?<month>\\d{1,2})/(?<day>\\d{1,2})/(?<year>\\d{2,4})$");
				Regex regexHourMinute = new Regex("^(?<hour>\\d{1,2}):(?<minute>\\d{1,2})");
				String[] parts = value.Split(new char[] { '/', '-', ' ', ':', '.' });

				if (regexDate.IsMatch(value))
				{
					Int32 year = Int32.Parse(parts[2]);
					Int32 month = Int32.Parse(parts[1]);
					Int32 day = Int32.Parse(parts[0]);

					dateTime = new DateTime(year, month, day, 0, 0, 0, 0);
				}
				else if (regexHourMinute.IsMatch(value))
				{
					Int32 hour = Int32.Parse(parts[0]);
					Int32 minute = (parts.Length >= 2 ? Int32.Parse(parts[1]) : 0);
					Int32 second = (parts.Length >= 3 ? Int32.Parse(parts[2]) : 0);
					Int32 millisecond = (parts.Length >= 4 ? Int32.Parse(parts[3]) : 0);

					dateTime = new DateTime(1, 1, 1, hour, minute, second, millisecond);
				}
				else
				{
					if (parts.Length >= 3)
					{
						Int32 year = Int32.Parse(parts[0]);
						Int32 month = Int32.Parse(parts[1]);
						Int32 day = Int32.Parse(parts[2]);
						Int32 hour = (parts.Length >= 4 ? Int32.Parse(parts[3]) : 0);
						Int32 minute = (parts.Length >= 5 ? Int32.Parse(parts[4]) : 0);
						Int32 second = (parts.Length >= 6 ? Int32.Parse(parts[5]) : 0);
						Int32 millisecond = (parts.Length >= 7 ? Int32.Parse(parts[6]) : 0);

						dateTime = new DateTime(year, month, day, hour, minute, second, millisecond);
					}
				}
			}

			return dateTime;
		}

		public static DateTime? DBStringToDateTimeNullable(Object dbObject)
		{
			DateTime? dateTime = null;

			if (dbObject != DBNull.Value)
			{
				dateTime = DBStringToDateTime(dbObject);
			}

			return dateTime;
		}


		public static TimeSpan? DBStringToTimeSpanNullable(Object dbObject)
		{
			TimeSpan? timeSpan = null;

			if ((!Object.ReferenceEquals(dbObject, System.DBNull.Value)) && dbObject is String)
			{
				String value = dbObject.ToString().TrimStart();

				if (value.Length > 0)
				{
					timeSpan = (TimeSpan?) DBStringToTimeSpan(dbObject);
				}
			}

			return timeSpan;
		}

		#endregion

		#region .NET to DB data type conversions

		public static Object StringToDBString(String s)
		{
			Object dbObject = DBNull.Value;

			if (s != null)
			{
				dbObject = s.Replace("\"", "\"\"");
			}

			return dbObject;
		}

		public static Object CharToDBChar(Char c)
		{
			Object dbObject = DBNull.Value;

			if (c != Char.MinValue)
			{
				dbObject = c;
			}

			return dbObject;
		}

		public static Object CharNullableToDBChar(Char? c)
		{
			Object dbObject = DBNull.Value;

			if (c != null)
			{
				dbObject = c;
			}

			return dbObject;
		}

		public static Object CharToDBString(Char c)
		{
			Object dbObject = DBNull.Value;

			if (c != Char.MinValue)
			{
				dbObject = c;
			}

			return dbObject;
		}

		public static Object BooleanToDBString(Boolean b)
		{
			return (b ? "Y" : "N");
		}

		public static Object Int16ToDBInt16(Int16 i)
		{
			Object dbObject = DBNull.Value;

			if (i != Int16.MinValue)
			{
				dbObject = i;
			}

			return dbObject;
		}

		public static Object Int16NullableToDBInt16(Int16? i)
		{
			Object dbObject = DBNull.Value;

			if (i.HasValue)
			{
				dbObject = i;
			}

			return dbObject;
		}

		public static Object BooleanNullableToDBBoolean(Boolean? b)
		{
			Object dbObject = DBNull.Value;

			if (b.HasValue)
			{
				dbObject = b;
			}

			return dbObject;
		}

		public static Object BooleanToDBInt16(Boolean b)
		{
			return (b ? 1 : 0);
		}

		public static Object Int32ToDBInt32(Int32 i)
		{
			Object dbObject = DBNull.Value;

			if (i != Int32.MinValue)
			{
				dbObject = i;
			}

			return dbObject;
		}

		public static Object Int32NullableToDBInt32(Int32? i)
		{
			Object dbObject = DBNull.Value;

			if (i.HasValue)
			{
				dbObject = i;
			}

			return dbObject;
		}


		public static Object DateTimeToDBDateTime(DateTime dateTime)
		{
			Object dbObject = DBNull.Value;

			if (dateTime != DateTime.MinValue)
			{
				dbObject = dateTime;
			}

			return dbObject;
		}

		public static Object DateTimeNullableToDBDateTime(DateTime? dateTime)
		{
			Object dbObject = DBNull.Value;

			if (dateTime.HasValue)
			{
				dbObject = dateTime;
			}

			return dbObject;
		}

		public static Object TimeSpanToDBTimeSpan(TimeSpan timeSpan)
		{
			Object dbObject = DBNull.Value;

			if (timeSpan != TimeSpan.MinValue)
			{
				dbObject = timeSpan;
			}

			return dbObject;
		}

		public static Object TimeSpanNullableToDBTimeSpan(TimeSpan? timeSpan)
		{
			Object dbObject = DBNull.Value;

			if (timeSpan.HasValue)
			{
				dbObject = timeSpan;
			}

			return dbObject;
		}

		public static Object DecimalToDBDecimal(Decimal d)
		{
			Object dbObject = DBNull.Value;

			if (d != Decimal.MinValue)
			{
				dbObject = d;
			}

			return dbObject;
		}

		public static Object DecimalNullableToDBDecimal(Decimal? d)
		{
			Object dbObject = DBNull.Value;

			if (d.HasValue)
			{
				dbObject = d;
			}

			return dbObject;
		}

		public static Object DoubleToDBDecimal(Double d)
		{
			Object dbObject = DBNull.Value;

			if (d != Double.MinValue)
			{
				dbObject = Convert.ToDecimal(d);
			}

			return dbObject;
		}

		#endregion
	}
}

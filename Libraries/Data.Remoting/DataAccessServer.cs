﻿using System;
using System.Collections;
using System.DirectoryServices.AccountManagement;
using System.Net;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Lifetime;
using System.Runtime.Serialization.Formatters;
using System.Text.RegularExpressions;
using Evolve.Libraries.Utilities;

namespace Evolve.Libraries.Data.Remoting
{
	[Serializable]
	public static class DataAccessServer
	{
		private static Boolean _isDirect = false;
		private static UserCredentials _userCredentials;
		private static String _protocol = "tcp";
		private static String _serverName = "localhost";
		private static String _server = "127.0.0.1";
		private static Int32 _port = 1234;
		private static DataConnection _persistentConnection = null;
		private static Int32 _renewOnCallTimeMilliseconds = 0;
		private static Int32 _sponserRenewTimeMilliseconds = 5000;
		private static Hashtable _linkServers;

		public static Boolean IsDirect
		{
			get { return _isDirect; }
			set { _isDirect = value; }
		}

		public static Boolean IsLocalHost
		{
			get { return _server == "127.0.0.1"; }
		}

		public static UserCredentials UserCredentials
		{
			get { return _userCredentials; }
			set { _userCredentials = value; }
		}

		public static String ServiceReference
		{
			get { return _protocol + "://" + _server + ":" + _port.ToString(); }
		}

		public static String Protocol
		{
			get { return _protocol; }
			set { _protocol = value; }
		}

		public static String ServerName
		{
			get { return _serverName; }
		}

		public static String Server
		{
			get { return _server; }
			
			set 
			{ 
				_serverName = value;
				_server = ResolveIPAddress(value); 
			}
		}

		public static Int32 Port
		{
			get { return _port; }
			set { _port = value; }
		}

		public static Int32 RenewOnCallTimeMilliseconds
		{
			get { return _renewOnCallTimeMilliseconds; }
			set { _renewOnCallTimeMilliseconds = value; }
		}

		public static Int32 SponserRenewTimeMilliseconds
		{
			get { return _sponserRenewTimeMilliseconds; }
			set { _sponserRenewTimeMilliseconds = value; }
		}

		public static Hashtable LinkServers
		{
			get { return _linkServers; }
			set { _linkServers = value; }
		}

		public static DataConnection PersistentConnection
		{
			get { return _persistentConnection; }
		}

		public static void SetSecurity()
		{
			if (_protocol == "tcp")
			{
				// Setup TCP port for call lease renewals of remote objects
				BinaryServerFormatterSinkProvider provider = new BinaryServerFormatterSinkProvider();
				provider.TypeFilterLevel = TypeFilterLevel.Full;

				Hashtable ports = new Hashtable();
				ports.Add("port", 0);

				TcpChannel channel = new TcpChannel(ports, null, provider);
				ChannelServices.RegisterChannel(channel, false);
			}
		}

		public static DataConnection NewConnection()
		{
			return new DataConnection();
		}

		public static void NewPersistentConnection()
		{
			NewPersistentConnection(true);
		}

		public static void NewPersistentConnection(Boolean openConnection)
		{
			SqlResponse sqlResponse = null;

			// Ensure that if we have requested a new persistent connection object that any old ones
			// are closed first (this should not happen, but will handle any poor code which may have occurred)
			if (_persistentConnection != null)
			{
				_persistentConnection.CloseConnection();
				_persistentConnection = null;
			}

			_persistentConnection = new DataConnection(true);

			if (openConnection)
			{
				sqlResponse = _persistentConnection.OpenConnection();

				if (sqlResponse.HasError)
				{
					throw new DatabaseException("Failed to open persistent connection (" + sqlResponse.Message + ")");
				}
			}
		}

		public static void TerminatePersistentConnection()
		{
			if (_persistentConnection != null)
			{
				SqlResponse sqlResponse = _persistentConnection.CloseConnection(false);
				_persistentConnection = null;
			}
		}

		public static IDataAccess NewConnection(Boolean isPersistentConnection)
		{
			IDataAccess dataAccess = null;

			if (_isDirect)
			{
				dataAccess = new DataAccessDirect(_userCredentials);
			}
			else
			{
				String serviceReference = ServiceReference + "/DataAccess";
				IDataAccessFactory factory = (IDataAccessFactory) Activator.GetObject(typeof(IDataAccessFactory), serviceReference);
				Int32 attempts = 0;
				Int32 maxAttempts = 10;
				Int32 waitMilliseconds = 250;

				// Attempt to get DataAccess object from the DAS (Note: A retry mechanisim has been implemented
				// to try and resolve occasional "Requested Service not found" errors, if they still occur we will
				// no also report the service reference in an attempt to track the source of the error)
				while (dataAccess == null)
				{
					attempts++;

					try
					{
						dataAccess = factory.GetNewInstance(_userCredentials, isPersistentConnection);

						if (dataAccess == null)
						{
							throw new ApplicationException("Data Access Factory returned a NULL response (" + _userCredentials.Environment + ")");
						}
					}
					catch (RemotingException remotingException)
					{
						if (remotingException.Message == "Requested Service not found")
						{
							remotingException.HelpLink = "Service Reference: " + serviceReference;
						}

						if (attempts == maxAttempts)
						{
							throw remotingException;
						}
						else
						{
							System.Threading.Thread.Sleep(waitMilliseconds);
						}
					}
				}
			}

			return dataAccess;
		}

		public static IAuthentificateUser AuthentificateUser()
		{
			String serviceReference = ServiceReference + "/AuthentificateUser";
			IAuthentificateUserFactory factory = (IAuthentificateUserFactory) Activator.GetObject(typeof(IAuthentificateUserFactory), serviceReference);
			IAuthentificateUser authentificateUser = null;

			Int32 attempts = 0;
			Int32 maxAttempts = 10;
			Int32 waitMilliseconds = 250;

			// Attempt to get DataAccess object from the DAS (Note: A retry mechanisim has been implemented
			// to try and resolve occasional "Requested Service not found" errors, if they still occur we will
			// no also report the service reference in an attempt to track the source of the error)
			while (authentificateUser == null)
			{
				attempts++;

				try
				{
					authentificateUser = factory.GetNewInstance(UserAuthentification.WindowsUserId);
				}
				catch (RemotingException remotingException)
				{
					if (remotingException.Message == "Requested Service not found")
					{
						remotingException.HelpLink = "Service Reference: " + serviceReference;
					}

					if (attempts == maxAttempts)
					{
						throw remotingException;
					}
					else
					{
						System.Threading.Thread.Sleep(waitMilliseconds);
					}
				}
			}

			return authentificateUser;
		}

		public static void SetLogLevel(LoggerLevel logLevel)
		{
			IDataAccessFactory factory = (IDataAccessFactory) Activator.GetObject(typeof(IDataAccessFactory), ServiceReference + "/DataAccess");
			factory.SetLogLevel(logLevel);
		}

		private static String ResolveIPAddress(String value)
		{
			String ipAddress = "";
			Regex regex = new Regex("[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}");

			if (!regex.IsMatch(value))
			{
				// Passed value is not a valid IP Address format, assume its a host name
				if (value.ToLower() == "localhost")
				{
					// Localhost specified, use loop back address
					ipAddress = "127.0.0.1";
				}
				else
				{
					// Host name, resolve via DNS
					IPHostEntry ipHostEntry = Dns.GetHostEntry(value);

					if (ipHostEntry != null)
					{
						if (ipHostEntry.AddressList.Length > 0)
						{
							ipAddress = ipHostEntry.AddressList[0].ToString();
						}
					}
				}
			}
			else
			{
				ipAddress = value;
			}

			return ipAddress;
		}
	}
}

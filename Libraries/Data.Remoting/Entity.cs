﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using Evolve.Libraries.Utilities;
using System.Threading;

namespace Evolve.Libraries.Data.Remoting
{
	public interface IEntity
	{
		int AcceptChanges();
		void AddReadFilter(string propertyName, string logicalCondition);
		void AddReadFilter(string propertyName, string logicalCondition, object value);
		void AddReadFilter(string sqlCondition);
		void AddReadFilterBetween(string propertyName, object fromValue, object toValue);
		void AddReadFilterIn(string propertyName, System.Collections.Generic.List<object> values);
		int AfterDelete();
		int AfterInsert();
		int AfterUpdate();
		string BaseEntityValuesToString();
		string BaseEntityValuesToString(string seperator);
		int BeforeDelete();
		int BeforeInsert();
		int BeforeUpdate();
		void CleanEntity();
		void ClearAllEntity();
		void ClearEntity();
		void ClearEntityCollection();
		string CurrentDate();
		string CurrentTime();
		string CustomSelect { get; set; }
		string DatabaseTable { get; set; }
		string DBDateText(DateTime value);
		string DBDateText(DateTime value, bool isUnload);
		string DBDateTimeFunction(DBDateTime from, DBDateTime to, string valueText);
		string DBTimeSpanFunction(DBDateTime from, DBDateTime to, string valueText);
		int Delete();
		int DeleteForReadFilter();
		SqlResponse ExecuteScaler(string sql);
		SqlResponse ExecuteSql(string sql);
		bool ExistsOnDatabase { get; set; }
		int? GetColumnWidth(string propertyName);
		int? GetDisplayOrder(string propertyName);
		EntityColumnAttribute GetEntityColumnAttribute(System.Reflection.PropertyInfo propertyInfo);
		string GetIdentityValueSql();
		string GroupBy { get; set; }
		bool IgnoreMissingColumns { get; set; }
		bool IncludeIdentityColumnInInsert();
		int Insert();
		bool IsDirtyColumn(string propertyName);
		bool IsEntityDirty { get; }
		bool IsMarkedForDeletion { get; set; }
		bool IsReadOnlyEntity { get; set; }
		string JoinCondition { get; set; }
		string JoinTableText { get; set; }
		string KeyValuesToString();
		string KeyValuesToString(string seperator);
		string LockSql();
		DataConnection ManualConnection { get; set; }
		string OrderBy { get; set; }
		bool PerformConcurrencyChecks { get; set; }
		event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
		string PropertyValueText(System.Reflection.PropertyInfo propertyInfo);
		string PropertyValueText(System.Reflection.PropertyInfo propertyInfo, object instance);
		string PropertyValueText(System.Reflection.PropertyInfo propertyInfo, object instance, bool isUnload);
		System.ComponentModel.IBindingList Read();
		System.ComponentModel.IBindingList Read(int rowLimit, out bool isRowLimitExceeded);
		System.ComponentModel.IBindingList Read(string condition);
		System.ComponentModel.IBindingList Read(string condition, int rowLimit, out bool isRowLimitExceeded);
		void ReadAllEntity();
		int ReadCount();
		int ReadCount(string condition);
		bool ReadCurrent();
		void ReadEntity();
		void ReadEntityCollection();
		string ReadFilter { get; set; }
		System.ComponentModel.IBindingList ReadWithDirtyProperty();
		System.ComponentModel.IBindingList ReadWithDirtyProperty(bool useReadFilter);
		System.ComponentModel.IBindingList ReadWithSql(string sql, int rowLimit, out bool isRowLimitExceeded);
		void ResetEntitySql();
		string SelectDistinct();
		short? SelectFirstNRecords { get; set; }
		bool SelectUnique { get; set; }
		string SequenceName { get; set; }
		void SetColumnClean();
		void SetColumnClean(string propertyName);
		void SetColumnDirty();
		void SetColumnDirty(string propertyName);
		string SqlCondition(string propertyName, string logicalCondition);
		string SqlConditionBetween(string propertyName, object fromValue, object toValue);
		string SqlConditionIn(string propertyName, System.Collections.Generic.List<object> values);
		EntityUnload Unload();
		EntityUnload Unload(char delimiter);
		int Update();
		int Update(bool updateAllColumns);
		char WildcardCharacter();
		string WildcardKeyword();
	}

	public abstract class Entity : INotifyPropertyChanged, Evolve.Libraries.Data.Remoting.IEntity
	{
		#region Abstract and Virtual Method Declarations for Database Language Specific Requirements

		protected enum EntityJoinType
		{
			Inner,
			LeftOuter,
			RightOuter
		}

		public String DatabaseTableSyntax(String databaseName, String tableName)
		{
			return this.DatabaseTableSyntax("", databaseName, tableName);
		}

		protected String LinkServerNode(String linkServer)
		{
			String node = DataAccessServer.LinkServers[linkServer].ToString();

			if (String.IsNullOrEmpty(node))
			{
				throw new ArgumentException("Invalid LinkServer specified");
			}

			return node;
		}

		public abstract String DatabaseTableSyntax(String linkServer, String databaseName, String tableName);
		protected abstract String SelectBuilderReadDateTime(String tableName, String columnName, String columnAlias, DBDateTime dbDateTimeFrom, DBDateTime dbDateTimeTo);
		protected abstract String SelectBuilderFirstXRows(String sql, Int32 selectNRows);
		protected abstract void JoinTable(String databaseName, String tableName, String condition, String tableAlias, EntityJoinType entityJoinType);
		public abstract Char WildcardCharacter();
		public abstract String WildcardKeyword();
		public abstract String SelectDistinct();
		public abstract String CurrentDate();
		public abstract String CurrentTime();
		public abstract String GetIdentityValueSql();
		public abstract String LockSql();
		public abstract String DBDateTimeFunction(DBDateTime from, DBDateTime to, String valueText);
		public abstract String DBTimeSpanFunction(DBDateTime from, DBDateTime to, String valueText);

		protected virtual string FunctionPrefix(string tableName)
		{
			return "";
		}

		protected virtual string EvaluateEntityColumnExpression(EntityColumnExpression columnExpression, String tableName, String columnName)
		{
			switch (columnExpression)
			{
				case EntityColumnExpression.Function:
					return FunctionPrefix(tableName) + columnName;
				case EntityColumnExpression.Count:
					return "count(*)";
				case EntityColumnExpression.CountColumn:
					return "count(" + tableName + "." + columnName + ")";
				case EntityColumnExpression.CountDistinctColumn:
					return "count(distinct " + tableName + "." + columnName + ")";
				case EntityColumnExpression.Sum:
					return "sum(" + tableName + "." + columnName + ")";
				case EntityColumnExpression.SumDistinct:
					return "sum(distinct " + tableName + "." + columnName + ")";
				case EntityColumnExpression.Min:
					return "min(" + tableName + "." + columnName + ")";
				case EntityColumnExpression.MinDistinct:
					return "min(distinct " + tableName + "." + columnName + ")";
				case EntityColumnExpression.Max:
					return "max(" + tableName + "." + columnName + ")";
				case EntityColumnExpression.MaxDistinct:
					return "max(distinct " + tableName + "." + columnName + ")";
				case EntityColumnExpression.Average:
					return "avg(" + tableName + "." + columnName + ")";
				case EntityColumnExpression.AverageDistinct:
					return "avg(distinct " + tableName + "." + columnName + ")";
				default:
					throw new ArgumentException("Invalid EntityColumnExpression value");
			}
		}

		public virtual Boolean IncludeIdentityColumnInInsert()
		{
			return false;
		}

		protected void JoinTable(String databaseName, String table, String condition)
		{
			JoinTable(databaseName, table, condition, "", EntityJoinType.Inner);
		}

		protected void JoinTable(String databaseName, String table, String condition, String tableAlias)
		{
			JoinTable(databaseName, table, condition, tableAlias, EntityJoinType.Inner);
		}

		protected void JoinTable(String databaseName, String table, String condition, EntityJoinType entityJoinType)
		{
			JoinTable(databaseName, table, condition, "", entityJoinType);
		}

		#endregion

		// Properties
		private String _databaseTable = "";
		private Boolean _baseSQLIsValid = false;
		private String _baseSelect = "";
		private String _baseCount = "";
		private String _baseUpdate = "";
		private String _baseDelete = "";
		private String _baseCurrentCondition = "";
		private String _customSelect = "";
		private Boolean _selectUnique = false;
		private String _readFilter = "";
		private String _joinTableText = "";
		private String _joinCondition = "";
		private String _groupBy = "";
		private String _orderBy = "";
		private Boolean _ignoreMissingColumns = true;
		private List<String> _dirtyColumns = new List<String>();
		private Boolean _isEntityDirty = false;
		private Boolean _suspendPropertyChangedEvents = false;
		private Boolean _existsOnDatabase = false;
		private Boolean _isMarkedForDeletion = false;
		private Int16? _selectFirstNRecords;
		private Boolean _isReadOnlyEntity = false;
		private String _sequenceName = "";
		private Boolean _performConcurrencyChecks = false;
		private DataConnection _manualConnection = null;

		[Browsable(false)]
		public DataConnection ManualConnection
		{
			get { return _manualConnection; }
			set { _manualConnection = value; }
		}

		[Browsable(false)]
		public Boolean PerformConcurrencyChecks
		{
			get { return _performConcurrencyChecks; }
			set { _performConcurrencyChecks = value; }
		}

		[Browsable(false)]
		protected virtual String ConcurrencyCheck
		{
			get { return ""; }
		}

		[Browsable(false)]
		public String DatabaseTable
		{
			get { return _databaseTable; }
			set { _databaseTable = value; _baseSQLIsValid = false; }
		}

		[Browsable(false)]
		public String CustomSelect
		{
			get { return _customSelect; }
			set { _customSelect = value; }
		}

		[Browsable(false)]
		public Boolean SelectUnique
		{
			get { return _selectUnique; }
			set { _selectUnique = value; }
		}

		[Browsable(false)]
		public String ReadFilter
		{
			get { return _readFilter; }
			set { _readFilter = value; }
		}

		[Browsable(false)]
		public String GroupBy
		{
			get { return _groupBy; }
			set { _groupBy = value; }
		}

		[Browsable(false)]
		public String OrderBy
		{
			get { return _orderBy; }
			set { _orderBy = value; }
		}

		[Browsable(false)]
		public String JoinTableText
		{
			get { return _joinTableText; }
			set { _joinTableText = value; _baseSQLIsValid = false; }
		}

		[Browsable(false)]
		public String JoinCondition
		{
			get { return _joinCondition; }
			set { _joinCondition = value; _baseSQLIsValid = false; }
		}

		[Browsable(false)]
		public Boolean IgnoreMissingColumns
		{
			get { return _ignoreMissingColumns; }
			set { _ignoreMissingColumns = value; }
		}

		[Browsable(false)]
		public Boolean ExistsOnDatabase
		{
			get
			{
				return _existsOnDatabase;
			}
			set
			{
				if (value != _existsOnDatabase)
				{
					_existsOnDatabase = value;
					_isEntityDirty = true;
				}
			}
		}

		[Browsable(false)]
		public Boolean IsMarkedForDeletion
		{
			get { return _isMarkedForDeletion; }
			set { _isMarkedForDeletion = value; NotifyPropertyChanged("IsMarkedForDeletion"); }
		}

		[Browsable(false)]
		public Int16? SelectFirstNRecords
		{
			get { return _selectFirstNRecords; }
			set { _selectFirstNRecords = value; }
		}

		[Browsable(false)]
		public Boolean IsEntityDirty
		{
			get { return (_isEntityDirty || _isMarkedForDeletion); }
		}

		[Browsable(false)]
		public Boolean IsReadOnlyEntity
		{
			get { return _isReadOnlyEntity; }
			set { _isReadOnlyEntity = value; }
		}

		[Browsable(false)]
		public String SequenceName
		{
			get { return _sequenceName; }
			set { _sequenceName = value; }
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public String SqlCondition(String propertyName, String logicalCondition)
		{
			PropertyInfo propertyInfo = this.GetType().GetProperty(propertyName);

			if (propertyInfo == null)
			{
				throw new DataException("AddReadCondition() invalid property " + propertyName + " specified");
			}

			EntityColumnAttribute entityColumnAttribute = GetEntityColumnAttribute(propertyInfo);

			if (entityColumnAttribute == null)
			{
				throw new InvalidOperationException("You cannot add a read filter to a non-entity column");
			}

			String valueText = PropertyValueText(propertyInfo);
			String condition = "{" + propertyName + "} " + logicalCondition + " " + valueText;

			if (logicalCondition == "=" && valueText.ToLower() == "null")
			{
				condition = "{" + propertyName + "} is null";
			}

			if (logicalCondition == "!=" && valueText.ToLower() == "null")
			{
				condition = "{" + propertyName + "} is not null";
			}

			return condition;
		}

		public String SqlConditionBetween(String propertyName, Object fromValue, Object toValue)
		{
			PropertyInfo propertyInfo = this.GetType().GetProperty(propertyName);
			EntityColumnAttribute entityColumnAttribute = GetEntityColumnAttribute(propertyInfo);

			if (entityColumnAttribute == null)
			{
				throw new InvalidOperationException("You cannot add a read filter to a non-entity column");
			}

			Object saveValue = propertyInfo.GetValue(this, null);

			propertyInfo.SetValue(this, fromValue, null);

			String condition = "{" + propertyName + "} between " + PropertyValueText(propertyInfo, this, false);

			propertyInfo.SetValue(this, toValue, null);

			condition += " and " + PropertyValueText(propertyInfo, this, false);

			propertyInfo.SetValue(this, saveValue, null);
			this.SetColumnClean(propertyName);

			return condition;
		}

		public String SqlConditionIn(String propertyName, List<Object> values)
		{
			PropertyInfo propertyInfo = this.GetType().GetProperty(propertyName);
			EntityColumnAttribute entityColumnAttribute = GetEntityColumnAttribute(propertyInfo);

			if (entityColumnAttribute == null)
			{
				throw new InvalidOperationException("You cannot add a read filter to a non-entity column");
			}

			if (values.Count == 0)
			{
				throw new ArgumentException("Empty list of values encountered");
			}

			Object saveValue = propertyInfo.GetValue(this, null);

			String condition = "{" + propertyName + "} in (";

			String comma = "";
			foreach (Object value in values)
			{
				propertyInfo.SetValue(this, value, null);

				condition += comma + PropertyValueText(propertyInfo, this, false);
				comma = ",";
			}

			condition += ")";

			propertyInfo.SetValue(this, saveValue, null);
			this.SetColumnClean(propertyName);

			return condition;
		}

		public void AddReadFilter(String propertyName, String logicalCondition)
		{
			String condition = SqlCondition(propertyName, logicalCondition);

			_readFilter += (_readFilter.Length > 0 ? " and " + condition : condition);
		}

		public void AddReadFilter(String propertyName, String logicalCondition, Object value)
		{
			PropertyInfo propertyInfo = this.GetType().GetProperty(propertyName);
			propertyInfo.SetValue(this, value, null);
			AddReadFilter(propertyName, logicalCondition);
		}

		public void AddReadFilter(String sqlCondition)
		{
			_readFilter += (String.IsNullOrEmpty(_readFilter) ? "" : " and ") + sqlCondition;
		}

		public void AddReadFilterBetween(String propertyName, Object fromValue, Object toValue)
		{
			String condition = SqlConditionBetween(propertyName, fromValue, toValue);

			_readFilter += (_readFilter.Length > 0 ? " and " + condition : condition);
		}

		public void AddReadFilterIn(String propertyName, List<Object> values)
		{
			String condition = SqlConditionIn(propertyName, values);

			_readFilter += (_readFilter.Length > 0 ? " and " + condition : condition);
		}

		public String PropertyValueText(PropertyInfo propertyInfo)
		{
			return PropertyValueText(propertyInfo, this, false);
		}

		public String PropertyValueText(PropertyInfo propertyInfo, Object instance)
		{
			return PropertyValueText(propertyInfo, this, false);
		}

		public String PropertyValueText(PropertyInfo propertyInfo, Object instance, Boolean isUnload)
		{
			String methodName = "";
			String valueText = "";
			EntityColumnAttribute entityColumnAttribute = GetEntityColumnAttribute(propertyInfo);

			EntityColumnConversionAttribute entityColumnConversionAttribute = (EntityColumnConversionAttribute) TypeDescriptor.GetProperties(this)[propertyInfo.Name].Attributes[typeof(EntityColumnConversionAttribute)];
			MethodInfo methodConvert = null;
			Type sqlHelperType = typeof(SqlHelper);

			if (entityColumnConversionAttribute == null || entityColumnConversionAttribute.NativeToDB.Length == 0)
			{
				String targetType = SqlHelper.GetNativeTypeDescription(propertyInfo.PropertyType);

				// Ensure target type description matches the database types
				if (targetType == "Boolean")
				{
					targetType = "Int16";
				}

				targetType = targetType.Replace("Nullable", "");

				// Now construct conversion method name and find method
				methodName = SqlHelper.GetNativeTypeDescription(propertyInfo.PropertyType) + "ToDB" + targetType;
				methodConvert = sqlHelperType.GetMethod(methodName);
			}
			else
			{
				String assemblyQualifiedName;
				methodName = entityColumnConversionAttribute.NativeToDB;

				if (entityColumnConversionAttribute.Class.Length > 0)
				{
					if (entityColumnConversionAttribute.Namespace.Length > 0)
					{
						// Custom assembly qualifier used
						assemblyQualifiedName = entityColumnConversionAttribute.Namespace + "." + entityColumnConversionAttribute.Class + ", " + entityColumnConversionAttribute.Assembly;
					}
					else
					{
						// No custom assembly specified, assume local assembly for conversion
						Type local = this.GetType();
						assemblyQualifiedName = local.Namespace + "." + entityColumnConversionAttribute.Class + ", " + local.Assembly;
					}

					Type conversionType = Type.GetType(assemblyQualifiedName);

					if (conversionType != null)
					{
						methodConvert = conversionType.GetMethod(methodName.Substring(methodName.LastIndexOf('.') + 1));
					}
				}
				else
				{
					// No assembly details specified assume the conversion
					// method exists in the base SqlHelper class
					methodConvert = sqlHelperType.GetMethod(methodName);
				}
			}

			if (methodConvert == null)
			{
				throw new DatabaseException("Data conversion method " + methodName + " not found");
			}
			else
			{
				Object value;

				try
				{
					value = methodConvert.Invoke(null, new Object[] { propertyInfo.GetValue(instance, null) });

					if (value == DBNull.Value)
					{
						valueText = "null";
					}
					else
					{
						try
						{
							String typeDescription = value.GetType().Name;

							switch (typeDescription)
							{
								case "String":
									valueText = value.ToString().Replace("'", "''");
									valueText = (isUnload ? valueText : "'" + valueText + "'");
									break;

								case "Char":
									valueText = value.ToString();
									valueText = (isUnload ? valueText : "'" + valueText + "'");
									break;

								case "DateTime":
									valueText = DBDateTimeText(entityColumnAttribute, (DateTime) value, isUnload);
									break;

								case "TimeSpan":
									valueText = DBTimeSpanText(entityColumnAttribute, (TimeSpan) value, isUnload);
									break;

								default:
									valueText = value.ToString();
									break;
							}
						}
						catch (ArgumentOutOfRangeException ex)
						{
							throw ex;
						}
						catch
						{
							throw new DataException("Failed to convert to text (Conversion method " + methodName + ")");
						}
					}
				}
				catch (IndexOutOfRangeException)
				{
					throw new DatabaseException("No data conversion method " + methodName);
				}
			}

			return valueText;
		}

		private String DBDateTimeText(EntityColumnAttribute entityColumnAttribute, DateTime value)
		{
			return DBDateTimeText(entityColumnAttribute, value, false);
		}

		private String DBDateTimeText(EntityColumnAttribute entityColumnAttribute, DateTime value, Boolean isUnload)
		{
			String[] dateTimeParts = new String[6];
			String valueText = "";

			dateTimeParts[0] = value.Year.ToString("0000");
			dateTimeParts[1] = value.Month.ToString("00");
			dateTimeParts[2] = value.Day.ToString("00");
			dateTimeParts[3] = value.Hour.ToString("00");
			dateTimeParts[4] = value.Minute.ToString("00");
			dateTimeParts[5] = value.Second.ToString("00");

			if (entityColumnAttribute.DBDateTimeFrom == DBDateTime.Null || entityColumnAttribute.DBDateTimeTo == DBDateTime.Null)
			{
				throw new ArgumentOutOfRangeException("Date time from and to attributes not defined for " + entityColumnAttribute.ColumnName);
			}

			Int32 from = (Int32) entityColumnAttribute.DBDateTimeFrom;
			Int32 to = (Int32) entityColumnAttribute.DBDateTimeTo;

			if (from == (Int32) DBDateTime.Year && to == (Int32) DBDateTime.Day)
			{
				valueText = DBDateText(value, isUnload);
			}
			else
			{
				// Normal DateTime column
				for (Int32 component = from; component <= to; component++)
				{
					valueText += dateTimeParts[component];

					if (component < to)
					{
						if (component < (Int32) DBDateTime.Day)
						{
							valueText += "-";
						}
						else if (component == (Int32) DBDateTime.Day)
						{
							valueText += " ";
						}
						else
						{
							valueText += ":";
						}
					}
				}

				if (!isUnload)
				{
					valueText = this.DBDateTimeFunction(entityColumnAttribute.DBDateTimeFrom, entityColumnAttribute.DBDateTimeTo, valueText);
				}
			}

			return valueText;
		}

		public String DBDateText(DateTime value)
		{
			return DBDateText(value, false);
		}

		public String DBDateText(DateTime value, Boolean isUnload)
		{
			String dateText = "";

			switch (UserAuthentification.Credentials.DatabaseDateFormat)
			{
				case DatabaseDateFormat.British:
					dateText = value.Day.ToString("00") + "-" + value.Month.ToString("00") + "-" + value.Year.ToString("0000");
					break;
				case DatabaseDateFormat.American:
					dateText = value.Month.ToString("00") + "-" + value.Day.ToString("00") + "-" + value.Year.ToString("0000");
					break;
				case DatabaseDateFormat.YearMonthDay:
					dateText = value.Year.ToString("0000") + "-" + value.Month.ToString("00") + "-" + value.Day.ToString("00");
					break;
				default:
					throw new DataException("Invalid DatabaseDateFormat in UserCredentials");
			}

			if (!isUnload)
			{
				dateText = this.DBDateTimeFunction(DBDateTime.Year, DBDateTime.Day, dateText);
			}

			return dateText;
		}

		private String DBTimeSpanText(EntityColumnAttribute entityColumnAttribute, TimeSpan value)
		{
			return DBTimeSpanText(entityColumnAttribute, value, false);
		}

		private String DBTimeSpanText(EntityColumnAttribute entityColumnAttribute, TimeSpan value, Boolean isUnload)
		{
			String[] dateTimeParts = new String[6];
			String valueText = "";

			dateTimeParts[0] = "0";
			dateTimeParts[1] = "0";
			dateTimeParts[2] = "0";
			dateTimeParts[3] = value.Hours.ToString("00");
			dateTimeParts[4] = value.Minutes.ToString("00");
			dateTimeParts[5] = value.Seconds.ToString("00");

			Int32 from = (Int32) entityColumnAttribute.DBDateTimeFrom;
			Int32 to = (Int32) entityColumnAttribute.DBDateTimeTo;

			for (Int32 component = from; component <= to; component++)
			{
				valueText += dateTimeParts[component];

				if (component < to)
				{
					valueText += (component >= 3 ? ":" : " ");
				}
			}

			if (!isUnload)
			{
				valueText = this.DBDateTimeFunction(entityColumnAttribute.DBDateTimeFrom, entityColumnAttribute.DBDateTimeTo, valueText);
			}

			return valueText;
		}

		public IBindingList Read()
		{
			Boolean isRowLimitExceeded;
			return Read("", -1, out isRowLimitExceeded);
		}

		public IBindingList Read(Int32 rowLimit, out Boolean isRowLimitExceeded)
		{
			return Read("", rowLimit, out isRowLimitExceeded);
		}

		public IBindingList Read(String condition)
		{
			Boolean isRowLimitExceeded = false;
			return Read(condition, -1, out isRowLimitExceeded);
		}

		public IBindingList Read(String condition, Int32 rowLimit, out Boolean isRowLimitExceeded)
		{
			String sql = "";

			if (_customSelect.Length > 0)
			{
				sql = _customSelect;
			}
			else
			{
				if (!_baseSQLIsValid)
				{
					SetupBaseSQLStatements();
				}

				sql = _baseSelect;
			}

			if (condition.Length > 0)
			{
				if (sql.ToLower().Contains(" where "))
				{
					sql += " and " + condition;
				}
				else
				{
					sql += " where " + condition;
				}
			}

			if (_readFilter.Length > 0)
			{
				if (sql.ToLower().Contains(" where "))
				{
					sql += " and " + _readFilter;
				}
				else
				{
					sql += " where " + _readFilter;
				}
			}

			if (_groupBy.Length > 0)
			{
				sql += " group by " + _groupBy;
			}

			if (_orderBy.Length > 0)
			{
				sql += " order by " + _orderBy;
			}

			return ReadWithSql(sql, rowLimit, out isRowLimitExceeded);
		}
		
		public IBindingList ReadWithSql(String sql, Int32 rowLimit, out Boolean isRowLimitExceeded)
		{
			SqlResponse sqlResponse = new SqlResponse();
			DataConnection dataConnection = null;
			String assemblyQualifiedName = this.GetType().AssemblyQualifiedName;
			assemblyQualifiedName = assemblyQualifiedName.Insert(assemblyQualifiedName.IndexOf(','), "Collection");

			Type typeInstance = Type.GetType(this.GetType().AssemblyQualifiedName);

			Type genericType = typeof(ExtendedBindingList<>);
			Type collectionType = genericType.MakeGenericType(new Type[] { typeInstance });
			Object collection = Activator.CreateInstance(collectionType);
			MethodInfo methodAdd = collectionType.GetMethod("Add");
			Int32 rowCount = 0;
			String exceptionText = "";

			isRowLimitExceeded = false;

			try
			{
				if (this.ManualConnection != null)
				{
					dataConnection = this.ManualConnection;
				}
				else if (DataAccessServer.PersistentConnection != null)
				{
					dataConnection = DataAccessServer.PersistentConnection;
				}
				else
				{
					dataConnection = new DataConnection();
					sqlResponse = dataConnection.OpenConnection();
				}

				if (sqlResponse != null && !sqlResponse.HasError)
				{
					// Convert any {PropertyName} values to their column names
					// and any {%PropertyName%} to its value
					sql = PropertyNamesToColumnNames(sql);

					// Now check to see if we need to restrict the number of rows we return
					Int32 selectNRecords = -1;

					if (rowLimit > 0)
					{
						selectNRecords = rowLimit + 1;
					}

					if (_selectFirstNRecords.HasValue)
					{
						if (selectNRecords >= 0)
						{
							selectNRecords = (_selectFirstNRecords < rowLimit ? (Int32) _selectFirstNRecords : rowLimit);
						}
						else
						{
							selectNRecords = (Int32) _selectFirstNRecords;
						}
					}

					if (selectNRecords > 0)
					{
						sql = SelectBuilderFirstXRows(sql, selectNRecords);
					}

					// Process SQL (used DataAdapter and DataTable instead of DataReader as this
					// seemed to be much faster over OLE to Informix)
					sqlResponse = dataConnection.ExecuteSqlToDataTable(sql);
				}

				if (sqlResponse != null && !sqlResponse.HasError && sqlResponse.Results != null)
				{
					DataTable dataTable = (DataTable) sqlResponse.Results[0];

					foreach (DataRow dataRow in dataTable.Rows)
					{
						if (rowLimit > 0 && ++rowCount > rowLimit)
						{
							isRowLimitExceeded = true;
							break;
						}

						Object instance = Activator.CreateInstance(typeInstance);
						Type sqlHelperType = typeof(SqlHelper);
						((Entity) instance).ExistsOnDatabase = true;

						foreach (PropertyInfo propertyInfo in this.GetType().GetProperties())
						{
							if (propertyInfo.CanWrite)
							{
								EntityColumnAttribute entityColumnAttribute = GetEntityColumnAttribute(propertyInfo);

								if (entityColumnAttribute != null && entityColumnAttribute.ColumnAlias.Length > 0)
								{
									if (dataTable.Columns[entityColumnAttribute.ColumnAlias] != null)
									{
										String methodName = "";
										EntityColumnConversionAttribute entityColumnConversionAttribute = (EntityColumnConversionAttribute) TypeDescriptor.GetProperties(instance)[propertyInfo.Name].Attributes[typeof(EntityColumnConversionAttribute)];
										MethodInfo methodConvert = null;

										if (entityColumnConversionAttribute == null || entityColumnConversionAttribute.DBToNative.Length == 0)
										{
											methodName = "DB" + dataTable.Columns[entityColumnAttribute.ColumnAlias].DataType.Name + "To" + SqlHelper.GetNativeTypeDescription(propertyInfo.PropertyType);
											methodConvert = sqlHelperType.GetMethod(methodName);
										}
										else
										{
											methodName = entityColumnConversionAttribute.DBToNative;

											if (entityColumnConversionAttribute.Class.Length > 0)
											{
												if (entityColumnConversionAttribute.Namespace.Length > 0)
												{
													// Custom assembly qualifier used
													assemblyQualifiedName = entityColumnConversionAttribute.Namespace + "." + entityColumnConversionAttribute.Class + ", " + entityColumnConversionAttribute.Assembly;
												}
												else
												{
													// No custom assembly specified, assume local assembly for conversion
													Type local = this.GetType();
													assemblyQualifiedName = local.Namespace + "." + entityColumnConversionAttribute.Class + ", " + local.Assembly;
												}

												Type conversionType = Type.GetType(assemblyQualifiedName);

												if (conversionType != null)
												{
													methodConvert = conversionType.GetMethod(methodName.Substring(methodName.LastIndexOf('.') + 1));
												}
											}
											else
											{
												// No assembly details specified assume the conversion
												// method exists in the base SqlHelper class
												methodConvert = sqlHelperType.GetMethod(methodName);
											}
										}

										if (methodConvert != null)
										{
											try
											{
												Object value = methodConvert.Invoke(null, new Object[] { dataRow[entityColumnAttribute.ColumnAlias] });
												propertyInfo.SetValue(instance, value, null);
											}
											catch (IndexOutOfRangeException)
											{
												if (!_ignoreMissingColumns)
												{
													DatabaseException databaseException = new DatabaseException("No data returned for column " + entityColumnAttribute.ColumnName);
													databaseException.IsLockError = sqlResponse.IsLockError;

													throw databaseException;
												}
											}
										}
										else
										{
											throw new DatabaseException("No data conversion method " + methodName + " for property " + propertyInfo.Name);
										}
									}
								}
							}
						}

						((Entity) instance).SetColumnClean();
						methodAdd.Invoke(collection, new Object[] { instance });
					}
				}
				else
				{
					exceptionText = "NULL SqlResponse: " + sql;

					if (sqlResponse != null)
					{
						exceptionText = sqlResponse.Message + ": " + sql;
					}
				}

				if (exceptionText != "")
				{
					// SQL error has occurred within the DataAccessServer
					collection = null;

					DatabaseException databaseException = new DatabaseException(exceptionText);
					databaseException.IsLockError = (sqlResponse == null ? false : sqlResponse.IsLockError);

					throw databaseException;
				}
			}
			finally
			{
				if ((this.ManualConnection == null && DataAccessServer.PersistentConnection == null) || exceptionText.Length > 0)
				{
					dataConnection.CloseConnection();
				}

				dataConnection = null;
			}

			return (IBindingList) collection;
		}

		public Boolean ReadCurrent()
		{
			if (!_baseSQLIsValid)
			{
				SetupBaseSQLStatements();
			}

			Boolean success = false;
			String condition = _baseCurrentCondition;

			foreach (PropertyInfo propertyInfo in this.GetType().GetProperties())
			{
				EntityColumnAttribute entityColumnAttribute = GetEntityColumnAttribute(propertyInfo);

				if (entityColumnAttribute != null && entityColumnAttribute.IsKeyColumn)
				{
					condition = condition.Replace("{" + propertyInfo.Name + "}", PropertyValueText(propertyInfo));

					if (condition.EndsWith("= null"))
					{
						condition = condition.Remove(condition.Length - 6) + "is null";
					}
				}
			}

			// Now apply filter and attempt to read, should only get 1 record
			IBindingList collection = Read(condition);

			if (collection != null)
			{
				// Ok the read must have returned something, check that we have exactly
				// one row, otherwise the unique key failed
				if (collection.Count == 1)
				{
					// Ok have 1 row, extract the object from the collection
					Object current = collection[0];

					if (current != null)
					{
						_suspendPropertyChangedEvents = true;

						// Now we have the single object set the properties of 'this'
						// object to match the values in the one just read from the
						// database
						foreach (PropertyInfo propertyInfo in this.GetType().GetProperties())
						{
							if (propertyInfo.CanWrite)
							{
								propertyInfo.SetValue(this, propertyInfo.GetValue(current, null), null);
							}
						}

						success = true;
						_existsOnDatabase = true;
						_suspendPropertyChangedEvents = false;

						SetColumnClean();
					}
				}
				else if (collection.Count > 1)
				{
					throw new DataException("ReadCurrent() returned " + collection.Count.ToString() + " rows");
				}
			}

			return success;
		}

		public EntityColumnAttribute GetEntityColumnAttribute(PropertyInfo propertyInfo)
		{
			EntityColumnAttribute entityColumnAttribute = null;
			PropertyDescriptor propertyDescriptor = TypeDescriptor.GetProperties(this)[propertyInfo.Name];

			if (propertyDescriptor != null)
			{
				entityColumnAttribute = (EntityColumnAttribute) propertyDescriptor.Attributes[typeof(EntityColumnAttribute)];
			}

			return entityColumnAttribute;
		}

		protected Boolean IsSelectable(PropertyInfo propertyInfo)
		{
			PropertyDescriptor propertyDescriptor = TypeDescriptor.GetProperties(this)[propertyInfo.Name];
			Selectable selectable = null;

			if (propertyDescriptor != null)
			{
				selectable = (Selectable) propertyDescriptor.Attributes[typeof(Selectable)];
			}

			return (selectable == null ? true : selectable.CanSelect);
		}

		protected Boolean IsReadOnlyColumn(PropertyInfo propertyInfo)
		{
			Boolean isReadOnly = true;
			PropertyDescriptor propertyDescriptor = TypeDescriptor.GetProperties(this)[propertyInfo.Name];
			EntityColumnAttribute entityColumnAttribute = null;

			if (propertyDescriptor != null)
			{
				entityColumnAttribute = (EntityColumnAttribute) propertyDescriptor.Attributes[typeof(EntityColumnAttribute)];

				if (entityColumnAttribute != null)
				{
					isReadOnly = entityColumnAttribute.IsReadOnly;
				}
			}

			return isReadOnly;
		}

		public Int32? GetColumnWidth(String propertyName)
		{
			Int32? width = null;
			EntityColumnWidthAttribute entityColumnWidthAttribute = null;
			PropertyDescriptor propertyDescriptor = TypeDescriptor.GetProperties(this)[propertyName];

			if (propertyDescriptor != null)
			{
				entityColumnWidthAttribute = (EntityColumnWidthAttribute) propertyDescriptor.Attributes[typeof(EntityColumnWidthAttribute)];

				if (entityColumnWidthAttribute != null)
				{
					width = entityColumnWidthAttribute.Width;
				}
			}

			return width;
		}

		public Int32? GetDisplayOrder(String propertyName)
		{
			Int32? order = null;
			DisplayOrderAttribute displayOrderAttribute = null;
			PropertyDescriptor propertyDescriptor = TypeDescriptor.GetProperties(this)[propertyName];

			if (propertyDescriptor != null)
			{
				displayOrderAttribute = (DisplayOrderAttribute) propertyDescriptor.Attributes[typeof(DisplayOrderAttribute)];

				if (displayOrderAttribute != null)
				{
					order = displayOrderAttribute.Order;
				}
			}

			return order;
		}

		public IBindingList ReadWithDirtyProperty()
		{
			return ReadWithDirtyProperty(false);
		}

		public IBindingList ReadWithDirtyProperty(Boolean useReadFilter)
		{
			String sql = "";
			Boolean isRowLimitExceeded;

			// Setup base SQL
			if (_customSelect.Length > 0)
			{
				sql = _customSelect;
			}
			else
			{
				if (!_baseSQLIsValid)
				{
					SetupBaseSQLStatements();
				}

				sql = _baseSelect;
			}

			// Apply dirty properties as filters
			foreach (PropertyInfo propertyInfo in this.GetType().GetProperties())
			{
				EntityColumnAttribute entityColumnAttribute = GetEntityColumnAttribute(propertyInfo);

				if (entityColumnAttribute != null && entityColumnAttribute.ColumnName.Length > 0)
				{
					if (IsDirtyColumn(propertyInfo))
					{
						if (sql.ToLower().Contains(" where "))
						{
							sql += " and ";
						}
						else
						{
							sql += " where ";
						}

						if (entityColumnAttribute.EntityAlias.Length > 0)
						{
							sql += entityColumnAttribute.EntityAlias;
						}
						else
						{
							sql += _databaseTable;
						}

						sql += "." + entityColumnAttribute.ColumnAlias + " = " + PropertyValueText(propertyInfo);
					}
				}
			}

			// Apply filter and sort order as required
			if (_readFilter.Length > 0 && useReadFilter)
			{
				if (sql.ToLower().Contains(" where "))
				{
					sql += " and " + _readFilter;
				}
				else
				{
					sql += " where " + _readFilter;
				}
			}

			if (_groupBy.Length > 0)
			{
				sql += " group by " + _groupBy;
			}

			if (_orderBy.Length > 0)
			{
				sql += " order by " + _orderBy;
			}

			return ReadWithSql(sql, -1, out isRowLimitExceeded);
		}

		public Int32 ReadCount()
		{
			return ReadCount("");
		}

		public Int32 ReadCount(String condition)
		{
			Int32 count = 0;
			String sql = "";

			if (!_baseSQLIsValid)
			{
				SetupBaseSQLStatements();
			}

			sql = _baseCount;

			if (condition.Length > 0)
			{
				if (sql.ToLower().Contains(" where "))
				{
					sql += " and " + condition;
				}
				else
				{
					sql += " where " + condition;
				}
			}

			if (_readFilter.Length > 0)
			{
				if (sql.ToLower().Contains(" where "))
				{
					sql += " and " + _readFilter;
				}
				else
				{
					sql += " where " + _readFilter;
				}
			}

			// Convert any {PropertyName} values to their column names
			sql = PropertyNamesToColumnNames(sql);

			// Now execute the Sql using ExecuteScalar to get value back
			DataConnection dataConnection = null;
			SqlResponse sqlResponse = new SqlResponse();

			try
			{
				if (this.ManualConnection != null)
				{
					dataConnection = this.ManualConnection;
				}
				else if (DataAccessServer.PersistentConnection != null)
				{
					dataConnection = DataAccessServer.PersistentConnection;
				}
				else
				{
					dataConnection = new DataConnection();
					sqlResponse = dataConnection.OpenConnection();
				}

				if (sqlResponse != null && !sqlResponse.HasError)
				{
					sqlResponse = dataConnection.ExecuteScalar(sql);
				}

				if (sqlResponse == null || sqlResponse.HasError)
				{
					String exceptionText = "NULL SqlResponse: " + sql;

					if (sqlResponse != null)
					{
						exceptionText = sqlResponse.Message + ": " + sql;
					}

					DatabaseException databaseException = new DatabaseException(exceptionText);
					databaseException.IsLockError = (sqlResponse == null ? false : sqlResponse.IsLockError);

					throw databaseException;
				}
				else
				{
					count = Int32.Parse(sqlResponse.Results[0].ToString());
				}
			}
			finally
			{
				// Close the connection if not public
				if (this.ManualConnection == null && DataAccessServer.PersistentConnection == null)
				{
					dataConnection.CloseConnection();
				}

				dataConnection = null;
			}

			return count;
		}

		public Int32 Update()
		{
			return Update(false);
		}

		public SqlResponse ExecuteSql(String sql)
		{
			DataConnection dataConnection = null;
			SqlResponse sqlResponse = new SqlResponse();
			Boolean isInsertUpdateDelete = sql.ToLower().StartsWith("update") || sql.ToLower().StartsWith("delete") || sql.ToLower().StartsWith("insert");

			sql = sql.Trim();

			// Define a data connector
			try
			{
				if (this.ManualConnection != null)
				{
					dataConnection = this.ManualConnection;
				}
				else if (DataAccessServer.PersistentConnection != null)
				{
					dataConnection = DataAccessServer.PersistentConnection;
				}
				else 
				{
					if (isInsertUpdateDelete)
					{
						throw new DatabaseException("Attempting insert / update / delete outside of a transaction");
					}

					dataConnection = new DataConnection();
					sqlResponse = dataConnection.OpenConnection();
				}

				if (sqlResponse != null && !sqlResponse.HasError)
				{
					// Convert any {PropertyName} values to their column names
					sql = PropertyNamesToColumnNames(sql);

					// Now execute update SQL
					sqlResponse = dataConnection.ExecuteSql(sql);
				}

				if (sqlResponse == null || sqlResponse.HasError)
				{
					String exceptionText = "NULL SqlResponse: " + sql;

					if (sqlResponse != null)
					{
						exceptionText = sqlResponse.Message + ": " + sql;
					}

					DatabaseException databaseException = new DatabaseException(exceptionText);
					databaseException.IsLockError = (sqlResponse == null ? false : sqlResponse.IsLockError);

					throw databaseException;
				}

				if (isInsertUpdateDelete)
				{
					// If SQL has updated the database somehow, then add this entity to the list of entities that need to be cleaned upon commit.
					dataConnection.AddDirtyEntity(this);
				}
			}
			finally
			{
				// Close the connection if not public
				if (this.ManualConnection == null && DataAccessServer.PersistentConnection == null && dataConnection != null)
				{
					dataConnection.CloseConnection();
				}

				dataConnection = null;
			}

			return sqlResponse;
		}

		public SqlResponse ExecuteScaler(String sql)
		{
			DataConnection dataConnection = null;
			SqlResponse sqlResponse = new SqlResponse();

			sql = sql.Trim();

			// Define a data connector
			try
			{
				if (this.ManualConnection != null)
				{
					dataConnection = this.ManualConnection;
				}
				else if (DataAccessServer.PersistentConnection != null)
				{
					dataConnection = DataAccessServer.PersistentConnection;
				} 
				else
				{
					dataConnection = new DataConnection();
					sqlResponse = dataConnection.OpenConnection();
				}

				if (sqlResponse != null && !sqlResponse.HasError)
				{
					// Convert any {PropertyName} values to their column names
					sql = PropertyNamesToColumnNames(sql);

					// Now execute update SQL
					sqlResponse = dataConnection.ExecuteScalar(sql);
				}

				if (sqlResponse == null || sqlResponse.HasError)
				{
					String exceptionText = "NULL SqlResponse: " + sql;

					if (sqlResponse != null)
					{
						exceptionText = sqlResponse.Message + ": " + sql;
					}

					DatabaseException databaseException = new DatabaseException(exceptionText);
					databaseException.IsLockError = (sqlResponse == null ? false : sqlResponse.IsLockError);

					throw databaseException;
				}
			}
			finally
			{
				// Close the connection if not public
				if (this.ManualConnection == null && DataAccessServer.PersistentConnection == null)
				{
					dataConnection.CloseConnection();
				}

				dataConnection = null;
			}

			return sqlResponse;
		}

		public Int32 Update(Boolean updateAllColumns)
		{
			if (_isReadOnlyEntity)
			{
				throw new ReadOnlyException(this.GetType().Name + " is a read only entity");
			}

			if (!_baseSQLIsValid)
			{
				SetupBaseSQLStatements();
			}

			Int32 result = this.BeforeUpdate();

			if (result >= 0)
			{
				String sql = _baseUpdate;
				String condition = _baseCurrentCondition;

				foreach (PropertyInfo propertyInfo in this.GetType().GetProperties())
				{
					EntityColumnAttribute entityColumnAttribute = GetEntityColumnAttribute(propertyInfo);

					if (entityColumnAttribute != null && entityColumnAttribute.ColumnName.Length > 0 && entityColumnAttribute.EntityAlias.Length == 0)
					{
						if (entityColumnAttribute.IsKeyColumn)
						{
							condition = condition.Replace("{" + propertyInfo.Name + "}", PropertyValueText(propertyInfo));
						}
						else if ((IsDirtyColumn(propertyInfo) || updateAllColumns) && !entityColumnAttribute.IsReadOnly)
						{
							sql += entityColumnAttribute.ColumnName + " = " + PropertyValueText(propertyInfo) + ",";
						}
					}
				}

				// Tidy up SQL as may now be invalid after removing non-dirty columns
				if (sql.EndsWith(","))
				{
					DataConnection dataConnection = null;
					SqlResponse sqlResponse = new SqlResponse();

					sql = sql.Substring(0, sql.Length - 1) + " where " + condition;

					// Open connection (with transaction) if required
					try
					{
						if (this.ManualConnection != null)
						{
							dataConnection = this.ManualConnection;
						}
						else if (DataAccessServer.PersistentConnection != null)
						{
							dataConnection = DataAccessServer.PersistentConnection;
						}
						else
						{
							dataConnection = new DataConnection();
							sqlResponse = dataConnection.OpenConnection();

							if (sqlResponse != null && !sqlResponse.HasError)
							{
								sqlResponse = dataConnection.BeginTransaction();
							}
						}

						String executeSql = "";

						if (sqlResponse != null && !sqlResponse.HasError && !String.IsNullOrEmpty(this.LockSql()))
						{
							// Check for locks if required
							executeSql = "select * from " + this.DatabaseTable + " where " + condition + " " + this.LockSql();
							sqlResponse = dataConnection.ExecuteSql(executeSql);
						}

						if (sqlResponse != null && !sqlResponse.HasError)
						{
							// Now execute update SQL
							executeSql = sql;
							sqlResponse = dataConnection.ExecuteSql(executeSql);
						}

						// Check result and either throw an exception or return the result
						if (sqlResponse == null || sqlResponse.HasError)
						{
							String exceptionText = "NULL SqlResponse: " + executeSql;

							if (sqlResponse != null)
							{
								exceptionText = sqlResponse.Message + ": " + executeSql;
							}

							DatabaseException databaseException = new DatabaseException(exceptionText);
							databaseException.IsLockError = (sqlResponse == null ? false : sqlResponse.IsLockError);

							throw databaseException;
						}
						else
						{
							if (sqlResponse.Results != null && sqlResponse.Results.Length > 0)
							{
								result = (Int32) sqlResponse.Results[0];
							}

							dataConnection.AddDirtyEntity(this);
						}
					}
					finally
					{
						// Close connection if needed
						if (this.ManualConnection == null && DataAccessServer.PersistentConnection == null)
						{
							if (sqlResponse == null || sqlResponse.HasError)
							{
								dataConnection.RollbackTransaction();
							}
							else
							{
								dataConnection.CommitTransaction();
							}

							dataConnection.CloseConnection();
							dataConnection = null;
						}
					}
				}
				else
				{
					// No rows updated as no columns where dirty
					result = 0;
				}
			}

			if (result >= 0)
			{
				Int32 afterUpdateResult = this.AfterUpdate();

				if (afterUpdateResult < 0)
				{
					result = afterUpdateResult;
				}
			}

			return result;
		}

		public Int32 Delete()
		{
			if (_isReadOnlyEntity)
			{
				throw new ReadOnlyException(this.GetType().Name + " is a read only entity");
			}

			Int32 result = this.BeforeDelete();

			if (result >= 0)
			{
				DataConnection dataConnection = null;
				SqlResponse sqlResponse = new SqlResponse();

				if (!_baseSQLIsValid)
				{
					SetupBaseSQLStatements();
				}

				String sql = _baseDelete;
				String condition = _baseCurrentCondition;

				foreach (PropertyInfo propertyInfo in this.GetType().GetProperties())
				{
					EntityColumnAttribute entityColumnAttribute = GetEntityColumnAttribute(propertyInfo);

					if (entityColumnAttribute != null && entityColumnAttribute.ColumnName.Length > 0 && entityColumnAttribute.EntityAlias.Length == 0)
					{
						if (entityColumnAttribute.IsKeyColumn)
						{
							condition = condition.Replace("{" + propertyInfo.Name + "}", PropertyValueText(propertyInfo));
						}
					}
				}

				sql = sql.Substring(0, sql.Length - 1) + " where " + condition;

				// Open connection (with transaction) if required
				try
				{
					if (this.ManualConnection != null)
					{
						dataConnection = this.ManualConnection;
					}
					else if (DataAccessServer.PersistentConnection != null)
					{
						dataConnection = DataAccessServer.PersistentConnection;
					}
					else
					{
						dataConnection = new DataConnection();
						sqlResponse = dataConnection.OpenConnection();

						if (sqlResponse != null && !sqlResponse.HasError)
						{
							dataConnection.BeginTransaction();
						}
					}

					if (sqlResponse != null && !sqlResponse.HasError && !String.IsNullOrEmpty(this.LockSql()))
					{
						// Check for locks if required
						String checkLockSql = _baseSelect + " where " + condition + " " + this.LockSql();
						sqlResponse = dataConnection.ExecuteSql(checkLockSql);
					}

					if (sqlResponse != null && !sqlResponse.HasError)
					{
						// Now execute update SQL
						sqlResponse = dataConnection.ExecuteSql(sql);
					}

					// Throw exception if an error occurred or return the result
					if (sqlResponse == null || sqlResponse.HasError)
					{
						String exceptionText = "NULL SqlResponse: " + sql;

						if (sqlResponse != null)
						{
							exceptionText = sqlResponse.Message + ": " + sql;
						}

						DatabaseException databaseException = new DatabaseException(exceptionText);
						databaseException.IsLockError = (sqlResponse == null ? false : sqlResponse.IsLockError);

						throw databaseException;
					}
					else
					{
						if (sqlResponse.Results != null && sqlResponse.Results.Length > 0)
						{
							result = (Int32) sqlResponse.Results[0];
						}

						dataConnection.AddDirtyEntity(this);
					}
				}
				finally
				{
					// Close connection if needed
					if (this.ManualConnection == null && DataAccessServer.PersistentConnection == null)
					{
						if (sqlResponse == null || sqlResponse.HasError)
						{
							dataConnection.RollbackTransaction();
						}
						else
						{
							dataConnection.CommitTransaction();
						}

						dataConnection.CloseConnection();
					}

					dataConnection = null;
				}
			}

			if (result >= 0)
			{
				Int32 afterDeleteResult = this.AfterDelete();

				if (afterDeleteResult < 0)
				{
					result = afterDeleteResult;
				}
			}

			return result;
		}

		public Int32 DeleteForReadFilter()
		{
			if (_isReadOnlyEntity)
			{
				throw new ReadOnlyException(this.GetType().Name + " is a read only entity");
			}

			Int32 result = 0;
			DataConnection dataConnection = null;
			SqlResponse sqlResponse = new SqlResponse();

			if (!_baseSQLIsValid)
			{
				SetupBaseSQLStatements();
			}

			String sql = _baseDelete;
			sql = sql.Substring(0, sql.Length - 1) + " where " + _readFilter;
			sql = PropertyNamesToColumnNames(sql);

			// Open connection (with transaction) if required
			try
			{
				if (this.ManualConnection != null)
				{
					dataConnection = this.ManualConnection;
				}
				else if (DataAccessServer.PersistentConnection != null)
				{
					dataConnection = DataAccessServer.PersistentConnection;
				} 
				else 
				{
					dataConnection = new DataConnection();
					sqlResponse = dataConnection.OpenConnection();

					if (sqlResponse != null && !sqlResponse.HasError)
					{
						dataConnection.BeginTransaction();
					}
				}

				if (sqlResponse != null && !sqlResponse.HasError)
				{
					// Now execute update SQL
					sqlResponse = dataConnection.ExecuteSql(sql);
				}

				// Throw exception if an error occurred or return the result
				if (sqlResponse == null || sqlResponse.HasError)
				{
					String exceptionText = "NULL SqlResponse: " + sql;

					if (sqlResponse != null)
					{
						exceptionText = sqlResponse.Message + ": " + sql;
					}

					DatabaseException databaseException = new DatabaseException(exceptionText);
					databaseException.IsLockError = (sqlResponse == null ? false : sqlResponse.IsLockError);

					throw databaseException;
				}
				else
				{
					if (sqlResponse.Results != null && sqlResponse.Results.Length > 0)
					{
						result = (Int32) sqlResponse.Results[0];
					}

					dataConnection.AddDirtyEntity(this);
				}
			}
			finally
			{
				// Close connection if needed
				if (this.ManualConnection == null && DataAccessServer.PersistentConnection == null)
				{
					if (sqlResponse == null || sqlResponse.HasError)
					{
						dataConnection.RollbackTransaction();
					}
					else
					{
						dataConnection.CommitTransaction();
					}

					dataConnection.CloseConnection();
				}

				dataConnection = null;
			}

			return result;
		}

		public Int32 Insert()
		{
			if (_isReadOnlyEntity)
			{
				throw new ReadOnlyException(this.GetType().Name + " is a read only entity");
			}

			PropertyInfo identityProperty = null;
			Int32 result = this.BeforeInsert();

			if (result >= 0)
			{
				if (!_baseSQLIsValid)
				{
					SetupBaseSQLStatements();
				}

				String sql = "insert into " + _databaseTable + " ( ";
				String values = "values ( ";

				foreach (PropertyInfo propertyInfo in this.GetType().GetProperties())
				{
					EntityColumnAttribute entityColumnAttribute = GetEntityColumnAttribute(propertyInfo);

					if (entityColumnAttribute != null)
					{
						if (entityColumnAttribute.IsIdentity)
						{
							identityProperty = propertyInfo;
						}

						if (entityColumnAttribute.ColumnName.Length > 0 &&
							entityColumnAttribute.EntityAlias.Length == 0 &&
							!entityColumnAttribute.IsReadOnly &&
							(entityColumnAttribute.IsIdentity == false || IncludeIdentityColumnInInsert()))
						{
							sql += entityColumnAttribute.ColumnName + ", ";
							values += PropertyValueText(propertyInfo) + ", ";
						}
					}
				}

				// Tidy up SQL as may now be invalid after removing non-dirty columns
				if (sql.EndsWith(", "))
				{
					DataConnection dataConnection = null;
					SqlResponse sqlResponse = new SqlResponse();

					sql = sql.Substring(0, sql.Length - 2) + " ) " + values.Substring(0, values.Length - 2) + " )";

					// Open connection if required
					try
					{
						if (this.ManualConnection != null)
						{
							dataConnection = this.ManualConnection;
						}
						else if (DataAccessServer.PersistentConnection != null)
						{
							dataConnection = DataAccessServer.PersistentConnection;
						} 
						else
						{
							dataConnection = new DataConnection();
							sqlResponse = dataConnection.OpenConnection();

							if (sqlResponse != null && !sqlResponse.HasError)
							{
								dataConnection.BeginTransaction();
							}
						}

						if (sqlResponse != null && !sqlResponse.HasError)
						{
							// Now execute update SQL
							sqlResponse = dataConnection.ExecuteSql(sql);
						}

						// Throw exception if an error occurred or return the result
						if (sqlResponse == null || sqlResponse.HasError)
						{
							String exceptionText = "NULL SqlResponse: " + sql;

							if (sqlResponse != null)
							{
								exceptionText = sqlResponse.Message + ": " + sql;
							}

							// Generate database ex
							DatabaseException databaseException = new DatabaseException(exceptionText);

							databaseException.IsLockError = (sqlResponse == null ? false : sqlResponse.IsLockError);
							databaseException.EntityConcurrencyError = (sqlResponse != null && sqlResponse.IsDuplicate  && this.PerformConcurrencyChecks ? EntityConcurrencyError.Insert : EntityConcurrencyError.None);

							throw databaseException;
						}
						else
						{
							if (sqlResponse.Results != null && sqlResponse.Results.Length > 0)
							{
								result = (Int32) sqlResponse.Results[0];

								// Check for identity column and populate if required
								if (result > 0 && identityProperty != null)
								{
									sqlResponse = dataConnection.ExecuteScalar(this.GetIdentityValueSql());

									if (sqlResponse.Results[0] == DBNull.Value)
									{
										// This happens on SQL Server when inserting across linked servers. Will need to handle this using
										// a stored procedure or similar
										identityProperty.SetValue(this, -1, null);
									}
									else
									{
										result = Convert.ToInt32(sqlResponse.Results[0]);
										identityProperty.SetValue(this, result, null);
									}

									SetColumnClean(identityProperty.Name);
								}
							}

							dataConnection.AddDirtyEntity(this);
						}
					}
					finally
					{
						// Close connection if needed
						if (this.ManualConnection == null && DataAccessServer.PersistentConnection == null)
						{
							if (sqlResponse == null || sqlResponse.HasError)
							{
								dataConnection.RollbackTransaction();
							}
							else
							{
								dataConnection.CommitTransaction();
							}

							dataConnection.CloseConnection();
						}

						dataConnection = null;
					}
				}
				else
				{
					// No rows updated as no columns where dirty
					result = 0;
				}
			}

			if (result >= 0)
			{
				Int32 afterInsertResult = this.AfterInsert();

				if (afterInsertResult < 0)
				{
					result = afterInsertResult;
				}
			}

			return result;
		}

		public virtual void ReadEntity()
		{
		}

		public virtual void ReadEntityCollection()
		{
		}

		public virtual void ReadAllEntity()
		{
		}

		public virtual void ClearEntity()
		{
		}

		public virtual void ClearEntityCollection()
		{
		}

		public virtual void ClearAllEntity()
		{
		}

		private void SetupBaseSQLStatements()
		{
			// Now build base SQL's
			_baseSelect = "select ";

			if (_selectUnique)
			{
				_baseSelect += SelectDistinct() + " ";
			}

			_baseCount = "select count(*)";
			_baseUpdate = "update " + _databaseTable + " set ";
			_baseDelete = "delete from " + _databaseTable + " ";
			_baseCurrentCondition = "";

			foreach (PropertyInfo propertyInfo in this.GetType().GetProperties())
			{
				EntityColumnAttribute entityColumnAttribute = GetEntityColumnAttribute(propertyInfo);

				if (entityColumnAttribute != null && entityColumnAttribute.ColumnName.Length > 0 && IsSelectable(propertyInfo))
				{
					String tableName = (entityColumnAttribute.EntityAlias.Length > 0 ? entityColumnAttribute.EntityAlias : _databaseTable);

					if (entityColumnAttribute.DBDateTimeFrom != DBDateTime.Null && entityColumnAttribute.DBDateTimeTo != DBDateTime.Null)
					{
						// If we are selecting a date, datetime, or interval we may need a database specific
						// format, (for instance Informix ODBC connections need to convert the date to a datetime 
						// to ensure year to day, and then cast it as a string in order for the entity conversion 
						// methods to be sure of the format)
						_baseSelect += SelectBuilderReadDateTime(tableName, entityColumnAttribute.ColumnName, entityColumnAttribute.ColumnAlias, entityColumnAttribute.DBDateTimeFrom, entityColumnAttribute.DBDateTimeTo);
					}
					else
					{
						// Normal column select
						String columnDetail = tableName + "." + entityColumnAttribute.ColumnName;

						if (entityColumnAttribute.ColumnExpression != EntityColumnExpression.None)
						{
							columnDetail = EvaluateEntityColumnExpression(entityColumnAttribute.ColumnExpression, tableName, entityColumnAttribute.ColumnName);
						}

						_baseSelect += columnDetail;

						if (entityColumnAttribute.ColumnName != entityColumnAttribute.ColumnAlias)
						{
							_baseSelect += " as " + entityColumnAttribute.ColumnAlias;
						}
					}

					// Append to key field condition if the column is flagged as a key field
					if (entityColumnAttribute.IsKeyColumn)
					{
						if (_baseCurrentCondition.Length != 0)
						{
							_baseCurrentCondition += " and ";
						}

						_baseCurrentCondition += _databaseTable + "." + entityColumnAttribute.ColumnName + " = {" + propertyInfo.Name + "}";
					}

					_baseSelect += ", ";
				}
			}

			// Complete base select statement
			_baseSelect = _baseSelect.Substring(0, _baseSelect.Length - 2);

			String tableJoins = "";

			if (_joinTableText.Length > 0)
			{
				if (_joinTableText.ToLower().Contains(" join "))
				{
					// Sql Server table join format
					tableJoins = " from " + _databaseTable + " " + _joinTableText;
				}
				else
				{
					// Informix format
					tableJoins = " from " + _databaseTable + ", " + _joinTableText;
				}

				if (_joinCondition.Length > 0)
				{
					tableJoins += " where " + _joinCondition;
				}
			}
			else
			{
				tableJoins += " from " + _databaseTable;

				if (_joinCondition.Length > 0)
				{
					tableJoins += " where " + _joinCondition;
				}
			}

			_baseSelect += tableJoins;
			_baseCount += tableJoins;

			// Mark SQL as generated and ensure Database table structure is known within 
			// the static ObjectDBStructure class
			_baseSQLIsValid = true;
		}

		private void RegisterDataAccessSponser(IDataAccess dataAccess)
		{
		}

		public Boolean IsDirtyColumn(String propertyName)
		{
			return _dirtyColumns.Contains(propertyName);
		}

		public void SetColumnDirty()
		{
			if (!_suspendPropertyChangedEvents)
			{
				foreach (PropertyInfo propertyInfo in this.GetType().GetProperties())
				{
					if (!IsReadOnlyColumn(propertyInfo))
					{
						SetColumnDirty(propertyInfo.Name);
					}
				}
			}
		}

		public void SetColumnDirty(String propertyName)
		{
			if (!_suspendPropertyChangedEvents)
			{
				PropertyInfo propertyInfo = this.GetType().GetProperty(propertyName);

				if (propertyInfo != null && !IsReadOnlyColumn(propertyInfo))
				{
					_isEntityDirty = true;

					if (!_dirtyColumns.Contains(propertyName))
					{
						_dirtyColumns.Add(propertyName);
					}

					NotifyPropertyChanged(propertyName);
				}
			}
		}

		protected void NotifyPropertyChanged(String propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		public void CleanEntity()
		{
			ExistsOnDatabase = !_isMarkedForDeletion;
			SetColumnClean();
		}

		public virtual void SetColumnClean()
		{
			_dirtyColumns.Clear();
			_isEntityDirty = false;
		}

		public virtual void SetColumnClean(String propertyName)
		{
			_dirtyColumns.Remove(propertyName);

			if (_dirtyColumns.Count == 0)
			{
				_isEntityDirty = false;
			}
		}

		private Boolean IsDirtyColumn(PropertyInfo propertyInfo)
		{
			return _dirtyColumns.Contains(propertyInfo.Name);
		}

		public String KeyValuesToString()
		{
			return KeyValuesToString(", ");
		}

		public String KeyValuesToString(String seperator)
		{
			return PropertyValuesToString(seperator, false);
		}

		public String BaseEntityValuesToString()
		{
			return BaseEntityValuesToString(", ");
		}

		public String BaseEntityValuesToString(String seperator)
		{
			return PropertyValuesToString(seperator, true);
		}

		private String PropertyValuesToString(String seperator, Boolean allColumns)
		{
			StringBuilder output = new StringBuilder();

			foreach (PropertyInfo propertyInfo in this.GetType().GetProperties())
			{
				EntityColumnAttribute entityColumnAttribute = GetEntityColumnAttribute(propertyInfo);

				if (entityColumnAttribute != null)
				{
					if (entityColumnAttribute.EntityAlias.Length == 0)
					{
						if (allColumns || entityColumnAttribute.IsKeyColumn)
						{
							if (output.Length > 0)
							{
								output.Append(seperator);
							}

							output.Append(propertyInfo.Name + " = ");

							Object value = propertyInfo.GetValue(this, null);

							if (value == null)
							{
								output.Append("NULL");
							}
							else
							{
								output.Append(value.ToString());
							}
						}
					}
				}
			}

			return output.ToString();
		}

		private String PropertyNamesToColumnNames(String sql)
		{
			Regex regex = new Regex("{[^{]*}");
			MatchCollection matches = regex.Matches(sql);

			if (matches != null)
			{
				for (Int32 index = 0; index < matches.Count; index++)
				{
					Boolean useValue = false;
					String propertyName = matches[index].Value.Substring(1, matches[index].Value.Length - 2);

					if (propertyName.StartsWith("%"))
					{
						propertyName = propertyName.Replace("%", "");
						useValue = true;
					}

					PropertyInfo propertyInfo = this.GetType().GetProperty(propertyName);

					if (propertyInfo != null)
					{
						EntityColumnAttribute entityColumnAttribute = GetEntityColumnAttribute(propertyInfo);

						if (entityColumnAttribute == null)
						{
							if (useValue)
							{
								sql = sql.Replace(matches[index].Value, propertyInfo.GetValue(this, null).ToString());
							}
						} 
						else
						{
							String columnName = entityColumnAttribute.ColumnName;

							if (useValue)
							{
								// Replace with actual property value
								columnName = PropertyValueText(propertyInfo);
							}
							else
							{
								if (entityColumnAttribute.EntityAlias.Length == 0)
								{
									// Informix ODBC patch means INTERVAL and DATETIME columns will be selected
									// as "Table.Column || '' as Column" therefore do not want table name
									if (entityColumnAttribute.DBDateTimeFrom == DBDateTime.Null && entityColumnAttribute.DBDateTimeTo == DBDateTime.Null)
									{
										columnName = this.DatabaseTable + "." + columnName;
									}
								}
								else
								{
									columnName = entityColumnAttribute.EntityAlias + "." + columnName;
								}
							}

							sql = sql.Replace(matches[index].Value, columnName);
						}
					}
				}
			}

			return sql;
		}

		public Int32 AcceptChanges()
		{
			if (_isReadOnlyEntity)
			{
				throw new ReadOnlyException(this.GetType().Name + " is a read only entity");
			}

			Int32 result = 0;

			if (_isEntityDirty || IsMarkedForDeletion)
			{
				if (_existsOnDatabase)
				{
					if (_isMarkedForDeletion)
					{
						result = this.Delete();
					}
					else
					{
						result = this.Update();
					}
				}
				else
				{
					if (!_isMarkedForDeletion)
					{
						result = this.Insert();
					}
				}
			}

			return result;
		}

		public void ResetEntitySql()
		{
			_baseSQLIsValid = false;
		}

		public virtual Int32 BeforeDelete()
		{
			return 0;
		}

		public virtual Int32 AfterDelete()
		{
			return 0;
		}

		public virtual Int32 BeforeInsert()
		{
			return 0;
		}

		public virtual Int32 AfterInsert()
		{
			return 0;
		}

		public virtual Int32 BeforeUpdate()
		{
			if (this.PerformConcurrencyChecks)
			{
				// Re-read current entity
				Type type = this.GetType();
				Entity entity = (Entity) Activator.CreateInstance(type);

				foreach (PropertyInfo propertyInfo in type.GetProperties())
				{
					EntityColumnAttribute entityColumnAttribute = GetEntityColumnAttribute(propertyInfo);

					if (entityColumnAttribute != null)
					{
						if (entityColumnAttribute.EntityAlias.Length == 0)
						{
							if (entityColumnAttribute.IsKeyColumn)
							{
								propertyInfo.SetValue(entity, propertyInfo.GetValue(this, null), null);
							}
						}
					}
				}

				entity.ReadCurrent();

				// No check for concurrency
				String databaseCheck = entity.ConcurrencyCheck;
				String currentCheck = this.ConcurrencyCheck;

				if (databaseCheck != currentCheck)
				{
					String message = 
						this.GetType().FullName + 
						" has reported a database concurrency error on UPDATE Key Values: " + 
						this.KeyValuesToString() +
						" Database Check Value: " + databaseCheck +
						" Current Entity Value: " + currentCheck;

					DatabaseException databaseException = new DatabaseException(message);
					databaseException.EntityConcurrencyError = EntityConcurrencyError.Update;

					throw databaseException;
				}
			}

			return 0;
		}

		public virtual Int32 AfterUpdate()
		{
			return 0;
		}

		public EntityUnload Unload()
		{
			return Unload('|');
		}

		public EntityUnload Unload(Char delimiter)
		{
			EntityUnload entityUnload = new EntityUnload(this.GetType().Name);

			// Output column details into a header row
			foreach (PropertyInfo propertyInfo in this.GetType().GetProperties())
			{
				EntityColumnAttribute entityColumnAttribute = GetEntityColumnAttribute(propertyInfo);

				if (entityColumnAttribute != null && entityColumnAttribute.ColumnName.Length > 0 && entityColumnAttribute.EntityAlias.Length == 0)
				{
					entityUnload.Unload.Append(entityColumnAttribute.ColumnName);
					entityUnload.Unload.Append(delimiter);
				}
			}

			entityUnload.Unload.AppendLine();

			// Now output actual column values
			foreach (Object item in this.Read())
			{
				foreach (PropertyInfo propertyInfo in this.GetType().GetProperties())
				{
					EntityColumnAttribute entityColumnAttribute = GetEntityColumnAttribute(propertyInfo);

					if (entityColumnAttribute != null && entityColumnAttribute.ColumnName.Length > 0 && entityColumnAttribute.EntityAlias.Length == 0)
					{
						String text = PropertyValueText(propertyInfo, item, true);

						if (entityUnload.Unload.Capacity + text.Length > entityUnload.Unload.MaxCapacity)
						{
							throw new DataException("StringBuilder has reached MaxCapacity of " + entityUnload.Unload.MaxCapacity.ToString() + " for Entity " + this.GetType().Name);
						}

						entityUnload.Unload.Append(text);
						entityUnload.Unload.Append(delimiter);
					}
				}

				entityUnload.Unload.AppendLine();
			}

			return entityUnload;
		}
	}

	public sealed class EntityColumnAttribute : Attribute
	{
		public String ColumnName = "";
		public String EntityAlias = "";
		public String ColumnAlias = "";
		public Boolean IsKeyColumn = false;
		public Boolean IsIdentity = false;
		public DBDateTime DBDateTimeFrom = DBDateTime.Null;
		public DBDateTime DBDateTimeTo = DBDateTime.Null;

		private Boolean _isReadOnly = false;
		public Boolean IsReadOnly
		{
			get
			{
				return _isReadOnly;
			}
			set
			{
				_isReadOnly = (ColumnExpression != EntityColumnExpression.None ? true : value);
			}
		}

		private EntityColumnExpression _columnExpression = EntityColumnExpression.None;
		public EntityColumnExpression ColumnExpression
		{
			get 
			{ 
				return _columnExpression; 
			}
			set 
			{ 
				_columnExpression = value; 
				
				if (_columnExpression != EntityColumnExpression.None) 
				{
					this.IsReadOnly = true;
				};
			}
		}

		// Entity column definitions
		public EntityColumnAttribute(String columnName)
		{
			this.ColumnName = columnName;
			this.ColumnAlias = columnName;
		}

		public EntityColumnAttribute(String columnName, Boolean isKeyColumn)
		{
			this.ColumnName = columnName;
			this.ColumnAlias = columnName;
			this.IsKeyColumn = isKeyColumn;
		}

		public EntityColumnAttribute(String columnName, Boolean isKeyColumn, Boolean isIdentity)
		{
			this.ColumnName = columnName;
			this.ColumnAlias = columnName;
			this.IsKeyColumn = isKeyColumn;
			this.IsIdentity = isIdentity;
		}

		public EntityColumnAttribute(String columnName, DBDateTime from, DBDateTime to)
		{
			this.ColumnName = columnName;
			this.ColumnAlias = columnName;
			this.DBDateTimeFrom = from;
			this.DBDateTimeTo = to;
		}

		public EntityColumnAttribute(String columnName, DBDateTime from, DBDateTime to, EntityColumnExpression columnExpression)
		{
			this.ColumnName = columnName;
			this.ColumnAlias = columnName;
			this.DBDateTimeFrom = from;
			this.DBDateTimeTo = to;
			this.ColumnExpression = columnExpression;
		}

		public EntityColumnAttribute(String columnName, Boolean isKeyColumn, DBDateTime from, DBDateTime to)
		{
			this.ColumnName = columnName;
			this.ColumnAlias = columnName;
			this.IsKeyColumn = isKeyColumn;
			this.DBDateTimeFrom = from;
			this.DBDateTimeTo = to;
		}

		// Entity Column definitions for columns from joined tables
		public EntityColumnAttribute(String columnName, String entityAlias)
		{
			this.ColumnName = columnName;
			this.EntityAlias = entityAlias;
			this.ColumnAlias = columnName;
		}

		public EntityColumnAttribute(String columnName, String entityAlias, String columnAlias)
		{
			this.ColumnName = columnName;
			this.EntityAlias = entityAlias;
			this.ColumnAlias = columnAlias;
		}

		public EntityColumnAttribute(String columnName, String entityAlias, String columnAlias, Boolean isReadOnly)
		{
			this.ColumnName = columnName;
			this.EntityAlias = entityAlias;
			this.ColumnAlias = columnAlias;
			this.IsReadOnly = isReadOnly;
		}

		public EntityColumnAttribute(String columnName, String entityAlias, String columnAlias, DBDateTime from, DBDateTime to)
		{
			this.ColumnName = columnName;
			this.EntityAlias = entityAlias;
			this.ColumnAlias = columnAlias;
			this.DBDateTimeFrom = from;
			this.DBDateTimeTo = to;
		}

		public EntityColumnAttribute(String columnName, String entityAlias, String columnAlias, DBDateTime from, DBDateTime to, Boolean isReadOnly)
		{
			this.ColumnName = columnName;
			this.EntityAlias = entityAlias;
			this.ColumnAlias = columnAlias;
			this.DBDateTimeFrom = from;
			this.DBDateTimeTo = to;
			this.IsReadOnly = isReadOnly;
		}
	}

	public sealed class EntityColumnConversionAttribute : Attribute
	{
		public String DBToNative = "";
		public String NativeToDB = "";
		public String Class = "";
		public String Namespace = "";
		public String Assembly = "";

		public EntityColumnConversionAttribute(String DBToNative, String NativeToDB)
		{
			this.DBToNative = DBToNative;
			this.NativeToDB = NativeToDB;
		}

		public EntityColumnConversionAttribute(String DBToNative, String NativeToDB, String Class)
		{
			this.DBToNative = DBToNative;
			this.NativeToDB = NativeToDB;
			this.Class = Class;
		}

		public EntityColumnConversionAttribute(String DBToNative, String NativeToDB, String Class, String Namespace, String Assembly)
		{
			this.DBToNative = DBToNative;
			this.NativeToDB = NativeToDB;
			this.Class = Class;
			this.Namespace = Namespace;
			this.Assembly = Assembly;
		}
	}

	public sealed class Selectable : Attribute
	{
		public Boolean CanSelect = true;

		public Selectable(Boolean canSelect)
		{
			this.CanSelect = canSelect;
		}
	}

	public enum DBDateTime
	{
		Year,
		Month,
		Day,
		Hour,
		Minute,
		Second,
		Null
	}

	public class RelatedEntityException : Exception
	{
		public RelatedEntityException()
		{
		}

		public RelatedEntityException(String message)
			: base(message)
		{
		}

		public RelatedEntityException(String message, Exception innerException)
			: base(message, innerException)
		{
		}

		public RelatedEntityException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}

	public enum EntityColumnExpression
	{
		None,
		Function,
		Count,
		CountColumn,
		CountDistinctColumn,
		Average,
		AverageDistinct,
		Min,
		MinDistinct,
		Max,
		MaxDistinct,
		Sum,
		SumDistinct
	}
	
	public class DatabaseException : Exception
	{
		private Boolean _isLockError = false;
		private EntityConcurrencyError _entityConcurrencyError = EntityConcurrencyError.None;

		public Boolean IsLockError
		{
			get { return _isLockError; }
			set { _isLockError = value; }
		}

		public EntityConcurrencyError EntityConcurrencyError
		{
			get { return _entityConcurrencyError; }
			set { _entityConcurrencyError = value; }
		}

		public DatabaseException()
		{
		}

		public DatabaseException(String message)
			: base(message)
		{
		}

		public DatabaseException(String message, Exception innerException)
			: base(message, innerException)
		{
		}

		public DatabaseException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}

	public sealed class EntityColumnWidthAttribute : Attribute
	{
		public Int32 Width;

		public EntityColumnWidthAttribute(Int32 width)
		{
			this.Width = width;
		}
	}

	public sealed class DisplayOrderAttribute : Attribute
	{
		public Int32 Order;

		public DisplayOrderAttribute(Int32 order)
		{
			this.Order = order;
		}
	}

	public enum EntityConcurrencyError
	{
		None,
		Update,
		Insert
	}

	#region Create Text Delimited Unloads

	public class EntityUnload
	{
		private String _fileExtension = ".unl";
		private String _name = "";
		private StringBuilder _unload = new StringBuilder();

		public String FileExtension
		{
			get { return _fileExtension; }
			set { _fileExtension = value; }
		}

		public String Name
		{
			get { return _name; }
		}

		public String FileName
		{
			get { return _name + _fileExtension; }
		}

		public StringBuilder Unload
		{
			get { return _unload; }
		}

		public String UnloadString
		{
			get { return _unload.ToString(); }
		}

		public MemoryStream UnloadStream
		{
			get
			{
				Byte[] bytes = Encoding.ASCII.GetBytes(_unload.ToString());
				MemoryStream memoryStream = new MemoryStream(bytes);
				return memoryStream;
			}
		}

		public EntityUnload(String name)
		{
			_name = name;
		}
	}

	#endregion

	#region EntityListView for use with DataGridViews

	public class EntityListView<T> : BindingListView<T>
	{
		public EntityListView(ExtendedBindingList<T> extendedBindingList)
			: base(extendedBindingList)
		{
			if (!(typeof(T).IsSubclassOf(typeof(Entity))))
			{
				throw new ArgumentException("EntityListView: Member type must be derived from base type Evolve.Libraries.Data.Remoting.Entity");
			}

			this.IsDeletedPropertyName = "IsMarkedForDeletion";
		}
	}

	#endregion

	#region ExtensionMethods

	public static class EntityExtentionMethods
	{
		public static Int32 Save<T>(this IEnumerable<T> entities) where T : Entity
		{
			return entities.Save("", 0);
		}

		public static Int32 Save<T>(this IEnumerable<T> entities, String parentIdPropertyName, Int32 id) where T : Entity
		{
			Int32 result = 0;
			PropertyInfo propertyInfo = null;

			if (!String.IsNullOrEmpty(parentIdPropertyName))
			{
				Type genericMemberType = entities.GetType().GetGenericArguments()[0];
				propertyInfo = genericMemberType.GetProperty(parentIdPropertyName);

				if (propertyInfo == null)
				{
					throw new ArgumentException("Property " + parentIdPropertyName + " does not exist in " + genericMemberType.Name);
				}
			}

			foreach (Entity entity in entities)
			{
				if (propertyInfo != null)
				{
					propertyInfo.SetValue(entity, id, null);
				}

				result = entity.AcceptChanges();

				if (result < 0)
				{
					break;
				}
			}

			return result;
		}


		public static int SqlBulkCopy<T>(this IEnumerable<T> entities) where T : SqlServerEntity
		{
			DataConnection dataConnection = new DataConnection();
			dataConnection.OpenConnection();

			int result = SqlServerUtilities.SqlBulkCopy(dataConnection.SqlConnection, dataConnection.SqlTransaction, entities);

			dataConnection.CommitTransaction();
			dataConnection.CloseConnection();

			return result;
		}

		public static Int32 MultiThreadedBulkInsert<T>(this IEnumerable<T> entities, int maximumThreads = 10) where T : OracleEntity
		{
			Int32 inserted = 0;

			// Build bulk Sqls
			List<String> sqls = BulkInsertSql(entities);

			if (sqls.Count > 0)
			{
				Int32 maxThreadCount = (sqls.Count > maximumThreads ? maximumThreads : sqls.Count);
				ManualResetEvent[] doneEvents = new ManualResetEvent[maxThreadCount];
				MultiThreadedSql[] multiThreadedSqls = new MultiThreadedSql[maxThreadCount];
				DataConnection[] dataConnections = new DataConnection[maxThreadCount];
				BackgroundWorker[] backgroundWorkers = new BackgroundWorker[maxThreadCount];
				Thread[] threads = new Thread[maxThreadCount];

				// Queue background executions using the TheadPool (maximum of 64 entries can exist
				// in threadpool queue for ManualResetEvents)
				Int32 index = 0;
				Int32 threadIndex = 0;

				do
				{
					if (index < maxThreadCount)
					{
						dataConnections[index] = new DataConnection();
						SqlResponse sqlResponse = dataConnections[index].OpenConnection();
					}

					//doneEvents[threadPoolIndex] = new ManualResetEvent(false);
					MultiThreadedSql mtSql = new MultiThreadedSql(dataConnections[threadIndex], sqls[index], doneEvents[threadIndex]);
					multiThreadedSqls[threadIndex] = mtSql;
					threads[threadIndex] = mtSql.ExecuteSqlAsync();

					threadIndex++;
					index++;

					if (threadIndex == maxThreadCount)
					{
						// Wait for all threads to complete before sending next batch
						for (Int32 ti = 0; ti < maxThreadCount; ti++)
						{
							threads[ti].Join();
						}

						// Accumulate results
						for (Int32 insertedIndex = 0; insertedIndex < threadIndex; insertedIndex++)
						{
							inserted += multiThreadedSqls[insertedIndex].Result;
						}

						threadIndex = 0;
					}
				} while (index < sqls.Count);

				// Wait for any remaining entries in the queue to complete
				if (threadIndex > 0)
				{
					// Wait for all threads to complete before sending next batch
					for (Int32 ti = 0; ti < threadIndex; ti++)
					{
						threads[ti].Join();
					}

					// Combine results
					for (Int32 insertedIndex = 0; insertedIndex < threadIndex; insertedIndex++)
					{
						inserted += multiThreadedSqls[insertedIndex].Result;
					}
				}

				if (maxThreadCount > 0)
				{
					for (Int32 connectionIndex = 0; connectionIndex < maxThreadCount; connectionIndex++)
					{
						try
						{
							dataConnections[connectionIndex].CommitTransaction();
							dataConnections[connectionIndex].CloseConnection();
						}
						catch
						{
							inserted = -1;
						}
						finally
						{
							dataConnections[connectionIndex] = null;
						}
					}
				}
			}

			return inserted;
		}

		public static Int32 BulkInsert<T>(this IEnumerable<T> entities) where T : OracleEntity
		{
			Int32 result = 0;
			Int32 rowsInserted = 0;

			if (entities.Count() > 0)
			{
				if (DataAccessServer.PersistentConnection == null)
				{
					throw new DatabaseException("NULL data connection within BulkInsert()");
				}

				List<String> sqls = BulkInsertSql(entities);

				// Now execute statement
				foreach (String sql in sqls)
				{
					rowsInserted = ExecuteBulkInsert(sql);

					if (rowsInserted < 0)
					{
						result = rowsInserted;
					}
					else
					{
						result += rowsInserted;
					}
				}
			}

			return result;
		}

		private static List<String> BulkInsertSql<T>(this IEnumerable<T> entities) where T : OracleEntity
		{
			List<String> sqls = new List<String>();
			Int32 columnCount = 0;
			Int32 columnsInSql = 0;

			if (entities.Count() > 0)
			{
				// Build bulk Sql
				String sql = "insert all";
				String columns = "( ";
				Boolean isFirst = true;

				foreach (Entity entity in entities)
				{
					// Check if we have reached the maximum possible columns in Oracle Bulk Insert
					columnsInSql += columnCount;

					if (columnsInSql > 750)
					{
						sql += " select * from dual";
						sqls.Add(sql);

						columnsInSql = columnCount;
						sql = "insert all";
					}

					String values = "( ";

					foreach (PropertyInfo propertyInfo in entity.GetType().GetProperties())
					{
						EntityColumnAttribute entityColumnAttribute = entity.GetEntityColumnAttribute(propertyInfo);

						if (entityColumnAttribute != null)
						{
							if (entityColumnAttribute.ColumnName.Length > 0 &&
								entityColumnAttribute.EntityAlias.Length == 0 &&
								!entityColumnAttribute.IsReadOnly &&
								(entityColumnAttribute.IsIdentity == false || entity.IncludeIdentityColumnInInsert()))
							{
								if (isFirst)
								{
									columns += entityColumnAttribute.ColumnName + ", ";
									columnCount++;
								}

								values += entity.PropertyValueText(propertyInfo) + ", ";
							}
						}
					}

					if (isFirst)
					{
						columns = columns.Substring(0, columns.Length - 2) + " )";
						isFirst = false;
					}

					sql += " into " + entity.DatabaseTable + " " + columns + " values " + values.Substring(0, values.Length - 2) + " )";
				}

				sql += " select * from dual";

				// Now add sql to collection
				sqls.Add(sql);
			}

			return sqls;
		}

		private static Int32 ExecuteBulkInsert(String sql)
		{
			return ExecuteBulkInsert(sql, DataAccessServer.PersistentConnection);
		}

		private static Int32 ExecuteBulkInsert(String sql, DataConnection dataConnection)
		{
			Int32 result = 0;
			SqlResponse sqlResponse = dataConnection.ExecuteSql(sql);

			if (sqlResponse == null || sqlResponse.HasError)
			{
				String exceptionText = "NULL SqlResponse: " + sql;

				if (sqlResponse != null)
				{
					exceptionText = sqlResponse.Message + ": " + sql;
				}

				DatabaseException databaseException = new DatabaseException(exceptionText);
				databaseException.IsLockError = (sqlResponse == null ? false : sqlResponse.IsLockError);

				throw databaseException;
			}
			else
			{
				result = Convert.ToInt32(sqlResponse.Results[0]);
			}

			return result;
		}
	}

	#endregion
}

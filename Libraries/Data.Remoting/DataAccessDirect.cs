﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Net;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;
using System.Data.SqlClient;

namespace Evolve.Libraries.Data.Remoting
{
	public class DataAccessDirect : IDataAccess
	{
		private DbConnection _dbConnection;
		private DbProviderFactory _dbProviderFactory;
		private DbTransaction _dbTransaction;
		private UserCredentials _userCredentials;
		private Guid _guid;
		private Boolean _logAuthentificationExceptions = false;
		private DbReadMode _dbTableReadMode = DbReadMode.DbDataAdapter;
		private DbReadMode _dbStoredProcedureReadMode = DbReadMode.DbDataAdapter;
		private String _provider;

		public Boolean IsTransactionOpen
		{
			get { return _dbTransaction != null; }
		}
		
		public SqlConnection SqlConnection
		{
			get
			{
				if (_dbConnection != null && _dbConnection is SqlConnection)
					return (SqlConnection) _dbConnection;

				return null;
			}
		}

		public SqlTransaction SqlTransaction
		{
			get
			{
				if (_dbTransaction != null && _dbTransaction is SqlTransaction)
					return (SqlTransaction)_dbTransaction;

				return null;
			}
		}

		public Guid Guid
		{
			get { return _guid; }
		}

		public UserCredentials UserCredentials
		{
			get { return _userCredentials; }
		}

		public DataAccessDirect(UserCredentials userCredentials)
		{
		    _userCredentials = userCredentials;
			_guid = Guid.NewGuid();

			InitializeComponent();
		}

		private void InitializeComponent()
		{
			DataAccessEngine.InitializeComponent(_userCredentials, _guid, ref _provider, ref _dbProviderFactory, ref _dbConnection, ref _logAuthentificationExceptions, ref _dbTableReadMode, ref _dbStoredProcedureReadMode);
		}

		~DataAccessDirect()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private void Dispose(Boolean isDisposing)
		{
			DataAccessEngine.DisposeOfDataAccess(isDisposing, ref _userCredentials, _guid, ref _provider, ref _dbProviderFactory, ref _dbConnection, ref _dbTransaction);
		}

		public Boolean AuthenticateUser()
		{
			return DataAccessEngine.AuthenticateUser(ref _userCredentials, _guid, _dbProviderFactory, ref _dbConnection, ref _dbTransaction, _logAuthentificationExceptions);
		}

		public SqlResponse OpenConnection()
		{
			return DataAccessEngine.OpenConnection(_userCredentials, _guid, _provider, _dbProviderFactory, ref _dbConnection, ref _dbTransaction, _logAuthentificationExceptions);
		}

		public SqlResponse OpenConnection(IsolationLevel isolationLevel)
		{
			return DataAccessEngine.OpenConnection(_userCredentials, _guid, _provider, _dbProviderFactory, ref _dbConnection, ref _dbTransaction, isolationLevel, _logAuthentificationExceptions);
		}

		private SqlResponse OpenConnection(IsolationLevel isolationLevel, Boolean isAuthenticateUser)
		{
			return DataAccessEngine.OpenConnection(_userCredentials, _guid, _provider, _dbProviderFactory, ref _dbConnection, ref _dbTransaction, isolationLevel, isAuthenticateUser, _logAuthentificationExceptions);
		}

		public SqlResponse CloseConnection()
		{
			return DataAccessEngine.CloseConnection(_userCredentials, _guid, ref _dbConnection, ref _dbTransaction);
		}

		private SqlResponse CloseConnection(Boolean isAuthenticateUser)
		{
			return DataAccessEngine.CloseConnection(_userCredentials, _guid, ref _dbConnection, ref _dbTransaction, isAuthenticateUser);
		}

		public SqlResponse BeginTransaction()
		{
			return DataAccessEngine.BeginTransaction(_userCredentials, _guid, _provider, _dbProviderFactory, ref _dbConnection, ref _dbTransaction);
		}

		public SqlResponse BeginTransaction(IsolationLevel isolationLevel)
		{
			return DataAccessEngine.BeginTransaction(_userCredentials, _guid, _provider, _dbProviderFactory, ref _dbConnection, ref _dbTransaction, isolationLevel);
		}

		public SqlResponse CommitTransaction()
		{
			return DataAccessEngine.CommitTransaction(_userCredentials, _guid, ref _dbConnection, ref _dbTransaction);
		}

		public SqlResponse RollbackTransaction()
		{
			return DataAccessEngine.RollbackTransaction(_userCredentials, _guid, _provider, _dbProviderFactory, ref _dbConnection, ref _dbTransaction);
		}

		public SqlResponse ExecuteSql(String sql)
		{
			return DataAccessEngine.ExecuteSql(_userCredentials, _guid, _provider, _dbProviderFactory, ref _dbConnection, ref _dbTransaction, sql);
		}

		public SqlResponse ExecuteScalar(String sql)
		{
			return DataAccessEngine.ExecuteScalar(_userCredentials, _guid, _dbProviderFactory, ref _dbConnection, ref _dbTransaction, sql);
		}

		public SqlResponse ExecuteSqlToDataTable(String sql)
		{
			return DataAccessEngine.ExecuteSqlToDataTable(_userCredentials, _guid, _dbProviderFactory, ref _dbConnection, ref _dbTransaction, _dbTableReadMode, sql);
		}

		public SqlResponse ExecuteProcedure(String procedureName, Object[] parameters)
		{
			return DataAccessEngine.ExecuteProcedure(_userCredentials, _guid, _dbProviderFactory, ref _dbConnection, ref _dbTransaction, _dbTableReadMode, procedureName, parameters);
		}

		public void RenewUserCredentials(UserCredentials userCredentials)
		{
			DataAccessEngine.RenewUserCredentials(ref _userCredentials, userCredentials, _guid, ref _provider, ref _dbProviderFactory, ref _dbConnection);
		}
	}
}

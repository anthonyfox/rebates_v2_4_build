﻿using System;
using System.Collections;
using System.Net;

namespace Evolve.Libraries.Data.Remoting
{
	public static class DataAccessConfiguration
	{
		private static String _hostName = Dns.GetHostName();
		private static Hashtable _settings = null;

		public static String HostName
		{
			get { return (DataAccessServer.IsDirect ? "DIRECT" : _hostName); }
			set { _hostName = value; }
		}

		public static Hashtable Settings
		{
			get { return _settings; }
			set { _settings = value; }
		}
	}
}

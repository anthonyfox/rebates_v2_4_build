﻿using System;
using System.Reflection;
using Evolve.Libraries.SubVersion;

namespace Evolve.Libraries.Data.Remoting
{
	/// <summary>
	/// The Sql Response class holds information relating to the success or failure of any
	/// database access
	/// </summary>
	[Serializable]
	public class SqlResponse
	{
		#region Properties

		private Boolean _hasError = false;
		private Boolean _isLockError = false;
		private Boolean _isAuthentificationError = false;
		private Boolean _isDuplicate = false;
		private String _message = "";
		private String _sql;
		private Object[] _results;
		private String _hostName;
		private Guid _connectionId;

		public Boolean HasError
		{
			get { return _hasError; }
		}

		public Boolean IsLockError
		{
			get { return _isLockError; }
		}

		public Boolean IsAuthentificationError
		{
			get { return _isAuthentificationError; }
		}

		public Boolean IsDuplicate
		{
			get { return _isDuplicate; }
		}

		public String Message
		{
			get { return _message; }
		}

		public String Sql
		{
			get { return _sql; }
			set { _sql = value; }
		}

		public Object[] Results
		{
			get { return _results; }
			set { _results = value; }
		}

		public String HostName
		{
			get { return _hostName; }
			set { _hostName = value; }
		}

		public Guid ConnectionId
		{
			get { return _connectionId; }
			set { _connectionId = value; }
		}

		#endregion

		#region Constructors

		public SqlResponse()
		{
		}

		public SqlResponse(Guid connectionId, String hostName)
		{
			_connectionId = connectionId;
			_hostName = hostName;
		}

		public SqlResponse(Guid connectionId, String hostName, String sql)
		{
			_connectionId = connectionId;
			_hostName = hostName;
			_sql = sql;
		}

		#endregion

		#region Public Methods

		public void ParseException(Exception ex)
		{
			_hasError = (ex != null);

			if (ex != null)
			{
				_message = "[" + SubVersion.Build.Revision + (_hostName.Length > 0 ? ", " + _hostName : "");

				if (_connectionId != null)
				{
					_message += ": " + _connectionId.ToString();
				}

				_message += "] " + ex.Message;

				// Log native error code if its an Informix exception (use reflection in order to
				// remove the need to add a reference to the DLL, we can therefore switch to and from
				// older versions of the Informix Connect software without re-compiles)
				Type exceptionType = ex.GetType();

				switch (exceptionType.Name)
				{
					case "IfxException":
						PropertyInfo errorsPropertyInfo = exceptionType.GetProperty("Errors");

						if (errorsPropertyInfo != null)
						{
							Object ifxErrorCollection = errorsPropertyInfo.GetValue(ex, null);

							if (ifxErrorCollection != null)
							{
								Type ifxCollectionType = ifxErrorCollection.GetType();
								PropertyInfo countPropertyInfo = ifxCollectionType.GetProperty("Count");
								PropertyInfo itemPropertyInfo = ifxCollectionType.GetProperty("Item");

								if (countPropertyInfo != null && itemPropertyInfo != null)
								{
									Int32 count = (Int32) countPropertyInfo.GetValue(ifxErrorCollection, null);

									if (count > 0)
									{
										Object ifxError = itemPropertyInfo.GetValue(ifxErrorCollection, new Object[] { 0 });

										if (ifxError != null)
										{
											Type ifxErrorType = ifxError.GetType();
											PropertyInfo nativeErrorPropertyInfo = ifxErrorType.GetProperty("NativeError");

											if (nativeErrorPropertyInfo != null)
											{
												Int32 nativeError = (Int32) nativeErrorPropertyInfo.GetValue(ifxError, null);
												_message += " (" + nativeError.ToString() + ")";

												if (nativeError == -244)
												{
													_isLockError = true;
												}
											}
										}
									}
								}
							}
						}

						break;

					case "OdbcException":
						if (ex.Message.StartsWith("ERROR [HY000] [Informix][Informix ODBC Driver][Informix]Could not do a physical-order read to fetch next row") ||
							ex.Message.StartsWith("ERROR [HY000] [Informix][Informix ODBC Driver][Informix]Cannot read system catalog") ||
							ex.Message.StartsWith("ERROR [HY000] [Informix][Informix ODBC Driver][Informix]Could not position within a file via an index") ||
							ex.Message.StartsWith("ERROR [HY000] [Informix][Informix ODBC Driver][Informix]Could not position within a table"))
						{
							_isLockError = true;
						}

						if (ex.Message.StartsWith("ERROR [HY000] [Informix][Informix ODBC Driver][-11302] Insufficient Connection information was supplied") ||
							ex.Message.StartsWith("ERROR [28000] [Informix][Informix ODBC Driver][Informix]User") ||
							ex.Message.StartsWith("ERROR [28000] [Informix][Informix ODBC Driver][Informix]Incorrect password or user"))
						{
							_isAuthentificationError = true;
						}

						if (ex.Message.StartsWith("ERROR [23000] [Informix][Informix ODBC Driver][Informix]Could not insert new row - duplicate value"))
						{
							_isDuplicate = true;
						}

						break;

					case "SqlClient":
						if (ex.Message.StartsWith("[708"))
						{
							_isLockError = true;
						}

						break;

					case "OracleException":
						if (ex.Message.StartsWith("ORA-00054: resource busy"))
						{
							_isLockError = true;
						}

						break;

					default:
						_isAuthentificationError = true;
						break;
				}
			}
		}

		#endregion
	}
}

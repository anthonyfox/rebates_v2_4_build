﻿using System;
using System.DirectoryServices.AccountManagement;

namespace Evolve.Libraries.Data.Remoting
{
	public interface IAuthentificateUserFactory
	{
		IAuthentificateUser GetNewInstance(String windowsUserId);
	}

	public interface IAuthentificateUser
	{
		String Environments { get; }
		UserCredentials ReadCredentials(String environment, String userId, Boolean needsDatabaseId);
		void SaveCredentials(UserCredentials userCredentials, Boolean needsDatabaseId);
	}
}

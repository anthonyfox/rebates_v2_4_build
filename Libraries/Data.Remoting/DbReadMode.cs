﻿using System;

namespace Evolve.Libraries.Data.Remoting
{
	public enum DbReadMode
	{
		DbDataReader,
		DbDataAdapter
	}
}

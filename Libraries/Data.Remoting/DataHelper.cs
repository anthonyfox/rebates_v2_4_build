using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using Evolve.Libraries.Utilities;

namespace Evolve.Libraries.Data.Remoting
{
	public class DataHelper
	{
		public static DataTable BindingListToDataTable<T>(ExtendedBindingList<T> bindingList)
		{
			return BindingListToDataTable((BindingList<T>) bindingList);
		}

		public static DataTable BindingListToDataTable<T>(BindingList<T> bindingList)
		{
			DataTable dataTable = null;

			if (bindingList != null)
			{
				dataTable = new DataTable();

				foreach (PropertyInfo info in typeof(T).GetProperties())
				{
                    Object[] attributes = info.GetCustomAttributes(typeof(DisplayNameAttribute), true);

                    if (attributes.Length > 0)
                    {
                        DisplayNameAttribute displayNameAttribute = (DisplayNameAttribute) attributes[0];
                        dataTable.Columns.Add(new DataColumn(displayNameAttribute.DisplayName, info.PropertyType));
                    }
                    else
                    {
                        dataTable.Columns.Add(new DataColumn(info.Name, info.PropertyType));
                    }
				}

				foreach (T t in bindingList)
				{
					DataRow row = dataTable.NewRow();
                    Int32 columnIndex = 0;

					foreach (PropertyInfo info in typeof(T).GetProperties())
					{
						row[columnIndex] = info.GetValue(t, null);
                        columnIndex++;
					}

					dataTable.Rows.Add(row);
				}
			}

			return dataTable;
		}
	}
}

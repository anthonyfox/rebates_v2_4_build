﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;

namespace Evolve.Libraries.Data.Remoting
{
	public class SqlServerUtilities
	{
		public static int SqlBulkCopy<T>(
			SqlConnection sqlConnection,
			SqlTransaction sqlTransaction,
			IEnumerable<T> entities)
		{
			int result = 0;

			if (entities != null && entities.Any())
			{
				object firstItem = entities.First();

				if (firstItem != null)
				{
					Entity entity = (Entity)firstItem;
					SqlBulkCopy sqlBulkCopy = CreateSqlBulkCopyObject(sqlConnection, sqlTransaction, entity);

					if (sqlBulkCopy != null)
					{
						DataTable dataTable = EntityCollectionToDataTable(entities);
						sqlBulkCopy.WriteToServer(dataTable);
						result = entities.Count();
					}
				}
			}

			return result;
		}

		private static SqlBulkCopy CreateSqlBulkCopyObject(
			SqlConnection sqlConnection,
			SqlTransaction sqlTransaction,
			Entity entity,
			int batchSize = 1000,
			int bulkCopyTimeout = 60,
			SqlBulkCopyOptions options = SqlBulkCopyOptions.Default)
		{
			SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(sqlConnection, options, sqlTransaction)
			{
				DestinationTableName = entity.DatabaseTable,
				BatchSize = batchSize,
				BulkCopyTimeout = bulkCopyTimeout
			};

			PropertyDescriptorCollection propertyDescriptors = TypeDescriptor.GetProperties(entity);

			foreach (PropertyInfo propertyInfo in entity.GetType().GetProperties().OrderBy(p => p.MetadataToken))
			{
				EntityColumnAttribute entityColumnAttribute = entity.GetEntityColumnAttribute(propertyInfo);

				if (entityColumnAttribute != null)
				{
					if (entityColumnAttribute.ColumnName.Length > 0 &&
						entityColumnAttribute.EntityAlias.Length == 0 &&
						!entityColumnAttribute.IsReadOnly &&
						(entityColumnAttribute.IsIdentity == false || entity.IncludeIdentityColumnInInsert()))
					{
						sqlBulkCopy.ColumnMappings.Add(propertyInfo.Name, entityColumnAttribute.ColumnName.ToUpper());
					}
				}
			}

			return sqlBulkCopy;
		}

		/// <summary>
		/// Create a DataTable from an IEnumerable collection
		/// </summary>
		/// <param name="items"></param>
		/// <returns>DataTable</returns>
		public static DataTable EntityCollectionToDataTable<T>(IEnumerable<T> items)
		{
			Type type = typeof(T);
			DataTable dataTable = new DataTable();
			PropertyDescriptorCollection propertyDescriptors = TypeDescriptor.GetProperties(type);
			object itemObject = items.First();
			Entity entity = (Entity)itemObject;

			foreach (var propertyInfo in type.GetProperties().OrderBy(p => p.MetadataToken))
			{
				EntityColumnAttribute entityColumnAttribute = entity.GetEntityColumnAttribute(propertyInfo);

				if (entityColumnAttribute != null)
				{
					if (entityColumnAttribute.ColumnName.Length > 0 &&
						entityColumnAttribute.EntityAlias.Length == 0 &&
						!entityColumnAttribute.IsReadOnly &&
						(entityColumnAttribute.IsIdentity == false || entity.IncludeIdentityColumnInInsert()))
					{
						var dataColumn = new DataColumn(propertyInfo.Name,
						Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType);
						dataTable.Columns.Add(dataColumn);
					}
				}
			}

			foreach (var item in items)
			{
				var dataRow = dataTable.NewRow();

				foreach (var propertyInfo in type.GetProperties().OrderBy(p => p.MetadataToken))
				{
					if (dataTable.Columns[propertyInfo.Name] != null)
						dataRow[propertyInfo.Name] = propertyInfo.GetValue(item, null) ?? DBNull.Value;
				}

				dataTable.Rows.Add(dataRow);
			}

			return dataTable;
		}
	}
}

﻿using System;
using System.Data;
using Evolve.Libraries.Utilities;
using System.Data.SqlClient;

namespace Evolve.Libraries.Data.Remoting
{
	public interface IDataAccessFactory
	{
		IDataAccess GetNewInstance(UserCredentials userCredentials, Boolean isPersistentConnection);
		void SetLogLevel(LoggerLevel logLevel);
	}

	public interface IDataAccess
	{
		Boolean IsTransactionOpen { get; }
		Guid Guid { get; }
		UserCredentials UserCredentials { get; }
		SqlConnection SqlConnection { get; }
		SqlTransaction SqlTransaction { get; }

		Boolean AuthenticateUser();
		SqlResponse OpenConnection();
		SqlResponse CloseConnection();
		SqlResponse BeginTransaction();
		SqlResponse BeginTransaction(IsolationLevel isolationLevel);
		SqlResponse CommitTransaction();
		SqlResponse RollbackTransaction();
		SqlResponse ExecuteSql(String sql);
		SqlResponse ExecuteScalar(String sql);
		SqlResponse ExecuteSqlToDataTable(String sql);
		SqlResponse ExecuteProcedure(String procedureName, Object[] parameters);
		void RenewUserCredentials(UserCredentials userCredentials);
	}
}

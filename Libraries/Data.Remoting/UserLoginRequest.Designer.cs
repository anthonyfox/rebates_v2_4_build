﻿namespace Evolve.Libraries.Data.Remoting
{
	partial class UserLoginRequest
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this._userIDLabel = new System.Windows.Forms.Label();
			this._passwordLabel = new System.Windows.Forms.Label();
			this._userId = new System.Windows.Forms.TextBox();
			this._password = new System.Windows.Forms.TextBox();
			this._message = new System.Windows.Forms.Label();
			this._login = new System.Windows.Forms.Button();
			this._cancel = new System.Windows.Forms.Button();
			this._environmentLabel = new System.Windows.Forms.Label();
			this._environment = new System.Windows.Forms.ComboBox();
			this.SuspendLayout();
			// 
			// _userIDLabel
			// 
			this._userIDLabel.AutoSize = true;
			this._userIDLabel.Location = new System.Drawing.Point(40, 91);
			this._userIDLabel.Name = "_userIDLabel";
			this._userIDLabel.Size = new System.Drawing.Size(43, 13);
			this._userIDLabel.TabIndex = 1;
			this._userIDLabel.Text = "User ID";
			// 
			// _passwordLabel
			// 
			this._passwordLabel.AutoSize = true;
			this._passwordLabel.Location = new System.Drawing.Point(30, 114);
			this._passwordLabel.Name = "_passwordLabel";
			this._passwordLabel.Size = new System.Drawing.Size(53, 13);
			this._passwordLabel.TabIndex = 3;
			this._passwordLabel.Text = "Password";
			// 
			// _userId
			// 
			this._userId.Location = new System.Drawing.Point(89, 89);
			this._userId.MaxLength = 10;
			this._userId.Name = "_userId";
			this._userId.Size = new System.Drawing.Size(83, 20);
			this._userId.TabIndex = 2;
			// 
			// _password
			// 
			this._password.Location = new System.Drawing.Point(89, 112);
			this._password.Name = "_password";
			this._password.Size = new System.Drawing.Size(83, 20);
			this._password.TabIndex = 4;
			this._password.UseSystemPasswordChar = true;
			// 
			// _message
			// 
			this._message.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
			this._message.Location = new System.Drawing.Point(17, 9);
			this._message.Name = "_message";
			this._message.Size = new System.Drawing.Size(251, 39);
			this._message.TabIndex = 0;
			this._message.Text = "Your user credentials have expired, or are invalid. Please confirm your user iden" +
				"tity and password";
			this._message.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// _login
			// 
			this._login.Location = new System.Drawing.Point(33, 152);
			this._login.Name = "_login";
			this._login.Size = new System.Drawing.Size(75, 23);
			this._login.TabIndex = 5;
			this._login.Text = "Login";
			this._login.UseVisualStyleBackColor = true;
			// 
			// _cancel
			// 
			this._cancel.Location = new System.Drawing.Point(114, 152);
			this._cancel.Name = "_cancel";
			this._cancel.Size = new System.Drawing.Size(75, 23);
			this._cancel.TabIndex = 6;
			this._cancel.Text = "Cancel";
			this._cancel.UseVisualStyleBackColor = true;
			// 
			// _environmentLabel
			// 
			this._environmentLabel.AutoSize = true;
			this._environmentLabel.Location = new System.Drawing.Point(17, 68);
			this._environmentLabel.Name = "_environmentLabel";
			this._environmentLabel.Size = new System.Drawing.Size(66, 13);
			this._environmentLabel.TabIndex = 7;
			this._environmentLabel.Text = "Environment";
			// 
			// _environment
			// 
			this._environment.Enabled = false;
			this._environment.FormattingEnabled = true;
			this._environment.Location = new System.Drawing.Point(89, 65);
			this._environment.Name = "_environment";
			this._environment.Size = new System.Drawing.Size(121, 21);
			this._environment.TabIndex = 8;
			// 
			// UserLoginRequest
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(284, 188);
			this.Controls.Add(this._environment);
			this.Controls.Add(this._environmentLabel);
			this.Controls.Add(this._cancel);
			this.Controls.Add(this._login);
			this.Controls.Add(this._message);
			this.Controls.Add(this._password);
			this.Controls.Add(this._userId);
			this.Controls.Add(this._passwordLabel);
			this.Controls.Add(this._userIDLabel);
			this.Name = "UserLoginRequest";
			this.Text = "User Credentials";
			this.Load += new System.EventHandler(this.UserLoginRequest_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label _userIDLabel;
		private System.Windows.Forms.Label _passwordLabel;
		private System.Windows.Forms.TextBox _userId;
		private System.Windows.Forms.TextBox _password;
		private System.Windows.Forms.Label _message;
		private System.Windows.Forms.Button _login;
		private System.Windows.Forms.Button _cancel;
		private System.Windows.Forms.Label _environmentLabel;
		private System.Windows.Forms.ComboBox _environment;
	}
}
﻿using System;
using System.Threading;
using Evolve.Libraries.Utilities;

namespace Evolve.Libraries.Data.Remoting
{
	public class MultiThreadedSql
	{
		private ManualResetEvent _doneEvent;
		private ManualResetEvent DoneEvent { get { return _doneEvent; } set { _doneEvent = value; } }

		private String _sql;
		private String Sql { get { return _sql; } set { _sql = value; } }

		private Int32 _result;
		public Int32 Result { get { return _result; } private set { _result = value; } }

		private DataConnection _dataConnection;
		public DataConnection DataConnection { get { return _dataConnection; } set { _dataConnection = value; } }

		public MultiThreadedSql(String sql, ManualResetEvent doneEvent)
		{
			this.Sql = sql;
			this.DoneEvent = doneEvent;
		}

		public MultiThreadedSql(DataConnection dataConnection, String sql, ManualResetEvent doneEvent)
		{
			this.DataConnection = dataConnection;
			this.Sql = sql;
			this.DoneEvent = doneEvent;
		}

		public Thread ExecuteSqlAsync()
		{
			Thread thread = new Thread(this.ExecuteSql);
			thread.Start();
			return thread;
		}

		// Execution method
		public void ExecuteSql()
		{
			DataConnection dataConnection = this.DataConnection;
			Int32 result = 0;

			try
			{
				SqlResponse sqlResponse = null;

				if (this.DataConnection == null)
				{
					dataConnection = new DataConnection();
					sqlResponse = dataConnection.OpenConnection();
				}

				sqlResponse = dataConnection.ExecuteSql(this.Sql);
				result = (Int32) sqlResponse.Results[0];

				if (this.DataConnection == null)
				{
					dataConnection.CommitTransaction();
				}
			}
			catch
			{
				result = -1;

				if (this.DataConnection == null && dataConnection != null)
				{
					dataConnection.RollbackTransaction();
				}
			}
			finally
			{
				if (this.DataConnection == null && dataConnection != null)
				{
					dataConnection.CloseConnection();
					dataConnection = null;
				}
			}

			this.Result = result;
		}
	}
}

﻿using System;
using System.ComponentModel;
using System.DirectoryServices.AccountManagement;
using System.Windows.Forms;

namespace Evolve.Libraries.Data.Remoting
{
	public partial class UserLoginRequest : Form
	{
		private String _defaultEnvironment;
		private String _defaultUserId;
		private Boolean _isRenewal = false;

		public String DefaultEnvironment
		{
			get
			{
				return _defaultEnvironment;
			}
			set
			{
				_defaultEnvironment = value;
			}
		}

		public String UserId
		{
			get 
			{ 
				return _userId.Text; 
			}

			set 
			{
				_defaultUserId = value;
				_userId.Text = _defaultUserId;

				_environment.Enabled = (_defaultEnvironment.Length == 0);
				_userId.Enabled = (_defaultUserId.Length == 0);
				_login.Enabled = (_defaultUserId.Length > 0);
			}
		}

		public Boolean IsRenewal
		{
			get { return _isRenewal; }
			set { _isRenewal = value; }
		}

		public UserLoginRequest()
		{
			InitializeComponent();
		}

		private void UserLoginRequest_Load(object sender, EventArgs e)
		{
			IAuthentificateUser authentificateUser = DataAccessServer.AuthentificateUser();
			String environments = authentificateUser.Environments;

			_environment.DataSource = environments.Split(new Char[] { '|' });

			for (Int32 index = 0; index < _environment.Items.Count; index++)
			{
				if (_environment.Items[index].ToString() == _defaultEnvironment)
				{
					_environment.SelectedIndex = index;
					break;
				}
			}

			_userId.Validating += new CancelEventHandler(_userID_Validating);
			_password.KeyPress += new KeyPressEventHandler(_password_KeyPress);
			_login.Click += new EventHandler(_login_Click);
			_cancel.Click += new EventHandler(_cancel_Click);

			_login.Enabled = (_defaultUserId.Length > 0);
		}

		void _password_KeyPress(object sender, KeyPressEventArgs e)
		{
			if ((Int32) e.KeyChar == 13)
			{
				// Return key pressed, execute login click
				_login_Click(sender, (EventArgs) e);
			}
		}

		void _userID_Validating(object sender, CancelEventArgs e)
		{
			_login.Enabled = (_userId.Text.Length > 0);
		}

		void _login_Click(object sender, EventArgs e)
		{
			// Grey out all controls
			_userId.Enabled = false;
			_password.Enabled = false;
			_login.Enabled = false;
			_cancel.Enabled = false;

			// Change the mouse pointer to the timer
			this.Cursor = Cursors.WaitCursor;

			// Do application events to ensure all controls match their specified
			// status
			Application.DoEvents();

			// Now attempt to validate the user
			UserAuthentification.Environment = _environment.SelectedValue.ToString();
			UserAuthentification.UserId = _userId.Text;
			UserAuthentification.Password = _password.Text;
			
			UserAuthentification.ValidateUser(true);

			this.Cursor = Cursors.Default;

			if (UserAuthentification.Credentials == null)
			{
				MessageBox.Show("Please check your user ID and password", "Login Failed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

				// Reset controls enabled status
				_userId.Enabled = (_defaultUserId.Length == 0);
				_password.Enabled = true;
				_login.Enabled = true;
				_cancel.Enabled = true;

				// Set focus back to relevant location
				if (_defaultUserId.Length == 0)
				{
					_userId.Focus();
				}
				else
				{
					_password.Focus();
				}
			}
			else
			{
				this.Close();
			}
		}

		void _cancel_Click(object sender, EventArgs e)
		{
			if (_isRenewal)
			{
				if (MessageBox.Show("Are you sure, cancelling user authentification\nwill cause your program to exit", "User Authentification", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					Environment.Exit(0);
				}
			}
			else
			{
				this.Close();
			}
		}
	}
}

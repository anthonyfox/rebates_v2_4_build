﻿using System;
using System.Reflection;

namespace Evolve.Libraries.Data.Remoting
{
	public abstract class StoredProcedure
	{
		private String _procedureName;
		private Object _parameters;
		private Object _outputs;
		private Int32 _maxRetries = 10;
		private Int32 _retryMilliseconds = 500;

		public String ProcedureName
		{
			get { return _procedureName; }
			set { _procedureName = value; }
		}

		public Object Parameters
		{
			get { return _parameters; }
			set { _parameters = value; }
		}

		public Object Outputs
		{
			get { return _outputs; }
		}

		public void ExecuteProcedure()
		{
			SqlResponse sqlResponse = new SqlResponse();
			Boolean useExistingDataConnection = (DataAccessServer.PersistentConnection != null);
			DataConnection dataConnection = null;

			Int32 parameterCount = _parameters.GetType().GetProperties().Length;
			Object[] parameters = new Object[parameterCount];

			try
			{
				if (_parameters != null)
				{
					// Open data connection if required
					if (useExistingDataConnection)
					{
						dataConnection = DataAccessServer.PersistentConnection;
					}
					else
					{
						dataConnection = new DataConnection();
						sqlResponse = dataConnection.OpenConnection();
					}

					if (!sqlResponse.HasError)
					{
						// Create object array of parameters class
						Int32 index = 0;

						foreach (PropertyInfo propertyInfo in _parameters.GetType().GetProperties())
						{
							Object value = propertyInfo.GetValue(_parameters, null);

							if (value == null)
							{
								parameters[index++] = DBNull.Value;
							}
							else
							{
								parameters[index++] = ConvertObjectValue(ObjectDirection.Input, propertyInfo, value);
							}
						}

						// Execute the procedure
						Boolean isCompleted = true;
						Int32 attempts = 0;

						do
						{
							attempts++;
							sqlResponse = dataConnection.ExecuteProcedure(_procedureName, parameters);

							if (sqlResponse != null && sqlResponse.IsLockError)
							{
								isCompleted = attempts > _maxRetries;

								if (!isCompleted)
								{
									System.Threading.Thread.Sleep(_retryMilliseconds);
								}
							}
							else
							{
								isCompleted = true;
							}
						} while (!isCompleted);
					}

					// Now populate the outputs class (if required)
					if (sqlResponse != null && !sqlResponse.HasError && sqlResponse.Results != null && sqlResponse.Results.Length > 0)
					{
						Int32 index = 0;
						String name = this.GetType().Name;
						String assemblyQualifiedName = this.GetType().AssemblyQualifiedName.Replace(name, name + "Outputs");
						Type typeOutputs = Type.GetType(assemblyQualifiedName);

						if (typeOutputs != null)
						{
							_outputs = Activator.CreateInstance(typeOutputs);

							foreach (PropertyInfo propertyInfo in typeOutputs.GetProperties())
							{
                                if (sqlResponse.Results[index] == DBNull.Value)
                                {
                                    propertyInfo.SetValue(_outputs, ConvertObjectValue(ObjectDirection.Input, propertyInfo, null), null);
                                }
                                else
                                {
                                    propertyInfo.SetValue(_outputs, ConvertObjectValue(ObjectDirection.Input, propertyInfo, sqlResponse.Results[index]), null);
                                }

                                index++;
							}
						}
					}
					else
					{
						// Build exception text
						String exceptionText = "NULL SqlResponse: ";

						if (sqlResponse != null)
						{
							exceptionText = sqlResponse.Message + ": ";
						}

						exceptionText += "ExecuteProcedure(" + this.ProcedureName;

						if (parameterCount > 0)
						{
							foreach (Object parameter in parameters)
							{
								exceptionText += ", " + parameter.ToString();
							}
						}

						exceptionText += ")";

						// Finally contstuct and throw the database exception
						DatabaseException databaseException = new DatabaseException(exceptionText);
						databaseException.IsLockError = (sqlResponse == null ? false : sqlResponse.IsLockError);

						throw databaseException;
					}
				}
			}
			finally
			{
				if (!useExistingDataConnection && dataConnection != null)
				{
					dataConnection.CloseConnection();
				}

				dataConnection = null;
			}
		}

		private Object ConvertObjectValue(ObjectDirection direction, PropertyInfo propertyInfo, Object value)
		{
			Object convertedValue = null;
			StoredProcedureConversionAttribute conversionAttribute = null;
			Object[] attributes = propertyInfo.GetCustomAttributes(typeof(StoredProcedureConversionAttribute), true);
			MethodInfo methodConvert = null;
			Type type = null;
			String methodName;

			if (attributes != null && attributes.Length == 1)
			{
				conversionAttribute = (StoredProcedureConversionAttribute) attributes[0];

				if (conversionAttribute.Method.Contains("."))
				{
					String assembly = conversionAttribute.AssemblyName;
					String nameSpace = conversionAttribute.Namespace;
					String className = conversionAttribute.Method.Substring(0, conversionAttribute.Method.IndexOf('.'));

					methodName = conversionAttribute.Method.Substring(conversionAttribute.Method.IndexOf('.') + 1);

					// Assume conversion method is in local assembly unless we have specified otherwise
					if (assembly.Length == 0)
					{
						assembly = this.GetType().Assembly.ToString();
					}

					// Assume conversion method is in current namespace unless specified otherwise
					if (nameSpace.Length == 0)
					{
						nameSpace = this.GetType().Namespace;
					}

					// Get type of conversion class and get the method info
					type = Type.GetType(nameSpace + "." + className + ", " + assembly);
				}
				else
				{
					type = typeof(SqlHelper);
					methodName = conversionAttribute.Method;
				}

				if (type != null)
				{
					methodConvert = type.GetMethod(methodName);
				}
			}
			else
			{
				if (direction == ObjectDirection.Input && value != null && !propertyInfo.PropertyType.Equals(value.GetType()))
				{
					methodName = "DB" + SqlHelper.GetNativeTypeDescription(value.GetType()) + "To" + SqlHelper.GetNativeTypeDescription(propertyInfo.PropertyType);
					methodConvert = typeof(SqlHelper).GetMethod(methodName);

					if (methodConvert == null)
					{
						throw new DatabaseException("No data conversion method " + methodName);
					}
				}
			}

			if (methodConvert == null)
			{
				if (value is String)
				{
					convertedValue = value.ToString().TrimEnd();
				}
				else
				{
					convertedValue = value;
				}
			} 
			else
			{
				convertedValue = methodConvert.Invoke(null, new Object[] { value });
			}

			return convertedValue;
		}

		private enum ObjectDirection
		{
			Input,
			Output
		}
	}

	/// <summary>
	/// Attribute to specify a custom data conversion attribute for a stored procedure parameter or return value
	/// </summary>
	public sealed class StoredProcedureConversionAttribute : Attribute
	{
		public String AssemblyName = "";
		public String Namespace = "";
		public String Method = "";

		public StoredProcedureConversionAttribute(String method)
		{
			Method = method;
		}

		public StoredProcedureConversionAttribute(String method, String nameSpace)
		{
			Method = method;
			Namespace = nameSpace;
		}

		public StoredProcedureConversionAttribute(String method, String nameSpace, String assembly)
		{
			Method = method;
			Namespace = nameSpace;
			AssemblyName = assembly;
		}
	}

	/// <summary>
	/// Attribute to specify a custom date format for a stored procedure date parameter
	/// </summary>
	public sealed class StoredProcedureDateAttribute : Attribute
	{
		public DBDateTime DateFrom;
		public DBDateTime DateTo;

		public StoredProcedureDateAttribute(DBDateTime dateFrom, DBDateTime dateTo)
		{
			DateFrom = dateFrom;
			DateTo = dateTo;
		}
	}

}

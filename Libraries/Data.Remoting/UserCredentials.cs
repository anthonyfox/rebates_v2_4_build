﻿using System;
using System.DirectoryServices.AccountManagement;

namespace Evolve.Libraries.Data.Remoting
{
	[Serializable]
	public class UserCredentials
	{
		private String _environment = "";
		private String _windowsUserId = "";
		private String _databaseUserId = "";
		private String _password = "";
		private String _applicationUserId = "";
		private Int32 _minimumDASRevsionRequired = 0;
		private DatabaseDateFormat _databaseDateFormat = DatabaseDateFormat.British;
		private String _databaseProvider = "";

		public String Environment
		{
			get { return _environment; }
			set { _environment = value; }
		}

		public String WindowsUserId
		{
			get { return _windowsUserId; }
		}

		public String DatabaseUserId
		{
			get { return _databaseUserId; }
			set { _databaseUserId = value; }
		}

		public String Password
		{
			get { return _password; }
			set { _password = value; }
		}

		public String ApplicationUserId
		{
			get { return (_applicationUserId.Length == 0 ? _databaseUserId : _applicationUserId); }
			set { _applicationUserId = value; }
		}

		public Int32 MinimumDASRevisionRequired
		{
			get { return _minimumDASRevsionRequired; }
			set { _minimumDASRevsionRequired = value; }
		}

		public Boolean IsAssummedIdentity
		{
			get { return _databaseUserId != _applicationUserId; }
		}

		public DatabaseDateFormat DatabaseDateFormat
		{
			get { return _databaseDateFormat; }
			set { _databaseDateFormat = value; }
		}

		public String DatabaseProvider
		{
			get { return _databaseProvider; }
			set { _databaseProvider = value; }
		}

		public UserCredentials(String windowsUserId)
		{
			_windowsUserId = windowsUserId;
		}
	}

	public enum DatabaseDateFormat
	{
		British,
		American,
		YearMonthDay,
	}
}

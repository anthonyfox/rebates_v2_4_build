﻿using System;
using System.Security.Principal;
//using System.DirectoryServices.AccountManagement;

namespace Evolve.Libraries.Data.Remoting
{
	public static class UserAuthentification
	{
		private static String _environment = "Development";
		private static WindowsIdentity _windowsUser = null;
		private static String _userId = "";
		private static String _applicationUserId = "";
		private static String _password = "";
		private static Int32 _minimumDASRevisionRequired = 0;
		private static UserCredentials _userCredentials;
		private static Boolean _needsDatabaseId = false;

		public static String Environment
		{
			get { return _environment; }
			set { _environment = value; }
		}

		public static String WindowsUserId
		{
			get { return (_windowsUser == null ? "" : _windowsUser.Name); }
		}

		public static WindowsIdentity WindowsUser
		{
			get { return _windowsUser; }
			set { _windowsUser = value; }
		}

		public static String UserId
		{
			get { return _userId; }
			set { _userId = value; }
		}

		public static String Password
		{
			get { return _password; }
			set { _password = value; }
		}

		public static String ApplicationUserId
		{
			get	{ return (String.IsNullOrEmpty(_applicationUserId) ? UserAuthentification.WindowsUserId : _applicationUserId); }
			set { _applicationUserId = value; }
		}

		public static Int32 MinimumDASRevisionRequired
		{
			get { return _minimumDASRevisionRequired; }
			set { _minimumDASRevisionRequired = value; }
		}

		public static Boolean NeedsDatabaseId
		{
			get { return _needsDatabaseId; }
			set { _needsDatabaseId = value; }
		}

		public static UserCredentials Credentials
		{
			get { return _userCredentials; }
		}

		public static void ReadCredentials()
		{
			ReadCredentials(false);
		}

		public static void ReadCredentials(Boolean isRenewal)
		{
			if (_windowsUser == null)
			{
				throw new ArgumentException("Windows user principal not configured in User Authentification module");
			}

			if (!DataAccessServer.IsDirect)
			{
				if (_userId == "NoDataAccess")
				{
					// Special account which means the data access module will ignore
					// all attempts to access the database
					_userCredentials = new UserCredentials(UserAuthentification.WindowsUserId);
					_userCredentials.Environment = _environment;
					_userCredentials.DatabaseUserId = "NoDataAccess";
				}
				else
				{
					// Attempt to read existing user credentials from the data access server
					if (String.IsNullOrEmpty(_userId))
					{
						_userId = UserAuthentification.WindowsUserId;
					}

					if (_userId.Length > 0)
					{
						IAuthentificateUser authentificateUser = DataAccessServer.AuthentificateUser();
						_userCredentials = authentificateUser.ReadCredentials(_environment, _userId, _needsDatabaseId);

						ValidateUser(false);
					}

					// No valid credentials currently exist, will have to prompt the
					// user for them
					if (_userCredentials == null)
					{
						UserLoginRequest userLogin = new UserLoginRequest();

						userLogin.IsRenewal = isRenewal;
						userLogin.DefaultEnvironment = _environment;
						userLogin.UserId = (_needsDatabaseId ? "" : _userId);
						userLogin.ShowDialog();
					}

					// Set application user id as required, and confirm the database date format
					if (_userCredentials != null)
					{
						if (_applicationUserId != "")
						{
							_userCredentials.ApplicationUserId = _applicationUserId;
						}

						ValidateDateFormat();
					}
				}
			}
		}

		public static void SetDirectAccess(String environment)
		{
			_userCredentials = new UserCredentials(UserAuthentification.WindowsUserId);
			_userCredentials.Environment = environment;
            ValidateDateFormat();
		}

		private static void ValidateDateFormat()
		{
			switch (UserAuthentification.Credentials.DatabaseProvider)
			{
				case "SqlClient":
					_userCredentials.DatabaseDateFormat = DatabaseDateFormat.YearMonthDay;
					break;

				case "Informix":
				case "Odbc": // +++ ACTION: Need to identify difference (13th Dec 2011)
					String sql = "select * from systables where tabid = 1 and created >= '31/12/1899'";

					DataConnection dataConnection = new DataConnection();
					SqlResponse openResponse = dataConnection.OpenConnection();
					SqlResponse sqlResponse = dataConnection.ExecuteSqlToDataTable(sql);
					dataConnection.CloseConnection();

					if (sqlResponse.HasError)
					{
						_userCredentials.DatabaseDateFormat = DatabaseDateFormat.American;
					}

					break;

				default:
					_userCredentials.DatabaseDateFormat = DatabaseDateFormat.YearMonthDay;
					break;
			}
		}

		public static void ValidateUser(Boolean saveCredentials)
		{
			if (_userId.Length > 0)
			{
				if (_userCredentials == null)
				{
					_userCredentials = new UserCredentials(UserAuthentification.WindowsUserId);
					_userCredentials.Environment = _environment;
					_userCredentials.DatabaseUserId = _userId;
					_userCredentials.Password = _password;
				}

				_userCredentials.MinimumDASRevisionRequired = _minimumDASRevisionRequired;

				DataAccessServer.UserCredentials = _userCredentials;
				DataConnection dataConnection = DataAccessServer.NewConnection();

				if (dataConnection.AuthenticateUser())
				{
					_userCredentials = dataConnection.UserCredentials;
					SqlResponse sqlResponse = dataConnection.CloseConnection();

					if (saveCredentials)
					{
						// Get an object from the DAS which handles the user authentification details
						// and save the credentials
						IAuthentificateUser authentificateUser = DataAccessServer.AuthentificateUser();
						authentificateUser.SaveCredentials(_userCredentials, _needsDatabaseId);
					}
				}
				else
				{
					_userCredentials = null;
				}
			}
			else
			{
				_userCredentials = null;
			}
		}
	}
}
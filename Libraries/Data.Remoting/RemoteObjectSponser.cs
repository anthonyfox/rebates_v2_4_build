﻿using System;
using System.Runtime.Remoting.Lifetime;

namespace Evolve.Libraries.Data.Remoting
{
	public class RemoteObjectSponser : MarshalByRefObject, ISponsor
	{
		private Boolean _doRenewal = true;
		private Int32 _renewalMilliseconds = 5000;

		public Boolean DoRenewal
		{
			get { return _doRenewal; }
			set { _doRenewal = value; }
		}

		public RemoteObjectSponser()
		{
		}

		public RemoteObjectSponser(Int32 renewalMilliseconds)
		{
			_renewalMilliseconds = renewalMilliseconds;
		}

		public TimeSpan Renewal(ILease lease)
		{
			if (_doRenewal)
			{
				return TimeSpan.FromMilliseconds(_renewalMilliseconds);
			}
			else
			{
				return TimeSpan.FromMilliseconds(0);
			}
		}
	}
}

﻿using System;
using System.Data;
using System.Data.Common;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Evolve.Libraries.Utilities;

namespace Evolve.Libraries.Data.Remoting
{
	public static class DataAccessEngine
	{
		public static void InitializeComponent(UserCredentials userCredentials, Guid guid, ref String provider, ref DbProviderFactory dbProviderFactory, ref DbConnection dbConnection, ref Boolean logAuthentificationExceptions, ref DbReadMode dbTableReadMode, ref DbReadMode dbStoredProcedureReadMode)
		{
			// Setup authentification exceptions and read mode 
			logAuthentificationExceptions = Convert.ToBoolean(DataAccessConfiguration.Settings["LogAuthentificationExceptions"]);

			if (DataAccessConfiguration.Settings[userCredentials.Environment + "_TableReadMode"] != null)
			{
				String dbReadMode = DataAccessConfiguration.Settings[userCredentials.Environment + "_TableReadMode"].ToString();

				if (!String.IsNullOrEmpty(dbReadMode))
				{
					if (dbReadMode.ToLower() == "dbdatareader")
					{
						dbTableReadMode = DbReadMode.DbDataReader;
					}
				}
			}

			if (DataAccessConfiguration.Settings[userCredentials.Environment + "_StoredProcedureReadMode"] != null)
			{
				String dbReadMode = DataAccessConfiguration.Settings[userCredentials.Environment + "_StoredProcedureReadMode"].ToString();

				if (!String.IsNullOrEmpty(dbReadMode))
				{
					if (dbReadMode.ToLower() == "dbdatareader")
					{
						dbStoredProcedureReadMode = DbReadMode.DbDataReader;
					}
				}
			}

			// Initialise the database connection details
			DataAccessEngine.InitializeDbConnection(userCredentials, guid, ref provider, ref dbProviderFactory, ref dbConnection);
		}

		private static void InitializeDbConnection(UserCredentials userCredentials, Guid guid, ref String provider, ref DbProviderFactory dbProviderFactory, ref DbConnection dbConnection)
		{
			// Read provider details, and define if we wish to log authentification exceptions
			provider = DataAccessConfiguration.Settings[userCredentials.Environment + "_Provider"].ToString();

			// Setup DbProviderFactory object
			if (provider != null && provider.Length > 0)
			{
				dbProviderFactory = DbProviderFactories.GetFactory(provider);

				// Strip out leading namespace details for the Provider as this is no longer
				// required
				provider = provider.Substring(provider.LastIndexOf('.') + 1);
			}

			// Now setup connection details
			if (dbProviderFactory != null)
			{
				String connectionKey = userCredentials.Environment + "_" + provider;
				String connectionString = DataAccessConfiguration.Settings[connectionKey].ToString();

				// Check if the settings contain a fixed password for the connection
				String encryptedKey = userCredentials.Environment + "_EncryptedPassword";

				if (DataAccessConfiguration.Settings.ContainsKey(encryptedKey))
				{
					String encryptedPassword = DataAccessConfiguration.Settings[encryptedKey].ToString();
					Encryption encryption = new Encryption(EncryptionKey.Two);
					userCredentials.Password = encryption.Decrypt(encryptedPassword);
				}

				if (connectionString != null && connectionString.Length > 0)
				{
					if (LicenseManager.UsageMode == LicenseUsageMode.Runtime && userCredentials != null)
					{
						Logger.Instance.Debug(DataAccessEngine.LoggerPrefix(userCredentials, guid) + provider + ": " + connectionString);

						connectionString = connectionString.Replace("%UserId%", userCredentials.DatabaseUserId);
						connectionString = connectionString.Replace("%Password%", userCredentials.Password);

						dbConnection = dbProviderFactory.CreateConnection();
						dbConnection.ConnectionString = connectionString;
					}
				}
			}
		}

		public static Boolean AuthenticateUser(ref UserCredentials userCredentials, Guid guid, DbProviderFactory dbProviderFactory, ref DbConnection dbConnection, ref DbTransaction dbTransaction, Boolean logAuthentificationExceptions)
		{
			String provider = DataAccessConfiguration.Settings[userCredentials.Environment + "_Provider"].ToString();
			provider = provider.Substring(provider.LastIndexOf('.') + 1);
			userCredentials.DatabaseProvider = provider;

			Boolean isAuthenticated;

			SqlResponse sqlResponse = OpenConnection(userCredentials, guid, provider, dbProviderFactory, ref dbConnection, ref dbTransaction, IsolationLevel.ReadCommitted, true, logAuthentificationExceptions);
			isAuthenticated = !sqlResponse.IsAuthentificationError;
			sqlResponse = CloseConnection(userCredentials, guid, ref dbConnection, ref dbTransaction, true);

			Logger.Instance.Debug(LoggerPrefix(userCredentials, guid), "AuthenticateUser()");

			if (isAuthenticated)
			{
				Logger.Instance.Debug(LoggerPrefix(userCredentials, guid), "AuthenticateUser() Successful");
			}
			else
			{
				Logger.Instance.Debug(LoggerPrefix(userCredentials, guid), "AuthenticateUser() Failed");
			}

			return isAuthenticated;
		}

		public static SqlResponse OpenConnection(UserCredentials userCredentials, Guid guid, String provider, DbProviderFactory dbProviderFactory, ref DbConnection dbConnection, ref DbTransaction dbTransaction, Boolean logAuthentificationExceptions)
		{
			IsolationLevel isolationLevel = DataAccessEngine.DefaultIsolationLevel(userCredentials, provider);
			return OpenConnection(userCredentials, guid, provider, dbProviderFactory, ref dbConnection, ref dbTransaction, isolationLevel, false, logAuthentificationExceptions);
		}

		public static IsolationLevel DefaultIsolationLevel(UserCredentials userCredentials, String provider)
		{
			IsolationLevel isolationLevel = IsolationLevel.ReadUncommitted;

			if (DataAccessConfiguration.Settings[provider + "_IsolationLevel"] != null)
			{
				isolationLevel = (IsolationLevel) DataAccessConfiguration.Settings[provider + "_IsolationLevel"];
			}

			return isolationLevel;
		}

		public static SqlResponse OpenConnection(UserCredentials userCredentials, Guid guid, String provider, DbProviderFactory dbProviderFactory, ref DbConnection dbConnection, ref DbTransaction dbTransaction, IsolationLevel isolationLevel, Boolean logAuthentificationExceptions)
		{
			return OpenConnection(userCredentials, guid, provider, dbProviderFactory, ref dbConnection, ref dbTransaction, isolationLevel, false, logAuthentificationExceptions);
		}

		public static SqlResponse OpenConnection(UserCredentials userCredentials, Guid guid, String provider, DbProviderFactory dbProviderFactory, ref DbConnection dbConnection, ref DbTransaction dbTransaction, IsolationLevel isolationLevel, Boolean isAuthenticateUser, Boolean logAuthentificationExceptions)
		{
			SqlResponse sqlResponse = new SqlResponse(guid, DataAccessConfiguration.HostName);

			if (!isAuthenticateUser)
			{
				Logger.Instance.Debug(LoggerPrefix(userCredentials, guid), "OpenConnection()");
			}

			if (dbConnection != null)
			{
				if (dbConnection.State == ConnectionState.Closed)
				{
					try
					{
						// Check DAS revision against the minimum requirements specified by the requesting
						// application
						if (Int32.Parse(SubVersion.Build.Revision) >= userCredentials.MinimumDASRevisionRequired)
						{
							dbConnection.Open();

							if (!isAuthenticateUser)
							{
								BeginTransaction(userCredentials, guid, provider, dbProviderFactory, ref dbConnection, ref dbTransaction, isolationLevel);
							}
						}
						else
						{
							String message = "DAS does not meet the minimum requirements (Requested " + userCredentials.MinimumDASRevisionRequired.ToString() + ", DAS " + SubVersion.Build.Revision + ")";
							throw new Exception(message);
						}
					}
					catch (Exception ex)
					{
						sqlResponse.ParseException(ex);
						ValidateConnectionState(ref dbConnection, ref dbTransaction);

						if (logAuthentificationExceptions)
						{
							Logger.Instance.Error(LoggerPrefix(userCredentials, guid), ex);
						}
					}
				}
			}

			return sqlResponse;
		}

		public static SqlResponse CloseConnection(UserCredentials userCredentials, Guid guid, ref DbConnection dbConnection, ref DbTransaction dbTransaction)
		{
			return CloseConnection(userCredentials, guid, ref dbConnection, ref dbTransaction, false);
		}

		public static SqlResponse CloseConnection(UserCredentials userCredentials, Guid guid, ref DbConnection dbConnection, ref DbTransaction dbTransaction, Boolean isAuthenticateUser)
		{
			SqlResponse sqlResponse = new SqlResponse(guid, DataAccessConfiguration.HostName);

			if (dbTransaction != null)
			{
				try
				{
					Logger.Instance.Debug(LoggerPrefix(userCredentials, guid), "RollbackTransaction()");

					try
					{
						dbTransaction.Rollback();
					}
					catch (InvalidOperationException)
					{
					}
				}
				catch (Exception ex)
				{
					ValidateConnectionState(ref dbConnection, ref dbTransaction);
					sqlResponse.ParseException(ex);
					Logger.Instance.Error(LoggerPrefix(userCredentials, guid), ex);
				}
				finally
				{
					try
					{
						dbTransaction.Dispose();
					}
					catch
					{
					}
					finally
					{
						dbTransaction = null;
					}
				}
			}

			if (dbConnection != null)
			{
				if (!isAuthenticateUser)
				{
					Logger.Instance.Debug(LoggerPrefix(userCredentials, guid), "CloseConnnection()");
				}

				try
				{
					dbConnection.Close();
				}
				catch (Exception ex)
				{
					sqlResponse.ParseException(ex);
					Logger.Instance.Error(LoggerPrefix(userCredentials, guid), ex);
				}
				finally
				{
					dbConnection.Dispose();
					dbConnection = null;
				}
			}

			return sqlResponse;
		}

		public static SqlResponse BeginTransaction(UserCredentials userCredentials, Guid guid, String provider, DbProviderFactory dbProviderFactory, ref DbConnection dbConnection, ref DbTransaction dbTransaction)
		{
			IsolationLevel isolationLevel = DataAccessEngine.DefaultIsolationLevel(userCredentials, provider);
			return BeginTransaction(userCredentials, guid, provider, dbProviderFactory, ref dbConnection, ref dbTransaction, isolationLevel);
		}

		public static SqlResponse BeginTransaction(UserCredentials userCredentials, Guid guid, String provider, DbProviderFactory dbProviderFactory, ref DbConnection dbConnection, ref DbTransaction dbTransaction, IsolationLevel isolationLevel)
		{
			SqlResponse sqlResponse = new SqlResponse(guid, DataAccessConfiguration.HostName);

			if (dbConnection != null && dbConnection.State == ConnectionState.Open && dbTransaction == null)
			{
				Logger.Instance.Debug(LoggerPrefix(userCredentials, guid), "BeginTransaction(" + isolationLevel.ToString() + ")");

				try
				{
					dbTransaction = dbConnection.BeginTransaction(isolationLevel);

					if (DataAccessConfiguration.Settings[provider + "_PostBeginTransactionSql"] != null && DataAccessConfiguration.Settings[provider + "_PostBeginTransactionSql"].ToString() != "")
					{
						String[] sqls = DataAccessConfiguration.Settings[provider + "_PostBeginTransactionSql"].ToString().Split(new char[] { ';' });

						foreach (String sql in sqls)
						{
							ExecuteSql(userCredentials, guid, provider, dbProviderFactory, ref dbConnection, ref dbTransaction, sql);
						}
					}
				}
				catch (Exception ex)
				{
					ValidateConnectionState(ref dbConnection, ref dbTransaction);
					sqlResponse.ParseException(ex);
					Logger.Instance.Error(LoggerPrefix(userCredentials, guid), ex);
				}
			}

			return sqlResponse;
		}

		public static SqlResponse CommitTransaction(UserCredentials userCredentials, Guid guid, ref DbConnection dbConnection, ref DbTransaction dbTransaction)
		{
			SqlResponse sqlResponse = new SqlResponse(guid, DataAccessConfiguration.HostName);
			Logger.Instance.Debug(LoggerPrefix(userCredentials, guid), "CommitTransaction()");

			try
			{
				if (dbTransaction != null)
				{
					dbTransaction.Commit();
					dbTransaction.Dispose();
					dbTransaction = null;
				}
			}
			catch (Exception ex)
			{
				ValidateConnectionState(ref dbConnection, ref dbTransaction);
				sqlResponse.ParseException(ex);
				Logger.Instance.Error(LoggerPrefix(userCredentials, guid), ex);
			}

			return sqlResponse;
		}

		public static SqlResponse RollbackTransaction(UserCredentials userCredentials, Guid guid, String provider, DbProviderFactory dbProviderFactory, ref DbConnection dbConnection, ref DbTransaction dbTransaction)
		{
			SqlResponse sqlResponse = new SqlResponse(guid, DataAccessConfiguration.HostName);
			Logger.Instance.Debug(LoggerPrefix(userCredentials, guid), "RollbackTransaction()");

			try
			{
				if (dbTransaction != null)
				{
					try
					{
						dbTransaction.Rollback();
					}
					catch (InvalidOperationException)
					{
					}

					dbTransaction.Dispose();
					dbTransaction = null;

					BeginTransaction(userCredentials, guid, provider, dbProviderFactory, ref dbConnection, ref dbTransaction);
				}
				else
				{
					Logger.Instance.Debug(LoggerPrefix(userCredentials, guid), "_dbTransaction is null in RollbackTransaction()");
				}
			}
			catch (Exception ex)
			{
				ValidateConnectionState(ref dbConnection, ref dbTransaction);
				sqlResponse.ParseException(ex);
				Logger.Instance.Error(LoggerPrefix(userCredentials, guid), ex);
			}

			return sqlResponse;
		}

		public static SqlResponse ExecuteSql(UserCredentials userCredentials, Guid guid, String provider, DbProviderFactory dbProviderFactory, ref DbConnection dbConnection, ref DbTransaction dbTransaction, String sql)
		{
			SqlResponse sqlResponse = new SqlResponse(guid, DataAccessConfiguration.HostName, sql);

			Logger.Instance.Debug(LoggerPrefix(userCredentials, guid), "ExecuteSql(" + sql + ")");
			Int32 result = -1;

			if (dbConnection != null)
			{
				try
				{
					DbCommand dbCommand = dbProviderFactory.CreateCommand();
					dbCommand.CommandText = sql;
					dbCommand.Connection = dbConnection;
					dbCommand.Transaction = dbTransaction;
					dbCommand.CommandTimeout = 0;

					result = dbCommand.ExecuteNonQuery();
					dbCommand.Dispose();
				}
				catch (Exception ex)
				{
					ValidateConnectionState(ref dbConnection, ref dbTransaction);
					sqlResponse.ParseException(ex);
					Logger.Instance.Error(LoggerPrefix(userCredentials, guid), ex);
				}
				finally
				{
					sqlResponse.Results = new Object[1];
					sqlResponse.Results[0] = result;
				}
			}

			Logger.Instance.Debug(LoggerPrefix(userCredentials, guid), "ExecuteSql() Complete");
			return sqlResponse;
		}

		private static void ValidateConnectionState(ref DbConnection dbConnection, ref DbTransaction dbTransaction)
		{
			if (dbConnection != null)
			{
				if (dbConnection.State != ConnectionState.Open)
				{
					// Connection may have been forcefully terminated at the server ensure that the
					// transaction and connection objects are cleaned up accordingly
					if (dbTransaction != null)
					{
						try
						{
							dbTransaction.Rollback();
						}
						catch (InvalidOperationException)
						{
						}
						finally
						{
							try
							{
								dbTransaction.Dispose();
							}
							catch
							{
							}
						}
					}

					try
					{
						dbConnection.Dispose();
					}
					catch
					{
					}
					finally
					{
						dbConnection = null;
						dbTransaction = null;
					}
				}
			}
		}

		public static SqlResponse ExecuteScalar(UserCredentials userCredentials, Guid guid, DbProviderFactory dbProviderFactory, ref DbConnection dbConnection, ref DbTransaction dbTransaction, String sql)
		{
			SqlResponse sqlResponse = new SqlResponse(guid, DataAccessConfiguration.HostName, sql);

			Logger.Instance.Debug(LoggerPrefix(userCredentials, guid), "ExecuteSql(" + sql + ")");
			Object result = null;

			if (dbConnection != null)
			{
				try
				{
					DbCommand dbCommand = dbProviderFactory.CreateCommand();
					dbCommand.CommandText = sql;
					dbCommand.Connection = dbConnection;
					dbCommand.Transaction = dbTransaction;

					result = dbCommand.ExecuteScalar();
					dbCommand.Dispose();
				}
				catch (Exception ex)
				{
					ValidateConnectionState(ref dbConnection, ref dbTransaction);
					sqlResponse.ParseException(ex);
					Logger.Instance.Error(LoggerPrefix(userCredentials, guid), ex);
				}
				finally
				{
					sqlResponse.Results = new Object[1];
					sqlResponse.Results[0] = result;
				}
			}

			Logger.Instance.Debug("ExecuteSql() Complete");
			return sqlResponse;
		}

		public static SqlResponse ExecuteSqlToDataTable(UserCredentials userCredentials, Guid guid, DbProviderFactory dbProviderFactory, ref DbConnection dbConnection, ref DbTransaction dbTransaction, DbReadMode dbTableReadMode, String sql)
		{
			SqlResponse sqlResponse = new SqlResponse(guid, DataAccessConfiguration.HostName, sql);

			Logger.Instance.Debug(LoggerPrefix(userCredentials, guid), "ExecuteSqlToDataTable(" + sql + ")");
			DataTable dataTable = null;

			if (dbConnection != null)
			{
				try
				{
					dataTable = new DataTable();

					// Turn off indexing notifications for maximum load performance
					dataTable.BeginLoadData();

					// Now load datatable
					DbCommand dbCommand = dbProviderFactory.CreateCommand();
					dbCommand.CommandText = sql;
					dbCommand.Connection = dbConnection;
					dbCommand.Transaction = dbTransaction;

					if (dbTableReadMode == DbReadMode.DbDataAdapter)
					{
						DbDataAdapter dbDataAdapter = dbProviderFactory.CreateDataAdapter();
						dbDataAdapter.SelectCommand = dbCommand;
						dbDataAdapter.Fill(dataTable);
						dbDataAdapter.Dispose();
					}
					else
					{
						Boolean isFirst = true;
						DbDataReader dbDataReader = dbCommand.ExecuteReader();

						if (dbDataReader != null && dbDataReader.HasRows)
						{
							while (dbDataReader.Read())
							{
								if (isFirst)
								{
									DataTable schemaTable = dbDataReader.GetSchemaTable();

									foreach (DataRow schemaRow in schemaTable.Rows)
									{
										DataColumn column = new DataColumn(schemaRow["ColumnName"].ToString());
										column.DataType = Type.GetType(schemaRow["DataType"].ToString());
										dataTable.Columns.Add(column);
									}

									isFirst = false;
								}

								DataRow row = dataTable.NewRow();

								for (Int32 index = 0; index < dbDataReader.FieldCount; index++)
								{
									row[index] = dbDataReader[index];
								}

								dataTable.Rows.Add(row);
							}
						}

						dbDataReader.Close();
						dbDataReader.Dispose();
					}

					// Turn on indexing again
					dataTable.EndLoadData();
					dbCommand.Dispose();
				}
				catch (Exception ex)
				{
					ValidateConnectionState(ref dbConnection, ref dbTransaction);
					sqlResponse.ParseException(ex);
					Logger.Instance.Error(LoggerPrefix(userCredentials, guid), ex);
					dataTable = null;
				}
				finally
				{
					if (dataTable != null)
					{
						sqlResponse.Results = new Object[1];
						sqlResponse.Results[0] = dataTable;
					}
				}
			}

			Logger.Instance.Debug(LoggerPrefix(userCredentials, guid), "ExecuteSqlToDataTable() Complete");
			return sqlResponse;
		}

		public static SqlResponse ExecuteProcedure(UserCredentials userCredentials, Guid guid, DbProviderFactory dbProviderFactory, ref DbConnection dbConnection, ref DbTransaction dbTransaction, DbReadMode dbStoredProcedureReadMode, String procedureName, Object[] parameters)
		{
			SqlResponse sqlResponse = new SqlResponse(guid, DataAccessConfiguration.HostName);
			Object[] returnValues = null;

			if (dbConnection != null)
			{
				String logCommand = "ExecuteProcedure(" + procedureName;

				try
				{
					// Create procedure command using text (in addition create log of command)
					String sql = "execute procedure " + procedureName + "(";

					for (Int32 index = 0; index < parameters.Length; index++)
					{
						String valueText = ValueText(parameters[index]);

						logCommand += ", " + valueText;
						sql += "?, ";
					}

					logCommand += ")";

					if (parameters.Length > 0)
					{
						sql = sql.Substring(0, sql.Length - 2);
					}

					sql += ")";

					// Create DbCommand of type Stored Procedure
					DbCommand dbCommand = dbProviderFactory.CreateCommand();
					dbCommand.CommandType = CommandType.Text;
					dbCommand.CommandText = sql;
					dbCommand.Connection = dbConnection;
					dbCommand.Transaction = dbTransaction;

					// Now add in the parameters
					for (Int32 index = 0; index < parameters.Length; index++)
					{
						DbParameter dbParameter = dbProviderFactory.CreateParameter();
						dbParameter.ParameterName = "@Parameter" + index.ToString();
						dbParameter.Value = parameters[index];
						dbCommand.Parameters.Add(dbParameter);
					}

					// Execure stored procedure and process return values
					Logger.Instance.Debug(LoggerPrefix(userCredentials, guid), logCommand);

					if (dbStoredProcedureReadMode == DbReadMode.DbDataAdapter)
					{
						DataTable dataTable = new DataTable();
						DbDataAdapter dbDataAdapter = dbProviderFactory.CreateDataAdapter();
						dbDataAdapter.SelectCommand = dbCommand;
						dbDataAdapter.Fill(dataTable);
						dbDataAdapter.Dispose();

						if (dataTable != null && dataTable.Rows.Count > 0)
						{
							returnValues = new Object[dataTable.Columns.Count];

							for (Int32 index = 0; index < dataTable.Columns.Count; index++)
							{
								returnValues[index] = dataTable.Rows[0][index];
							}
						}

						dataTable.Dispose();
					}
					else
					{
						DbDataReader dbDataReader = dbCommand.ExecuteReader();

						if (dbDataReader != null && dbDataReader.HasRows)
						{
							returnValues = new Object[dbDataReader.FieldCount];
							dbDataReader.Read();

							for (Int32 index = 0; index < dbDataReader.FieldCount; index++)
							{
								returnValues[index] = dbDataReader[index];
							}

							dbDataReader.Close();
							dbDataReader.Dispose();
						}
					}

					dbCommand.Dispose();

					// Log command completion
					Logger.Instance.Debug(LoggerPrefix(userCredentials, guid), "ExecuteProcedure() Complete");
				}
				catch (Exception ex)
				{
					ValidateConnectionState(ref dbConnection, ref dbTransaction);
					sqlResponse.ParseException(ex);
					Logger.Instance.Error(LoggerPrefix(userCredentials, guid), ex);
				}
				finally
				{
					sqlResponse.Results = returnValues;
				}
			}

			return sqlResponse;
		}

		public static void RenewUserCredentials(ref UserCredentials userCredentials, UserCredentials newCredentials, Guid guid, ref String provider, ref DbProviderFactory dbProviderFactory, ref DbConnection dbConnection)
		{
			try
			{
				Logger.Instance.Debug("RenewUserCredentials()");

				if (dbConnection != null)
				{
					dbConnection.Close();
					dbConnection.Dispose();
					dbConnection = null;
				}

				userCredentials = newCredentials;
				InitializeDbConnection(userCredentials, guid, ref provider, ref dbProviderFactory, ref dbConnection);
			}
			catch (Exception ex)
			{
				Logger.Instance.Error(LoggerPrefix(userCredentials, guid), ex);
			}
		}

		private static String ValueText(Object value)
		{
			String text;

			if (value.Equals(DBNull.Value))
			{
				text = "null";
			}
			else if (value is String || value is DateTime)
			{
				text = "'" + value.ToString() + "'";
			}
			else
			{
				text = value.ToString();
			}

			return text;
		}

		private static String LoggerPrefix(UserCredentials userCredentials, Guid guid)
		{
			String prefix = "";

			if (guid != null)
			{
				prefix += guid.ToString() + ", ";
			}

			if (userCredentials != null && userCredentials.Environment.Length > 0)
			{
				prefix += userCredentials.Environment + ", ";
			}

			if (userCredentials != null && userCredentials.DatabaseUserId.Length > 0)
			{
				prefix += userCredentials.DatabaseUserId + ", ";
			}

			if (prefix.Length > 2)
			{
				prefix = "(" + prefix.Substring(0, prefix.Length - 2) + ") ==> ";
			}

			return prefix;
		}

		public static void DisposeOfDataAccess(Boolean isDisposing, ref UserCredentials userCredentials, Guid guid, ref String provider, ref DbProviderFactory dbProviderFactory, ref DbConnection dbConnection, ref DbTransaction dbTransaction)
		{
			if (dbTransaction != null || dbConnection != null)
			{
				Logger.Instance.Debug(LoggerPrefix(userCredentials, guid), "Disposing DataAccess Object");
			}

			if (dbTransaction != null)
			{
				try
				{
					try
					{
						dbTransaction.Rollback();
					}
					catch (InvalidOperationException)
					{
					}
				}
				catch (Exception ex)
				{
					Logger.Instance.Error(LoggerPrefix(userCredentials, guid), ex);
				}
				finally
				{
					try
					{
						dbTransaction.Dispose();
					}
					catch
					{
					}
					finally
					{
						dbTransaction = null;
					}
				}
			}

			if (dbConnection != null)
			{
				try
				{
					dbConnection.Close();
				}
				catch (Exception ex)
				{
					Logger.Instance.Error(LoggerPrefix(userCredentials, guid), ex);
				}
				finally
				{
					try
					{
						dbConnection.Dispose();
					}
					catch
					{
					}
					finally
					{
						dbConnection = null;
					}
				}
			}

			// Final clean up of objects
			dbProviderFactory = null;
			userCredentials = null;
			provider = null;
		}
	}
}

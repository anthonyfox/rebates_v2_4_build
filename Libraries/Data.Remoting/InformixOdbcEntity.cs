﻿using System;

namespace Evolve.Libraries.Data.Remoting
{
	public abstract class InformixOdbcEntity : Entity
	{
		public override bool IncludeIdentityColumnInInsert()
		{
			return true;
		}

		public override string DatabaseTableSyntax(string linkServer, string databaseName, string tableName)
		{
			return (databaseName.Length > 0 ? databaseName + ":" + tableName : tableName);
		}

		protected override string SelectBuilderReadDateTime(String tableName, String columnName, String columnAlias, DBDateTime dbDateTimeFrom, DBDateTime dbDateTimeTo)
		{
			String columnText = "";

			if (dbDateTimeFrom == DBDateTime.Year && dbDateTimeTo == DBDateTime.Day)
			{
				columnText = "extend(" + tableName + "." + columnName + ", year to day)";
			}
			else
			{
				columnText = tableName + "." + columnName;
			}

			columnText += " || '' as " + columnAlias;
			return columnText;
		}

		protected override string SelectBuilderFirstXRows(String sql, Int32 selectNRecords)
		{
			return sql.Replace("select ", "select first " + selectNRecords.ToString() + " ");
		}

		public override string SelectDistinct()
		{
			return "unique";
		}

		protected override void JoinTable(string databaseName, string tableName, string condition, string tableAlias, EntityJoinType entityJoinType)
		{
			String join = (JoinTableText.Length > 0 ? "," : "");

			switch (entityJoinType)
			{
				case EntityJoinType.Inner:
					break;
				case EntityJoinType.LeftOuter:
					join += "outer";
					break;
				default:
					throw new NotImplementedException("Join type not available in Informix: " + entityJoinType.ToString());
			}

			String alias = (tableAlias.Length > 0 ? tableAlias : tableName);
			join += " " + DatabaseTableSyntax(databaseName, tableName) + " as " + alias;

			this.JoinTableText += join;
			this.JoinCondition = (this.JoinCondition.Length > 0 ? this.JoinCondition + " and " : "") + condition;
		}

		public override String WildcardKeyword()
		{
			return "matches";
		}

		public override char WildcardCharacter()
		{
			return '*';
		}

		public override string CurrentDate()
		{
			return "today";
		}

		public override string CurrentTime()
		{
			return "current";
		}

		public override string GetIdentityValueSql()
		{
			return "select dbinfo('sqlca.sqlerrd1') from systables where tabid = 1";
		}

		public override string LockSql()
		{
			return "";
		}

		public override string DBDateTimeFunction(DBDateTime from, DBDateTime to, string valueText)
		{
			return "'" + valueText + "'";
		}

		public override string DBTimeSpanFunction(DBDateTime from, DBDateTime to, string valueText)
		{
			return DBDateTimeFunction(from, to, valueText);
		}
	}
}

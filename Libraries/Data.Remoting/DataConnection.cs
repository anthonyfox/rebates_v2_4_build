﻿using System;
using System.Data;
using System.Collections;
using System.Runtime.Remoting.Lifetime;
using Evolve.Libraries.Utilities;
using System.Data.SqlClient;

namespace Evolve.Libraries.Data.Remoting
{
	public class DataConnection : IDisposable
	{
		private IDataAccess _dataAccess = null;
		private ILease _lease = null;
		private RemoteObjectSponser _sponser = null;
		private Boolean _isPersistentConnection = false;
		private Boolean _renewObjectLeases = true;
		private ExtendedBindingList<Entity> _dirtyEntities = new ExtendedBindingList<Entity>();

		public Boolean IsTransactionOpen
		{
			get { return (_dataAccess == null ? false : _dataAccess.IsTransactionOpen); }
		}

		public String Guid
		{
			get { return (_dataAccess == null ?  "" : _dataAccess.Guid.ToString()); }
		}

		public Boolean RenewObjectLeases
		{
			get { return _renewObjectLeases; }
			set { _renewObjectLeases = value; }
		}

		public UserCredentials UserCredentials
		{
			get { return (_dataAccess == null ? null : _dataAccess.UserCredentials); }
		}

		public DataConnection()
		{
		}

		public SqlConnection SqlConnection
		{
			get
			{
				if (_dataAccess == null) return null;
				return _dataAccess.SqlConnection;
			}
		}

		public SqlTransaction SqlTransaction
		{
			get
			{
				if (_dataAccess == null) return null;
				return _dataAccess.SqlTransaction;
			}
		}

		public DataConnection(Boolean isPersistentConnection)
		{
			_isPersistentConnection = isPersistentConnection;
		}

		public DataConnection(Boolean isPersistentConnection, Boolean renewObjectLeases)
		{
			_isPersistentConnection = isPersistentConnection;
			_renewObjectLeases = renewObjectLeases;
		}

		~DataConnection()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		public void Dispose(Boolean isDisposing)
		{
			if (_dataAccess != null)
			{
				try
				{
					_dataAccess.CloseConnection();
				}
				catch
				{
				}
				finally
				{
					UnRegisterSponser();
				}
			}
		}

		private void RegisterSponser()
		{
			if (_dataAccess != null && _renewObjectLeases && !DataAccessServer.IsDirect)
			{
				_lease = (ILease) ((MarshalByRefObject) _dataAccess).GetLifetimeService();

				if (System.Diagnostics.Debugger.IsAttached && DataAccessServer.IsLocalHost)
				{
					// If we are debugging and using a localhost then increase the lease time of the remote 
					// data access object to prevent the object terminating during the debug
					_lease = (ILease) ((MarshalByRefObject) _dataAccess).GetLifetimeService();
					_lease.Renew(new TimeSpan(0, 30, 0));
				}

				// Get the lifetime lease, and register a sponser to ensure that our connection
				// object does not time out on us
				_sponser = new RemoteObjectSponser(DataAccessServer.SponserRenewTimeMilliseconds);
				_lease.Register(_sponser);
			}
		}

		private void UnRegisterSponser()
		{
			try
			{
				if (_lease != null && _sponser != null)
				{
					// De-register the Remote Object Sponser in order to allow the data access object to
					// timeout normally on the remote server
					_lease.Unregister(_sponser);
				}
			}
			catch
			{
			}
			finally
			{
				_sponser = null;
				_lease = null;
				_dataAccess = null;
			}
		}

		#region Expose DataAccess Methods

		public Boolean AuthenticateUser()
		{
			SqlResponse sqlResponse = OpenConnection(false);
			return (_dataAccess != null && _dataAccess.AuthenticateUser());
		}

		public SqlResponse OpenConnection()
		{
			return OpenConnection(true);
		}

		public SqlResponse OpenConnection(Boolean isAllowedToAuthentificate)
		{
			SqlResponse sqlResponse = null;
			Boolean isComplete = false;

			if (_dataAccess == null)
			{
				_dataAccess = DataAccessServer.NewConnection(_isPersistentConnection);
			}

			if (_dataAccess != null)
			{
				RegisterSponser();

				while (!isComplete && isAllowedToAuthentificate)
				{
					sqlResponse = _dataAccess.OpenConnection();

					if (sqlResponse.IsAuthentificationError && isAllowedToAuthentificate)
					{
						UserAuthentification.ReadCredentials(true);

						if (UserAuthentification.Credentials != null)
						{
							DataAccessServer.UserCredentials = UserAuthentification.Credentials;
							_dataAccess.RenewUserCredentials(UserAuthentification.Credentials);
						}
					}
					else
					{
						isComplete = true;
					}
				}
			}

			return sqlResponse;
		}

		public SqlResponse CloseConnection()
		{
			return CloseConnection(true);
		}

		internal SqlResponse CloseConnection(Boolean checkForPersistentConnection)
		{
			SqlResponse sqlResponse = null;

			if (_dataAccess != null)
			{
				try
				{
					if (_isPersistentConnection && checkForPersistentConnection)
					{
						throw new InvalidOperationException("You cannot explicitly close a PersistentConnection use DataAccessServer.TerminatePersistentConnection()");
					}
					else
					{
						sqlResponse = _dataAccess.CloseConnection();
					}
				}
				catch
				{
				}
				finally
				{
					UnRegisterSponser();
				}
			}

			return sqlResponse;
		}

		public SqlResponse BeginTransaction()
		{
			SqlResponse sqlResponse = null;

			if (_dataAccess != null)
			{
				sqlResponse = _dataAccess.BeginTransaction();
				_dirtyEntities.Clear();
			}

			return sqlResponse;
		}

		public SqlResponse BeginTransaction(IsolationLevel isolationLevel)
		{
			SqlResponse sqlResponse = null;

			if (_dataAccess != null)
			{
				sqlResponse = _dataAccess.BeginTransaction(isolationLevel);
				_dirtyEntities.Clear();
			}

			return sqlResponse;
		}

		public SqlResponse CommitTransaction()
		{
			return CommitTransaction(true);
		}

		public SqlResponse CommitTransaction(Boolean cleanEntityUponSuccess)
		{
			SqlResponse sqlResponse = null;

			if (_dataAccess != null)
			{
				sqlResponse = _dataAccess.CommitTransaction();
				if (sqlResponse != null && !sqlResponse.HasError && cleanEntityUponSuccess)
				{
					// Clean the columns and make ExistsOnDatabase match reality
					foreach (Entity dirtyEntity in _dirtyEntities)
					{
						dirtyEntity.CleanEntity();
					}
					_dirtyEntities.Clear();
				}
			}

			return sqlResponse;
		}

		public SqlResponse RollbackTransaction()
		{
			SqlResponse sqlResponse = null;

			if (_dataAccess != null)
			{
				sqlResponse = _dataAccess.RollbackTransaction();
			}

			return sqlResponse;
		}

		public SqlResponse ExecuteSql(String sql)
		{
			SqlResponse sqlResponse = null;

			if (_dataAccess != null)
			{
				sqlResponse = _dataAccess.ExecuteSql(sql);
			}

			return sqlResponse;
		}

		public SqlResponse ExecuteScalar(String sql)
		{
			SqlResponse sqlResponse = null;

			if (_dataAccess != null)
			{
				sqlResponse = _dataAccess.ExecuteScalar(sql);
			}

			return sqlResponse;
		}

		public SqlResponse ExecuteSqlToDataTable(String sql)
		{
			SqlResponse sqlResponse = null;

			if (_dataAccess != null)
			{
				sqlResponse = _dataAccess.ExecuteSqlToDataTable(sql);
			}

			return sqlResponse;
		}

		public SqlResponse ExecuteProcedure(String procedureName, Object[] parameters)
		{
			SqlResponse sqlResponse = null;

			if (_dataAccess != null)
			{
				sqlResponse = _dataAccess.ExecuteProcedure(procedureName, parameters);
			}

			return sqlResponse;
		}

		#endregion

		public void AddDirtyEntity(Entity entity)
		{
			if (!_dirtyEntities.Contains(entity))
			{
				_dirtyEntities.Add(entity);
			}
		}
	}
}

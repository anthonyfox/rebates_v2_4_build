﻿using System;

namespace Evolve.Libraries.Data.Remoting
{
	public abstract class SqlServerEntity : Entity
	{
		public override string DatabaseTableSyntax(string linkServer, string databaseName, string tableName)
		{
			if (linkServer.Length > 0)
			{
				linkServer = "[" + LinkServerNode(linkServer) + "]";
			}

			if (databaseName.Length > 0 && !databaseName.StartsWith("["))
			{
				databaseName = "[" + databaseName + "]";
			}

			if (!tableName.StartsWith("["))
			{
				tableName = "[" + tableName + "]";
			}

			string syntax = "";

			if (linkServer.Length > 0)
			{
				syntax += linkServer + ".";
			}

			if (databaseName.Length > 0)
			{
				syntax += databaseName + ".";
			}


			syntax += "dbo." + tableName;
			return syntax;
		}

		protected override string SelectBuilderReadDateTime(String tableName, String columnName, String columnAlias, DBDateTime dbDateTimeFrom, DBDateTime dbDateTimeTo)
		{
			String columnSelect = tableName + "." + columnName;

			if (columnName != columnAlias)
			{
				columnSelect += " as " + columnAlias;
			}

			return columnSelect;
		}

		protected override string SelectBuilderFirstXRows(String sql, Int32 selectNRecords)
		{
			return sql.Replace("select ", "select top " + selectNRecords.ToString() + " ");
		}

		public override string SelectDistinct()
		{
			return "distinct";
		}

		protected override string FunctionPrefix(string tableName)
		{
			int index = tableName.LastIndexOf('.');
			return tableName.Substring(0, index + 1);
		}

		protected override void JoinTable(string databaseName, string tableName, string condition, string tableAlias, EntityJoinType entityJoinType)
		{
			String join;

			switch (entityJoinType)
			{
				case EntityJoinType.Inner:
					join = "inner join";
					break;
				case EntityJoinType.LeftOuter:
					join = "left outer join";
					break;
				case EntityJoinType.RightOuter:
					join = "right outer join";
					break;
				default:
					throw new NotImplementedException("Join type not available in Sql Server: " + entityJoinType.ToString());
			}

			join += " " + DatabaseTableSyntax(databaseName, tableName);

			if (tableAlias.Length > 0)
			{
				join += " as " + tableAlias;
			}

			join += " on " + condition;
			this.JoinTableText += (this.JoinTableText.Length > 0 ? " " : "") + join;
		}

		public override String WildcardKeyword()
		{
			return "like";
		}

		public override char WildcardCharacter()
		{
			return '%';
		}

		public override string CurrentDate()
		{
			return "getdate()";
		}

		public override string CurrentTime()
		{
			return "getdate()";
		}

		public override string GetIdentityValueSql()
		{
			return $"select ident_current('{DatabaseTable}')";
        }

		public override string LockSql()
		{
			return "";
		}

		public override string DBDateTimeFunction(DBDateTime from, DBDateTime to, string valueText)
		{
			return "'" + valueText + "'";
		}

		public override string DBTimeSpanFunction(DBDateTime from, DBDateTime to, string valueText)
		{
			return "'" + valueText + "'";
		}
	}
}

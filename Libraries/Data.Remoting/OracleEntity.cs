﻿using System;
using System.Data;
using System.Reflection;

namespace Evolve.Libraries.Data.Remoting
{
	public abstract class OracleEntity : Entity
	{
		public override string DatabaseTableSyntax(string linkServer, string databaseName, string tableName)
		{
			return tableName;
		}

		protected override string SelectBuilderReadDateTime(String tableName, String columnName, String columnAlias, DBDateTime dbDateTimeFrom, DBDateTime dbDateTimeTo)
		{
			String columnSelect = tableName + "." + columnName;

			if (columnName != columnAlias)
			{
				columnSelect += " as " + columnAlias;
			}

			return columnSelect;
		}

		protected override string SelectBuilderFirstXRows(String sql, Int32 selectNRecords)
		{
			return "select * from (" + sql + ") where rownum <= " + selectNRecords.ToString();
		}

		public override string SelectDistinct()
		{
			return "unique";
		}

		protected override void JoinTable(string databaseName, string tableName, string condition, string tableAlias, EntityJoinType entityJoinType)
		{
			String join;

			switch (entityJoinType)
			{
				case EntityJoinType.Inner:
					join = "inner join";
					break;
				case EntityJoinType.LeftOuter:
					join = "left join";
					break;
				case EntityJoinType.RightOuter:
					join = "right join";
					break;
				default:
					throw new NotImplementedException("Join type not available in Oracle: " + entityJoinType.ToString());
			}

			join += " " + DatabaseTableSyntax(databaseName, tableName);

			if (tableAlias.Length > 0)
			{
				join += " " + tableAlias;
			}

			join += " on " + condition;
			this.JoinTableText += (this.JoinTableText.Length > 0 ? " " : "") + join;
		}

		public override String WildcardKeyword()
		{
			return "like";
		}

		public override char WildcardCharacter()
		{
			return '%';
		}

		public override string CurrentDate()
		{
			return "current_date";
		}

		public override string CurrentTime()
		{
			return "sysdate";
		}

		public override string GetIdentityValueSql()
		{
			return "select " + (String.IsNullOrEmpty(SequenceName) ? DatabaseTable + "_SEQ" : SequenceName) + ".currval from dual";
		}

		public override string LockSql()
		{
			return "for update nowait";
		}

		public override string DBDateTimeFunction(DBDateTime from, DBDateTime to, string valueText)
		{
			String convert = "";

			for (Int32 index = (Int32) from; index <= (Int32) to; index++)
			{
				switch ((DBDateTime) index)
				{
					case DBDateTime.Year:
						convert += "yyyy-";
						break;
					case DBDateTime.Month:
						convert += "mm-";
						break;
					case DBDateTime.Day:
						convert += "dd ";
						break;
					case DBDateTime.Hour:
						convert += "hh24:";
						break;
					case DBDateTime.Minute:
						convert += "mi:";
						break;
					case DBDateTime.Second:
						convert += "ss:";
						break;
					default:
						throw new DataException("Invalid DBDateTime value");
				}
			}

			return "to_date('" + valueText + "', '" + convert.Substring(0, convert.Length - 1) + "')";
		}

		public override string DBTimeSpanFunction(DBDateTime from, DBDateTime to, string valueText)
		{
			return this.DBDateTimeFunction(from, to, valueText);
		}
	}
}

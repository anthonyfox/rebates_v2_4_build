﻿namespace Evolve.Libraries.WinformsApplication
{
	partial class frmLockMessage
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this._pnlMessage = new System.Windows.Forms.Panel();
			this._lblLockMessage = new System.Windows.Forms.Label();
			this._lblInformationImage = new System.Windows.Forms.Label();
			this._btnYes = new System.Windows.Forms.Button();
			this._btnNo = new System.Windows.Forms.Button();
			this._btnDetail = new System.Windows.Forms.Button();
			this._grpLockDetail = new System.Windows.Forms.GroupBox();
			this._txtLockDetail = new System.Windows.Forms.TextBox();
			this._pnlMessage.SuspendLayout();
			this._grpLockDetail.SuspendLayout();
			this.SuspendLayout();
			// 
			// _pnlMessage
			// 
			this._pnlMessage.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this._pnlMessage.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this._pnlMessage.Controls.Add(this._lblLockMessage);
			this._pnlMessage.Controls.Add(this._lblInformationImage);
			this._pnlMessage.Location = new System.Drawing.Point(0, 0);
			this._pnlMessage.Name = "_pnlMessage";
			this._pnlMessage.Size = new System.Drawing.Size(407, 91);
			this._pnlMessage.TabIndex = 0;
			// 
			// _lblLockMessage
			// 
			this._lblLockMessage.AutoSize = true;
			this._lblLockMessage.Font = new System.Drawing.Font("Microsoft New Tai Lue", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
			this._lblLockMessage.Location = new System.Drawing.Point(73, 36);
			this._lblLockMessage.Name = "_lblLockMessage";
			this._lblLockMessage.Size = new System.Drawing.Size(304, 17);
			this._lblLockMessage.TabIndex = 1;
			this._lblLockMessage.Text = "Details are locked by another user, do you wish to retry ?";
			// 
			// _lblInformationImage
			// 
			this._lblInformationImage.Image = global::Evolve.Libraries.WinformsApplication.Properties.Resources.Annotate_info;
			this._lblInformationImage.Location = new System.Drawing.Point(21, 19);
			this._lblInformationImage.Name = "_lblInformationImage";
			this._lblInformationImage.Size = new System.Drawing.Size(46, 44);
			this._lblInformationImage.TabIndex = 0;
			// 
			// _btnYes
			// 
			this._btnYes.Location = new System.Drawing.Point(158, 108);
			this._btnYes.Name = "_btnYes";
			this._btnYes.Size = new System.Drawing.Size(75, 23);
			this._btnYes.TabIndex = 1;
			this._btnYes.Text = "Yes";
			this._btnYes.UseVisualStyleBackColor = true;
			// 
			// _btnNo
			// 
			this._btnNo.Location = new System.Drawing.Point(239, 108);
			this._btnNo.Name = "_btnNo";
			this._btnNo.Size = new System.Drawing.Size(75, 23);
			this._btnNo.TabIndex = 2;
			this._btnNo.Text = "&No";
			this._btnNo.UseVisualStyleBackColor = true;
			// 
			// _btnDetail
			// 
			this._btnDetail.Location = new System.Drawing.Point(320, 108);
			this._btnDetail.Name = "_btnDetail";
			this._btnDetail.Size = new System.Drawing.Size(75, 23);
			this._btnDetail.TabIndex = 3;
			this._btnDetail.Text = "&Detail";
			this._btnDetail.UseVisualStyleBackColor = true;
			// 
			// _grpLockDetail
			// 
			this._grpLockDetail.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this._grpLockDetail.Controls.Add(this._txtLockDetail);
			this._grpLockDetail.Location = new System.Drawing.Point(12, 142);
			this._grpLockDetail.Name = "_grpLockDetail";
			this._grpLockDetail.Size = new System.Drawing.Size(383, 153);
			this._grpLockDetail.TabIndex = 4;
			this._grpLockDetail.TabStop = false;
			this._grpLockDetail.Text = "Lock Message Details";
			// 
			// _txtLockDetail
			// 
			this._txtLockDetail.Dock = System.Windows.Forms.DockStyle.Fill;
			this._txtLockDetail.Location = new System.Drawing.Point(3, 16);
			this._txtLockDetail.Multiline = true;
			this._txtLockDetail.Name = "_txtLockDetail";
			this._txtLockDetail.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this._txtLockDetail.Size = new System.Drawing.Size(377, 134);
			this._txtLockDetail.TabIndex = 0;
			// 
			// frmLockMessage
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(407, 304);
			this.Controls.Add(this._grpLockDetail);
			this.Controls.Add(this._btnDetail);
			this.Controls.Add(this._btnNo);
			this.Controls.Add(this._btnYes);
			this.Controls.Add(this._pnlMessage);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmLockMessage";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.Text = "Database Locked";
			this._pnlMessage.ResumeLayout(false);
			this._pnlMessage.PerformLayout();
			this._grpLockDetail.ResumeLayout(false);
			this._grpLockDetail.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel _pnlMessage;
		private System.Windows.Forms.Label _lblInformationImage;
		private System.Windows.Forms.Label _lblLockMessage;
		private System.Windows.Forms.Button _btnYes;
		private System.Windows.Forms.Button _btnNo;
		private System.Windows.Forms.Button _btnDetail;
		private System.Windows.Forms.GroupBox _grpLockDetail;
		private System.Windows.Forms.TextBox _txtLockDetail;
	}
}
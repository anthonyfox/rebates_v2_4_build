﻿using System;
using System.Reflection;
using System.Threading;
using Evolve.Libraries.Data.Remoting;

namespace Evolve.Libraries.WinformsApplication
{
	public class CSharpApplication
	{
		// The default instance
		private static CSharpApplication _defaultValue = new CSharpApplication();
		// Initial count for the semaphore (very high value)
		private const Int32 _maximumCount = 10000;
		// System wide semaphore
		private Semaphore _semaphore;

		public static Int32 InstanceCount
		{
			get
			{
				// Release the semaphore and grab the previous count
				Int32 previousCount = _defaultValue._semaphore.Release();

				// Acquire the semaphore again
				_defaultValue._semaphore.WaitOne();

				// Evaluate the number of instances that are currently running
				return _maximumCount - previousCount;
			}
		}

		private CSharpApplication()
		{
			// Create a named system wide semaphore
			Boolean ownership = false;
			String name = Assembly.GetExecutingAssembly().Location.Replace(":", "").Replace("\\", "") + UserAuthentification.ApplicationUserId;

			// Create the semaphore (or get an existing one) and decrement the count
			_semaphore = new Semaphore(_maximumCount, _maximumCount, name, out ownership);
			_semaphore.WaitOne();
		}

		// Release the semaphore in destructor to ensure counter is decremented on close 
		// of application
		~CSharpApplication()
		{
			_semaphore.Release();
		}
	}
}

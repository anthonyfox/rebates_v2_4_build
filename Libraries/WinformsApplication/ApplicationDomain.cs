﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Windows.Forms;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;

namespace Evolve.Libraries.WinformsApplication
{
	public class ApplicationDomain
	{
		#region DefineUnhandledExceptionHandler

		#region METHOD: DefineUnhandledExceptionHandler
		/// <summary>
		/// Define handler for exceptions we don't explicitly handle so that our ApplicationError is always called before the program terminates
		/// </summary>
		public static void DefineUnhandledExceptionHandler()
		{
			AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(WinformsApplication.ApplicationDomain.CurrentDomain_UnhandledException);
			Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
		}
		#endregion

		#region EVENT: Application_ThreadException
		/// <summary>
		/// Call our ApplicationError routine to handle any ThreadException
		/// </summary>
		/// <param name="sender">Thread</param>
		/// <param name="e">ThreadExceptionEventArgs</param>
		private static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
		{
			ApplicationError applicationError = new ApplicationError(e.Exception);
		}
		#endregion

		#region EVENT: CurrentDomain_UnhandledException
		/// <summary>
		/// Call our ApplicationError to handle any exception that hasn't been handled already when the program terinates.
		/// </summary>
		/// <param name="sender">Application Domain</param>
		/// <param name="e">UnhandledExceptionEventArgs</param>
		static void CurrentDomain_UnhandledException(Object sender, UnhandledExceptionEventArgs e)
		{
			if (e.IsTerminating)
			{
				ApplicationError applicationError = new ApplicationError(e.ExceptionObject);
			}
		}
		#endregion
		#endregion

		#region SystemName
		/// <summary>
		/// Get the system name which is an an amalgamation of the product name and the SVN revision
		/// </summary>
		/// <returns>"ProductName(SvnRevision)"</returns>
		public static String SystemName()
		{
			String systemName = Application.ProductName;
			String svnRevision = SubVersion.Build.Revision;

			if (svnRevision != "")
			{
				systemName += " (" + svnRevision + ")";
			}

			return systemName;
		}
		#endregion

		#region FormSaveValues

		public delegate void SaveFormValuesToDatabaseObjects();
		public delegate void SaveDatabaseObjectsToDatabase();
		public delegate void OnSuccess();
		public delegate void OnFailure();

		#region METHOD: FormSaveValues
		/// <summary>
		/// Validate the form and if valid save the values to the database. 
		/// </summary>
		/// <remarks>Handles all the locking errors with prompts for retry.</remarks>
		/// <param name="errorProvider">Error provider for the form.</param>
		/// <param name="saveFormValuesToDatabaseObjects">Optional delegate to save the details for the form into database objects.</param>
		/// <param name="saveDatabaseObjectsToDatabase">Optional delegate to save the database objects to the database.</param>
		/// <param name="onSuccess">Optional delegate to call if database update is successful.</param>
		/// <param name="onFailure">Optional delegate to call if locking error encountered.</param>
		/// <returns>abortFormClose<value>TRUE if required to abort the closing of the form when validation fails or locking error encountered.</value>
		/// </returns>
		/// <exception cref="DatabaseException">Update to database fails with non-locking error.</exception>
		static public bool FormSaveValues(
			ErrorProvider errorProvider,
			SaveFormValuesToDatabaseObjects saveFormValuesToDatabaseObjects,
			SaveDatabaseObjectsToDatabase saveDatabaseObjectsToDatabase,
			OnSuccess onSuccess,
			OnFailure onFailure)
		{
			bool abortFormClose = false;
			
			Form form = (Form) errorProvider.ContainerControl;
			
			// Validate all the controls prior to saving
			form.ValidateChildren();

			if (ValidateControl.HasErrors(form, errorProvider))
			{
				Control invalidControl = ValidateControl.ControlWithError(form, errorProvider);
				FormUtilities.SetFocus(invalidControl);
				// Invalid controls, so abort the close of the form
				abortFormClose = true;
			}
			else
			{
				// Save the details from the form into entity objects
				if (saveFormValuesToDatabaseObjects != null)
				{
					saveFormValuesToDatabaseObjects();
				}

				if (saveDatabaseObjectsToDatabase == null)
				{
					// No save routine, so just call the success routine (if it exists) as we haven't failed to save.
					if (onSuccess != null)
					{
						onSuccess();
					}
				}
				else
				{
					// Now save everything to the database
					form.Cursor = Cursors.WaitCursor;
					string errorMessage = SaveToDatabase(saveDatabaseObjectsToDatabase, onSuccess, onFailure);

					if (!String.IsNullOrEmpty(errorMessage))
					{
						abortFormClose = true;
						MessageBox.Show(errorMessage, "Save Details", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}

					form.Cursor = Cursors.Default;
				}
			}

			return abortFormClose;
		}
		#endregion

		#region METHOD: SaveToDatabase
		/// <summary>
		/// Save database objects to the database.
		/// </summary>
		/// <remarks>
		/// If locking error encountered, then the user is prompted to retry.
		/// </remarks>
		/// ///<param name="saveDatabaseObjectsToDatabase">Delegate to save the database objects to the database.</param>
		/// <param name="onSuccess">Optional delegate to call if database update is successful.</param>
		/// <param name="onFailure">Optional delegate to call if locking error encountered.</param>
		/// <returns>ErrorMessage - This is an empty string if everything worked ok othewise it is the error message for a locking error.</returns>
		/// <exception cref="DatabaseException">Update to database fails with non-locking error.</exception>
		static private string SaveToDatabase(SaveDatabaseObjectsToDatabase saveDatabaseObjectsToDatabase, OnSuccess onSuccess, OnFailure onFailure)
		{
			Boolean retry = false;
			String errorMessage = "";

			do
			{
				try
				{
					retry = false;

					DataAccessServer.NewPersistentConnection();
					DataAccessServer.PersistentConnection.BeginTransaction();

					saveDatabaseObjectsToDatabase();

					DataAccessServer.PersistentConnection.CommitTransaction();
					DataAccessServer.TerminatePersistentConnection();

					if (onSuccess != null)
					{
						onSuccess();
					}
				}
				catch (DatabaseException databaseException)
				{
					DataAccessServer.PersistentConnection.RollbackTransaction();
					DataAccessServer.TerminatePersistentConnection();

					if (databaseException.IsLockError)
					{
						errorMessage = databaseException.Message;

						if (LockMessage(errorMessage) == DialogResult.Yes)
						{
							retry = true;
						}
						else
						{
							if (onFailure != null)
							{
								onFailure();
							}
						}
					}
					else if (databaseException.EntityConcurrencyError != EntityConcurrencyError.None)
					{
						errorMessage = "Record has been " + (databaseException.EntityConcurrencyError == EntityConcurrencyError.Update ? "amended" : "inserted") + " by another user";

						if (onFailure != null)
						{
							onFailure();
						}
					} 
					else
					{
						throw;
					}
				}
			}
			while (retry);

			return errorMessage;
		}
		#endregion

		#endregion

		#region SendEvolveMail

		public static Boolean SendEvolveMail(EvolveEMailType evolveEMailType, String message)
		{
			return SendEvolveMail(evolveEMailType, message, DateTime.Now);
		}

		public static Boolean SendEvolveMail(EvolveEMailType evolveEMailType, String message, DateTime messageTime)
		{
			Boolean hasSent = false;

			// Send a mail containing all the details if we are not running against a local access server
			// which would indicate we are running on our own PC's from Evolve
			try
			{
				if (ApplicationArguments.MailEnabled)
				{
					SmtpClient smtpClient = new SmtpClient(ApplicationArguments.SmtpServer, ApplicationArguments.SmtpPort);
					MailMessage mailMessage = new MailMessage(ApplicationArguments.MailFrom, ApplicationArguments.MailTo);

					String applicationUserId = (UserAuthentification.Credentials != null ? UserAuthentification.Credentials.ApplicationUserId : "");
					mailMessage.Subject = evolveEMailType.ToString().ToUpper() + " - " + UserAuthentification.Environment + ":" + applicationUserId + " " + Application.ProductName + " (" + SubVersion.Build.Revision + ") " + messageTime.ToString();

					mailMessage.Body = message;
					smtpClient.Send(mailMessage);

					hasSent = true;
				}
			}
			catch (Exception mailException)
			{
				MessageBox.Show(mailException.ToString(), "Failed to Email Error to " + ApplicationArguments.SmtpServer, MessageBoxButtons.OK, MessageBoxIcon.Error);
				ApplicationDomain.WriteErrorLog(mailException.ToString(), messageTime);
			}

			return hasSent;
		}

		#endregion

		#region SendEmail

		public static Boolean SendEmail(string toEmailAddress, string emailFrom, string emailSubject, String emailBody, string smtpServer, int smtpPort)
		{
			Boolean hasSent = false;

			try
			{
				if (ApplicationArguments.EvolveEmailEnabled)
				{
					SmtpClient smtpClient = new SmtpClient(smtpServer, smtpPort);
					MailMessage mailMessage = new MailMessage(emailFrom, toEmailAddress);

					String applicationUserId = (UserAuthentification.Credentials != null ? UserAuthentification.Credentials.ApplicationUserId : "");
					mailMessage.Subject = emailSubject;
					mailMessage.Body = emailBody;
					mailMessage.IsBodyHtml = true;

					smtpClient.Send(mailMessage);

					hasSent = true;
				}
			}
			catch (Exception mailException)
			{
				MessageBox.Show(mailException.ToString(), "Failed to send email to " + smtpServer, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}

			return hasSent;
		}
		
		#endregion

		#region WriteErrorLog

		/// <summary>
		/// Write the error to a suitable log file to ensure full and complete auditing of
		/// application errors
		/// </summary>
		/// <param name="message">String</param>
		/// <param name="messageTime">DateTime</param>
		public static void WriteErrorLog(String message, DateTime messageTime)
		{
			if (ApplicationArguments.ErrorLogEnabled)
			{
				String svnRevision = SubVersion.Build.Revision;
				String fileDate = messageTime.Year.ToString("0000") + messageTime.Month.ToString("00") + messageTime.Day.ToString("00");
				String fileName = Application.ProductName + "_" + fileDate + "_" + svnRevision + ".txt";
				StreamWriter writer = new StreamWriter(ApplicationArguments.ErrorFilePath + fileName, true);

				writer.WriteLine("     Application: " + Application.ProductName);
				writer.WriteLine("    SVN Revision: " + svnRevision);
				writer.WriteLine("     Environment: " + UserAuthentification.Environment);
				writer.WriteLine("Application User: " + UserAuthentification.ApplicationUserId);
				writer.WriteLine("            Date: " + messageTime.ToString() + "\r\n");
				writer.WriteLine(message + "\r\n\r\n");

				writer.Flush();
				writer.Close();
			}
		}
		#endregion

		#region MailSnapshot
		/// <summary>
		/// Email data snapshot to Evolve
		/// </summary>
		/// <param name="name">Title for the snapshot</param>
		/// <param name="unloads">List of entity unloads to attach</param>
		public static void MailSnapshot(String name, List<EntityUnload> unloads)
		{
			MailSnapshot(name, null, unloads);
		}

		/// <summary>
		/// Email screen snapshot to Evolve
		/// </summary>
		/// <param name="name">Title for the snapshot</param>
		/// <param name="screenshot">Screenshot to attach</param>
		public static void MailSnapshot(String name, Bitmap screenshot)
		{
			MailSnapshot(name, screenshot, null);
		}

		/// <summary>
		/// Email data and screen snapshot to Evolve
		/// </summary>
		/// <param name="name">Title for the snapshot</param>
		/// <param name="screenshot">Screenshot to attach</param>
		/// <param name="unloads">List of entity unloads to attach</param>
		public static void MailSnapshot(String name, Bitmap screenshot, List<EntityUnload> unloads)
		{
			if (screenshot != null || unloads != null)
			{
				SmtpClient smtpClient = new SmtpClient(ApplicationArguments.SmtpServer, ApplicationArguments.SmtpPort);
				MailMessage mailMessage = new MailMessage(ApplicationArguments.MailFrom, ApplicationArguments.MailTo);

				mailMessage.Subject = EvolveEMailType.SnapShot.ToString() + " - " + UserAuthentification.Environment + ":" + UserAuthentification.ApplicationUserId + " " + Application.ProductName + " (" + SubVersion.Build.Revision + ") " + DateTime.Now.ToString() + " Snapshot " + name;

				if (screenshot != null)
				{
					MemoryStream memoryStream = new MemoryStream();
					screenshot.Save(memoryStream, ImageFormat.Jpeg);
					memoryStream.Position = 0;

					Attachment attachment = new Attachment(memoryStream, MediaTypeNames.Image.Jpeg);
					ContentDisposition disposition = attachment.ContentDisposition;
					disposition.FileName = "screenshot.jpg";

					mailMessage.Attachments.Add(attachment);
				}

				if (unloads != null && unloads.Count > 0)
				{
					for (Int32 index = 0; index < unloads.Count; index++)
					{
						Attachment attachment = new Attachment(unloads[index].UnloadStream, unloads[index].FileName, null);
						mailMessage.Attachments.Add(attachment);
					}
				}

				smtpClient.Send(mailMessage);
			}
		}
		#endregion

		#region LockMessage

		public static DialogResult LockMessage(String message)
		{
			frmLockMessage frmLockMessage = new frmLockMessage(message);
			DialogResult dialogResult = frmLockMessage.ShowDialog();
			frmLockMessage.Dispose();

			return dialogResult;
		}

		#endregion
	}

	public enum EvolveEMailType
	{
		Error,
		Lock,
		SnapShot,
		Information,
		Warning
	}
}

﻿using System;
using System.Windows.Forms;
using Evolve.Libraries.Data.Remoting;

namespace Evolve.Libraries.WinformsApplication
{
	public class ApplicationStatus : StatusStrip
	{
		private const String SYSTEM_NAME_PANEL = "SystemName";
		private const String APPLICATION_USER_PANEL = "ApplicationUser";
		private const String ENVIRONMENT_PANEL = "Environment";

		public bool IsInDesignMode
		{
			get { return this.DesignMode; }
		}

		public bool IsInRuntimeMode
		{
			get { return !IsInDesignMode; }
		}

		public String SystemName
		{
			set
			{
				if (IsInRuntimeMode)
				{
					SetStatus(SYSTEM_NAME_PANEL, value);
				}
			}
			
			get
			{
				if (this.Items.Find(SYSTEM_NAME_PANEL, true).Length > 0)
				{
					return this.Items.Find(SYSTEM_NAME_PANEL, true)[0].Text;
				}
				else
				{
					return "";
				}
			}
		}

		public String ApplicationUser
		{
			set
			{
				if (IsInRuntimeMode)
				{
					SetStatus(APPLICATION_USER_PANEL, value);
				}
			}

			get
			{
				if (this.Items.Find(APPLICATION_USER_PANEL, true).Length > 0)
				{
					return this.Items.Find(APPLICATION_USER_PANEL, true)[0].Text;
				}
				else
				{
					return "";
				}
			}
		}

		public String Environment
		{
			set
			{
				if (IsInRuntimeMode)
				{
					SetStatus(ENVIRONMENT_PANEL, value);
				}
			}

			get
			{
				if (this.Items.Find(ENVIRONMENT_PANEL, true).Length > 0)
				{
					return this.Items.Find(ENVIRONMENT_PANEL, true)[0].Text;
				}
				else
				{
					return "";
				}
			}
		}

		public ApplicationStatus()
		{
			String systemName = ApplicationDomain.SystemName();
			InitializeComponent(systemName);
		}

		public ApplicationStatus(string systemName)
		{
			InitializeComponent(systemName);
		}

		private void InitializeComponent(string systemName)
		{
			if (systemName != null)
			{
				this.SetStandardFields(systemName);
			}

			this.Dock = DockStyle.Bottom;
		}

		public void SetStatus(String statusPanelName, String statusMessage)
		{
			if (IsInRuntimeMode && statusMessage.Length > 0)
			{
				if (this.Items.ContainsKey(statusPanelName))
				{
					this.Items.Find(statusPanelName, true)[0].Text = statusMessage;
				}
				else
				{
					ToolStripStatusLabel statusPanel = new ToolStripStatusLabel(statusMessage);
					statusPanel.Name = statusPanelName;
					statusPanel.AutoSize = true;
					statusPanel.BorderSides = ToolStripStatusLabelBorderSides.Right;
					statusPanel.BorderStyle = Border3DStyle.Raised;

					// If this is the application user panel and the name is different to the database user then
					// set a different background colour for that panel. This only happens if we set LOGNAME different
					// to the whoami user.
					if (statusPanelName == APPLICATION_USER_PANEL && UserAuthentification.Credentials.DatabaseUserId != statusMessage)
					{
						statusPanel.BackColor = System.Drawing.Color.LightSalmon;
					}

					// If this is the environment panel and we are using our local data access server
					// then set a different background color for that panel.
					if (statusPanelName == ENVIRONMENT_PANEL)
					{
						if (DataAccessServer.IsLocalHost)
						{
							statusPanel.BackColor = System.Drawing.Color.LightSalmon;
						}
					}

					this.Items.Add(statusPanel);
				}
			}
		}

		public void DeleteStatus(String statusPanelName)
		{
			if (this.Items.ContainsKey(statusPanelName))
			{
				this.Items.RemoveAt(this.Items.IndexOfKey(statusPanelName));
			}
		}

		public Boolean StatusPanelExists(String panelName)
		{
			return this.Items.ContainsKey(panelName);
		}

		private void SetStandardFields(String systemName)
		{
			if (UserAuthentification.Credentials != null)
			{
				SetStatus(SYSTEM_NAME_PANEL, systemName);
				SetStatus(APPLICATION_USER_PANEL, UserAuthentification.Credentials == null ? "ApplicationUser" : UserAuthentification.Credentials.ApplicationUserId);
				SetStatus(ENVIRONMENT_PANEL, UserAuthentification.Credentials == null ? "Environment" : UserAuthentification.Credentials.Environment);
			}
		}
	}
}

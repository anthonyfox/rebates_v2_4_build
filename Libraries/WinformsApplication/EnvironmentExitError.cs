﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evolve.Libraries.WinformsApplication
{
	public static class EnvironmentExitError
	{
		public const int FatalError = -1;
		public const int OvernightTermination = -2;
		public const int MaximumInstancesExceeded = -3;
		public const int AccessDenied = -4;
	}
}

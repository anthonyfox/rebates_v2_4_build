﻿using System;
using System.ComponentModel;
using System.Reflection;
using EvolveIT.Data.Remoting;
using EvolveIT.Utilities;

namespace EvolveIT.WinformsApplication
{
	public static class RemoteObjects
	{
		private static String _protocol = "tcp";
		private static String _ipAddress = "127.0.0.1";
		private static Int32 _port = 1234;

		public static String Protocol
		{
			get { return _protocol; }
			set { _protocol = value; }
		}

		public static String IPAddress
		{
			get { return _ipAddress; }
			set { _ipAddress = value; }
		}

		public static Int32 Port
		{
			get { return _port; }
			set { _port = value; }
		}

		public static Object CreateProxy(Type type)
		{
			return CreateProxy(type, new Type[] { });
		}

		public static Object CreateProxy(Type type, Type[] parameters)
		{
			Object proxyObject = null;

			if (LicenseManager.UsageMode == LicenseUsageMode.Runtime)
			{
				try
				{
					Int32 index = type.AssemblyQualifiedName.IndexOf(",");
					String factoryName = type.AssemblyQualifiedName.Insert(index, "Factory");
					Type factoryType = Type.GetType(factoryName);

					String serviceName = _protocol + "://" + _ipAddress + ":" + _port.ToString() + "/";
					serviceName += type.FullName.Remove(type.FullName.LastIndexOf('.') + 1, 1) + "Factory";
					serviceName = serviceName.Replace(".Remoting.", ".");

					Object factoryObject = Activator.GetObject(factoryType, serviceName);
					MethodInfo newInstance = factoryType.GetMethod("NewInstance", parameters);

					proxyObject = newInstance.Invoke(factoryObject, parameters);

					// Now find out if we need to set the user credentials up
					Boolean isEntity = false;

					foreach (Type interfaceType in type.GetInterfaces())
					{
						if (interfaceType.Equals(typeof(IEntity)))
						{
							isEntity = true;
							break;
						}
					}

					if (isEntity)
					{
						PropertyInfo userCredentials = typeof(IEntity).GetProperty("UserCredentials");
						userCredentials.SetValue(proxyObject, UserAuthentification.Credentials, null);
					}
				}
				catch (Exception ex)
				{
					Logger.WriteLog(ex);
				}
			}

			return proxyObject;
		}
	}
}

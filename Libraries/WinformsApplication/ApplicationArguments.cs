﻿using System;
using System.Collections;
using System.Security.Principal;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using System.Linq;
using Evolve.Libraries.Data.Remoting;
using Evolve.Libraries.Utilities;
using System.Xml.XPath;

namespace Evolve.Libraries.WinformsApplication
{
	public class ApplicationArguments
	{
		private static String _smtpServer = "mail.evolve-itconsulting.com";
		private static Int32 _smtpPort = 25;
		private static String _mailFrom = "dotnet@lafarge.com";
		private static String _mailTo = "support@evolve-itconsulting.com";
		private static Boolean _mailEnabled = false;

		private static String _evolveSmtpServer = "mail.evolve-itconsulting.com";
		private static Int32 _evolveSmtpPort = 25;
		private static String _evolveEmailFrom = "support@evolve-consultants.co.uk";
		private static Boolean _evolveEmailEnabled = false;
		private static Boolean _evolveCustomerSurveySend = false;

		private static String _errorFilePath = "C:\\";
		private static Boolean _errorLogEnabled = false;
		private static Int32 _initialLeaseTimeMilliseconds = 15000;
		private static Int32 _sponserTimeoutMilliseconds = 500;
		private static Int32 _renewOnCallTimeMilliseconds = 0;
		private static Int32 _sponserRenewTimeMilliseconds = 5000;
		private static Int32 _leaseManagerPollTimeMilliseconds = 5000;
		private static DateTime _overnightStartTime = DateTime.Today.AddHours(20);
		private static Double _overnightWarningMinutes = 5d;
		private static String _overnightExcludedApplications = "";
		private static Int32 _minimumDASRequired = 0;
		private static String _dataAccessServer = "localhost";
		private static String _dataAccessServerProtocol = "tcp";
		private static Int32 _dataAccessServerPort = 1234;
		private static Hashtable _applicationProperties = new Hashtable();

		public static String EvolveSmtpServer { get { return _evolveSmtpServer; } }
		public static Int32 EvolveSmtpPort { get { return _evolveSmtpPort; } }
		public static String EvolveEmailFrom { get { return _evolveEmailFrom; } }
		public static Boolean EvolveEmailEnabled { get { return _evolveEmailEnabled; } }
		public static Boolean EvolveCustomerSurveySend { get { return _evolveCustomerSurveySend; } }

		public static String SmtpServer { get { return _smtpServer; } }
		public static Int32 SmtpPort { get { return _smtpPort; } }
		public static String MailFrom { get { return _mailFrom; } }
		public static String MailTo { get { return _mailTo; } }
		public static Boolean MailEnabled { get { return _mailEnabled; } }

		public static String ErrorFilePath { get { return _errorFilePath; } }
		public static Boolean ErrorLogEnabled { get { return _errorLogEnabled; } }
		public static Int32 InitialLeaseTimeMilliseconds { get { return _initialLeaseTimeMilliseconds; } }
		public static Int32 SponserTimeoutMilliseconds { get { return _sponserTimeoutMilliseconds; } }
		public static Int32 RenewOnCallTimeMilliseconds	{ get { return _renewOnCallTimeMilliseconds; } }
		public static Int32 SponserRenewTimeMilliseconds { get { return _sponserRenewTimeMilliseconds; } }
		public static Int32 LeaseManagerPollTimeMilliseconds { get { return _leaseManagerPollTimeMilliseconds; } }
		public static DateTime OvernightStartTime { get { return _overnightStartTime; } }
		public static Double OvernightWarningMinutes { get { return _overnightWarningMinutes; } }
		public static String OvernightExcludedApplications { get { return _overnightExcludedApplications; } }
		public static Int32 MinimumDASRequired { get { return _minimumDASRequired; } }
		public static Hashtable ApplicationProperties { get { return _applicationProperties; } }

		public static void ProcessWindowsLaunch(string[] arguments)
		{
			ProcessWindowsLaunch(arguments, "");
		}

		public static void ProcessWindowsLaunch(string[] arguments, string groupPrefix)
		{
			ProcessWindowsLaunch(arguments, groupPrefix, false);
		}

		public static void ProcessWindowsLaunch(string[] arguments, string groupPrefix, bool isDirectAccess)
		{
			if (arguments.Length >= 1)
			{
				String environment = arguments[0];
				WindowsIdentity windowsIdentity = WindowsIdentity.GetCurrent();

				UserAuthentification.WindowsUser = windowsIdentity;
				UserAuthentification.Environment = environment;

				DataAccessServer.IsDirect = isDirectAccess;
				ReadGlobalConfiguration(environment);

				if (isDirectAccess)
				{
					UserAuthentification.SetDirectAccess(environment);
					DataAccessServer.UserCredentials = UserAuthentification.Credentials;

					DataConnection dataConnection = DataAccessServer.NewConnection();
					SqlResponse sqlResponse = dataConnection.OpenConnection();

					dataConnection.CloseConnection();
					dataConnection.Dispose();

					if (sqlResponse.HasError)
					{
						ApplicationError applicationError = new ApplicationError(sqlResponse.Message);
					}
				}
				else
				{
					// Check for DAS override in program arguments
					String server = arguments.SingleOrDefault(a => a.StartsWith("-server="));
					String protocol = arguments.SingleOrDefault(a => a.StartsWith("-protocol="));
					String port = arguments.SingleOrDefault(a => a.StartsWith("-port="));

					if (!String.IsNullOrEmpty(server))
					{
						_dataAccessServer = server.Substring(server.IndexOf('=') + 1);
					}

					if (!String.IsNullOrEmpty(protocol))
					{
						_dataAccessServerProtocol = protocol.Substring(protocol.IndexOf('=') + 1);
					}

					if (!String.IsNullOrEmpty(port))
					{
						Int32 portNumber;

						if (Int32.TryParse(port.Substring(port.IndexOf('=') + 1), out portNumber))
						{
							_dataAccessServerPort = portNumber;
						}
					}

					// Configure DAS information
					DataAccessServer.RenewOnCallTimeMilliseconds = _renewOnCallTimeMilliseconds;
					DataAccessServer.SponserRenewTimeMilliseconds = _sponserRenewTimeMilliseconds;

					DataAccessServer.Server = _dataAccessServer;
					DataAccessServer.Protocol = _dataAccessServerProtocol;
					DataAccessServer.Port = _dataAccessServerPort;

					DataAccessServer.SetSecurity();
				}

				// Check user in group
				Boolean userInGroup = ActiveDirectory.UserInGroup(groupPrefix, environment);

				if (userInGroup)
				{
					// Setup user database and application credentials
					UserAuthentification.MinimumDASRevisionRequired = ApplicationArguments.MinimumDASRequired;
					UserAuthentification.NeedsDatabaseId = !isDirectAccess;

					// Check instance count if required and start overnight logout timers
					CheckMaximumInstances();
					StartOvernightTimers();

					// Authentificate user to database
					UserAuthentification.ReadCredentials();
				}
				else
				{
					MessageBox.Show("You are not a member of the user group " + groupPrefix + "_" + environment, "Access Denied", MessageBoxButtons.OK, MessageBoxIcon.Stop);
					Environment.Exit(EnvironmentExitError.AccessDenied);
				}
			}
			else
			{
				ArgumentsError(arguments);
			}
		}

		public static void ProcessStandardArguments(String[] arguments)
		{
			if (arguments.Length >= 6)
			{
				// Now configure DataAccessService remoting details
				String environment = arguments[3];
				Int32 port;

				if (Int32.TryParse(arguments[2], out port))
				{
					DataAccessServer.Port = port;
				}
				else
				{
					throw new ArgumentException("DataAccessServer port number " + arguments[2] + " is invalid");
				}

				// Read the global application confiuration file and setup the application
				// configuration variables accordingly
				ReadGlobalConfiguration(environment);
				
				DataAccessServer.RenewOnCallTimeMilliseconds = ApplicationArguments.RenewOnCallTimeMilliseconds;
				DataAccessServer.SponserRenewTimeMilliseconds = ApplicationArguments.SponserRenewTimeMilliseconds;
				DataAccessServer.Server = arguments[0];
				DataAccessServer.Protocol = arguments[1];
				DataAccessServer.SetSecurity();

				// Setup user database and application credentials
				WindowsIdentity windowsIndentity = WindowsIdentity.GetCurrent();

				UserAuthentification.Environment = environment;
				UserAuthentification.WindowsUser = windowsIndentity;
				UserAuthentification.UserId = arguments[4];
				UserAuthentification.ApplicationUserId = arguments[5];
				UserAuthentification.MinimumDASRevisionRequired = ApplicationArguments.MinimumDASRequired;

				// Check instance count if required and start overnight logout timers
				CheckMaximumInstances();
				StartOvernightTimers();

				// Authentificate user to database
				UserAuthentification.ReadCredentials();
			}
			else
			{
				ArgumentsError(arguments);
			}
		}

		private static void CheckMaximumInstances()
		{
			// Check instance count if required
			Int32 maximumInstances = 0;

			if (_applicationProperties.ContainsKey("MaximumInstances"))
			{
				maximumInstances = Int32.Parse(_applicationProperties["MaximumInstances"].ToString());
			}

			if (maximumInstances > 0)
			{
				Int32 instanceCount = CSharpApplication.InstanceCount;

				if (instanceCount > maximumInstances)
				{
					MessageBox.Show("You already have the maximum number of instances of this application running", Application.ProductName + " (" + UserAuthentification.ApplicationUserId + ")", MessageBoxButtons.OK, MessageBoxIcon.Information);
					Environment.Exit(EnvironmentExitError.MaximumInstancesExceeded);
				}
			}
		}

		private static void ArgumentsError(String[] arguments)
		{
			// Unable to continue with program execution, insufficient details where specified
			String message;

			if (arguments.Length == 0)
			{
				message = "No program arguments where specified";
			}
			else
			{
				message = "Insufficient program arguments specified (";

				for (Int32 index = 0; index < arguments.Length; index++)
				{
					message += arguments[index].ToString() + ", ";
				}

				message = message.Substring(0, message.Length - 2) + ")";
			}

			throw new ArgumentException(message);
		}

		private static void ReadGlobalConfiguration(String environment)
		{
			try
			{
				String filePath = Application.StartupPath + "\\GlobalApplicationConfiguration.xml";
				Boolean fileExists = File.Exists(filePath);

				if (!fileExists && System.Diagnostics.Debugger.IsAttached)
				{
					// In development environment, pickup the application configuration file from its SVN
					// location
					filePath = Application.StartupPath.Substring(0, Application.StartupPath.IndexOf("Clients\\"));
					filePath += "Libraries\\WinformsApplication\\GlobalApplicationConfiguration.xml";
					fileExists = File.Exists(filePath);
				}

				if (fileExists)
				{
					String value = "";
					XmlDocument configuration = new XmlDocument();
					configuration.Load(filePath);

					// Process application error configurarion
					String basePath = "ApplicationError/";

					// Mail configuration
					value = ReadNodeValue(configuration, basePath + "Mail/SMTPServer");

					if (value != "")
					{
						_smtpServer = value;
					}

					value = ReadNodeValue(configuration, basePath + "Mail/SMTPPort");

					if (value != "")
					{
						_smtpPort = Int32.Parse(value);
					}

					value = ReadNodeValue(configuration, basePath + "Mail/MailFrom");

					if (value != "")
					{
						_mailFrom = value;
					}

					value = ReadNodeValue(configuration, basePath + "Mail/MailTo");

					if (value != "")
					{
						_mailTo = value;
					}

					value = ReadNodeValue(configuration, basePath + "Mail/Enabled");

					if (value != "")
					{
						_mailEnabled = Boolean.Parse(value);
					}

					// Evolve Mail configuration					
					basePath = "EvolveApplicationEmail/";

					value = ReadNodeValue(configuration, basePath + "EvolveEmail/EvolveSMTPServer");

					if (value != "")
					{
						_evolveSmtpServer = value;
					}

					value = ReadNodeValue(configuration, basePath + "EvolveEmail/EvolveEmailFrom");

					if (value != "")
					{
						_evolveEmailFrom = value;
					}

					value = ReadNodeValue(configuration, basePath + "EvolveEmail/EvolveEmailEnabled");

					if (value != "")
					{
						_evolveEmailEnabled = Boolean.Parse(value);
					}

					value = ReadNodeValue(configuration, basePath + "EvolveEmail/EvolveCustomerSurveySend");

					if (value != "")
					{
						_evolveCustomerSurveySend = Boolean.Parse(value);
					}

					// Log file configuration
					value = ReadNodeValue(configuration, basePath + "LogFile/FilePath");

					if (value != "")
					{
						_errorFilePath = value;
					}

					value = ReadNodeValue(configuration, basePath + "LogFile/Enabled");

					if (value != "")
					{
						_errorLogEnabled = Boolean.Parse(value);
					}

					// DataAccess remote object renewal seconds
					basePath = "DataAccessServer/";

					value = ReadNodeValue(configuration, basePath + "Name");

					if (value != "")
					{
						_dataAccessServer = value;
					}

					value = ReadNodeValue(configuration, basePath + "Protocol");

					if (value != "")
					{
						_dataAccessServerProtocol = value;
					}

					value = ReadNodeValue(configuration, basePath + "Port");

					if (value != "")
					{
						_dataAccessServerPort = Int32.Parse(value);
					}

					value = ReadNodeValue(configuration, basePath + "RemoteObjects/RenewOnCallTimeMilliseconds");

					if (value != "")
					{
						_renewOnCallTimeMilliseconds = Int32.Parse(value);
					}

					value = ReadNodeValue(configuration, basePath + "RemoteObjects/SponserRenewTimeMilliseconds");

					if (value != "")
					{
						_sponserRenewTimeMilliseconds = Int32.Parse(value);
					}

					value = ReadNodeValue(configuration, basePath + "RemoteObjects/MinimumRevisionRequired");

					if (value != "")
					{
						_minimumDASRequired = Int32.Parse(value);
					}

					// Direct Data Access
					if (DataAccessServer.IsDirect)
					{
						Boolean isEnvironmentFound = false;

						Hashtable settings = new Hashtable();
						XmlNode environmentSetup = configuration.SelectSingleNode("/GlobalApplicationConfiguration/DirectDataAccess/Environments");
						XmlNode generalSetup = configuration.SelectSingleNode("/GlobalApplicationConfiguration/DirectDataAccess/General");

						if (environmentSetup != null)
						{
							foreach (XmlNode node in environmentSetup.ChildNodes)
							{
								if (node.Name.StartsWith(environment + "_"))
								{
									settings.Add(node.Name, node.InnerText);
									isEnvironmentFound = true;
								}
							}
						}

						if (!isEnvironmentFound)
						{
							throw new ApplicationException("Invalid / unknown environment (" + environment + ") specified");
						}

						if (generalSetup != null)
						{
							foreach (XmlNode node in generalSetup.ChildNodes)
							{
								settings.Add(node.Name, node.InnerText);
							}
						}

						DataAccessServer.IsDirect = true;
						DataAccessConfiguration.Settings = settings;

						// Now read the environment linked server details
						Hashtable linkServers = new Hashtable();
						basePath = "/GlobalApplicationConfiguration/Application/";

						try
						{
							XmlNode linkServersXml = configuration.SelectSingleNode("/GlobalApplicationConfiguration/DirectDataAccess/Environments/" + environment + "_LinkServers");

							if (linkServersXml != null)
							{
								foreach (XmlNode node in linkServersXml.ChildNodes)
								{
									linkServers.Add(node.Name, node.InnerText);
								}
							}
						}
						catch (XPathException)
						{
						}

						DataAccessServer.LinkServers = linkServers;
					}

					// Overnight 
					basePath = "Overnights/";

					value = ReadNodeValue(configuration, basePath + "StartTime");

					if (value != "")
					{
						String[] parts = value.Split(new char[] { ':' });
						Double hours = Double.Parse(parts[0]);
						Double minutes = Double.Parse(parts[1]);

						_overnightStartTime = DateTime.Today.AddHours(hours).AddMinutes(minutes);
					}

					value = ReadNodeValue(configuration, basePath + "WarningMinutes");
					
					if (value != "")
					{
						_overnightWarningMinutes = Double.Parse(value);
					}

					value = ReadNodeValue(configuration, basePath + "ExcludedApplicationIds");
					
					if (value != "")
					{
						_overnightExcludedApplications = value;
					}

					// Now read the application specific configuration
					basePath = "/GlobalApplicationConfiguration/Application/";

					try
					{
						XmlNode applicationNode = configuration.SelectSingleNode(basePath + Application.ProductName);

						if (applicationNode != null)
						{
							foreach (XmlNode node in applicationNode.ChildNodes)
							{
								if (node.NodeType != XmlNodeType.Comment)
								{
									_applicationProperties.Add(node.Name, node.InnerText);
								}
							}
						}
					}
					catch (XPathException)
					{
					}
				}
			}
			catch (Exception ex)
			{
				throw new ArgumentException("Invalid GlobalApplicationsConfiguration.xml file found\r\n\r\n" + ex.Message);
			}
		}

		private static String ReadNodeValue(XmlDocument configuration, String nodeName)
		{
			String value = "";
			String basePath = "/GlobalApplicationConfiguration/";
			String xPath;

			xPath = basePath + UserAuthentification.Environment + "/" + nodeName;

			if (configuration.SelectSingleNode(xPath) != null)
			{
				// Environment specific value exists
				value = configuration.SelectSingleNode(xPath).InnerText;
			}
			else
			{
				// Global value exists
				xPath = basePath + nodeName;

				if (configuration.SelectSingleNode(xPath) != null)
				{
					value = configuration.SelectSingleNode(xPath).InnerText;
				}
			}

			return value;
		}

		private static void StartOvernightTimers()
		{
			//if (!_overnightExcludedApplications.Contains(Application.ProductName))
			//{
			//	if (_overnightStartTime < DateTime.Now)
			//	{
			//		_overnightStartTime = _overnightStartTime.AddDays(1);
			//	}

			//	Double warningInterval = _overnightStartTime.AddMinutes(_overnightWarningMinutes * -1).Subtract(DateTime.Now).TotalMilliseconds ;

			//	if (warningInterval < _overnightWarningMinutes * 1000)
			//	{
			//		String message = "Your application cannot be started as the overnights start at " + _overnightStartTime.Hour.ToString("00") + ":" + _overnightStartTime.Minute.ToString("00");
			//		MessageBox.Show(message, "Overnight Routines", MessageBoxButtons.OK, MessageBoxIcon.Information);
			//		Environment.Exit(EnvironmentExitError.OvernightTermination);
			//	}
			//	else
			//	{
			//		Timer warningTimer = new Timer();
			//		warningTimer.Interval = (Int32) _overnightStartTime.AddMinutes(_overnightWarningMinutes * -1).Subtract(DateTime.Now).TotalMilliseconds;
			//		warningTimer.Tick += new EventHandler(warningTimer_Tick);
			//		warningTimer.Start();
			//	}

			//	Timer stopApplicationTimer = new Timer();
			//	stopApplicationTimer.Interval = (Int32) _overnightStartTime.Subtract(DateTime.Now).TotalMilliseconds;
			//	stopApplicationTimer.Tick += new EventHandler(stopApplicationTimer_Tick);
			//	stopApplicationTimer.Start();
			//}
		}

		static void stopApplicationTimer_Tick(object sender, EventArgs e)
		{
			Environment.Exit(EnvironmentExitError.OvernightTermination);
		}

		static void warningTimer_Tick(object sender, EventArgs e)
		{
			Timer timer = (Timer) sender;
			timer.Stop();

			String message = "Your application will terminate in " + _overnightWarningMinutes.ToString() + (_overnightWarningMinutes == 1 ? " minute" : " minutes") + "\nfor overnight processing\n\nPlease complete your work and logout";
			MessageBox.Show(message, "Overnight Routines", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}
	}
}

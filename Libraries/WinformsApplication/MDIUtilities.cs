﻿using System;
using System.Windows.Forms;

namespace Evolve.Libraries.WinformsApplication
{
	public class MdiUtilities
	{
		public static Form OpenMDIChild(Form mdiParent, Type formType)
		{
			return OpenMDIChild(mdiParent, formType, null);
		}

		public static Form OpenMDIChild(Form mdiParent, Type formType, Object[] parameters)
		{
			Form form = null;

			try
			{
				foreach (Form childForm in mdiParent.MdiChildren)
				{

					if (childForm.GetType() == formType)
					{
						form = childForm;
						break;
					}
				}

				if (form == null)
				{
					if (parameters == null)
					{
						form = (Form) Activator.CreateInstance(formType);
					}
					else
					{
						form = (Form) Activator.CreateInstance(formType, parameters);
					}

					form.MdiParent = mdiParent;
					form.Show();
				}
				else
				{
					form.Focus();
				}
			}
			catch (Exception ex)
			{
				ApplicationError applicationError = new ApplicationError(ex);
			}

			return form;
		}
	}
}

﻿using System;
using System.Runtime.Serialization;


namespace Evolve.Libraries.WinformsApplication
{
    public class ProgramArgumentException : Exception
    {
        public ProgramArgumentException()
        {
        }

        public ProgramArgumentException(String message)
            : base(message)
        {
        }

        public ProgramArgumentException(String message, Exception innerException)
            : base(message, innerException)
        {
        }

        public ProgramArgumentException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}

﻿using System;
using System.Windows.Forms;
using Evolve.Libraries.Data.Remoting;

namespace Evolve.Libraries.WinformsApplication
{
	public partial class ApplicationError : Form
	{
		private Boolean _isFatal = true;
		private DateTime _errorTime = DateTime.Now;

		public ApplicationError(Object exceptionObject)
		{
			InitializeComponent();
			InitializeControl(exceptionObject);
		}

		public ApplicationError(Object exceptionObject, Boolean isFatal)
		{
			_isFatal = isFatal;

			InitializeComponent();
			InitializeControl(exceptionObject);
		}

		public ApplicationError(Exception ex)
		{
			InitializeComponent();
			InitializeControl(ex);
		}

		public ApplicationError(Exception ex, Boolean isFatal)
		{
			_isFatal = isFatal;

			InitializeComponent();
			InitializeControl(ex);
		}

		private void InitializeControl(Object exceptionObject)
		{
			Exception closeException = null;

			// Clean up public connection if used
			if (_isFatal && DataAccessServer.PersistentConnection != null)
			{
				try
				{
					DataAccessServer.PersistentConnection.CloseConnection();
				}
				catch (Exception exception)
				{
					closeException = exception;
				}
			}

			// Include date and time in window title
			this.Text += " (" + _errorTime.ToString() + ")";

			// Add event handlers
			btnCopy.Click += new EventHandler(btnCopy_Click);
			btnClose.Click += new EventHandler(btnClose_Click);
			this.FormClosed += new FormClosedEventHandler(ApplicationException_FormClosed);

			// Set exception text
			txtException.Text = "";

			if (exceptionObject is Exception)
			{
				Exception ex = (Exception) exceptionObject;

				if (ex.HelpLink != null)
				{
					txtException.Text = ex.HelpLink.ToString() + "\r\n\r\n";
				}
			}

			txtException.Text += exceptionObject.ToString();

			if (closeException != null)
			{
				txtException.Text += "\r\n\r\n" + closeException.ToString();
			}

			// Process emails and logging as configured within the XML configuration
			ApplicationDomain.SendEvolveMail(EvolveEMailType.Error, txtException.Text, _errorTime);
			ApplicationDomain.WriteErrorLog(txtException.Text, _errorTime);

			// Finally show the user the dialog
			this.ShowDialog();

			// Force garbage collection to clear any unused objects (this is mainly to ensure
			// all database connections are closed cleanly asap)
			GC.Collect();
			GC.WaitForPendingFinalizers();
		}

		void btnCopy_Click(object sender, EventArgs e)
		{
			Clipboard.Clear();
			Clipboard.SetText(txtException.Text);
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			try
			{
				this.Close();
			}
			catch
			{
			}
		}

		void ApplicationException_FormClosed(object sender, FormClosedEventArgs e)
		{
			if (_isFatal)
			{
				try
				{
					Environment.Exit(EnvironmentExitError.FatalError);
				}
				catch
				{
				}
			}
		}

		private void txtException_TextChanged(object sender, EventArgs e)
		{

		}
	}
}

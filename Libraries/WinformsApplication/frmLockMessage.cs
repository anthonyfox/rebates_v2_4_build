﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Evolve.Libraries.WinformsApplication
{
	public partial class frmLockMessage : Form
	{
		private Boolean _hasMailedMessage = false;
	
		private const Int32 _STANDARD_HEIGHT = 183;
		private const Int32 _DETAIL_HEIGHT = 342;
	
		public frmLockMessage(String lockMessage)
		{
			InitializeComponent();

			// Initialise dialog result
			this.DialogResult = DialogResult.No;

			// Set the lock message, and set focus to retry yes
			_txtLockDetail.Text = lockMessage;
			_grpLockDetail.Visible = false;
			_btnYes.Focus();
			this.Height = _STANDARD_HEIGHT;

			// Setup event handlers
			_btnYes.Click += new EventHandler(_btnYes_Click);
			_btnNo.Click += new EventHandler(_btnNo_Click);
			_btnDetail.Click += new EventHandler(_btnDetail_Click);
		}

		void _btnYes_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.Yes;
			this.Close();
		}

		void _btnNo_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.No;
			this.Close();
		}

		void _btnDetail_Click(object sender, EventArgs e)
		{
			_grpLockDetail.Visible = !_grpLockDetail.Visible;
			_btnDetail.Text = (_grpLockDetail.Visible ? "&Hide" : "&Detail");
			this.Height = (_grpLockDetail.Visible ? _DETAIL_HEIGHT : _STANDARD_HEIGHT);

			if (_grpLockDetail.Visible && !_hasMailedMessage)
			{
				MailLockMessage();
			}
		}

		private void MailLockMessage()
		{
			_hasMailedMessage = ApplicationDomain.SendEvolveMail(EvolveEMailType.Lock, _txtLockDetail.Text);
		}
	}
}

﻿namespace Evolve.Libraries.WinformsApplication
{
	partial class ApplicationError
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblHelpDesk1 = new System.Windows.Forms.Label();
			this.lblDoNotClose1 = new System.Windows.Forms.Label();
			this.lblDoNotClose2 = new System.Windows.Forms.Label();
			this.txtException = new System.Windows.Forms.TextBox();
			this.btnCopy = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.applicationStatus = new Evolve.Libraries.WinformsApplication.ApplicationStatus();
			this.SuspendLayout();
			// 
			// lblHelpDesk1
			// 
			this.lblHelpDesk1.AutoSize = true;
			this.lblHelpDesk1.Location = new System.Drawing.Point(12, 14);
			this.lblHelpDesk1.Name = "lblHelpDesk1";
			this.lblHelpDesk1.Size = new System.Drawing.Size(432, 13);
			this.lblHelpDesk1.TabIndex = 1;
			this.lblHelpDesk1.Text = "A fatal error has occured. Please ring the Helpdesk on 0116 264 8199 and report t" +
				"he fault.";
			// 
			// lblDoNotClose1
			// 
			this.lblDoNotClose1.AutoSize = true;
			this.lblDoNotClose1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
			this.lblDoNotClose1.Location = new System.Drawing.Point(12, 43);
			this.lblDoNotClose1.Name = "lblDoNotClose1";
			this.lblDoNotClose1.Size = new System.Drawing.Size(494, 13);
			this.lblDoNotClose1.TabIndex = 4;
			this.lblDoNotClose1.Text = "Do not close this window until told to do so by the Helpdesk as it contains infor" +
				"mation";
			// 
			// lblDoNotClose2
			// 
			this.lblDoNotClose2.AutoSize = true;
			this.lblDoNotClose2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
			this.lblDoNotClose2.Location = new System.Drawing.Point(12, 56);
			this.lblDoNotClose2.Name = "lblDoNotClose2";
			this.lblDoNotClose2.Size = new System.Drawing.Size(230, 13);
			this.lblDoNotClose2.TabIndex = 5;
			this.lblDoNotClose2.Text = "which needs to be logged with the call.";
			// 
			// txtException
			// 
			this.txtException.Location = new System.Drawing.Point(15, 82);
			this.txtException.Multiline = true;
			this.txtException.Name = "txtException";
			this.txtException.ReadOnly = true;
			this.txtException.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.txtException.Size = new System.Drawing.Size(485, 169);
			this.txtException.TabIndex = 8;
			this.txtException.TabStop = false;
			this.txtException.TextChanged += new System.EventHandler(this.txtException_TextChanged);
			// 
			// btnCopy
			// 
			this.btnCopy.Location = new System.Drawing.Point(15, 257);
			this.btnCopy.Name = "btnCopy";
			this.btnCopy.Size = new System.Drawing.Size(131, 23);
			this.btnCopy.TabIndex = 9;
			this.btnCopy.Text = "Copy Error to Clipboard";
			this.btnCopy.UseVisualStyleBackColor = true;
			// 
			// btnClose
			// 
			this.btnClose.Location = new System.Drawing.Point(425, 257);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(75, 23);
			this.btnClose.TabIndex = 10;
			this.btnClose.Text = "Close";
			this.btnClose.UseVisualStyleBackColor = true;
			// 
			// applicationStatus
			// 
			this.applicationStatus.ApplicationUser = "";
			this.applicationStatus.Environment = "";
			this.applicationStatus.Location = new System.Drawing.Point(0, 301);
			this.applicationStatus.Name = "applicationStatus";
			this.applicationStatus.Size = new System.Drawing.Size(521, 22);
			this.applicationStatus.SystemName = "";
			this.applicationStatus.TabIndex = 0;
			this.applicationStatus.Text = "applicationStatus1";
			// 
			// ApplicationError
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(521, 323);
			this.Controls.Add(this.btnClose);
			this.Controls.Add(this.btnCopy);
			this.Controls.Add(this.txtException);
			this.Controls.Add(this.lblDoNotClose2);
			this.Controls.Add(this.lblDoNotClose1);
			this.Controls.Add(this.lblHelpDesk1);
			this.Controls.Add(this.applicationStatus);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ApplicationError";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Application Error";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private ApplicationStatus applicationStatus;
		private System.Windows.Forms.Label lblHelpDesk1;
		private System.Windows.Forms.Label lblDoNotClose1;
		private System.Windows.Forms.Label lblDoNotClose2;
		private System.Windows.Forms.TextBox txtException;
		private System.Windows.Forms.Button btnCopy;
		private System.Windows.Forms.Button btnClose;
	}
}

﻿using System;
using System.Reflection;

namespace Evolve.Libraries.SubVersion
{
	public class Build
	{
		private static Version _version = Assembly.GetCallingAssembly().GetName().Version;

		public static String Revision
		{
			get 
			{ 
				return _version.Revision.ToString();
			}
		}
	}
}

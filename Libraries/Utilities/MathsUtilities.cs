﻿using System;

namespace Evolve.Libraries.Utilities
{
	public static class MathsUtilities
	{
		#region METHOD: Ordinal

		public static String Ordinal(Int32 integer)
		{
			String ordinal;

			switch(Math.Abs (integer) % 100)
			{
				case 11:
				case 12:
				case 13:
					ordinal = "th";
					break;
				default:
					switch (Math.Abs(integer) % 10)
					{
						case 1:
							ordinal =  "st";
							break;
						case 2:
							ordinal =  "nd";
							break;
						case 3:
							ordinal =  "rd";
							break;
						default:
							ordinal =  "th";
							break;
					}
					break;
			}
			return ordinal;
		}
		#endregion
	}
}
﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Forms;

namespace Evolve.Libraries.Utilities
{
	public class EnumUtilities
	{
		public static void ComboBox(ComboBox comboBox, Type enumType)
		{
			ComboBox(comboBox, enumType, true);
		}

		public static void ComboBox(ComboBox comboBox, Type enumType, Boolean showNull)
		{
			ComboBox(comboBox, enumType, showNull, "");
		}

		public static void ComboBox(ComboBox comboBox, Type enumType, Boolean showNull, String nullText)
		{
			comboBox.DataSource = List(enumType, showNull, nullText);
			comboBox.ValueMember = "Value";
			comboBox.DisplayMember = "Description";
		}

		public static List<EnumItem> List(Type enumType)
		{
			return List(enumType, true);
		}

		public static List<EnumItem> List(Type enumType, Boolean showNull)
		{
			List<EnumItem> list = new List<EnumItem>();

			foreach (FieldInfo fieldInfo in enumType.GetFields(BindingFlags.Public | BindingFlags.Static))
			{
				EnumItem item = new EnumItem(fieldInfo.GetValue(fieldInfo), StringUtilities.ToSentence(fieldInfo.Name));

				if (showNull || fieldInfo.Name.ToLower() != "null")
				{
					list.Add(item);
				}
			}

			return list;
		}

		public static List<EnumItem> List(Type enumType, Boolean showNull, String nullText)
		{
			List<EnumItem> list = new List<EnumItem>();

			foreach (FieldInfo fieldInfo in enumType.GetFields(BindingFlags.Public | BindingFlags.Static))
			{
				EnumItem item = new EnumItem(fieldInfo.GetValue(fieldInfo), fieldInfo.Name, nullText);

				if (showNull || fieldInfo.Name.ToLower() != "null")
				{
					list.Add(item);
				}
			}

			return list;
		}
	}

	public class EnumItem
	{
		private Object _value;
		private String _description;

		public Object Value
		{
			get { return _value; }
		}

		public String Description
		{
			get { return _description; }
		}

		public EnumItem(Object value, String description)
		{
			Initialise(value, description, "");
		}

		public EnumItem(Object value, String description, String nullText)
		{
			Initialise(value, description, nullText);
		}

		private void Initialise(Object value, String description, String nullText)
		{
			_value = value;
			_description = (description.ToLower() == "null" ? nullText : description);
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;
using System.ComponentModel;

namespace Evolve.Libraries.Utilities
{
	#region ExportToFile

	public sealed class Exportable : Attribute
	{
		public Boolean CanExport = true;

		public Exportable(Boolean canExport)
		{
			this.CanExport = canExport;
		}
	}

	public static class GenericExtensionMethods
	{
		public static void ExportToFile<T>(this IEnumerable<T> items, String fileName)
		{
			GenericExtensionMethods.ExportToFile(items, fileName, ',');
		}

		public static void ExportToFile<T>(this IEnumerable<T> items, String fileName, Char delimiter)
		{
			// Open FileStream
			using (FileStream fileStream = new FileStream(fileName, FileMode.Create))
			{
				// Determine type and properties from entity of type T, and if they should be exported
				// outputing header details as we go
				Type type = typeof(T);
				PropertyInfo[] properties = type.GetProperties();
				Boolean[] export = new Boolean[properties.Length];
				StringBuilder header = new StringBuilder();

				for (Int32 index = 0; index < properties.Length; index++)
				{
					Object[] attributes = ((MemberInfo) type).GetCustomAttributes(typeof(Exportable), false);

					if (attributes != null && attributes.Length == 1)
					{
						export[index] = ((Exportable) attributes[0]).CanExport;
					}
					else
					{
						export[index] = true;
					}

					// Now export header details if required
					if (export[index])
					{
						header.Append(properties[index].Name + delimiter);
					}
				}

				if (header.Length > 0)
				{
					header.Remove(header.Length - 1, 1).AppendLine();
					Byte[] bytes = Encoding.ASCII.GetBytes(header.ToString());
					fileStream.Write(bytes, 0, bytes.Length);
				}

				// Output data based on exportable attribute info
				foreach (T item in items)
				{
					StringBuilder lineText = new StringBuilder();

					for (Int32 index = 0; index < properties.Length; index++)
					{
						// Now export if required
						if (export[index])
						{
							Object value = properties[index].GetValue(item, null);

							if (value != null)
							{
								lineText.Append(value.ToString());
							}

							lineText.Append(delimiter);
						}
					}

					// Remove last delimiter and write to file stream
					if (lineText.Length > 0)
					{
						lineText.Remove(lineText.Length - 1, 1).AppendLine();
						Byte[] bytes = Encoding.ASCII.GetBytes(lineText.ToString());
						fileStream.Write(bytes, 0, bytes.Length);
					}
				}

				fileStream.Close();
				fileStream.Dispose();
			}
		}
	}

	#endregion
}

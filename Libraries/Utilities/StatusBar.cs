﻿using System;
using System.Windows.Forms;

namespace Evolve.Libraries.Utilities
{
	class StatusStrip
	{
		public StatusStrip Create(String SystemName, String SubSystemName, String applicationUser, String environment)
		{
			StatusStrip statusStrip = new StatusStrip();
			//statusStrip.Name = "statusBar";

			ToolStripStatusLabel toolStripStatusLabelSystem = new ToolStripStatusLabel(SystemName + (SubSystemName.Length > 0 ? "." + SubSystemName : String.Empty));
			toolStripStatusLabelSystem.BorderSides = ToolStripStatusLabelBorderSides.Right;
			toolStripStatusLabelSystem.BorderStyle = Border3DStyle.Raised;
			//toolStripStatusLabelSystem.Name = "toolStripStatusLabelSystem";

			ToolStripStatusLabel toolStripStatusLabelLogin = new ToolStripStatusLabel(applicationUser);
			toolStripStatusLabelLogin.BorderSides = ToolStripStatusLabelBorderSides.Right;
			toolStripStatusLabelLogin.BorderStyle = Border3DStyle.Raised;
			//toolStripStatusLabelLogin.Name = "toolStripStatusLabelLogin";

			ToolStripStatusLabel toolStripStatusLabelEnvironment = new ToolStripStatusLabel(environment);
			toolStripStatusLabelEnvironment.BorderSides = ToolStripStatusLabelBorderSides.Right;
			toolStripStatusLabelEnvironment.BorderStyle = Border3DStyle.Raised;
			//toolStripStatusLabelEnvironment.Name = "toolStripStatusLabelEnvironment";

			//statusStrip.Container.Add(toolStripStatusLabelSystem);
			//statusStrip.Container.Add(toolStripStatusLabelLogin);
			//statusStrip.Container.Add(toolStripStatusLabelEnvironment);

			return statusStrip;
		}
	}
}

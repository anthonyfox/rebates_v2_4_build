﻿using System;
using System.Collections.Generic;

namespace Evolve.Libraries.Utilities
{
	/// <summary>
	/// Utilities to compare objects
	/// </summary>
	public static class CompareUtilities
	{
		#region METHOD: DictionaryIsEqual
		/// <summary>
		/// Compare that source dictionary matches target dictionary
		/// </summary>
		/// <typeparam name="T1">Type of source dictionary</typeparam>
		/// <typeparam name="T2">Type of target dictionary</typeparam>
		/// <param name="dictionary1">Source dictionary</param>
		/// <param name="dictionary2">Target dictionary</param>
		/// <returns>TRUE if keys and values are identical</returns>
		public static Boolean DictionaryIsEqual<T1, T2>(Dictionary<T1, T2> dictionary1, Dictionary<T1, T2> dictionary2)
		{
			Boolean isEqual = false;

			if (dictionary1 == null || dictionary2 == null)
			{
				isEqual = (dictionary1 == null && dictionary2 == null);
			}
			else
			{
				if (typeof(T2).GetInterface("IComparable") == null)
				{
					throw new ArgumentException("Must implement IComparable", "T2");
				}

				if (dictionary1.Count == dictionary2.Count)
				{
					isEqual = true;

					foreach (KeyValuePair<T1, T2> keyValuePair1 in dictionary1)
					{
						if (!dictionary2.ContainsKey(keyValuePair1.Key))
						{
							// Key from Dictionary1 does not exist in dictionary2
							isEqual = false;
							break;
						}

						T2 value2 = dictionary2[keyValuePair1.Key];

						if (((IComparable) keyValuePair1.Value).CompareTo(value2) != 0)
						{
							isEqual = false;
							break;
						}
					}
				}
			}

			return isEqual;
		}
		#endregion
	}

}

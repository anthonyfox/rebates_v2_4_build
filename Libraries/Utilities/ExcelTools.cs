﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Excel = Microsoft.Office.Interop.Excel;

namespace Evolve.Libraries.Utilities
{
	public class ExcelTools
	{
		public void ExportDataTable<T>(IEnumerable<T> data)
		{
			// Load excel and create a new workbook
			Excel.Application excelDocument = new Excel.Application();
			Excel.Worksheet worksheet = null;
			Type type = typeof (T);

			try
			{
				excelDocument.Workbooks.Add();

				// Get a reference to the active worksheet
				worksheet = (Excel.Worksheet) excelDocument.ActiveSheet;

				// Create column headings
				int columnIndex = 1;
				int rowIndex = 1;

				foreach (T row in data)
				{
					foreach (PropertyInfo property in type.GetProperties())
					{
						if (rowIndex == 1)
						{
							worksheet.Cells[columnIndex, rowIndex] = property.Name;
						}

						//worksheet.Cells[columnIndex, (rowIndex + 1)] = property.GetValue(row, null);
						columnIndex++;
					}

					columnIndex = 1;
					rowIndex++;
				}

				// Check file path
				excelDocument.Visible = true;
			}
			catch (Exception ex)
			{
				throw new Exception("ExcelTools.ExportDataTable(): Failed");
			}
			finally
			{
				// Clean up all COM references to ensure that Excel closes down properly
				if (worksheet != null)
				{
					System.Runtime.InteropServices.Marshal.ReleaseComObject(worksheet);
					System.Runtime.InteropServices.Marshal.FinalReleaseComObject(worksheet);
					worksheet = null;
				}

				System.Runtime.InteropServices.Marshal.ReleaseComObject(excelDocument);
				System.Runtime.InteropServices.Marshal.FinalReleaseComObject(excelDocument);
				excelDocument = null;
			}
		}
	}
}

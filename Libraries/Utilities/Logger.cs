using System;
using System.Diagnostics;
using System.IO;

namespace Evolve.Libraries.Utilities
{
	#region ENUM: LoggerLevel
	public enum LoggerLevel
	{
		Off,
		Errors,
		Information,
		Debug
	}
	#endregion

	#region CLASS: LoggerLevelConvertor
	public static class LoggerLevelConvertor
	{
		public static LoggerLevel StringToLoggerLevelConvertor(String code)
		{
			Char value = Char.MinValue;
			LoggerLevel loggerLevel = LoggerLevel.Off;

			if (code.Length == 1)
			{
				value = code[0];

				switch (value)
				{
					case 'O':
						loggerLevel = LoggerLevel.Off;
						break;
					case 'D':
						loggerLevel = LoggerLevel.Debug;
						break;
					case 'E':
						loggerLevel = LoggerLevel.Errors;
						break;
					case 'I':
						loggerLevel = LoggerLevel.Information;
						break;
				}
			}

			return loggerLevel;
		}
	}
	#endregion

	#region CLASS: Logger
	public sealed class Logger
	{
		#region PROPERTY: Instance
		private static readonly Logger _instance = new Logger();
		public static Logger Instance
		{
			get
			{
				return _instance;
			}
		}
		#endregion

		#region PROPERTY: LogFilePath
		private String _logFilePath = ".";
		public String LogFilePath
		{
			get { return _logFilePath; }
			set { _logFilePath = (value.Length == 0 ? "." : value); }
		}
		#endregion

		#region PROPERTY: FileName
		private String _fileName = "";
		public String FileName
		{
			get { return _fileName; }
			set { _fileName = value; }
		}
		#endregion

		#region PROPERTY: FileExtension
		private String _extension = ".txt";
		public String FileExtension
		{
			get { return _extension; }
			set { _extension = value; }
		}
		#endregion

		#region PROPERTY: Level
		private LoggerLevel _level = LoggerLevel.Debug;
		public LoggerLevel Level
		{
			get { return _level; }
			set { _level = value; }
		}
		#endregion

		#region PROPERTY: IncludeMilliseconds
		private Boolean _includeMilliseconds = false;
		public Boolean IncludeMilliseconds
		{
			get { return _includeMilliseconds; }
			set
			{
				_includeMilliseconds = value;
				this.Pad = String.Empty;
			}
		}
		#endregion

		#region PROPERTY: LogFile
		private String _logFile = "";
		private String LogFile
		{
			get
			{
				return _logFile;
			}
			set
			{
				_logFile = value;
			}
		}
		#endregion

		#region PROPERTY: StreamWriter
		private StreamWriter _streamWriter = null;
		private StreamWriter StreamWriter
		{
			get
			{
				return _streamWriter;
			}
			set
			{
				_streamWriter = value;
			}
		}
		#endregion

		#region PROPERTY: ThrowExceptions
		private Boolean _throwExceptions = true;
		public Boolean ThrowExceptions
		{
			get
			{
				return _throwExceptions;
			}
			set
			{
				_throwExceptions = value;
			}
		}
		#endregion

		#region PROPERTY: EvolveFilePath
		public static String EvolveFilePath
		{
			get
			{
				return "C:\\ProgramData\\Evolve Management Consultants\\";
			}
		}
		#endregion

		#region PROPERTY: Pad
		String _pad = String.Empty;
		public String Pad
		{
			get
			{
				if (_pad.Length == 0)
				{
					this.Pad = '\n' + new String(' ', this.TimeStamp.Length + 1);
				}
				return _pad;
			}
			private set
			{
				_pad = value;
			}
		}
		#endregion

		#region DERIVED PROPERTY: TimeStamp
		private String TimeStamp
		{
			get
			{
				DateTime now = DateTime.Now;
				return now.ToShortDateString() + " " + now.ToLongTimeString() + (this.IncludeMilliseconds ? "." + now.Millisecond.ToString("000") : "");
			}
		}
		#endregion

		#region Constructor
		private Logger()
		{
		}
		#endregion

		#region METHOD: Stop
		public void Stop()
		{
			try
			{
				if (this.StreamWriter != null)
				{
					lock (this.StreamWriter)
					{
						this.StreamWriter.Close();
						this.StreamWriter.Dispose();
					}
				}
			}
			catch
			{
			}
		}
		#endregion

		#region METHOD: Error
		public void Error(Exception ex)
		{
			Error("", ex);
		}

		public void Error(String prefix, Exception ex)
		{
			while (ex != null)
			{
				Append(ex.ToString(), LoggerLevel.Errors);
				ex = ex.InnerException;
			}
		}
		#endregion

		#region METHOD: Information
		public void Information(String message)
		{
			Information("", message);
		}

		public void Information(String prefix, String message)
		{
			Append(prefix + message, LoggerLevel.Information);
		}
		#endregion

		#region METHOD: Debug
		public void Debug(String message)
		{
			Debug("", message);
		}

		public void Debug(String prefix, String message)
		{
			Append(prefix + message, LoggerLevel.Debug);
		}
		#endregion

		#region METHOD: Append
		private void Append(String message, LoggerLevel level)
		{
			if (this.Level >= level && this.FileName != "")
			{
				String year = DateTime.Now.Year.ToString();
				String month = DateTime.Now.Month.ToString("00");
				String day = DateTime.Now.Day.ToString("00");

				String logFile = this.LogFilePath + "\\" + this.FileName + "_" + year + month + day + this.FileExtension;

				// Check to see if we need to close an existing stream and create a new log file
				// on change of day
				if (logFile != this.LogFile && this.StreamWriter != null)
				{
					lock (this.StreamWriter)
					{
						try
						{
							this.StreamWriter.Close();
							this.StreamWriter.Dispose();
						}
						catch
						{
							if (this.ThrowExceptions)
							{
								throw;
							}
						}
						finally
						{
							this.StreamWriter = null;
						}
					}
				}

				this.LogFile = logFile;

				// Check to see if the log file exists, if not create one
				try
				{
					if (!File.Exists(this.LogFile))
					{
						if (!Directory.Exists(Logger.Instance.LogFilePath))
						{
							Directory.CreateDirectory(Logger.Instance.LogFilePath);
						}

						FileStream fileStream = File.Create(this.LogFile);
						fileStream.Close();
					}
				}
				catch
				{
					if (this.ThrowExceptions)
					{
						throw;
					}
				}

				// Open the stream for writing
				if (this.StreamWriter == null)
				{
					try
					{
						this.StreamWriter = File.AppendText(this.LogFile);
					}
					catch
					{
						if (this.ThrowExceptions)
						{
							throw;
						}
					}
				}

				// Finally log the message
				if (this.StreamWriter != null)
				{
					try
					{
						lock (this.StreamWriter)
						{
							this.StreamWriter.WriteLine(this.TimeStamp + " " + message);
							this.StreamWriter.Flush();
						}
					}
					catch
					{
						if (this.ThrowExceptions)
						{
							throw;
						}
					}
				}
			}
		}
		#endregion

		#region METHOD: LogException
		public static void LogException(Exception ex, Logger logger, String programName)
		{
			if (logger == null)
			{
				String errorSource = programName;
				String errorLog = "Application";

				if (!EventLog.SourceExists(errorSource, errorLog))
				{
					EventLog.CreateEventSource(errorSource, errorLog);
				}

				while (ex != null)
				{
					EventLog.WriteEntry(errorSource, ex.ToString(), EventLogEntryType.Error);
					ex = ex.InnerException;
				}
			}
			else
			{
				logger.Error(ex);
			}
		}
		#endregion

		#region METHOD: PerformHouseKeeping
		public void PerformHouseKeeping(Int32 keepNDays)
		{
			DateTime today = DateTime.Today;

			if (keepNDays > 0)
			{
				try
				{
					String[] files = Directory.GetFiles(this.LogFilePath, this.FileName + "_*" + this.FileExtension);

					if (files != null && files.Length > 0)
					{
						foreach (String file in files)
						{
							DateTime creationTime = File.GetCreationTime(file);

							if (today.Subtract(creationTime.Date).Days > keepNDays)
							{
								File.Delete(file);
							}
						}
					}
				}
				catch
				{
					if (this.ThrowExceptions)
					{
						throw;
					}
				}
			}
		}
		#endregion
	}
	#endregion
}

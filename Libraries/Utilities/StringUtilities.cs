﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Evolve.Libraries.Utilities
{
	#region CLASS: StringUtilities
	/// <summary>
	/// Useful string functions
	/// </summary>
	public static class StringUtilities
	{
		#region METHOD: ReplaceFirstOccurance
		/// <summary>
		/// Replace first occurance of string
		/// </summary>
		/// <param name="haystack">String to search in</param>
		/// <param name="needle">String to replace</param>
		/// <param name="newNeedle">String to replace it with</param>
		/// <returns>Searched string after replacement</returns>
		public static String ReplaceFirstOccurance(String haystack, String needle, String newNeedle)
		{
			Int32 index = haystack.IndexOf(needle);
			return index == -1 ? haystack : haystack.Remove(index, needle.Length).Insert(index, newNeedle);
		}
		#endregion

		#region METHOD: ReplaceLastOccurance
		/// <summary>
		/// Replace last occurance of string
		/// </summary>
		/// <param name="haystack">String to search in</param>
		/// <param name="needle">String to replace</param>
		/// <param name="newNeedle">String to replace it with</param>
		/// <returns>Searched string after replacement</returns>
		public static String ReplaceLastOccurance(String haystack, String needle, String newNeedle)
		{
			Int32 index = haystack.LastIndexOf(needle);
			return index == -1 ? haystack : haystack.Remove(index, needle.Length).Insert(index, newNeedle);
		}
		#endregion

		#region METHOD: ToSentence
		/// <summary>
		/// Convert a camel case or pascal case string to a sentence by putting a space between the various words
		/// </summary>
		/// <param name="value">String to split</param>
		/// <returns>Sentence version of the string</returns>
		public static String ToSentence(String value)
		{
			StringBuilder sentence = new StringBuilder();
			Boolean firstWord = true;

			foreach (Match match in Regex.Matches(value, "([A-Z][a-z]+)|[A-Z]|[0-9]+"))
			{
				String word = match.Value;
				if (firstWord)
				{
					sentence.Append(word);
					firstWord = false;
				}
				else
				{
					sentence.Append(" ");
					sentence.Append(word);
				}
			}

			return sentence.ToString();
		}
		#endregion
	}
	#endregion

	#region CLASS: SliceRule
	/// <summary>
	/// Rules used to slice a string into fixed length strings. <see cref="SliceRule.Slice"/> for how to use the rules. 
	/// </summary>
	public class SliceRule
	{
		#region PROPERTY: RuleName
		/// <summary>
		/// Name of the slice rule
		/// </summary>
		private String _ruleName;
		public String RuleName
		{
			get
			{
				return _ruleName;
			}
			set
			{
				_ruleName = value;
			}
		}
		#endregion

		#region PROPERTY: Offset
		/// <summary>
		/// Offset from the start of the string to start the slice
		/// </summary>
		private Int32 _offset;
		public Int32 Offset
		{
			get
			{
				return _offset;
			}
			set
			{
				if (value < 0)
				{
					throw new ArgumentOutOfRangeException("Offset", "Value cannot be negative");
				}
				_offset = value;
			}
		}
		#endregion

		#region PROPERTY: Length
		/// <summary>
		/// Length of the slice
		/// </summary>
		private Int32 _length;
		public Int32 Length
		{
			get
			{
				return _length;
			}
			set
			{
				if (value < 0)
				{
					throw new ArgumentOutOfRangeException("Length", "Value cannot be negative");
				}
				_length = value;
			}
		}
		#endregion

		#region Constructors
		/// <summary>
		/// Initialise an instance of SliceRule
		/// </summary>
		/// <param name="ruleName">Name of the rule</param>
		/// <param name="offset">Offset for the start of the slice</param>
		/// <param name="length">Length of the slice</param>
		public SliceRule(String ruleName, Int32 offset = 0, Int32 length = 1)
		{
			this.RuleName = ruleName;
			this.Offset = offset;
			this.Length = length;
		}
		#endregion

		#region METHOD: Slice
		/// <summary>
		/// Slice the given string using the slice rules provided.
		/// </summary>
		/// <param name="message">String to slice</param>
		/// <param name="sliceRules">Collection of rules used to control the slice.</param>
		/// <returns>Dictionary of sliced strings with the rule name as the key</returns>
		/// <exception cref="System.ApplicationException">Offset + length from the slice rule exceeds length of string that is being sliced.</exception>
		public static Dictionary<String, String> Slice(String message, List<SliceRule> sliceRules)
		{
			Dictionary<String, String> result = new Dictionary<String, String>();
			Int32 length = message.Length;

			foreach (SliceRule sliceRule in sliceRules)
			{
				if (sliceRule.Offset + sliceRule.Length > length)
				{
					throw new ApplicationSliceException(sliceRule, message);
				}
				result[sliceRule.RuleName] = message.Substring(sliceRule.Offset, sliceRule.Length);
			}
			return result;
		}
		#endregion
	}
	#endregion

	#region CLASS: ApplicationSliceException
	/// <summary>
	/// String too short to slice
	/// </summary>
	public class ApplicationSliceException : ApplicationException
	{
		#region Properties

		#region PROPERTY: SliceRule
		SliceRule _sliceRule;
		/// <summary>
		/// Current slice rule
		/// </summary>
		public SliceRule SliceRule
		{
			get
			{
				return _sliceRule;
			}
			private set
			{
				_sliceRule = value;
			}
		}
		#endregion

		#region PROPERTY: SliceMessageLength
		private Int32 _sliceMessageLength;
		/// <summary>
		/// Length of message being sliced
		/// </summary>
		public Int32 SliceMessageLength
		{
			get
			{
				return _sliceMessageLength;
			}
			private set
			{
				_sliceMessageLength = value;
			}
		}
		#endregion

		#region PROPERTY: SliceMessage
		private String _sliceMessage;
		/// <summary>
		/// Message being sliced
		/// </summary>
		public String SliceMessage
		{
			get
			{
				return _sliceMessage;
			}
			private set
			{
				_sliceMessage = value;
				this.SliceMessageLength = value.Length;
			}
		}
		#endregion

		#region DERIVED PROPERTY: Message
		/// <summary>
		/// Override default exception message
		/// </summary>
		public new String Message
		{
			get
			{
				return String.Format(
					"Slice rule for {0} is invalid: Slice starts at offset {1} for {2} characters, but the subject is only {3} chars in length (Message is [{4}])",
					this.SliceRule.RuleName,
					this.SliceRule.Offset,
					this.SliceRule.Length,
					this.SliceMessageLength,
					this.SliceMessage);
			}
		}
		#endregion

		#endregion

		#region Constructors
		/// <summary>
		/// Create an instance of ApplicationSliceException
		/// </summary>
		/// <param name="sliceRule">Slice rule that triggered the exception</param>
		/// <param name="sliceMessage">Message being sliced</param>
		public ApplicationSliceException(SliceRule sliceRule, String sliceMessage)
		{
			this.SliceRule = sliceRule;
			this.SliceMessage = sliceMessage;
		}
		#endregion

	}
	#endregion

}

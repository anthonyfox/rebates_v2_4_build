﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.IO;

namespace Evolve.Libraries.Utilities
{
	public class WindowsServiceUtilities
	{
		#region METHOD: IsDebugAssembly
		/// <summary>
		/// Checks if given assembly is a debug assembly
		/// </summary>
		/// <param name="assembly">An assembly object of the assembly</param>
		/// <returns>True if the assembly can be debugged. False otherwise</returns>
		public static Boolean IsDebugAssembly(Assembly assembly)
		{
			foreach (Object attribute in assembly.GetCustomAttributes(false))
			{
				if (attribute is DebuggableAttribute)
				{
					return ((DebuggableAttribute) attribute).IsJITTrackingEnabled;
				}
			}
			return false;
		}
		#endregion

		/// <summary>
		/// Look for the file 'Debug.txt' in the Service Directory.  If it exists, give the user the option to launch the debugger
		/// </summary>
		/// <param name="currentDomain">Current domain</param>
		/// <param name="assembly">assembly (usually this.Getype().Assembly)</param>
		public static void Debugger(AppDomain currentDomain, Assembly assembly)
		{
			Directory.SetCurrentDirectory(currentDomain.BaseDirectory);

			if (WindowsServiceUtilities.IsDebugAssembly(assembly) && File.Exists("debug.txt"))
			{
				System.Diagnostics.Debugger.Launch();
			}
		}

	}
}

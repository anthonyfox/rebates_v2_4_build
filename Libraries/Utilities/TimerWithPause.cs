﻿using System;
using System.Diagnostics;
using System.Timers;

namespace Evolve.Libraries.Utilities
{
	/// <summary>
	/// Based on System.Timers.Timer, this class extends the base class to include
	/// Pause and Resume functionality
	/// </summary>
	public class TimerWithPause : Timer
	{
		private Stopwatch _stopwatch;
		private Boolean _paused;
		private Double _originalInterval;
		private Double? _intervalRemaining;

		/// <summary>
		/// Indicates if the timer is current paused
		/// </summary>
		public Boolean Paused
		{
			get { return _paused; }
		}

		/// <summary>
		/// The original timer interval (in Milliseconds)
		/// </summary>
		public Double OriginalInterval
		{
			get { return _originalInterval; }
		}

		/// <summary>
		/// Amount of time remaining to the next Elapsed event (in Milliseconds)
		/// </summary>
		public Double? IntervalRemaining
		{
			get { return _intervalRemaining; }
		}

		/// <summary>
		/// Indicates if the timer is currently enabled or not
		/// </summary>
		public new Boolean Enabled
		{
			get { return base.Enabled; }
			set { base.Enabled = value; ResetStopWatch(base.Enabled, true); }
		}

		/// <summary>
		/// Current timer interval (in Milliseconds)
		/// </summary>
		public new Double Interval
		{
			get
			{
				return base.Interval;
			}
			set
			{
				base.Interval = value;

				if (!_paused)
				{
					_originalInterval = base.Interval;
				}
			}
		}

		/// <summary>
		/// Start the timer
		/// </summary>
		public new void Start()
		{
			ResetStopWatch(true, true);
			base.Start();
		}

		/// <summary>
		/// Pause the timer
		/// </summary>
		public void Pause()
		{
			if (this.Enabled)
			{
				_intervalRemaining = this.Interval - _stopwatch.ElapsedMilliseconds;
				_paused = true;
				ResetStopWatch(false, false);
				base.Stop();
			}
		}

		/// <summary>
		/// Resume timer events
		/// </summary>
		public void Resume()
		{
			if (_paused)
			{
				this.Interval = (_intervalRemaining.HasValue ? _intervalRemaining.Value : _originalInterval);
				ResetStopWatch(true, false);
				base.Start();
			}
		}

		private void ResetStopWatch(Boolean startNew, Boolean resetPause)
		{
			if (_stopwatch != null)
			{
				_stopwatch.Stop();
				_stopwatch = null;
			}

			if (resetPause)
			{
				if (_paused)
				{
					this.Interval = _originalInterval;
				}

				_paused = false;
				_intervalRemaining = null;
			}

			if (startNew)
			{
				_stopwatch = Stopwatch.StartNew();
			}
		}

		~TimerWithPause()
		{
			Dispose(false);
		}

		public new void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private new void Dispose(Boolean isDisposing)
		{
			if (_stopwatch != null)
			{
				_stopwatch.Stop();
			}

			_stopwatch = null;
			base.Dispose();
		}
	}
}

﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Evolve.Libraries.Utilities
{
	public class Encryption
	{
		private Byte[] _key1 = { 63, 23, 35, 46, 5, 6, 7, 18, 39, 10, 11, 12, 13, 4, 15, 46, 17, 28, 19, 20, 44, 22, 3, 24 };
		private Byte[] _initialisationVector1 = { 85, 22, 33, 68, 71, 100, 178, 142 };

		private Byte[] _key2 = { 63, 23, 35, 46, 5, 6, 7, 18, 39, 10, 11, 12, 13, 4, 15, 46, 17, 28, 19, 20, 44, 22, 3, 24 };
		private Byte[] _initialisationVector2 = { 65, 110, 68, 26, 69, 178, 200, 219 };

		private Byte[] _key;
		private Byte[] _initialisationVector;

		public Encryption(EncryptionKey key)
		{
			switch (key)
			{
				case EncryptionKey.One:
					_key = _key1;
					_initialisationVector = _initialisationVector1;
					break;

				case EncryptionKey.Two:
					_key = _key2;
					_initialisationVector = _initialisationVector2;
					break;
			}
		}

		public String Encrypt(String data)
		{
			// Use UTF8Encoding to get Byte array of data
			UTF8Encoding utf8Encoder = new UTF8Encoding();
			Byte[] inputBytes = utf8Encoder.GetBytes(data);

			// Create a new TripleDES service provider
			TripleDESCryptoServiceProvider tripleDESProvider = new TripleDESCryptoServiceProvider();

			// The ICryptTransform interface uses the TripleDES crypt procider along
			// with encryption key and initialisation vector information
			ICryptoTransform cryptoTransform = tripleDESProvider.CreateEncryptor(_key, _initialisationVector);

			// All cryptographic functions need a stream to output the 
			// encrypted information. Here we declare a memory stream for 
			// this purpose
			MemoryStream encryptedStream = new MemoryStream();
			CryptoStream cryptoStream = new CryptoStream(encryptedStream, cryptoTransform, CryptoStreamMode.Write);

			// Write the encrypted information to the stream, flush the information
			// to ensure everything is out of the buffer
			cryptoStream.Write(inputBytes, 0, (Int32) inputBytes.Length);
			cryptoStream.FlushFinalBlock();
			encryptedStream.Position = 0;

			// Read the stream back into a byte array and return it to the calling
			// method
			Byte[] result = new Byte[encryptedStream.Length];
			encryptedStream.Read(result, 0, (Int32) encryptedStream.Length);
			cryptoStream.Close();

			String encryptedData = "";

			foreach (Byte singleByte in result)
			{
				encryptedData += (Char) singleByte;
			}

			// Convert special characters to their XML safe equivilent values
			encryptedData = encryptedData.Replace("&", "&amp;");
			encryptedData = encryptedData.Replace("<", "&lt;");
			encryptedData = encryptedData.Replace(">", "&gt;");
			encryptedData = encryptedData.Replace("\"", "&quot;");
			encryptedData = encryptedData.Replace("'", "&apos;");

			return encryptedData;
		}

		public String Decrypt(String encryptedData)
		{
			// Convert XML specific character strings back to their actual values
			encryptedData = encryptedData.Replace("&amp;", "&");
			encryptedData = encryptedData.Replace("&lt;", "<");
			encryptedData = encryptedData.Replace("&gt;", ">");
			encryptedData = encryptedData.Replace("&quot;", "\"");
			encryptedData = encryptedData.Replace("&apos;", "'");

			// Use UnicodeEncoding to get Byte array of encrypted data
			Byte[] inputBytes = new Byte[encryptedData.Length];

			for (Int32 index = 0; index < encryptedData.Length; index++)
			{
				inputBytes[index] = (Byte) encryptedData[index];
			}

			// Create a new TripleDES service provider
			TripleDESCryptoServiceProvider tripleDESProvider = new TripleDESCryptoServiceProvider();

			// As before we must provide the encryption/decryption key along with 
			// the initialisation vector
			ICryptoTransform cryptoTransform = tripleDESProvider.CreateDecryptor(_key, _initialisationVector);

			// Provide a memory stream to decrypt information into
			MemoryStream decryptedStream = new MemoryStream();
			CryptoStream cryptoStream = new CryptoStream(decryptedStream, cryptoTransform, CryptoStreamMode.Write);

			cryptoStream.Write(inputBytes, 0, (Int32) inputBytes.Length);
			cryptoStream.FlushFinalBlock();
			decryptedStream.Position = 0;

			// Read the memory stream and convert it back into a string 
			Byte[] result = new Byte[decryptedStream.Length];
			decryptedStream.Read(result, 0, (Int32) decryptedStream.Length);
			cryptoStream.Close();

			String data = "";

			foreach (Byte singleByte in result)
			{
				data += (Char) singleByte;
			}

			return data;
		}
	}

	public enum EncryptionKey
	{
		One,
		Two
	};
}

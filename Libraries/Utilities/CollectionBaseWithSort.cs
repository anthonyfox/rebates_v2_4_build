﻿using System;
using System.Collections;

namespace Evolve.Libraries.Utilities
{
	[Serializable]
	public abstract class CollectionBaseWithSort : CollectionBase
	{
		public void Sort(String sort)
		{
			InnerList.Sort(new ObjectComparer(sort));
		}
	}
}


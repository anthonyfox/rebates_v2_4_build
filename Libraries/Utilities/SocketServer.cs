﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;

namespace Evolve.Libraries.Utilities
{
	#region CLASS: MultiThreadedSocketListener
	public class MultiThreadedSocketListener
	{
		public delegate void InitialisedHandler(Object sender, MultiThreadedSocketListenerInitialisedEventArgs e);
		public delegate void NewClientHandlerHandler(Object sender, MultiThreadedSocketListenerNewClientHandlerHandlerEventArgs e);
		public delegate void MessageReceivedHandler(Object sender, MultiThreadedSocketListenerMessageReceivedEventArgs e);
		public delegate void ClientHandlerClosingHandler(Object sender, MultiThreadedSocketListenerClientHandlerClosingEventArgs e);

		#region PROPERTY: ProgramName
		private String _programName = "Evolve Multi Threaded Socket Listener";
		internal String ProgramName
		{
			get
			{
				return _programName;
			}
			private set
			{
				if (value.Length > 0)
				{
					_programName = value;
				}
				else
				{
					throw new ArgumentNullException("ProgramName");
				}
			}
		}
		#endregion

		#region PROPERTY: Logger
		private Logger _logger = null;
		public Logger Logger
		{
			get
			{
				return _logger;
			}
			set
			{
				_logger = value;
			}
		}
		#endregion

		#region PROPERTY: TcpListener
		private TcpListener _tcpListener;
		private TcpListener TcpListener
		{
			get
			{
				return _tcpListener;
			}
			set
			{
				_tcpListener = value;
			}
		}
		#endregion

		#region PROPERTY: ClientHandlers
		private List<ClientHandler> _clientHandlers;
		private List<ClientHandler> ClientHandlers
		{
			get
			{
				return _clientHandlers;
			}
			set
			{
				_clientHandlers = value;
			}
		}
		#endregion

		#region PROPERTY: ReclaimTimer
		private System.Timers.Timer _reclaimTimer;
		private System.Timers.Timer ReclaimTimer
		{
			get
			{
				return _reclaimTimer;
			}
			set
			{
				_reclaimTimer = value;
			}
		}
		#endregion

		#region PROPERTY: Status
		private MultiThreadedSocketListenerStatus _status = MultiThreadedSocketListenerStatus.Uninitialised;
		public MultiThreadedSocketListenerStatus Status
		{
			get
			{
				return _status;
			}
			private set
			{
				_status = value;
			}
		}
		#endregion

		#region PROPERTY: InitialLeaseTime
		private TimeSpan _initialLeaseTime = TimeSpan.FromSeconds(5);
		public TimeSpan InitialLeaseTime
		{
			get
			{
				return _initialLeaseTime;
			}
			set
			{
				if (value <= TimeSpan.Zero)
				{
					throw new ArgumentOutOfRangeException("InitialLeaseTime", "Must be greater than 0");
				}
				else
				{
					_initialLeaseTime = value;
					ResetTimers();
				}
			}
		}
		#endregion

		#region PROPERTY: SponsorshipTimeout
		private TimeSpan _sponsorshipTimeout = TimeSpan.FromMilliseconds(250);
		public TimeSpan SponsorshipTimeout
		{
			get
			{
				return _sponsorshipTimeout;
			}
			set
			{
				if (value <= TimeSpan.Zero)
				{
					throw new ArgumentOutOfRangeException("SponsorshipTimeout", "Must be greater than 0");
				}
				else
				{
					_sponsorshipTimeout = value;
					ResetTimers();
				}
			}
		}
		#endregion

		#region PROPERTY: Thread
		private Thread _thread;
		private Thread Thread
		{
			get
			{
				return _thread;
			}
			set
			{
				_thread = value;
			}
		}
		#endregion

		#region PROPERTY: DelegateStore
		/// <summary>
		/// Used to store delegates for events
		/// </summary>
		private Dictionary<Object, Delegate> _delegateStore = new Dictionary<Object, Delegate>();
		private Dictionary<Object, Delegate> DelegateStore
		{
			get
			{
				return _delegateStore;
			}
		}
		#endregion

		#region PROPERTY: InitialisedEventKey
		/// <summary>
		/// Unique key to access the delegate store for Initialised event
		/// </summary>
		private static Object _initialisedEventKey = new Object();
		private Object InitialisedEventKey
		{
			get
			{
				return _initialisedEventKey;
			}
		}
		#endregion

		#region PROPERTY: MessageReceivedEventKey
		/// <summary>
		/// Unique key to access the delegate store for MessageReceived event
		/// </summary>
		private static Object _messageReceivedEventKey = new Object();
		private Object MessageReceivedEventKey
		{
			get
			{
				return _messageReceivedEventKey;
			}
		}
		#endregion

		#region PROPERTY: NewClientHandlerEventKey
		/// <summary>
		/// Unique key to access the delegate store for Initialised event
		/// </summary>
		private static Object _newClientHandlerEventKey = new Object();
		private Object NewClientHandlerEventKey
		{
			get
			{
				return _newClientHandlerEventKey;
			}
		}
		#endregion

		#region PROPERTY: ClientHandlerClosingEventKey
		/// <summary>
		/// Unique key to access the delegate store for ClientHandlerClosing event
		/// </summary>
		private static Object _clientHandlerClosingEventKey = new Object();
		private Object ClientHandlerClosingEventKey
		{
			get
			{
				return _clientHandlerClosingEventKey;
			}
		}
		#endregion

		#region Constructor
		public MultiThreadedSocketListener(String programName)
		{
			this.Status = MultiThreadedSocketListenerStatus.Initialising;
			this.ProgramName = programName;
			this.Status = MultiThreadedSocketListenerStatus.Initialised;
		}
		#endregion

		#region METHOD: Process
		public void Process(IPAddress ipAddress, Int32 port)
		{
			// Initialise objects
			this.ClientHandlers = new List<ClientHandler>();
			this.ReclaimTimer = new System.Timers.Timer();

			// Initialise listener
			this.TcpListener = new TcpListener(ipAddress, port);

			try
			{
				// Start listener process
				this.TcpListener.Start();

				// Reset reclaimation and ping timers
				this.ReclaimTimer.Elapsed += new System.Timers.ElapsedEventHandler(ReclaimTimer_Elapsed);
				ResetTimers();
				OnInitialised();

				// Main listener loop
				Int32 connectionNumber = 0;

				this.Status = MultiThreadedSocketListenerStatus.Listening;

				while (this.Status == MultiThreadedSocketListenerStatus.Listening)
				{
					try
					{
						TcpClient tcpClient = this.TcpListener.AcceptTcpClient();
						this.Status = MultiThreadedSocketListenerStatus.Processing;
						connectionNumber = connectionNumber == 99999 ? 1 : connectionNumber + 1;

						// An incoming connection needs to be processed.
						ClientHandler clientHandler = new ClientHandler(this, tcpClient, connectionNumber);
						clientHandler.MessageReceived += new ClientHandler.MessageReceivedHandler(clientHandler_MessageReceived);
						clientHandler.Closing += new ClientHandler.ClosingHandler(clientHandler_Closing);
						OnNewClientHandler(clientHandler);
						lock (this.ClientHandlers)
						{
							this.ClientHandlers.Add(clientHandler);
						}
						clientHandler.Start();
					}
					catch (SocketException ex)
					{
						if (ex.SocketErrorCode != SocketError.Interrupted)
						{
							throw ex;
						}
					}
					if (this.Status != MultiThreadedSocketListenerStatus.Stopping)
					{
						this.Status = MultiThreadedSocketListenerStatus.Listening;
					}
				}

				CleanUp();
			}
			catch (Exception ex)
			{
				LogException(ex, this.Logger, this.ProgramName);
			}
			this.Status = MultiThreadedSocketListenerStatus.Stopped;
		}

		private static void LogException(Exception ex, Logger logger, String programName)
		{
			if (logger == null)
			{
				String errorSource = programName;
				String errorLog = "Application";

				if (!EventLog.SourceExists(errorSource, errorLog))
				{
					EventLog.CreateEventSource(errorSource, errorLog);
				}

				while (ex != null)
				{
					EventLog.WriteEntry(errorSource, ex.ToString(), EventLogEntryType.Error);
					ex = ex.InnerException;
				}
			}
			else
			{
				logger.Error(ex);
			}
		}
		#endregion

		#region METHOD: CleanUp
		private void CleanUp()
		{
			this.TcpListener.Stop();
			this.ReclaimTimer.Stop();

			foreach (ClientHandler clientHandler in this.ClientHandlers)
			{
				clientHandler.Stop();
			}

			Reclaim();
		}
		#endregion

		#region METHOD: Start
		public void Start(IPAddress ipAddress, Int32 port)
		{
			switch (this.Status)
			{
				case MultiThreadedSocketListenerStatus.Initialised:
				case MultiThreadedSocketListenerStatus.Stopped:
					this.Status = MultiThreadedSocketListenerStatus.Stopping;
					if (ipAddress == null)
					{
						ipAddress = IPAddress.Any;
					}

					this.Thread = new Thread(() => Process(ipAddress, port));
					this.Thread.Start();
					break;
			}
		}

		#endregion

		#region METHOD: Stop
		public void Stop()
		{
			this.Status = MultiThreadedSocketListenerStatus.Stopping;
			this.TcpListener.Stop();
		}
		#endregion

		#region METHOD: ResetTimers
		private void ResetTimers()
		{
			switch (this.Status)
			{
				case MultiThreadedSocketListenerStatus.Listening:
				case MultiThreadedSocketListenerStatus.Processing:
					this.ReclaimTimer.Stop();
					this.ReclaimTimer.Interval = this.SponsorshipTimeout.Milliseconds;
					this.ReclaimTimer.Start();
					break;
			}
		}
		#endregion

		#region METHOD: Reclaim
		private void Reclaim()
		{
			lock (this.ClientHandlers)
			{
				this.ClientHandlers.RemoveAll(c => !c.IsAlive);
			}

			// Ping each socket
			foreach (ClientHandler clientHandler in this.ClientHandlers)
			{
				DateTime now = DateTime.Now;
				if (clientHandler.StartTime.Add(this.InitialLeaseTime) <= DateTime.Now)
				{
					clientHandler.Ping();
				}
			}

			lock (this.ClientHandlers)
			{
				this.ClientHandlers.RemoveAll(c => !c.IsAlive);
			}

			Console.WriteLine("\n" + this.ClientHandlers.Count.ToString() + " Connected");
		}
		#endregion

		#region Events

		#region ClientHandler
		void clientHandler_MessageReceived(Object sender, ClientHandlerMessageReceivedEventArgs e)
		{
			ClientHandler clientHandler = (ClientHandler) sender;
			OnMessageReceived(clientHandler, e.Message);
		}

		void clientHandler_Closing(object sender, EventArgs e)
		{
			ClientHandler clientHandler = (ClientHandler) sender;
			OnClientHandlerClosing(clientHandler);
		}
		#endregion

		#region ReclaimTimer
		void ReclaimTimer_Elapsed(Object sender, System.Timers.ElapsedEventArgs e)
		{
			Reclaim();
		}
		#endregion

		#endregion

		#region EventHandlers

		#region EVENT: Initialised
		/// <summary>
		/// Add and remove delegates for the Initialised event
		/// </summary>
		public event InitialisedHandler Initialised
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			add
			{
				if (this.DelegateStore.ContainsKey(this.InitialisedEventKey))
				{
					this.DelegateStore[this.InitialisedEventKey] = Delegate.Combine((Delegate) this.DelegateStore[this.InitialisedEventKey], value);
				}
				else
				{
					this.DelegateStore[this.InitialisedEventKey] = value;
				}
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			remove
			{
				this.DelegateStore[this.InitialisedEventKey] = Delegate.Remove((Delegate) this.DelegateStore[this.InitialisedEventKey], value);
			}
		}

		/// <summary>
		/// Fire the delegates for the Initialised event
		/// </summary>
		private void OnInitialised()
		{
			if (this.DelegateStore.ContainsKey(this.InitialisedEventKey))
			{
				InitialisedHandler handler = (InitialisedHandler) this.DelegateStore[this.InitialisedEventKey];

				if (handler != null)
				{
					MultiThreadedSocketListenerInitialisedEventArgs eventArgs = new MultiThreadedSocketListenerInitialisedEventArgs();
					eventArgs.TcpListener = this.TcpListener;
					handler(this, eventArgs);
				}
			}
		}
		#endregion

		#region EVENT: NewClientHandler
		/// <summary>
		/// Add and remove delegates for the Initialised event
		/// </summary>
		public event NewClientHandlerHandler NewClientHandler
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			add
			{
				if (this.DelegateStore.ContainsKey(this.NewClientHandlerEventKey))
				{
					this.DelegateStore[this.NewClientHandlerEventKey] = Delegate.Combine((Delegate) this.DelegateStore[this.NewClientHandlerEventKey], value);
				}
				else
				{
					this.DelegateStore[this.NewClientHandlerEventKey] = value;
				}
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			remove
			{
				this.DelegateStore[this.NewClientHandlerEventKey] = Delegate.Remove((Delegate) this.DelegateStore[this.NewClientHandlerEventKey], value);
			}
		}

		/// <summary>
		/// Fire the delegates for the NewClientHandler event
		/// </summary>
		private void OnNewClientHandler(ClientHandler clientHandler)
		{
			if (this.DelegateStore.ContainsKey(this.NewClientHandlerEventKey))
			{
				NewClientHandlerHandler handler = (NewClientHandlerHandler) this.DelegateStore[this.NewClientHandlerEventKey];

				if (handler != null)
				{
					MultiThreadedSocketListenerNewClientHandlerHandlerEventArgs eventArgs = new MultiThreadedSocketListenerNewClientHandlerHandlerEventArgs();
					eventArgs.ClientHandler = clientHandler;
					handler(this, eventArgs);
				}
			}
		}
		#endregion

		#region EVENT: MessageReceived
		/// <summary>
		/// Add and remove delegates for the MessageReceived event
		/// </summary>
		public event MessageReceivedHandler MessageReceived
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			add
			{
				if (this.DelegateStore.ContainsKey(this.MessageReceivedEventKey))
				{
					this.DelegateStore[this.MessageReceivedEventKey] = Delegate.Combine((Delegate) this.DelegateStore[this.MessageReceivedEventKey], value);
				}
				else
				{
					this.DelegateStore[this.MessageReceivedEventKey] = value;
				}
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			remove
			{
				this.DelegateStore[this.MessageReceivedEventKey] = Delegate.Remove((Delegate) this.DelegateStore[this.MessageReceivedEventKey], value);
			}
		}

		/// <summary>
		/// Fire the delegates for the MessageReceived event
		/// </summary>
		private void OnMessageReceived(ClientHandler clientHandler, String message)
		{
			if (this.DelegateStore.ContainsKey(this.MessageReceivedEventKey))
			{
				MessageReceivedHandler handler = (MessageReceivedHandler) this.DelegateStore[this.MessageReceivedEventKey];

				if (handler != null)
				{
					MultiThreadedSocketListenerMessageReceivedEventArgs eventArgs = new MultiThreadedSocketListenerMessageReceivedEventArgs();
					eventArgs.ClientHandler = clientHandler;
					eventArgs.Message = message;
					handler(this, eventArgs);
				}
			}
		}
		#endregion

		#region EVENT: ClientHandlerClosing
		/// <summary>
		/// Add and remove delegates for the ClientHandlerClosing event
		/// </summary>
		public event ClientHandlerClosingHandler ClientHandlerClosing
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			add
			{
				if (this.DelegateStore.ContainsKey(this.ClientHandlerClosingEventKey))
				{
					this.DelegateStore[this.ClientHandlerClosingEventKey] = Delegate.Combine((Delegate) this.DelegateStore[this.ClientHandlerClosingEventKey], value);
				}
				else
				{
					this.DelegateStore[this.ClientHandlerClosingEventKey] = value;
				}
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			remove
			{
				this.DelegateStore[this.ClientHandlerClosingEventKey] = Delegate.Remove((Delegate) this.DelegateStore[this.ClientHandlerClosingEventKey], value);
			}
		}

		/// <summary>
		/// Fire the delegates for the ClientHandlerClosing event
		/// </summary>
		private void OnClientHandlerClosing(ClientHandler clientHandler)
		{
			if (this.DelegateStore.ContainsKey(this.ClientHandlerClosingEventKey))
			{
				ClientHandlerClosingHandler handler = (ClientHandlerClosingHandler) this.DelegateStore[this.ClientHandlerClosingEventKey];

				if (handler != null)
				{
					MultiThreadedSocketListenerClientHandlerClosingEventArgs eventArgs = new MultiThreadedSocketListenerClientHandlerClosingEventArgs();
					eventArgs.ClientHandler = clientHandler;
					handler(this, eventArgs);
				}
			}
		}
		#endregion

		#endregion
	}
	#endregion

	#region CLASS: MultiThreadedSocketListenerInitialisedEventArgs
	public class MultiThreadedSocketListenerInitialisedEventArgs : EventArgs
	{
		#region PROPERTY: TcpListener
		private TcpListener _tcpListener = null;
		public TcpListener TcpListener
		{
			get
			{
				return _tcpListener;
			}
			set
			{
				_tcpListener = value;
			}
		}
		#endregion
	}
	#endregion

	#region CLASS: MultiThreadedSocketListenerNewClientHandlerHandlerEventArgs
	public class MultiThreadedSocketListenerNewClientHandlerHandlerEventArgs : EventArgs
	{
		#region PROPERTY: ClientHandler
		private ClientHandler _clientHandler = null;
		public ClientHandler ClientHandler
		{
			get
			{
				return _clientHandler;
			}
			set
			{
				_clientHandler = value;
			}
		}
		#endregion
	}
	#endregion

	#region CLASS: MultiThreadedSocketListenerMessageReceivedEventArgs
	public class MultiThreadedSocketListenerMessageReceivedEventArgs : EventArgs
	{
		#region PROPERTY: ClientHandler
		private ClientHandler _clientHandler;
		public ClientHandler ClientHandler
		{
			get
			{
				return _clientHandler;
			}
			set
			{
				_clientHandler = value;
			}
		}
		#endregion

		#region PROPERTY: Message
		private String _message;
		public String Message
		{
			get
			{
				return _message;
			}
			set
			{
				_message = value;
			}
		}
		#endregion
	}
	#endregion

	#region CLASS: MultiThreadedSocketListenerClosingEventArgs
	public class MultiThreadedSocketListenerClientHandlerClosingEventArgs : EventArgs
	{
		#region PROPERTY: ClientHandler
		private ClientHandler _clientHandler;
		public ClientHandler ClientHandler
		{
			get
			{
				return _clientHandler;
			}
			set
			{
				_clientHandler = value;
			}
		}
		#endregion
	}
	#endregion

	#region CLASS: ClientHandler
	public class ClientHandler
	{
		public delegate void MessageReceivedHandler(Object sender, ClientHandlerMessageReceivedEventArgs e);
		public delegate void ClosingHandler(Object sender, EventArgs e);

		#region PROPERTY: Listener
		MultiThreadedSocketListener _listener;
		private MultiThreadedSocketListener Listener
		{
			get
			{
				return _listener;
			}
			set
			{
				_listener = value;
			}
		}
		#endregion

		#region PROPERTY: ClientSocket
		private TcpClient _clientSocket;
		private TcpClient ClientSocket
		{
			get
			{
				return _clientSocket;
			}
			set
			{
				_clientSocket = value;
			}
		}
		#endregion

		#region PROPERTY: ContinueProcess
		private Boolean _continueProcess = false;
		private Boolean ContinueProcess
		{
			get
			{
				return _continueProcess;
			}
			set
			{
				_continueProcess = value;
			}
		}
		#endregion

		#region PROPERTY: ClientThread
		private Thread _clientThread;
		private Thread ClientThread
		{
			get
			{
				return _clientThread;
			}
			set
			{
				_clientThread = value;
			}
		}
		#endregion

		#region PROPERTY: NetworkStream
		private NetworkStream _networkStream;
		private NetworkStream NetworkStream
		{
			get
			{
				return _networkStream;
			}
			set
			{
				_networkStream = value;
			}
		}
		#endregion

		#region PROPERTY: StartTime
		private DateTime _startTime = DateTime.Now;
		internal DateTime StartTime
		{
			get
			{
				return _startTime;
			}
		}
		#endregion

		#region PROPERTY: OutMessageTerminator
		private Char _outMessageTerminator = '\r';
		public Char OutMessageTerminator
		{
			get
			{
				return _outMessageTerminator;
			}
			set
			{
				_outMessageTerminator = value;
			}
		}
		#endregion

		#region PROPERTY: InMessageTerminator
		private Char _inMessageTerminator = '\r';
		public Char InMessageTerminator
		{
			get
			{
				return _inMessageTerminator;
			}
			set
			{
				_inMessageTerminator = value;
			}
		}
		#endregion

		#region PROPERTY: TerminateMessage
		private String _terminateMessage = null;
		public String TerminateMessage
		{
			get
			{
				return _terminateMessage;
			}
			set
			{
				_terminateMessage = value;
			}
		}
		#endregion

		#region PROPERTY: PingPhrase
		private String _pingPhrase = "Ping";
		public String PingPhrase
		{
			get
			{
				return _pingPhrase;
			}
			set
			{
				_pingPhrase = value;
			}
		}
		#endregion

		#region PROPERTY: ConnectionNumber
		private Int32 _connectionNumber = 0;
		public Int32 ConnectionNumber
		{
			get
			{
				return _connectionNumber;
			}
			private set
			{
				if (value <= 0)
				{
					throw new ArgumentOutOfRangeException("ConnectionNumber", "Must be greater than zero");
				}
				_connectionNumber = value;
			}
		}
		#endregion

		#region DERIVED PROPERTY: IsAlive
		public Boolean IsAlive
		{
			get
			{
				return (this.ClientThread != null && this.ClientThread.IsAlive);
			}
		}
		#endregion

		#region PROPERTY: DelegateStore
		/// <summary>
		/// Used to store delegates for events
		/// </summary>
		private Dictionary<Object, Delegate> _delegateStore = new Dictionary<Object, Delegate>();
		private Dictionary<Object, Delegate> DelegateStore
		{
			get
			{
				return _delegateStore;
			}
		}
		#endregion

		#region PROPERTY: MessageReceivedEventKey
		/// <summary>
		/// Unique key to access the delegate store for MessageReceived event
		/// </summary>
		private static Object _messageReceivedEventKey = new Object();
		private Object MessageReceivedEventKey
		{
			get
			{
				return _messageReceivedEventKey;
			}
		}
		#endregion

		#region PROPERTY: ClosingEventKey
		/// <summary>
		/// Unique key to access the delegate store for Closing event
		/// </summary>
		private static Object _closingEventKey = new Object();
		private Object ClosingEventKey
		{
			get
			{
				return _closingEventKey;
			}
		}
		#endregion

		#region Constructor
		public ClientHandler(MultiThreadedSocketListener listener, TcpClient clientSocket, Int32 connectionNumber)
		{
			InitialiseComponent(listener, clientSocket);
			this.ConnectionNumber = connectionNumber;
		}
		private void InitialiseComponent(MultiThreadedSocketListener listener, TcpClient clientSocket)
		{
			this.Listener = listener;
			this.ClientSocket = clientSocket;
		}
		#endregion

		#region METHOD: Start
		public void Start()
		{
			this.ContinueProcess = true;
			this.ClientThread = new Thread(new ThreadStart(Process));
			this.ClientThread.Start();
		}
		#endregion

		#region METHOD: Stop
		public void Stop()
		{
			this.ContinueProcess = false;

			if (this.IsAlive)
			{
				this.ClientThread.Join();
			}
		}
		#endregion

		#region METHOD: Process
		private void Process()
		{
			try
			{
				// Incoming data from the client.
				String data = String.Empty;

				// Data buffer for incoming data.
				Byte[] bytes;

				if (this.ClientSocket != null)
				{
					this.NetworkStream = this.ClientSocket.GetStream();

					while (this.ContinueProcess)
					{
						bytes = new byte[this.ClientSocket.ReceiveBufferSize];

						try
						{
							Int32 bytesRead = this.NetworkStream.Read(bytes, 0, (Int32) this.ClientSocket.ReceiveBufferSize);

							if (bytesRead > 0)
							{
								data += Encoding.ASCII.GetString(bytes, 0, bytesRead);

								Int32 pos;

								while ((pos = data.IndexOf(this.InMessageTerminator)) > -1)
								{
									// Extract the message (without the terminator)
									String message = data.Substring(0, pos);

									// Drop the message (and its terminator)
									data = data.Remove(0, pos + 1);

									if (this.TerminateMessage != null && this.TerminateMessage == message)
									{
										throw new NotImplementedException();
									}
									else
									{
										OnMessageReceived(message);
									}
								}
							}
							else
							{
								this.ContinueProcess = false;
							}
						}
						catch (IOException ex)
						{
							if (ex.InnerException != null && ex.InnerException is SocketException)
							{
								SocketException socketException = (SocketException) ex.InnerException;

								switch (socketException.SocketErrorCode)
								{
									case SocketError.ConnectionReset:
										this.ContinueProcess = false;
										break;
									default:
										throw ex;
								}
							}
							else
							{
								throw ex;
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				Logger.LogException(ex, this.Listener.Logger, this.Listener.ProgramName);
			}
			finally
			{

				OnClosing();
				this.NetworkStream.Close();
				this.ClientSocket.Close();
			}
		}
		#endregion

		#region METHOD: SendMessage
		public void SendMessage(String message)
		{
			Byte[] sendBytes = Encoding.ASCII.GetBytes(message + this.OutMessageTerminator);
			this.NetworkStream.Write(sendBytes, 0, sendBytes.Length);
		}
		#endregion

		#region METHOD: Ping
		public void Ping()
		{
			try
			{
				byte[] sendBytes = Encoding.ASCII.GetBytes(this.PingPhrase + this.OutMessageTerminator);
				this.NetworkStream.Write(sendBytes, 0, sendBytes.Length);
			}
			catch
			{
				this.ContinueProcess = false;
				this.NetworkStream.Close();
				this.ClientSocket.Close();
			}
		}
		#endregion

		#region EventHandlers

		#region EVENT: MessageReceived
		/// <summary>
		/// Add and remove delegates for the MessageReceived event
		/// </summary>
		public event MessageReceivedHandler MessageReceived
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			add
			{
				if (this.DelegateStore.ContainsKey(this.MessageReceivedEventKey))
				{
					this.DelegateStore[this.MessageReceivedEventKey] = Delegate.Combine((Delegate) this.DelegateStore[this.MessageReceivedEventKey], value);
				}
				else
				{
					this.DelegateStore[this.MessageReceivedEventKey] = value;
				}
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			remove
			{
				this.DelegateStore[this.MessageReceivedEventKey] = Delegate.Remove((Delegate) this.DelegateStore[this.MessageReceivedEventKey], value);
			}
		}

		/// <summary>
		/// Fire the delegates for the MessageReceived event
		/// </summary>
		private void OnMessageReceived(String message)
		{
			if (this.DelegateStore.ContainsKey(this.MessageReceivedEventKey))
			{
				MessageReceivedHandler handler = (MessageReceivedHandler) this.DelegateStore[this.MessageReceivedEventKey];

				if (handler != null)
				{
					ClientHandlerMessageReceivedEventArgs eventArgs = new ClientHandlerMessageReceivedEventArgs();
					eventArgs.Message = message;
					handler(this, eventArgs);
				}
			}
		}
		#endregion

		#region EVENT: Closing
		/// <summary>
		/// Add and remove delegates for the Closing event
		/// </summary>
		public event ClosingHandler Closing
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			add
			{
				if (this.DelegateStore.ContainsKey(this.ClosingEventKey))
				{
					this.DelegateStore[this.ClosingEventKey] = Delegate.Combine((Delegate) this.DelegateStore[this.ClosingEventKey], value);
				}
				else
				{
					this.DelegateStore[this.ClosingEventKey] = value;
				}
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			remove
			{
				this.DelegateStore[this.ClosingEventKey] = Delegate.Remove((Delegate) this.DelegateStore[this.ClosingEventKey], value);
			}
		}

		/// <summary>
		/// Fire the delegates for the MessageReceived event
		/// </summary>
		private void OnClosing()
		{
			if (this.DelegateStore.ContainsKey(this.ClosingEventKey))
			{
				ClosingHandler handler = (ClosingHandler) this.DelegateStore[this.ClosingEventKey];

				if (handler != null)
				{
					handler(this, null);
				}
			}
		}
		#endregion

		#endregion
	}
	#endregion

	#region CLASS: ClientHandlerMessageReceivedEventArgs
	public class ClientHandlerMessageReceivedEventArgs
	{
		#region PROPERTY: Message
		private String _message;
		public String Message
		{
			get
			{
				return _message;
			}
			set
			{
				_message = value;
			}
		}
		#endregion
	}
	#endregion

	public enum MultiThreadedSocketListenerStatus
	{
		Uninitialised,
		Initialising,
		Initialised,
		Starting,
		Listening,
		Processing,
		Stopping,
		Stopped
	}
}

﻿using System;
using System.DirectoryServices.AccountManagement;
using System.Linq;

namespace Evolve.Libraries.Utilities
{
	public class ActiveDirectory
	{
		public static Boolean UserInGroup(String groupPrefix, String environment)
		{
			Boolean userInGroup = false;
			UserPrincipal user = null;

			if (System.Diagnostics.Debugger.IsAttached)
			{
				userInGroup = true;
			}
			else
			{
				// Attempt to read user credentials from Active Directory (LDAP) server
				try
				{
					user = UserPrincipal.Current;
				}
				catch
				{
				}

				// Check if the user is in the requested user group
				if (user != null)
				{
					userInGroup = UserInGroup(user, groupPrefix, environment);
				}
			}

			return userInGroup;
		}

		public static Boolean UserInGroup(UserPrincipal user, String groupPrefix, String environment)
		{
			Boolean userInGroup = false;

			if (String.IsNullOrEmpty(groupPrefix) || String.IsNullOrEmpty(environment) || System.Diagnostics.Debugger.IsAttached)
			{
				userInGroup = true;
			}
			else
			{
				if (user != null)
				{
					using (PrincipalContext principalContext = new PrincipalContext(ContextType.Domain, Environment.UserDomainName))
					{
						PrincipalSearchResult<Principal> groups = user.GetGroups(principalContext);
						String groupName = groupPrefix + "_" + environment;
						userInGroup = (groups.Count(group => group.Name.ToUpper() == groupName.ToUpper()) > 0);
					}
				}
			}

			return userInGroup;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Evolve.Libraries.Utilities
{
	public class DataGridViewUtilities
	{
		public static void RemoveColumns(DataGridView dataGridView, List<String> columnsToKeep)
		{
			RemoveColumns(dataGridView, columnsToKeep, null);
		}

		public static void RemoveColumns(DataGridView dataGridView, List<String> columnsToKeep, DataGridViewDefaultColumnProperties defaults)
		{
			List<DataGridViewColumn> removeColumns = new List<DataGridViewColumn>();

			foreach (DataGridViewColumn column in dataGridView.Columns)
			{
				if (columnsToKeep.Contains(column.Name))
				{
					if (defaults != null)
					{
						if (defaults.ReadOnly != null)
						{
							column.ReadOnly = (Boolean) defaults.ReadOnly;
						}
					}
				}
				else
				{
					removeColumns.Add(column);
				}
			}

			foreach (DataGridViewColumn column in removeColumns)
			{
				dataGridView.Columns.Remove(column);
			}
		}

		public static void DataError(object sender, DataGridViewDataErrorEventArgs e)
		{
			e.ThrowException = true;
		}

		public static void SetFormat(DataGridView dataGridView, Type columnType, String format)
		{
			if (dataGridView != null)
			{
				foreach (DataGridViewColumn dataGridViewColumn in dataGridView.Columns)
				{
					if (dataGridViewColumn.ValueType.FullName == columnType.FullName)
					{
						SetColumnFormat(dataGridViewColumn, format, dataGridViewColumn.DefaultCellStyle.Alignment);
					}
				}
			}
		}

		public static void SetFormat(DataGridView dataGridView, Type columnType, String format, DataGridViewContentAlignment dataGridViewContentAlignment)
		{
			if (dataGridView != null)
			{
				foreach (DataGridViewColumn dataGridViewColumn in dataGridView.Columns)
				{
					if (dataGridViewColumn.ValueType.FullName == columnType.FullName)
					{
						SetColumnFormat(dataGridViewColumn, format, dataGridViewContentAlignment);
					}
				}
			}
		}

		public static void SetColumnFormat(DataGridViewColumn dataGridViewColumn, String format)
		{
			SetColumnFormat(dataGridViewColumn, format, dataGridViewColumn.DefaultCellStyle.Alignment);
		}

		public static void SetColumnFormat(DataGridViewColumn dataGridViewColumn, String format, DataGridViewContentAlignment dataGridViewContentAlignment)
		{
			if (dataGridViewColumn != null)
			{
				dataGridViewColumn.DefaultCellStyle.Format = format;
				dataGridViewColumn.DefaultCellStyle.Alignment = dataGridViewContentAlignment;
				dataGridViewColumn.HeaderCell.Style.Alignment = dataGridViewContentAlignment;
			}
		}

		#region Validate

		public delegate String DataGridViewCellValidator(DataGridViewCell cell);
		public delegate String DataGridViewRowValidator(DataGridViewRow row);
		public delegate String DataGridViewValidator(DataGridView dataGridView);

		public static void Validate(
			DataGridView dataGridView,
			ErrorProvider errorProvider,
			Control errorControl,
			DataGridViewCellValidator dataGridViewCellValidator,
			DataGridViewRowValidator dataGridViewRowValidator,
			DataGridViewValidator dataGridViewValidator)
		{
			String errorText = "";
			errorProvider.SetError(errorControl, "");

			Int16 rowErrorCount = 0;
			String rowsWithErrors = "";
			String singleRowError = "";

			for (Int16 row = 0; row < dataGridView.RowCount; row++)
			{
				if (row != dataGridView.NewRowIndex)
				{
					Int16 cellErrorCount = 0;
					String cellsWithErrors = "";
					String singleCellError = "";

					for (Int16 column = 0; column < dataGridView.ColumnCount; column++)
					{
						DataGridViewCell cell = dataGridView[column, row];
						errorText = dataGridViewCellValidator == null ? "" : dataGridViewCellValidator(cell);
						cell.ErrorText = errorText;

						if (errorText != "")
						{
							cellErrorCount++;

							cellsWithErrors += (cellErrorCount == 1 ? "" : ", ") + cell.OwningColumn.HeaderText;
							singleCellError = errorText;
						}
					}

					if (cellErrorCount > 0)
					{
						rowErrorCount++;
						if (cellErrorCount == 1)
						{
							singleRowError = singleCellError;
						}
						else
						{
							singleRowError = "Problems with " + StringUtilities.ReplaceLastOccurance(cellsWithErrors, ",", " and");
						}
						rowsWithErrors += (rowErrorCount == 1 ? "" : ", ") + (row + 1).ToString();
					}
					else
					{
						singleRowError = dataGridViewRowValidator == null ? "" : dataGridViewRowValidator(dataGridView.Rows[row]);
						if (singleRowError != "")
						{
							rowErrorCount++;
							rowsWithErrors += (rowErrorCount == 1 ? "" : ", ") + (row + 1).ToString();
						}
					}
					dataGridView.Rows[row].ErrorText = singleRowError;
				}
			}

			switch (rowErrorCount)
			{
				case 0:
					errorText = dataGridViewValidator == null ? "" : dataGridViewValidator(dataGridView);
					break;
				case 1:
					errorText = singleRowError;
					break;
				default:
					errorText = "Problems with rows " + StringUtilities.ReplaceLastOccurance(rowsWithErrors, ",", " and");
					break;
			}

			errorProvider.SetError(errorControl, errorText);
		}

		#endregion
	}

	public class DataGridViewDefaultColumnProperties
	{
		Boolean? _readOnly;

		public Boolean? ReadOnly
		{
			get { return _readOnly; }
			set { _readOnly = value; }
		}

	}
}

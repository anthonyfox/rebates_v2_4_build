﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace Evolve.Libraries.Utilities
{
	public interface IExtendedBindingList : IBindingList
	{
		void ApplySort(System.ComponentModel.ListSortDescriptionCollection sorts);
		string Filter { get; set; }
		void RemoveFilter();
		void Sort(string sort);
		System.ComponentModel.ListSortDescriptionCollection SortDescriptions { get; }
		bool SupportsAdvancedSorting { get; }
		bool SupportsFiltering { get; }
	}

	public class ExtendedBindingList<T> : BindingList<T>, IBindingListView, Evolve.Libraries.Utilities.IExtendedBindingList
	{
		#region DataGridView Functionality

		private Boolean _isSorted = false;
		private ListSortDirection _sortDirection = ListSortDirection.Ascending;
		private PropertyDescriptor _sortProperty = null;
		private String _sort;
		private String _filter;

		protected override Boolean SupportsSearchingCore
		{
			get { return true; }
		}

		protected override Boolean SupportsSortingCore
		{
			get { return true; }
		}

		protected override Boolean IsSortedCore
		{
			get { return _isSorted; }
		}

		protected override ListSortDirection SortDirectionCore
		{
			get { return _sortDirection; }
		}

		protected override PropertyDescriptor SortPropertyCore
		{
			get { return _sortProperty; }
		}

		public String Filter
		{
			get { return _filter; }
			set { _filter = value; }
		}

		public Boolean SupportsFiltering
		{
			get { return false; }
		}

		public void RemoveFilter()
		{
			_filter = String.Empty;
		}

		protected override void ApplySortCore(PropertyDescriptor property, ListSortDirection direction)
		{
			ArrayList sortedList = new ArrayList();

			// Check to see if the property type we are sorting by implements
			// the IComparable interface.
			Type interfaceType = property.PropertyType.GetInterface("IComparable");

			if (interfaceType == null && property.PropertyType.IsValueType)
			{
				Type underlyingType = Nullable.GetUnderlyingType(property.PropertyType);

				// Nullable.GetUnderlyingType() only returns a non-null value if the
				// supplied type was indeed a nullable type
				if (underlyingType != null)
				{
					interfaceType = underlyingType.GetInterface("IComparable");
				}
			}

			if (interfaceType != null)
			{
				// Store current sort details
				_sortDirection = direction;
				_sortProperty = property;

				this.Sort(_sortProperty.Name + (_sortDirection == ListSortDirection.Descending ? " desc" : ""));
				_isSorted = true;

				// Raise the ListChanged event so bound controls refresh their
				// values.        
				OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
			}
			else
			{
				// If the property type does not implement IComparable, let the user
				// know.        
				throw new NotSupportedException("Cannot sort by " + property.Name + ". This" + property.PropertyType.ToString() + " does not implement IComparable");
			}
		}

		protected override Int32 FindCore(PropertyDescriptor property, Object key)
		{
			Int32 index = 0;

			if (key != null)
			{
				for (index = 0; index < this.Items.Count; index++)
				{
					if (key.Equals(property.GetValue(this[index])))
					{
						break;
					}
				}

				if (index == this.Items.Count)
				{
					index = -1;
				}
			}

			return index;
		}

		#endregion

		#region Sort Functionality

		// Uncomment properties and Implement IBindingListView to get MultiColumn sorts

		private ListSortDescriptionCollection _sortDescriptions;
		private List<PropertyComparer<T>> _comparers;

		public ListSortDescriptionCollection SortDescriptions
		{
			get { return _sortDescriptions; }
		}

		public Boolean SupportsAdvancedSorting
		{
			get { return true; }
		}

		public void Sort(String sort)
		{
			_sort = sort;

			String[] propertyNames = sort.Split(new Char[] { ',' });
			ListSortDescription[] sortDescriptions = new ListSortDescription[propertyNames.Length];
			Type[] arguments = this.Items.GetType().GetGenericArguments();

			if (arguments != null && arguments.Length == 1)
			{
				PropertyDescriptorCollection propertyDescriptors = TypeDescriptor.GetProperties(arguments[0]);

				for (Int32 index = 0; index < propertyNames.Length; index++)
				{
					ListSortDirection direction = ListSortDirection.Ascending;
					propertyNames[index] = propertyNames[index].Trim();

					if (propertyNames[index].ToUpper().EndsWith(" ASC"))
					{
						propertyNames[index] = propertyNames[index].Remove(propertyNames[index].Length - 4);
					}

					if (propertyNames[index].ToUpper().EndsWith(" DESC"))
					{
						propertyNames[index] = propertyNames[index].Remove(propertyNames[index].Length - 5);
						direction = ListSortDirection.Descending;
					}

					PropertyDescriptor propertyDescriptor = propertyDescriptors.Find(propertyNames[index], false);

					if (propertyDescriptor != null)
					{
						sortDescriptions[index] = new ListSortDescription(propertyDescriptor, direction);
					}
					else
					{
						throw new ArgumentOutOfRangeException("Property " + propertyNames[index] + " not found");
					}
				}

				ApplySort(new ListSortDescriptionCollection(sortDescriptions));
			}
			else
			{
				throw new ArgumentException("Could not determine base generic type");
			}
		}

		public void ApplySort(ListSortDescriptionCollection sorts)
		{
			List<T> items = this.Items as List<T>;

			// Apply and set the sort, if items to sort
			if (items != null)
			{
				_sortDescriptions = sorts;
				_comparers = new List<PropertyComparer<T>>();

				foreach (ListSortDescription sort in sorts)
				{
					_comparers.Add(new PropertyComparer<T>(sort.PropertyDescriptor, sort.SortDirection));
				}

				items.Sort(CompareValuesByProperties);
				_isSorted = true;
			}
			else
			{
				_isSorted = false;
			}

			this.OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
		}

		private int CompareValuesByProperties(T x, T y)
		{
			if (x == null)
			{
				return (y == null) ? 0 : -1;
			}
			else
			{
				if (y == null)
				{
					return 1;
				}
				else
				{
					foreach (PropertyComparer<T> comparer in _comparers)
					{
						Int32 retval = comparer.Compare(x, y);

						if (retval != 0)
						{
							return retval;
						}
					}

					return 0;
				}
			}
		}

		#endregion

		#region Property Comparer

		private class PropertyComparer<TYPE> : IComparer<T>
		{
			private PropertyDescriptor _property;
			private ListSortDirection _direction;

			public PropertyComparer(PropertyDescriptor property, ListSortDirection direction)
			{
				_property = property;
				_direction = direction;
			}

			#region IComparer<T>

			public int Compare(T xWord, T yWord)
			{
				// Get property values
				Object xValue = GetPropertyValue(xWord, _property.Name);
				Object yValue = GetPropertyValue(yWord, _property.Name);

				// Determine sort order
				if (_direction == ListSortDirection.Ascending)
				{
					return CompareAscending(xValue, yValue);
				}
				else
				{
					return CompareDescending(xValue, yValue);
				}
			}

			public bool Equals(T xWord, T yWord)
			{
				return xWord.Equals(yWord);
			}

			public int GetHashCode(T obj)
			{
				return obj.GetHashCode();
			}

			#endregion

			private int CompareAscending(Object x, Object y)
			{
				Int32 result;

				if (x == null && y == null)
				{
					// Consider both nulls as the same
					result = 0;
				} 
				else if (x == null)
				{
					result = -1;
				}  
				else if (x is IComparable)
				{
					// Values implement IComparer
					result = ((IComparable) x).CompareTo(y);
				}
				else if (x.Equals(y))
				{
					// Values don't implement IComparer but are equivalent
					result = 0;
				}
				else
				{
					// Values don't implement IComparer and are not equivalent,
					// so compare as typed values
					result = ((IComparable) x).CompareTo(y);
				}

				return result;
			}

			private Int32 CompareDescending(Object x, Object y)
			{
				// Return result adjusted for ascending or descending sort order ie
				// multiplied by 1 for ascending or -1 for descending
				return -CompareAscending(x, y);
			}

			private object GetPropertyValue(T value, String property)
			{
				PropertyInfo propertyInfo = value.GetType().GetProperty(property);
				return propertyInfo.GetValue(value, null);
			}
		}

		#endregion

		#region Clone

		public ExtendedBindingList<T> Clone()
		{
			if (typeof(T).GetInterface("ICloneable") == null)
			{
				throw new ArgumentException("ExtendedBindingList<T>.Clone(), <T> does not implement ICloneable");
			}

			ExtendedBindingList<T> clone = new ExtendedBindingList<T>();

			foreach (ICloneable item in this)
			{
				clone.Add((T) item.Clone());
			}

			return clone;
		}

		#endregion

		#region AddMultiple

		public void AddMultiple(IEnumerable<T> list)
		{
			Type memberType = this.GetType().GetGenericArguments()[0];

			if (typeof(T) != memberType)
			{
				throw new ArgumentException("IEnumerables's of type <T> must have the same member type");
			}

			foreach (T t in list)
			{
				this.Add(t);
			}
		}

		#endregion
	}

	public class BindingListView<T> : IList<T>, IBindingList, IEnumerable<T>, ICancelAddNew, IBindingListView
	{
		#region Nested types

		private class ListItem
		{
			// The reference to the item is kept to manage the values cache
			private T _item;

			// Position in the original list
			private Int32 _baseIndex;

			// Contains the values on witch the sort is performed
			// these values are cached so on the next sort will be available.
			private HybridDictionary _itemValues = new HybridDictionary();

			public T Item
			{
				get { return _item; }
			}

			public Int32 BaseIndex
			{
				get { return _baseIndex; }
				set { _baseIndex = value; }
			}

			public ListItem(T item, Int32 baseIndex)
			{
				_item = item;
				_baseIndex = baseIndex;
			}

			// If at construction time we already know the sort properties,
			// we can cache at this moment.
			public ListItem(T item, Int32 baseIndex, PropertyDescriptorCollection properties)
			{
				_item = item;
				_baseIndex = baseIndex;
				CachePropertyValues(properties);
			}

			public Object GetPropertyValue(PropertyDescriptor propertyDesc)
			{
				Object value = _itemValues[propertyDesc];

				if (value == null)
				{
					// Retrieve the value
					value = propertyDesc.GetValue(_item);

					// Cache it for next times
					_itemValues[propertyDesc] = value;
				}

				return value;
			}

			public void CachePropertyValues(PropertyDescriptorCollection properties)
			{
				foreach (PropertyDescriptor propertyDesc in properties)
				{
					_itemValues[propertyDesc] = propertyDesc.GetValue(_item);
				}
			}
		}

		private class SortComparer : IComparer<ListItem>
		{
			// For complex sort
			private ListSortDescriptionCollection _sortCollection = null;

			// For simple sort
			private PropertyDescriptor _propertyDescriptor = null;
			private ListSortDirection _direction = ListSortDirection.Ascending;

			public SortComparer()
			{
			}

			// The simple case
			public SortComparer(PropertyDescriptor propDesc, ListSortDirection direction)
			{
				_propertyDescriptor = propDesc;
				_direction = direction;
			}

			// The complex case
			public SortComparer(ListSortDescriptionCollection sortCollection)
			{
				_sortCollection = sortCollection;
			}

			public void SetSortCriteria()
			{
				_propertyDescriptor = null;
				_direction = ListSortDirection.Ascending;
				_sortCollection = null;
			}

			public void SetSortCriteria(PropertyDescriptor propDesc, ListSortDirection direction)
			{
				_propertyDescriptor = propDesc;
				_direction = direction;
				_sortCollection = null;
			}

			public void SetSortCriteria(ListSortDescriptionCollection sortCollection)
			{
				_propertyDescriptor = null;
				_direction = ListSortDirection.Ascending;
				_sortCollection = sortCollection;
			}

			#region IComparer<ListItem> Members

			Int32 IComparer<ListItem>.Compare(ListItem x, ListItem y)
			{
				Int32 result;

				if (_propertyDescriptor != null)
				{
					// Simple sort 
					if (x == null && y == null)
					{
						result = 0;
					}
					else if(x == null && y != null)
					{
						return 1;
					}
					else if (x != null && y == null)
					{
						return -1;
					}
					else
					{
						Object xValue = x.GetPropertyValue(_propertyDescriptor);
						Object yValue = y.GetPropertyValue(_propertyDescriptor);

						result = CompareValues(xValue, yValue, _direction);
					}
				}
				else if (_sortCollection != null && _sortCollection.Count > 0)
				{
					// Complex sort
					result = MultiPropertyComparer(x, y);
				}
				else
				{
					// Sorting by original position
					result = x.BaseIndex.CompareTo(y.BaseIndex);
				}

				return result;
			}

			#endregion

			private Int32 CompareValues(Object xValue, Object yValue, ListSortDirection direction)
			{
				Int32 retValue = 0;
				Boolean done = false;

				IComparable xComp = xValue as IComparable;

				if (xComp != null)
				{
					// Can ask the x value
					retValue = xComp.CompareTo(yValue);
					done = true;
				}
				else
				{
					IComparable yComp = yValue as IComparable;

					if (yComp != null)
					{
						retValue = yComp.CompareTo(xValue) * -1;
						done = true;
					}
				}

				if (!done)
				{
					// Not comparable, compare String representations
					if (xValue != null && yValue != null)
					{
						retValue = xValue.ToString().CompareTo(yValue.ToString());
					}
				}

				if (direction == ListSortDirection.Descending)
				{
					retValue *= -1;
				}

				return retValue;
			}

			private Int32 MultiPropertyComparer(ListItem x, ListItem y)
			{
				Int32 retValue = 0;
				Int32 index = 0;

				while (index < _sortCollection.Count && retValue == 0)
				{
					ListSortDescription listSortDesc = _sortCollection[index];
					Object xValue = x.GetPropertyValue(listSortDesc.PropertyDescriptor);
					Object yValue = y.GetPropertyValue(listSortDesc.PropertyDescriptor);

					retValue = CompareValues(xValue, yValue, listSortDesc.SortDirection);

					index++;
				}

				return retValue;
			}
		}

		private class ViewEnumerator : IEnumerator<T>
		{
			private IList<T> _list;
			private List<ListItem> _viewIndex;
			private Int32 _index;

			public ViewEnumerator(IList<T> list, List<ListItem> viewIndex)
			{
				_list = list;
				_viewIndex = viewIndex;

				Reset();
			}

			public T Current
			{
				get { return _list[_viewIndex[_index].BaseIndex]; }
			}

			Object System.Collections.IEnumerator.Current
			{
				get { return _list[_viewIndex[_index].BaseIndex]; }
			}

			public Boolean MoveNext()
			{
				Boolean moved = false;

				if (_index < _viewIndex.Count - 1)
				{
					_index++;
					moved = true;
				}

				return moved;
			}

			public void Reset()
			{
				_index = -1;
			}

			#region IDisposable Support

			// To detect redundant calls
			private Boolean _disposedValue = false; 

			// IDisposable
			protected virtual void Dispose(bool disposing)
			{
				if (!_disposedValue)
				{
					if (disposing)
					{
						// Free unmanaged resources when explicitly called
					}

					// Free shared unmanaged resources
				}

				_disposedValue = true;
			}

			// This code added to correctly implement the disposable pattern.
			public void Dispose()
			{
				// Do not change this code.  Put cleanup code in Dispose(bool disposing) above.
				Dispose(true);
				GC.SuppressFinalize(this);
			}

			~ViewEnumerator()
			{
				Dispose(false);
			}

			#endregion
		}

		#endregion

		private IList<T> _list;
		private Boolean _supportsBinding;
		private IBindingList _bindingList;

		private Boolean _sorted = false;
		private ListSortDirection _sortDirection = ListSortDirection.Ascending;
		private PropertyDescriptor _sortProperty = null;
		private ListSortDescriptionCollection _sortDescriptions = null;

		private Boolean _filtered = false;
		private String _filterString = String.Empty;
		private IFilterEngine<T> _filterEngine = null;

		// Used to perform pseudo deletes where a suitable flag exists
		private String _isDeletedPropertyName = String.Empty;

		public String IsDeletedPropertyName
		{
			get 
			{ 
				return _isDeletedPropertyName; 
			}
			set 
			{ 
				_isDeletedPropertyName = value;
				
				// Check the property name passed is valid, and is a Boolean
				Type memberType = typeof(T);
				PropertyInfo propertyInfo = memberType.GetProperty(_isDeletedPropertyName);

				if (propertyInfo == null || propertyInfo.PropertyType != typeof(Boolean))
				{
					throw new ArgumentException("BindingListView: IsDeletedPropetyName, must refer to a property of type Boolean from the Member object");
				}

				// Use the existing filter and add in the IsDeletedProperty condition
				this.Filter = _filterString + " ";
			}
		}

		// View on data
		private List<ListItem> _viewIndex = new List<ListItem>();

		// Used to sort the view
		private SortComparer _sortComparer = new SortComparer();

		public String Filter
		{
			get 
			{ 
				return _filterString; 
			}

			set
			{
				if (_filterString != value)
				{
					if (IsDeletedPropertyName != "")
					{
						_filterString = value.Trim();

						if (!_filterString.Contains(IsDeletedPropertyName + " = false"))
						{
							_filterString = (_filterString.Length > 0 ? _filterString + " and " : "") + IsDeletedPropertyName + " = false";
						}
					}
					else
					{
						_filterString = value.Trim();
					}

					_filterEngine = null;
					UpdateView();
				}
			}
		}

		// This can be used to sort a view programmatically, 
		// how we do with DataView.Sort
		public String Sort
		{
			get
			{
				// Obtaining a string representation of the sort criteria
				String sort = String.Empty;

				if (_sortProperty != null)
				{
					sort = GetSortString(_sortProperty, _sortDirection);
				}
				else if (_sortDescriptions != null && _sortDescriptions.Count > 0)
				{
					StringBuilder sb = new StringBuilder();

					foreach (ListSortDescription desc in _sortDescriptions)
					{
						if (sb.Length > 0)
						{
							sb.Append(", ");
						}

						sb.Append(GetSortString(desc.PropertyDescriptor, desc.SortDirection));
					}

					sort = sb.ToString();
				}

				return sort;
			}

			set
			{
				// Parsing the string to obtain the sort criteria
				if (String.IsNullOrEmpty(value))
				{
					UndoSort();
				}
				else
				{

					String[] sortParts = value.Split(new char[] { ',' });

					if (sortParts.Length == 1)
					{
						ListSortDescription desc = GetSortDescription(sortParts[0].Trim());
						ApplySort(desc.PropertyDescriptor, desc.SortDirection);
					}
					else
					{
						ListSortDescription[] descs = new ListSortDescription[sortParts.Length];

						for (Int32 i = 0; i < sortParts.Length; i++)
						{
							descs[i] = GetSortDescription(sortParts[i].Trim());
						}

						ListSortDescriptionCollection sorts = new ListSortDescriptionCollection(descs);
						ApplySort(sorts);
					}
				}
			}
		}

		private String GetSortString(PropertyDescriptor property, ListSortDirection sortDirection)
		{
			String name = property.Name;
			String direction = sortDirection == ListSortDirection.Ascending ? "ASC" : "DESC";

			return String.Format("{0} {1}", name, direction);
		}

		private ListSortDescription GetSortDescription(String input)
		{
			String propertyName;
			ListSortDirection direction = ListSortDirection.Ascending;

			RegexOptions options = RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture;
			Regex regex = new Regex(@"^((\[(?<Property>.+)\])|(?<Property>\S+))([ ]+(?<Direction>ASC|DESC))?$", options);

			// Get match
			Match match = regex.Match(input);

			if (match != null && match.Success)
			{
				propertyName = match.Groups["Property"].Value;
				Group group = match.Groups["Direction"];

				if (group != null && group.Success)
				{
					String dir = group.Value.ToUpper();
					direction = dir == "ASC" ? ListSortDirection.Ascending : ListSortDirection.Descending;
				}
			}
			else
			{
				throw new ArgumentException("Invalid sort expression");
			}

			PropertyDescriptor property = GetPropertyDescriptor(propertyName);

			if (property == null)
			{
				throw new ArgumentException("The property '" + propertyName + "' is not a property of the list item type.");
			}

			return new ListSortDescription(property, direction);
		}

		#region IList<T> Members

		public int IndexOf(T item)
		{
			return GetViewPosition(_list.IndexOf(item));
		}

		public void Insert(int index, T item)
		{
			_list.Insert(index, item);
		}

		/// <summary>
		/// Removes an item from the list
		/// </summary>
		/// <param name="index">Index of item to be removed.</param>
		public void RemoveAt(int index)
		{
			if (String.IsNullOrEmpty(_isDeletedPropertyName))
			{
				_list.RemoveAt(GetOriginalPosition(index));
			}
			else
			{
				MarkForDeletion(GetOriginalPosition(index));
			}
		}

		private void MarkForDeletion(int index)
		{
			PropertyInfo isDeletedProperty = _list[index].GetType().GetProperty(_isDeletedPropertyName);
			isDeletedProperty.SetValue(_list[index], true, null);
		}

		public T this[int index]
		{
			get
			{
				ListItem listItem = _viewIndex[index];
				return listItem.Item;
			}
			set
			{
				_list[GetOriginalPosition(index)] = value;
			}
		}

		#endregion

		#region ICollection<T> Members

		public void Add(T item)
		{
			_list.Add(item);
		}

		public void Clear()
		{
			_list.Clear();
		}

		public bool Contains(T item)
		{
			return _list.Contains(item);
		}

		/// <summary>
		/// Copies the contents of the list to an array
		/// </summary>
		/// <param name="array">Array to receive the data</param>
		/// <param name="arrayIndex">Starting array index</param>
		public void CopyTo(T[] array, int arrayIndex)
		{
			_list.CopyTo(array, arrayIndex);
		}

		public int Count
		{
			get { return _viewIndex.Count; }
		}

		public bool IsReadOnly
		{
			get { return _list.IsReadOnly; }
		}

		public bool Remove(T item)
		{
			bool isDeleted;

			if (String.IsNullOrEmpty(_isDeletedPropertyName))
			{
				isDeleted = _list.Remove(item);
			} 
			else
			{
				int index = _list.IndexOf(item);

				if (index >= 0)
				{
					MarkForDeletion(index);
				}

				isDeleted = (index >= 0);
			}

			return isDeleted;
		}

		#endregion

		#region IEnumerable<T> Members

		public IEnumerator<T> GetEnumerator()
		{
			return new ViewEnumerator(_list, _viewIndex);
		}

		#endregion

		#region IEnumerable Members

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		#endregion

		#region IBindingList Members

		/// <summary>
		/// Implemented by IList source object
		/// </summary>
		/// <param name="property">Property on which to build the index</param>
		public void AddIndex(PropertyDescriptor property)
		{
			if (_supportsBinding)
			{
				_bindingList.AddIndex(property);
			}
		}

		/// <summary>
		/// Implemented by IList source object
		/// </summary>
		public object AddNew()
		{
			T result;

			if (_supportsBinding)
			{
				result = (T) _bindingList.AddNew();
			}
			else
			{
				result = default(T);
			}

			return result;
		}

		/// <summary>
		/// Implemented by IList source object
		/// </summary>
		public bool AllowEdit
		{
			get
			{
				if (_supportsBinding)
				{
					return _bindingList.AllowEdit;
				}
				else
				{
					return false;
				}
			}
		}

		/// <summary>
		/// Implemented by IList source object.
		/// </summary>
		public bool AllowNew
		{
			get
			{
				if (_supportsBinding)
				{
					return _bindingList.AllowNew;
				}
				else
				{
					return false;
				}
			}
		}

		/// <summary>
		/// Implemented by IList source object.
		/// </summary>
		public bool AllowRemove
		{
			get
			{
				if (_supportsBinding)
				{
					return _bindingList.AllowRemove;
				}
				else
				{
					return false;
				}
			}
		}

		/// <summary>
		/// Applies a sort to the view
		/// </summary>
		/// <param name="property">A PropertyDescriptor for the property on which to sort</param>
		/// <param name="direction">The direction to sort the data</param>
		public void ApplySort(PropertyDescriptor property, ListSortDirection direction)
		{
			_sortProperty = property;
			_sortDirection = direction;
			_sortDescriptions = null;

			SortView(true);
		}

		/// <summary>
		/// Finds an item in the view
		/// </summary>
		/// <param name="propertyName">Name of the property to search</param>
		/// <param name="key">Value to find</param>
		public int Find(string propertyName, object key)
		{
			PropertyDescriptor findProperty = GetPropertyDescriptor(propertyName);
			Int32 result = -1;

			if (findProperty != null)
			{
				result = Find(findProperty, key);
			}

			return result;
		}

		/// <summary>
		/// Implemented by IList source object.
		/// </summary>
		/// <param name="key">Key value for which to search.</param>
		/// <param name="property">Property to search for the key
		/// value.</param>
		public int Find(PropertyDescriptor property, Object key)
		{
			// Simple iteration
			for (Int32 i = 0; i < Count; i++)
			{
				T item = this[i];

				if (property.GetValue(item).Equals(key))
				{
					return i;
				}
			}

			// Not found
			return -1;
		}

		/// <summary>
		/// Gets a value indicating whether the view is currently sorted
		/// </summary>
		public bool IsSorted
		{
			get { return _sorted; }
		}

		/// <summary>
		/// Raised to indicate that the list's data has changed.
		/// </summary>
		/// <remarks>
		/// This event is raised if the underling IList object's data changes
		/// (assuming the underling IList also implements the IBindingList
		/// interface). It is also raised if the filter or sort
		/// is changed to indicate that the view's data has changed
		/// </remarks>
		public event ListChangedEventHandler ListChanged;

		/// <summary>
		/// Raises the ListChanged event
		/// </summary>
		/// <param name="e">Parameter for the event</param>
		protected virtual void OnListChanged(ListChangedEventArgs e)
		{
			ListChangedEventHandler temp = ListChanged;

			if (temp != null)
			{
				ListChanged(this, e);
			}
		}

		/// <summary>
		/// Implemented by IList source object.
		/// </summary>
		/// <param name="property">Property for which the index should be removed.</param>
		public void RemoveIndex(PropertyDescriptor property)
		{
			if (_supportsBinding)
			{
				_bindingList.RemoveIndex(property);
			}
		}

		/// <summary>
		/// Removes any sort currently applied to the view
		/// </summary>
		public void RemoveSort()
		{
			UndoSort();
		}

		/// <summary>
		/// Returns the direction of the current sort
		/// </summary>
		public ListSortDirection SortDirection
		{
			get { return _sortDirection; }
		}

		/// <summary>
		/// Returns the PropertyDescriptor of the current sort
		/// </summary>
		public PropertyDescriptor SortProperty
		{
			get { return _sortProperty; }
		}

		/// <summary>
		/// Returns <see langword="true"/> since this object does raise the
		/// ListChanged event
		/// </summary>
		public bool SupportsChangeNotification
		{
			get { return true; }
		}

		public bool SupportsSearching
		{
			get { return true; }
		}

		public bool SupportsSorting
		{
			get { return true; }
		}

		#endregion

		#region IList Members

		public int Add(object value)
		{
			Add((T) value);

			Int32 index = GetViewPosition(_list.Count - 1);

			if (index > -1)
			{
				return index;
			}
			else
			{
				return 0;
			}
		}

		public bool Contains(object value)
		{
			return Contains((T) value);
		}

		public int IndexOf(object value)
		{
			return IndexOf((T) value);
		}

		public void Insert(int index, object value)
		{
			Insert(index, (T) value);
		}

		public bool IsFixedSize
		{
			get { return false; }
		}

		public void Remove(object value)
		{
			Remove((T) value);
		}

		object IList.this[int index]
		{
			get
			{
				return this[index];
			}
			set
			{
				this[index] = (T) value;
			}
		}

		#endregion

		#region ICollection Members

		public void CopyTo(Array array, int index)
		{
			CopyTo((T[]) array, index);
		}

		public bool IsSynchronized
		{
			get { return false; }
		}

		public object SyncRoot
		{
			get { return _list; }
		}

		#endregion

		#region ICancelAddNew Members

		public void CancelNew(int itemIndex)
		{
			ICancelAddNew can = _list as ICancelAddNew;

			if (can != null)
			{
				can.CancelNew(itemIndex);
			}
			else
			{
				_list.RemoveAt(itemIndex);
			}
		}

		public void EndNew(int itemIndex)
		{
			ICancelAddNew can = _list as ICancelAddNew;

			if (can != null)
			{
				can.EndNew(itemIndex);
			}
		}

		#endregion

		#region IBindingListView Members

		public void ApplySort(ListSortDescriptionCollection sorts)
		{
			_sortDescriptions = sorts;
			_sortDirection = ListSortDirection.Ascending;
			_sortProperty = null;

			SortView(true);
		}

		public void RemoveFilter()
		{
			Filter = string.Empty;
		}

		public ListSortDescriptionCollection SortDescriptions
		{
			get { return _sortDescriptions; }
		}

		public bool SupportsAdvancedSorting
		{
			get { return true; }
		}

		public bool SupportsFiltering
		{
			get { return true; }
		}

		#endregion

		/// <summary>
		/// Creates a new view based on the provided IList Object
		/// </summary>
		/// <param name="list">The IList (collection) containing the data</param>
		public BindingListView(IList<T> list)
		{
			List = list;
		}

		/// <summary>
		/// Creates a new view based on the provided ExtendedBindingList Object
		/// </summary>
		/// <param name="list">The ExtendedBindingList (collection) containing the data</param>
		public BindingListView(ExtendedBindingList<T> list)
		{
			List = (IList<T>) list;
		}

		/// <summary>
		/// Creates a new view based on the provided IList Object
		/// </summary>
		/// <param name="list">The IList (collection) containing the data</param>
		/// <param name="filterEngine">
		/// Filter engine instance Object
		/// </param>
		public BindingListView(IList<T> list, IFilterEngine<T> filterEngine)
		{
			FilterEngine = filterEngine;
			List = list;
		}

		public IList<T> List
		{
			get { return _list; }

			set
			{
				if (_bindingList != null)
				{
					_bindingList.ListChanged -= new ListChangedEventHandler(SourceChanged);
					_bindingList = null;
					_supportsBinding = false;
				}

				_list = value;

				if (_list is IBindingList)
				{
					_supportsBinding = true;
					_bindingList = (IBindingList) _list;
					_bindingList.ListChanged += new ListChangedEventHandler(SourceChanged);
				}

				UpdateView();
			}
		}

		public IFilterEngine<T> FilterEngine
		{
			get
			{
				if (_filterEngine == null)
				{
					_filterEngine = new DefaultFilterEngine<T>(_filterString);
				}

				return _filterEngine;
			}

			set
			{
				_filterEngine = value;

				if (_filterEngine == null)
				{
					_filterEngine = new DefaultFilterEngine<T>();
				}

				_filterEngine.FilterString = _filterString;
			}
		}

		/// <summary>
		/// Returns True if the view is currently filtered.
		/// </summary>
		public Boolean IsFiltered
		{
			get { return _filtered; }
		}

		private PropertyDescriptorCollection GetSortProperties()
		{
			List<PropertyDescriptor> list = new List<PropertyDescriptor>();

			if (_sortProperty != null)
			{
				list.Add(_sortProperty);
			}
			else if (_sortDescriptions != null)
			{
				foreach (ListSortDescription sortDesc in _sortDescriptions)
				{
					list.Add(sortDesc.PropertyDescriptor);
				}
			}

			PropertyDescriptorCollection sortProperties = new PropertyDescriptorCollection(list.ToArray());
			return sortProperties;
		}

		private void SourceChanged(Object sender, ListChangedEventArgs e)
		{
			Int32 origPos = -1;
			Int32 viewPos = -1;
			T newItem;

			PropertyDescriptorCollection sortProperties = GetSortProperties();

			switch (e.ListChangedType)
			{
				// when an item is added, we do not reapply the sort or the filter
				// and the item is simply added to the last postition
				case ListChangedType.ItemAdded:
					origPos = e.NewIndex;

					// Add new item to view
					newItem = _list[origPos];
					_viewIndex.Add(new ListItem(newItem, origPos, sortProperties));
					viewPos = _viewIndex.Count - 1;

					// Raise event
					OnListChanged(new ListChangedEventArgs(e.ListChangedType, viewPos));
					break;

				case ListChangedType.ItemChanged:
					origPos = e.OldIndex;

					// Update view item
					viewPos = GetViewPosition(origPos);

					if (viewPos != -1)
					{
						newItem = _list[origPos];
						_viewIndex[viewPos] = new ListItem(newItem, origPos, sortProperties);
						OnListChanged(new ListChangedEventArgs(e.ListChangedType, viewPos));
					}

					break;

				case ListChangedType.ItemDeleted:
					origPos = e.NewIndex;

					// Delete corresponding item from index if (any)
					viewPos = GetViewPosition(origPos);

					if (viewPos != -1)
					{
						_viewIndex.RemoveAt(viewPos);
					}

					// Adjust index xref values
					foreach (ListItem item in _viewIndex)
					{
						if (item.BaseIndex > e.NewIndex)
						{
							item.BaseIndex--;
						}
					}

					// Raise event if appropriate
					if (viewPos > -1)
					{
						OnListChanged(new ListChangedEventArgs(e.ListChangedType, viewPos));
					}

					break;
			}

			UpdateView();
		}

		private Int32 GetOriginalPosition(Int32 viewPosition)
		{
			return _viewIndex[viewPosition].BaseIndex;
		}

		private Int32 GetViewPosition(Int32 originaPosition)
		{
			Int32 result = -1;

			for (Int32 i = 0; i < _viewIndex.Count; i++)
			{
				if (_viewIndex[i].BaseIndex == originaPosition)
				{
					result = i;
					break;
				}
			}

			return result;
		}

		#region Filtering / Sorting view

		private void UpdateView()
		{
			int index = 0;
			_viewIndex.Clear();

			PropertyDescriptorCollection sortProperties = GetSortProperties();
			IFilterEngine<T> filterEngine = FilterEngine;

			if (_list != null)
			{
				if (String.IsNullOrEmpty(_filterString))
				{
					foreach (T item in _list)
					{
						_viewIndex.Add(new ListItem(item, index, sortProperties));
						index++;
					}

					_filtered = false;
				}
				else
				{
					foreach (T item in _list)
					{
						if (filterEngine.Evaluate(item))
						{
							_viewIndex.Add(new ListItem(item, index, sortProperties));
						}

						index++;
					}

					_filtered = true;
				}
			}

			if (_sorted)
			{
				SortView(false);
			}

			OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, 0));
		}

		private void SortView(bool raiseListChanged)
		{
			if (_sortProperty != null)
			{
				_sortComparer.SetSortCriteria(_sortProperty, _sortDirection);
				_sorted = true;
			}
			else if (_sortDescriptions != null && _sortDescriptions.Count > 0)
			{
				_sortComparer.SetSortCriteria(_sortDescriptions);
				_sorted = true;
			}
			else
			{
				_sortComparer.SetSortCriteria();
				_sorted = false;
			}

			_viewIndex.Sort(_sortComparer);

			if (raiseListChanged)
			{
				OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, 0));
			}
		}

		private void UndoSort()
		{
			_sortProperty = null;
			_sortDirection = ListSortDirection.Ascending;
			_sortDescriptions = null;

			SortView(true);
		}

		private PropertyDescriptor GetPropertyDescriptor(string propertyName)
		{
			PropertyDescriptor desc = null;

			if (!String.IsNullOrEmpty(propertyName))
			{
				Type itemType = typeof(T);

				foreach (PropertyDescriptor prop in TypeDescriptor.GetProperties(itemType))
				{
					if (prop.Name == propertyName)
					{
						desc = prop;
						break;
					}
				}
			}

			return desc;
		}

		#endregion
	}

	public interface IFilterEngine<T>
	{
		String FilterString
		{
			get;
			set;
		}

		Boolean Evaluate(Object item);
	}

	internal class DefaultFilterEngine<T> : IFilterEngine<T>
	{
		private String _filterString;
		private List<Predicate<Object>> _predicates = new List<Predicate<Object>>();

		#region IFilterEngine Members

		public string FilterString
		{
			get { return _filterString; }

			set
			{
				if (_filterString != value)
				{
					_filterString = value;
					BuildPredicates();
				}
			}
		}

		public bool Evaluate(object item)
		{
			foreach (Predicate<object> predicate in _predicates)
			{
				if (!predicate(item))
				{
					return false;
				}
			}

			return true;
		}

		#endregion


		public DefaultFilterEngine()
		{
		}

		public DefaultFilterEngine(String filterString)
		{
			FilterString = filterString;
		}

		private void BuildPredicates()
		{
			_predicates.Clear();

			if (!String.IsNullOrEmpty(_filterString))
			{
				Regex regex = new Regex(@" AND ", RegexOptions.IgnoreCase);
				String[] parts = regex.Split(_filterString);

				foreach (String part in parts)
				{
					Predicate<Object> predicate = GetPredicate(part);
					_predicates.Add(predicate);
				}
			}
		}

		private Predicate<Object> GetPredicate(String expression)
		{
			Predicate<Object> predicate = null;
			String[] filterParts = expression.Split(new String[] { "=", "<=", ">=", "!=", "<>" }, StringSplitOptions.None);

			if (filterParts.Length != 2 || filterParts[0] == "" || filterParts[1] == "")
			{
				throw new ArgumentException("Filter string must be of the form 'propertyName=value'.", "Filter");
			}

			// Trim whitespace from property name and value
			String propertyName = filterParts[0].Trim();
			String propertyValue = filterParts[1].Trim();

			// Trim square brackets
			if (!String.IsNullOrEmpty(propertyName))
			{
				if (propertyName[0] == '[')
				{
					if (propertyName[propertyName.Length - 1] != ']')
					{
						throw new ArgumentException("Unbalanced brackets in property name.", "Filter");
					}

					propertyName = propertyName.Trim(new char[] { '[', ']' });
				}
			}

			// Get a property descriptor for the filter property
			PropertyDescriptor property = TypeDescriptor.GetProperties(typeof(T))[propertyName];

			if (property == null)
			{
				throw new ArgumentException("The property '" + propertyName + "' is not a property of the list item type.", "Filter");
			}

			if (property.PropertyType == typeof(String))
			{
				// Check for regular expression
				String exp = GetRegularExpression(propertyValue);

				if (!String.IsNullOrEmpty(exp))
				{
					propertyValue = exp;
					StringComparerPredicate pred = new StringComparerPredicate(property, propertyValue, true);
					predicate = new Predicate<Object>(pred.Matches);

					// Done
					return predicate;
				}
			}

			// Trim quoted values
			if (propertyValue[0] == '\'')
			{
				if (propertyValue[propertyValue.Length - 1] != '\'')
				{
					throw new ArgumentException("Unbalanced quotes in property value.", "Filter");
				}

				propertyValue = propertyValue.Trim(new char[] { '\'' });
			}
			else if (propertyValue[0] == '"')
			{
				if (propertyValue[propertyValue.Length - 1] != '"')
				{
					throw new ArgumentException("Unbalanced quotes in property value.", "Filter");
				}

				propertyValue = propertyValue.Trim(new char[] { '"' });
			}

			if (String.Compare(propertyValue, "null", true) == 0)
			{
				propertyValue = null;
			}

			Type propertyType = property.PropertyType;

			if (propertyType == typeof(SByte))
			{
				SByteComparerPredicate pred = new SByteComparerPredicate(property, propertyValue);
				predicate = new Predicate<Object>(pred.Matches);
			}
			else if (propertyType == typeof(Byte))
			{
				ByteComparerPredicate pred = new ByteComparerPredicate(property, propertyValue);
				predicate = new Predicate<Object>(pred.Matches);
			}
			else if (propertyType == typeof(Int16))
			{
				Int16ComparerPredicate pred = new Int16ComparerPredicate(property, propertyValue);
				predicate = new Predicate<Object>(pred.Matches);
			}
			else if (propertyType == typeof(UInt16))
			{
				UInt16ComparerPredicate pred = new UInt16ComparerPredicate(property, propertyValue);
				predicate = new Predicate<Object>(pred.Matches);
			}
			else if (propertyType == typeof(Int32))
			{
				Int32ComparerPredicate pred = new Int32ComparerPredicate(property, propertyValue);
				predicate = new Predicate<Object>(pred.Matches);
			}
			else if (propertyType == typeof(UInt32))
			{
				UInt32ComparerPredicate pred = new UInt32ComparerPredicate(property, propertyValue);
				predicate = new Predicate<Object>(pred.Matches);
			}
			else if (propertyType == typeof(Int64))
			{
				Int64ComparerPredicate pred = new Int64ComparerPredicate(property, propertyValue);
				predicate = new Predicate<Object>(pred.Matches);
			}
			else if (propertyType == typeof(UInt64))
			{
				UInt64ComparerPredicate pred = new UInt64ComparerPredicate(property, propertyValue);
				predicate = new Predicate<Object>(pred.Matches);
			}
			else if (propertyType == typeof(Char))
			{
				CharComparerPredicate pred = new CharComparerPredicate(property, propertyValue);
				predicate = new Predicate<Object>(pred.Matches);
			}
			else if (propertyType == typeof(Single))
			{
				SingleComparerPredicate pred = new SingleComparerPredicate(property, propertyValue);
				predicate = new Predicate<Object>(pred.Matches);
			}
			else if (propertyType == typeof(Double))
			{
				DoubleComparerPredicate pred = new DoubleComparerPredicate(property, propertyValue);
				predicate = new Predicate<Object>(pred.Matches);
			}
			else if (propertyType == typeof(Boolean))
			{
				BooleaneanComparerPredicate pred = new BooleaneanComparerPredicate(property, propertyValue);
				predicate = new Predicate<Object>(pred.Matches);
			}
			else if (propertyType == typeof(Decimal))
			{
				DecimalComparerPredicate pred = new DecimalComparerPredicate(property, propertyValue);
				predicate = new Predicate<Object>(pred.Matches);
			}
			else if (property.PropertyType == typeof(String))
			{
				StringComparerPredicate pred = new StringComparerPredicate(property, propertyValue, false);
				predicate = new Predicate<Object>(pred.Matches);
			}
			else
			{
				PropertyComparerPredicate pred = new PropertyComparerPredicate(property, propertyValue);
				predicate = new Predicate<Object>(pred.Matches);
			}

			return predicate;
		}

		private String GetRegularExpression(String input)
		{
			String exp = String.Empty;
			Regex regex = new Regex(@"regex\s*\(\s*\""([^""]*)\""\s*\)", RegexOptions.IgnoreCase);

			// Check for match
			Boolean isMatch = regex.IsMatch(input);

			if (isMatch)
			{
				// Get match
				Match match = regex.Match(input);
	
				if (match != null && match.Groups.Count > 1)
				{
					Group group = match.Groups[1];
					exp = group.Value;
				}
			}

			return exp;
		}
	}

	internal class PropertyComparerPredicate
	{
		private Object _value;
		private PropertyDescriptor _property;

		public PropertyComparerPredicate(PropertyDescriptor property, String value)
		{
			if (property == null)
			{
				throw new ArgumentNullException("property");
			}

			this._property = property;

			if (value == null)
			{
				this._value = null;
			}
			else
			{
				this._value = property.Converter.ConvertFromString(value);
			}
		}

		public Boolean Matches(Object item)
		{
			if (item == null)
			{
				throw new ArgumentNullException("item");
			}

			if (_property.ComponentType != item.GetType())
			{
				throw new ArgumentException("item");
			}

			Object testValue = _property.GetValue(item);
			Boolean result = false;

			if (_value == null)
			{
				result = (testValue == null);
			}
			else
			{
				result = (((IComparable) _value).CompareTo(testValue) == 0);
			}

			return result;
		}
	}

	internal class SByteComparerPredicate
	{
		private SByte _value;
		private PropertyDescriptor _property;

		public SByteComparerPredicate(PropertyDescriptor property, String value)
		{
			if (property == null)
			{
				throw new ArgumentNullException("property");
			}

			if (property.PropertyType != typeof(sbyte))
			{
				throw new ArgumentException("property");
			}

			if (String.IsNullOrEmpty(value))
			{
				throw new ArgumentNullException("value");
			}

			_property = property;
			_value = SByte.Parse(value);
		}

		public Boolean Matches(Object item)
		{
			if (item == null)
			{
				throw new ArgumentNullException("item");
			}

			if (_property.ComponentType != item.GetType())
			{
				throw new ArgumentException("item");
			}

			SByte testValue = (SByte) _property.GetValue(item);
			return _value.CompareTo(testValue) == 0;
		}
	}

	internal class ByteComparerPredicate
	{
		private Byte _value;
		private PropertyDescriptor _property;

		public ByteComparerPredicate(PropertyDescriptor property, String value)
		{
			if (property == null)
			{
				throw new ArgumentNullException("property");
			}

			if (property.PropertyType != typeof(Byte))
			{
				throw new ArgumentException("property");
			}

			if (String.IsNullOrEmpty(value))
			{
				throw new ArgumentNullException("value");
			}

			_property = property;
			_value = Byte.Parse(value);
		}

		public Boolean Matches(Object item)
		{
			if (item == null)
			{
				throw new ArgumentNullException("item");
			}


			if (_property.ComponentType != item.GetType())
			{
				throw new ArgumentException("item");
			}

			Byte testValue = (Byte) _property.GetValue(item);
			return _value.CompareTo(testValue) == 0;
		}
	}

	internal class Int16ComparerPredicate
	{
		private Int16 _value;
		private PropertyDescriptor _property;

		public Int16ComparerPredicate(PropertyDescriptor property, String value)
		{
			if (property == null)
			{
				throw new ArgumentNullException("property");
			}

			if (property.PropertyType != typeof(short))
			{
				throw new ArgumentException("property");
			}

			if (String.IsNullOrEmpty(value))
			{
				throw new ArgumentNullException("value");
			}

			_property = property;
			_value = Int16.Parse(value);
		}

		public Boolean Matches(Object item)
		{
			if (item == null)
			{
				throw new ArgumentNullException("item");
			}

			if (_property.ComponentType != item.GetType())
			{
				throw new ArgumentException("item");
			}

			Int16 testValue = (Int16) _property.GetValue(item);
			return _value.CompareTo(testValue) == 0;
		}
	}

	internal class UInt16ComparerPredicate
	{
		private UInt16 _value;
		private PropertyDescriptor _property;

		public UInt16ComparerPredicate(PropertyDescriptor property, String value)
		{
			if (property == null)
			{
				throw new ArgumentNullException("property");
			}

			if (property.PropertyType != typeof(UInt16))
			{
				throw new ArgumentException("property");
			}

			if (String.IsNullOrEmpty(value))
			{
				throw new ArgumentNullException("value");
			}

			_property = property;
			_value = UInt16.Parse(value);
		}

		public Boolean Matches(Object item)
		{
			if (item == null)
			{
				throw new ArgumentNullException("item");
			}

			if (_property.ComponentType != item.GetType())
			{
				throw new ArgumentException("item");
			}

			UInt16 testValue = (UInt16) _property.GetValue(item);
			return _value.CompareTo(testValue) == 0;
		}
	}

	internal class Int32ComparerPredicate
	{
		private Int32 _value;
		private PropertyDescriptor _property;

		public Int32ComparerPredicate(PropertyDescriptor property, String value)
		{
			if (property == null)
			{
				throw new ArgumentNullException("property");
			}

			if (property.PropertyType != typeof(Int32))
			{
				throw new ArgumentException("property");
			}

			if (String.IsNullOrEmpty(value))
			{
				throw new ArgumentNullException("value");
			}

			_property = property;
			_value = Int32.Parse(value);
		}

		public Boolean Matches(Object item)
		{
			if (item == null)
			{
				throw new ArgumentNullException("item");
			}

			if (_property.ComponentType != item.GetType())
			{
				throw new ArgumentException("item");
			}

			Int32 testValue = (Int32) _property.GetValue(item);
			return _value.CompareTo(testValue) == 0;
		}
	}

	internal class UInt32ComparerPredicate
	{
		private UInt32 _value;
		private PropertyDescriptor _property;

		public UInt32ComparerPredicate(PropertyDescriptor property, String value)
		{
			if (property == null)
			{
				throw new ArgumentNullException("property");
			}

			if (property.PropertyType != typeof(UInt32))
			{
				throw new ArgumentException("property");
			}

			if (String.IsNullOrEmpty(value))
			{
				throw new ArgumentNullException("value");
			}

			_property = property;
			_value = UInt32.Parse(value);
		}

		public Boolean Matches(Object item)
		{
			if (item == null)
			{
				throw new ArgumentNullException("item");
			}

			if (_property.ComponentType != item.GetType())
			{
				throw new ArgumentException("item");
			}

			UInt32 testValue = (UInt32) _property.GetValue(item);
			return _value.CompareTo(testValue) == 0;
		}
	}

	internal class Int64ComparerPredicate
	{
		private long _value;
		private PropertyDescriptor _property;

		public Int64ComparerPredicate(PropertyDescriptor property, String value)
		{
			if (property == null)
			{
				throw new ArgumentNullException("property");
			}

			if (property.PropertyType != typeof(long))
			{
				throw new ArgumentException("property");
			}

			if (String.IsNullOrEmpty(value))
			{
				throw new ArgumentNullException("value");
			}

			_property = property;
			_value = long.Parse(value);
		}

		public Boolean Matches(Object item)
		{
			if (item == null)
			{
				throw new ArgumentNullException("item");
			}

			if (_property.ComponentType != item.GetType())
			{
				throw new ArgumentException("item");
			}

			Int64 testValue = (Int64) _property.GetValue(item);
			return _value.CompareTo(testValue) == 0;
		}
	}

	internal class UInt64ComparerPredicate
	{
		private UInt64 _value;
		private PropertyDescriptor _property;

		public UInt64ComparerPredicate(PropertyDescriptor property, String value)
		{
			if (property == null)
			{
				throw new ArgumentNullException("property");
			}

			if (property.PropertyType != typeof(ulong))
			{
				throw new ArgumentException("property");
			}

			if (String.IsNullOrEmpty(value))
			{
				throw new ArgumentNullException("value");
			}

			_property = property;
			_value = UInt64.Parse(value);
		}

		public Boolean Matches(Object item)
		{
			if (item == null)
			{
				throw new ArgumentNullException("item");
			}

			if (_property.ComponentType != item.GetType())
			{
				throw new ArgumentException("item");
			}

			UInt64 testValue = (UInt64) _property.GetValue(item);
			return _value.CompareTo(testValue) == 0;
		}
	}

	internal class CharComparerPredicate
	{
		private Char _value;
		private PropertyDescriptor _property;

		public CharComparerPredicate(PropertyDescriptor property, String value)
		{
			if (property == null)
			{
				throw new ArgumentNullException("property");
			}

			if (property.PropertyType != typeof(Char))
			{
				throw new ArgumentException("property");
			}

			if (String.IsNullOrEmpty(value))
			{
				throw new ArgumentNullException("value");
			}

			_property = property;
			_value = Char.Parse(value);
		}

		public Boolean Matches(Object item)
		{
			if (item == null)
			{
				throw new ArgumentNullException("item");
			}

			if (_property.ComponentType != item.GetType())
			{
				throw new ArgumentException("item");
			}

			Char testValue = (Char) _property.GetValue(item);
			return _value.CompareTo(testValue) == 0;
		}
	}

	internal class SingleComparerPredicate
	{
		private float _value;
		private PropertyDescriptor _property;

		public SingleComparerPredicate(PropertyDescriptor property, String value)
		{
			if (property == null)
			{
				throw new ArgumentNullException("property");
			}

			if (property.PropertyType != typeof(float))
			{
				throw new ArgumentException("property");
			}

			if (String.IsNullOrEmpty(value))
			{
				throw new ArgumentNullException("value");
			}

			_property = property;
			_value = float.Parse(value);
		}

		public Boolean Matches(Object item)
		{
			if (item == null)
			{
				throw new ArgumentNullException("item");
			}

			if (_property.ComponentType != item.GetType())
			{
				throw new ArgumentException("item");
			}

			float testValue = (float) _property.GetValue(item);
			return _value.CompareTo(testValue) == 0;
		}
	}

	internal class DoubleComparerPredicate
	{
		private Double _value;
		private PropertyDescriptor _property;

		public DoubleComparerPredicate(PropertyDescriptor property, String value)
		{
			if (property == null)
			{
				throw new ArgumentNullException("property");
			}

			if (property.PropertyType != typeof(Double))
			{
				throw new ArgumentException("property");
			}

			if (String.IsNullOrEmpty(value))
			{
				throw new ArgumentNullException("value");
			}

			_property = property;
			_value = Double.Parse(value);
		}

		public Boolean Matches(Object item)
		{
			if (item == null)
			{
				throw new ArgumentNullException("item");
			}

			if (_property.ComponentType != item.GetType())
			{
				throw new ArgumentException("item");
			}

			Double testValue = (Double) _property.GetValue(item);
			return _value.CompareTo(testValue) == 0;
		}
	}

	internal class BooleaneanComparerPredicate
	{
		private Boolean _value;
		private PropertyDescriptor _property;

		public BooleaneanComparerPredicate(PropertyDescriptor property, String value)
		{
			if (property == null)
			{
				throw new ArgumentNullException("property");
			}

			if (property.PropertyType != typeof(Boolean))
			{
				throw new ArgumentException("property");
			}

			if (String.IsNullOrEmpty(value))
			{
				throw new ArgumentNullException("value");
			}

			_property = property;
			_value = Boolean.Parse(value);
		}

		public Boolean Matches(Object item)
		{
			if (item == null)
			{
				throw new ArgumentNullException("item");
			}

			//if (_property.ComponentType != item.GetType())
			//{
			//    throw new ArgumentException("item");
			//}

			Boolean testValue = (Boolean) _property.GetValue(item);
			return _value.CompareTo(testValue) == 0;
		}
	}

	internal class DecimalComparerPredicate
	{
		private Decimal _value;
		private PropertyDescriptor _property;

		public DecimalComparerPredicate(PropertyDescriptor property, String value)
		{
			if (property == null)
			{
				throw new ArgumentNullException("property");
			}

			if (property.PropertyType != typeof(Decimal))
			{
				throw new ArgumentException("property");
			}

			if (String.IsNullOrEmpty(value))
			{
				throw new ArgumentNullException("value");
			}

			_property = property;
			_value = Decimal.Parse(value);
		}

		public Boolean Matches(Object item)
		{
			if (item == null)
			{
				throw new ArgumentNullException("item");
			}

			if (_property.ComponentType != item.GetType())
			{
				throw new ArgumentException("item");
			}

			Decimal testValue = (Decimal) _property.GetValue(item);
			return _value.CompareTo(testValue) == 0;
		}
	}

	internal class DateTimeComparerPredicate
	{
		private DateTime _value;
		private PropertyDescriptor _property;

		public DateTimeComparerPredicate(PropertyDescriptor property, String value)
		{
			if (property == null)
			{
				throw new ArgumentNullException("property");
			}

			if (property.PropertyType != typeof(DateTime))
			{
				throw new ArgumentException("property");
			}

			if (String.IsNullOrEmpty(value))
			{
				throw new ArgumentNullException("value");
			}

			_property = property;
			_value = DateTime.Parse(value);
		}

		public Boolean Matches(Object item)
		{
			if (item == null)
			{
				throw new ArgumentNullException("item");
			}

			if (_property.ComponentType != item.GetType())
			{
				throw new ArgumentException("item");
			}

			DateTime testValue = (DateTime) _property.GetValue(item);
			return _value.CompareTo(testValue) == 0;
		}
	}

	internal class StringComparerPredicate
	{
		private String _value;
		private PropertyDescriptor _property;
		private Regex m_regEx;

		public StringComparerPredicate(PropertyDescriptor property, String value, Boolean isRegex)
		{
			if (property == null)
			{
				throw new ArgumentNullException("property");
			}

			this._property = property;
			this._value = value;

			if (isRegex)
			{
				this.m_regEx = new Regex(_value, RegexOptions.IgnoreCase);
			}
			else
			{
				if (_value.IndexOfAny(new char[] { '%', '*' }) > -1)
				{
					StringBuilder expr = new StringBuilder();
					expr.Append("^");

					foreach (char c in _value)
					{
						if (c == '*' || c == '%')
						{
							expr.Append(".*");
						}
						else
						{
							expr.Append(c);
						}
					}

					this.m_regEx = new Regex(expr.ToString(), RegexOptions.IgnoreCase);
				}
			}
		}

		public Boolean Matches(Object item)
		{
			if (item == null)
			{
				throw new ArgumentNullException("item");
			}

			if (_property.ComponentType != item.GetType())
			{
				throw new ArgumentException("item");
			}

			String testValue = (String) _property.GetValue(item);
			Boolean result = false;

			if (String.IsNullOrEmpty(_value))
			{
				result = (String.IsNullOrEmpty(testValue));
			}
			else
			{
				if (!String.IsNullOrEmpty(testValue))
				{
					if (m_regEx == null)
					{
						result = (_value.CompareTo(testValue) == 0);
					}
					else
					{
						Match m = m_regEx.Match(testValue);
						result = m.Success;
					}
				}
			}

			return result;
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Evolve.Libraries.Utilities
{
	public static class FieldValidation
	{
		#region Validation
		public static String ValidateName(String name, Boolean allowNull)
		{
			return (name.Length > 0 ? "" : "A Name must be specified");
		}

		public static String ValidateCode(String name, Boolean allowNull)
		{
			return (name.Length > 0 ? "" : "A Code must be specified");
		}

		public static String ValidateNoNullAllowed(String name, Boolean allowNull,string text)
		{
			string message = "A " + text + " must be specified";
			
			return (name.Length > 0 ? "" : message);
		}

		public static String ValidateFirstName(String name, Boolean allowNull)
		{
			if (allowNull == false && name.Trim() == "")
			{
					return allowNull ? "" : "A First Name must be specified";
			}
			else
			{
				return (name.Length > 0 ? "" : "A First Name must be specified");
			}
		}

		public static String ValidateSurName(String name, Boolean allowNull)
		{
			if (allowNull == false && name.Trim() == "")
			{
				return allowNull ? "" : "A Surname name must be specified";
			}
			else
			{
				return (name.Length > 0 ? "" : "A Surname must be specified");
			}
		}

		public static String ValidateAccountPeriod(String value, Boolean allowNull)
		{
			Decimal decimalValue;			

			if (value.Length == 0)
			{
				return  "An Accounting period must be entered";
			}

			Boolean Isdecimal = Decimal.TryParse(value, out decimalValue);

			if (Isdecimal == false)
			{
				return "Please enter a decimal value in this field";
			}
			else
			{
				Int32 intValue = Convert.ToInt32(value);

				if (intValue > 12 || intValue < 1)
				{
					return "Period needs to be between 1 and 12";
				}

			}
			return "";
		}

		public static String ValidateAccountYear(String value, Boolean allowNull)
		{
			Decimal decimalValue;

			if (value.Length == 0)
			{
				return "An Accounting Year must be entered";
			}

			Boolean Isdecimal = Decimal.TryParse(value, out decimalValue);

			if (Isdecimal == false)
			{
				return "Please enter a decimal value in this field";
			}
			else
			{
				Int32 intValue = Convert.ToInt32(value);

				// Must be greater then 2001
				if (intValue < 2001)
				{
					return "Please enter a valid year > 2001";
				}
			}
			return "";
		}

		public static String ValidateBurnerTotal(String value, Boolean allowNull)
		{
			Int32 Int32Value;

			Boolean IsInt32 = Int32.TryParse(value, out Int32Value);

			if (!IsInt32)
			{
				return "Calculated Total fuel usage too high";
			}

			return "";
		}

		public static String ValidateOilAndGasTotal(String value, Boolean allowNull)
		{
			Int32 Int32Value;

			Boolean IsInt32 = Int32.TryParse(value, out Int32Value);

			if (!IsInt32)
			{
				return   value.ToString() + " - Usage too high";
			}

			return "";
		}

		public static String ValidateLandLine(String landLine)
		{
			return ValidateLandLine(landLine, false);
		}
		
		public static String ValidateLandLine(String landLine, Boolean allowNull)
		{
			if (landLine == "")
			{
				return allowNull ? "" : "Landline number must be provided";
			}
			else
			{
				string patternStrict = @"((\+44\s?\(0\)\s?\d{2,4})|(\+44\s?(01|02|03|07|08)\d{2,3})|(\+44\s?(1|2|3|7|8)\d{2,3})|(\(\+44\)\s?\d{3,4})|(\(\d{5}\))|((01|02|03|07|08)\d{2,3})|(\d{5}))(\s|-|.)(((\d{3,4})(\s|-)(\d{3,4}))|((\d{6,7})))";

				//string patternStrict = @"^(((\+44\s?|0044\s?)?|(\(?0))((2[03489]\)?\s?\d{4}\s?\d{4})|(1[23456789]1\)?\s?\d{3}\s?\d{4})|(1[23456789][234578][0234679]\)?\s?\d{6})|(1[2579][0245][0467]\)?\s?\d{5})|(11[345678]\)?\s?\d{3}\s?\d{4})|(1[35679][234689]\s?[46789][234567]\)?\s?\d{4,5})|([389]\d{2}\s?\d{3}\s?\d{4})|([57][0-9]\s?\d{4}\s?\d{4})|(500\s?\d{6})|(7[456789]\d{2}\s?\d{6})))";

				Regex reStrict = new Regex(patternStrict);
				return reStrict.IsMatch(landLine) ? "" : "Landline number is not valid";
			}
		}

		public static String ValidateEmail(String email)
		{
			return ValidateEmail(email, false);
		}

		public static String ValidateEmail(String email, Boolean allowNull)
		{
			if (email == "")
			{
				return allowNull ? "" : "Email must be provided";
			}
			else
			{
				string patternStrict = @"^(([^<>()[\]\\.,;:\s@\""]+"
							+ @"(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@"
							+ @"((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"
							+ @"\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+"
							+ @"[a-zA-Z]{2,}))$";

				Regex reStrict = new Regex(patternStrict);

				return reStrict.IsMatch(email) ? "" : "Email address not valid";
			}	
		}

		public static String ValidateMobile(String mobileno)
		{
			return ValidateMobile(mobileno, false);
		}

		public static String ValidateMobile(String mobileno, Boolean allowNull)
		{
			if (mobileno == "")
			{
				return allowNull ? "" : "Mobile number must be provided";
			}
			else
			{
				string patternStrict = @"^\+?(?:44|0|44 0)\s?7[5789][\s\d]{8}$";

				Regex reStrict = new Regex(patternStrict);

				return reStrict.IsMatch(mobileno) ? "" : "Mobile number is not valid";
			}
		}

		public static String ValidatePayroll(String payroll)
		{
			return ValidatePayroll(payroll, false);
		}
		
		public static String ValidatePayroll(String payroll, Boolean allowNull)
		{
			if (payroll == "")
			{
				return allowNull ? "" : "A Payroll number must be provided";
			}
			else
			{
				string Str = payroll;
				double Num;
				bool isNum = double.TryParse(Str, out Num);

				if (!isNum)
				{
					return "The Payroll number is not valid";
				}
				else
				{
					return "";
				}
			}
		}

		public static String ValidateBankAccountNo(String bankaccountno)
		{
			return ValidateBankAccountNo(bankaccountno, false);
		}

		public static String ValidateBankAccountNo(String bankaccountno, Boolean allowNull)
		{
			if (bankaccountno == "")
			{
				return allowNull ? "" : "A Bank Account number must be provided";
			}
			else
			{
				string Str = bankaccountno;
				double Num;
				bool isNum = double.TryParse(Str, out Num);
				if (!isNum)
				{
					return "Bank Account number is not valid";
				}
				else
				{
					return "";
				}
			}
		}

		public static String ValidateBankSortCode(String banksortcode)
		{
			return ValidateBankSortCode(banksortcode, false);
		}

		public static String ValidateBankSortCode(String banksortcode, Boolean allowNull)
		{
			if (banksortcode == "")
			{
				return allowNull ? "" : "A Bank Sort Code must be provided";
			}
			else
			{
				string Str = banksortcode;
				double Num;
				bool isNum = double.TryParse(Str, out Num);
				if (!isNum)
				{
					return "Bank Sort Code is not valid";
				}
				else
				{
					return "";
				}
			}
		}

		public static String IsDecimal(String value, Boolean allowNull)
		{
			Decimal decimalValue;

			Boolean Isdecimal = Decimal.TryParse(value, out decimalValue);

			if (Isdecimal == false)
			{
				return "Please enter a decimal value in this field";
			}
			return "";
		}

		public static String IsInt32(String value, Boolean allowNull)
		{
			Int32 Int32Value;

			if (value == "")
            {
                if (allowNull)
                {
                    return "";
                }
            }

            Boolean IsInt32 = Int32.TryParse(value, out Int32Value);

            if (IsInt32 == false)
            {
                return "Please enter a Numeric value in this field";
            }
                  
			return "";
		}

        public static String IsPositive(String value, Boolean mandatory)
        {
            Int32 Int32Value;
            
            if(String.IsNullOrEmpty(value))
            {
                if (!mandatory)
                {
                    return "";
                }
            }

            Boolean IsInt32 = Int32.TryParse(value, out Int32Value);

            if (IsInt32)
            {
				if (Int32Value < 0)
				{
					return "Please enter a positive value";
				}
            }
            else
            {
				return "Please enter a valid Numeric value in this field";
            }

            return "";
        }
		
		public static String ValidateMessage(String name, Boolean allowNull)
		{
			return (name.Length > 0 ? "" : "A message must be specified");
		}

		public static String ValidateTextField(String name, Boolean allowNull)
		{
			if (name == "")
			{
				//return allowNull ? "" : "This field cannot be left empty.";
				if (!allowNull)
				{
					return "This field cannot be left empty.";
				}
				else
				{
					return "";
				}
			}
			else
			{
				return "";
			}
		}

		public static String ValidateEffectiveDate(DateTime toDay, DateTime endDate, Boolean allowNull)
		{
			// check the dates do not overlap
			if (toDay > endDate)
			{
				return "The effective date must be today or in the future";
			}

			return "";
		}

		public static String ValidateTargetDate(DateTime toDay, DateTime targetDate, Boolean allowNull)
		{
			// check the dates do not overlap
			if (toDay >= targetDate)
			{
				return "This target date must be in the future";
			}

			return "";
		}

		public static String ValidateLTIDate(DateTime toDay, DateTime ltiDate, Boolean allowNull)
		{
			// check the dates do not overlap
			if (toDay <= ltiDate)
			{
				return "This LTI date must be in the past";
			}

			return "";
		}

        public static String ValidateRate(String value, Boolean allowNull)
        {
            Decimal decimalValue;

            if (value.Length == 0)
            {
                return "A rate must be entered";
            }

            Boolean Isdecimal = Decimal.TryParse(value, out decimalValue);

            if (Isdecimal == false)
            {
                return "Please enter a decimal value in this field";
            }
            else
            {
                Decimal intValue = Convert.ToDecimal(value);

				if (intValue > Convert.ToDecimal(999.99))
				{
					return "Rate greater than 999.99";
				}
            }
            return "";
        }

		public static String ValidateSummary(String name, Boolean allowNull)
		{
			return (name.Length > 0 ? "" : "Please enter Summary details");
		}

		public static String ValidateFaultlogDescription(String name, Boolean allowNull)
		{
			return (name.Length > 0 ? "" : "Please enter fault log description details");
		}

		#endregion Validation
	}
}

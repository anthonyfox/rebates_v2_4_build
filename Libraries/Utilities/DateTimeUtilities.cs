﻿using System;
using System.Windows.Forms;
namespace Evolve.Libraries.Utilities
{
	public static class DateTimeUtilities
	{
		#region Date
		private const CurrentWeek DEFAULT_CURRENT_WEEK = CurrentWeek.ThisWeek;
		private const string DEFAULT_DATE_FORMAT = "dd-MMM-yyyy";

		public static String Date(DateTime dateTime)
		{
			return Date(dateTime, DEFAULT_CURRENT_WEEK, DEFAULT_DATE_FORMAT);
		}
		public static String Date(DateTime dateTime, CurrentWeek currentWeek)
		{
			return Date(dateTime, currentWeek, DEFAULT_DATE_FORMAT);
		}
		public static String Date(DateTime dateTime, String dateFormat)
		{
			return Date(dateTime, DEFAULT_CURRENT_WEEK, dateFormat);
		}
		public static String Date(DateTime dateTime, CurrentWeek currentWeek, String dateFormat)
		{
			String formattedDate = String.Empty;

			// Calculate the dates for Today, Tomorrow and Yesterday without any time component
			DateTime today = DateTime.Today;
			DateTime tomorrow = today.AddDays(1);
			DateTime yesterday = today.AddDays(-1);

			// String the time component from our date
			DateTime date = dateTime.Date;

			// Always use Today, Yesterday or Tomorrow if matched, regardless of what th current week is
			switch (date.CompareTo(today))
			{
				case 0:
					formattedDate = "Today";
					break;
				case -1:
					if (date.CompareTo(yesterday) == 0)
					{
						formattedDate = "Yesterday";
					}
					break;
				case 1:
					if (date.CompareTo(tomorrow) == 0)
					{
						formattedDate = "Tomorrow";
					}
					break;

			}

			// Not Today, Yesterday or Tomorrow, so see if it within the current week
			if (formattedDate == String.Empty)
			{
				bool isContemporaryDate = false;

				switch (currentWeek)
				{
					case CurrentWeek.LastWeek:
						if (date >= today.AddDays(-6) && date <= today)
						{
							isContemporaryDate = true;
						}
						break;
					case CurrentWeek.ThisWeek:
						if (date >= today.AddDays(-3) && date <= today.AddDays(3))
						{
							isContemporaryDate = true;
						}
						break;
					case CurrentWeek.NextWeek:
						if (date >= today && date <= today.AddDays(6))
						{
							isContemporaryDate = true;
						}
						break;
				}

				// If within current week use weekday name otherwise use user supplied date format
				if (isContemporaryDate)
				{
					formattedDate = date.ToString("dddd");
				}
				else
				{
					if (String.IsNullOrEmpty(dateFormat))
					{
						formattedDate = dateTime.ToShortDateString();
					}
					else
					{
						formattedDate = dateTime.ToString(dateFormat);
					}
				}
			}

			return formattedDate;
		}
		#endregion

		#region RoundTimeSpan

		public static TimeSpan RoundTimeSpan(TimeSpan timeSpan, Int16 round)
		{
			// Count of round number in this total minutes...

			double countRound = (timeSpan.TotalMinutes / round);

			// The main formula to calculate round time...

			int minutes = (int) Math.Truncate(countRound + 0.5) * round;

			return new TimeSpan(0, minutes, 0);

		}


		#endregion

		#region RoundDateTime

		public static DateTime RoundDateTime(DateTime dateTime, Int16 round)
		{
			TimeSpan timeSpan = dateTime.TimeOfDay;
			timeSpan = DateTimeUtilities.RoundTimeSpan(timeSpan, round);
			return dateTime.Date + timeSpan;
		}

		public static TimeSpan GetTimeDifference(DateTimePicker dateTimePicker1, DateTimePicker dateTimePicker2)
		{
			return dateTimePicker2.Value - dateTimePicker1.Value;
		}

		public static TimeSpan GetTimeDifferenceDateTime(DateTime Time1, DateTime Time2)
		{
			return Time1 - Time2;
		}

		public static String SameDateEntered(DateTime compareDate, DateTime enteredDate, Boolean allowNull)
		{
			// check the dates do not match.
			if (compareDate.Date == enteredDate.Date)
			{
				return "This Effective Date has already been used";
			}

			return "";
		}

		public static String ValidateStartDate(DateTime? startDate, DateTime? endDate, Boolean allowNull)
		{
			// check the dates do not overlap
			if (startDate.Value != null && endDate.Value != null)
			{
				if (startDate > endDate)
				{
					return "The Start Date must be before the End date";
				}
			}

			return "";
		}

		public static String ValidateEndDate(DateTime? startDate, DateTime? endDate, Boolean allowNull)
		{
			// check the dates do not overlap
			if (startDate.Value != null && endDate.Value != null)
			{
				if (endDate < startDate)
				{
					return "The End Date must be after the Start date";
				}
			}
			return "";
		}

		public static String ValidateStartDateNull(DateTime? startDate, DateTime? endDate, Boolean allowNull)
		{
			// check the dates do not overlap
			if (startDate > endDate)
			{
				return "The Start Date must be before the End date";
			}
			else
			{
				if (startDate == endDate)
				{
					return "Start and End Dates cannot be the same";
				}
			}

			return "";
		}

		public static String ValidateEndDateNull(DateTime? startDate, DateTime? endDate, Boolean allowNull)
		{
			// check the dates do not overlap
			if (endDate < startDate)
			{
				return "The End Date must be after the Start date";
			}
			else
			{
				if (startDate == endDate)
				{
					return "Start and End Dates cannot be the same";
				}
			}
			return "";
		}

		public static String ValidateStartStopTimes(DateTime startDate, DateTime endDate, Boolean allowNull)
		{
			// check the dates do not overlap
			if (endDate < startDate)
			{
				return "The Stop Time must be after the Start Time";
			}
			return "";
		}

		public static String FutureDate(DateTimePicker date)
		{
			// check the dates do not match.
			if (date.Value > DateTime.Today)
			{
				return "Cannot be a future date";
			}

			return "";
		}

		#endregion

		public static DateTime GetFirstDayOfMonth(DateTime givenDate)
		{
			return new DateTime(givenDate.Year, givenDate.Month, 1);
		}

		public static DateTime GetTheLastDayOfMonth(DateTime givenDate)
		{
			return GetFirstDayOfMonth(givenDate).AddMonths(1).Subtract(new TimeSpan(1, 0, 0, 0, 0));
		}
	}

	public enum CurrentWeek
	{
		LastWeek,
		ThisWeek,
		NextWeek
	}


}

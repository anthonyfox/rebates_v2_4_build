using System;
using System.Collections;
using System.Reflection;

namespace Evolve.Libraries.Utilities
{
    public class ObjectComparer : IComparer
    {
		private String _sort;

		private enum SortDirection
		{
			Ascending,
			Descending
		}

		public ObjectComparer(String sort)
		{
			_sort = sort;
		}

		/// <summary>
		/// Compares two objects and returns the result of Object.CompareTo()
		/// </summary>
        public Int32 Compare(Object x, Object y)
        {
			Int32 result = 0;
			String[] propertyNames = _sort.Split(new Char[] { ',' });

			for (Int32 index = 0; index < propertyNames.Length; index++)
			{
				SortDirection sortDirection = SortDirection.Ascending;

				if (propertyNames[index].ToUpper().EndsWith(" ASC"))
				{
					propertyNames[index] = propertyNames[index].Remove(propertyNames[index].Length - 4);
				}

				if (propertyNames[index].ToUpper().EndsWith(" DESC"))
				{
					propertyNames[index] = propertyNames[index].Remove(propertyNames[index].Length - 5);
					sortDirection = SortDirection.Descending;
				}

				propertyNames[index] = propertyNames[index].Trim();
				PropertyInfo propertyInfo = x.GetType().GetProperty(propertyNames[index]);

				IComparable x1 = propertyInfo.GetValue(x, null) as IComparable;
				IComparable y1 = propertyInfo.GetValue(y, null) as IComparable;

				result = (sortDirection == SortDirection.Ascending) ? x1.CompareTo(y1) : y1.CompareTo(x1);

				if (result != 0)
				{
					break;
				}
			}

			return result;
        }
    }
}
using System;

namespace Evolve.Libraries.Utilities
{
	public static class StringValidation
	{
		public static Boolean IsInt16(String text)
		{
			Int16 integerValue;
			return Int16.TryParse(text, out integerValue);
		}

		public static Boolean IsInt32(String text)
		{
			Int32 integerValue;
			return Int32.TryParse(text, out integerValue);
		}

		public static Boolean IsDecimal(String text)
		{
			Decimal decimalValue;
			return Decimal.TryParse(text, out decimalValue);
		}
	}
}

using System;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;

namespace Evolve.Libraries.Utilities
{
	public static class ValidateControl
	{
		public static Control ControlWithError(Control control, ErrorProvider errorProvider)
		{
			Control controlWithError = null;

			if (control.HasChildren)
			{
				foreach (Control childControl in control.Controls)
				{
					if (childControl.HasChildren)
					{
						controlWithError = ValidateControl.ControlWithError(childControl, errorProvider);

						if (controlWithError == null)
						{
							if (errorProvider.GetError(childControl) != "")
							{
								controlWithError = childControl;
							}
						}
					}
					else
					{
						if (errorProvider.GetError(childControl) != "")
						{
							controlWithError = childControl;
						}
					}

					if (controlWithError == null && childControl is DataGridView)
					{
						if (DataGridViewHasErrors((DataGridView) childControl))
						{
							controlWithError = childControl;
						}
					}

					if (controlWithError != null)
					{
						break;
					}
				}
			}
			else
			{
				if (errorProvider.GetError(control) != "")
				{
					controlWithError = control;
				}
				else
				{
					if (control is DataGridView)
					{
						if (DataGridViewHasErrors((DataGridView) control))
						{
							controlWithError = control;
						}
					}
				}
			}

			if (controlWithError == null && control is DataGridView)
			{
				if (DataGridViewHasErrors((DataGridView) control))
				{
					controlWithError = control;
				}
			}
			return controlWithError;
		}

		public static Boolean HasErrors(Control control, ErrorProvider errorProvider)
		{
			Boolean hasErrors = false;

			if (control.HasChildren)
			{
				foreach (Control childControl in control.Controls)
				{
					if (childControl.HasChildren)
					{
						hasErrors = ValidateControl.HasErrors(childControl, errorProvider);

						if (!hasErrors)
						{
							hasErrors = (errorProvider.GetError(childControl) != "");
						}
					}
					else
					{
						if (errorProvider.GetError(childControl) != "")
						{
							hasErrors = true;
						}
					}
					if (!hasErrors && childControl is DataGridView)
					{
						hasErrors = DataGridViewHasErrors((DataGridView) childControl);
					}

					if (hasErrors)
					{
						break;
					}
				}
			}
			else
			{
				if (errorProvider.GetError(control) != "")
				{
					hasErrors = true;
				}
			}

			if (!hasErrors && control is DataGridView)
			{
				hasErrors = DataGridViewHasErrors((DataGridView) control);
			}
			return hasErrors;
		}

		public static Boolean DataGridViewHasErrors(DataGridView dataGridView)
		{
			Boolean hasErrors = false;

			for (Int32 row = 0; row < dataGridView.RowCount; row++)
			{
				String errorText = dataGridView.Rows[row].ErrorText;
				if (errorText.Length == 0)
				{
					for (Int32 column = 0; column < dataGridView.ColumnCount; column++)
					{
						errorText = dataGridView.Rows[row].Cells[column].ErrorText;
						if (errorText.Length > 0)
						{
							break;
						}
					}
				}

				if (errorText.Length > 0)
				{
					hasErrors = true;
					break;
				}
			}

			return hasErrors;
		}
	}

	public static class ControlUtilities
	{
		public static Icon GetIcon(Control control)
		{
			Icon icon = null;
			Control parent = control.Parent;

			while (parent != null)
			{
				if (parent is Form)
				{
					icon = ((Form) parent).Icon;
					break;
				}

				parent = parent.Parent;
			}

			return icon;
		}

		public delegate void SetTextCallback(Form form, TextBox textBox, String text);

		public static void SetText(Form form, TextBox textBox, String text)
		{
			if (textBox.InvokeRequired)
			{
				SetTextCallback d = new SetTextCallback(ControlUtilities.SetText);
				form.Invoke(d, new Object[] { form, textBox, text });
			}
			else
			{
				textBox.Text = text;
			}
		}
	}

	public static class FormUtilities
	{
		public static Bitmap Screenshot(Form activeForm)
		{
			activeForm.BringToFront();
			Graphics graphics = null;
			Bitmap bitmap = new Bitmap(activeForm.Width, activeForm.Height);

			try
			{
				graphics = Graphics.FromImage(bitmap);
				graphics.CopyFromScreen(activeForm.Location, new Point(0, 0), bitmap.Size);
			}
			finally
			{
				if (graphics != null)
				{
					graphics.Dispose();
				}
			}

			return bitmap;
		}

		public static void SetFocus(Control controlToFocus)
		{
			// Treewalk control parents until we hit a form selecting every TabPage we come across
			Control control = controlToFocus;

			while(!(control is Form))
			{
				if (control is TabPage)
				{
					TabPage tabPage = (TabPage) control;
					TabControl tabControl = (TabControl) tabPage.Parent;

					tabControl.SelectTab(tabPage);
				}
				control = control.Parent;
			}

			// Treewalk control parents until we find a control that can focus
			control = controlToFocus;

			do
			{
				if (control.CanFocus)
				{
					control.Focus();
					break;
				}
				control = control.Parent;
			}
			while (!(control is Form));
		}
	}
}
